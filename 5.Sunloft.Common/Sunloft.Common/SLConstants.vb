﻿Public Class SLConstants

  Public Const DURATION As Integer = 3000

  Public Class TAX
    Public Const TAX_EXCLUDED As String = "税抜"
    Public Const TAX_INCLUDED As String = "税込"
    Public Const NO_TAX As String = "非"

    Public Const TAX_EXCLUDED_CODE As Integer = 0
    Public Const TAX_INCLUDED_CODE As Integer = 1
  End Class

  Public Enum ComboType
    WareHouse = 0
    Shipping
  End Enum

  Public Enum GDOSearchIndex As Short
    None = 0
    Customer
    Supplier
    Product
    Warehouse
    Project
    Remarks
  End Enum

  ''' <summary>EMS(区分マスタ)</summary>
  ''' <remarks></remarks>
  Public Enum EMS
    CostomerKbn1 = 11
    CostomerKbn2 = 12
    CostomerKbn3 = 13
    CostomerKbn4 = 14
    CostomerKbn5 = 15
    ProductKbn1 = 21
    ProductKbn2 = 22
    ProductKbn3 = 23
    ProductKbn4 = 24
    ProductKbn5 = 25
    SuppilerKbn1 = 31
    SuppilerKbn2 = 32
    SuppilerKbn3 = 33
    SuppilerKbn4 = 34
    SuppilerKbn5 = 35

    ''' <summary>Memo(Inquirer)</summary>
    EstimationMemo1 = 61

    ''' <summary>Memo(Delivery deadline)</summary>
    EstimationMemo2 = 62

    ''' <summary>Memo(Delivery place)</summary>
    EstimationMemo3 = 63

    ''' <summary>Memo(Payment conditions)</summary>
    EstimationMemo4 = 64

    ''' <summary>Memo(Estimates deadline)</summary>
    EstimationMemo5 = 65

    ''' <summary>Memo(Estimated requirements)</summary>
    EstimationMemo6 = 66

    MemoJ = 67
    MemoN = 68
    MemoS = 69
    Warehouse = 70
  End Enum

  Public Class WarehouseKbn
    Public Const WAREHOUSE_ALL As String = " 全数" & vbNewLine & " 入荷"
    Public Const WAREHOUSE_APART As String = " 一部" & vbNewLine & " 入荷"
    Public Const WAREHOUSE_ALL_CODE As Integer = 0
    Public Const WAREHOUSE_APART_CODE As Integer = 1

    Public Const NORMAL_WAREHOUSING_CODE As Integer = 0
    Public Const RETURN_GOODS_CODE As Integer = 1
    Public Const NORMAL_WAREHOUSING As String = "通常入荷"
    Public Const RETURN_GOODS As String = "返品"
  End Class

  Public Class ShipmentKbn
    Public Const SHIPPING_ALL As String = " 全数" & vbNewLine & " 出荷"
    Public Const SHIPPING_APART As String = " 一部" & vbNewLine & " 出荷"

    Public Const NORMAL_SHIPPING_CODE As Integer = 0
    Public Const RETURN_GOODS_CODE As Integer = 1
    Public Const NORMAL_SHIPPING As String = "通常出荷"
    Public Const RETURN_GOODS As String = "返品"
  End Class

  Public Class ConfirmMessage
    Public Const CONFIRM_MESSAGE As String = "よろしいですか？"
    Public Const TITLE_MESSAGE As String = "確認"
    Public Const TRANSFER_CONFIRM As String = "PCA出荷/仕入伝票への振替処理を行います。" & CONFIRM_MESSAGE
    Public Const DELETE_CONFIRM_MESSAGE As String = "削除しても" & CONFIRM_MESSAGE
    Public Const COPY_CONFIRM_MESSAGE As String = "複写しますか？"
    Public Const PRINT_CONFIRM_MESSAGE As String = "印刷しても" & CONFIRM_MESSAGE
    Public Const REGISTER_CONFIRM_MESSAGE As String = "登録しても" & CONFIRM_MESSAGE
    Public Const CONVERT_CONFIRM_MESSAGE As String = "振替を実行します。" & CONFIRM_MESSAGE
    Public Const DELETE_ROW_CONFIRM_MESSAGE As String = "この明細行を削除しても" & CONFIRM_MESSAGE
  End Class

  Public Class CommonMessage
    Public Const COMPLATE_MESSAGE As String = "が完了しました"
    Public Const TITLE_MESSAGE As String = "完了"
    Public Const SUCCESS_MESSAGE As String = "登録" & COMPLATE_MESSAGE
    Public Const TRANSFER_SUCCESS_MESSAGE As String = "振替" & COMPLATE_MESSAGE
    Public Const SUCCESS_CARRYOVER As String = "在庫繰越処理" & COMPLATE_MESSAGE
  End Class

  Public Class NotifyMessage
    Public Const EXCESS_MESSAGE As String = "を超過しています"
    Public Const NOT_FOUND As String = "が見つかりませんでした"
    Public Const CANNOT_REGISTER_SLIP As String = "登録はできません"
    Public Const INPUT_REQUIRED As String = "を入力してください"
    Public Const SELECT_REQUIRED As String = "を選択してください"
    Public Const DATA_NOT_FOUND As String = "対象データがありません"
    Public Const PRINT_DATA_NOT_FOUND As String = "印刷データがありません"
    Public Const CANNOT_DELETE_SLIP As String = "削除できません"
    Public Const TITLE_MESSAGE As String = "確認"

    Public Const NUMBER_OVER As String = "本数が在庫本数" & EXCESS_MESSAGE
    Public Const QUANTITY_OVER As String = "数量が在庫数量" & EXCESS_MESSAGE
    Public Const NUMBER_OVER_WH As String = "本数が入庫本数" & EXCESS_MESSAGE
    Public Const QUANTITY_OVER_wH As String = "数量が入庫数量" & EXCESS_MESSAGE
    Public Const TRANSFER_SLIP_UNSELECTED As String = "振替する行を選択してください"
    Public Const TRANSFER_NUMBER_OVER As String = "移動" & NUMBER_OVER
    Public Const TRANSFER_QUANTITY_OVER As String = "移動" & QUANTITY_OVER
    Public Const ADJUST_NUMBER_OVER_ST As String = "調整" & NUMBER_OVER
    Public Const ADJUST_QUANTITY_OVER_ST As String = "調整" & QUANTITY_OVER
    Public Const ADJUST_NUMBER_OVER_WH As String = "調整" & NUMBER_OVER_WH
    Public Const ADJUST_QUANTITY_OVER_WH As String = "調整" & QUANTITY_OVER_wH
    Public Const SUPPILER_NOT_INPUT_MESSAGE As String = "仕入先が未入力です"
    Public Const STOCKTAKING_NUMBER_OVER As String = "棚卸本数が在庫本数" & EXCESS_MESSAGE
    Public Const STOCKTAKING_QUANTITY_OVER As String = "棚卸数量が在庫数量" & EXCESS_MESSAGE
    Public Const PREVIOUS_MESSAGE_NOT_FOUND As String = "前伝票" & NOT_FOUND
    Public Const NEXT_MESSAGE_NOT_FOUND As String = "次伝票" & NOT_FOUND

    Public Const CANNOT_CARRYOVER As String = "年月日が月末の日付ではないため、繰越処理ができません"
    Public Const MASTER_NOT_FOUND As String = "が未登録です"
    Public Const IMPROPRIETY_INPUTDATA As String = "入力値が不正です"

    Public Const CANNOT_REG_BY_STOCKTAKING As String = "棚卸を行った伝票なので" & CANNOT_REGISTER_SLIP
    Public Const CANNOT_REG_BY_CLOSEDATE As String = "調整日付が締日を超過しているので" & CANNOT_REGISTER_SLIP

    Public Const CANNOT_DELETE_BY_CONVERTED As String = "振替済みのため" & CANNOT_DELETE_SLIP
  End Class

  Public Class CSV
    Public Const CSV_FILE_FILTER As String = "CSVファイル (*.csv)|*.csv"
    Public Const CSV_OUTPUT_COMPLETE_MESSAGE As String = "CSVの出力が完了しました"
  End Class

  Public Const SYSTEM_CODE As Integer = 0
  Public Const NOTHING_DATE As String = "0:00:00"

  ''' <summary>SlipType [SL_ZDN]</summary>
  ''' <remarks>在庫詳細現在データ:入庫区分</remarks>
  Public Enum SL_ZDNSlipType As Integer
    Warehousing = 1
    Manufacture = 2
    TransWarehousing = 3
  End Enum

  Public Enum SL_SYKH_RelaySlipType As Integer
    ManualShipping = 0
    OrderReceiving = 1
    Sales = 2
  End Enum

  Public Enum SetLotType
    Manual
    Auto
  End Enum


  ''' <summary>SlipType [SL_ZHK]</summary>
  ''' <remarks>在庫引当データ:出庫区分</remarks>
  Public Enum SL_ZHKSlipType As Integer
    ''' <summary>Warehousing</summary>
    ''' <remarks>入庫(実際にZHKには存在しないが受払上存在。)</remarks>
    Warehousing = 0

    ''' <summary>Shipping</summary>
    ''' <remarks>出庫</remarks>
    Shipping = 1

    ''' <summary>Manufacturing (Materials)</summary>
    ''' <remarks>製造(材料)</remarks>
    Manufacturing = 2

    ''' <summary>Transfer</summary>
    ''' <remarks>移動</remarks>
    Transfer = 3

    ''' <summary>WarehouseAdjustment</summary>
    ''' <remarks>在庫調整</remarks>
    WarehouseAdjustment = 4

    ''' <summary>ReturnWarehousing</summary>
    ''' <remarks>入庫返品</remarks>
    ReturnWarehousing = 5
  End Enum

  Public Enum SL_ZADJType
    ''' <summary>Shipping</summary>
    ''' <remarks>出庫</remarks>
    Shipping = 1

    ''' <summary>Warehousing</summary>
    ''' <remarks>入庫</remarks>
    Warehousing = 2

    ''' <summary>Stocktaking(-)</summary>
    ''' <remarks>棚卸(出庫)</remarks>
    StocktakingMinus = 11

    ''' <summary>Stocktaking(+)</summary>
    ''' <remarks>棚卸(入庫)</remarks>
    StocktakingPlus = 12

  End Enum

End Class

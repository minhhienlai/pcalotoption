﻿Option Strict On
Imports System.Text
Imports Sunloft.PCAIF
Imports PCA.TSC.Kon.Tools
Imports System.Windows.Forms
Imports Sunloft.Message

Public Class SLCmnFunction


  Public Shared Function standardlizeCode(ByVal strCodeText As String, ByVal intNoOfDigit As Integer) As String
    Try
      If IsNumeric(strCodeText) Then
        If strCodeText.Length < intNoOfDigit Then
          For intCount As Integer = 1 To intNoOfDigit - strCodeText.Length
            strCodeText = "0" & strCodeText
          Next

        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

    Return strCodeText
  End Function

  Public Shared Function checkObjectNothingEmpty(ByVal cell As Object) As Boolean
    Try
      If cell Is Nothing Then Return True
      If cell.ToString.Trim = String.Empty Then Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
    
    Return False
  End Function

  Public Shared Function StartUpInputExe(ByVal exeName As String, ByVal loginString As String, ByVal key As String) As Boolean
    Try
      Dim process As New Process
      If IO.File.Exists(exeName) Then
        Dim startInfo As ProcessStartInfo = New ProcessStartInfo(exeName)
        startInfo.Arguments = loginString & " " & key 'Pass loginstring (to log in PCA) and key (ex: slip no....)
        process.Start(startInfo)
      End If
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
    
  End Function

  ''' <summary>get last day</summary>
  ''' <param name="pYear"></param>
  ''' <param name="pMonth"></param>
  ''' <returns></returns>
  ''' <remarks>指定した日付の月末を返します。</remarks>
  Public Shared Function GetLastDay(Optional ByVal pYear As Integer = 0, Optional ByVal pMonth As Integer = 0) As Integer

    Try
      Dim intYear As Integer
      Dim intMonth As Integer
      If pYear > 0 Then intYear = pYear Else intYear = Year(Now)
      If pMonth > 0 Then intMonth = pMonth Else intMonth = Month(Now)

      Dim dtDate = New Date(intYear, intMonth, 1)

      dtDate = dtDate.AddMonths(1)
      dtDate = dtDate.AddDays(-1)

      Return CInt(dtDate.ToString("yyyyMMdd"))
    Catch ex As Exception
      Return 0
    End Try

  End Function

#Region " ShowToolTip"

  ''' <summary>ShowToolTip</summary>
  ''' <param name="strDisplayString">DisplayString</param>
  ''' <param name="strTitle">DisplayTitle</param>
  ''' <param name="TooltipIcon">TooltipIcon</param>
  ''' <param name="intDuration">second of display</param>
  ''' <param name="argPosForm">DrawingPoint</param>
  ''' <param name="OwnerToolTip">OwnerToolTip</param>
  ''' <param name="Owner">Owner</param>
  ''' <returns></returns>
  ''' <remarks>Show ToolTipMessage (FormPosition)</remarks>
  ''' 
  Public Shared Function ShowToolTip(ByVal strDisplayString As String, ByVal strTitle As String, ByVal TooltipIcon As System.Windows.Forms.ToolTipIcon, _
                                     intDuration As Integer, ByVal argPosForm As System.Drawing.Point, ByVal OwnerToolTip As System.Windows.Forms.ToolTip, _
                                     ByVal Owner As System.Windows.Forms.IWin32Window) As Boolean

    Dim PosForm As System.Drawing.Point = argPosForm
    Dim intX As Integer = PosForm.X
    Dim intY As Integer = PosForm.Y

    With OwnerToolTip
      .ToolTipTitle = strTitle
      .ToolTipIcon = TooltipIcon
      .Show(strDisplayString, Owner, intX, intY, intDuration)
    End With

    Return True
  End Function

  ''' <summary>ShowToolTip</summary>
  ''' <param name="strDisplayString">DisplayString</param>
  ''' <param name="strTitle">DisplayTitle</param>
  ''' <param name="TooltipIcon">TooltipIcon</param>
  ''' <param name="intDuration">second of display</param>
  ''' <param name="PcaFunctionBar">PcaFunctionBar</param>
  ''' <param name="OwnerToolTip">OwnerToolTip</param>
  ''' <param name="Owner">Owner</param>
  ''' <returns></returns>
  ''' <remarks>Show ToolTipMessage (PcaFunctionBar)</remarks>
  ''' 
  Public Shared Function ShowToolTip(ByVal strDisplayString As String, ByVal strTitle As String, ByVal TooltipIcon As System.Windows.Forms.ToolTipIcon, _
                                     intDuration As Integer, ByVal PcaFunctionBar As PCA.Controls.PcaFunctionBar, ByVal OwnerToolTip As System.Windows.Forms.ToolTip, _
                                     ByVal Owner As System.Windows.Forms.IWin32Window, ByVal intFunctionKeyID As Integer) As Boolean
    Try
      Dim intX As Integer
      Dim intY As Integer
      Dim intBtnWidth As Integer = CInt(PcaFunctionBar.Width / 12)
      If intFunctionKeyID > 0 Then
        intX = CInt((intBtnWidth * intFunctionKeyID + intBtnWidth * (intFunctionKeyID - 1)) / 2)
        intY = PcaFunctionBar.Location.Y
      End If
      With OwnerToolTip
        .ToolTipTitle = strTitle
        .ToolTipIcon = TooltipIcon
        .Show(strDisplayString, Owner, intX, intY, intDuration)
      End With

    Catch ex As Exception
      Return False
    End Try
    Return True
  End Function

  ''' <summary>ShowToolTip</summary>
  ''' <param name="strDisplayString">DisplayString</param>
  ''' <param name="strTitle">DisplayTitle</param>
  ''' <param name="TooltipIcon">TooltipIcon</param>
  ''' <param name="intDuration">second of display</param>
  ''' <param name="PcaCodeset">PcaCodeset</param>
  ''' <param name="OwnerToolTip">OwnerToolTip</param>
  ''' <param name="Owner">Owner</param>
  ''' <returns></returns>
  ''' <remarks>Show ToolTipMessage (PcaCodeset)</remarks>
  '''
  Public Shared Function ShowToolTip(ByVal strDisplayString As String, ByVal strTitle As String, ByVal TooltipIcon As System.Windows.Forms.ToolTipIcon, _
                                     intDuration As Integer, ByVal PcaCodeset As PCA.Controls.PcaCodeSet, ByVal OwnerToolTip As System.Windows.Forms.ToolTip, _
                                     ByVal Owner As System.Windows.Forms.IWin32Window) As Boolean

    Try
      Dim intX As Integer
      Dim intY As Integer
      intX = PcaCodeset.Left + PcaCodeset.MaxNameLength + PcaCodeset.MaxCodeLength
      intY = PcaCodeset.Top - PcaCodeset.Height
      With OwnerToolTip
        .ToolTipTitle = strTitle
        .ToolTipIcon = TooltipIcon
        .Show(strDisplayString, Owner, intX, intY, intDuration)
      End With

    Catch ex As Exception
      Return False
    End Try
    Return True
  End Function

  ''' <summary>ShowToolTip</summary>
  ''' <param name="strDisplayString">DisplayString</param>
  ''' <param name="strTitle">DisplayTitle</param>
  ''' <param name="TooltipIcon">TooltipIcon</param>
  ''' <param name="intDuration">second of display</param>
  ''' <param name="Rectangle">TscMeisaiTable Rectangle</param>
  ''' <param name="OwnerToolTip">OwnerToolTip</param>
  ''' <param name="Owner">Owner</param>
  ''' <returns></returns>
  ''' <remarks>Show ToolTipMessage (TscMeisaiTable.Rectangle)</remarks>
  '''
  Public Shared Function ShowToolTip(ByVal strDisplayString As String, ByVal strTitle As String, ByVal TooltipIcon As System.Windows.Forms.ToolTipIcon, _
                                     intDuration As Integer, ByVal Rectangle As System.Drawing.Rectangle, ByVal OwnerToolTip As System.Windows.Forms.ToolTip, _
                                     ByVal Owner As System.Windows.Forms.IWin32Window) As Boolean

    Dim intX As Integer = Rectangle.X
    Dim intY As Integer = Rectangle.Y
    With OwnerToolTip
      .ToolTipTitle = strTitle
      .ToolTipIcon = TooltipIcon
      .Show(strDisplayString, Owner, intX, intY, intDuration)
    End With

    Return True
  End Function

  ''' <summary>ShowToolTip</summary>
  ''' <param name="strDisplayString">DisplayString</param>
  ''' <param name="strTitle">DisplayTitle</param>
  ''' <param name="TooltipIcon">TooltipIcon</param>
  ''' <param name="argPosForm">DrawingPoint</param>
  ''' <param name="OwnerToolTip">OwnerToolTip</param>
  ''' <param name="Owner">Owner</param>
  ''' <returns></returns>
  ''' <remarks>Show ToolTipMessage (FormPosition)</remarks>
  ''' 
  Public Shared Function ShowToolTip(ByVal strDisplayString As String, ByVal strTitle As String, ByVal TooltipIcon As System.Windows.Forms.ToolTipIcon, _
                                     ByVal argPosForm As System.Drawing.Point, ByVal OwnerToolTip As System.Windows.Forms.ToolTip, _
                                     ByVal Owner As System.Windows.Forms.IWin32Window) As Boolean

    Dim PosForm As System.Drawing.Point = argPosForm
    Dim intX As Integer = PosForm.X
    Dim intY As Integer = PosForm.Y

    With OwnerToolTip
      .ToolTipTitle = strTitle
      .ToolTipIcon = TooltipIcon
      .Show(strDisplayString, Owner, intX, intY, SLConstants.DURATION)
    End With

    Return True
  End Function

  ''' <summary>ShowToolTip</summary>
  ''' <param name="strDisplayString">DisplayString</param>
  ''' <param name="strTitle">DisplayTitle</param>
  ''' <param name="TooltipIcon">TooltipIcon</param>
  ''' <param name="PcaFunctionBar">PcaFunctionBar</param>
  ''' <param name="OwnerToolTip">OwnerToolTip</param>
  ''' <param name="Owner">Owner</param>
  ''' <returns></returns>
  ''' <remarks>Show ToolTipMessage (PcaFunctionBar)</remarks>
  ''' 
  Public Shared Function ShowToolTip(ByVal strDisplayString As String, ByVal strTitle As String, ByVal TooltipIcon As System.Windows.Forms.ToolTipIcon, _
                                     ByVal PcaFunctionBar As PCA.Controls.PcaFunctionBar, ByVal OwnerToolTip As System.Windows.Forms.ToolTip _
                                     , ByVal Owner As System.Windows.Forms.IWin32Window, ByVal intFunctionKeyID As Integer) As Boolean
    Try
      Dim intX As Integer
      Dim intY As Integer
      Dim intBtnWidth As Integer = CInt(PcaFunctionBar.Width / 12)
      If intFunctionKeyID > 0 Then
        intX = CInt((intBtnWidth * intFunctionKeyID + intBtnWidth * (intFunctionKeyID - 1)) / 2)
        intY = PcaFunctionBar.Location.Y
      End If
      With OwnerToolTip
        .ToolTipTitle = strTitle
        .ToolTipIcon = TooltipIcon
        .Show(strDisplayString, Owner, intX, intY, SLConstants.DURATION)
      End With

    Catch ex As Exception
      Return False
    End Try
    Return True
  End Function

  ''' <summary>ShowToolTip</summary>
  ''' <param name="strDisplayString">DisplayString</param>
  ''' <param name="strTitle">DisplayTitle</param>
  ''' <param name="TooltipIcon">TooltipIcon</param>
  ''' <param name="PcaCodeset">PcaCodeset</param>
  ''' <param name="OwnerToolTip">OwnerToolTip</param>
  ''' <param name="Owner">Owner</param>
  ''' <returns></returns>
  ''' <remarks>Show ToolTipMessage (PcaCodeset)</remarks>
  '''
  Public Shared Function ShowToolTip(ByVal strDisplayString As String, ByVal strTitle As String, ByVal TooltipIcon As System.Windows.Forms.ToolTipIcon _
                                     , ByVal PcaCodeset As PCA.Controls.PcaCodeSet, ByVal OwnerToolTip As System.Windows.Forms.ToolTip _
                                     , ByVal Owner As System.Windows.Forms.IWin32Window) As Boolean

    Try
      Dim intX As Integer
      Dim intY As Integer
      intX = PcaCodeset.Left + PcaCodeset.MaxNameLength + PcaCodeset.MaxCodeLength
      intY = PcaCodeset.Top - PcaCodeset.Height
      With OwnerToolTip
        .ToolTipTitle = strTitle
        .ToolTipIcon = TooltipIcon
        .Show(strDisplayString, Owner, intX, intY, SLConstants.DURATION)
      End With

    Catch ex As Exception
      Return False
    End Try
    Return True
  End Function

  ''' <summary>ShowToolTip</summary>
  ''' <param name="strDisplayString">DisplayString</param>
  ''' <param name="strTitle">DisplayTitle</param>
  ''' <param name="TooltipIcon">TooltipIcon</param>
  ''' <param name="Rectangle">TscMeisaiTable Rectangle</param>
  ''' <param name="OwnerToolTip">OwnerToolTip</param>
  ''' <param name="Owner">Owner</param>
  ''' <returns></returns>
  ''' <remarks>Show ToolTipMessage (TscMeisaiTable.Rectangle)</remarks>
  '''
  Public Shared Function ShowToolTip(ByVal strDisplayString As String, ByVal strTitle As String, ByVal TooltipIcon As System.Windows.Forms.ToolTipIcon _
                                     , ByVal Rectangle As System.Drawing.Rectangle, ByVal OwnerToolTip As System.Windows.Forms.ToolTip _
                                     , ByVal Owner As System.Windows.Forms.IWin32Window) As Boolean

    Dim intX As Integer = Rectangle.X
    Dim intY As Integer = Rectangle.Y
    With OwnerToolTip
      .ToolTipTitle = strTitle
      .ToolTipIcon = TooltipIcon
      .Show(strDisplayString, Owner, intX, intY, SLConstants.DURATION)
    End With

    Return True
  End Function


#End Region

#Region " Output CSV"

  ''' <summary>output the CSV from TscMeisai</summary>
  ''' <param name="strHeaderList">HeaderList：出力列</param>
  ''' <param name="tblTable">TscMeisaiTable：出力対象の明細テーブル</param>
  ''' <param name="strSaveFileName"></param>
  ''' <returns></returns>
  ''' <remarks>PCA標準のTscMeisaiからのCSV出力(全件)</remarks>
  ''' 
  Public Shared Function OutPutCSV(ByVal strHeaderList() As String, ByVal tblTable As PCA.TSC.Kon.Tools.TscMeisaiTable, ByVal strSaveFileName As String) As Boolean

    Dim sbCsv As New StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0
    Dim isSuccess As Boolean = True
    Dim strTargetString As String = String.Empty

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try

      'Header
      With strHeaderList
        For intLpc = 0 To .Length - 1
          If intLpc = 0 Then
            sbCsv.Append(strHeaderList(intLpc))
          ElseIf intLpc = .Length - 1 Then
            sbCsv.AppendLine("," & strHeaderList(intLpc))
          Else
            sbCsv.Append("," & strHeaderList(intLpc))
          End If
        Next

      End With

      intRow = 0

      'Detail

      While intRow < tblTable.DataRowCount

        With strHeaderList
          For intLpc = 0 To .Length - 1
            strTargetString = Replace(tblTable.GetCellValue(intRow, strHeaderList(intLpc)).ToString.Trim, ",", String.Empty)

            If intLpc = 0 Then
              sbCsv.Append(strTargetString)
            ElseIf intLpc = .Length - 1 Then
              sbCsv.AppendLine("," & strTargetString)
            Else
              sbCsv.Append("," & strTargetString)
            End If
          Next

        End With
        intRow += 1

      End While

      '書き込むファイルを開く
      Dim sr As New System.IO.StreamWriter(strSaveFileName, False, enc)
      sr.Write(sbCsv)
      sr.Close()
      isSuccess = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess

  End Function

  ''' <summary>output the CSV from DataGridView</summary>
  ''' <param name="strHeaderList">HeaderList：出力列</param>
  ''' <param name="tblTable">TscMeisaiTable：出力対象の明細テーブル</param>
  ''' <param name="strSaveFileName"></param>
  ''' <returns></returns>
  ''' <remarks>PCA標準のTscMeisaiからのCSV出力(全件)</remarks>
  ''' 
  Public Shared Function OutPutCSVFromDataGridView(ByVal strHeaderList() As String, ByVal tblTable As DataGridView, ByVal strSaveFileName As String, ByVal ResultCount As Integer, Optional ByVal conditionHeader As Integer = -1) As Boolean

    Dim sbCsv As New StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0
    Dim isSuccess As Boolean = True
    Dim strTargetString As String = String.Empty

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try

      'Header
      With strHeaderList
        For intLpc = 0 To .Length - 1
          If intLpc <> conditionHeader Then
            If intLpc = 0 OrElse (intLpc = 1 AndAlso conditionHeader = 0) Then
              sbCsv.Append(strHeaderList(intLpc))
            ElseIf intLpc = .Length - 1 Then
              sbCsv.AppendLine("," & strHeaderList(intLpc))
            Else
              sbCsv.Append("," & strHeaderList(intLpc))
            End If
          End If
        Next

      End With

      intRow = 0

      'Detail

      While intRow < ResultCount

        With strHeaderList
          For intLpc = 0 To .Length - 1
            If intLpc <> conditionHeader AndAlso (conditionHeader = -1 OrElse (Not tblTable.Rows.Item(intRow).Cells(conditionHeader).Value Is Nothing AndAlso tblTable.Rows.Item(intRow).Cells(conditionHeader).Value.ToString = "True")) Then
              If Not tblTable.Rows.Item(intRow).Cells(strHeaderList(intLpc)).Value Is Nothing Then
                strTargetString = Replace(tblTable.Rows.Item(intRow).Cells(strHeaderList(intLpc)).Value.ToString.Trim, ",", String.Empty)
              Else
                strTargetString = " "
              End If

              If intLpc = 0 OrElse (intLpc = 1 AndAlso conditionHeader = 0) Then
                sbCsv.Append(strTargetString)
              ElseIf intLpc = .Length - 1 Then
                sbCsv.AppendLine("," & strTargetString)
              Else
                sbCsv.Append("," & strTargetString)
              End If
            End If
          Next

        End With
        intRow += 1

      End While

      '書き込むファイルを開く
      Dim sr As New System.IO.StreamWriter(strSaveFileName, False, enc)
      sr.Write(sbCsv)
      sr.Close()
      isSuccess = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function

  ''' <summary>output the CSV from TscMeisai</summary>
  ''' <param name="strHeaderList">HeaderList：出力Header</param>
  ''' <param name="strBodyList">strBodyList：出力内容</param>
  ''' <param name="strSaveFileName"></param>
  ''' <returns></returns>
  ''' <remarks>Stringbuliderから吐き出し</remarks>
  ''' 
  Public Shared Function OutPutCSV(ByVal strHeaderList As String, ByVal strBodyList As StringBuilder, ByVal strSaveFileName As String) As Boolean

    Dim sbCsv As New StringBuilder
    Dim isSuccess As Boolean = True
    Dim strTargetString As String = String.Empty

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      Dim sr As New System.IO.StreamWriter(strSaveFileName, False, enc)
      'Header
      If strHeaderList.Trim.Length > 0 Then sbCsv.AppendLine(strHeaderList)
      'Detail
      sbCsv.Append(strBodyList)

      sr.Write(sbCsv)
      sr.Close()
      isSuccess = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess

  End Function

#End Region

  ''' <summary>SlipTypeName [SL_ZDN]</summary>
  ''' <param name="SL_ZDNSlipType"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GetZDNSlipTypeName(ByVal SL_ZDNSlipType As SLConstants.SL_ZDNSlipType) As String

    Select Case SL_ZDNSlipType
      Case SLConstants.SL_ZDNSlipType.Warehousing
        Return "入庫"
      Case SLConstants.SL_ZDNSlipType.Manufacture
        Return "製造"
      Case SLConstants.SL_ZDNSlipType.TransWarehousing
        Return "移動"
      Case Else
        Return String.Empty
    End Select
  End Function

  Public Shared Function Truncate(ByVal decValue As Decimal, ByVal intDecimals As Integer) As Integer
    Try
      Dim dblCoef As Double = System.Math.Pow(10, intDecimals)

      If decValue > 0 Then
        Return CInt(System.Math.Floor(decValue * dblCoef) / dblCoef)
      Else
        Return CInt(System.Math.Ceiling(decValue * dblCoef) / dblCoef)
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return -1
    End Try
  End Function

  Public Shared Function GetTaxRate(ByVal strProductCode As String, ByVal dtWhdate As Integer, ByRef SMSPClass As SMSP, ByRef TaxClass As TAX) As Decimal
    Try
      'Get tax rate initial value
      TaxClass.ReadTaxrateByDate(dtWhdate)
      SMSPClass.ReadByID(strProductCode)
      Dim intRetValue As Integer = -1
      Dim arTax() As Integer = {0, TaxClass.tax_rate1, TaxClass.tax_rate2, TaxClass.tax_rate3, TaxClass.tax_rate4, TaxClass.tax_rate5 _
                               , TaxClass.tax_rate6, TaxClass.tax_rate7, TaxClass.tax_rate8, TaxClass.tax_rate9}

      intRetValue = arTax(SMSPClass.smsp_tax)

      Return intRetValue
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return -1
    End Try
  End Function

  Public Shared Sub SetEditableColumnColor(ByVal dgv As Windows.Forms.DataGridView, ByVal intColIndex As Integer, Color As System.Drawing.Color)

    Try
      If Not Color = Nothing Then
        dgv.Columns(intColIndex).DefaultCellStyle.BackColor = Color
        dgv.Columns(intColIndex).DefaultCellStyle.SelectionBackColor = Color
      Else
        dgv.Columns(intColIndex).DefaultCellStyle.BackColor = Drawing.Color.LightYellow
        dgv.Columns(intColIndex).DefaultCellStyle.SelectionBackColor = Drawing.Color.LightYellow
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Public Shared Sub SetEditableCellColor(ByVal dgv As Windows.Forms.DataGridView, ByVal intColIndex As Integer, ByVal intRowIndex As Integer, Color As System.Drawing.Color)
    Try
      If Not Color = Nothing Then
        dgv(intColIndex, intRowIndex).Style.BackColor = Color
        dgv(intColIndex, intRowIndex).Style.SelectionBackColor = Color
      Else
        dgv(intColIndex, intRowIndex).Style.BackColor = Drawing.Color.LightYellow
        dgv(intColIndex, intRowIndex).Style.SelectionBackColor = Drawing.Color.LightYellow

      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Setting DetailType To PcaCell
  ''' </summary>
  ''' <param name="cell"></param>
  ''' <param name="ComboType"></param>
  ''' <remarks>Setting DetailType To PcaCell</remarks>
  Public Shared Sub SetPcaCellCombo(ByRef cell As PCA.Controls.PcaCell, ByVal ComboType As SLConstants.ComboType)
    Try
      Select Case ComboType
        Case SLConstants.ComboType.WareHouse
          cell.SelectItems = {SLConstants.WarehouseKbn.WAREHOUSE_ALL, SLConstants.WarehouseKbn.WAREHOUSE_APART}
        Case SLConstants.ComboType.Shipping
          cell.SelectItems = {SLConstants.ShipmentKbn.SHIPPING_ALL, SLConstants.ShipmentKbn.SHIPPING_APART}
      End Select
      cell.CellStyle.Alignment = Drawing.StringAlignment.Center
      cell.Enabled = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#Region " Convert"

  ''' <summary>
  ''' Convert integer(date) to String(yyyy/mm/dd)
  ''' </summary>
  ''' <param name="pDate"></param>
  ''' <returns></returns>
  ''' <remarks>integer(date) -> string(yyyy/mm/dd)</remarks>
  Public Shared Function ConvertIntToDatestring(ByVal pDate As String) As String
    Dim sRetStr As String = pDate

    Try
      If sRetStr.Trim = Str(0).Trim Then Return String.Empty
      If sRetStr.Trim.Length <> 8 Then Return pDate

      sRetStr = Format(CInt(sRetStr), "0000/00/00")

      Return sRetStr
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return pDate
    End Try

  End Function

  ''' <summary>
  ''' Convert String(yyyy/mm/dd) to integer(date) 
  ''' </summary>
  ''' <param name="pDate"></param>
  ''' <returns></returns>
  ''' <remarks>string(yyyy/mm/dd) -> integer(date) </remarks>
  Public Shared Function ConvertDatestringToInteger(ByVal pDate As String) As Integer
    Dim intRet As Integer = 0

    Try
      Return CInt(Replace(pDate, "/", String.Empty))
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return intRet
    End Try

  End Function

#End Region

  Public Shared Sub SetCodesetInitialize(ByRef argCodeset As PCA.Controls.PcaCodeSet, ByVal intCodeTextLength As Integer, Optional ByVal intCodesetWidth As Integer = 0)

    Try
      argCodeset.LimitLength = intCodeTextLength
      argCodeset.MaxCodeLength = intCodeTextLength + 1
      If intCodesetWidth > 0 Then argCodeset.MaxNameLength = intCodesetWidth - intCodeTextLength - 2
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  Public Shared Sub DeleteRow(ByRef tblResult As TscMeisaiTable, ByRef intRowIndex As Integer, ByRef intRowCount As Integer, strColumnName As String)
    Try
      tblResult.RemoveRow(intRowIndex)
      intRowCount -= 1
      tblResult.DataRowCount += 1
      SetBranchNumber(tblResult, strColumnName)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Public Shared Sub InsertRow(ByRef tblResult As TscMeisaiTable, ByRef intRowIndex As Integer, ByRef intRowCount As Integer, ByVal strColumnName As String, ByVal strProductColName As String _
                              , ByVal intProductColIdx As Integer, ByVal intProductCellIdx As Integer)
    Try
      If intRowCount = 0 OrElse intRowIndex = intRowCount Then Return
      tblResult.BeginInsertRow(intRowIndex)
      tblResult.CommitInsertRow()
      intRowCount += 1
      tblResult.SetCellFocus(intRowIndex, strProductColName)
      tblResult.BodyColumns(intProductColIdx).Cells(intProductCellIdx).EditMode = True
      tblResult.RemoveRow(tblResult.DataRowCount - 1)
      SetBranchNumber(tblResult, strColumnName)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  Public Shared Sub SetBranchNumber(tblResult As TscMeisaiTable, BranchColumnName As String)
    Try
      For intCount As Integer = 0 To tblResult.DataRowCount - 1
        tblResult.SetCellValue(intCount, BranchColumnName, intCount + 1)
      Next
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  Public Shared Function GetColsName(ByVal strTableName As String, ByVal isForDisplay As Boolean, Optional ByVal RowData As SL_GDOD() = Nothing) As String

    Try
      Const TABLENAME_SL_LMB As String = "GET_SL_LMB"
      Const TABLENAME_SL_SMS As String = "GET_SL_SMS"

      Const TABLENAME_SL_LMD1 As String = "GET_SL_LMD1"
      Const TABLENAME_SL_LMD2 As String = "GET_SL_LMD2"
      Const TABLENAME_SL_LMD3 As String = "GET_SL_LMD3"
      Const TABLENAME_SL_LMD4 As String = "GET_SL_LMD4"
      Const TABLENAME_SL_LMD5 As String = "GET_SL_LMD5"
      Const TABLENAME_SL_LMD6 As String = "GET_SL_LMD6"
      Const TABLENAME_SL_LMD7 As String = "GET_SL_LMD7"
      Const TABLENAME_SL_LMD8 As String = "GET_SL_LMD8"
      Const TABLENAME_SL_LMD9 As String = "GET_SL_LMD9"
      Const TABLENAME_SL_LMD10 As String = "GET_SL_LMD10"

      Const TABLENAME_SL_NYK As String = "GET_SL_NYK"
      Const TABLENAME_SL_SYK As String = "GET_SL_SYK"
      Const TABLENAME_SL_ZADJ As String = "GET_SL_ZADJ"
      Const TABLENAME_SL_WHT As String = "GET_SL_WHT"

      Dim sb As New StringBuilder
      If RowData Is Nothing Then
        Select Case strTableName
          Case TABLENAME_SL_LMB
            If isForDisplay Then
              sb.Append("出荷期限利用,")
              sb.Append("使用期限利用,")
              sb.Append("賞味期限利用,")
              sb.Append("消費期限利用,")

              sb.Append("出荷期限引当区分,")
              sb.Append("使用期限引当区分,")
              sb.Append("賞味期限引当区分,")
              sb.Append("消費期限引当区分,")

              sb.Append("最大ロット詳細情報数,")
              sb.Append("ロット自動引当区分,")
              sb.Append("ロット自動引当順序,")
              sb.Append("ロット番号自動割振,")
              sb.Append("ロット番号フォーマット,")
              sb.Append("ロット番号入力方法,")

              sb.Append("ロット詳細ラベル名1,")
              sb.Append("ロット詳細ラベル名2,")
              sb.Append("ロット詳細ラベル名3,")
              sb.Append("ロット詳細ラベル名4,")
              sb.Append("ロット詳細ラベル名5,")
              sb.Append("ロット詳細ラベル名6,")
              sb.Append("ロット詳細ラベル名7,")
              sb.Append("ロット詳細ラベル名8,")
              sb.Append("ロット詳細ラベル名9,")
              sb.Append("ロット詳細ラベル名10,")
              sb.Append("月次在庫締切年月,")
              sb.Append("在庫振替出荷先コード")

            Else
              sb.Append("sl_lmb_sdlflg,")
              sb.Append("sl_lmb_ubdflg,")
              sb.Append("sl_lmb_bbdflg,")
              sb.Append("sl_lmb_expflg,")

              sb.Append("sl_lmb_sdlhikiate,")
              sb.Append("sl_lmb_udbhikiate,")
              sb.Append("sl_lmb_bbdhikiate,")
              sb.Append("sl_lmb_exphikiate,")

              sb.Append("sl_lmb_maxlotop,")
              sb.Append("sl_lmb_autohikiate,")
              sb.Append("sl_lmb_hikiatesort,")
              sb.Append("sl_lmb_autolot,")
              sb.Append("sl_lmb_lotfmt,")
              sb.Append("sl_lmb_lotime,")

              sb.Append("sl_lmb_label1,")
              sb.Append("sl_lmb_label2,")
              sb.Append("sl_lmb_label3,")
              sb.Append("sl_lmb_label4,")
              sb.Append("sl_lmb_label5,")
              sb.Append("sl_lmb_label6,")
              sb.Append("sl_lmb_label7,")
              sb.Append("sl_lmb_label8,")
              sb.Append("sl_lmb_label9,")
              sb.Append("sl_lmb_label10,")
              sb.Append("sl_lmb_zcutoff_date,")
              sb.Append("sl_lmb_tcd")

            End If
          Case TABLENAME_SL_SMS
            If isForDisplay Then
              sb.Append("商品コード,")
              sb.Append("ロット管理フラグ,")

              sb.Append("賞味期限月初期値,")
              sb.Append("賞味期限日初期値,")
              sb.Append("出荷期限月初期値,")
              sb.Append("出荷期限日初期値,")

              sb.Append("使用期限月初期値,")
              sb.Append("使用期限日初期値,")
              sb.Append("消費期限月初期値,")
              sb.Append("消費期限日初期値,")

              sb.Append("GW率")
            Else
              sb.Append("sl_sms_scd,")
              sb.Append("sl_sms_lkbn,")

              sb.Append("sl_sms_bbmini,")
              sb.Append("sl_sms_bbdini,")
              sb.Append("sl_sms_sdlmini,")
              sb.Append("sl_sms_sdldini,")

              sb.Append("sl_sms_ubmini,")
              sb.Append("sl_sms_ubdini,")
              sb.Append("sl_sms_expmini,")
              sb.Append("sl_sms_expdini,")

              sb.Append("sl_sms_gw")
            End If

          Case TABLENAME_SL_LMD1, TABLENAME_SL_LMD2, TABLENAME_SL_LMD3, TABLENAME_SL_LMD4, TABLENAME_SL_LMD5, _
               TABLENAME_SL_LMD6, TABLENAME_SL_LMD7, TABLENAME_SL_LMD8, TABLENAME_SL_LMD9, TABLENAME_SL_LMD10
            If isForDisplay Then
              sb.Append("ロット詳細コード,")
              sb.Append("ロット詳細名")
            Else
              sb.Append("sl_lmd_ldcd,")
              sb.Append("sl_lmd_mei")
            End If

          Case TABLENAME_SL_NYK
            If isForDisplay Then
              sb.Append("入荷No,")
              sb.Append("返品区分,")
              sb.Append("入荷日,")
              sb.Append("仕入先コード,")
              sb.Append("担当者コード,")
              sb.Append("摘要コード,")
              sb.Append("摘要名,")
              sb.Append("発注No,")
              sb.Append("仕入No,")
              sb.Append("入荷No2,")
              sb.Append("部門コード,")
              sb.Append("プロジェクトコード,")
              sb.Append("テーブル区分,")
              sb.Append("枝番,")
              sb.Append("納品区分,")
              sb.Append("商品コード,")
              sb.Append("商品名,")
              sb.Append("規格・型番,")
              sb.Append("色,")
              sb.Append("サイズ,")
              sb.Append("倉庫コード,")
              sb.Append("税区分,")
              sb.Append("税込区分,")
              sb.Append("入数,")
              sb.Append("箱数,")
              sb.Append("数量,")
              sb.Append("本数,")
              sb.Append("単位,")
              sb.Append("単価,")
              sb.Append("金額,")
              sb.Append("備考,")
              sb.Append("税率,")
              sb.Append("システムロット番号,")
              sb.Append("ユーザロット番号,")
              sb.Append("相手先ロット番号,")
              sb.Append("管理番号,")
              sb.Append("出荷期限,")
              sb.Append("使用期限,")
              sb.Append("賞味期限,")
              sb.Append("消費期限,")
              sb.Append("ロット詳細ID1,")
              sb.Append("ロット詳細名1,")
              sb.Append("ロット詳細ID2,")
              sb.Append("ロット詳細名2,")
              sb.Append("ロット詳細ID3,")
              sb.Append("ロット詳細名3,")
              sb.Append("ロット詳細ID4,")
              sb.Append("ロット詳細名4,")
              sb.Append("ロット詳細ID5,")
              sb.Append("ロット詳細名5,")
              sb.Append("ロット詳細ID6,")
              sb.Append("ロット詳細名6,")
              sb.Append("ロット詳細ID7,")
              sb.Append("ロット詳細名7,")
              sb.Append("ロット詳細ID8,")
              sb.Append("ロット詳細名8,")
              sb.Append("ロット詳細ID9,")
              sb.Append("ロット詳細名9,")
              sb.Append("ロット詳細ID10,")
              sb.Append("ロット詳細名10,")
              sb.Append("入荷変換区分,")
              sb.Append("登録ユーザー,")
              sb.Append("登録日時,")
              sb.Append("更新ユーザー,")
              sb.Append("更新日時")
            Else
              sb.Append("sl_nykh_denno,")
              sb.Append("sl_nykh_return,")
              sb.Append("sl_nykh_uribi,")
              sb.Append("sl_nykh_tcd,")
              sb.Append("sl_nykh_jtan,")
              sb.Append("sl_nykh_tekcd,")
              sb.Append("sl_nykh_tekmei,")
              sb.Append("sl_nykh_hno,")
              sb.Append("sl_nykh_sno,")
              sb.Append("sl_nykh_denno2,")
              sb.Append("sl_nykh_jbmn,")
              sb.Append("sl_nykh_pjcode,")
              sb.Append("sl_nykh_datakbn,")
              sb.Append("sl_nykd_eda,")
              sb.Append("sl_nykd_nkbn,")
              sb.Append("sl_nykd_scd,")
              sb.Append("sl_nykd_mei,")
              sb.Append("sl_nykd_kikaku,")
              sb.Append("sl_nykd_color,")
              sb.Append("sl_nykd_size,")
              sb.Append("sl_nykd_souko,")
              sb.Append("sl_nykd_tax,")
              sb.Append("sl_nykd_komi,")
              sb.Append("sl_nykd_iri,")
              sb.Append("sl_nykd_hako,")
              sb.Append("sl_nykd_suryo,")
              sb.Append("sl_nykd_honsu,")
              sb.Append("sl_nykd_tani,")
              sb.Append("sl_nykd_tanka,")
              sb.Append("sl_nykd_kingaku,")
              sb.Append("sl_nykd_biko,")
              sb.Append("sl_nykd_rate,")
              sb.Append("sl_nykd_lotno,")
              sb.Append("sl_nykd_ulotno,")
              sb.Append("sl_nykd_alotno,")
              sb.Append("sl_nykd_kanrino,")
              sb.Append("sl_nykd_sdldate,")
              sb.Append("sl_nykd_ubdate,")
              sb.Append("sl_nykd_bbdate,")
              sb.Append("sl_nykd_expdate,")
              sb.Append("sl_nykd_ldid1,")
              sb.Append("sl_nykd_ldmei1,")
              sb.Append("sl_nykd_ldid2,")
              sb.Append("sl_nykd_ldmei2,")
              sb.Append("sl_nykd_ldid3,")
              sb.Append("sl_nykd_ldmei3,")
              sb.Append("sl_nykd_ldid4,")
              sb.Append("sl_nykd_ldmei4,")
              sb.Append("sl_nykd_ldid5,")
              sb.Append("sl_nykd_ldmei5,")
              sb.Append("sl_nykd_ldid6,")
              sb.Append("sl_nykd_ldmei6,")
              sb.Append("sl_nykd_ldid7,")
              sb.Append("sl_nykd_ldmei7,")
              sb.Append("sl_nykd_ldid8,")
              sb.Append("sl_nykd_ldmei8,")
              sb.Append("sl_nykd_ldid9,")
              sb.Append("sl_nykd_ldmei9,")
              sb.Append("sl_nykd_ldid10,")
              sb.Append("sl_nykd_ldmei10,")
              sb.Append("sl_nykd_convert,")
              sb.Append("sl_nykh_insuser,")
              sb.Append("sl_nykh_insdate,")
              sb.Append("sl_nykh_upduser,")
              sb.Append("sl_nykh_upddate")
            End If

          Case TABLENAME_SL_SYK
            If isForDisplay Then
              sb.Append("出荷No,")
              sb.Append("返品区分,")
              sb.Append("出荷日,")
              sb.Append("納期,")
              sb.Append("出荷先コード,")
              sb.Append("運送会社コード,")
              sb.Append("担当者コード,")
              sb.Append("摘要コード,")
              sb.Append("摘要名,")
              sb.Append("出荷No2,")
              sb.Append("プロジェクトコード,")
              sb.Append("部門コード,")
              sb.Append("テーブル区分,")
              sb.Append("枝番,")
              sb.Append("出荷指示,")
              sb.Append("商品コード,")
              sb.Append("商品名,")
              sb.Append("規格・型番,")
              sb.Append("色,")
              sb.Append("サイズ,")
              sb.Append("倉庫コード,")
              sb.Append("税区分,")
              sb.Append("税込区分,")
              sb.Append("入数,")
              sb.Append("箱数,")
              sb.Append("数量,")
              sb.Append("本数,")
              sb.Append("単位,")
              sb.Append("単価,")
              sb.Append("売上金額,")
              sb.Append("原単価,")
              sb.Append("原価金額,")
              sb.Append("粗利益,")
              sb.Append("備考,")
              sb.Append("税率,")
              sb.Append("出荷変換区分,")
              sb.Append("登録ユーザー,")
              sb.Append("登録日時,")
              sb.Append("更新ユーザー,")
              sb.Append("更新日時")
            Else
              sb.Append("sl_sykh_denno,")
              sb.Append("sl_sykh_return,")
              sb.Append("sl_sykh_uribi,")
              sb.Append("sl_sykh_nouki,")
              sb.Append("sl_sykh_tcd,")
              sb.Append("sl_sykh_carrier,")
              sb.Append("sl_sykh_jtan,")
              sb.Append("sl_sykh_tekcd,")
              sb.Append("sl_sykh_tekmei,")
              sb.Append("sl_sykh_denno2,")
              sb.Append("sl_sykh_pjcode,")
              sb.Append("sl_sykh_jbmn,")
              sb.Append("sl_sykh_datakbn,")
              sb.Append("sl_sykd_eda,")
              sb.Append("sl_sykd_skbn,")
              sb.Append("sl_sykd_scd,")
              sb.Append("sl_sykd_mei,")
              sb.Append("sl_sykd_kikaku,")
              sb.Append("sl_sykd_color,")
              sb.Append("sl_sykd_size,")
              sb.Append("sl_sykd_souko,")
              sb.Append("sl_sykd_tax,")
              sb.Append("sl_sykd_komi,")
              sb.Append("sl_sykd_iri,")
              sb.Append("sl_sykd_hako,")
              sb.Append("sl_sykd_suryo,")
              sb.Append("sl_sykd_honsu,")
              sb.Append("sl_sykd_tani,")
              sb.Append("sl_sykd_tanka,")
              sb.Append("sl_sykd_kingaku,")
              sb.Append("sl_sykd_gentan,")
              sb.Append("sl_sykd_genka,")
              sb.Append("sl_sykd_arari,")
              sb.Append("sl_sykd_biko,")
              sb.Append("sl_sykd_rate,")
              sb.Append("sl_sykd_convert,")
              sb.Append("sl_sykh_insuser,")
              sb.Append("sl_sykh_insdate,")
              sb.Append("sl_sykh_upduser,")
              sb.Append("sl_sykh_upddate")
            End If

          Case TABLENAME_SL_ZADJ
            If isForDisplay Then
              sb.Append("在庫調整No,")
              sb.Append("在庫調整No2,")
              sb.Append("調整日,")
              sb.Append("商品コード,")
              sb.Append("倉庫コード,")
              sb.Append("入出庫区分,")
              sb.Append("数量,")
              sb.Append("本数,")
              sb.Append("摘要コード,")
              sb.Append("摘要名,")
              sb.Append("登録ユーザー,")
              sb.Append("登録日時,")
              sb.Append("更新ユーザー,")
              sb.Append("更新日時")
            Else
              sb.Append("sl_zadj_denno,")
              sb.Append("sl_zadj_denno2,")
              sb.Append("sl_zadj_date,")
              sb.Append("sl_zadj_scd,")
              sb.Append("sl_zadj_souko,")
              sb.Append("sl_zadj_iokbn,")
              sb.Append("sl_zadj_suryo,")
              sb.Append("sl_zadj_honsu,")
              sb.Append("sl_zadj_tekcd,")
              sb.Append("sl_zadj_tekmei,")
              sb.Append("sl_zadj_insuser,")
              sb.Append("sl_zadj_insdate,")
              sb.Append("sl_zadj_upduser,")
              sb.Append("sl_zadj_upddate")
            End If
          Case TABLENAME_SL_WHT
            If isForDisplay Then
              sb.Append("移動No,")
              sb.Append("移動No2,")
              sb.Append("移動日,")
              sb.Append("着日,")
              sb.Append("商品コード,")
              sb.Append("移動元倉庫コード,")
              sb.Append("移動先倉庫コード,")
              sb.Append("数量,")
              sb.Append("本数,")
              sb.Append("摘要コード,")
              sb.Append("摘要名,")
              sb.Append("登録ユーザー,")
              sb.Append("登録日時,")
              sb.Append("更新ユーザー,")
              sb.Append("更新日時")
            Else
              sb.Append("sl_wht_denno,")
              sb.Append("sl_wht_denno2,")
              sb.Append("sl_wht_date,")
              sb.Append("sl_wht_dlvdate,")
              sb.Append("sl_wht_scd,")
              sb.Append("sl_wht_soukof,")
              sb.Append("sl_wht_soukot,")
              sb.Append("sl_wht_suryo,")
              sb.Append("sl_wht_honsu,")
              sb.Append("sl_wht_tekcd,")
              sb.Append("sl_wht_tekmei,")
              sb.Append("sl_wht_insuser,")
              sb.Append("sl_wht_insdate,")
              sb.Append("sl_wht_upduser,")
              sb.Append("sl_wht_upddate")
            End If

        End Select
        Return sb.ToString
      Else
        Dim aryTargetString() As String = GetColsName(strTableName, False).Split(","c)
        Dim aryDisplayString() As String = GetColsName(strTableName, True).Split(","c)
        Dim sbSortedString As New StringBuilder
        Dim strSortedString As String
        Dim strTargetCol As String = String.Empty
        Dim intTarget As Integer = 0

        For intLpc As Integer = 0 To RowData.Count - 1
          strTargetCol = RowData(intLpc).sl_gdod_item_name
          intTarget = Array.IndexOf(aryTargetString, strTargetCol)

          sbSortedString.Append(aryDisplayString(intTarget) & ",")

        Next
        strSortedString = Strings.Left(sbSortedString.ToString, sbSortedString.ToString.Length - 1)
        Return strSortedString
      End If

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return String.Empty
    End Try

  End Function

  Public Shared Function formatNumberOfDigit(ByVal number As Object, Optional ByVal numberOfDigit As Integer = 0) As String
    Dim decVar As Decimal = 0
    Decimal.TryParse(number.ToString, decVar)
    Dim result As String
    If numberOfDigit = 0 Then
      result = decVar.ToString("N0", System.Globalization.CultureInfo.CreateSpecificCulture("ja-JP")) 'CInt(var).ToString
    ElseIf numberOfDigit = 1 Then
      result = decVar.ToString("N1", System.Globalization.CultureInfo.CreateSpecificCulture("ja-JP")) 'String.Format("{0:0,0.0}", var)
    ElseIf numberOfDigit = 2 Then
      result = decVar.ToString("N2", System.Globalization.CultureInfo.CreateSpecificCulture("ja-JP")) 'String.Format("{0:0,0.00}", var)
    ElseIf numberOfDigit = 3 Then
      result = decVar.ToString("N3", System.Globalization.CultureInfo.CreateSpecificCulture("ja-JP")) 'String.Format("{0:0,0.000}", var)
    Else
      result = decVar.ToString("N4", System.Globalization.CultureInfo.CreateSpecificCulture("ja-JP")) 'String.Format("{0:0,0.0000}", var)
    End If
    Return result
  End Function

  ''' <summary>
  ''' Setting Format for comma
  ''' </summary>
  ''' <param name="intDegit"></param>
  ''' <returns>formatstring</returns>
  ''' <remarks></remarks>
  Public Shared Function FormatComma(ByVal intDegit As Integer) As String
    Dim strRet As String = String.Empty
    Try
      strRet = "#,##0." & New String("0"c, intDegit)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return strRet
  End Function

  Public Shared Function GetUnitPrice(ByVal SMSPClass As SMSP, ByVal TMSClass As TMS) As Decimal
    Try
      Dim decUnitPrice As Decimal = SMSPClass.smsp_gen

      Select Case TMSClass.tms_bno
        Case 0
          '標準価格
          decUnitPrice = SMSPClass.smsp_hyo
        Case 1
          '売価1
          decUnitPrice = SMSPClass.smsp_bai1
        Case 2
          '売価2
          decUnitPrice = SMSPClass.smsp_bai2
        Case 3
          '売価3
          decUnitPrice = SMSPClass.smsp_bai3
        Case 4
          '売価4
          decUnitPrice = SMSPClass.smsp_bai4
        Case 5
          '売価5
          decUnitPrice = SMSPClass.smsp_bai5
        Case 6
          '原価
          decUnitPrice = SMSPClass.smsp_gen
      End Select

      Return KonCalc.RoundOff(decUnitPrice * TMSClass.tms_kake / 1000D, 0)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return SMSPClass.smsp_gen
    End Try
  End Function

  Public Shared Function GetOriginalUnitPrice(ByVal SMSPClass As SMSP, ByVal TMSClass As TMS) As Decimal
    Try
      Dim decUnitPrice As Decimal = SMSPClass.smsp_gen

      Return KonCalc.RoundOff(decUnitPrice * TMSClass.tms_kake / 1000D, 0)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return SMSPClass.smsp_gen
    End Try
  End Function

  Public Shared Function NewLineManager(ByRef myString As String, ByVal lineLength As Integer) As String
    Try
      Dim newString As String = ""
      Dim lineLengthCount As Integer = 0 ' number of character on current line
      For intCount As Integer = 0 To myString.Length - 1
        If newString.Length > 0 AndAlso lineLengthCount + Encoding.GetEncoding("Shift_JIS").GetByteCount(myString.Substring(intCount, 1)) > lineLength Then
          newString = newString & Environment.NewLine & myString.Substring(intCount, 1)
          lineLengthCount = Encoding.GetEncoding("Shift_JIS").GetByteCount(myString.Substring(intCount, 1))
        Else
          newString = newString & myString.Substring(intCount, 1)
          lineLengthCount += Encoding.GetEncoding("Shift_JIS").GetByteCount(myString.Substring(intCount, 1))
        End If
      Next
      Return newString
    Catch ex As Exception
      Return Nothing
    End Try
  End Function
End Class

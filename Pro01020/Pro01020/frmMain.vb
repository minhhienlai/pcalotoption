﻿Imports System.Windows.Forms
Imports PCA.Controls
Imports PCA.ApplicationIntegration
Imports PCA.Kon.Integration
Imports Sunloft.PCAIF
Imports Sunloft.Common
Imports Sunloft.Message

Public Class frmMain
  Private m_intProductCodeWidth As Single = 10
  Private Const WIDTH_PRODUCT_NAME As Single = 3
  Private Const WIDTH_IS_USE_LOT As Single = 1
  Private Const WIDTH_BBMINI As Single = 1
  Private Const WIDTH_BBDINI As Single = 1
  Private Const WIDTH_SDLMINI As Single = 1
  Private Const WIDTH_SDLDINI As Single = 1
  Private Const WIDTH_UBMINI As Single = 1
  Private Const WIDTH_UBDINI As Single = 1
  Private Const WIDTH_EXPMINI As Single = 1
  Private Const WIDTH_EXPDINI As Single = 1
  Private Const WIDTH_GW As Single = 1

  Private Const MONTH_LIMIT As Integer = 3
  Private Const DAY_LIMIT As Integer = 4
  Private Const GW_LIMIT As Integer = 2

  Private Const CELL_LIMIT_LENGTH As Integer = 100
  Private Const COL_LOTMANAGE_START As Integer = 3  'ロット管理する/しないで編集制御する最初の列
  Private Const GW_DISPLAY = 1 ' If GW is display or not
  Private Const LOT_USE = 1

  Private Const CODE_COLUMN As Integer = 0
  Private Const NAME_COLUMN As Integer = 1

  Private Const PG_NAME As String = "商品オプションマスタ"

  Private connector As IIntegratedApplication = Nothing
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private m_conf As Sunloft.PcaConfig

  Private m_arFlag(7) As Integer  '0, 1 - 賞味、2, 3 - 出荷、4, 5 - 使用、6,7 - 消費
  Private m_result As ColumnResult()

  ''Set list of Integer (Clear())
  Private m_strCellToValidate() As String = {ColumnName.UbMIni, ColumnName.UbDIni, ColumnName.BbMIni, ColumnName.BbDIni, ColumnName.ExpMIni, ColumnName.ExpDIni, _
                                             ColumnName.SdlMIni, ColumnName.SdlDIni, ColumnName.GWRate}

  Private rowCountOld As Integer
  Private columnCount As Integer = 0
  Private F12CloseFlag As Boolean = True 'If F12 is Close or Register
  Private m_RowIndex As Integer = -1

#Region "Initiate"

  Public Sub New()
    ' この呼び出しはデザイナーで必要です。
    InitializeComponent()

    m_appClass.ConnectToPCA()
    connector = m_appClass.connector

    Dim AMS1Class = New AMS1(connector)
    Dim SL_LMBClass = New SL_LMB(connector)
    AMS1Class.ReadAll()
    SL_LMBClass.ReadOnlyRow()

    m_conf = New Sunloft.PcaConfig

    m_intProductCodeWidth = Math.Max(CSng(AMS1Class.ams1_ProductLength * 1.2), m_intProductCodeWidth)

    InitTable(SL_LMBClass)

    InitComboBox()

  End Sub

  Private Sub InitComboBox()
    cbBox.Items.Clear()
    cbBox.Items.Add("0:一般商品")
    cbBox.Items.Add("1:雑商品")
    cbBox.Items.Add("2:諸雑費")
    cbBox.Items.Add("3:値引")
    cbBox.Items.Add("4:記事 ")
    cbBox.SelectedIndex = 0
  End Sub

  Private Sub InitTable(ByVal SL_LMBClass As SL_LMB)
    Try
      GetFlagArray(SL_LMBClass)
      With tblResult
        .DataRowCount = 99

        '入力用青枠の表示
        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader()     'ヘッダー部
        InitTableBody()       'ボディ部

        .UpdateTable()
        '.UpDownKeyAsTab = True

        .BodyColumns.DefaultStyle.BackColor = Color.White
        .BodyColumns.DefaultAltStyle.BackColor = Color.White
        .BodyColumns(CODE_COLUMN).Style = New PcaColumnStyle(Color.Black, Color.LightYellow)
        .BodyColumns(CODE_COLUMN).AltStyle = New PcaColumnStyle(Color.Black, Color.LightYellow)
        .BodyColumns(NAME_COLUMN).Style = New PcaColumnStyle(Color.Black, Color.LightYellow)
        .BodyColumns(NAME_COLUMN).AltStyle = New PcaColumnStyle(Color.Black, Color.LightYellow)
      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  Private Sub InitTableHeader()
    tblResult.HeadTextHeight = 1
    tblResult.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
    tblResult.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

    InitHeaderColumn(ColumnName.ProductCode, m_intProductCodeWidth, , False)
    InitHeaderColumn(ColumnName.ProductName, WIDTH_PRODUCT_NAME)
    InitHeaderColumn(ColumnName.IsUseLot, WIDTH_IS_USE_LOT)
    InitHeaderColumn(ColumnName.BbMIni, 1, m_arFlag(0))
    InitHeaderColumn(ColumnName.BbDIni, 1, m_arFlag(1))
    InitHeaderColumn(ColumnName.SdlMIni, 1, m_arFlag(2))
    InitHeaderColumn(ColumnName.SdlDIni, 1, m_arFlag(3))
    InitHeaderColumn(ColumnName.UbMIni, 1, m_arFlag(4))
    InitHeaderColumn(ColumnName.UbDIni, 1, m_arFlag(5))
    InitHeaderColumn(ColumnName.ExpMIni, 1, m_arFlag(6))
    InitHeaderColumn(ColumnName.ExpDIni, 1, m_arFlag(7))
    InitHeaderColumn(ColumnName.GWRate, 1)
    InitHeaderColumn(ColumnName.IsEdited, 0)
  End Sub

  Private Sub InitHeaderColumn(cellname As String, size As Single, Optional isDisplayed As Integer = 1, Optional isResize As Boolean = True, Optional alignment As System.Drawing.StringAlignment = StringAlignment.Center)
    Dim column As PcaColumn
    Dim cell As PcaCell
    If isDisplayed > 0 Then
      column = New PcaColumn(cellname, size)
    Else
      column = New PcaColumn(cellname, 0)
    End If

    column.CanResize = isResize
    cell = tblResult.PrepareFixTextCell(cellname)
    cell.CellStyle.Alignment = alignment
    column.Cells.Add(cell)
    tblResult.HeadColumns.Add(column)
  End Sub

  Private Sub InitTableBody()

    InitBodyColumn(ColumnName.ProductCode, CellType.Text, m_intProductCodeWidth, False, , StringAlignment.Near, , , False)
    InitBodyColumn(ColumnName.ProductName, CellType.Text, WIDTH_PRODUCT_NAME, False, , StringAlignment.Near)
    InitBodyColumn(ColumnName.IsUseLot, PCA.Controls.CellType.ItemSelect, WIDTH_IS_USE_LOT, True, , StringAlignment.Near)

    InitBodyColumn(ColumnName.bbmini, CellType.Number, WIDTH_BBMINI, True, MONTH_LIMIT, , m_arFlag(0))
    InitBodyColumn(ColumnName.bbdini, CellType.Number, WIDTH_BBDINI, True, DAY_LIMIT, , m_arFlag(1))
    InitBodyColumn(ColumnName.sdlmini, CellType.Number, WIDTH_SDLMINI, True, MONTH_LIMIT, , m_arFlag(2))
    InitBodyColumn(ColumnName.sdldini, CellType.Number, WIDTH_SDLDINI, True, DAY_LIMIT, , m_arFlag(3))
    InitBodyColumn(ColumnName.ubmini, CellType.Number, WIDTH_UBMINI, True, MONTH_LIMIT, , m_arFlag(4))
    InitBodyColumn(ColumnName.ubdini, CellType.Number, WIDTH_UBDINI, True, DAY_LIMIT, , m_arFlag(5))
    InitBodyColumn(ColumnName.expmini, CellType.Number, WIDTH_EXPMINI, True, MONTH_LIMIT, , m_arFlag(6))
    InitBodyColumn(ColumnName.expdini, CellType.Number, WIDTH_EXPDINI, True, DAY_LIMIT, , m_arFlag(7))
    InitBodyColumn(ColumnName.GWRate, CellType.Number, WIDTH_GW, True, GW_LIMIT, , 1)
    InitBodyColumn(ColumnName.IsEdited, CellType.Boolean, WIDTH_GW, True, GW_LIMIT, , 0)

  End Sub

  Private Sub InitBodyColumn(strCellName As String, CellType As CellType, cLength As Single, Optional isEditable As Boolean = True, Optional lengthLimit As Short = CELL_LIMIT_LENGTH, _
                             Optional ByVal Alignment As StringAlignment = StringAlignment.Far, Optional isDisplayed As Integer = 1, Optional srtDecimalDigit As Short = 0, _
                             Optional isResize As Boolean = True)
    Dim column As PcaColumn
    Dim cell As PcaCell

    If isDisplayed > 0 Then
      column = New PcaColumn(strCellName, cLength)
    Else
      column = New PcaColumn(strCellName, 0)
    End If

    column.CanResize = isResize

    cell = New PcaCell(strCellName, CellType)
    cell.EditMode = isEditable
    cell.Enabled = isEditable
    cell.CellStyle.Alignment = Alignment
    cell.LimitLength = lengthLimit
    cell.DecimalDigit = srtDecimalDigit

    If CellType = PCA.Controls.CellType.ItemSelect Then cell.SelectItems = {"しない", "する"}
    column.Cells.Add(cell)
    tblResult.BodyColumns.Add(column)

  End Sub
#End Region

#Region "Private method"

  Private Sub GetFlagArray(ByVal SL_LMBClass As SL_LMB)
    Try

      m_arFlag(0) = SL_LMBClass.sl_lmb_bbdflg
      m_arFlag(1) = SL_LMBClass.sl_lmb_bbdflg
      m_arFlag(2) = SL_LMBClass.sl_lmb_sdlflg
      m_arFlag(3) = SL_LMBClass.sl_lmb_sdlflg
      m_arFlag(4) = SL_LMBClass.sl_lmb_ubdflg
      m_arFlag(5) = SL_LMBClass.sl_lmb_ubdflg
      m_arFlag(6) = SL_LMBClass.sl_lmb_expflg
      m_arFlag(7) = SL_LMBClass.sl_lmb_expflg

      columnCount = (m_arFlag(0) + m_arFlag(2) + m_arFlag(4) + m_arFlag(6)) * 2 + 3 '3 column which is always displayed: Product code, product name, GW
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SetTableColor()
    Try

      For intCount As Integer = 0 To m_result.Length - 1
        SetRowColor(intCount, m_result(intCount).intIsUseLot)
      Next

      For intCount = m_result.Length To rowCountOld
        SetRowColor(intCount, LOT_USE)
      Next

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SetRowColor(intCount As Integer, isUseLot As Integer)
    Try
      tblResult.Rows(intCount) = New PcaRow
      Dim column As PcaRowColumn
      Dim cell As PcaRowCell
      For Each cellnameToValidate As String In m_strCellToValidate
        column = New PcaRowColumn(cellnameToValidate)
        cell = New PcaRowCell(cellnameToValidate)

        tblResult.Rows(intCount).Columns.Add(column)
        column.Cells.Add(cell)
        cell.Style = PcaColumnStyle.NewRowStyle()

        If intCount > m_result.Length - 1 Or m_result Is Nothing Then
          cell.Style.BackColor = Color.White
        ElseIf isUseLot = LOT_USE Then
          cell.Style.BackColor = Color.White
        Else
          cell.Style.BackColor = Color.LightGray
        End If
      Next
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Friend Sub SearchAndDisplay(ByVal selectedType As Integer)
    Try
      GetSearchResult(selectedType)
      DisplaySearchResult()
      If Not F12CloseFlag Then F12CloseFlag = True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub GetSearchResult(ByVal selectedType As Integer)
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader
    Dim intCount As Integer = -1
    Try
      If Not cbBox.SelectedIndex = selectedType Then cbBox.SelectedIndex = selectedType

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SMS_SL_SMS")
      selectCommand.Parameters("@sms_mkbn").SetValue(selectedType)
      reader = selectCommand.ExecuteReader

      If Not m_result Is Nothing Then rowCountOld = m_result.Length

      m_result = {}

      While reader.Read() = True

        intCount = intCount + 1
        ReDim Preserve m_result(intCount)
        m_result(intCount) = New ColumnResult

        m_result(intCount).strProductCode = reader.GetValue("sms_scd").ToString
        m_result(intCount).strProductName = reader.GetValue("sms_mei").ToString

        If Not IsDBNull(reader.GetValue("sl_sms_lkbn")) Then
          m_result(intCount).intIsUseLot = CInt(reader.GetValue("sl_sms_lkbn"))
        Else
          m_result(intCount).intIsUseLot = 0
        End If

        If Not IsDBNull(reader.GetValue("sl_sms_bbmini")) Then
          m_result(intCount).intBbMIni = CInt(reader.GetValue("sl_sms_bbmini"))
        Else
          m_result(intCount).intBbMIni = 0
        End If

        If Not IsDBNull(reader.GetValue("sl_sms_bbdini")) Then
          m_result(intCount).intBbDIni = CInt(reader.GetValue("sl_sms_bbdini"))
        Else
          m_result(intCount).intBbDIni = 0
        End If

        If Not IsDBNull(reader.GetValue("sl_sms_sdlmini")) Then
          m_result(intCount).intSdlMIni = CInt(reader.GetValue("sl_sms_sdlmini"))
        Else
          m_result(intCount).intSdlMIni = 0
        End If

        If Not IsDBNull(reader.GetValue("sl_sms_sdldini")) Then
          m_result(intCount).intSdlDIni = CInt(reader.GetValue("sl_sms_sdldini"))
        Else
          m_result(intCount).intSdlDIni = 0
        End If

        If Not IsDBNull(reader.GetValue("sl_sms_ubmini")) Then
          m_result(intCount).intUbMIni = CInt(reader.GetValue("sl_sms_ubmini"))
        Else
          m_result(intCount).intUbMIni = 0
        End If

        If Not IsDBNull(reader.GetValue("sl_sms_ubdini")) Then
          m_result(intCount).intUbDIni = CInt(reader.GetValue("sl_sms_ubdini"))
        Else
          m_result(intCount).intUbDIni = 0
        End If

        If Not IsDBNull(reader.GetValue("sl_sms_expmini")) Then
          m_result(intCount).intExpMIni = CInt(reader.GetValue("sl_sms_expmini"))
        Else
          m_result(intCount).intExpMIni = 0
        End If

        If Not IsDBNull(reader.GetValue("sl_sms_expdini")) Then
          m_result(intCount).intExpDIni = CInt(reader.GetValue("sl_sms_expdini"))
        Else
          m_result(intCount).intExpDIni = 0
        End If

        If Not IsDBNull(reader.GetValue("sl_sms_gw")) Then
          m_result(intCount).decGWRate = CInt(reader.GetValue("sl_sms_gw"))
        Else
          m_result(intCount).decGWRate = 0
        End If


      End While

      reader.Close()
      reader.Dispose()

      StatusLabel2.Text = m_result.Length.ToString & "件"

      SetTableColor()

      F12CloseFlag = True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub DisplaySearchResult()
    Try
      If m_result.Length < 1 Then
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, CommandRefresh.CommandId)
        tblResult.BodyColumns.Clear()
      Else
        If tblResult.BodyColumns.Count = 0 Then InitTableBody()
        tblResult.UpdateTable()
        tblResult.DataRowCount = m_result.Length
        tblResult.BodyRowCount = m_result.Length
      End If
      For intCount As Integer = 0 To m_result.Length - 1
        If Not SLCmnFunction.checkObjectNothingEmpty(m_result(intCount)) Then
          tblResult.SetCellValue(intCount, ColumnName.ProductCode, m_result(intCount).strProductCode)
          tblResult.SetCellValue(intCount, ColumnName.ProductName, m_result(intCount).strProductName)
          tblResult.SetCellValue(intCount, ColumnName.IsUseLot, m_result(intCount).intIsUseLot)
          tblResult.SetCellValue(intCount, ColumnName.BbMIni, m_result(intCount).intBbMIni)
          tblResult.SetCellValue(intCount, ColumnName.BbDIni, m_result(intCount).intBbDIni)
          tblResult.SetCellValue(intCount, ColumnName.SdlMIni, m_result(intCount).intSdlMIni)
          tblResult.SetCellValue(intCount, ColumnName.SdlDIni, m_result(intCount).intSdlDIni)
          tblResult.SetCellValue(intCount, ColumnName.UbMIni, m_result(intCount).intUbMIni)
          tblResult.SetCellValue(intCount, ColumnName.UbDIni, m_result(intCount).intUbDIni)
          tblResult.SetCellValue(intCount, ColumnName.ExpMIni, m_result(intCount).intExpMIni)
          tblResult.SetCellValue(intCount, ColumnName.ExpDIni, m_result(intCount).intExpDIni)
          tblResult.SetCellValue(intCount, ColumnName.GWRate, m_result(intCount).decGWRate)
        End If
      Next
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

  'Is leave or not
  Private Function LeaveConfirm() As Boolean
    If Not F12CloseFlag Then
      Dim result As Integer = MsgBox(SLConstants.ConfirmMessage.REGISTER_CONFIRM_MESSAGE, MsgBoxStyle.YesNo, SLConstants.NotifyMessage.TITLE_MESSAGE)
      If (result = Windows.Forms.DialogResult.Yes AndAlso Register()) OrElse result = Windows.Forms.DialogResult.No Then
        F12CloseFlag = True : Return True
      Else
        Return False
      End If
    End If
    F12CloseFlag = True
    Return True
  End Function

  Private Sub SetCellEditable(ByVal intColumnStart As Integer, ByVal intColumnEnd As Integer, intUseLot As Integer)
    Try
      For value As Integer = intColumnStart To intColumnEnd
        If intUseLot = LOT_USE Then
          tblResult.BodyColumns(value).Cells(0).EditMode = True
          tblResult.BodyColumns(value).Cells(0).Enabled = True
        Else
          tblResult.BodyColumns(value).Cells(0).EditMode = False
          tblResult.BodyColumns(value).Cells(0).Enabled = False
        End If

      Next
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SetCellZero(intRowIndex As Integer)
    Try
      For Each cellName In m_strCellToValidate
        tblResult.SetCellValue(intRowIndex, cellName, 0)
      Next
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Function Register() As Boolean
    Dim isExisted As Boolean = False
    Dim session As ICustomSession = connector.CreateTransactionalSession
    Dim isSuccess As Boolean = True
    Dim SL_SMSClass = New SL_SMS(connector)
    Dim strNewProductCode As String
    Dim intInsertedCnt As Integer = 0
    Try
      tblResult.ValidateInputCellValue()
      For value As Integer = 0 To m_result.Length - 1
        'If there is any row changed, save each changed row.
        If CBool(tblResult.GetCellValue(value, ColumnName.IsEdited)) AndAlso isSuccess Then
          'Check if row is exist or not -> insert or update
          strNewProductCode = tblResult.GetCellValue(value, ColumnName.ProductCode).ToString.Trim

          isExisted = SL_SMSClass.ReadByProductCode(strNewProductCode)
          SL_SMSClass.sl_sms_scd = strNewProductCode
          SL_SMSClass.sl_sms_lkbn = CInt(tblResult.GetCellValue(value, ColumnName.IsUseLot))
          SL_SMSClass.sl_sms_bbmini = CInt(tblResult.GetCellValue(value, ColumnName.BbMIni))
          SL_SMSClass.sl_sms_bbdini = CInt(tblResult.GetCellValue(value, ColumnName.BbDIni))
          SL_SMSClass.sl_sms_sdlmini = CInt(tblResult.GetCellValue(value, ColumnName.SdlMIni))
          SL_SMSClass.sl_sms_sdldini = CInt(tblResult.GetCellValue(value, ColumnName.SdlDIni))
          SL_SMSClass.sl_sms_ubmini = CInt(tblResult.GetCellValue(value, ColumnName.UbMIni))
          SL_SMSClass.sl_sms_ubdini = CInt(tblResult.GetCellValue(value, ColumnName.UbDIni))
          SL_SMSClass.sl_sms_expmini = CInt(tblResult.GetCellValue(value, ColumnName.ExpMIni))
          SL_SMSClass.sl_sms_expdini = CInt(tblResult.GetCellValue(value, ColumnName.ExpDIni))
          SL_SMSClass.sl_sms_gw = CDec(tblResult.GetCellValue(value, ColumnName.GWRate))

          'If row is existed and Flag =1 => update,
          'If row is existed and Flag =0 => Delete
          'If row is not existed and Flag = 1 => Insert
          'With selectCommand

          If isExisted Then
            If SL_SMSClass.sl_sms_lkbn = LOT_USE Then
              'Update
              isSuccess = SL_SMSClass.Update(session)
            Else
              isSuccess = SL_SMSClass.Delete(session)
            End If
          Else
            If SL_SMSClass.sl_sms_lkbn = LOT_USE Then
              SL_SMSClass.InsertedCnt = intInsertedCnt
              isSuccess = SL_SMSClass.Insert(session)
              intInsertedCnt += 1
            End If

          End If
          tblResult.SetCellValue(value, ColumnName.IsEdited, False)
        End If
      Next

      If isSuccess Then
        session.Commit()
        session.Dispose()
        F12CloseFlag = True
        SLCmnFunction.ShowToolTip(SLConstants.CommonMessage.SUCCESS_MESSAGE, "完了", ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, CommandRegister.CommandId)
        Return True
      Else
        session.Rollback()
        session.Dispose()
        F12CloseFlag = False
        Return False
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function
#End Region

#Region "Function Bar"
  Private Sub PcaCommandManager_Command(sender As System.Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    If e.CommandItem Is CommandRefresh Then
      If LeaveConfirm() Then
        tblResult.ClearCellValues()
        SearchAndDisplay(cbBox.SelectedIndex)
      End If
    ElseIf e.CommandItem Is CommandClose Then
      Me.Close()
    ElseIf e.CommandItem Is CommandRegister Then
      If Register() Then SearchAndDisplay(cbBox.SelectedIndex)
    End If
  End Sub
#End Region

#Region "Command Manager"
  Private Sub PcaCommandManager1_UpdateCommandUI(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.UpdateCommandUI
    Dim commandItem As PcaCommandItem = e.CommandItem
    If commandItem Is Me.CommandRegister Then
      commandItem.Enabled = Not F12CloseFlag
    ElseIf commandItem Is Me.CommandClose Then
      commandItem.Enabled = F12CloseFlag
    End If
  End Sub
#End Region

#Region "Table Meisai"
  Private Sub TscMeisaiTable1_CellValueChanged(sender As Object, args As PCA.Controls.EditingCellEventArgs) Handles tblResult.CellValueChanged
    Try
      If args.CellName = ColumnName.IsUseLot Then ' Change color and editable status when flag changes
        If CInt(args.EditingValue) = 0 Then
          SetCellZero(m_RowIndex)
        End If
        SetCellEditable(COL_LOTMANAGE_START, columnCount, CInt(args.EditingValue))
        SetRowColor(m_RowIndex, CInt(args.EditingValue))
      End If
      F12CloseFlag = False
      tblResult.SetCellValue(m_RowIndex, ColumnName.IsEdited, True)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub TscMeisaiTable1_EnterInputRow(sender As Object, args As PCA.Controls.InputRowEventArgs) Handles tblResult.EnterInputRow
    Try
      If args.RowIndex > m_result.Length Then
        args.Cancel = True
      Else
        m_RowIndex = args.RowIndex
        SetCellEditable(COL_LOTMANAGE_START, columnCount, CInt(tblResult.GetCellValue(args.RowIndex, ColumnName.IsUseLot)))
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub TscMeisaiTable1_GotCellFocus(sender As Object, args As PCA.Controls.CellEventArgs) Handles tblResult.GotCellFocus
    Try
      Select Case args.CellName
        Case ColumnName.BbDIni, ColumnName.SdlDIni, ColumnName.UbDIni, ColumnName.ExpDIni
          ToolStripStatusLabel1.Text = "半角4桁以内で入力してください"
        Case ColumnName.BbMIni, ColumnName.SdlMIni, ColumnName.UbMIni, ColumnName.ExpMIni
          ToolStripStatusLabel1.Text = "半角3桁以内で入力してください"
        Case ColumnName.GWRate
          ToolStripStatusLabel1.Text = "G/W率を入力してください"
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub TscMeisaiTable1_LostCellFocus(sender As Object, args As PCA.Controls.CellEventArgs) Handles tblResult.LostCellFocus
    ToolStripStatusLabel1.Text = ""
  End Sub

  Private Sub TscMeisaiTable1_ValidatingCell(sender As Object, args As PCA.Controls.CancelCellEventArgs) Handles tblResult.ValidatingCell
    Try
      If args.NewValue.ToString = "" And m_strCellToValidate.Contains(args.CellName) Then
        tblResult.SetCellValue(m_RowIndex, args.CellName, 0)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
#End Region

  Private Sub Form_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    Try
      If LeaveConfirm() Then
        If Not m_appClass.isAttach Then connector.LogOffSystem()
      Else
        e.Cancel = True
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub cbBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbBox.SelectedIndexChanged
    If LeaveConfirm() Then
      tblResult.ClearCellValues()
      SearchAndDisplay(cbBox.SelectedIndex)
    End If
  End Sub

End Class

Public Class ColumnName

  Public Const ProductCode As String = "商品コード"
  Public Const ProductName As String = "品名"
  Public Const IsUseLot As String = "ロット管理"
  Public Const BbMIni As String = "賞味期限(月)"
  Public Const BbDIni As String = "賞味期限(日)"
  Public Const SdlMIni As String = "出荷期限(月)"
  Public Const SdlDIni As String = "出荷期限(日)"
  Public Const UbMIni As String = "使用期限(月)"
  Public Const UbDIni As String = "使用期限(日)"
  Public Const ExpMIni As String = "消費期限(月)"
  Public Const ExpDIni As String = "消費期限(日)"
  Public Const GWRate As String = "G/W 率"
  Public Const IsEdited As String = "IsEdited"
End Class

Public Class ColumnResult
  Public Property strProductCode As String
  Public Property strProductName As String
  Public Property intIsUseLot As Integer
  Public Property intBbMIni As Integer
  Public Property intBbDIni As Integer
  Public Property intSdlMIni As Integer
  Public Property intSdlDIni As Integer
  Public Property intUbMIni As Integer
  Public Property intUbDIni As Integer
  Public Property intExpMIni As Integer
  Public Property intExpDIni As Integer
  Public Property decGWRate As Decimal
  Public Property IsEdited As Boolean
End Class
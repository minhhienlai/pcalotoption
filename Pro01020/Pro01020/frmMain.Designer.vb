﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.ファイルFToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRegister = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
    Me.ToolStripButtonRefresh = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
    Me.ToolStripButtonRegister = New System.Windows.Forms.ToolStripButton()
    Me.cbBox = New PCA.Controls.PcaLabeledComboBox()
    Me.tblResult = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandRefresh = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRegister = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.CommandRegister = New PCA.Controls.PcaCommandItem()
    Me.CommandClose = New PCA.Controls.PcaCommandItem()
    Me.CommandRefresh = New PCA.Controls.PcaCommandItem()
    Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
    Me.StatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.StatusStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ファイルFToolStripMenuItem})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(1384, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'ファイルFToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ファイルFToolStripMenuItem, Nothing)
    Me.ファイルFToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemRegister, Me.ToolStripMenuItemClose})
    Me.ファイルFToolStripMenuItem.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ファイルFToolStripMenuItem.Name = "ファイルFToolStripMenuItem"
    Me.ファイルFToolStripMenuItem.Size = New System.Drawing.Size(96, 20)
    Me.ファイルFToolStripMenuItem.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemRegister
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRegister, Me.CommandRegister)
    Me.ToolStripMenuItemRegister.Name = "ToolStripMenuItemRegister"
    Me.ToolStripMenuItemRegister.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemRegister.Text = "登録(&S)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.CommandClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripSeparator1, Me.ToolStripButtonRefresh, Me.ToolStripSeparator2, Me.ToolStripButtonRegister})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1384, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.CommandClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripSeparator1
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripSeparator1, Nothing)
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 37)
    '
    'ToolStripButtonRefresh
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonRefresh, Me.CommandRefresh)
    Me.ToolStripButtonRefresh.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonRefresh.Image = CType(resources.GetObject("ToolStripButtonRefresh.Image"), System.Drawing.Image)
    Me.ToolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonRefresh.Name = "ToolStripButtonRefresh"
    Me.ToolStripButtonRefresh.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonRefresh.Text = "最新"
    Me.ToolStripButtonRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripSeparator2
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripSeparator2, Nothing)
    Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
    Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 37)
    '
    'ToolStripButtonRegister
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonRegister, Me.CommandRegister)
    Me.ToolStripButtonRegister.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonRegister.Image = CType(resources.GetObject("ToolStripButtonRegister.Image"), System.Drawing.Image)
    Me.ToolStripButtonRegister.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonRegister.Name = "ToolStripButtonRegister"
    Me.ToolStripButtonRegister.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonRegister.Text = "登録"
    Me.ToolStripButtonRegister.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'cbBox
    '
    Me.cbBox.ClientProcess = Nothing
    Me.cbBox.DropDownWidth = 140
    Me.cbBox.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.cbBox.LabelText = "マスター区分"
    Me.cbBox.Location = New System.Drawing.Point(23, 91)
    Me.cbBox.Name = "cbBox"
    Me.cbBox.SelectedIndex = -1
    Me.cbBox.SelectedItem = Nothing
    Me.cbBox.SelectedText = ""
    Me.cbBox.SelectedValue = Nothing
    Me.cbBox.Size = New System.Drawing.Size(280, 22)
    Me.cbBox.TabIndex = 2
    '
    'tblResult
    '
    Me.tblResult.AccessibleRole = System.Windows.Forms.AccessibleRole.None
    Me.tblResult.AllowDrop = True
    Me.tblResult.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tblResult.BackColor = System.Drawing.Color.Transparent
    Me.tblResult.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
    Me.tblResult.BEKihonJoho = CType(resources.GetObject("tblResult.BEKihonJoho"), PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblResult.BufferRowCount = 0
    Me.tblResult.ContextMenu = Nothing
    Me.tblResult.DisabledViewStyle = PCA.Controls.DisabledViewStyle.None
    Me.tblResult.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.tblResult.HeadContextMenu = Nothing
    Me.tblResult.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.tblResult.Location = New System.Drawing.Point(23, 129)
    Me.tblResult.Name = "tblResult"
    Me.tblResult.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblResult.Size = New System.Drawing.Size(1349, 528)
    Me.tblResult.SplitterEnabled = True
    Me.tblResult.TabIndex = 111
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandRefresh, Me.PcaFunctionCommandClose, Me.PcaFunctionCommandRegister})
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 677)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1384, 28)
    Me.PcaFunctionBar1.TabIndex = 112
    Me.PcaFunctionBar1.TabStop = False
    Me.ToolTip1.SetToolTip(Me.PcaFunctionBar1, "Tooltip1")
    '
    'PcaFunctionCommandRefresh
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefresh, Me.CommandRefresh)
    Me.PcaFunctionCommandRefresh.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandRefresh.Tag = Nothing
    Me.PcaFunctionCommandRefresh.Text = "最新"
    Me.PcaFunctionCommandRefresh.ToolTipText = "最新"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.CommandClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    Me.PcaFunctionCommandClose.ToolTipText = "閉じる"
    '
    'PcaFunctionCommandRegister
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRegister, Me.CommandRegister)
    Me.PcaFunctionCommandRegister.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandRegister.Tag = Nothing
    Me.PcaFunctionCommandRegister.Text = "登録"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.CommandRefresh, Me.CommandClose, Me.CommandRegister})
    Me.PcaCommandManager1.Parent = Me
    '
    'CommandRegister
    '
    Me.CommandRegister.CommandId = 12
    Me.CommandRegister.CommandName = "テストregister"
    '
    'CommandClose
    '
    Me.CommandClose.CommandId = 12
    Me.CommandClose.CommandName = "閉じる"
    '
    'CommandRefresh
    '
    Me.CommandRefresh.CommandId = 5
    Me.CommandRefresh.CommandName = "最新"
    '
    'ToolStripStatusLabel1
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripStatusLabel1, Nothing)
    Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
    Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 21)
    '
    'StatusLabel2
    '
    Me.StatusLabel2.ActiveLinkColor = System.Drawing.Color.Transparent
    Me.StatusLabel2.AutoSize = False
    Me.PcaCommandManager1.SetCommandItem(Me.StatusLabel2, Nothing)
    Me.StatusLabel2.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.StatusLabel2.Margin = New System.Windows.Forms.Padding(0)
    Me.StatusLabel2.Name = "StatusLabel2"
    Me.StatusLabel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.StatusLabel2.RightToLeftAutoMirrorImage = True
    Me.StatusLabel2.Size = New System.Drawing.Size(1367, 26)
    Me.StatusLabel2.Spring = True
    Me.StatusLabel2.Text = "ToolStripStatusLabel2"
    Me.StatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.StatusLabel2})
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 704)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
    Me.StatusStrip1.Size = New System.Drawing.Size(1384, 26)
    Me.StatusStrip1.TabIndex = 113
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'frmMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1384, 730)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.tblResult)
    Me.Controls.Add(Me.cbBox)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1024, 300)
    Me.Name = "frmMain"
    Me.Text = "商品オプションマスタ"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    Me.StatusStrip1.ResumeLayout(False)
    Me.StatusStrip1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents ファイルFToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemRegister As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonRefresh As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents cbBox As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Protected WithEvents CommandRefresh As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandRefresh As PCA.Controls.PcaFunctionCommand
  Protected WithEvents CommandClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents StatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents ToolStripButtonRegister As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents CommandRegister As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandRegister As PCA.Controls.PcaFunctionCommand
  Friend WithEvents tblResult As PCA.TSC.Kon.Tools.TscMeisaiTable

End Class

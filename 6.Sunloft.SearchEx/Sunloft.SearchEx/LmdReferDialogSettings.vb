﻿
#Region "【更新履歴】コメント"
'14.10.31 YN@Lmd  <AppId>Kon10</AppId><Version>1.0</Version><Revision>4.00</Revision>
'
#End Region


Imports System.ComponentModel
Imports PCA.TSC.Kon.Tools

''' <summary>共通参照画面の動作形態定義項目クラス
''' </summary>
Public Class LmdReferDialogSettings

  ''' <summary>共通参照画面の表示名
  ''' （補足）
  ''' </summary>
  Public Property DialogText() As String = String.Empty

  ''' <summary>共通参照画面の表示データ項目リスト
  ''' （補足）
  ''' </summary>
  Public Property ReferItemPropertyList() As List(Of ReferItemProperty) = New List(Of ReferItemProperty)

  ''' <summary>共通参照画面参照条件画面ボタン要否
  ''' （補足）
  ''' </summary>
  Public Property NeedConditionButton() As Boolean = False

  ''' <summary>参照条件画面の属するアセンブリ名
  ''' （補足）NeedConditionButton=True時は必須。DLLやEXEファイル名
  ''' </summary>
  Public Property AssemblyName() As String = String.Empty

  ''' <summary>参照条件画面の属する名前空間名
  ''' （補足）NeedConditionButton=True時は必須。
  ''' </summary>
  Public Property NameSpaceName() As String = String.Empty

  ''' <summary>参照条件画面のクラス名
  ''' （補足）NeedConditionButton=True時は必須。
  ''' </summary>
  Public Property ClassName() As String = String.Empty

  ''' <summary>参照条件画面のコンストラクタ引数（配列）
  ''' （補足）NeedConditionButton=True時に任意。
  ''' </summary>
  Public Property ConditionDialogArgs() As Object() = Nothing

End Class


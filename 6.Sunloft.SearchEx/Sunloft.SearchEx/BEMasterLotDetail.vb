﻿Imports PCA.BaseFramework


''' <summary>業務エンティティ「ロット詳細マスター」項目定義クラス
''' </summary>
<Serializable()> _
Public Class BEMasterLotDetail

  '業務エンティティ共通基底クラス
  Inherits BusinessEntityBase

#Region "プロパティ・メンバ"

  ''' <summary>ロット詳細コード
  ''' </summary>
  Public Property LotDetailId() As Int32 = 0

  ''' <summary>ロット詳細コード
  ''' </summary>
  Public Property LotDetailCode() As String = String.Empty

  ''' <summary>ロット詳細名
  ''' </summary>
  Public Property LotDetailMei() As String = String.Empty

#End Region

#Region "コンストラクタ"


#End Region

#Region "メソッド"


#End Region

End Class

''' <summary>業務エンティティ「ロット詳細マスター」配列定義クラス
''' </summary>
<Serializable()> _
Public Class ArrayOfBEMasterLotDetail

  Public Property BEMasterLotDetail As List(Of BEMasterLotDetail) = New List(Of BEMasterLotDetail)

End Class

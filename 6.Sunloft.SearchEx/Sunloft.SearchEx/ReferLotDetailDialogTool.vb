﻿
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon
Imports System.Windows.Forms
Imports PCA.TSC.Kon.Tools

''' <summary>ロット詳細マスター参照画面のツールクラス
''' </summary>
Public Class ReferLotDetailDialogTool

  ''' <summary>共通参照画面共通変数</summary>
  Private m_ReferDialogCommonVariable As ReferDialogCommonVariable

#Region "コンストラクタ"

  Public Sub New(ByVal referDialogCommonVariable As ReferDialogCommonVariable)
    '
    m_ReferDialogCommonVariable = referDialogCommonVariable

  End Sub

#End Region

#Region "メソッド"

  ''' <summary>ロット詳細マスター参照画面の動作形態を設定
  ''' </summary>
  Public Function Setting() As Tools.TscReferDialogSettings

    '表示項目のプロパティ
    Dim referItemProperty = New Tools.ReferItemProperty
    Dim referItemPropertyList = New List(Of Tools.ReferItemProperty)

    Dim referDialogSettings As New Tools.TscReferDialogSettings

    '参照画面のフォーム名を設定
    referDialogSettings.DialogText = BusinessEntity.ItemText.Tantosya

    '参照条件画面の使用有無を設定
    referDialogSettings.NeedConditionButton = False

    '表示項目のプロパティ（表示名、カラム幅、水平配置）を設定
    referItemProperty = New Tools.ReferItemProperty With {.ItemName = BusinessEntity.ItemText.Code, .ItemWidth = 135, .ItemAlignment = HorizontalAlignment.Left}
    referItemPropertyList.Add(referItemProperty)
    referItemProperty = New Tools.ReferItemProperty With {.ItemName = BusinessEntity.ItemText.Meisyo, .ItemWidth = 240, .ItemAlignment = HorizontalAlignment.Left}
    referItemPropertyList.Add(referItemProperty)

    referDialogSettings.ReferItemPropertyList = referItemPropertyList

    Return referDialogSettings

  End Function

  ''' <summary>業務エンティティから参照画面の表示項目を編集''' </summary>
  Public Function BEToReferItems(ByVal beMasterLotDetail As BEMasterLotDetail) As ReferItems
    '
    Dim referItems As New ReferItems

    referItems = New ReferItems
    referItems.Item1 = beMasterLotDetail.LotDetailId.ToString 'ロット詳細ID
    referItems.Item2 = beMasterLotDetail.LotDetailCode        'ロット詳細コード
    referItems.Item3 = beMasterLotDetail.LotDetailMei         'ロット詳細名

    referItems.Key = beMasterLotDetail.LotDetailId.ToString   'ロット詳細ID
    '
    Return referItems

  End Function

  ''' <summary>業務エンティティから参照画面項目のリストを作成
  ''' </summary>
  ''' <param name="beMasterLotDetailList">業務エンティティリスト</param>
  Public Function SettingReferItemsList(ByVal beMasterLotDetailList As List(Of BEMasterLotDetail)) As List(Of ReferItems)
    '
    Dim referItems As New ReferItems
    Dim referItemsList As New List(Of ReferItems)

    For i As Integer = 0 To beMasterLotDetailList.Count - 1

      referItems = BEToReferItems(beMasterLotDetailList(i))
      If referItems.Key.Length <> 0 Then
        'キー設定済のみ
        referItemsList.Add(referItems)
      End If

    Next

    Return referItemsList

  End Function


  ''' <summary>業務エンティティから参照画面項目のリストを作成
  ''' </summary>
  ''' <param name="beObjectList">BEMasterLotDetailが設定されたObjectのリスト</param>
  Public Function SettingReferItemsList(ByVal beObjectList As List(Of Object)) As List(Of ReferItems)
    '
    Dim beMasterLotDetail As New BEMasterLotDetail

    Dim referItems As New ReferItems
    Dim referItemsList As New List(Of ReferItems)

    If beObjectList Is Nothing Then
      'Nothing時はゼロ件のリストを返す
      Return referItemsList
    End If

    'Object→業務エンティティ→参照画面項目リストに変換
    For i As Integer = 0 To beObjectList.Count - 1

      beMasterLotDetail = DirectCast(beObjectList(i), BEMasterLotDetail)
      referItems = BEToReferItems(beMasterLotDetail)
      If referItems.Key.Length <> 0 Then
        'キー設定済のみ
        referItemsList.Add(referItems)
      End If

    Next

    Return referItemsList

  End Function

  ''' <summary>参照画面の表示項目リストを作成
  ''' </summary>
  ''' <param name="iApplication">IIntegratedApplicationインターフェース</param>
  ''' <param name="masterLotDetailTool">業務エンティティツール</param>
  Public Function LoadReferItemsList(ByRef iApplication As IIntegratedApplication, ByVal masterLotDetailTool As MasterLotDetailTool) As List(Of ReferItems)

    Dim findParameter As New FindParameter
    Dim beObjectList As New List(Of Object)

    Dim referItemsList As New List(Of ReferItems)

    Try
      'Findのパラメータを設定

      'Find
      beObjectList = KonAPI.FindBEList(CType(iApplication, IntegratedApplication), masterLotDetailTool, findParameter)

      '表示項目リストへ変換
      referItemsList = SettingReferItemsList(beObjectList)

      Return referItemsList

    Catch ex As Exception
      Throw

    End Try

  End Function

#End Region

End Class

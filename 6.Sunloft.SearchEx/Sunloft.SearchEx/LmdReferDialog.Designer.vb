﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LmdReferDialog
    Inherits PCA.UITools.PcaDialog

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LmdReferDialog))
        Me.ToolStrip = New System.Windows.Forms.ToolStrip()
        Me.ToolStripDropDownButtonSortOrder = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ToolStripTextBox = New PCA.Controls.PcaToolStripTextBox()
        Me.ButtonConditionDialog = New PCA.Controls.PcaButton()
        Me.ListView = New PCA.Controls.PcaListView()
        Me.ToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButtonOK
        '
        Me.ButtonOK.Location = New System.Drawing.Point(250, 356)
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(328, 356)
        '
        'ButtonHelp
        '
        Me.ButtonHelp.Location = New System.Drawing.Point(328, 356)
        '
        'ToolStrip
        '
        Me.ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripDropDownButtonSortOrder, Me.ToolStripTextBox})
        Me.ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip.Name = "ToolStrip"
        Me.ToolStrip.Size = New System.Drawing.Size(412, 25)
        Me.ToolStrip.TabIndex = 103
        Me.ToolStrip.Text = "ToolStrip1"
        '
        'ToolStripDropDownButtonSortOrder
        '
        Me.ToolStripDropDownButtonSortOrder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButtonSortOrder.Image = CType(resources.GetObject("ToolStripDropDownButtonSortOrder.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButtonSortOrder.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButtonSortOrder.Name = "ToolStripDropDownButtonSortOrder"
        Me.ToolStripDropDownButtonSortOrder.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.ToolStripDropDownButtonSortOrder.Size = New System.Drawing.Size(57, 22)
        Me.ToolStripDropDownButtonSortOrder.Text = "コード"
        '
        'ToolStripTextBox
        '
        Me.ToolStripTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.ToolStripTextBox.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
        Me.ToolStripTextBox.Margin = New System.Windows.Forms.Padding(4, 0, 4, 2)
        Me.ToolStripTextBox.Name = "ToolStripTextBox"
        Me.ToolStripTextBox.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.ToolStripTextBox.Size = New System.Drawing.Size(306, 23)
        '
        'ButtonConditionDialog
        '
        Me.ButtonConditionDialog.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonConditionDialog.Location = New System.Drawing.Point(304, 35)
        Me.ButtonConditionDialog.MaxTextLength = 16
        Me.ButtonConditionDialog.Name = "ButtonConditionDialog"
        Me.ButtonConditionDialog.Size = New System.Drawing.Size(96, 22)
        Me.ButtonConditionDialog.TabIndex = 1
        Me.ButtonConditionDialog.Text = "参照条件(F8)..."
        Me.ButtonConditionDialog.UseVisualStyleBackColor = True
        '
        'ListView
        '
        Me.ListView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListView.DeactiveSelectionBackColor = System.Drawing.Color.Empty
        Me.ListView.FullRowSelect = True
        Me.ListView.HideSelection = False
        Me.ListView.Location = New System.Drawing.Point(12, 65)
        Me.ListView.MultiSelect = False
        Me.ListView.Name = "ListView"
        Me.ListView.Size = New System.Drawing.Size(388, 276)
        Me.ListView.TabIndex = 104
        Me.ListView.UseCompatibleStateImageBehavior = False
        Me.ListView.View = System.Windows.Forms.View.Details
        '
        'LmdReferDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(412, 390)
        Me.Controls.Add(Me.ToolStrip)
        Me.Controls.Add(Me.ButtonConditionDialog)
        Me.Controls.Add(Me.ListView)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.HelpVisible = False
        Me.MinimumSize = New System.Drawing.Size(410, 380)
        Me.Name = "LmdReferDialog"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.StatusBarVisible = False
        Me.Text = "参照画面"
        Me.Controls.SetChildIndex(Me.ListView, 0)
        Me.Controls.SetChildIndex(Me.ButtonConditionDialog, 0)
        Me.Controls.SetChildIndex(Me.ToolStrip, 0)
        Me.Controls.SetChildIndex(Me.ButtonOK, 0)
        Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
        Me.Controls.SetChildIndex(Me.ButtonHelp, 0)
        Me.ToolStrip.ResumeLayout(False)
        Me.ToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripDropDownButtonSortOrder As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ToolStripTextBox As PCA.Controls.PcaToolStripTextBox
    Friend WithEvents ButtonConditionDialog As PCA.Controls.PcaButton
    Friend WithEvents ListView As PCA.Controls.PcaListView
End Class

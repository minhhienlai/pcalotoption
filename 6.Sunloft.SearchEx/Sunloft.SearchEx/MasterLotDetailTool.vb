﻿Imports System.Xml
Imports PCA.TSC.Kon.BusinessEntity

''' <summary>業務エンティティ「ロット詳細マスター」ツールクラス
''' </summary>
Public Class MasterLotDetailTool

  ''' <summary>業務エンティティ「ロット詳細マスター」 URI</summary>
  Public Const URI As String = "SL_LMD"

  '業務エンティティ「ロット詳細マスター」エンティティバージョン
  Public Const BEVersion As Short = 1

  ''' <summary>業務エンティティ「ロット詳細マスター」XElement</summary>
  Public Const XElement As String = Defines.BusinessEntityPrefix & URI
  ''' <summary>業務エンティティ「ロット詳細マスター」XNode</summary>
  Public Const XNode As String = Defines.BusinessEntityNodePrefix & XElement
  ''' <summary>業務エンティティ「ロット詳細マスター」XPath</summary>
  Public Const XPath As String = XNode & Defines.XPathCharacter & XElement

#Region "メンバ・プロパティ"

#End Region

#Region "コンストラクタ"


#End Region

#Region "メソッド"

  ''' <summary>「ロット詳細マスター」のXMLから項目定義クラスへ変換</summary>
  Public Shared Function XmlNodeToBE(ByVal xmlNode As XmlNode) As BEMasterLotDetail
    '
    Dim beMasterLotDetail As New BEMasterLotDetail

    beMasterLotDetail.LotDetailId = CInt(xmlNode.SelectSingleNode("sl_lmd_id").InnerText)   'ロット詳細ID
    beMasterLotDetail.LotDetailCode = xmlNode.SelectSingleNode("sl_lmd_ldcd").InnerText     'ロット詳細コード
    beMasterLotDetail.LotDetailMei = xmlNode.SelectSingleNode("sl_lmd_mei").InnerText       'ロット詳細名

    Return beMasterLotDetail

  End Function

#End Region

End Class

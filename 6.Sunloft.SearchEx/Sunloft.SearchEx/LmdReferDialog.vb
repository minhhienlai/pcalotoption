﻿Imports System.Collections.Generic
Imports System.Drawing
Imports System.Windows.Forms

Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon
Imports PCA.TSC.Kon.Tools

''' <summary>共通参照画面
''' </summary>
Public Class LmdReferDialog

#Region "メンバ変数"

  Private m_ReferItemsList As List(Of ReferItems) = New List(Of ReferItems)
  ''' <summary>
  ''' リストビューに表示するアイテムのリストのバックアップ
  ''' (メイン画面で渡されたままのデータを保持します)
  ''' </summary>
  Private m_OriginalReferItemsList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>
  ''' ドロップダウンアイテムとListViewのカラムを表すクラスのリスト
  ''' </summary>
  Private m_ReferItemPropertyList As List(Of ReferItemProperty) = New List(Of ReferItemProperty)

  ''' <summary>
  ''' 検索するアイテムを表すインデックス
  ''' </summary>
  Dim m_TargetItemIndex As Integer = 0

  ''' <summary>
  ''' リストビューのアイテムについて、メイン画面から渡されたままのものに戻す場合はTrue
  ''' </summary>
  Dim m_NeedToSetOriginal As Boolean = False

  ''' <summary>
  ''' ロット詳細マスタ
  ''' </summary>
  ''' <remarks></remarks>
  Private m_LotDetailReader As ICustomDataReader = Nothing

  Private _conf As New Sunloft.PcaConfig

  ''' <summary>コードの長さ</summary>
  Private Const LEN_DETAILCODE As Integer = 120

  ''' <summary>名称の長さ</summary>
  Private Const LEN_DETAILNAME As Integer = 160

#End Region

#Region "プロパティ"

  ''' <summary>選択された項目のキーを取得・設定します</summary>
  Public Property SelectedItem() As String = String.Empty

  ''' <summary>IIntegratedApplicationインターフェースを設定します</summary>
  Public WriteOnly Property IApplication() As IIntegratedApplication
    Set(value As IIntegratedApplication)
      _IApplication = value
    End Set
  End Property
  Private _IApplication As IIntegratedApplication

#End Region

#Region "キー選択ドロップダウンボタンのアイテム"

  ''' <summary>1個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem1 As ToolStripMenuItem

  ''' <summary>2個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem2 As ToolStripMenuItem

  ''' <summary>3個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem3 As ToolStripMenuItem

  ''' <summary>4個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem4 As ToolStripMenuItem

  ''' <summary>5個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem5 As ToolStripMenuItem

  ''' <summary>6個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem6 As ToolStripMenuItem

  ''' <summary>7個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem7 As ToolStripMenuItem

  ''' <summary>8個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem8 As ToolStripMenuItem

  ''' <summary>9個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem9 As ToolStripMenuItem

  ''' <summary>10個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem10 As ToolStripMenuItem

  ''' <summary>11個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem11 As ToolStripMenuItem

  ''' <summary>12個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem12 As ToolStripMenuItem

  ''' <summary>13個目のToolStripMenuItem</summary>
  WithEvents ToolStripMenuItem13 As ToolStripMenuItem

  ''' <summary>ドロップダウンボタンの下のToolStripMenuItemのリスト</summary>
  Dim m_ListToolStripItem As List(Of ToolStripMenuItem)

#End Region

#Region "参照条件画面"

  ''' <summary>参照条件ダイアログ</summary>
  Dim m_ConditionDialog As Object

#End Region

  Public Sub New(ByVal LmdReferDialogSettings As LmdReferDialogSettings, ByVal intLotDetailNo As Integer, Optional ByRef IApplication As IIntegratedApplication = Nothing)
    '
    ' この呼び出しはデザイナーで必要です。
    InitializeComponent()

    Me.Text = "ロット詳細マスタ" & intLotDetailNo

    
    GetLotDetailMaster(IApplication, intLotDetailNo)

    SetSortOrderItemAndListViewItem()

    '参照条件ダイアログ起動要否により、
    If LmdReferDialogSettings.NeedConditionButton = False Then
      '参照設定ボタンの表示/非表示とリストビューの高さを設定
      SetControls_InvisibleUpperButtons()
    Else
      '参照条件ダイアログの初期化
      If LmdReferDialogSettings.ConditionDialogArgs Is Nothing Then
        m_ConditionDialog = SysTools.CreateInstanceByNameString(LmdReferDialogSettings.AssemblyName, LmdReferDialogSettings.NameSpaceName, LmdReferDialogSettings.ClassName)
      Else
        m_ConditionDialog = SysTools.CreateInstanceByNameString(LmdReferDialogSettings.AssemblyName, LmdReferDialogSettings.NameSpaceName, LmdReferDialogSettings.ClassName, LmdReferDialogSettings.ConditionDialogArgs)

      End If
    End If

    SetListViewItems()

  End Sub

#Region "コントロールを設定するメソッド"

  ''' <summary>
  ''' キー選択ドロップダウンボタンを設定</summary>
  Private Sub SetSortOrderItemAndListViewItem()
    '
    m_ListToolStripItem = New List(Of ToolStripMenuItem)()

    ToolStripDropDownButtonSortOrder.Text = m_ReferItemPropertyList(0).ItemName
    ToolStripMenuItem1 = New ToolStripMenuItem(m_ReferItemPropertyList(0).ItemName)
    m_ListToolStripItem.Add(ToolStripMenuItem1)
    ToolStripDropDownButtonSortOrder.DropDownItems.Add(ToolStripMenuItem1)
    ToolStripMenuItem1.CheckState = CheckState.Checked
    ListView.Columns.Add(m_ReferItemPropertyList(0).ItemName, m_ReferItemPropertyList(0).ItemWidth, m_ReferItemPropertyList(0).ItemAlignment)
    
    ToolStripMenuItem2 = New ToolStripMenuItem(m_ReferItemPropertyList(1).ItemName)
    m_ListToolStripItem.Add(ToolStripMenuItem2)
    ToolStripDropDownButtonSortOrder.DropDownItems.Add(ToolStripMenuItem2)
    ListView.Columns.Add(m_ReferItemPropertyList(1).ItemName, m_ReferItemPropertyList(1).ItemWidth, m_ReferItemPropertyList(1).ItemAlignment)

  End Sub

  ''' <summary>参照条件ボタンが非表示の時のダイアログのコントロールを設定</summary>
  Private Sub SetControls_InvisibleUpperButtons()
    '
    ButtonConditionDialog.Visible = False     '参照条件ボタンを非表示

    Dim PointY As Integer = ButtonConditionDialog.Location.Y      '参照条件ボタンのY座標保持
    Dim ListViewPoint As Point = ListView.Location              'デフォルト(上部のボタン表示)時のListViewの位置を保持
    Dim ListViewSize As Size = ListView.Size                    'デフォルト(上部のボタン表示)時のListViewのサイズを保持

    ListView.Location = New Point(ListViewPoint.X, PointY)
    ListView.Size = New Size(ListViewSize.Width, ListViewSize.Height + (ListViewPoint.Y - PointY))

    ButtonOK.Enabled = False

  End Sub

  ''' <summary>リストビューのアイテムを設定</summary>
  Private Sub SetListViewItems()

    Dim strLotDetailName As String = String.Empty
    Dim strLotDetailCode As String = String.Empty
    Dim intLotDetailId As Int32 = 0
    Dim strAddRow() As String

    ListView.Items.Clear()

    If m_ReferItemsList Is Nothing Then
      Exit Sub
    End If

    While m_LotDetailReader.Read

      intLotDetailId = m_LotDetailReader.GetValue("sl_lmd_id")
      strLotDetailCode = m_LotDetailReader.GetValue("sl_lmd_ldcd")
      strLotDetailName = m_LotDetailReader.GetValue("sl_lmd_mei")

      strAddRow = {strLotDetailCode, strLotDetailName}
      ListView.Items.Add(New ListViewItem(strAddRow) With {.Tag = intLotDetailId}) 'Tag = Primary key(UNIQUE key)

    End While

  End Sub

  ''' <summary>リストビューのアイテムを選択し、フォーカスを設定</summary>
  Private Sub SelectAndSetFocusListViewItem()
    '
    If m_ReferItemsList.Count > 0 Then
      'リストビューに表示する項目があるときは、一番上のアイテムを選択
      ListView.Items(0).Selected = True
      ListView.Items(0).Focused = True    '破線表示対策
      ListView.Focus()
    Else
      'リストビューに表示する項目がないときは、[設定]ボタンを選択不可にする
      ButtonOK.Enabled = False
    End If

  End Sub

#End Region

#Region "フォームイベント(オーバーライド含む)"

  ''' <summary>フォームがアクティブになった時にリストビューの選択項目を設定</summary>
  Private Sub LmdReferDialog_Activated(sender As System.Object, e As System.EventArgs) Handles MyBase.Activated
    '
    '参照条件ボタンがあるダイアログは、リストビューのアイテム保持リストを設定し直す
    If ButtonConditionDialog.Visible = True And m_NeedToSetOriginal = True Then
      m_ReferItemsList = m_OriginalReferItemsList
      SetListViewItems()
    End If

    If ListView.Items.Count > 0 Then
      ListView.TopItem.Selected = True
    End If

    If Not String.IsNullOrEmpty(_SelectedItem) Then
      For i As Integer = 0 To ListView.Items.Count - 1
        If ListView.Items(i).Tag.ToString() = _SelectedItem Then
          ListView.Items(i).Selected = True
          ListView.Items(i).Focused = True    '破線表示対策
          Exit For
        End If
      Next
    End If
  End Sub

  ''' <summary>設定ボタンが押下の時、上位画面に返すプロパティの値を設定</summary>
  Protected Overrides Sub ClickOkButton()
    '
    MyBase.ClickOkButton()

    'プロパティに渡す値を設定
    _SelectedItem = ListView.SelectedItems(0).Tag.ToString()

  End Sub

  ''' <summary>参照ダイアログが閉じられた時、フラグを設定</summary>
  Protected Overrides Sub OnClosed(e As System.EventArgs)
    '
    MyBase.OnClosed(e)

    m_NeedToSetOriginal = True

  End Sub

  Protected Overrides Function ProcessDialogKey(keyData As System.Windows.Forms.Keys) As Boolean
    Select Case keyData
      Case Keys.F1
        Return MyBase.ProcessDialogKey(Keys.None)
      Case Keys.F8
        CommandShowConditionDialog()
    End Select
    Return MyBase.ProcessDialogKey(keyData)
  End Function

#End Region

#Region "リストビューのイベント"

  ''' <summary>アイテムがダブルクリックされた時、設定ボタン押下時と同様の動作</summary>
  Private Sub ListView_DoubleClick(sender As System.Object, e As System.EventArgs) Handles ListView.DoubleClick
    '
    If ListView.SelectedItems.Count > 0 Then

      'プロパティに渡す値を設定
      _SelectedItem = ListView.SelectedItems(0).Tag.ToString()
      Me.DialogResult = Windows.Forms.DialogResult.OK

    End If

  End Sub

  ''' <summary>アイテムが選択されている時は設定ボタンをEnableに、未選択の時はDisableに。</summary>
  Private Sub ListView_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListView.SelectedIndexChanged
    '
    ButtonOK.Enabled = (ListView.SelectedItems.Count > 0)

  End Sub

#End Region

#Region "ToolStripアイテムのイベント"

  ''' <summary>ドロップダウンボタンがクリックされた時、クリックされたボタンだけチェック状態をOnにし、それ以外のチェック状態はOffに設定</summary>
  Private Sub ToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) _
   Handles ToolStripMenuItem1.Click, _
           ToolStripMenuItem2.Click, _
           ToolStripMenuItem3.Click, _
           ToolStripMenuItem4.Click, _
           ToolStripMenuItem5.Click, _
           ToolStripMenuItem6.Click, _
           ToolStripMenuItem6.Click, _
           ToolStripMenuItem7.Click, _
           ToolStripMenuItem8.Click, _
           ToolStripMenuItem9.Click, _
           ToolStripMenuItem10.Click, _
           ToolStripMenuItem11.Click, _
           ToolStripMenuItem12.Click, _
           ToolStripMenuItem13.Click
    '
    m_TargetItemIndex = 0

    For i As Integer = 0 To m_ListToolStripItem.Count - 1
      If Object.ReferenceEquals(m_ListToolStripItem(i), sender) Then
        'ClickされたToolStripMenuItemならば、Checkedにする
        m_ListToolStripItem(i).CheckState = CheckState.Checked
        'ドロップダウンのテキストを変更する
        ToolStripDropDownButtonSortOrder.Text = m_ReferItemPropertyList(i).ItemName

        m_TargetItemIndex = i

      Else
        'ClickされたToolStripMenuItemでなければ、Uncheckedにする
        m_ListToolStripItem(i).CheckState = CheckState.Unchecked

      End If
    Next

    'リストビューの項目を並べ替える
    For i As Integer = 0 To m_ReferItemPropertyList.Count - 1
      If i = m_TargetItemIndex Then
        ListView.Columns(i).DisplayIndex = 0
      ElseIf i < m_TargetItemIndex Then
        ListView.Columns(i).DisplayIndex = i + 1
      Else
        ListView.Columns(i).DisplayIndex = i
      End If
    Next

    'リストビューのアイテムを強制的に再描画
    If ListView.Items.Count > 0 Then
      ListView.RedrawItems(0, ListView.Items.Count - 1, False)
    End If

  End Sub

  Private Sub ToolStripTextBox_StopSearch(sender As Object, e As System.EventArgs) Handles ToolStripTextBox.StopSearch
    '
    ToolStripTextBox.Text = String.Empty
    ListView.TopItem.Selected = True

    ToolStripTextBox.TextBox.SearchBoxMode = PCA.Controls.SearchBoxMode.Standby

  End Sub

  ''' <summary>テキストボックスの文字列が変更された時、ListViewの選択アイテムを変更</summary>
  Private Sub ToolStripTextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles ToolStripTextBox.TextChanged
    '
    ToolStripTextBox.TextBox.SearchBoxMode = PCA.Controls.SearchBoxMode.Searching

    Dim searchString As String = ToolStripTextBox.Text
    Dim index As Integer = 0

    For index = 0 To ListView.Items.Count - 1

      'ListViewのいちばん左の項目にテキストボックスに文字が含まれていた場合はループを抜ける
      If 0 < InStr(ListView.Items(index).SubItems(m_TargetItemIndex).Text, searchString) Then

        'ループにより得た index のアイテムを選択
        ListView.Items(index).Selected = True

        Exit For
        '一致しない場合は最後までループを回す
      End If
    Next

    If index = ListView.Items.Count Then
      ListView.Items(index - 1).Selected = True

    End If

  End Sub


#End Region

  ''' <summary>参照条件ボタンクリックイベント</summary>
  Private Sub ButtonRefConditions_Click(sender As System.Object, e As System.EventArgs) Handles ButtonConditionDialog.Click
    '
    CommandShowConditionDialog()

  End Sub

  ''' <summary>参照条件画面を表示</summary>
  Private Sub CommandShowConditionDialog()
    '
    m_NeedToSetOriginal = False

    m_ConditionDialog.IApplication = DirectCast(_IApplication, IIntegratedApplication)

    If Windows.Forms.DialogResult.OK = m_ConditionDialog.ShowDialog() Then

      m_ReferItemsList = m_ConditionDialog.ReferItemsList
      SetListViewItems()
      SelectAndSetFocusListViewItem()

    End If

  End Sub

  ''' <summary>
  ''' ロット詳細マスタを取得する
  ''' </summary>
  ''' <param name="app">DBConnecter</param>
  ''' <param name="intLotDetailNo">ロット詳細マスタの番号</param>
  ''' <remarks></remarks>
  Private Sub GetLotDetailMaster(ByRef app As IIntegratedApplication, ByVal intLotDetailNo As Integer)

    Dim reader As ICustomDataReader
    Dim cmdselectCommand As ICustomCommand
    Dim referToolsProperty As New PCA.TSC.Kon.Tools.ReferItemProperty

    Try

    
      cmdselectCommand = app.CreateIsolatedCommand(_conf.PcaAddinID.Trim & "%" & "GET_SL_LMD" & intLotDetailNo)

      'cmdselectCommand.Parameters("@sl_lmd_id").SetValue(-1)
      'cmdselectCommand.Parameters("@sl_lmd_ldcd").SetValue(String.Empty)

      reader = cmdselectCommand.ExecuteReader

      m_LotDetailReader = reader

      referToolsProperty = New Tools.ReferItemProperty With {.ItemName = "ロット詳細コード", .ItemWidth = LEN_DETAILCODE, .ItemAlignment = HorizontalAlignment.Left}
      m_ReferItemPropertyList.Add(referToolsProperty)
      referToolsProperty = New Tools.ReferItemProperty With {.ItemName = "ロット詳細名", .ItemWidth = LEN_DETAILNAME, .ItemAlignment = HorizontalAlignment.Left}
      m_ReferItemPropertyList.Add(referToolsProperty)

    Catch ex As Exception
      MessageBox.Show(ex.Message.ToString)

    End Try

  End Sub

End Class



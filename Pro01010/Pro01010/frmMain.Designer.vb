﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.tsmiFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.tsmiRegister = New System.Windows.Forms.ToolStripMenuItem()
    Me.tsmiClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.toolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.btnClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
    Me.btnRegister = New System.Windows.Forms.ToolStripButton()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandRegister = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.CommandRegister = New PCA.Controls.PcaCommandItem()
    Me.CommandClose = New PCA.Controls.PcaCommandItem()
    Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.rdoArea = New PCA.Controls.PcaMultiRadioButton()
    Me.rdoNumbering = New PCA.Controls.PcaMultiRadioButton()
    Me.rdoShoumi = New PCA.Controls.PcaMultiRadioButton()
    Me.rdoShouhi = New PCA.Controls.PcaMultiRadioButton()
    Me.lblKigen = New PCA.Controls.PcaLabel()
    Me.lblKigenJK = New PCA.Controls.PcaLabel()
    Me.cmbShukka = New PCA.Controls.PcaLabeledComboBox()
    Me.cmbShiyou = New PCA.Controls.PcaLabeledComboBox()
    Me.cmbShoumi = New PCA.Controls.PcaLabeledComboBox()
    Me.cmbShouhi = New PCA.Controls.PcaLabeledComboBox()
    Me.cmbOrder = New PCA.Controls.PcaLabeledComboBox()
    Me.cmbInputing = New PCA.Controls.PcaLabeledComboBox()
    Me.rdoShukka = New PCA.Controls.PcaMultiRadioButton()
    Me.rdoShiyou = New PCA.Controls.PcaMultiRadioButton()
    Me.ltxtLotNoFormat = New PCA.Controls.PcaLabeledTextBox()
    Me.ldtCutOffDate = New PCA.Controls.PcaLabeledDate()
    Me.tabBasicInfo = New PCA.Controls.PcaTabControl()
    Me.TabBase = New System.Windows.Forms.TabPage()
    Me.lblInputing = New PCA.Controls.PcaLabel()
    Me.lblOrder = New PCA.Controls.PcaLabel()
    Me.TabLotDetail = New System.Windows.Forms.TabPage()
    Me.lnumLotNo = New PCA.Controls.PcaLabeledNumberBox()
    Me.lbDetail = New PCA.Controls.PcaLabel()
    Me.lbNote = New PCA.Controls.PcaLabel()
    Me.ltxtName10 = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtName5 = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtName9 = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtName4 = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtName8 = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtName3 = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtName7 = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtName2 = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtName6 = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtName1 = New PCA.Controls.PcaLabeledTextBox()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.MenuStrip1.SuspendLayout()
    Me.toolStrip1.SuspendLayout()
    Me.tabBasicInfo.SuspendLayout()
    Me.TabBase.SuspendLayout()
    Me.TabLotDetail.SuspendLayout()
    Me.StatusStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiFile})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(614, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'tsmiFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.tsmiFile, Nothing)
    Me.tsmiFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiRegister, Me.tsmiClose})
    Me.tsmiFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.tsmiFile.Name = "tsmiFile"
    Me.tsmiFile.Size = New System.Drawing.Size(96, 20)
    Me.tsmiFile.Text = "ファイル(&F)"
    '
    'tsmiRegister
    '
    Me.PcaCommandManager1.SetCommandItem(Me.tsmiRegister, Me.CommandRegister)
    Me.tsmiRegister.Name = "tsmiRegister"
    Me.tsmiRegister.Size = New System.Drawing.Size(137, 22)
    Me.tsmiRegister.Text = "登録(&S)"
    '
    'tsmiClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.tsmiClose, Me.CommandClose)
    Me.tsmiClose.Name = "tsmiClose"
    Me.tsmiClose.Size = New System.Drawing.Size(137, 22)
    Me.tsmiClose.Text = "閉じる(&X)"
    '
    'toolStrip1
    '
    Me.toolStrip1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.toolStrip1.AutoSize = False
    Me.toolStrip1.Dock = System.Windows.Forms.DockStyle.None
    Me.toolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.toolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnClose, Me.ToolStripSeparator1, Me.btnRegister})
    Me.toolStrip1.Location = New System.Drawing.Point(0, 29)
    Me.toolStrip1.Name = "toolStrip1"
    Me.toolStrip1.Size = New System.Drawing.Size(614, 44)
    Me.toolStrip1.TabIndex = 1
    Me.toolStrip1.Text = "ToolStrip1"
    '
    'btnClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.btnClose, Me.CommandClose)
    Me.btnClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
    Me.btnClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(53, 41)
    Me.btnClose.Text = "閉じる"
    Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripSeparator1
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripSeparator1, Nothing)
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 44)
    '
    'btnRegister
    '
    Me.PcaCommandManager1.SetCommandItem(Me.btnRegister, Me.CommandRegister)
    Me.btnRegister.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.btnRegister.Image = CType(resources.GetObject("btnRegister.Image"), System.Drawing.Image)
    Me.btnRegister.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.btnRegister.Name = "btnRegister"
    Me.btnRegister.Size = New System.Drawing.Size(39, 41)
    Me.btnRegister.Text = "登録"
    Me.btnRegister.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandRegister})
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 573)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(614, 28)
    Me.PcaFunctionBar1.TabIndex = 112
    Me.PcaFunctionBar1.TabStop = False
    Me.ToolTip1.SetToolTip(Me.PcaFunctionBar1, "Tooltip1")
    '
    'PcaFunctionCommandRegister
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRegister, Me.CommandRegister)
    Me.PcaFunctionCommandRegister.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandRegister.Tag = Nothing
    Me.PcaFunctionCommandRegister.Text = "登録"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.CommandClose, Me.CommandRegister})
    Me.PcaCommandManager1.Parent = Me
    '
    'CommandRegister
    '
    Me.CommandRegister.CommandId = 12
    Me.CommandRegister.CommandName = "CommandRegister"
    '
    'CommandClose
    '
    Me.CommandClose.CommandId = 1
    Me.CommandClose.CommandName = "閉じる"
    '
    'ToolStripStatusLabel1
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripStatusLabel1, Nothing)
    Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
    Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 18)
    '
    'rdoArea
    '
    Me.rdoArea.AutoTextSize = False
    Me.rdoArea.AutoTopMargin = False
    Me.rdoArea.BackColor = System.Drawing.Color.Transparent
    Me.rdoArea.BindingEntity = Nothing
    Me.rdoArea.ClientProcess = Nothing
    Me.rdoArea.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "自動引当", 0, ""))
    Me.rdoArea.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "手動引当", 0, ""))
    Me.rdoArea.ItemType = System.TypeCode.[String]
    Me.rdoArea.Location = New System.Drawing.Point(27, 278)
    Me.rdoArea.Name = "rdoArea"
    Me.rdoArea.Size = New System.Drawing.Size(340, 22)
    Me.rdoArea.TabIndex = 9
    Me.rdoArea.TargetProperty = Nothing
    Me.rdoArea.Text = "ロット引当区分"
    Me.rdoArea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'rdoNumbering
    '
    Me.rdoNumbering.AutoTextSize = False
    Me.rdoNumbering.AutoTopMargin = False
    Me.rdoNumbering.BackColor = System.Drawing.Color.Transparent
    Me.rdoNumbering.BindingEntity = Nothing
    Me.rdoNumbering.ClientProcess = Nothing
    Me.rdoNumbering.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "手動入力", 0, ""))
    Me.rdoNumbering.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "自動入力", 0, ""))
    Me.rdoNumbering.Location = New System.Drawing.Point(27, 320)
    Me.rdoNumbering.Name = "rdoNumbering"
    Me.rdoNumbering.Size = New System.Drawing.Size(340, 22)
    Me.rdoNumbering.TabIndex = 11
    Me.rdoNumbering.TargetProperty = Nothing
    Me.rdoNumbering.Text = "ロット番号付番方法"
    Me.rdoNumbering.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdoNumbering.XMargin = 15
    '
    'rdoShoumi
    '
    Me.rdoShoumi.AutoTextSize = False
    Me.rdoShoumi.AutoTopMargin = False
    Me.rdoShoumi.BackColor = System.Drawing.Color.Transparent
    Me.rdoShoumi.BindingEntity = Nothing
    Me.rdoShoumi.ClientProcess = Nothing
    Me.rdoShoumi.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "する", 0, ""))
    Me.rdoShoumi.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "しない", 0, ""))
    Me.rdoShoumi.Location = New System.Drawing.Point(27, 78)
    Me.rdoShoumi.Name = "rdoShoumi"
    Me.rdoShoumi.Size = New System.Drawing.Size(340, 22)
    Me.rdoShoumi.TabIndex = 3
    Me.rdoShoumi.TargetProperty = Nothing
    Me.rdoShoumi.Text = "賞味期限管理"
    Me.rdoShoumi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdoShoumi.XMargin = 15
    '
    'rdoShouhi
    '
    Me.rdoShouhi.AutoTextSize = False
    Me.rdoShouhi.AutoTopMargin = False
    Me.rdoShouhi.BackColor = System.Drawing.Color.Transparent
    Me.rdoShouhi.BindingEntity = Nothing
    Me.rdoShouhi.ClientProcess = Nothing
    Me.rdoShouhi.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "する", 0, ""))
    Me.rdoShouhi.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "しない", 0, ""))
    Me.rdoShouhi.Location = New System.Drawing.Point(27, 99)
    Me.rdoShouhi.Name = "rdoShouhi"
    Me.rdoShouhi.Size = New System.Drawing.Size(340, 22)
    Me.rdoShouhi.TabIndex = 4
    Me.rdoShouhi.TargetProperty = Nothing
    Me.rdoShouhi.Text = "消費期限管理"
    Me.rdoShouhi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdoShouhi.XMargin = 15
    '
    'lblKigen
    '
    Me.lblKigen.Location = New System.Drawing.Point(27, 14)
    Me.lblKigen.Name = "lblKigen"
    Me.lblKigen.Size = New System.Drawing.Size(140, 22)
    Me.lblKigen.TabIndex = 2
    Me.lblKigen.Text = "管理項目の設定"
    '
    'lblKigenJK
    '
    Me.lblKigenJK.Location = New System.Drawing.Point(27, 141)
    Me.lblKigenJK.Name = "lblKigenJK"
    Me.lblKigenJK.Size = New System.Drawing.Size(140, 22)
    Me.lblKigenJK.TabIndex = 2
    Me.lblKigenJK.Text = "管理項目の設定"
    '
    'cmbShukka
    '
    Me.cmbShukka.AutoTextSize = False
    Me.cmbShukka.ClientProcess = Nothing
    Me.cmbShukka.DropDownWidth = 201
    Me.cmbShukka.Items.AddRange(New Object() {"期限越えの引当可", "期限越えの引当不可", "期限越え時アラート表示"})
    Me.cmbShukka.LabelText = "出荷期限 引当条件"
    Me.cmbShukka.Location = New System.Drawing.Point(27, 166)
    Me.cmbShukka.Name = "cmbShukka"
    Me.cmbShukka.SelectedIndex = -1
    Me.cmbShukka.SelectedItem = Nothing
    Me.cmbShukka.SelectedText = ""
    Me.cmbShukka.SelectedValue = Nothing
    Me.cmbShukka.Size = New System.Drawing.Size(340, 22)
    Me.cmbShukka.TabIndex = 5
    Me.cmbShukka.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'cmbShiyou
    '
    Me.cmbShiyou.AutoTextSize = False
    Me.cmbShiyou.ClientProcess = Nothing
    Me.cmbShiyou.DropDownWidth = 201
    Me.cmbShiyou.Items.AddRange(New Object() {"期限越えの引当可", "期限越えの引当不可", "期限越え時アラート表示"})
    Me.cmbShiyou.LabelText = "使用期限 引当条件"
    Me.cmbShiyou.Location = New System.Drawing.Point(27, 187)
    Me.cmbShiyou.Name = "cmbShiyou"
    Me.cmbShiyou.SelectedIndex = -1
    Me.cmbShiyou.SelectedItem = Nothing
    Me.cmbShiyou.SelectedText = ""
    Me.cmbShiyou.SelectedValue = Nothing
    Me.cmbShiyou.Size = New System.Drawing.Size(340, 22)
    Me.cmbShiyou.TabIndex = 6
    Me.cmbShiyou.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'cmbShoumi
    '
    Me.cmbShoumi.AutoTextSize = False
    Me.cmbShoumi.ClientProcess = Nothing
    Me.cmbShoumi.DropDownWidth = 201
    Me.cmbShoumi.Items.AddRange(New Object() {"期限越えの引当可", "期限越えの引当不可", "期限越え時アラート表示"})
    Me.cmbShoumi.LabelText = "賞味期限 引当条件"
    Me.cmbShoumi.Location = New System.Drawing.Point(27, 208)
    Me.cmbShoumi.Name = "cmbShoumi"
    Me.cmbShoumi.SelectedIndex = -1
    Me.cmbShoumi.SelectedItem = Nothing
    Me.cmbShoumi.SelectedText = ""
    Me.cmbShoumi.SelectedValue = Nothing
    Me.cmbShoumi.Size = New System.Drawing.Size(340, 22)
    Me.cmbShoumi.TabIndex = 7
    Me.cmbShoumi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'cmbShouhi
    '
    Me.cmbShouhi.AutoTextSize = False
    Me.cmbShouhi.ClientProcess = Nothing
    Me.cmbShouhi.DropDownWidth = 201
    Me.cmbShouhi.Items.AddRange(New Object() {"期限越えの引当可", "期限越えの引当不可", "期限越え時アラート表示"})
    Me.cmbShouhi.LabelText = "消費期限 引当条件"
    Me.cmbShouhi.Location = New System.Drawing.Point(27, 229)
    Me.cmbShouhi.Name = "cmbShouhi"
    Me.cmbShouhi.SelectedIndex = -1
    Me.cmbShouhi.SelectedItem = Nothing
    Me.cmbShouhi.SelectedText = ""
    Me.cmbShouhi.SelectedValue = Nothing
    Me.cmbShouhi.Size = New System.Drawing.Size(340, 22)
    Me.cmbShouhi.TabIndex = 8
    Me.cmbShouhi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'cmbOrder
    '
    Me.cmbOrder.AutoTextSize = False
    Me.cmbOrder.ClientProcess = Nothing
    Me.cmbOrder.Cursor = System.Windows.Forms.Cursors.Default
    Me.cmbOrder.DropDownWidth = 201
    Me.cmbOrder.Items.AddRange(New Object() {"入庫日順"})
    Me.cmbOrder.LabelText = "ロット自動引当順序"
    Me.cmbOrder.Location = New System.Drawing.Point(27, 299)
    Me.cmbOrder.Name = "cmbOrder"
    Me.cmbOrder.SelectedIndex = 0
    Me.cmbOrder.SelectedItem = "入庫日順"
    Me.cmbOrder.SelectedText = ""
    Me.cmbOrder.SelectedValue = Nothing
    Me.cmbOrder.Size = New System.Drawing.Size(340, 22)
    Me.cmbOrder.TabIndex = 10
    Me.cmbOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'cmbInputing
    '
    Me.cmbInputing.AutoTextSize = False
    Me.cmbInputing.ClientProcess = Nothing
    Me.cmbInputing.DropDownWidth = 201
    Me.cmbInputing.Items.AddRange(New Object() {"半角入力", "全角入力"})
    Me.cmbInputing.LabelText = "ロット番号入力方法"
    Me.cmbInputing.Location = New System.Drawing.Point(27, 341)
    Me.cmbInputing.Name = "cmbInputing"
    Me.cmbInputing.SelectedIndex = 0
    Me.cmbInputing.SelectedItem = "半角入力"
    Me.cmbInputing.SelectedText = ""
    Me.cmbInputing.SelectedValue = Nothing
    Me.cmbInputing.Size = New System.Drawing.Size(340, 22)
    Me.cmbInputing.TabIndex = 12
    Me.cmbInputing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'rdoShukka
    '
    Me.rdoShukka.AutoTextSize = False
    Me.rdoShukka.AutoTopMargin = False
    Me.rdoShukka.BackColor = System.Drawing.Color.Transparent
    Me.rdoShukka.BindingEntity = Nothing
    Me.rdoShukka.ClientProcess = Nothing
    Me.rdoShukka.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "する", 0, ""))
    Me.rdoShukka.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("1", "しない", 0, ""))
    Me.rdoShukka.Location = New System.Drawing.Point(27, 36)
    Me.rdoShukka.Name = "rdoShukka"
    Me.rdoShukka.Size = New System.Drawing.Size(340, 22)
    Me.rdoShukka.TabIndex = 1
    Me.rdoShukka.TargetProperty = Nothing
    Me.rdoShukka.Text = "出荷期限管理"
    Me.rdoShukka.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdoShukka.XMargin = 15
    '
    'rdoShiyou
    '
    Me.rdoShiyou.AutoTextSize = False
    Me.rdoShiyou.AutoTopMargin = False
    Me.rdoShiyou.BackColor = System.Drawing.Color.Transparent
    Me.rdoShiyou.BindingEntity = Nothing
    Me.rdoShiyou.ClientProcess = Nothing
    Me.rdoShiyou.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "する", 0, ""))
    Me.rdoShiyou.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "しない", 0, ""))
    Me.rdoShiyou.Location = New System.Drawing.Point(27, 57)
    Me.rdoShiyou.Name = "rdoShiyou"
    Me.rdoShiyou.Size = New System.Drawing.Size(340, 22)
    Me.rdoShiyou.TabIndex = 2
    Me.rdoShiyou.TargetProperty = Nothing
    Me.rdoShiyou.Text = "使用期限管理"
    Me.rdoShiyou.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdoShiyou.XMargin = 15
    '
    'ltxtLotNoFormat
    '
    Me.ltxtLotNoFormat.AutoTextSize = False
    Me.ltxtLotNoFormat.ClientProcess = Nothing
    Me.ltxtLotNoFormat.ImeMode = System.Windows.Forms.ImeMode.Alpha
    Me.ltxtLotNoFormat.LabelText = "ロット番号フォーマット"
    Me.ltxtLotNoFormat.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtLotNoFormat.LimitLength = 26
    Me.ltxtLotNoFormat.Location = New System.Drawing.Point(27, 362)
    Me.ltxtLotNoFormat.Name = "ltxtLotNoFormat"
    Me.ltxtLotNoFormat.Size = New System.Drawing.Size(340, 22)
    Me.ltxtLotNoFormat.TabIndex = 13
    '
    'ldtCutOffDate
    '
    Me.ldtCutOffDate.ClientProcess = Nothing
    Me.ldtCutOffDate.DayLabel = ""
    Me.ldtCutOffDate.EmptyIfDisabled = True
    Me.ldtCutOffDate.Holidays = Nothing
    Me.ldtCutOffDate.LabelText = "ロット在庫締日"
    Me.ldtCutOffDate.Location = New System.Drawing.Point(27, 410)
    Me.ldtCutOffDate.MonthLabel = "/"
    Me.ldtCutOffDate.Name = "ldtCutOffDate"
    Me.ldtCutOffDate.Size = New System.Drawing.Size(253, 22)
    Me.ldtCutOffDate.TabIndex = 14
    Me.ldtCutOffDate.Visible = False
    Me.ldtCutOffDate.YearLabel = "/"
    '
    'tabBasicInfo
    '
    Me.tabBasicInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tabBasicInfo.Controls.Add(Me.TabBase)
    Me.tabBasicInfo.Controls.Add(Me.TabLotDetail)
    Me.tabBasicInfo.Location = New System.Drawing.Point(15, 77)
    Me.tabBasicInfo.Name = "tabBasicInfo"
    Me.tabBasicInfo.SelectedIndex = 0
    Me.tabBasicInfo.Size = New System.Drawing.Size(590, 493)
    Me.tabBasicInfo.TabIndex = 115
    '
    'TabBase
    '
    Me.TabBase.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
    Me.TabBase.Controls.Add(Me.lblInputing)
    Me.TabBase.Controls.Add(Me.lblOrder)
    Me.TabBase.Controls.Add(Me.rdoShukka)
    Me.TabBase.Controls.Add(Me.ldtCutOffDate)
    Me.TabBase.Controls.Add(Me.lblKigen)
    Me.TabBase.Controls.Add(Me.ltxtLotNoFormat)
    Me.TabBase.Controls.Add(Me.rdoArea)
    Me.TabBase.Controls.Add(Me.cmbShouhi)
    Me.TabBase.Controls.Add(Me.rdoNumbering)
    Me.TabBase.Controls.Add(Me.cmbInputing)
    Me.TabBase.Controls.Add(Me.rdoShiyou)
    Me.TabBase.Controls.Add(Me.cmbShiyou)
    Me.TabBase.Controls.Add(Me.rdoShoumi)
    Me.TabBase.Controls.Add(Me.cmbShoumi)
    Me.TabBase.Controls.Add(Me.rdoShouhi)
    Me.TabBase.Controls.Add(Me.cmbOrder)
    Me.TabBase.Controls.Add(Me.lblKigenJK)
    Me.TabBase.Controls.Add(Me.cmbShukka)
    Me.TabBase.Location = New System.Drawing.Point(4, 23)
    Me.TabBase.Name = "TabBase"
    Me.TabBase.Padding = New System.Windows.Forms.Padding(3)
    Me.TabBase.Size = New System.Drawing.Size(582, 466)
    Me.TabBase.TabIndex = 0
    Me.TabBase.Text = "基本情報"
    Me.TabBase.UseVisualStyleBackColor = True
    '
    'lblInputing
    '
    Me.lblInputing.AutoTextSize = False
    Me.lblInputing.Location = New System.Drawing.Point(376, 341)
    Me.lblInputing.Name = "lblInputing"
    Me.lblInputing.Size = New System.Drawing.Size(133, 22)
    Me.lblInputing.TabIndex = 15
    Me.lblInputing.Text = "※自動入力時は必須"
    Me.lblInputing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblOrder
    '
    Me.lblOrder.AutoTextSize = False
    Me.lblOrder.BackColor = System.Drawing.Color.Transparent
    Me.lblOrder.Location = New System.Drawing.Point(376, 299)
    Me.lblOrder.Name = "lblOrder"
    Me.lblOrder.Size = New System.Drawing.Size(170, 22)
    Me.lblOrder.TabIndex = 15
    Me.lblOrder.Text = "※自動引当時のみ設定可能"
    Me.lblOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'TabLotDetail
    '
    Me.TabLotDetail.Controls.Add(Me.lnumLotNo)
    Me.TabLotDetail.Controls.Add(Me.lbDetail)
    Me.TabLotDetail.Controls.Add(Me.lbNote)
    Me.TabLotDetail.Controls.Add(Me.ltxtName10)
    Me.TabLotDetail.Controls.Add(Me.ltxtName5)
    Me.TabLotDetail.Controls.Add(Me.ltxtName9)
    Me.TabLotDetail.Controls.Add(Me.ltxtName4)
    Me.TabLotDetail.Controls.Add(Me.ltxtName8)
    Me.TabLotDetail.Controls.Add(Me.ltxtName3)
    Me.TabLotDetail.Controls.Add(Me.ltxtName7)
    Me.TabLotDetail.Controls.Add(Me.ltxtName2)
    Me.TabLotDetail.Controls.Add(Me.ltxtName6)
    Me.TabLotDetail.Controls.Add(Me.ltxtName1)
    Me.TabLotDetail.Location = New System.Drawing.Point(4, 23)
    Me.TabLotDetail.Name = "TabLotDetail"
    Me.TabLotDetail.Padding = New System.Windows.Forms.Padding(3)
    Me.TabLotDetail.Size = New System.Drawing.Size(582, 466)
    Me.TabLotDetail.TabIndex = 1
    Me.TabLotDetail.Text = "ロット詳細設定"
    Me.TabLotDetail.UseVisualStyleBackColor = True
    '
    'lnumLotNo
    '
    Me.lnumLotNo.ClientProcess = Nothing
    Me.lnumLotNo.LabelText = "ロット詳細情報使用数"
    Me.lnumLotNo.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lnumLotNo.LimitLength = 2
    Me.lnumLotNo.Location = New System.Drawing.Point(27, 31)
    Me.lnumLotNo.MaxLimit = New Decimal(New Integer() {10, 0, 0, 0})
    Me.lnumLotNo.MaxTextLength = 4
    Me.lnumLotNo.Name = "lnumLotNo"
    Me.lnumLotNo.SelectedText = "0"
    Me.lnumLotNo.SelectionLength = 1
    Me.lnumLotNo.ShowCalculator = False
    Me.lnumLotNo.Size = New System.Drawing.Size(168, 22)
    Me.lnumLotNo.TabIndex = 1
    Me.lnumLotNo.Text = "0"
    Me.lnumLotNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'lbDetail
    '
    Me.lbDetail.AutoTextSize = False
    Me.lbDetail.Location = New System.Drawing.Point(27, 73)
    Me.lbDetail.Name = "lbDetail"
    Me.lbDetail.Size = New System.Drawing.Size(229, 22)
    Me.lbDetail.TabIndex = 15
    Me.lbDetail.Text = "ロット詳細名設定"
    '
    'lbNote
    '
    Me.lbNote.AutoTextSize = False
    Me.lbNote.Location = New System.Drawing.Point(209, 31)
    Me.lbNote.Name = "lbNote"
    Me.lbNote.Size = New System.Drawing.Size(229, 22)
    Me.lbNote.TabIndex = 15
    Me.lbNote.Text = "※0～10までの数字を入力してください"
    '
    'ltxtName10
    '
    Me.ltxtName10.AutoTextSize = False
    Me.ltxtName10.ClientProcess = Nothing
    Me.ltxtName10.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName10.LabelText = "ロット詳細名10"
    Me.ltxtName10.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName10.Location = New System.Drawing.Point(27, 293)
    Me.ltxtName10.Name = "ltxtName10"
    Me.ltxtName10.Size = New System.Drawing.Size(498, 25)
    Me.ltxtName10.TabIndex = 11
    '
    'ltxtName5
    '
    Me.ltxtName5.AutoSize = True
    Me.ltxtName5.AutoTextSize = False
    Me.ltxtName5.ClientProcess = Nothing
    Me.ltxtName5.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName5.LabelText = "ロット詳細名5"
    Me.ltxtName5.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName5.Location = New System.Drawing.Point(27, 188)
    Me.ltxtName5.Name = "ltxtName5"
    Me.ltxtName5.Size = New System.Drawing.Size(498, 22)
    Me.ltxtName5.TabIndex = 6
    '
    'ltxtName9
    '
    Me.ltxtName9.AutoTextSize = False
    Me.ltxtName9.ClientProcess = Nothing
    Me.ltxtName9.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName9.LabelText = "ロット詳細名9"
    Me.ltxtName9.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName9.Location = New System.Drawing.Point(27, 272)
    Me.ltxtName9.Name = "ltxtName9"
    Me.ltxtName9.Size = New System.Drawing.Size(498, 22)
    Me.ltxtName9.TabIndex = 10
    '
    'ltxtName4
    '
    Me.ltxtName4.AutoTextSize = False
    Me.ltxtName4.ClientProcess = Nothing
    Me.ltxtName4.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName4.LabelText = "ロット詳細名4"
    Me.ltxtName4.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName4.Location = New System.Drawing.Point(27, 167)
    Me.ltxtName4.Name = "ltxtName4"
    Me.ltxtName4.Size = New System.Drawing.Size(498, 22)
    Me.ltxtName4.TabIndex = 5
    '
    'ltxtName8
    '
    Me.ltxtName8.AutoTextSize = False
    Me.ltxtName8.ClientProcess = Nothing
    Me.ltxtName8.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName8.LabelText = "ロット詳細名8"
    Me.ltxtName8.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName8.Location = New System.Drawing.Point(27, 251)
    Me.ltxtName8.Name = "ltxtName8"
    Me.ltxtName8.Size = New System.Drawing.Size(498, 22)
    Me.ltxtName8.TabIndex = 9
    '
    'ltxtName3
    '
    Me.ltxtName3.AutoTextSize = False
    Me.ltxtName3.ClientProcess = Nothing
    Me.ltxtName3.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName3.LabelText = "ロット詳細名3"
    Me.ltxtName3.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName3.Location = New System.Drawing.Point(27, 146)
    Me.ltxtName3.Name = "ltxtName3"
    Me.ltxtName3.Size = New System.Drawing.Size(498, 22)
    Me.ltxtName3.TabIndex = 4
    '
    'ltxtName7
    '
    Me.ltxtName7.AutoTextSize = False
    Me.ltxtName7.ClientProcess = Nothing
    Me.ltxtName7.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName7.LabelText = "ロット詳細名7"
    Me.ltxtName7.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName7.Location = New System.Drawing.Point(27, 230)
    Me.ltxtName7.Name = "ltxtName7"
    Me.ltxtName7.Size = New System.Drawing.Size(498, 22)
    Me.ltxtName7.TabIndex = 8
    '
    'ltxtName2
    '
    Me.ltxtName2.AutoTextSize = False
    Me.ltxtName2.ClientProcess = Nothing
    Me.ltxtName2.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName2.LabelText = "ロット詳細名2"
    Me.ltxtName2.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName2.Location = New System.Drawing.Point(27, 125)
    Me.ltxtName2.Name = "ltxtName2"
    Me.ltxtName2.Size = New System.Drawing.Size(498, 22)
    Me.ltxtName2.TabIndex = 3
    '
    'ltxtName6
    '
    Me.ltxtName6.AutoTextSize = False
    Me.ltxtName6.ClientProcess = Nothing
    Me.ltxtName6.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName6.LabelText = "ロット詳細名6"
    Me.ltxtName6.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName6.Location = New System.Drawing.Point(27, 209)
    Me.ltxtName6.Name = "ltxtName6"
    Me.ltxtName6.Size = New System.Drawing.Size(498, 22)
    Me.ltxtName6.TabIndex = 7
    '
    'ltxtName1
    '
    Me.ltxtName1.AutoTextSize = False
    Me.ltxtName1.BackColor = System.Drawing.Color.Transparent
    Me.ltxtName1.ClientProcess = Nothing
    Me.ltxtName1.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.ltxtName1.LabelForeColor = System.Drawing.SystemColors.ActiveCaptionText
    Me.ltxtName1.LabelText = "ロット詳細名1"
    Me.ltxtName1.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtName1.Location = New System.Drawing.Point(27, 104)
    Me.ltxtName1.Name = "ltxtName1"
    Me.ltxtName1.Size = New System.Drawing.Size(498, 22)
    Me.ltxtName1.TabIndex = 2
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.StatusStrip1.AutoSize = False
    Me.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None
    Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 604)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(614, 23)
    Me.StatusStrip1.TabIndex = 116
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    '_frmMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(614, 630)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.tabBasicInfo)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.toolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(630, 668)
    Me.Name = "_frmMain"
    Me.Text = "ロット基本情報の登録"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.toolStrip1.ResumeLayout(False)
    Me.toolStrip1.PerformLayout()
    Me.tabBasicInfo.ResumeLayout(False)
    Me.TabBase.ResumeLayout(False)
    Me.TabLotDetail.ResumeLayout(False)
    Me.TabLotDetail.PerformLayout()
    Me.StatusStrip1.ResumeLayout(False)
    Me.StatusStrip1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents tsmiFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents tsmiRegister As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents toolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents btnClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Protected WithEvents CommandClose As PCA.Controls.PcaCommandItem
  Friend WithEvents btnRegister As System.Windows.Forms.ToolStripButton
  Friend WithEvents tsmiClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents CommandRegister As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandRegister As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ldtCutOffDate As PCA.Controls.PcaLabeledDate
  Friend WithEvents ltxtLotNoFormat As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents cmbShouhi As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents cmbInputing As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents cmbShiyou As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents cmbShoumi As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents cmbOrder As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents cmbShukka As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents lblKigenJK As PCA.Controls.PcaLabel
  Friend WithEvents lblKigen As PCA.Controls.PcaLabel
  Friend WithEvents rdoShouhi As PCA.Controls.PcaMultiRadioButton
  Friend WithEvents rdoShoumi As PCA.Controls.PcaMultiRadioButton
  Friend WithEvents rdoShiyou As PCA.Controls.PcaMultiRadioButton
  Friend WithEvents rdoNumbering As PCA.Controls.PcaMultiRadioButton
  Friend WithEvents rdoShukka As PCA.Controls.PcaMultiRadioButton
  Friend WithEvents rdoArea As PCA.Controls.PcaMultiRadioButton
  Friend WithEvents tabBasicInfo As PCA.Controls.PcaTabControl
  Friend WithEvents TabBase As System.Windows.Forms.TabPage
  Friend WithEvents TabLotDetail As System.Windows.Forms.TabPage
  Friend WithEvents lbDetail As PCA.Controls.PcaLabel
  Friend WithEvents lbNote As PCA.Controls.PcaLabel
  Friend WithEvents ltxtName10 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtName5 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtName9 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtName4 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtName8 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtName3 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtName7 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtName2 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtName6 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtName1 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents lnumLotNo As PCA.Controls.PcaLabeledNumberBox
  Friend WithEvents lblInputing As PCA.Controls.PcaLabel
  Friend WithEvents lblOrder As PCA.Controls.PcaLabel
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel

End Class

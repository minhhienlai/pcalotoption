﻿Imports PCA
Imports PCA.ApplicationIntegration
Imports PCA.Kon.Integration
Imports Sunloft.PcaConfig
Imports System.Xml
Imports System.IO
Imports System.Globalization
Imports System.Text
Imports System.Net
Imports System.Collections.Specialized
Imports System.Environment
Imports System.ComponentModel

Imports PCA.Controls
Imports PCA.TSC.Kon.BusinessEntity
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports System.Windows.Forms
Imports Sunloft.PCAIF
Imports Sunloft.Common

Public Class frmMain
  Private Const RB_DB_USE_AUTO As Integer = 1 'Use/Auto value for Shukka,Shiyou,Shoumi,Shouhi,Area. Default (Database)
  Private Const RB_DB_UNUSE_MANUAL As Integer = 0 'Unuse/Manual value for Shukka,Shiyou,Shoumi,Shouhi,Area,Numbering (Database)
  Private Const CB_NOTHING_SELECTED As Integer = -1 'ComboBox no selection
  Private Const CB_DEFAULT_SELECTED As Integer = 0 'Combobox select the first option as default

  'Error message
  Private Const ALLOCATE_NOT_SELECTED_MESSAGE = "引当条件を選択してください"
  Private Const ORDER_NOT_SELECTED_MESSAGE = "ロット自動引当順序を選択してください。"
  Private Const LOT_NO_FORMAT_NOT_SELECTED_MESSAGE = "ロット番号フォーマットを入力してください。"

  Private Const WAREHOUSING_ORDER = "入庫日順"
  Private Const SHUKKA_ORDER = "出荷期限順"
  Private Const SHIYOU_ORDER = "使用期限順"
  Private Const SHOUMI_ORDER = "賞味期限順"
  Private Const SHOUHI_ORDER = "消費期限順"

  Private connector As IIntegratedApplication = Nothing
  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private m_appClass As ConnectToPCA = New ConnectToPCA()

  Private m_isEditedFlag As Boolean = False
  Private m_intOrderTemp As Integer = 0
  Private m_isLoadingFlag As Boolean = True
  Private m_strOldLotNoFormat As String = String.Empty
  Private m_isExisted As Boolean = False
  Private m_comboSource As New Dictionary(Of Integer, String)()
  Private m_isOrderSetup As Boolean = False

#Region "Initiate"
  Public Sub New()
    InitializeComponent()

    m_appClass.ConnectToPCA()
    connector = m_appClass.connector

    IsDisplaydtpCutOffDate()
    SearchAndDisplay()

    m_isLoadingFlag = False
  End Sub
#End Region

#Region "Get value from items (To DB)"
  ''' <summary>
  ''' Convert RadioButton value to DB value
  ''' </summary>
  ''' <param name="item">Radio Button value</param>
  ''' <returns>Value to save in Database</returns>
  ''' <remarks>Get value from Radio Button and change to corresponding data value in Database</remarks>
  Private Function RdoToDBValue(ByVal item As PcaMultiRadioButton) As SLConstants.SetLotType
    Try
      Select Case item.Name
        Case rdoShukka.Name, rdoShiyou.Name, rdoShoumi.Name, rdoShouhi.Name, rdoArea.Name
          If item.RadioButtons(1 - RB_DB_USE_AUTO).Checked Then
            Return SLConstants.SetLotType.Auto
          ElseIf item.RadioButtons(1 - RB_DB_UNUSE_MANUAL).Checked = True Then
            Return SLConstants.SetLotType.Manual
          Else
            Return SLConstants.SetLotType.Auto
          End If
        Case rdoNumbering.Name
          If item.RadioButtons(RB_DB_USE_AUTO).Checked Then
            Return SLConstants.SetLotType.Auto
          ElseIf item.RadioButtons(RB_DB_UNUSE_MANUAL).Checked = True Then
            Return SLConstants.SetLotType.Manual
          Else
            Return SLConstants.SetLotType.Auto
          End If
        Case Else
          Return SLConstants.SetLotType.Auto
      End Select

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return SLConstants.SetLotType.Auto
    End Try
  End Function


  ''' <summary>
  ''' Convert combobox value to DB value
  ''' </summary>
  ''' <param name="item">ComboBox item</param>
  ''' <returns>Value to save in Database</returns>
  ''' <remarks>Get value from Combo Box and change to corresponding data value in Database => Prepare to save in DB</remarks>
  Private Function CmbToDBValue(ByVal item As PcaLabeledComboBox) As Integer
    Select Case item.Name
      Case cmbShukka.Name, cmbShiyou.Name, cmbShoumi.Name, cmbShouhi.Name, cmbInputing.Name
        Return item.SelectedIndex
      Case cmbOrder.Name
        Return CInt(item.SelectedValue)
      Case Else
        Return -1
    End Select
  End Function

#End Region

#Region "Set value for items (to display)"

  ''' <summary>
  ''' UpdateCmbOrder
  ''' </summary>
  ''' <remarks>Set Combobox Order (ロット自動引当順序) 's option corresponding to selected value in Radio Button (4期限管理）</remarks>
  Private Sub UpdateCmbOrder()
    Try
      m_isOrderSetup = False 'At CboOrder_SelectedIndexChanged not reset orderTemp value
      m_comboSource.Clear()
      m_comboSource.Add(0, WAREHOUSING_ORDER)

      If rdoShukka.RadioButtons(1 - RB_DB_USE_AUTO).Checked Then m_comboSource.Add(1, SHUKKA_ORDER)
      If rdoShiyou.RadioButtons(1 - RB_DB_USE_AUTO).Checked Then m_comboSource.Add(2, SHIYOU_ORDER)
      If rdoShoumi.RadioButtons(1 - RB_DB_USE_AUTO).Checked Then m_comboSource.Add(3, SHOUMI_ORDER)
      If rdoShouhi.RadioButtons(1 - RB_DB_USE_AUTO).Checked Then m_comboSource.Add(4, SHOUHI_ORDER)

      cmbOrder.DataSource = New BindingSource(m_comboSource, Nothing)
      cmbOrder.DisplayMember = "Value"
      cmbOrder.ValueMember = "Key"

      cmbOrder.SelectedValue = m_intOrderTemp
      If cmbOrder.SelectedIndex = -1 Then cmbOrder.SelectedIndex = CB_DEFAULT_SELECTED
      m_isOrderSetup = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  ''' <summary>
  ''' Convert DB value to combobox value
  ''' </summary>
  ''' <param name="rdoThis">This Reader/Radio Button</param>
  ''' <param name="cmbThis">This Reader/Radio Button</param>
  ''' <param name="intDisplayDefault">Default Value</param>
  ''' <remarks>Enable/Unable Combobox. If しない (Unuse) is selected in Radio Button, disable ComboBox. If not, enable it.</remarks>
  Private Sub SetCmbFromRB(ByVal rdoThis As PCA.Controls.PcaMultiRadioButton, ByVal cmbThis As PCA.Controls.PcaLabeledComboBox, Optional ByVal intDisplayDefault As Integer = -1)
    Try
      If rdoThis.RadioButtons(1 - RB_DB_UNUSE_MANUAL).Checked = True Then
        cmbThis.Enabled = False
        'When a ComboBox is enable, default value is -1, no selection.
        cmbThis.SelectedIndex = intDisplayDefault
      Else
        cmbThis.Enabled = True
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region "Form & Item Events"

  Private Sub Form1_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    Try
      If LeaveConfirm() Then
        If Not m_appClass.isAttach Then connector.LogOffSystem()
      Else
        e.Cancel = True
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  ''' <summary>
  ''' TabBasicInfo_SelectedIndexChanged
  ''' </summary>
  ''' <param name="sender">Based on the handle</param>
  ''' <param name="e">Based on the handle</param>
  ''' <remarks>When selected index change, erase old tooltip</remarks>
  Private Sub TabBasicInfo_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles tabBasicInfo.SelectedIndexChanged
    ToolStripStatusLabel1.Text = String.Empty
  End Sub

  ''' <summary>
  ''' Item_ValueChanged
  ''' </summary>
  ''' <param name="sender">Based on the handle</param>
  ''' <param name="e">Based on the handle</param>
  ''' <remarks>When user edit something,  set tooltiptext </remarks>
  Private Sub Item_ValueChanged(sender As Object, e As System.EventArgs) Handles cmbShukka.SelectedIndexChanged, cmbShiyou.SelectedIndexChanged, _
                                                                                 cmbShouhi.SelectedIndexChanged, cmbShoumi.SelectedIndexChanged, _
                                                                                 rdoNumbering.SelectedValueChanged, cmbInputing.SelectedIndexChanged, _
                                                                                 ldtCutOffDate.DateChanged, ltxtName1.TextChanged, ltxtName2.TextChanged, _
                                                                                 ltxtName3.TextChanged, ltxtName4.TextChanged, ltxtName5.TextChanged, _
                                                                                 ltxtName6.TextChanged, ltxtName7.TextChanged, ltxtName8.TextChanged, _
                                                                                 ltxtName9.TextChanged, ltxtName10.TextChanged
    Try
      If Not m_isLoadingFlag Then m_isEditedFlag = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' rdo_ValueChanged
  ''' </summary>
  ''' <param name="sender">Based on the handle</param>
  ''' <param name="e">Based on the handle</param>
  ''' <remarks>Update cmbOrder selection, set tooltip and mark data as edited</remarks>
  Private Sub rdo_ValueChanged(sender As Object, e As System.EventArgs) Handles rdoShukka.SelectedValueChanged, rdoShiyou.SelectedValueChanged, _
                                                                                rdoShoumi.SelectedValueChanged, rdoShouhi.SelectedValueChanged, _
                                                                                rdoArea.SelectedValueChanged, rdoNumbering.SelectedValueChanged
    Try
      Dim senderName = CType(sender, PcaMultiRadioButton).Name
      Select Case senderName
        Case rdoShukka.Name
          SetCmbFromRB(rdoShukka, cmbShukka)
        Case rdoShiyou.Name
          SetCmbFromRB(rdoShiyou, cmbShiyou)
        Case rdoShoumi.Name
          SetCmbFromRB(rdoShoumi, cmbShoumi)
        Case rdoShouhi.Name
          SetCmbFromRB(rdoShouhi, cmbShouhi)
        Case rdoArea.Name
          If rdoArea.RadioButtons(1).Checked = True Then
            cmbOrder.SelectedIndex = CB_DEFAULT_SELECTED
            cmbOrder.Enabled = False
          Else
            cmbOrder.Enabled = True
            If ltxtLotNoFormat.Text = String.Empty Then
              ltxtLotNoFormat.Focus()
            End If
          End If
      End Select

      If Not m_isLoadingFlag Then
        If {rdoShukka.Name, rdoShiyou.Name, rdoShoumi.Name, rdoShouhi.Name}.Contains(senderName) Then UpdateCmbOrder()
        SetToolTipSelect(CType(sender, PcaMultiRadioButton).Text.ToString.Trim)
        m_isEditedFlag = True
      End If

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' ltxtLotNoFormat_TextChanged
  ''' </summary>
  ''' <param name="sender">Based on the handle</param>
  ''' <param name="e">Based on the handle</param>
  ''' <remarks>When selected ltxtLotNoFormat changes, validate input, mark data as edited.</remarks>
  Private Sub ltxtLotNoFormat_TextChanged(sender As Object, e As System.EventArgs) Handles ltxtLotNoFormat.TextChanged
    Try
      If Not System.Text.RegularExpressions.Regex.IsMatch(ltxtLotNoFormat.Text, "^[a-zA-Z0-9!-/:-@¥[-`{-~]+$") AndAlso ltxtLotNoFormat.Text <> String.Empty Then
        ltxtLotNoFormat.Text = m_strOldLotNoFormat
      Else
        m_strOldLotNoFormat = ltxtLotNoFormat.Text
      End If
      If Not m_isLoadingFlag Then m_isEditedFlag = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  ''' <summary>
  ''' lnumLotNo_TextChanged
  ''' </summary>
  ''' <param name="sender">Based on the handle</param>
  ''' <param name="e">Based on the handle</param>
  ''' <remarks>When text in lnumLotNo(ロット詳細情報使用数) changes, enable/disable corresponding fields in textName (ロット詳細名1-10)</remarks>
  Private Sub lnumLotNo_TextChanged(sender As Object, e As System.EventArgs) Handles lnumLotNo.TextChanged
    Try
      Dim intNameCount As Integer = 0
      Dim tbArray As PCA.Controls.PcaLabeledTextBox() = {ltxtName1, ltxtName2, ltxtName3, ltxtName4, ltxtName5, ltxtName6, ltxtName7, ltxtName8, ltxtName9, ltxtName10}

      Integer.TryParse(lnumLotNo.Text, intNameCount)

      For i = 0 To System.Math.Min(intNameCount - 1, 9)
        tbArray(i).Enabled = True
        tbArray(i).TextBackColor = Color.White
      Next
      For i = intNameCount To tbArray.Count - 1
        tbArray(i).Enabled = False
        tbArray(i).TextBackColor = Color.LightGray
      Next
      If Not m_isLoadingFlag Then m_isEditedFlag = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' CboOrder_SelectedIndexChanged
  ''' </summary>
  ''' <param name="sender">Based on the handle</param>
  ''' <param name="e">Based on the handle</param>
  ''' <remarks>If order is changed, set isEditedFlag = true</remarks>
  Private Sub CboOrder_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbOrder.SelectedIndexChanged
    Try
      If m_isOrderSetup Then m_intOrderTemp = DirectCast(cmbOrder.SelectedItem, KeyValuePair(Of Integer, String)).Key
      If Not m_isLoadingFlag Then m_isEditedFlag = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Item_Enter
  ''' </summary>
  ''' <param name="sender">Based on the handle</param>
  ''' <param name="e">Based on the handle</param>
  ''' <remarks>Set tooltip lnumLotNo(ロット詳細情報使用数)</remarks>
  Private Sub Item_Enter(sender As Object, e As System.EventArgs) Handles lnumLotNo.Click, ltxtLotNoFormat.Enter, _
                                                                          ltxtName1.Enter, ltxtName2.Enter, ltxtName3.Enter, ltxtName4.Enter, ltxtName5.Enter, _
                                                                          ltxtName6.Enter, ltxtName7.Enter, ltxtName8.Enter, ltxtName9.Enter, ltxtName10.Enter, _
                                                                          cmbInputing.Enter, cmbOrder.Enter, cmbShiyou.Enter, cmbShouhi.Enter, cmbShoumi.Enter, cmbShukka.Enter, ldtCutOffDate.Enter
    Try
      Dim senderType = sender.GetType.ToString
      Select Case senderType
        Case GetType(PCA.Controls.PcaLabeledNumberBox).ToString
          If Not m_isLoadingFlag Then SetToolTipInput(TryCast(sender, PcaLabeledNumberBox).LabelText.ToString.Trim)
        Case GetType(PCA.Controls.PcaLabeledTextBox).ToString
          If Not m_isLoadingFlag Then SetToolTipInput(TryCast(sender, PcaLabeledTextBox).LabelText.ToString.Trim)
        Case GetType(PCA.Controls.PcaLabeledComboBox).ToString
          If Not m_isLoadingFlag Then SetToolTipSelect(TryCast(sender, PcaLabeledComboBox).LabelText.ToString.Trim)
        Case GetType(PCA.Controls.PcaLabeledDate).ToString
          If Not m_isLoadingFlag Then SetToolTipSelect(TryCast(sender, PcaLabeledDate).LabelText.ToString.Trim)
      End Select

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub
#End Region

#Region "Private method"

  Private Function checkRdoAndCmb(rdo As PCA.Controls.PcaMultiRadioButton, cmb As PCA.Controls.PcaLabeledComboBox) As Boolean
    If RdoToDBValue(rdo) = RB_DB_USE_AUTO And CmbToDBValue(cmb) = CB_NOTHING_SELECTED Then
      tabBasicInfo.SelectedTab = TabBase
      cmb.Focus()
      SLCmnFunction.ShowToolTip(ALLOCATE_NOT_SELECTED_MESSAGE, SLConstants.NotifyMessage.TITLE_MESSAGE, _
                                               ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, CommandRegister.CommandId)
      Return False
    End If
    Return True
  End Function


  ''' <summary>
  ''' LeaveConfirm
  ''' </summary>
  ''' <returns> 0 if program can close, 1 if program cannot close (Close is cancelled or error when register before close)</returns>
  ''' <remarks>when user click/Press Close or click x on screen</remarks>
  Private Function LeaveConfirm() As Boolean
    If m_isEditedFlag = True Then
      Dim result As Integer = MsgBox(SLConstants.ConfirmMessage.REGISTER_CONFIRM_MESSAGE, MsgBoxStyle.YesNoCancel, SLConstants.ConfirmMessage.TITLE_MESSAGE)
      If (result = DialogResult.Yes AndAlso Register()) OrElse result = DialogResult.No Then
        m_isEditedFlag = False : Return True
      Else
        Return False
      End If
    End If
    Return True
  End Function

  ''' <summary>
  ''' Register to db
  ''' </summary>
  ''' <returns>Whether or not it succeeded</returns>
  ''' <remarks>Get data and save to Database</remarks>
  Private Function Register() As Boolean
    Try
      Dim result As Boolean
      Dim txtLotNameArr As PCA.Controls.PcaLabeledTextBox() = {ltxtName1, ltxtName2, ltxtName3, ltxtName4, ltxtName5, ltxtName6, ltxtName7, ltxtName8, ltxtName9, ltxtName10}
      Dim SL_LMBClass = New SL_LMB(connector)
      Dim session As ICustomSession = connector.CreateTransactionalSession
      'Validate
      If Not checkRdoAndCmb(rdoShukka, cmbShukka) OrElse Not checkRdoAndCmb(rdoShoumi, cmbShoumi) OrElse _
         Not checkRdoAndCmb(rdoShiyou, cmbShiyou) OrElse Not checkRdoAndCmb(rdoShouhi, cmbShouhi) Then Return False

      If RdoToDBValue(rdoArea) = RB_DB_USE_AUTO And CmbToDBValue(cmbOrder) = CB_NOTHING_SELECTED Then
        tabBasicInfo.SelectedTab = TabBase
        cmbOrder.Focus()
        SLCmnFunction.ShowToolTip(ORDER_NOT_SELECTED_MESSAGE, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, _
                                  CommandRegister.CommandId)
        Return False
      End If
      If RdoToDBValue(rdoNumbering) = RB_DB_USE_AUTO And String.IsNullOrEmpty(ltxtLotNoFormat.Text.ToString.Trim) Then
        tabBasicInfo.SelectedTab = TabBase
        ltxtLotNoFormat.Focus()
        SLCmnFunction.ShowToolTip(LOT_NO_FORMAT_NOT_SELECTED_MESSAGE, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, _
                                  Me, CommandRegister.CommandId)
        Return False
      End If
      For intLpc As Integer = 0 To CInt(lnumLotNo.Text) - 1
        If String.IsNullOrEmpty(txtLotNameArr(intLpc).Text) Then
          tabBasicInfo.SelectedTab = TabLotDetail
          txtLotNameArr(intLpc).Focus()
          Dim errMessage = "ロット詳細名" & (intLpc + 1).ToString & "を入力してください。"
          SLCmnFunction.ShowToolTip(errMessage, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, CommandRegister.CommandId)
          Return False
        End If
      Next

      'Register
      SL_LMBClass.sl_lmb_sdlflg = RdoToDBValue(rdoShukka)
      SL_LMBClass.sl_lmb_ubdflg = RdoToDBValue(rdoShiyou)
      SL_LMBClass.sl_lmb_bbdflg = RdoToDBValue(rdoShoumi)
      SL_LMBClass.sl_lmb_expflg = RdoToDBValue(rdoShouhi)
      SL_LMBClass.sl_lmb_sdlhikiate = CmbToDBValue(cmbShukka)
      SL_LMBClass.sl_lmb_udbhikiate = CmbToDBValue(cmbShiyou)
      SL_LMBClass.sl_lmb_bbdhikiate = CmbToDBValue(cmbShoumi)
      SL_LMBClass.sl_lmb_exphikiate = CmbToDBValue(cmbShouhi)

      SL_LMBClass.sl_lmb_maxlotop = Integer.Parse(lnumLotNo.Text.ToString.Trim)
      SL_LMBClass.sl_lmb_autohikiate = RdoToDBValue(rdoArea)
      SL_LMBClass.sl_lmb_hikiatesort = CmbToDBValue(cmbOrder)
      SL_LMBClass.sl_lmb_autolot = RdoToDBValue(rdoNumbering)
      SL_LMBClass.sl_lmb_lotfmt = ltxtLotNoFormat.Text.ToString.Trim
      SL_LMBClass.sl_lmb_lotime = CmbToDBValue(cmbInputing)

      SL_LMBClass.sl_lmb_label1 = ltxtName1.Text
      SL_LMBClass.sl_lmb_label2 = ltxtName2.Text
      SL_LMBClass.sl_lmb_label3 = ltxtName3.Text
      SL_LMBClass.sl_lmb_label4 = ltxtName4.Text
      SL_LMBClass.sl_lmb_label5 = ltxtName5.Text
      SL_LMBClass.sl_lmb_label6 = ltxtName6.Text
      SL_LMBClass.sl_lmb_label7 = ltxtName7.Text
      SL_LMBClass.sl_lmb_label8 = ltxtName8.Text
      SL_LMBClass.sl_lmb_label9 = ltxtName9.Text
      SL_LMBClass.sl_lmb_label10 = ltxtName10.Text

      SL_LMBClass.sl_lmb_zcutoff_date = Integer.Parse(Format(ldtCutOffDate.Date, "yyyyMMdd"))

      If m_isExisted Then
        result = SL_LMBClass.Update()
      Else
        result = SL_LMBClass.Insert()
      End If

      If result Then
        SLCmnFunction.ShowToolTip(SLConstants.CommonMessage.SUCCESS_MESSAGE, "完了", ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, CommandRegister.CommandId)
        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Friend Sub IsDisplaydtpCutOffDate()
    Dim result = connector.GetLogOnUser
    'Only User with UserLevel = 1 (System Admin) can see dtpCutOffDate (ロット在庫締日)
    Dim XMLDoc As New XmlDocument()
    Dim xmlNameNode As XmlNodeList
    Dim xmlLevelNode As XmlNodeList
    XMLDoc.LoadXml(result.BusinessValue)
    xmlNameNode = XMLDoc.GetElementsByTagName("UserId")
    xmlLevelNode = XMLDoc.GetElementsByTagName("UserLevel")
    For i = 0 To xmlNameNode.Count - 1
      If xmlNameNode(i).ChildNodes.Item(0).InnerText.Trim() = connector.UserId Then
        If xmlLevelNode(i).ChildNodes.Item(0).InnerText.Trim() = "1" Then ldtCutOffDate.Visible = True
      End If
    Next
  End Sub

  ''' <summary>
  ''' SearchAndDisplay
  ''' </summary>
  ''' <returns>isSuccess	：if function is excecuted successfully</returns>
  ''' <remarks>Get data from database and display.</remarks>
  Friend Function SearchAndDisplay() As Boolean
    Dim isSuccess As Boolean = True
    Try
      Dim SL_LMBClass = New SL_LMB(connector)
      If SL_LMBClass.ReadOnlyRow() Then m_isExisted = True

      rdoShukka.RadioButtons(1 - SL_LMBClass.sl_lmb_sdlflg).Checked = True
      rdoShiyou.RadioButtons(1 - SL_LMBClass.sl_lmb_ubdflg).Checked = True
      rdoShoumi.RadioButtons(1 - SL_LMBClass.sl_lmb_bbdflg).Checked = True
      rdoShouhi.RadioButtons(1 - SL_LMBClass.sl_lmb_expflg).Checked = True
      rdoArea.RadioButtons(1 - SL_LMBClass.sl_lmb_autohikiate).Checked = True
      rdoNumbering.RadioButtons(SL_LMBClass.sl_lmb_autolot).Checked = True

      cmbShukka.SelectedIndex = SL_LMBClass.sl_lmb_sdlhikiate
      cmbShiyou.SelectedIndex = SL_LMBClass.sl_lmb_udbhikiate
      cmbShoumi.SelectedIndex = SL_LMBClass.sl_lmb_bbdhikiate
      cmbShouhi.SelectedIndex = SL_LMBClass.sl_lmb_exphikiate

      If m_isOrderSetup Then m_intOrderTemp = DirectCast(cmbOrder.SelectedItem, KeyValuePair(Of Integer, String)).Key
      cmbInputing.SelectedIndex = SL_LMBClass.sl_lmb_lotime

      SetCmbFromRB(rdoShukka, cmbShukka)
      SetCmbFromRB(rdoShiyou, cmbShiyou)
      SetCmbFromRB(rdoShoumi, cmbShoumi)
      SetCmbFromRB(rdoShouhi, cmbShouhi)
      SetCmbFromRB(rdoArea, cmbOrder, 0)

      UpdateCmbOrder()
      cmbOrder.SelectedValue = SL_LMBClass.sl_lmb_hikiatesort

      ltxtLotNoFormat.Text = SL_LMBClass.sl_lmb_lotfmt
      If SL_LMBClass.sl_lmb_zcutoff_date <> 0 Then ldtCutOffDate.Date = Date.Parse(Format(SL_LMBClass.sl_lmb_zcutoff_date, "0000/00/00"))
      lnumLotNo.Text = SL_LMBClass.sl_lmb_maxlotop.ToString

      ltxtName1.Text = SL_LMBClass.sl_lmb_label1
      ltxtName2.Text = SL_LMBClass.sl_lmb_label2
      ltxtName3.Text = SL_LMBClass.sl_lmb_label3
      ltxtName4.Text = SL_LMBClass.sl_lmb_label4
      ltxtName5.Text = SL_LMBClass.sl_lmb_label5
      ltxtName6.Text = SL_LMBClass.sl_lmb_label6
      ltxtName7.Text = SL_LMBClass.sl_lmb_label7
      ltxtName8.Text = SL_LMBClass.sl_lmb_label8
      ltxtName9.Text = SL_LMBClass.sl_lmb_label9
      ltxtName10.Text = SL_LMBClass.sl_lmb_label10

      Dim arrTxt As PCA.Controls.PcaLabeledTextBox() = {ltxtName1, ltxtName2, ltxtName3, ltxtName4, ltxtName5, ltxtName6, ltxtName7, ltxtName8, ltxtName9, ltxtName10}
      For intCount As Integer = CInt(lnumLotNo.Text) To arrTxt.Count - 1
        arrTxt(intCount).Enabled = False
        arrTxt(intCount).TextBackColor = Color.LightGray 'TEXT_UNABLE_BACK_COLOR
      Next

      m_isEditedFlag = False
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function
#End Region

#Region "Command Manager"
  '***********************************************************																														
  'Name : PcaCommandManager_Command																														
  'Content : Run Close/Register function when command is called
  'Return Value : None
  'Argument : None
  'Created date : 2015/9/11 by Lai Minh Hien
  'Modyfied date :         by        Content																														
  '***********************************************************
  Private Sub PcaCommandManager_Command(sender As System.Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command        '
    If e.CommandItem Is CommandClose Then
      Me.Close()
    ElseIf e.CommandItem Is CommandRegister Then
      If Register() Then
        m_isEditedFlag = False
        SearchAndDisplay()
      End If
    End If
  End Sub
#End Region

#Region "ToolTip"
  '***********************************************************																														
  'Name : SetToolTipInput																														
  'Content : Set tooltip: ○○を入力してください (when user need to input something)
  'Return Value : None
  'Argument : strName: Field name
  'Created date : 2015/9/11 by Lai Minh Hien
  'Modyfied date :         by        Content																														
  '***********************************************************
  Private Sub SetToolTipInput(ByVal strName As String)
    ToolStripStatusLabel1.Text = strName & "を入力してください"
  End Sub

  '***********************************************************																														
  'Name : SetToolTipSelect																														
  'Content : Set tooltip: ○○を選択てください (when user need to select an options)
  'Return Value : None
  'Argument : strName: Field name
  'Created date : 2015/9/11 by Lai Minh Hien
  'Modyfied date :         by        Content																														
  '***********************************************************
  Private Sub SetToolTipSelect(ByVal strName As String)
    ToolStripStatusLabel1.Text = strName & "を選択してください"
  End Sub
#End Region


End Class


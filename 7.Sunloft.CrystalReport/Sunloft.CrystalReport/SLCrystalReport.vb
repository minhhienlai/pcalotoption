Namespace Windows.Forms
  Public Class SLCrystalReport
    'Private _reportFile As String = String.Empty
    'Private _dataSource As Object
    Private _prtSettings As System.Drawing.Printing.PrinterSettings
    Private _pageSettings As System.Drawing.Printing.PageSettings
    'Private _params As Dictionary(Of String, Object)
    Private _exportPDFFileName As String = String.Empty

#Region "パブリック・プロパティ"
    ''' <summary>レポートファイルのパスを取得または設定します。</summary>
    Public Property ReportFile() As String

    ''' <summary>印刷するデータの格納されたデータソースを取得または設定します。</summary>
    Public Property DataSource() As Object

    ''' <summary>レポートのPrinterSettingsを設定します。</summary>
    Public WriteOnly Property PrinterSettings() As System.Drawing.Printing.PrinterSettings
      Set(ByVal value As System.Drawing.Printing.PrinterSettings)
        Me._prtSettings = value
      End Set
    End Property

    ''' <summary>レポートのPageSettingsを設定します。</summary>
    Public WriteOnly Property PageSettings() As System.Drawing.Printing.PageSettings
      Set(ByVal value As System.Drawing.Printing.PageSettings)
        Me._pageSettings = value
      End Set
    End Property

    ''' <summary>パラメータをDictionalyで取得または設定します。</summary>
    Public Property Params() As Dictionary(Of String, Object)

    ''' <summary>PrintToPrinterメソッドで印刷されたデータをPDFファイルに保存する際のファイル名を設定します。</summary>
    Public WriteOnly Property ExportPDFFileName() As String
      Set(ByVal value As String)
        Me._exportPDFFileName = value
      End Set
    End Property

#End Region

#Region "パブリック・メソッド"
    ''' <summary>
    ''' 指定されたレポートをプレビューします。
    ''' </summary>
    ''' <param name="title">プレビューウィンドウのタイトル</param>
    ''' <remarks></remarks>
    Public Sub Preview(ByVal title As String)
      Me.Preview(title, True)
      'Dim frm As New Windows.Forms.SLCrystalReportViewer
      'frm.ReportFile = Me._reportFile
      'frm.DataSource = Me._dataSource
      'frm.PrinterSettings = Me._prtSettings
      'frm.PageSettings = Me._pageSettings
      'frm.Text = title
      'frm.Params = Me._params
      'frm.ShowDialog()
    End Sub

    ''' <summary>
    ''' 指定されたレポートをプレビューします。
    ''' </summary>
    ''' <param name="title">プレビューウィンドウのタイトル</param>
    ''' <remarks></remarks>
    Public Sub Preview(ByVal title As String, ByVal isModal As Boolean)
      Dim frm As New Windows.Forms.SLCrystalReportViewer
      frm.ReportFile = Me._reportFile
      frm.DataSource = Me._dataSource
      frm.PrinterSettings = Me._prtSettings
      frm.PageSettings = Me._pageSettings
      frm.Text = title
      frm.Params = Me._params
      If isModal Then
        frm.ShowDialog()
      Else
        frm.Show()
      End If
    End Sub

    ''' <summary>指定されたレポートを印刷します。</summary>
    Public Sub PrintToPrinter()
      Dim rpt As New CrystalDecisions.CrystalReports.Engine.ReportDocument()
      rpt.Load(_reportFile)
      rpt.SetDataSource(_dataSource)
      If Me._prtSettings IsNot Nothing AndAlso Me._pageSettings IsNot Nothing Then
        Try
          'ラベルプリンタでエラーになってしまうのでTryする
          rpt.PrintOptions.CopyFrom(Me._prtSettings, Me._pageSettings)
        Catch ex As Exception

        End Try
      End If

      'パラメータの設定
      If _params IsNot Nothing Then
        For Each pair As KeyValuePair(Of String, Object) In _params
          rpt.SetParameterValue(pair.Key, pair.Value)
        Next
      End If

      rpt.PrintToPrinter(1, False, 0, 0)

      If _exportPDFFileName.Length > 0 Then
        rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, _exportPDFFileName)
      End If
    End Sub
#End Region
  End Class
End Namespace

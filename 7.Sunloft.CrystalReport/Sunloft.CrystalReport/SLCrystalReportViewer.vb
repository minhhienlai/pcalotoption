﻿Imports System.Windows.Forms

Namespace Windows.Forms

  Friend Class SLCrystalReportViewer
    'Private ReportFile As String = String.Empty
    'Private _dataSource As Object
    Private _prtSettings As System.Drawing.Printing.PrinterSettings
    Private _pageSettings As System.Drawing.Printing.PageSettings
    'Private _params As Dictionary(Of String, Object)


#Region "パブリック・プロパティ"
    ''' <summary>レポートファイルのパスを取得または設定します。</summary>
    Public Property ReportFile() As String

    ''' <summary>印刷するデータの格納されたデータソースを取得または設定します。</summary>
    Public Property DataSource() As Object

    ''' <summary>レポートのPrinterSettingsを設定します。</summary>
    Public WriteOnly Property PrinterSettings() As System.Drawing.Printing.PrinterSettings
      Set(ByVal value As System.Drawing.Printing.PrinterSettings)
        Me._prtSettings = value
      End Set
    End Property

    ''' <summary>レポートのPageSettingsを設定します。</summary>
    Public WriteOnly Property PageSettings() As System.Drawing.Printing.PageSettings
      Set(ByVal value As System.Drawing.Printing.PageSettings)
        Me._pageSettings = value
      End Set
    End Property

    ''' <summary>パラメータをDictionalyで取得または設定します。</summary>
    Public Property Params() As Dictionary(Of String, Object)
#End Region

#Region "Form"
    Private Sub SLCrystalReportViewer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      Me.Cursor = Cursors.WaitCursor
      Dim rpt As New CrystalDecisions.CrystalReports.Engine.ReportDocument()
      rpt.Load(_reportFile)
      rpt.SetDataSource(_dataSource)

      If Me._prtSettings IsNot Nothing AndAlso Me._pageSettings IsNot Nothing Then
        Try
          'ラベルプリンタでエラーになってしまうのでTryする
          rpt.PrintOptions.CopyFrom(Me._prtSettings, Me._pageSettings)
        Catch ex As Exception

        End Try
      End If

      'パラメータの設定
      If _params IsNot Nothing Then
        For Each pair As KeyValuePair(Of String, Object) In _params
          rpt.SetParameterValue(pair.Key, pair.Value)
        Next
      End If

      Me.CrystalReportViewer1.ReportSource = rpt
      Me.CrystalReportViewer1.EnableDrillDown = False 'サブレポート参照不可とする
      Me.Cursor = Cursors.Default
    End Sub
#End Region
  End Class
End Namespace

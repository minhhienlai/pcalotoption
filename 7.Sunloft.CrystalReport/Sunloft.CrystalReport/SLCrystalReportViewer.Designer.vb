﻿Namespace Windows.Forms

  <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
  Partial Class SLCrystalReportViewer
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      Try
        If disposing AndAlso components IsNot Nothing Then
          components.Dispose()
        End If
      Finally
        MyBase.Dispose(disposing)
      End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
      Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
      Me.SuspendLayout()
      '
      'CrystalReportViewer1
      '
      Me.CrystalReportViewer1.ActiveViewIndex = -1
      Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
      Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
      Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
      Me.CrystalReportViewer1.SelectionFormula = ""
      Me.CrystalReportViewer1.ShowGroupTreeButton = False
      Me.CrystalReportViewer1.ShowLogo = False
      Me.CrystalReportViewer1.ShowParameterPanelButton = False
      Me.CrystalReportViewer1.ShowRefreshButton = False
      Me.CrystalReportViewer1.Size = New System.Drawing.Size(681, 563)
      Me.CrystalReportViewer1.TabIndex = 0
      Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
      Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
      '
      'SLCrystalReportViewer
      '
      Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
      Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
      Me.ClientSize = New System.Drawing.Size(681, 563)
      Me.Controls.Add(Me.CrystalReportViewer1)
      Me.Name = "SLCrystalReportViewer"
      Me.Text = "frmViewer1"
      Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
      Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
  End Class

End Namespace

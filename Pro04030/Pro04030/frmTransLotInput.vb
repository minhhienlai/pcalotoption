﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports Sunloft.PCAIF
Imports PCA.Controls
Imports PCA.TSC.Kon.Tools
Imports System.ComponentModel
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity.Defines


Public Class frmTransLotInput
  Property connector As IIntegratedApplication = Nothing

  'Table Column's width Constants
  Private Const DATE_LENGTH As Short = 13
  Private Const SLIPNO_LENGTH As Short = 10
  Private Const LOTNO_LENGTH As Short = 20
  Private Const NUMBER_LENGTH As Integer = 10
  Private Const QUANTITY_LENGTH As Integer = 12
  Private Const SUP_CODE_LENGTH As Integer = 15
  Private Const SUP_NAME_LENGTH As Integer = 40
  Private Const TYPE_LENGTH As Integer = 10
  Private Const ID_LENGTH As Integer = 10

  'Save to common lib?
  Private SL_ZDNClass As SL_ZDN
  Private SL_WHTClass As SL_WHT
  Private SL_ZHKClass As SL_ZHK
  Private SL_LMBClass As SL_LMB
  Private MasterDialog As SLMasterSearchDialog
  Private m_AMS1Class As AMS1

  Private m_IntHeaderID As Integer
  Private m_IntSlipNo As Integer
  Private m_IsF12CloseFlag As Boolean = True
  Private m_IsClosingFlag As Boolean = False
  Private m_IntRowIndex As Integer = -1
  Private m_IntRowCount As Integer = 0
  Private m_IsAllocatedSlip As Boolean = False

  Private m_conf As Sunloft.PcaConfig
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private m_arReadNumber(1) As Decimal
  Private m_arReadQuantity(1) As Decimal
  Private m_arDiffNumber(1) As Decimal
  Private m_arDiffQuantity(1) As Decimal

  Private m_isSlipMoving As Boolean = False
  Private m_strCurrentCode As String = String.Empty
  Private m_isSlipChanged As Boolean = False
  Private m_isMasterSearched As Boolean = False

#Region "Initialize Methods"

  Public Sub New()
    Try
      InitializeComponent()

      'Connect to PCA
      Dim strParam As String = Command()
      If strParam <> String.empty Then
        m_appClass.ConnectToPCA(strParam)
      Else
        m_appClass.ConnectToPCA()
      End If
      connector = m_appClass.connector

      m_conf = New Sunloft.PcaConfig

      PcaFunctionCommandRefer.Enabled = False
      PcaFunctionCommandCopy.Enabled = False
      PcaFunctionCommandNext.Enabled = False

      ltxtNumber.Text = CStr(0)
      ltxtQuantity.Text = CStr(0)

      InitControls(, False)
      InitTable()
      tblTable.Enabled = False

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitTable()
    Try
      With tblTable
        .DataRowCount = 99

        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader()
        InitTableBody()

        .UpdateTable()

        .BodyColumns.DefaultStyle.BackColor = Color.White
        .BodyColumns.DefaultAltStyle.BackColor = Color.White

      End With
    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try

  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    Try
      Dim configXMLDoc As New System.Xml.XmlDocument

      MyBase.OnLoad(e)
      Dim intSlipNo As Integer = 0
      If m_appClass.argumentContent <> String.empty AndAlso Integer.TryParse(m_appClass.argumentContent, intSlipNo) Then
        If DisplaySlipByNo(intSlipNo) Then
          setTransNo.Enabled = True
          SetUpEditMode(m_IsAllocatedSlip)
        Else
          setTransNo.CodeText = String.empty
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  ''' <summary>Init Table Header when create Table</summary>
  ''' <remarks></remarks>
  ''' 
  Private Sub InitTableHeader()
    Try
      tblTable.HeadTextHeight = 1
      tblTable.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
      tblTable.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell
      Dim strHeaderName As String = String.Empty

      column = New PcaColumn(ColumnName.TargetDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.TargetDate)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.SlipNo, SLIPNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.SlipNo)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.LotNo, LOTNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.LotNo)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.SupLotNo, LOTNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.SupLotNo)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.Number, NUMBER_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Number)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.Quantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Quantity)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.MoveNumber, NUMBER_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.MoveNumber)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.MoveQuantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.MoveQuantity)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      If SL_LMBClass.sl_lmb_sdlflg = 1 Then
        column = New PcaColumn(ColumnName.Shukka, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareFixTextCell(ColumnName.Shukka)
        column.Cells.Add(cell)
        tblTable.HeadColumns.Add(column)
      End If

      If SL_LMBClass.sl_lmb_ubdflg = 1 Then
        column = New PcaColumn(ColumnName.Shiyou, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareFixTextCell(ColumnName.Shiyou)
        column.Cells.Add(cell)
        tblTable.HeadColumns.Add(column)
      End If

      If SL_LMBClass.sl_lmb_bbdflg = 1 Then
        column = New PcaColumn(ColumnName.Shoumi, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareFixTextCell(ColumnName.Shoumi)
        column.Cells.Add(cell)
        tblTable.HeadColumns.Add(column)
      End If

      If SL_LMBClass.sl_lmb_expflg = 1 Then
        column = New PcaColumn(ColumnName.Shouhi, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareFixTextCell(ColumnName.Shouhi)
        column.Cells.Add(cell)
        tblTable.HeadColumns.Add(column)
      End If

      strHeaderName = Replace(ColumnName.SupCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_SupplierLength + 2 + SUP_NAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(strHeaderName)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.Type, TYPE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Type)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitPrice, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.UnitPrice)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Invisible column (From Lot Details screen)      
      InitHeaderColumn(column, cell, 0, ColumnName.LotID1, ColumnName.LotID1)
      InitHeaderColumn(column, cell, 0, ColumnName.LotID2, ColumnName.LotID2)
      InitHeaderColumn(column, cell, 0, ColumnName.LotID3, ColumnName.LotID3)
      InitHeaderColumn(column, cell, 0, ColumnName.LotID4, ColumnName.LotID4)
      InitHeaderColumn(column, cell, 0, ColumnName.LotID5, ColumnName.LotID5)
      InitHeaderColumn(column, cell, 0, ColumnName.LotID6, ColumnName.LotID6)
      InitHeaderColumn(column, cell, 0, ColumnName.LotID7, ColumnName.LotID7)
      InitHeaderColumn(column, cell, 0, ColumnName.LotID8, ColumnName.LotID8)
      InitHeaderColumn(column, cell, 0, ColumnName.LotID9, ColumnName.LotID9)
      InitHeaderColumn(column, cell, 0, ColumnName.LotID10, ColumnName.LotID10)

      InitHeaderColumn(column, cell, 0, ColumnName.LotName1, ColumnName.LotName1)
      InitHeaderColumn(column, cell, 0, ColumnName.LotName2, ColumnName.LotName2)
      InitHeaderColumn(column, cell, 0, ColumnName.LotName3, ColumnName.LotName3)
      InitHeaderColumn(column, cell, 0, ColumnName.LotName4, ColumnName.LotName4)
      InitHeaderColumn(column, cell, 0, ColumnName.LotName5, ColumnName.LotName5)
      InitHeaderColumn(column, cell, 0, ColumnName.LotName6, ColumnName.LotName6)
      InitHeaderColumn(column, cell, 0, ColumnName.LotName7, ColumnName.LotName7)
      InitHeaderColumn(column, cell, 0, ColumnName.LotName8, ColumnName.LotName8)
      InitHeaderColumn(column, cell, 0, ColumnName.LotName9, ColumnName.LotName9)
      InitHeaderColumn(column, cell, 0, ColumnName.LotName10, ColumnName.LotName10)

      InitHeaderColumn(column, cell, 0, ColumnName.Rate, ColumnName.Rate)
      InitHeaderColumn(column, cell, 0, ColumnName.ManageNo, ColumnName.ManageNo)
      InitHeaderColumn(column, cell, 0, ColumnName.SystemLotNo, ColumnName.SystemLotNo)
      InitHeaderColumn(column, cell, 0, ColumnName.StockID, ColumnName.StockID)
      InitHeaderColumn(column, cell, 0, ColumnName.FromStockID, ColumnName.FromStockID)
      InitHeaderColumn(column, cell, 0, ColumnName.CaseNumber, ColumnName.CaseNumber)
      InitHeaderColumn(column, cell, 0, ColumnName.CaseNumber, ColumnName.CaseNumber)
      InitHeaderColumn(column, cell, 0, ColumnName.PrevNum, ColumnName.PrevNum)
      InitHeaderColumn(column, cell, 0, ColumnName.PrevQty, ColumnName.PrevQty)


    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  ''' <summary>Init a Header column</summary>
  ''' <param name="column">PcaColumnControl</param>
  ''' <param name="cell">PcaCellControl</param>
  ''' <param name="columnWidth">columnWidth</param>
  ''' <param name="cellName">cellName</param>
  ''' <param name="columnName">columnName</param>
  ''' <remarks></remarks>
  ''' 
  Private Sub InitHeaderColumn(ByVal column As PcaColumn, ByVal cell As PcaCell, ByVal columnWidth As Integer, ByVal cellName As String, ByVal columnName As String)
    column = New PcaColumn(columnName, columnWidth)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(cellName, columnWidth, 3, 0.0F, 0.0F)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)
  End Sub


  ''' <summary>InitTableBody when create Table</summary>
  ''' <remarks></remarks>
  Private Sub InitTableBody()
    Try
      tblTable.BodyTextHeight = 1

      tblTable.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
      tblTable.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
      tblTable.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
      tblTable.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell
      Dim strHeaderName As String = String.Empty

      column = New PcaColumn(ColumnName.TargetDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.TargetDate, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.TargetDate)
      cell.EditMode = False
      cell.CellStyle.Alignment = StringAlignment.Center
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.SlipNo, SLIPNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.SlipNo, SLIPNO_LENGTH, 1, 0.0F, 0.0F, ColumnName.SlipNo)
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.LotNo, LOTNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.LotNo, LOTNO_LENGTH, 1, 0.0F, 0.0F, ColumnName.LotNo)
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.SupLotNo, LOTNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.SupLotNo, LOTNO_LENGTH, 1, 0.0F, 0.0F, ColumnName.SupLotNo)
      cell.EditMode = True
      cell.Enabled = True
      cell.TabStop = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.Number, NUMBER_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.Number, NUMBER_LENGTH, 1, 0.0F, 0.0F, ColumnName.Number)
      cell.EditMode = False
      cell.FormatComma = True
      cell.Enabled = False
      cell.CellStyle.Alignment = StringAlignment.Far
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.Quantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.Quantity, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.Quantity)
      cell.EditMode = False
      cell.Enabled = False
      cell.FormatComma = True
      cell.CellStyle.Alignment = StringAlignment.Far
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.MoveNumber, NUMBER_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.MoveNumber, NUMBER_LENGTH, 1, 0.0F, 0.0F, ColumnName.MoveNumber)
      cell.EditMode = True
      cell.Enabled = True
      cell.FormatComma = True
      cell.CellStyle.Alignment = StringAlignment.Far
      cell.DecimalDigit = 0
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.MoveQuantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.MoveQuantity, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.MoveQuantity)
      cell.EditMode = True
      cell.Enabled = True
      cell.FormatComma = True
      cell.CellStyle.Alignment = StringAlignment.Far
      cell.DecimalDigit = 4
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      If SL_LMBClass.sl_lmb_sdlflg = 1 Then
        column = New PcaColumn(ColumnName.Shukka, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareTextCell(ColumnName.Shukka, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Shukka)
        cell.CellStyle.Alignment = StringAlignment.Center
        cell.EditMode = False
        column.Cells.Add(cell)
        tblTable.BodyColumns.Add(column)
      End If

      If SL_LMBClass.sl_lmb_ubdflg = 1 Then
        column = New PcaColumn(ColumnName.Shiyou, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareTextCell(ColumnName.Shiyou, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Shiyou)
        cell.CellStyle.Alignment = StringAlignment.Center
        cell.EditMode = False
        column.Cells.Add(cell)
        tblTable.BodyColumns.Add(column)
      End If

      If SL_LMBClass.sl_lmb_bbdflg = 1 Then
        column = New PcaColumn(ColumnName.Shoumi, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareTextCell(ColumnName.Shoumi, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Shoumi)
        cell.CellStyle.Alignment = StringAlignment.Center
        cell.EditMode = False
        column.Cells.Add(cell)
        tblTable.BodyColumns.Add(column)
      End If

      If SL_LMBClass.sl_lmb_expflg = 1 Then
        column = New PcaColumn(ColumnName.Shouhi, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareTextCell(ColumnName.Shouhi, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Shouhi)
        cell.CellStyle.Alignment = StringAlignment.Center
        cell.EditMode = False
        column.Cells.Add(cell)
        tblTable.BodyColumns.Add(column)
      End If

      strHeaderName = Replace(ColumnName.SupCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_SupplierLength + 2 + SUP_NAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.SupCode, m_AMS1Class.ams1_SupplierLength + 2, 1, 0.0F, 0.0F, strHeaderName)
      cell.EditMode = False
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.SupName, SUP_NAME_LENGTH, 1, m_AMS1Class.ams1_SupplierLength + 2, 0.0F, strHeaderName)
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.Type, TYPE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.Type, TYPE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Type)
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitPrice, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.UnitPrice, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.UnitPrice)
      cell.CellStyle.Alignment = StringAlignment.Far
      cell.EditMode = False

      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Invisible column from Lot Details screen
      InitBodyColumnText(column, cell, ColumnName.LotID1, ColumnName.LotID1, 0, 1, ColumnName.LotID1, False)
      InitBodyColumnText(column, cell, ColumnName.LotID2, ColumnName.LotID2, 0, 1, ColumnName.LotID2, False)
      InitBodyColumnText(column, cell, ColumnName.LotID3, ColumnName.LotID3, 0, 1, ColumnName.LotID3, False)
      InitBodyColumnText(column, cell, ColumnName.LotID4, ColumnName.LotID4, 0, 1, ColumnName.LotID4, False)
      InitBodyColumnText(column, cell, ColumnName.LotID5, ColumnName.LotID5, 0, 1, ColumnName.LotID5, False)
      InitBodyColumnText(column, cell, ColumnName.LotID6, ColumnName.LotID6, 0, 1, ColumnName.LotID6, False)
      InitBodyColumnText(column, cell, ColumnName.LotID7, ColumnName.LotID7, 0, 1, ColumnName.LotID7, False)
      InitBodyColumnText(column, cell, ColumnName.LotID8, ColumnName.LotID8, 0, 1, ColumnName.LotID8, False)
      InitBodyColumnText(column, cell, ColumnName.LotID9, ColumnName.LotID9, 0, 1, ColumnName.LotID9, False)
      InitBodyColumnText(column, cell, ColumnName.LotID10, ColumnName.LotID10, 0, 1, ColumnName.LotID10, False)

      InitBodyColumnText(column, cell, ColumnName.LotName1, ColumnName.LotName1, 0, 1, ColumnName.LotName1, False)
      InitBodyColumnText(column, cell, ColumnName.LotName2, ColumnName.LotName2, 0, 1, ColumnName.LotName2, False)
      InitBodyColumnText(column, cell, ColumnName.LotName3, ColumnName.LotName3, 0, 1, ColumnName.LotName3, False)
      InitBodyColumnText(column, cell, ColumnName.LotName4, ColumnName.LotName4, 0, 1, ColumnName.LotName4, False)
      InitBodyColumnText(column, cell, ColumnName.LotName5, ColumnName.LotName5, 0, 1, ColumnName.LotName5, False)
      InitBodyColumnText(column, cell, ColumnName.LotName6, ColumnName.LotName6, 0, 1, ColumnName.LotName6, False)
      InitBodyColumnText(column, cell, ColumnName.LotName7, ColumnName.LotName7, 0, 1, ColumnName.LotName7, False)
      InitBodyColumnText(column, cell, ColumnName.LotName8, ColumnName.LotName8, 0, 1, ColumnName.LotName8, False)
      InitBodyColumnText(column, cell, ColumnName.LotName9, ColumnName.LotName9, 0, 1, ColumnName.LotName9, False)
      InitBodyColumnText(column, cell, ColumnName.LotName10, ColumnName.LotName10, 0, 1, ColumnName.LotName10, False)

      InitBodyColumnText(column, cell, ColumnName.Rate, ColumnName.Rate, 0, 1, ColumnName.Rate, False)
      InitBodyColumnText(column, cell, ColumnName.ManageNo, ColumnName.ManageNo, 0, 1, ColumnName.ManageNo, False)
      InitBodyColumnText(column, cell, ColumnName.SystemLotNo, ColumnName.SystemLotNo, 0, 1, ColumnName.SystemLotNo, False)
      InitBodyColumnText(column, cell, ColumnName.StockID, ColumnName.StockID, 0, 1, ColumnName.StockID, False)
      InitBodyColumnText(column, cell, ColumnName.FromStockID, ColumnName.FromStockID, 0, 1, ColumnName.FromStockID, False)
      InitBodyColumnText(column, cell, ColumnName.CaseNumber, ColumnName.CaseNumber, 0, 1, ColumnName.CaseNumber, False)
      InitBodyColumnText(column, cell, ColumnName.PrevNum, ColumnName.PrevNum, 0, 1, ColumnName.PrevNum, False)
      InitBodyColumnText(column, cell, ColumnName.PrevQty, ColumnName.PrevQty, 0, 1, ColumnName.PrevQty, False)

    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  ''' <summary>Init Text column when create Table</summary>
  ''' <param name="column"></param>
  ''' <param name="cell"></param>
  ''' <param name="columnName"></param>
  ''' <param name="cellname"></param>
  ''' <param name="cellwidth"></param>
  ''' <param name="cellHeight"></param>
  ''' <param name="headerName"></param>
  ''' <param name="editable"></param>
  ''' <remarks></remarks>
  Private Sub InitBodyColumnText(column As PcaColumn, cell As PcaCell, columnName As String, cellname As String, cellwidth As Short, cellHeight As Short, headerName As String, Optional ByVal editable As Boolean = True)
    'Create column text
    column = New PcaColumn(columnName, cellwidth)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(cellname, cellwidth, cellHeight, 0, 0, headerName)
    cell.EditMode = editable
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)
  End Sub

#End Region

#Region "Private Methods [Register,Modify,Delete]"

  ''' <summary>Initialize Controls</summary>
  ''' <remarks></remarks>
  Private Sub InitControls(Optional ByVal isClearData As Boolean = True, Optional ByVal isClearParameters As Boolean = True)
    Try
      HeaderLabel.State = LabelStateType.New
      If isClearData Then ClearData()

      setTransNo.CodeText = String.Empty
      m_isSlipChanged = False

      SL_WHTClass = New SL_WHT(connector)
      SL_ZDNClass = New SL_ZDN(connector)
      SL_ZHKClass = New SL_ZHK(connector)
      SL_LMBClass = New SL_LMB(connector)
      m_AMS1Class = New AMS1(connector)
      MasterDialog = New SLMasterSearchDialog(connector)
      SL_LMBClass.ReadOnlyRow()
      m_AMS1Class.ReadAll()

      setTransNo.Enabled = True
      tblTable.Enabled = True
      ldtMoveDate.DayEnabled = True
      ldtMoveDate.MonthEnabled = True
      ldtMoveDate.YearEnabled = True
      ldtMoveDate.ShowCalendar = True
      ldtArrivalDate.DayEnabled = True
      ldtArrivalDate.MonthEnabled = True
      ldtArrivalDate.YearEnabled = True
      ldtArrivalDate.ShowCalendar = True
      setProduct.Enabled = True
      setFromWarehouse.Enabled = True
      setToWarehouse.Enabled = True

      'FunctionKey Enabled Setting
      PcaFunctionCommandPrevious.Enabled = True 'F2
      PcaFunctionCommandNext.Enabled = False 'F3
      PcaFunctionCommandSlipSearch.Enabled = True 'F6
      PcaFunctionCommandRefer.Enabled = True 'F8
      PcaFunctionCommandCopy.Enabled = False 'F11

      'ToolStripButton Enabled Setting

      ToolStripButtonPrevious.Enabled = True
      ToolStripButtonNext.Enabled = False
      ToolStripButtonReset.Enabled = True
      ToolStripButtonDelete.Enabled = False
      ToolStripButtonCopy.Enabled = False

      'MenuBar Enabled Setting
      ToolStripMenuItemPrevious.Enabled = True
      ToolStripMenuItemNext.Enabled = False
      ToolStripMenuItemReset.Enabled = True
      ToolStripMenuItemDelete.Enabled = False
      ToolStripMenuItemCopy.Enabled = False

      'Controls String length setting
      setProduct.LimitLength = m_AMS1Class.ams1_ProductLength
      setFromWarehouse.LimitLength = m_AMS1Class.ams1_WarehouseLength
      setToWarehouse.LimitLength = m_AMS1Class.ams1_WarehouseLength
      setMemoCode.LimitLength = m_AMS1Class.ams1_MemoLength

      If isClearParameters Then InitClearParameters()

    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  ''' <summary>InitClearParameters</summary>
  ''' <remarks></remarks>
  Private Sub InitClearParameters()
    m_IntHeaderID = 0
    m_IntSlipNo = 0
    m_IsF12CloseFlag = True
    m_IsClosingFlag = False
    m_IntRowIndex = 0
    m_IntRowCount = 0
    tblTable.Enabled = False
  End Sub

  ''' <summary>SetUpEditMode</summary>
  ''' <param name="isAllocatedSlip"></param>
  ''' <remarks>Set to edit mode</remarks>
  Private Sub SetUpEditMode(Optional ByVal isAllocatedSlip As Boolean = False)
    HeaderLabel.State = LabelStateType.Modify
    setTransNo.Enabled = False
    m_IsF12CloseFlag = False
    tblTable.Enabled = True
    m_isSlipChanged = False
    If isAllocatedSlip Then
      tblTable.Enabled = False
      ldtMoveDate.DayEnabled = False
      ldtMoveDate.MonthEnabled = False
      ldtMoveDate.YearEnabled = False
      ldtMoveDate.ShowCalendar = False
      ldtArrivalDate.DayEnabled = False
      ldtArrivalDate.MonthEnabled = False
      ldtArrivalDate.YearEnabled = False
      ldtArrivalDate.ShowCalendar = False
      setProduct.Enabled = False
      setFromWarehouse.Enabled = False
      setToWarehouse.Enabled = False
    End If

    'FunctionKey Enabled Setting
    PcaFunctionCommandPrevious.Enabled = True 'F2
    PcaFunctionCommandNext.Enabled = True 'F3
    PcaFunctionCommandSlipSearch.Enabled = True 'F6
    PcaFunctionCommandRefer.Enabled = False 'F8
    PcaFunctionCommandCopy.Enabled = True 'F11

    'ToolStripButton Enabled Setting
    ToolStripButtonPrevious.Enabled = True
    ToolStripButtonNext.Enabled = True
    ToolStripButtonReset.Enabled = True
    ToolStripButtonDelete.Enabled = True
    ToolStripButtonCopy.Enabled = True

    'MenuBar Enabled Setting
    ToolStripMenuItemPrevious.Enabled = True
    ToolStripMenuItemNext.Enabled = True
    ToolStripMenuItemReset.Enabled = True
    ToolStripMenuItemDelete.Enabled = True
    ToolStripMenuItemCopy.Enabled = True
    ToolStripMenuItemRefer.Enabled = False

    ldtMoveDate.Focus()
  End Sub

  ''' <summary>
  ''' Insert Data when press F12
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function Insert() As Boolean
    Dim isSuccess As Boolean = True
    Dim session As ICustomSession

    If Not ValidateInput() Then Return False
    Try
      session = connector.CreateTransactionalSession

      isSuccess = InsertHeader(session)
      If isSuccess Then isSuccess = InsertDetails(session)
      If isSuccess Then

        session.Commit()
        session.Dispose()

        SLCmnFunction.ShowToolTip("伝票番号：" & SL_WHTClass.sl_wht_denno & " で登録されました。", _
                                  SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
      Else
        session.Rollback()
        session.Dispose()
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function

  ''' <summary>
  ''' Update Data when press F12
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shadows Function Update() As Boolean
    Dim isSuccess As Boolean = True
    Dim session As ICustomSession

    If Not ValidateInput() Then Return False
    Try
      session = connector.CreateTransactionalSession

      isSuccess = UpdateHeader(session)
      If isSuccess Then isSuccess = UpdateDetails(session)
      If isSuccess Then
        session.Commit()
        session.Dispose()
      Else
        session.Rollback()
        session.Dispose()
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function



  ''' <summary>If cell is not empty, insert cell text value to table</summary>
  ''' <param name="session">Session</param>
  ''' <returns>none</returns>
  ''' <remarks></remarks>
  Private Function InsertHeader(ByRef session As ICustomSession) As Boolean
    Try
      SetDBHeaderValues()

      'Save class data to database
      Return SL_WHTClass.Insert(session)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>If cell is not empty, update cell text value to table</summary>
  ''' <param name="session">Session</param>
  ''' <returns>none</returns>
  ''' <remarks></remarks>
  Private Function UpdateHeader(ByRef session As ICustomSession) As Boolean
    Try
      SetDBHeaderValues()

      'Save class data to database
      Return SL_WHTClass.Update(session)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  '''  Insert Details Data (Table)
  ''' </summary>
  ''' <param name="session">Session</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ''' 
  Private Function InsertDetails(ByRef session As ICustomSession) As Boolean
    Dim intLpc As Integer = 0
    Dim intSEQ As Integer = 0

    m_IntHeaderID = SL_WHTClass.sl_wht_id

    Try
      intSEQ = 1

      For intLpc = 0 To tblTable.DataRowCount

        If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity)) AndAlso CInt(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity)) > 0 Then

          '移動元データの在庫更新
          SetDBZDNValues(intLpc, True)

          If tblTable.GetCellValue(intLpc, ColumnName.FromStockID) Is Nothing OrElse CInt(tblTable.GetCellValue(intLpc, ColumnName.FromStockID)) = 0 Then
            If Not SL_ZDNClass.UpdateNumberQuantity(False, session) Then Return False
          Else
            If Not SL_ZDNClass.UpdateNumberQuantity(True, session) Then Return False
          End If

          '移動元データ在庫引当
          SetDBZHKValues(intLpc, intSEQ, True)
          If Not SL_ZHKClass.Insert(session) Then Return False

          '移動先データ在庫作成
          SetDBZDNValues(intLpc, False)
          If Not SL_ZDNClass.RegisterInventoryFromTrans(intSEQ, session) Then Return False

          '移動先データ在庫受払
          intSEQ += 1
          SetDBZHKValues(intLpc, intSEQ, False)
          SL_ZHKClass.sl_zhk_zdn_id = SL_ZDNClass.sl_zdn_id
          If Not SL_ZHKClass.Insert(session) Then Return False

          intSEQ += 1
        End If

      Next

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  ''' Update Details Data (Table)
  ''' </summary>
  ''' <param name="session">Session</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateDetails(ByRef session As ICustomSession) As Boolean
    Dim intLpc As Integer = 0
    Dim intSEQ As Integer = 0

    m_IntHeaderID = SL_WHTClass.sl_wht_id

    Try
      intSEQ = 1

      If Not DeleteData(m_IntHeaderID, SL_WHTClass.sl_wht_denno, True, session) Then Return False

      For intLpc = 0 To tblTable.DataRowCount

        If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity)) AndAlso CInt(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity)) > 0 Then

          '移動元データの更新
          SetDBZDNValues(intLpc, True)

          If tblTable.GetCellValue(intLpc, ColumnName.FromStockID) Is Nothing OrElse CInt(tblTable.GetCellValue(intLpc, ColumnName.FromStockID)) = 0 Then
            If Not SL_ZDNClass.UpdateNumberQuantity(False, session) Then Return False
          Else
            If Not SL_ZDNClass.UpdateNumberQuantity(True, session) Then Return False
          End If

          '移動元データ在庫引当
          SetDBZHKValues(intLpc, intSEQ, True)
          If Not SL_ZHKClass.Insert(session) Then Return False

          '移動先の在庫登録
          SetDBZDNValues(intLpc, False)
          If Not SL_ZDNClass.RegisterInventoryFromTrans(intSEQ, session) Then Return False

          '移動先データ在庫受払
          intSEQ += 1
          SetDBZHKValues(intLpc, intSEQ, False)
          SL_ZHKClass.sl_zhk_zdn_id = SL_ZDNClass.sl_zdn_id
          If Not SL_ZHKClass.Insert(session) Then Return False
          intSEQ += 1
        End If

      Next

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>Delete Data (In Edit mode, Delete -> Insert new data. Or when user chooses to delete data)</summary>
  ''' <param name="intHeaderID"></param>
  ''' <param name="intSlipNo"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DeleteData(ByVal intHeaderID As Integer, ByVal intSlipNo As Integer, ByVal isDetailOnly As Boolean, Optional ByRef argSession As ICustomSession = Nothing) As Boolean
    Dim session As ICustomSession
    Dim isSuccess As Boolean = True
    Dim intDetailID As Integer = 0
    Dim intLpc As Integer = 0
    Try

      If isDetailOnly Then
        session = argSession
        isSuccess = True
      Else
        session = connector.CreateTransactionalSession
        isSuccess = DeleteHeader(intHeaderID, session)
      End If

      If isSuccess Then
        For intLpc = 0 To m_IntRowCount
          If tblTable.GetCellValue(intLpc, ColumnName.FromStockID) Is Nothing OrElse CInt(tblTable.GetCellValue(intLpc, ColumnName.FromStockID)) = 0 Then
            isSuccess = True
          Else
            isSuccess = DeleteDetails(intHeaderID, intSlipNo, CInt(tblTable.GetCellValue(intLpc, ColumnName.StockID)), CInt(tblTable.GetCellValue(intLpc, ColumnName.FromStockID)), intLpc, session)
          End If

          If Not isSuccess Then Exit For
        Next
      End If

      If Not isDetailOnly Then
        If isSuccess Then
          session.Commit()
          session.Dispose()
        Else
          session.Rollback()
          session.Dispose()
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)

      isSuccess = False
    Finally
    End Try
    Return isSuccess
  End Function

  ''' <summary>
  ''' Delete Header Data (Table)
  ''' </summary>
  ''' <param name="intHeaderID">HeaderID</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DeleteHeader(ByVal intHeaderID As Integer, session As ICustomSession) As Boolean
    Try

      Return SL_WHTClass.Delete(session)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>Delete Details Data (Table)</summary>
  ''' <param name="intHeaderID"></param>
  ''' <param name="intSlipNo"></param>
  ''' <param name="intStockID"></param>
  ''' <param name="intRelayStockID"></param>
  ''' <param name="intCount"></param>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DeleteDetails(ByVal intHeaderID As Integer, ByVal intSlipNo As Integer, ByVal intStockID As Integer, ByVal intRelayStockID As Integer, _
                                 ByVal intCount As Integer, session As ICustomSession) As Boolean
    Try
      With SL_ZHKClass
        .sl_zhk_zdn_id = intStockID
        .sl_zhk_datakbn = SLConstants.SL_ZDNSlipType.TransWarehousing
        .sl_zhk_dataid = intHeaderID
        If Not .DeleteInventory(session, True, False) Then Return False
      End With

      With SL_ZDNClass
        .sl_zdn_dataid = intHeaderID
        .sl_zdn_datakbn = SLConstants.SL_ZDNSlipType.TransWarehousing
        .sl_zdn_relay_zdnid = intRelayStockID
        If Not .Delete(session) Then Return False

        .sl_zdn_nyukohonsu = CType(tblTable.GetCellValue(intCount, ColumnName.PrevNum), Integer) * -1
        .sl_zdn_nyukosuryo = CType(tblTable.GetCellValue(intCount, ColumnName.PrevQty), Decimal) * -1
        If Not .UpdateNumberQuantity(True, session) Then Return False
      End With

      Return True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function
  ''' <summary>Setting DB Values[SL_WHT]</summary>
  Private Sub SetDBHeaderValues()
    Try
      With SL_WHTClass

        If setTransNo.CodeText.Trim <> String.Empty Then .sl_wht_denno = CInt(setTransNo.CodeText)
        .sl_wht_denno2 = ltxtTransNo2.Text
        .sl_wht_date = ldtMoveDate.IntDate
        .sl_wht_dlvdate = ldtArrivalDate.IntDate

        .sl_wht_scd = setProduct.CodeText
        .sl_wht_soukof = setFromWarehouse.CodeText
        .sl_wht_soukot = setToWarehouse.CodeText

        .sl_wht_suryo = CDec(ltxtQuantity.Text)
        .sl_wht_honsu = CInt(ltxtNumber.Text)
        .sl_wht_tekcd = setMemoCode.CodeText
        .sl_wht_tekmei = setMemoContent.CodeText

        .sl_wht_insuser = connector.UserId
        .sl_wht_insdate = Now
        .sl_wht_upduser = connector.UserId
        .sl_wht_upddate = Now
      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>Setting DB Values[SL_ZHK]</summary>
  ''' <param name="intLpc">Current Row</param>
  ''' <param name="intSEQ">Current SEQ</param>
  Private Sub SetDBZHKValues(ByVal intLpc As Integer, ByVal intSEQ As Integer, ByVal isMoveFrom As Boolean)
    Try
      Dim intRegNumber As Integer = 0
      Dim decRegQuantity As Decimal = 0D

      Integer.TryParse(tblTable.GetCellValue(intLpc, ColumnName.MoveNumber).ToString, intRegNumber)
      Decimal.TryParse(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity).ToString, decRegQuantity)
      With SL_ZHKClass

        .sl_zhk_datakbn = SLConstants.SL_ZHKSlipType.Transfer
        .sl_zhk_dataid = m_IntHeaderID
        .sl_zhk_seq = intSEQ
        If isMoveFrom Then
          .sl_zhk_date = ldtMoveDate.IntDate
        Else
          .sl_zhk_date = ldtArrivalDate.IntDate
        End If
        .sl_zhk_zdn_id = CInt(tblTable.GetCellValue(intLpc, ColumnName.StockID))
        .sl_zhk_honsu = intRegNumber
        .sl_zhk_suryo = decRegQuantity
        .sl_zhk_insuser = connector.UserId
        .sl_zhk_insdate = Now
        .sl_zhk_upduser = Nothing
        .sl_zhk_upddate = Nothing
      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>Setting DB Values[SL_ZDN]</summary>
  ''' <param name="intLpc">Current Row</param>
  Private Sub SetDBZDNValues(ByVal intLpc As Integer, ByVal isMoveFrom As Boolean)
    Try

      Dim intRegNumber As Integer = 0
      Dim decRegQuantity As Decimal = 0D

      Integer.TryParse(tblTable.GetCellValue(intLpc, ColumnName.MoveNumber).ToString, intRegNumber)
      Decimal.TryParse(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity).ToString, decRegQuantity)

      With SL_ZDNClass
        .sl_zdn_id = CInt(tblTable.GetCellValue(intLpc, ColumnName.StockID))
        If isMoveFrom Then
          .sl_zdn_nyukohonsu = intRegNumber
          .sl_zdn_nyukosuryo = decRegQuantity
        Else

          .sl_zdn_datakbn = SLConstants.SL_ZDNSlipType.TransWarehousing
          .sl_zdn_dataid = m_IntHeaderID
          .sl_zdn_ulotno = tblTable.GetCellValue(intLpc, ColumnName.LotNo).ToString
          .sl_zdn_alotno = tblTable.GetCellValue(intLpc, ColumnName.SupLotNo).ToString
          .sl_zdn_scd = setProduct.CodeText
          .sl_zdn_souko = setToWarehouse.CodeText
          .sl_zdn_uribi = ldtArrivalDate.IntDate
          .sl_zdn_honsu = intRegNumber
          .sl_zdn_suryo = decRegQuantity
          .sl_zdn_nyukohonsu = intRegNumber
          .sl_zdn_nyukosuryo = decRegQuantity
          .sl_zdn_tanka = CDec(tblTable.GetCellValue(intLpc, ColumnName.UnitPrice))
          .sl_zdn_tax = CInt(tblTable.GetCellValue(intLpc, ColumnName.Rate))
          .sl_zdn_kanrino = tblTable.GetCellValue(intLpc, ColumnName.ManageNo).ToString
          If tblTable.GetCellValue(intLpc, ColumnName.Shukka) IsNot Nothing Then
            .sl_zdn_sdldate = SLCmnFunction.ConvertDatestringToInteger(tblTable.GetCellValue(intLpc, ColumnName.Shukka).ToString)
          End If

          If tblTable.GetCellValue(intLpc, ColumnName.Shiyou) IsNot Nothing Then
            .sl_zdn_sdldate = SLCmnFunction.ConvertDatestringToInteger(tblTable.GetCellValue(intLpc, ColumnName.Shiyou).ToString)
          End If
          If tblTable.GetCellValue(intLpc, ColumnName.Shoumi) IsNot Nothing Then
            .sl_zdn_sdldate = SLCmnFunction.ConvertDatestringToInteger(tblTable.GetCellValue(intLpc, ColumnName.Shoumi).ToString)
          End If
          If tblTable.GetCellValue(intLpc, ColumnName.Shouhi) IsNot Nothing Then
            .sl_zdn_sdldate = SLCmnFunction.ConvertDatestringToInteger(tblTable.GetCellValue(intLpc, ColumnName.Shouhi).ToString)
          End If
          .sl_zdn_ldid1 = tblTable.GetCellValue(intLpc, ColumnName.LotID1).ToString
          .sl_zdn_ldid2 = tblTable.GetCellValue(intLpc, ColumnName.LotID2).ToString
          .sl_zdn_ldid3 = tblTable.GetCellValue(intLpc, ColumnName.LotID3).ToString
          .sl_zdn_ldid4 = tblTable.GetCellValue(intLpc, ColumnName.LotID4).ToString
          .sl_zdn_ldid5 = tblTable.GetCellValue(intLpc, ColumnName.LotID5).ToString
          .sl_zdn_ldid6 = tblTable.GetCellValue(intLpc, ColumnName.LotID6).ToString
          .sl_zdn_ldid7 = tblTable.GetCellValue(intLpc, ColumnName.LotID7).ToString
          .sl_zdn_ldid8 = tblTable.GetCellValue(intLpc, ColumnName.LotID8).ToString
          .sl_zdn_ldid9 = tblTable.GetCellValue(intLpc, ColumnName.LotID9).ToString
          .sl_zdn_ldid10 = tblTable.GetCellValue(intLpc, ColumnName.LotID10).ToString
          .sl_zdn_ldmei1 = tblTable.GetCellValue(intLpc, ColumnName.LotName1).ToString
          .sl_zdn_ldmei2 = tblTable.GetCellValue(intLpc, ColumnName.LotName2).ToString
          .sl_zdn_ldmei3 = tblTable.GetCellValue(intLpc, ColumnName.LotName3).ToString
          .sl_zdn_ldmei4 = tblTable.GetCellValue(intLpc, ColumnName.LotName4).ToString
          .sl_zdn_ldmei5 = tblTable.GetCellValue(intLpc, ColumnName.LotName5).ToString
          .sl_zdn_ldmei6 = tblTable.GetCellValue(intLpc, ColumnName.LotName6).ToString
          .sl_zdn_ldmei7 = tblTable.GetCellValue(intLpc, ColumnName.LotName7).ToString
          .sl_zdn_ldmei8 = tblTable.GetCellValue(intLpc, ColumnName.LotName8).ToString
          .sl_zdn_ldmei9 = tblTable.GetCellValue(intLpc, ColumnName.LotName9).ToString
          .sl_zdn_ldmei10 = tblTable.GetCellValue(intLpc, ColumnName.LotName10).ToString
          .sl_zdn_insuser = connector.UserId
          .sl_zdn_insdate = Now
          .sl_zdn_upduser = Nothing
          .sl_zdn_upddate = Nothing
          .sl_zdn_denno = m_IntHeaderID
          If tblTable.GetCellValue(intLpc, ColumnName.FromStockID) Is Nothing OrElse CInt(tblTable.GetCellValue(intLpc, ColumnName.FromStockID)) = 0 Then
            .sl_zdn_relay_zdnid = CInt(tblTable.GetCellValue(intLpc, ColumnName.StockID))
          Else
            .sl_zdn_relay_zdnid = CInt(tblTable.GetCellValue(intLpc, ColumnName.FromStockID))
          End If
          .sl_zdn_iri = CDec(tblTable.GetCellValue(intLpc, ColumnName.CaseNumber))
        End If

      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

#End Region
#Region "Private Methods"


  ''' <summary>SearchItem</summary>
  ''' <param name="objPcaCodeSet">Target PcaCodeSet</param>
  ''' <param name="e">CancelEventsArgs</param>
  ''' <remarks>DisplaySearchScreen(SearchItemsLikePressF8)</remarks>
  Private Sub SearchItem(ByVal objPcaCodeSet As PcaCodeSet, Optional e As System.ComponentModel.CancelEventArgs = Nothing)

    Try
      m_isMasterSearched = True
      Dim location As Point = PCA.TSC.Kon.Tools.ControlTool.GetDialogLocation(objPcaCodeSet.ReferButton)
      Dim newCode As String = objPcaCodeSet.CodeText
      Dim strCurrentCode As String = String.Empty

      Select Case objPcaCodeSet.Name
        Case setProduct.Name
          newCode = MasterDialog.ShowReferSmsDialog(strCurrentCode, location)

          Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms = MasterDialog.FindBEMasterSms(newCode)

          If String.IsNullOrEmpty(newCode) Then
            If Not e Is Nothing Then e.Cancel = True
            objPcaCodeSet.Select()
            Return
          Else

            If beMasterSms.SyohinCode.Length > 0 Then
              objPcaCodeSet.CodeText = beMasterSms.SyohinCode
              objPcaCodeSet.NameText = beMasterSms.SyohinMei
              ltxtUnit.Text = beMasterSms.Tani
              Return
            Else
              If Not e Is Nothing Then e.Cancel = True
              objPcaCodeSet.Focus()
            End If
          End If
        Case setFromWarehouse.Name, setToWarehouse.Name

          newCode = MasterDialog.ShowReferSokoDialog(strCurrentCode, location)

          Dim beMasterSoko As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(newCode)

          If String.IsNullOrEmpty(newCode) Then
            If Not e Is Nothing Then e.Cancel = True
            objPcaCodeSet.Select()
            Return
          Else

            If beMasterSoko.SokoCode.Length > 0 Then
              objPcaCodeSet.CodeText = beMasterSoko.SokoCode
              objPcaCodeSet.NameText = beMasterSoko.SokoMei
              Return
            Else
              If Not e Is Nothing Then e.Cancel = True
              objPcaCodeSet.Focus()
            End If
          End If

        Case setMemoCode.Name

          newCode = MasterDialog.ShowReferTekiyoDialog(strCurrentCode, location)

          Dim beMasterMemo As PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo = MasterDialog.FindBEMasterTekiyo(KubunIdType.TekiyoKubun7, newCode)

          If String.IsNullOrEmpty(newCode) Then
            If Not e Is Nothing Then e.Cancel = True
            objPcaCodeSet.Select()
            Return
          Else

            If beMasterMemo.TekiyoCode.Length > 0 Then
              objPcaCodeSet.CodeText = beMasterMemo.TekiyoCode
              objPcaCodeSet.NameText = beMasterMemo.TekiyoMei
              Return
            Else
              If Not e Is Nothing Then e.Cancel = True
              objPcaCodeSet.Focus()
            End If
          End If

      End Select


    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  ''' <summary>Clear DisplayData</summary>
  Private Function ClearData() As Boolean
    Try
      'Clear data 
      tblTable.ClearCellValues()
      setTransNo.CodeText = String.Empty
      ltxtTransNo2.Text = String.Empty
      setProduct.CodeText = String.Empty
      setProduct.NameText = " "
      setFromWarehouse.CodeText = String.Empty
      setFromWarehouse.NameText = String.Empty
      setToWarehouse.CodeText = String.Empty
      setMemoCode.CodeText = String.Empty
      setMemoContent.CodeText = String.Empty

      ltxtCreatedAt.Text = String.Empty
      ltxtCreatedBy.Text = String.Empty
      ltxtModifiedAt.Text = String.Empty
      ltxtModifiedBy.Text = String.Empty
      ltxtNumber.Text = CStr(0)
      ltxtQuantity.Text = CStr(0)
      ldtArrivalDate.Date = Today
      ldtMoveDate.Date = Today
      tblTable.DataRowCount = 99
      tblTable.BodyRowCount = 99
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

  ''' <summary>Display TransSlip to screen (By slip no)</summary>
  ''' <param name="slipNo">slipNo</param>
  ''' <param name="isClearDataFirst">isClearDataFirst = False in case of Reset (Remain data in labels)</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DisplaySlipByNo(ByVal SlipNo As Integer, Optional isClearDataFirst As Boolean = True) As Boolean

    Dim intRowCount As Integer = 0
    Try

      m_isSlipMoving = True
      Dim SMSClass As New SMS(connector)

      ReDim m_arReadNumber(1)
      ReDim m_arReadQuantity(1)
      ReDim m_arDiffNumber(1)
      ReDim m_arDiffQuantity(1)

      If isClearDataFirst Then ClearData()
      tblTable.DataRowCount = 99
      tblTable.BodyRowCount = 99

      If SL_WHTClass.ReadByNoOnlyTrans(SlipNo) Then

        'Get header data
        HeaderLabel.State = LabelStateType.Modify
        PcaFunctionCommandNext.Enabled = True
        ToolStripButtonDelete.Enabled = True
        PcaFunctionCommandCopy.Enabled = True
        tblTable.Enabled = True

        With SL_WHTClass

          SMSClass.ReadByID(.sl_wht_scd)

          setTransNo.CodeText = .sl_wht_denno.ToString
          ltxtTransNo2.Text = .sl_wht_denno2

          ldtMoveDate.IntDate = .sl_wht_date
          ldtArrivalDate.IntDate = .sl_wht_dlvdate

          setProduct.CodeText = .sl_wht_scd
          Call PcaCodeSet_CodeTextValidating2(setProduct, Nothing)
          setFromWarehouse.CodeText = .sl_wht_soukof
          Call PcaCodeSet_CodeTextValidating2(setFromWarehouse, Nothing)
          setToWarehouse.CodeText = .sl_wht_soukot
          Call PcaCodeSet_CodeTextValidating2(setToWarehouse, Nothing)
          setMemoCode.CodeText = .sl_wht_tekcd
          setMemoContent.CodeText = .sl_wht_tekmei

          ltxtNumber.Text = .sl_wht_honsu.ToString
          ltxtQuantity.Text = .sl_wht_suryo.ToString("#,0." & New String("0"c, SMSClass.sms_sketa))

          ltxtCreatedBy.Text = .sl_wht_insuser
          If Not .sl_wht_insdate = Nothing Then ltxtCreatedAt.Text = CStr(.sl_wht_insdate)
          ltxtModifiedBy.Text = .sl_wht_upduser

          If Not .sl_wht_upddate = Nothing Then ltxtModifiedAt.Text = CStr(.sl_wht_upddate)
        End With

        Dim myTableDetails As SL_ZDN() = SL_ZDNClass.ReadByIDOnlyTrans(SlipNo, setProduct.CodeText, setFromWarehouse.CodeText)

        While intRowCount < myTableDetails.Count

          ReDim Preserve m_arReadNumber(intRowCount + 1)
          ReDim Preserve m_arReadQuantity(intRowCount + 1)
          ReDim Preserve m_arDiffNumber(intRowCount + 1)
          ReDim Preserve m_arDiffQuantity(intRowCount + 1)

          'Get details data
          tblTable.SetCellValue(intRowCount, ColumnName.TargetDate, Format(myTableDetails(intRowCount).sl_zdn_uribi, "0000/00/00"))
          tblTable.SetCellValue(intRowCount, ColumnName.SlipNo, myTableDetails(intRowCount).sl_zdn_denno)

          tblTable.SetCellValue(intRowCount, ColumnName.LotNo, myTableDetails(intRowCount).sl_zdn_ulotno)

          If myTableDetails(intRowCount).sl_zdn_relay_zdnid = 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.SupLotNo, String.Empty)
          Else
            tblTable.SetCellValue(intRowCount, ColumnName.SupLotNo, myTableDetails(intRowCount).sl_zdn_alotno)
          End If
          tblTable.SetCellValue(intRowCount, ColumnName.Number, myTableDetails(intRowCount).sl_zdn_honsu)
          tblTable.SetCellValue(intRowCount, ColumnName.Quantity, myTableDetails(intRowCount).sl_zdn_suryo.ToString("#,0." & New String("0"c, SMSClass.sms_sketa)))
          If myTableDetails(intRowCount).sl_zdn_nyukohonsu = 0 Or myTableDetails(intRowCount).sl_zdn_relay_zdnid = 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.MoveNumber, String.Empty)
            m_arReadNumber(intRowCount) = 0
          Else
            tblTable.SetCellValue(intRowCount, ColumnName.MoveNumber, myTableDetails(intRowCount).sl_zdn_nyukohonsu)
            m_arReadNumber(intRowCount) = myTableDetails(intRowCount).sl_zdn_nyukohonsu
          End If

          If myTableDetails(intRowCount).sl_zdn_nyukosuryo = 0 Or myTableDetails(intRowCount).sl_zdn_relay_zdnid = 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.MoveQuantity, String.Empty)
            m_arReadQuantity(intRowCount) = 0
          Else
            tblTable.SetCellValue(intRowCount, ColumnName.MoveQuantity, myTableDetails(intRowCount).sl_zdn_nyukosuryo.ToString("#,0." & New String("0"c, SMSClass.sms_sketa)))
            m_arReadQuantity(intRowCount) = myTableDetails(intRowCount).sl_zdn_nyukosuryo
          End If

          m_arDiffNumber(intRowCount) = 0
          m_arDiffQuantity(intRowCount) = 0

          If SL_LMBClass.sl_lmb_sdlflg = 1 And myTableDetails(intRowCount).sl_zdn_sdldate > 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.Shukka, Format(myTableDetails(intRowCount).sl_zdn_sdldate, "0000/00/00"))
          End If
          If SL_LMBClass.sl_lmb_ubdflg = 1 And myTableDetails(intRowCount).sl_zdn_ubdate > 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.Shukka, Format(myTableDetails(intRowCount).sl_zdn_ubdate, "0000/00/00"))
          End If
          If SL_LMBClass.sl_lmb_bbdflg = 1 And myTableDetails(intRowCount).sl_zdn_bbdate > 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.Shukka, Format(myTableDetails(intRowCount).sl_zdn_bbdate, "0000/00/00"))
          End If
          If SL_LMBClass.sl_lmb_expflg = 1 And myTableDetails(intRowCount).sl_zdn_expdate > 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.Shukka, Format(myTableDetails(intRowCount).sl_zdn_expdate, "0000/00/00"))
          End If

          If Not myTableDetails(intRowCount).sl_nykh_tcd = String.Empty Then tblTable.SetCellValue(intRowCount, ColumnName.SupCode, myTableDetails(intRowCount).sl_nykh_tcd)
          If Not myTableDetails(intRowCount).sl_cms_tnm = String.Empty Then tblTable.SetCellValue(intRowCount, ColumnName.SupName, myTableDetails(intRowCount).sl_cms_tnm)

          If Not String.IsNullOrEmpty(myTableDetails(intRowCount).sl_zdn_datakbn.ToString) Then
            Select Case myTableDetails(intRowCount).sl_zdn_datakbn
              Case 1
                tblTable.SetCellValue(intRowCount, ColumnName.Type, "入荷")
              Case 2
                tblTable.SetCellValue(intRowCount, ColumnName.Type, "製造")
              Case 3
                tblTable.SetCellValue(intRowCount, ColumnName.Type, "振替")
            End Select
          End If

          tblTable.SetCellValue(intRowCount, ColumnName.UnitPrice, myTableDetails(intRowCount).sl_zdn_tanka.ToString("#,0." & New String("0"c, SMSClass.sms_tketa)))
          tblTable.SetCellValue(intRowCount, ColumnName.LotID1, myTableDetails(intRowCount).sl_zdn_ldid1)
          tblTable.SetCellValue(intRowCount, ColumnName.LotID2, myTableDetails(intRowCount).sl_zdn_ldid2)
          tblTable.SetCellValue(intRowCount, ColumnName.LotID3, myTableDetails(intRowCount).sl_zdn_ldid3)
          tblTable.SetCellValue(intRowCount, ColumnName.LotID4, myTableDetails(intRowCount).sl_zdn_ldid4)
          tblTable.SetCellValue(intRowCount, ColumnName.LotID5, myTableDetails(intRowCount).sl_zdn_ldid5)
          tblTable.SetCellValue(intRowCount, ColumnName.LotID6, myTableDetails(intRowCount).sl_zdn_ldid6)
          tblTable.SetCellValue(intRowCount, ColumnName.LotID7, myTableDetails(intRowCount).sl_zdn_ldid7)
          tblTable.SetCellValue(intRowCount, ColumnName.LotID8, myTableDetails(intRowCount).sl_zdn_ldid8)
          tblTable.SetCellValue(intRowCount, ColumnName.LotID9, myTableDetails(intRowCount).sl_zdn_ldid9)
          tblTable.SetCellValue(intRowCount, ColumnName.LotID10, myTableDetails(intRowCount).sl_zdn_ldid10)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName1, myTableDetails(intRowCount).sl_zdn_ldmei1)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName2, myTableDetails(intRowCount).sl_zdn_ldmei2)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName3, myTableDetails(intRowCount).sl_zdn_ldmei3)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName4, myTableDetails(intRowCount).sl_zdn_ldmei4)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName5, myTableDetails(intRowCount).sl_zdn_ldmei5)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName6, myTableDetails(intRowCount).sl_zdn_ldmei6)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName7, myTableDetails(intRowCount).sl_zdn_ldmei7)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName8, myTableDetails(intRowCount).sl_zdn_ldmei8)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName9, myTableDetails(intRowCount).sl_zdn_ldmei9)
          tblTable.SetCellValue(intRowCount, ColumnName.LotName10, myTableDetails(intRowCount).sl_zdn_ldmei10)
          tblTable.SetCellValue(intRowCount, ColumnName.Rate, myTableDetails(intRowCount).sl_zdn_tax)
          tblTable.SetCellValue(intRowCount, ColumnName.ManageNo, myTableDetails(intRowCount).sl_zdn_kanrino)
          tblTable.SetCellValue(intRowCount, ColumnName.SystemLotNo, myTableDetails(intRowCount).sl_zdn_lotno)
          tblTable.SetCellValue(intRowCount, ColumnName.StockID, myTableDetails(intRowCount).sl_zdn_id)
          tblTable.SetCellValue(intRowCount, ColumnName.FromStockID, myTableDetails(intRowCount).sl_zdn_relay_zdnid)
          If myTableDetails(intRowCount).sl_zdn_relay_zdnid > 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.PrevNum, myTableDetails(intRowCount).sl_zdn_nyukohonsu)
            tblTable.SetCellValue(intRowCount, ColumnName.PrevQty, myTableDetails(intRowCount).sl_zdn_nyukosuryo)
          Else
            tblTable.SetCellValue(intRowCount, ColumnName.PrevNum, 0)
            tblTable.SetCellValue(intRowCount, ColumnName.PrevQty, 0)
          End If
          intRowCount = intRowCount + 1
        End While
        If intRowCount > 0 Then
          m_IntRowCount = intRowCount - 1
        Else
          DisplayBox.ShowNotify(SLConstants.NotifyMessage.DATA_NOT_FOUND)
          Return False
        End If

        Dim curAllocatedNumber As Decimal = 0D
        Dim curAllocatedQuantity As Decimal = 0D

        SL_WHTClass.GetAllocatedOnlyTrans(curAllocatedNumber, curAllocatedQuantity, SlipNo)

        If curAllocatedNumber > 0 Or curAllocatedQuantity > 0 Then m_IsAllocatedSlip = True Else m_IsAllocatedSlip = False

        Return True
      Else
        Return False
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
      m_isSlipMoving = False
      tblTable.DataRowCount = intRowCount
      tblTable.BodyRowCount = intRowCount
    End Try
  End Function

  ''' <summary>Validate input before register</summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ValidateInput() As Boolean

    HeaderLabel.InformationText = String.Empty
    Try

      ldtMoveDate.Focus()
      ldtArrivalDate.Focus()

      Dim intNumber As Integer = 0
      Dim intTotalNumber As Integer = 0
      Dim decQuantity As Decimal = 0D
      Dim decTotalQuantity As Decimal = 0D

      decQuantity = 0
      intNumber = 0

      For intLpc = 0 To tblTable.DataRowCount
        If Not tblTable.GetCellValue(intLpc, ColumnName.MoveNumber) Is Nothing AndAlso _
          IsNumeric(tblTable.GetCellValue(intLpc, ColumnName.MoveNumber).ToString) Then
          intNumber = CInt(tblTable.GetCellValue(intLpc, ColumnName.MoveNumber).ToString)
        Else
          intNumber = 0
        End If

        If Not tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity) Is Nothing AndAlso _
          IsNumeric(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity).ToString) Then
          decQuantity = CDec(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity).ToString)
        Else
          decQuantity = 0
        End If

        intTotalNumber += intNumber
        decTotalQuantity += decQuantity
      Next

      ltxtNumber.Text = CStr(intTotalNumber)
      ltxtQuantity.Text = CStr(decTotalQuantity)

      If ldtMoveDate.Date > ldtArrivalDate.Date Then
        DisplayBox.ShowError("移動日が着日を" & SLConstants.NotifyMessage.EXCESS_MESSAGE)
        Return False
      End If

      If String.IsNullOrEmpty(setProduct.CodeText) Then
        DisplayBox.ShowError("商品コード" & SLConstants.NotifyMessage.INPUT_REQUIRED)
        Return False
      End If

      '商品コードが変更されて登録ボタンが押された場合を考慮して再検索
      Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms = MasterDialog.FindBEMasterSms(setProduct.CodeText)
      If beMasterSms.SyohinCode.Length > 0 Then
        setProduct.CodeText = beMasterSms.SyohinCode
        setProduct.NameText = beMasterSms.SyohinMei
        ltxtUnit.Text = beMasterSms.Tani
      Else
        setProduct.Focus()
        Return False
      End If

      If String.IsNullOrEmpty(setFromWarehouse.CodeText) Then
        DisplayBox.ShowError("移動元倉庫コード" & SLConstants.NotifyMessage.INPUT_REQUIRED)
        setFromWarehouse.Focus()
        Return False
      End If

      '倉庫コードが変更されて登録ボタンが押された場合を考慮して再検索
      Dim beMasterSokoFrom As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(setFromWarehouse.CodeText)
      If beMasterSokoFrom.SokoCode.Length > 0 Then
        setFromWarehouse.CodeText = beMasterSokoFrom.SokoCode
        setFromWarehouse.NameText = beMasterSokoFrom.SokoMei
      Else
        setFromWarehouse.Focus()
        Return False
      End If


      If String.IsNullOrEmpty(setToWarehouse.CodeText) Then
        DisplayBox.ShowError("移動先倉庫コード" & SLConstants.NotifyMessage.INPUT_REQUIRED)
        setToWarehouse.Focus()
        Return False
      End If

      '倉庫コードが変更されて登録ボタンが押された場合を考慮して再検索
      Dim beMasterSokoTo As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(setToWarehouse.CodeText)
      If beMasterSokoTo.SokoCode.Length > 0 Then
        setToWarehouse.CodeText = beMasterSokoTo.SokoCode
        setToWarehouse.NameText = beMasterSokoTo.SokoMei
      Else
        setToWarehouse.Focus()
        Return False
      End If

      If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(0, ColumnName.StockID)) Then
        DisplayBox.ShowError("明細" & SLConstants.NotifyMessage.INPUT_REQUIRED)
        Return False
      End If

      If setFromWarehouse.CodeText = setToWarehouse.CodeText Then
        DisplayBox.ShowError("移動元と異なる倉庫" & SLConstants.NotifyMessage.INPUT_REQUIRED)
        setToWarehouse.Focus()
        Return False
      End If

      If ltxtQuantity.Text = CStr(0) Then
        DisplayBox.ShowError("移動を行うロットの数量" & SLConstants.NotifyMessage.INPUT_REQUIRED)
        tblTable.Focus()
        Return False
      End If

      If ldtMoveDate.IntDate <= SL_LMBClass.sl_lmb_zcutoff_date Or ldtArrivalDate.IntDate <= SL_LMBClass.sl_lmb_zcutoff_date Then

        DisplayBox.ShowError(MessageDefines.Information.CantUpdateZaikoShimekiri)
        HeaderLabel.InformationText = MessageDefines.HeaderLabel.CheckHizukeByZaiko
        Return False
      End If

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try

  End Function

  Private Function DisplayStockListOnNew() As Boolean
    Dim isSuccess As Boolean = False
    Try
      Return SetDetailList(SL_ZDNClass.ReadByProductWHOnlyTrans(setProduct.CodeText, setFromWarehouse.CodeText))

      isSuccess = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try

    Return isSuccess

  End Function

  Private Function DisplayStockListOnMod() As Boolean

    Dim isSuccess As Boolean = False
    Try
      Return SetDetailList(SL_ZDNClass.ReadByIDOnlyTrans(SL_WHTClass.sl_wht_id, setProduct.CodeText, setFromWarehouse.CodeText))

      isSuccess = True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try

    Return isSuccess

  End Function

  ''' <summary>Show Stick List</summary>
  ''' <returns></returns>
  ''' <remarks>移動元倉庫コード、商品コードを元に抽出します。</remarks>
  Private Function SetDetailList(myTableDetails As SL_ZDN()) As Boolean

    Const NEW_FUNCTION_NAME As String = "DisplayStockListOnNew"
    Dim strCallerFunc As String = New StackFrame(1).GetMethod.Name
    Dim intRowCount As Integer = 0
    Dim SMSClass As New SMS(connector)

    Try

      tblTable.Enabled = True

      tblTable.DataRowCount = 99
      tblTable.BodyRowCount = 99

      If myTableDetails Is Nothing Then
        intRowCount = 99
        tblTable.ClearCellValues()
        Return False
      End If

      SMSClass.ReadByID(setProduct.CodeText)

      While intRowCount < myTableDetails.Count

        ReDim Preserve m_arReadNumber(intRowCount + 1)
        ReDim Preserve m_arReadQuantity(intRowCount + 1)
        ReDim Preserve m_arDiffNumber(intRowCount + 1)
        ReDim Preserve m_arDiffQuantity(intRowCount + 1)


        'Get details data
        tblTable.SetCellValue(intRowCount, ColumnName.TargetDate, Format(myTableDetails(intRowCount).sl_zdn_uribi, "0000/00/00"))
        tblTable.SetCellValue(intRowCount, ColumnName.SlipNo, myTableDetails(intRowCount).sl_zdn_denno)

        tblTable.SetCellValue(intRowCount, ColumnName.LotNo, myTableDetails(intRowCount).sl_zdn_ulotno)
        If strCallerFunc = NEW_FUNCTION_NAME Then
          tblTable.SetCellValue(intRowCount, ColumnName.SupLotNo, String.Empty)
        Else
          tblTable.SetCellValue(intRowCount, ColumnName.SupLotNo, myTableDetails(intRowCount).sl_zdn_alotno)
        End If

        tblTable.SetCellValue(intRowCount, ColumnName.Number, myTableDetails(intRowCount).sl_zdn_honsu.ToString("#,0"))
        tblTable.SetCellValue(intRowCount, ColumnName.Quantity, myTableDetails(intRowCount).sl_zdn_suryo.ToString("#,0." & New String("0"c, SMSClass.sms_sketa)))

        If strCallerFunc = NEW_FUNCTION_NAME Then
          tblTable.SetCellValue(intRowCount, ColumnName.MoveNumber, String.Empty)
          tblTable.SetCellValue(intRowCount, ColumnName.MoveQuantity, String.Empty)
        Else
          If myTableDetails(intRowCount).sl_zdn_nyukohonsu <= 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.MoveNumber, String.Empty)
            m_arReadNumber(intRowCount) = 0
          Else
            tblTable.SetCellValue(intRowCount, ColumnName.MoveNumber, myTableDetails(intRowCount).sl_zdn_nyukohonsu.ToString("#,0"))
            m_arReadNumber(intRowCount) = myTableDetails(intRowCount).sl_zdn_nyukohonsu
          End If

          If myTableDetails(intRowCount).sl_zdn_nyukosuryo <= 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.MoveQuantity, String.Empty)
            m_arReadQuantity(intRowCount) = 0
          Else
            tblTable.SetCellValue(intRowCount, ColumnName.MoveQuantity, myTableDetails(intRowCount).sl_zdn_nyukosuryo.ToString("#,0." & New String("0"c, SMSClass.sms_sketa)))
            m_arReadQuantity(intRowCount) = CDec(Val(myTableDetails(intRowCount).sl_zdn_nyukosuryo))
          End If

          m_arDiffNumber(intRowCount) = 0
          m_arDiffQuantity(intRowCount) = 0
        End If

        If SL_LMBClass.sl_lmb_sdlflg = 1 And myTableDetails(intRowCount).sl_zdn_sdldate > 0 Then
          tblTable.SetCellValue(intRowCount, ColumnName.Shukka, Format(myTableDetails(intRowCount).sl_zdn_sdldate, "0000/00/00"))
        End If
        If SL_LMBClass.sl_lmb_ubdflg = 1 And myTableDetails(intRowCount).sl_zdn_ubdate > 0 Then
          tblTable.SetCellValue(intRowCount, ColumnName.Shukka, Format(myTableDetails(intRowCount).sl_zdn_ubdate, "0000/00/00"))
        End If
        If SL_LMBClass.sl_lmb_bbdflg = 1 And myTableDetails(intRowCount).sl_zdn_bbdate > 0 Then
          tblTable.SetCellValue(intRowCount, ColumnName.Shukka, Format(myTableDetails(intRowCount).sl_zdn_bbdate, "0000/00/00"))
        End If
        If SL_LMBClass.sl_lmb_expflg = 1 And myTableDetails(intRowCount).sl_zdn_expdate > 0 Then
          tblTable.SetCellValue(intRowCount, ColumnName.Shukka, Format(myTableDetails(intRowCount).sl_zdn_expdate, "0000/00/00"))
        End If

        If Not myTableDetails(intRowCount).sl_nykh_tcd = String.Empty Then tblTable.SetCellValue(intRowCount, ColumnName.SupCode, myTableDetails(intRowCount).sl_nykh_tcd)
        If Not myTableDetails(intRowCount).sl_cms_tnm = String.Empty Then tblTable.SetCellValue(intRowCount, ColumnName.SupName, myTableDetails(intRowCount).sl_cms_tnm)

        If Not String.IsNullOrEmpty(myTableDetails(intRowCount).sl_zdn_datakbn.ToString) Then
          Select Case myTableDetails(intRowCount).sl_zdn_datakbn
            Case 1
              tblTable.SetCellValue(intRowCount, ColumnName.Type, "入荷")
            Case 2
              tblTable.SetCellValue(intRowCount, ColumnName.Type, "製造")
            Case 3
              tblTable.SetCellValue(intRowCount, ColumnName.Type, "振替")
          End Select
        End If

        tblTable.SetCellValue(intRowCount, ColumnName.UnitPrice, Format(myTableDetails(intRowCount).sl_zdn_tanka, "#,0." & New String("0"c, SMSClass.sms_tketa)))
        tblTable.SetCellValue(intRowCount, ColumnName.LotID1, myTableDetails(intRowCount).sl_zdn_ldid1)
        tblTable.SetCellValue(intRowCount, ColumnName.LotID2, myTableDetails(intRowCount).sl_zdn_ldid2)
        tblTable.SetCellValue(intRowCount, ColumnName.LotID3, myTableDetails(intRowCount).sl_zdn_ldid3)
        tblTable.SetCellValue(intRowCount, ColumnName.LotID4, myTableDetails(intRowCount).sl_zdn_ldid4)
        tblTable.SetCellValue(intRowCount, ColumnName.LotID5, myTableDetails(intRowCount).sl_zdn_ldid5)
        tblTable.SetCellValue(intRowCount, ColumnName.LotID6, myTableDetails(intRowCount).sl_zdn_ldid6)
        tblTable.SetCellValue(intRowCount, ColumnName.LotID7, myTableDetails(intRowCount).sl_zdn_ldid7)
        tblTable.SetCellValue(intRowCount, ColumnName.LotID8, myTableDetails(intRowCount).sl_zdn_ldid8)
        tblTable.SetCellValue(intRowCount, ColumnName.LotID9, myTableDetails(intRowCount).sl_zdn_ldid9)
        tblTable.SetCellValue(intRowCount, ColumnName.LotID10, myTableDetails(intRowCount).sl_zdn_ldid10)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName1, myTableDetails(intRowCount).sl_zdn_ldmei1)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName2, myTableDetails(intRowCount).sl_zdn_ldmei2)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName3, myTableDetails(intRowCount).sl_zdn_ldmei3)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName4, myTableDetails(intRowCount).sl_zdn_ldmei4)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName5, myTableDetails(intRowCount).sl_zdn_ldmei5)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName6, myTableDetails(intRowCount).sl_zdn_ldmei6)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName7, myTableDetails(intRowCount).sl_zdn_ldmei7)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName8, myTableDetails(intRowCount).sl_zdn_ldmei8)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName9, myTableDetails(intRowCount).sl_zdn_ldmei9)
        tblTable.SetCellValue(intRowCount, ColumnName.LotName10, myTableDetails(intRowCount).sl_zdn_ldmei10)
        tblTable.SetCellValue(intRowCount, ColumnName.Rate, myTableDetails(intRowCount).sl_zdn_tax)
        tblTable.SetCellValue(intRowCount, ColumnName.ManageNo, myTableDetails(intRowCount).sl_zdn_kanrino)
        tblTable.SetCellValue(intRowCount, ColumnName.SystemLotNo, myTableDetails(intRowCount).sl_zdn_lotno)
        tblTable.SetCellValue(intRowCount, ColumnName.StockID, myTableDetails(intRowCount).sl_zdn_id)
        tblTable.SetCellValue(intRowCount, ColumnName.CaseNumber, myTableDetails(intRowCount).sl_zdn_iri)

        If strCallerFunc <> NEW_FUNCTION_NAME Then
          tblTable.SetCellValue(intRowCount, ColumnName.FromStockID, myTableDetails(intRowCount).sl_zdn_relay_zdnid)
        Else
          tblTable.SetCellValue(intRowCount, ColumnName.FromStockID, 0)
        End If

        intRowCount = intRowCount + 1
      End While

      If intRowCount > 0 Then m_IntRowCount = intRowCount - 1

      If intRowCount = 0 Then
        Return False
      Else
        Return True
      End If

    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    Finally
      tblTable.DataRowCount = intRowCount
      tblTable.BodyRowCount = intRowCount
    End Try

    Return True
  End Function

#End Region

#Region "PcaCodeSet Methods"

  Private Sub setPcaCodeSet_ClickReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setProduct.ClickReferButton2, _
                                                                                                                       setFromWarehouse.ClickReferButton2, _
                                                                                                                       setToWarehouse.ClickReferButton2, _
                                                                                                                       setMemoCode.ClickReferButton2, _
                                                                                                                       setTransNo.ClickReferButton2
    Dim objPcaCodeset As PcaCodeSet = DirectCast(sender, PcaCodeSet)
    If objPcaCodeset Is setTransNo Then
      Dim denpyoDialog = New SLDenSearchDialog(connector)
      Dim strNewCode As String =
denpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.TransferSlip)
      If String.IsNullOrEmpty(strNewCode) = False Then
        setTransNo.CodeText = denpyoDialog.returnValue()
        setTransNo_Validating(New Object, New CancelEventArgs(False))
      End If

    Else
      If m_isMasterSearched Then m_isMasterSearched = False : Return
      SearchItem(objPcaCodeset, e)
    End If

  End Sub

  Private Sub setTransNo_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setTransNo.Validating
    Try
      Dim intSlipNo As Integer = 0

      If setTransNo.CodeText = String.Empty Then Return

      If Integer.TryParse(setTransNo.CodeText, intSlipNo) AndAlso DisplaySlipByNo(intSlipNo) Then
        setTransNo.Enabled = True
        SetUpEditMode(m_IsAllocatedSlip)
      Else
        e.Cancel = True
        setTransNo.CodeText = String.Empty
        m_isSlipChanged = False
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub PcaCodeSet_Enter(sender As System.Object, e As System.EventArgs) Handles setProduct.Enter, setFromWarehouse.Enter, setToWarehouse.Enter, setMemoCode.Enter
    Try
      Dim objPcaCodeSet As PcaCodeSet = DirectCast(sender, PcaCodeSet)
      m_strCurrentCode = objPcaCodeSet.CodeText
      PcaFunctionCommandRefer.Enabled = True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub PcaCodeSet_Leave(sender As System.Object, e As System.EventArgs) Handles setProduct.Leave, setFromWarehouse.Leave, setToWarehouse.Leave, setMemoCode.Leave
    PcaFunctionCommandRefer.Enabled = False
  End Sub

  Private Sub PcaCodeSet_CodeTextChange(sender As System.Object, e As System.EventArgs) Handles setTransNo.CodeTextChanged, _
                                                                                                setProduct.CodeTextChanged, _
                                                                                                setFromWarehouse.CodeTextChanged, _
                                                                                                setToWarehouse.CodeTextChanged, _
                                                                                                setMemoCode.CodeTextChanged, _
                                                                                                setMemoContent.CodeTextChanged


    '変更が開始された
    If MasterDialog IsNot Nothing Then MasterDialog.BeginChanged()

    m_strCurrentCode = String.Empty
    m_isSlipChanged = True
    Dim objPcaCodeSet As PcaCodeSet = DirectCast(sender, PcaCodeSet)

    Select Case objPcaCodeSet.Name
      Case setProduct.Name, setFromWarehouse.Name
        objPcaCodeSet.NameText = String.Empty
        tblTable.ClearCellValues()
        tblTable.Enabled = False
        ltxtNumber.Text = Str(0)
        ltxtQuantity.Text = Str(0)

        If objPcaCodeSet.Name = setProduct.Name Then ltxtUnit.Text = String.Empty
      Case setToWarehouse.Name
        objPcaCodeSet.NameText = String.Empty
      Case setMemoCode.Name
        setMemoContent.CodeText = String.Empty
    End Select

  End Sub

  Private Sub PcaCodeSet_Validated(sender As System.Object, e As System.EventArgs) Handles setProduct.Validated, setFromWarehouse.Validated

    Try
      Dim objPcaCodeSet As PcaCodeSet = DirectCast(sender, PcaCodeSet)
      If objPcaCodeSet.CodeText = m_strCurrentCode Then Exit Sub

      If String.IsNullOrEmpty(setProduct.CodeText.ToString.Trim) Or String.IsNullOrEmpty(setProduct.NameText.ToString.Trim) _
      Or String.IsNullOrEmpty(setFromWarehouse.CodeText.ToString.Trim) Or String.IsNullOrEmpty(setFromWarehouse.NameText.ToString.Trim) Then
        m_IsF12CloseFlag = True
      Else
        'データ取得
        Dim isSuccess As Boolean = False
        If HeaderLabel.State = LabelStateType.New Then
          isSuccess = DisplayStockListOnNew()
        ElseIf HeaderLabel.State = LabelStateType.Modify Then
          isSuccess = DisplayStockListOnMod()
        End If

        If isSuccess Then
          m_IsF12CloseFlag = False
          tblTable.Enabled = True
        Else
          tblTable.Enabled = False
          ltxtNumber.Text = Str(0)
          ltxtQuantity.Text = Str(0)
          SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, objPcaCodeSet, ToolTip1, Me)
          m_IsF12CloseFlag = True
          objPcaCodeSet.Focus()
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try



  End Sub

  Private Sub PcaCodeSet_CodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setProduct.CodeTextValidating2, setFromWarehouse.CodeTextValidating2, _
                                                                                                                     setToWarehouse.CodeTextValidating2, setMemoCode.CodeTextValidating2, _
                                                                                                                     setTransNo.CodeTextValidating2

    Dim objPcaCodeSet As PcaCodeSet = DirectCast(sender, PcaCodeSet)
    Dim newCode As String = objPcaCodeSet.CodeText

    If m_IsClosingFlag Then Exit Sub

    Select Case objPcaCodeSet.Name
      Case setProduct.Name
        Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms = MasterDialog.FindBEMasterSms(newCode)
        If newCode.Length = 0 Then Return
        If beMasterSms.SyohinCode.Length > 0 Then
          objPcaCodeSet.CodeText = beMasterSms.SyohinCode
          objPcaCodeSet.NameText = beMasterSms.SyohinMei
          ltxtUnit.Text = beMasterSms.Tani
          If objPcaCodeSet.CodeText.ToString.Trim = String.Empty Then
            tblTable.ClearCellValues()
            ltxtNumber.Text = CStr(0)
            ltxtQuantity.Text = CStr(0)
          End If
        Else
          DisplayBox.ShowError("商品" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
          If Not e Is Nothing Then e.Cancel = True
          objPcaCodeSet.Focus()
        End If

      Case setFromWarehouse.Name, setToWarehouse.Name
        Dim beMasterSoko As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(newCode)
        If newCode.Length = 0 Then Return
        If beMasterSoko.SokoCode.Length > 0 Then
          objPcaCodeSet.CodeText = beMasterSoko.SokoCode
          objPcaCodeSet.NameText = beMasterSoko.SokoMei
          If objPcaCodeSet Is setFromWarehouse AndAlso objPcaCodeSet.CodeText.ToString.Trim = String.Empty Then
            tblTable.ClearCellValues()
            ltxtNumber.Text = CStr(0)
            ltxtQuantity.Text = CStr(0)
          End If

        Else
          DisplayBox.ShowError("倉庫" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
          If Not e Is Nothing Then e.Cancel = True
          objPcaCodeSet.Focus()
        End If

      Case setMemoCode.Name
        Dim beMasterMemo As PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo = MasterDialog.FindBEMasterTekiyo(KubunIdType.TekiyoKubun7, newCode)
        If newCode.Length = 0 Then Return
        If beMasterMemo.TekiyoCode.Length > 0 Then
          setMemoCode.CodeText = beMasterMemo.TekiyoCode
          setMemoContent.CodeText = beMasterMemo.TekiyoMei
          Return
        Else
          DisplayBox.ShowError("摘要" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
          If Not e Is Nothing Then e.Cancel = True
          objPcaCodeSet.Focus()
        End If
    End Select


    Return
  End Sub

#End Region

#Region "PcaLabeledDate Methods"

  Private Sub ldtMoveDate_DateChanged(sender As System.Object, e As System.EventArgs) Handles ldtMoveDate.DateChanged, ldtArrivalDate.DateChanged
    m_isSlipChanged = True
  End Sub
#End Region

#Region "PcaLabeledTextBox Methods"
  Private Sub PcaLabeledTextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles ltxtTransNo2.TextChanged
    m_isSlipChanged = True
  End Sub
#End Region

#Region "PcaTable Methods"

  Private Sub tblTable_CellValueChanged(sender As Object, args As PCA.Controls.EditingCellEventArgs) Handles tblTable.CellValueChanged
    Try
      m_isSlipChanged = True
      If args.CellName = ColumnName.SupLotNo Then Return
      If Not IsNumeric(args.EditingValue) And Strings.Right(args.EditingValue.ToString, 1) <> "." Then
        '数字でも .でもない場合
        If args.EditingValue.ToString.Length > 0 Then
          tblTable.SetCellValue(m_IntRowIndex, args.CellName, Strings.Left(args.EditingValue.ToString, args.EditingValue.ToString.Length - 1))
        End If
      End If

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub tblTable_ValidatingCell(sender As System.Object, args As PCA.Controls.CancelCellEventArgs) Handles tblTable.ValidatingCell

    Dim myTable As PCA.TSC.Kon.Tools.TscMeisaiTable = DirectCast(sender, PCA.TSC.Kon.Tools.TscMeisaiTable)
    Dim intLpc As Integer = 0

    Try
      Dim Rectangle = tblTable.GetCellRectangle(tblTable, m_IntRowIndex, args.CellName)
      If m_isSlipMoving Then Return
      Select Case args.CellName
        Case ColumnName.MoveNumber
          Dim intNumber As Integer = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Number))
          If IsNumeric(args.NewValue.ToString) AndAlso CInt(args.NewValue) > intNumber Then
            Rectangle.Y += tblTable.Top
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.TRANSFER_NUMBER_OVER, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, 1000, Rectangle, ToolTip1, Me)
            args.Cancel = True
          End If

        Case ColumnName.MoveQuantity
          Dim decQuantity As Decimal = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Quantity))
          If IsNumeric(args.NewValue.ToString) AndAlso CInt(args.NewValue) > decQuantity Then
            Rectangle.Y += tblTable.Top
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.TRANSFER_QUANTITY_OVER, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, 1000, Rectangle, ToolTip1, Me)
            args.Cancel = True
          End If


      End Select
    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  Private Sub tblTable_ValidatedCell(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.ValidatedCell
    Dim intLpc As Integer = 0

    Try

      If m_isSlipMoving Then Return
      Select Case args.CellName
        Case ColumnName.MoveNumber
          Dim intNumber As Integer = 0
          Dim intTotalNumber As Integer = 0

          For intLpc = 0 To tblTable.DataRowCount
            If Not tblTable.GetCellValue(intLpc, ColumnName.MoveNumber) Is Nothing AndAlso _
              IsNumeric(tblTable.GetCellValue(intLpc, ColumnName.MoveNumber).ToString) Then
              intNumber = CInt(tblTable.GetCellValue(intLpc, ColumnName.MoveNumber).ToString)
            Else
              intNumber = 0
            End If
            intTotalNumber += intNumber
          Next

          ltxtNumber.Text = CStr(intTotalNumber)
          If HeaderLabel.State = LabelStateType.Modify Then
            If Not tblTable.GetCellValue(m_IntRowIndex, ColumnName.MoveNumber).ToString = String.Empty Then
              m_arDiffNumber(m_IntRowIndex) = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.MoveNumber).ToString) - m_arReadNumber(m_IntRowIndex)
            End If
          End If

        Case ColumnName.MoveQuantity
          Dim decQuantity As Decimal = 0D
          Dim decTotalQuantity As Decimal = 0D
          Dim strQuantity As String = String.Empty

          For intLpc = 0 To tblTable.DataRowCount
            strQuantity = String.Empty

            If Not tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity) Is Nothing AndAlso _
              IsNumeric(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity).ToString) Then
              decQuantity = CDec(tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity).ToString)
            Else
              decQuantity = 0
            End If
            decTotalQuantity += decQuantity

            If Not tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity) Is Nothing Then strQuantity = tblTable.GetCellValue(intLpc, ColumnName.MoveQuantity).ToString

            If strQuantity.Length > 0 AndAlso Strings.Right(strQuantity, 1) = "." Then
              tblTable.SetCellValue(intLpc, ColumnName.MoveQuantity, Strings.Left(strQuantity, strQuantity.Length - 1))
            End If
            If strQuantity.Length > 0 AndAlso Strings.Left(strQuantity, 1) = "." Then
              tblTable.SetCellValue(intLpc, ColumnName.MoveQuantity, Strings.Right(strQuantity, strQuantity.Length - 1))
            End If
          Next

          ltxtQuantity.Text = CStr(decTotalQuantity)
          If HeaderLabel.State = LabelStateType.Modify Then
            If Not tblTable.GetCellValue(m_IntRowIndex, ColumnName.MoveQuantity).ToString = String.Empty Then
              m_arDiffQuantity(m_IntRowIndex) = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.MoveQuantity).ToString) - m_arReadQuantity(m_IntRowIndex)
            End If
          End If

      End Select
    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  Private Sub tblTable_EnterInputRow(sender As System.Object, args As PCA.Controls.InputRowEventArgs) Handles tblTable.EnterInputRow
    Try

      m_IntRowIndex = args.RowIndex
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return
    End Try

  End Sub


#End Region

#Region "Finalize Methods"

  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then
      connector.LogOffSystem()
    End If
  End Sub

#End Region

#Region "PcaCommandManager Methods"

  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try

      Dim commandItem As PcaCommandItem = e.CommandItem

      Dim isSuccess As Boolean
      Select Case commandItem.CommandName

        Case PcaCommandItemHelp.CommandName

        Case PcaCommandItemPrevious.CommandName 'F2

          Dim intPrevSlipNo As Integer = SL_WHTClass.sl_wht_denno

          '登録可能
          If Not m_IsF12CloseFlag And m_isSlipChanged Then
            Dim dlgResult = DisplayBox.ShowContact(String.Empty)
            If dlgResult = Windows.Forms.DialogResult.Yes Then

              If HeaderLabel.State = LabelStateType.New Then
                isSuccess = Insert()
              Else
                isSuccess = Update()
              End If

              If isSuccess Then
                intPrevSlipNo = SL_WHTClass.sl_wht_denno
                InitControls()
                HeaderLabel.State = LabelStateType.Modify
                m_IsF12CloseFlag = True
                ldtMoveDate.Focus()
              Else
                Return
              End If
            ElseIf dlgResult = Windows.Forms.DialogResult.Cancel Then
              Return
            End If
          End If

          If HeaderLabel.State = LabelStateType.New Then intPrevSlipNo = -1

          'Display slip with max headerID          
          intPrevSlipNo = SL_WHTClass.GetPrevSlipNo(intPrevSlipNo)

          'Display the first slip found
          If intPrevSlipNo > 0 Then
            If DisplaySlipByNo(intPrevSlipNo) Then SetUpEditMode()
          Else
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.PREVIOUS_MESSAGE_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, commandItem.CommandId)
          End If

        Case PcaCommandItemNext.CommandName 'F3

          Dim intNextSlipNo As Integer = SL_WHTClass.sl_wht_denno

          '登録可能
          If Not m_IsF12CloseFlag And m_isSlipChanged Then
            If DisplayBox.ShowContact(String.Empty) = Windows.Forms.DialogResult.Yes Then

              If HeaderLabel.State = LabelStateType.New Then
                isSuccess = Insert()
              Else
                isSuccess = Update()
              End If

              If isSuccess Then
                intNextSlipNo = SL_WHTClass.sl_wht_denno
                InitControls()
                HeaderLabel.State = LabelStateType.Modify
                m_IsF12CloseFlag = True
                ldtMoveDate.Focus()
              Else
                Return
              End If
            End If
          End If

          'Only enable in Edit mode
          If HeaderLabel.State = LabelStateType.Modify Then

            intNextSlipNo = SL_WHTClass.GetNextSlipNo(intNextSlipNo)

            If intNextSlipNo > 0 Then
              If DisplaySlipByNo(intNextSlipNo) Then SetUpEditMode()
            Else
              SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.NEXT_MESSAGE_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, commandItem.CommandId)
            End If

          End If

        Case PcaCommandItemSlipSearch.CommandName 'F6

          Dim denpyoDialog = New SLDenSearchDialog(connector)
          Dim strNewCode As String =
denpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.TransferSlip)
          If String.IsNullOrEmpty(strNewCode) = False Then
            setTransNo.CodeText = denpyoDialog.returnValue()
            setTransNo_Validating(sender, New CancelEventArgs(False))
          End If

        Case PcaCommandItemRefer.CommandName 'F8
          SearchItem(DirectCast(Me.ActiveControl, PcaCodeSet))

        Case PcaCommandItemCopy.CommandName 'F11

          With tblTable
            For intLpc As Integer = 0 To m_IntRowCount - 1
              If Not CInt(tblTable.GetCellValue(intLpc, ColumnName.FromStockID)) = 0 Then
                .SetCellValue(intLpc, ColumnName.StockID, .GetCellValue(intLpc, ColumnName.FromStockID))
                .SetCellValue(intLpc, ColumnName.FromStockID, 0)
              End If
            Next
          End With

          InitControls(False, False)
          ldtMoveDate.Focus()

        Case PcaCommandItemSave.CommandName 'F12

          If HeaderLabel.State = LabelStateType.New Then
            isSuccess = Insert()
          Else
            isSuccess = Update()
          End If

          If isSuccess Then
            InitControls()
            m_IsF12CloseFlag = True

            ldtMoveDate.Focus()
          End If

        Case PcaCommandItemClose.CommandName 'F12

          m_IsClosingFlag = True
          If m_IsF12CloseFlag And Not m_isSlipChanged Then
            Me.Close()
          Else
            Dim result As DialogResult = DisplayBox.ShowContact(String.Empty)
            If result = DialogResult.Yes Then
              If HeaderLabel.State = LabelStateType.New Then
                isSuccess = Insert()
              Else
                isSuccess = Update()
              End If
              Me.Close()
            ElseIf result = Windows.Forms.DialogResult.No Then
              Me.Close()
            End If

          End If

        Case PcaCommandItemDelete.CommandName 'Delete

          Dim result As DialogResult = DisplayBox.ShowWarning(SLConstants.ConfirmMessage.DELETE_CONFIRM_MESSAGE)
          If result = Windows.Forms.DialogResult.Yes Then
            If DeleteData(SL_WHTClass.sl_wht_id, SL_WHTClass.sl_wht_denno, False) Then
              InitControls()
              ldtMoveDate.Focus()
            End If
          End If

        Case PcaCommandItemNew.CommandName

          InitControls()
          ldtMoveDate.Focus()

        Case PcaCommandItemReset.CommandName

          If HeaderLabel.State = LabelStateType.New Then

            InitControls()
            ldtMoveDate.Focus()
          ElseIf HeaderLabel.State = LabelStateType.Modify Then

            'Clear table, display current slip
            tblTable.ClearCellValues()
            DisplaySlipByNo(SL_WHTClass.sl_wht_denno, False)
            SetUpEditMode()

          End If
      End Select
    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  ''' <summary>PcaCommandManager1_UpdateCommandUI</summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks>F12 = Close / Register</remarks>
  Private Sub PcaCommandManager1_UpdateCommandUI(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.UpdateCommandUI
    Dim commandItem As PcaCommandItem = e.CommandItem
    'Change F12 caption+function between 
    If commandItem Is PcaCommandItemSave Then
      commandItem.Enabled = Not m_IsF12CloseFlag
      ToolStripMenuItemClose.Enabled = m_IsF12CloseFlag
      ToolStripMenuItemSave.Enabled = Not m_IsF12CloseFlag
      ToolStripButtonSave.Enabled = Not m_IsF12CloseFlag
    ElseIf commandItem Is PcaCommandItemClose Then
      commandItem.Enabled = m_IsF12CloseFlag
      ToolStripMenuItemClose.Enabled = Not m_IsF12CloseFlag
      ToolStripMenuItemSave.Enabled = m_IsF12CloseFlag
      ToolStripButtonSave.Enabled = m_IsF12CloseFlag
    End If
    ToolStripButtonClose.Enabled = True
  End Sub

#End Region
End Class


''' <summary>ColumnName</summary>
''' <remarks></remarks>
Friend Class ColumnName
  Public Const TargetDate As String = "日付"
  Public Const SlipNo As String = "伝票番号"
  Public Const LotNo As String = "ロットNo"
  Public Const SupLotNo As String = "相手先ロットNo"
  Public Const Number As String = "現在本数"
  Public Const Quantity As String = "現在数量"
  Public Const MoveNumber As String = "移動本数"
  Public Const MoveQuantity As String = "移動数量"
  Public Const Shukka As String = "出荷期限"
  Public Const Shiyou As String = "使用期限"
  Public Const Shoumi As String = "賞味期限"
  Public Const Shouhi As String = "消費期限"
  Public Const SupCode As String = "仕入先コード"
  Public Const SupName As String = "仕入先名"
  Public Const Type As String = "区分"
  Public Const UnitPrice As String = "単価"
  Public Const LotID1 As String = "ロット詳細ID1"
  Public Const LotID2 As String = "ロット詳細ID2"
  Public Const LotID3 As String = "ロット詳細ID3"
  Public Const LotID4 As String = "ロット詳細ID4"
  Public Const LotID5 As String = "ロット詳細ID5"
  Public Const LotID6 As String = "ロット詳細ID6"
  Public Const LotID7 As String = "ロット詳細ID7"
  Public Const LotID8 As String = "ロット詳細ID8"
  Public Const LotID9 As String = "ロット詳細ID9"
  Public Const LotID10 As String = "ロット詳細ID10"
  Public Const LotName1 As String = "ロット詳細名1"
  Public Const LotName2 As String = "ロット詳細名2"
  Public Const LotName3 As String = "ロット詳細名3"
  Public Const LotName4 As String = "ロット詳細名4"
  Public Const LotName5 As String = "ロット詳細名5"
  Public Const LotName6 As String = "ロット詳細名6"
  Public Const LotName7 As String = "ロット詳細名7"
  Public Const LotName8 As String = "ロット詳細名8"
  Public Const LotName9 As String = "ロット詳細名9"
  Public Const LotName10 As String = "ロット詳細名10"
  Public Const ManageNo As String = "管理番号"
  Public Const Rate As String = "税率"
  Public Const SystemLotNo As String = "システムロットNo"
  Public Const StockID As String = "在庫ID"
  Public Const FromStockID As String = "移動元在庫ID"
  Public Const CaseNumber As String = "入数"
  Public Const PrevNum As String = "修正前本数"
  Public Const PrevQty As String = "修正前数量"
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransLotInput
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransLotInput))
    Me.ServiceController1 = New System.ServiceProcess.ServiceController()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.FToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSave = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemNew = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemReset = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPrevious = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemNext = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRefer = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemDelete = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemCopy = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPrevious = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonNext = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonCreate = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonReset = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonDelete = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonCopy = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSave = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.HeaderLabel = New PCA.TSC.Kon.Tools.TscInputHeaderLabel()
    Me.ldtMoveDate = New Sunloft.PCAControls.SLPcaLabeledDate()
    Me.ltxtTransNo2 = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.setProduct = New PCA.Controls.PcaCodeSet()
    Me.setFromWarehouse = New PCA.Controls.PcaCodeSet()
    Me.setMemoCode = New PCA.Controls.PcaCodeSet()
    Me.setMemoContent = New PCA.Controls.PcaCodeSet()
    Me.ltxtCreatedBy = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtCreatedAt = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtModifiedBy = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtModifiedAt = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrevious = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandNext = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSlipSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandCopy = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSave = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemSave = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemNew = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemReset = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPrevious = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemNext = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSlipSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemDelete = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemCopy = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.tblTable = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.setTransNo = New PCA.Controls.PcaCodeSet()
    Me.ldtArrivalDate = New Sunloft.PCAControls.SLPcaLabeledDate()
    Me.setToWarehouse = New PCA.Controls.PcaCodeSet()
    Me.ltxtNumber = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtQuantity = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtUnit = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.SlPcaLabeledTextBox1 = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtBlank = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'FToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.FToolStripMenuItemFile, Nothing)
    Me.FToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSave, Me.ToolStripMenuItemClose})
    Me.FToolStripMenuItemFile.Name = "FToolStripMenuItemFile"
    Me.FToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.FToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSave, Me.PcaCommandItemSave)
    Me.ToolStripMenuItemSave.Name = "ToolStripMenuItemSave"
    Me.ToolStripMenuItemSave.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSave.Text = "登録(&S)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemNew, Me.ToolStripMenuItemReset, Me.ToolStripMenuItemPrevious, Me.ToolStripMenuItemNext, Me.ToolStripMenuItemSearch, Me.ToolStripMenuItemRefer, Me.ToolStripMenuItemDelete, Me.ToolStripMenuItemCopy})
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemNew
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemNew, Me.PcaCommandItemNew)
    Me.ToolStripMenuItemNew.Name = "ToolStripMenuItemNew"
    Me.ToolStripMenuItemNew.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemNew.Text = "新規入力(&N)"
    '
    'ToolStripMenuItemReset
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemReset, Me.PcaCommandItemReset)
    Me.ToolStripMenuItemReset.Name = "ToolStripMenuItemReset"
    Me.ToolStripMenuItemReset.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemReset.Text = "入力前に戻る(&O)"
    '
    'ToolStripMenuItemPrevious
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPrevious, Me.PcaCommandItemPrevious)
    Me.ToolStripMenuItemPrevious.Name = "ToolStripMenuItemPrevious"
    Me.ToolStripMenuItemPrevious.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemPrevious.Text = "前伝票(&B)"
    '
    'ToolStripMenuItemNext
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemNext, Me.PcaCommandItemNext)
    Me.ToolStripMenuItemNext.Name = "ToolStripMenuItemNext"
    Me.ToolStripMenuItemNext.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemNext.Text = "次伝票(&F)"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSlipSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemSearch.Text = "検索(&S)"
    '
    'ToolStripMenuItemRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRefer, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemRefer.Name = "ToolStripMenuItemRefer"
    Me.ToolStripMenuItemRefer.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemRefer.Text = "参照(&U)"
    '
    'ToolStripMenuItemDelete
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemDelete, Me.PcaCommandItemDelete)
    Me.ToolStripMenuItemDelete.Name = "ToolStripMenuItemDelete"
    Me.ToolStripMenuItemDelete.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemDelete.Text = "伝票削除(&X)"
    '
    'ToolStripMenuItemCopy
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemCopy, Me.PcaCommandItemCopy)
    Me.ToolStripMenuItemCopy.Name = "ToolStripMenuItemCopy"
    Me.ToolStripMenuItemCopy.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemCopy.Text = "伝票複写(&Y)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContent.Text = "目次(&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonPrevious, Me.ToolStripButtonNext, Me.ToolStripButtonCreate, Me.ToolStripButtonReset, Me.ToolStripButtonDelete, Me.ToolStripButtonCopy, Me.ToolStripButtonSave, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1008, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Image = Global.Pro04030.My.Resources.Resources._Exit
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPrevious
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPrevious, Me.PcaCommandItemPrevious)
    Me.ToolStripButtonPrevious.Image = Global.Pro04030.My.Resources.Resources.Previous
    Me.ToolStripButtonPrevious.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPrevious.Name = "ToolStripButtonPrevious"
    Me.ToolStripButtonPrevious.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonPrevious.Text = "前伝票"
    Me.ToolStripButtonPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonNext
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonNext, Me.PcaCommandItemNext)
    Me.ToolStripButtonNext.Image = Global.Pro04030.My.Resources.Resources.Following
    Me.ToolStripButtonNext.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonNext.Name = "ToolStripButtonNext"
    Me.ToolStripButtonNext.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonNext.Text = "次伝票"
    Me.ToolStripButtonNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonCreate
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonCreate, Me.PcaCommandItemNew)
    Me.ToolStripButtonCreate.Image = Global.Pro04030.My.Resources.Resources._New
    Me.ToolStripButtonCreate.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonCreate.Name = "ToolStripButtonCreate"
    Me.ToolStripButtonCreate.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonCreate.Text = "新規"
    Me.ToolStripButtonCreate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonReset
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonReset, Me.PcaCommandItemReset)
    Me.ToolStripButtonReset.Image = Global.Pro04030.My.Resources.Resources.Before
    Me.ToolStripButtonReset.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonReset.Name = "ToolStripButtonReset"
    Me.ToolStripButtonReset.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonReset.Text = "入力前"
    Me.ToolStripButtonReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonDelete
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonDelete, Me.PcaCommandItemDelete)
    Me.ToolStripButtonDelete.Image = Global.Pro04030.My.Resources.Resources.Delete
    Me.ToolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonDelete.Name = "ToolStripButtonDelete"
    Me.ToolStripButtonDelete.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonDelete.Text = "削除"
    Me.ToolStripButtonDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonCopy
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonCopy, Me.PcaCommandItemCopy)
    Me.ToolStripButtonCopy.Image = Global.Pro04030.My.Resources.Resources.Copy
    Me.ToolStripButtonCopy.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonCopy.Name = "ToolStripButtonCopy"
    Me.ToolStripButtonCopy.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonCopy.Text = "複写"
    Me.ToolStripButtonCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSave, Me.PcaCommandItemSave)
    Me.ToolStripButtonSave.Image = Global.Pro04030.My.Resources.Resources.save
    Me.ToolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSave.Name = "ToolStripButtonSave"
    Me.ToolStripButtonSave.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonSave.Text = "登録"
    Me.ToolStripButtonSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Image = Global.Pro04030.My.Resources.Resources.help
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'HeaderLabel
    '
    Me.HeaderLabel.BackColor = System.Drawing.SystemColors.Control
    Me.HeaderLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.HeaderLabel.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Bold)
    Me.HeaderLabel.FusenFlag = PCA.TSC.Kon.BusinessEntity.Defines.DenpyoFusenCommentType.Nashi
    Me.HeaderLabel.FusenText = ""
    Me.HeaderLabel.Location = New System.Drawing.Point(0, 61)
    Me.HeaderLabel.Name = "HeaderLabel"
    Me.HeaderLabel.ShowFusenIcon = True
    Me.HeaderLabel.ShowSyoninIcon = True
    Me.HeaderLabel.Size = New System.Drawing.Size(1008, 25)
    Me.HeaderLabel.State = PCA.Controls.LabelStateType.[New]
    Me.HeaderLabel.SyoninFlag = PCA.TSC.Kon.BusinessEntity.Defines.DenpyoSyoninFlagType.Misyonin
    Me.HeaderLabel.SyoninText = ""
    Me.HeaderLabel.TabIndex = 2
    Me.HeaderLabel.TabStop = False
    '
    'ldtMoveDate
    '
    Me.ldtMoveDate.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.ldtMoveDate.ClientProcess = Nothing
    Me.ldtMoveDate.EmptyIfDisabled = True
    Me.ldtMoveDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtMoveDate.Holidays = Nothing
    Me.ldtMoveDate.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.ldtMoveDate.LabelText = "移動日"
    Me.ldtMoveDate.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtMoveDate.LabelTopAlign = True
    Me.ldtMoveDate.Location = New System.Drawing.Point(12, 92)
    Me.ldtMoveDate.MaxTextLength = 20
    Me.ldtMoveDate.MustInput = False
    Me.ldtMoveDate.Name = "ldtMoveDate"
    Me.ldtMoveDate.Size = New System.Drawing.Size(140, 44)
    Me.ldtMoveDate.TabIndex = 1
    '
    'ltxtTransNo2
    '
    Me.ltxtTransNo2.AllowTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.ltxtTransNo2.AutoTextSize = False
    Me.ltxtTransNo2.ClientProcess = Nothing
    Me.ltxtTransNo2.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtTransNo2.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.ltxtTransNo2.LabelText = "伝票No2"
    Me.ltxtTransNo2.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtTransNo2.LabelTopAlign = True
    Me.ltxtTransNo2.LimitLength = 32
    Me.ltxtTransNo2.Location = New System.Drawing.Point(422, 92)
    Me.ltxtTransNo2.Margin = New System.Windows.Forms.Padding(21, 0, 0, 0)
    Me.ltxtTransNo2.MaxLabelLength = 15
    Me.ltxtTransNo2.MaxLength = 8
    Me.ltxtTransNo2.MaxTextLength = 15
    Me.ltxtTransNo2.MustInput = False
    Me.ltxtTransNo2.Name = "ltxtTransNo2"
    Me.ltxtTransNo2.Size = New System.Drawing.Size(120, 44)
    Me.ltxtTransNo2.TabIndex = 3
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 708)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1008, 22)
    Me.StatusStrip1.TabIndex = 25
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'setProduct
    '
    Me.setProduct.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setProduct.AutoRestore = True
    Me.setProduct.AutoTextSize = False
    Me.setProduct.AutoTopMargin = False
    Me.setProduct.ClientProcess = Nothing
    Me.setProduct.FillZeroFlag = True
    Me.setProduct.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setProduct.GroupBoxVisible = False
    Me.setProduct.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setProduct.LimitLength = 4
    Me.setProduct.Location = New System.Drawing.Point(12, 164)
    Me.setProduct.MaxCodeLength = 18
    Me.setProduct.MaxLabelLength = 15
    Me.setProduct.MaxNameLength = 30
    Me.setProduct.Name = "setProduct"
    Me.setProduct.Size = New System.Drawing.Size(465, 21)
    Me.setProduct.TabIndex = 4
    Me.setProduct.Text = "商品コード"
    Me.setProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setFromWarehouse
    '
    Me.setFromWarehouse.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setFromWarehouse.AutoRestore = True
    Me.setFromWarehouse.AutoTextSize = False
    Me.setFromWarehouse.AutoTopMargin = False
    Me.setFromWarehouse.ClientProcess = Nothing
    Me.setFromWarehouse.FillZeroFlag = True
    Me.setFromWarehouse.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setFromWarehouse.GroupBoxVisible = False
    Me.setFromWarehouse.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setFromWarehouse.LimitLength = 4
    Me.setFromWarehouse.Location = New System.Drawing.Point(12, 185)
    Me.setFromWarehouse.MaxCodeLength = 18
    Me.setFromWarehouse.MaxLabelLength = 15
    Me.setFromWarehouse.MaxNameLength = 30
    Me.setFromWarehouse.Name = "setFromWarehouse"
    Me.setFromWarehouse.Size = New System.Drawing.Size(465, 21)
    Me.setFromWarehouse.TabIndex = 5
    Me.setFromWarehouse.Text = "移動元倉庫"
    Me.setFromWarehouse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setMemoCode
    '
    Me.setMemoCode.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setMemoCode.AutoRestore = True
    Me.setMemoCode.AutoTextSize = False
    Me.setMemoCode.AutoTopMargin = False
    Me.setMemoCode.ClientProcess = Nothing
    Me.setMemoCode.FillZeroFlag = True
    Me.setMemoCode.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setMemoCode.GroupBoxVisible = False
    Me.setMemoCode.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setMemoCode.LimitLength = 4
    Me.setMemoCode.Location = New System.Drawing.Point(12, 269)
    Me.setMemoCode.MaxCodeLength = 18
    Me.setMemoCode.MaxLabelLength = 15
    Me.setMemoCode.MaxNameLength = 0
    Me.setMemoCode.Name = "setMemoCode"
    Me.setMemoCode.Size = New System.Drawing.Size(252, 22)
    Me.setMemoCode.TabIndex = 7
    Me.setMemoCode.Text = "摘要"
    Me.setMemoCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setMemoContent
    '
    Me.setMemoContent.AutoTextSize = False
    Me.setMemoContent.AutoTopMargin = False
    Me.setMemoContent.ClientProcess = Nothing
    Me.setMemoContent.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setMemoContent.GroupBoxVisible = False
    Me.setMemoContent.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setMemoContent.LimitLength = 60
    Me.setMemoContent.Location = New System.Drawing.Point(264, 269)
    Me.setMemoContent.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setMemoContent.MaxCodeLength = 30
    Me.setMemoContent.MaxLabelLength = 0
    Me.setMemoContent.MaxNameLength = 0
    Me.setMemoContent.Name = "setMemoContent"
    Me.setMemoContent.ReferButtonVisible = False
    Me.setMemoContent.Size = New System.Drawing.Size(207, 22)
    Me.setMemoContent.TabIndex = 8
    '
    'ltxtCreatedBy
    '
    Me.ltxtCreatedBy.AutoTextSize = False
    Me.ltxtCreatedBy.ClientProcess = Nothing
    Me.ltxtCreatedBy.Enabled = False
    Me.ltxtCreatedBy.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtCreatedBy.LabelText = "登録者"
    Me.ltxtCreatedBy.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtCreatedBy.Location = New System.Drawing.Point(521, 164)
    Me.ltxtCreatedBy.MaxLabelLength = 15
    Me.ltxtCreatedBy.MaxTextLength = 25
    Me.ltxtCreatedBy.MustInput = False
    Me.ltxtCreatedBy.Name = "ltxtCreatedBy"
    Me.ltxtCreatedBy.ReadOnly = True
    Me.ltxtCreatedBy.Size = New System.Drawing.Size(280, 21)
    Me.ltxtCreatedBy.TabIndex = 42
    Me.ltxtCreatedBy.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtCreatedAt
    '
    Me.ltxtCreatedAt.AutoTextSize = False
    Me.ltxtCreatedAt.ClientProcess = Nothing
    Me.ltxtCreatedAt.Enabled = False
    Me.ltxtCreatedAt.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtCreatedAt.LabelText = "登録日時"
    Me.ltxtCreatedAt.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtCreatedAt.Location = New System.Drawing.Point(521, 184)
    Me.ltxtCreatedAt.MaxLabelLength = 15
    Me.ltxtCreatedAt.MaxTextLength = 25
    Me.ltxtCreatedAt.MustInput = False
    Me.ltxtCreatedAt.Name = "ltxtCreatedAt"
    Me.ltxtCreatedAt.ReadOnly = True
    Me.ltxtCreatedAt.Size = New System.Drawing.Size(280, 21)
    Me.ltxtCreatedAt.TabIndex = 42
    Me.ltxtCreatedAt.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtModifiedBy
    '
    Me.ltxtModifiedBy.AutoTextSize = False
    Me.ltxtModifiedBy.ClientProcess = Nothing
    Me.ltxtModifiedBy.Enabled = False
    Me.ltxtModifiedBy.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtModifiedBy.LabelText = "更新者"
    Me.ltxtModifiedBy.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtModifiedBy.Location = New System.Drawing.Point(521, 204)
    Me.ltxtModifiedBy.MaxLabelLength = 15
    Me.ltxtModifiedBy.MaxTextLength = 25
    Me.ltxtModifiedBy.MustInput = False
    Me.ltxtModifiedBy.Name = "ltxtModifiedBy"
    Me.ltxtModifiedBy.ReadOnly = True
    Me.ltxtModifiedBy.Size = New System.Drawing.Size(280, 21)
    Me.ltxtModifiedBy.TabIndex = 42
    Me.ltxtModifiedBy.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtModifiedAt
    '
    Me.ltxtModifiedAt.AutoTextSize = False
    Me.ltxtModifiedAt.ClientProcess = Nothing
    Me.ltxtModifiedAt.Enabled = False
    Me.ltxtModifiedAt.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtModifiedAt.LabelText = "更新日時"
    Me.ltxtModifiedAt.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtModifiedAt.Location = New System.Drawing.Point(521, 224)
    Me.ltxtModifiedAt.MaxLabelLength = 15
    Me.ltxtModifiedAt.MaxTextLength = 25
    Me.ltxtModifiedAt.MustInput = False
    Me.ltxtModifiedAt.Name = "ltxtModifiedAt"
    Me.ltxtModifiedAt.ReadOnly = True
    Me.ltxtModifiedAt.Size = New System.Drawing.Size(280, 25)
    Me.ltxtModifiedAt.TabIndex = 42
    Me.ltxtModifiedAt.TextBackColor = System.Drawing.SystemColors.Control
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandPrevious, Me.PcaFunctionCommandNext, Me.PcaFunctionCommandSlipSearch, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandCopy, Me.PcaFunctionCommandClose, Me.PcaFunctionCommandSave})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 680)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1008, 28)
    Me.PcaFunctionBar1.TabIndex = 44
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    '
    'PcaFunctionCommandPrevious
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrevious, Me.PcaCommandItemPrevious)
    Me.PcaFunctionCommandPrevious.FunctionKey = PCA.Controls.FunctionKey.F2
    Me.PcaFunctionCommandPrevious.Tag = Nothing
    Me.PcaFunctionCommandPrevious.Text = "前伝票"
    '
    'PcaFunctionCommandNext
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandNext, Me.PcaCommandItemNext)
    Me.PcaFunctionCommandNext.FunctionKey = PCA.Controls.FunctionKey.F3
    Me.PcaFunctionCommandNext.Tag = Nothing
    Me.PcaFunctionCommandNext.Text = "次伝票"
    '
    'PcaFunctionCommandSlipSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSlipSearch, Me.PcaCommandItemSlipSearch)
    Me.PcaFunctionCommandSlipSearch.FunctionKey = PCA.Controls.FunctionKey.F6
    Me.PcaFunctionCommandSlipSearch.Tag = Nothing
    Me.PcaFunctionCommandSlipSearch.Text = "検索"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    '
    'PcaFunctionCommandCopy
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandCopy, Me.PcaCommandItemCopy)
    Me.PcaFunctionCommandCopy.FunctionKey = PCA.Controls.FunctionKey.F11
    Me.PcaFunctionCommandCopy.Tag = Nothing
    Me.PcaFunctionCommandCopy.Text = "伝票複写"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'PcaFunctionCommandSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSave, Me.PcaCommandItemSave)
    Me.PcaFunctionCommandSave.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandSave.Tag = Nothing
    Me.PcaFunctionCommandSave.Text = "登録"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemPrevious, Me.PcaCommandItemNext, Me.PcaCommandItemSlipSearch, Me.PcaCommandItemRefer, Me.PcaCommandItemCopy, Me.PcaCommandItemClose, Me.PcaCommandItemSave, Me.PcaCommandItemDelete, Me.PcaCommandItemNew, Me.PcaCommandItemReset})
    Me.PcaCommandManager1.Parent = Me
    '
    'PcaCommandItemSave
    '
    Me.PcaCommandItemSave.CommandId = 12
    Me.PcaCommandItemSave.CommandName = "Save"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    Me.PcaCommandItemClose.CommandName = "Close"
    '
    'PcaCommandItemNew
    '
    Me.PcaCommandItemNew.CommandName = "New"
    '
    'PcaCommandItemPrevious
    '
    Me.PcaCommandItemPrevious.CommandId = 2
    Me.PcaCommandItemPrevious.CommandName = "Previous"
    '
    'PcaCommandItemNext
    '
    Me.PcaCommandItemNext.CommandId = 3
    Me.PcaCommandItemNext.CommandName = "Next"
    '
    'PcaCommandItemSlipSearch
    '
    Me.PcaCommandItemSlipSearch.CommandId = 6
    Me.PcaCommandItemSlipSearch.CommandName = "Search"
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandId = 8
    Me.PcaCommandItemRefer.CommandName = "Refer"
    '
    'PcaCommandItemDelete
    '
    Me.PcaCommandItemDelete.CommandName = "Delete"
    '
    'PcaCommandItemCopy
    '
    Me.PcaCommandItemCopy.CommandId = 11
    Me.PcaCommandItemCopy.CommandName = "Copy"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    Me.PcaCommandItemHelp.CommandName = "Help"
    '
    'tblTable
    '
    Me.tblTable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tblTable.BEKihonJoho = CType(resources.GetObject("tblTable.BEKihonJoho"), PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblTable.ContextMenu = Nothing
    Me.tblTable.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.tblTable.HeadContextMenu = Nothing
    Me.tblTable.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblTable.Location = New System.Drawing.Point(12, 320)
    Me.tblTable.Name = "tblTable"
    Me.tblTable.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblTable.Size = New System.Drawing.Size(984, 336)
    Me.tblTable.TabIndex = 45
    Me.tblTable.UpDownKeyAsTab = True
    '
    'setTransNo
    '
    Me.setTransNo.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setTransNo.AutoTextSize = False
    Me.setTransNo.AutoTopMargin = False
    Me.setTransNo.ClientProcess = Nothing
    Me.setTransNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setTransNo.GroupBoxVisible = False
    Me.setTransNo.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setTransNo.LabelTopAlign = True
    Me.setTransNo.LimitLength = 9
    Me.setTransNo.Location = New System.Drawing.Point(310, 92)
    Me.setTransNo.Margin = New System.Windows.Forms.Padding(18, 0, 0, 0)
    Me.setTransNo.MaxCodeLength = 10
    Me.setTransNo.MaxNameLength = 0
    Me.setTransNo.Name = "setTransNo"
    Me.setTransNo.Size = New System.Drawing.Size(91, 44)
    Me.setTransNo.TabIndex = 0
    Me.setTransNo.TabStop = False
    Me.setTransNo.Text = "伝票No"
    Me.setTransNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.setTransNo.TrimEndMode = True
    Me.setTransNo.TrimStartMode = True
    '
    'ldtArrivalDate
    '
    Me.ldtArrivalDate.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.ldtArrivalDate.ClientProcess = Nothing
    Me.ldtArrivalDate.EmptyIfDisabled = True
    Me.ldtArrivalDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtArrivalDate.Holidays = Nothing
    Me.ldtArrivalDate.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.ldtArrivalDate.LabelText = "着日"
    Me.ldtArrivalDate.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtArrivalDate.LabelTopAlign = True
    Me.ldtArrivalDate.Location = New System.Drawing.Point(152, 92)
    Me.ldtArrivalDate.MaxTextLength = 20
    Me.ldtArrivalDate.MustInput = False
    Me.ldtArrivalDate.Name = "ldtArrivalDate"
    Me.ldtArrivalDate.Size = New System.Drawing.Size(140, 44)
    Me.ldtArrivalDate.TabIndex = 2
    '
    'setToWarehouse
    '
    Me.setToWarehouse.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setToWarehouse.AutoRestore = True
    Me.setToWarehouse.AutoTextSize = False
    Me.setToWarehouse.AutoTopMargin = False
    Me.setToWarehouse.ClientProcess = Nothing
    Me.setToWarehouse.FillZeroFlag = True
    Me.setToWarehouse.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setToWarehouse.GroupBoxVisible = False
    Me.setToWarehouse.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setToWarehouse.LimitLength = 4
    Me.setToWarehouse.Location = New System.Drawing.Point(12, 206)
    Me.setToWarehouse.MaxCodeLength = 18
    Me.setToWarehouse.MaxLabelLength = 15
    Me.setToWarehouse.MaxNameLength = 30
    Me.setToWarehouse.Name = "setToWarehouse"
    Me.setToWarehouse.Size = New System.Drawing.Size(465, 22)
    Me.setToWarehouse.TabIndex = 6
    Me.setToWarehouse.Text = "移動先倉庫"
    Me.setToWarehouse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'ltxtNumber
    '
    Me.ltxtNumber.AutoTextSize = False
    Me.ltxtNumber.ClientProcess = Nothing
    Me.ltxtNumber.Enabled = False
    Me.ltxtNumber.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtNumber.LabelText = "移動本数"
    Me.ltxtNumber.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtNumber.Location = New System.Drawing.Point(12, 227)
    Me.ltxtNumber.MaxLabelLength = 15
    Me.ltxtNumber.MaxTextLength = 25
    Me.ltxtNumber.MustInput = False
    Me.ltxtNumber.Name = "ltxtNumber"
    Me.ltxtNumber.ReadOnly = True
    Me.ltxtNumber.Size = New System.Drawing.Size(231, 21)
    Me.ltxtNumber.TabIndex = 47
    Me.ltxtNumber.Text = "0"
    Me.ltxtNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ltxtNumber.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtQuantity
    '
    Me.ltxtQuantity.AutoTextSize = False
    Me.ltxtQuantity.ClientProcess = Nothing
    Me.ltxtQuantity.Enabled = False
    Me.ltxtQuantity.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtQuantity.LabelText = "移動数量"
    Me.ltxtQuantity.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtQuantity.Location = New System.Drawing.Point(12, 248)
    Me.ltxtQuantity.MaxLabelLength = 15
    Me.ltxtQuantity.MaxTextLength = 25
    Me.ltxtQuantity.MustInput = False
    Me.ltxtQuantity.Name = "ltxtQuantity"
    Me.ltxtQuantity.ReadOnly = True
    Me.ltxtQuantity.Size = New System.Drawing.Size(231, 21)
    Me.ltxtQuantity.TabIndex = 48
    Me.ltxtQuantity.Text = "0"
    Me.ltxtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ltxtQuantity.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtUnit
    '
    Me.ltxtUnit.AutoTextSize = False
    Me.ltxtUnit.ClientProcess = Nothing
    Me.ltxtUnit.Enabled = False
    Me.ltxtUnit.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtUnit.LabelText = "TextBox"
    Me.ltxtUnit.Location = New System.Drawing.Point(242, 248)
    Me.ltxtUnit.MaxLabelLength = 0
    Me.ltxtUnit.MaxTextLength = 25
    Me.ltxtUnit.MustInput = False
    Me.ltxtUnit.Name = "ltxtUnit"
    Me.ltxtUnit.ReadOnly = True
    Me.ltxtUnit.Size = New System.Drawing.Size(232, 22)
    Me.ltxtUnit.TabIndex = 49
    Me.ltxtUnit.TextBackColor = System.Drawing.SystemColors.Control
    '
    'SlPcaLabeledTextBox1
    '
    Me.SlPcaLabeledTextBox1.AutoTextSize = False
    Me.SlPcaLabeledTextBox1.ClientProcess = Nothing
    Me.SlPcaLabeledTextBox1.Enabled = False
    Me.SlPcaLabeledTextBox1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.SlPcaLabeledTextBox1.LabelText = "TextBox"
    Me.SlPcaLabeledTextBox1.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.SlPcaLabeledTextBox1.Location = New System.Drawing.Point(242, 227)
    Me.SlPcaLabeledTextBox1.MaxLabelLength = 0
    Me.SlPcaLabeledTextBox1.MaxTextLength = 25
    Me.SlPcaLabeledTextBox1.MustInput = False
    Me.SlPcaLabeledTextBox1.Name = "SlPcaLabeledTextBox1"
    Me.SlPcaLabeledTextBox1.ReadOnly = True
    Me.SlPcaLabeledTextBox1.Size = New System.Drawing.Size(232, 22)
    Me.SlPcaLabeledTextBox1.TabIndex = 50
    Me.SlPcaLabeledTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.SlPcaLabeledTextBox1.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtBlank
    '
    Me.ltxtBlank.AutoTextSize = False
    Me.ltxtBlank.ClientProcess = Nothing
    Me.ltxtBlank.Enabled = False
    Me.ltxtBlank.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtBlank.LabelText = "TextBox"
    Me.ltxtBlank.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtBlank.Location = New System.Drawing.Point(242, 269)
    Me.ltxtBlank.MaxLabelLength = 0
    Me.ltxtBlank.MaxTextLength = 25
    Me.ltxtBlank.MustInput = False
    Me.ltxtBlank.Name = "ltxtBlank"
    Me.ltxtBlank.ReadOnly = True
    Me.ltxtBlank.Size = New System.Drawing.Size(232, 22)
    Me.ltxtBlank.TabIndex = 51
    Me.ltxtBlank.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ltxtBlank.TextBackColor = System.Drawing.SystemColors.Control
    '
    'frmTransLotInput
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1008, 730)
    Me.Controls.Add(Me.SlPcaLabeledTextBox1)
    Me.Controls.Add(Me.ltxtUnit)
    Me.Controls.Add(Me.ltxtQuantity)
    Me.Controls.Add(Me.ltxtNumber)
    Me.Controls.Add(Me.setToWarehouse)
    Me.Controls.Add(Me.ldtArrivalDate)
    Me.Controls.Add(Me.tblTable)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.ltxtModifiedAt)
    Me.Controls.Add(Me.ltxtModifiedBy)
    Me.Controls.Add(Me.ltxtCreatedAt)
    Me.Controls.Add(Me.ltxtCreatedBy)
    Me.Controls.Add(Me.setProduct)
    Me.Controls.Add(Me.setFromWarehouse)
    Me.Controls.Add(Me.setMemoCode)
    Me.Controls.Add(Me.setMemoContent)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.ldtMoveDate)
    Me.Controls.Add(Me.setTransNo)
    Me.Controls.Add(Me.ltxtTransNo2)
    Me.Controls.Add(Me.HeaderLabel)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Controls.Add(Me.ltxtBlank)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1024, 768)
    Me.Name = "frmTransLotInput"
    Me.Text = "振替ロット入力"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents ServiceController1 As System.ServiceProcess.ServiceController
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents FToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSave As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemNew As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemReset As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPrevious As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemNext As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemRefer As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemDelete As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemCopy As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPrevious As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonNext As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonCreate As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonReset As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonDelete As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonCopy As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonSave As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents HeaderLabel As PCA.TSC.Kon.Tools.TscInputHeaderLabel
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents setProduct As PCA.Controls.PcaCodeSet
  Friend WithEvents setFromWarehouse As PCA.Controls.PcaCodeSet
  Friend WithEvents setMemoCode As PCA.Controls.PcaCodeSet
  Friend WithEvents setMemoContent As PCA.Controls.PcaCodeSet
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents ltxtCreatedBy As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemPrevious As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemNext As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSlipSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemCopy As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSave As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandPrevious As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandNext As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSlipSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandCopy As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSave As PCA.Controls.PcaFunctionCommand
  Friend WithEvents tblTable As PCA.TSC.Kon.Tools.TscMeisaiTable
  Private WithEvents setTransNo As PCA.Controls.PcaCodeSet
  Friend WithEvents ldtMoveDate As Sunloft.PCAControls.SLPcaLabeledDate
  Friend WithEvents ltxtTransNo2 As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtCreatedAt As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtModifiedBy As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtModifiedAt As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents PcaCommandItemDelete As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemNew As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemReset As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents ldtArrivalDate As Sunloft.PCAControls.SLPcaLabeledDate
  Friend WithEvents ltxtNumber As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents setToWarehouse As PCA.Controls.PcaCodeSet
  Friend WithEvents ltxtQuantity As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents SlPcaLabeledTextBox1 As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtUnit As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtBlank As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip

End Class

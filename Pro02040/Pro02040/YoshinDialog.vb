﻿
''' <summary>与信限度額チェック画面クラス
''' </summary>
Public Class YoshinDialog
  Inherits PCA.UITools.PcaDialog
  ''' <summary>更新かどうか。
  ''' </summary>
  Private m_IsModifying As Boolean = False

  ''' <summary>与信限度額に表示するテキストを取得、設定。
  ''' </summary>
  Public Property YoshinGendogaku() As String
    Get
      Return Me.TextBoxYoshin.Text
    End Get
    Set(value As String)
      Me.TextBoxYoshin.Text = value
    End Set
  End Property

  ''' <summary>売掛残高に表示するテキストを取得、設定。
  ''' </summary>
  Public Property UrikakekinZandaka() As String
    Get
      Return Me.TextBoxUrikakekinZandaka.Text
    End Get
    Set(value As String)
      Me.TextBoxUrikakekinZandaka.Text = value
    End Set
  End Property

  ''' <summary>取引限度額に表示するテキストを取得、設定。
  ''' </summary>
  Public Property TorihikiGendogaku() As String
    Get
      Return Me.TextBoxTori.Text
    End Get
    Set(value As String)
      Me.TextBoxTori.Text = value
    End Set
  End Property

  ''' <summary>警告ラインの取引限度額に表示するテキストを取得、設定。
  ''' </summary>
  Public Property LineGendogaku() As String
    Get
      Return Me.TextBoxLineGendogaku.Text
    End Get
    Set(value As String)
      Me.TextBoxLineGendogaku.Text = value
    End Set
  End Property

  ''' <summary>コンストラクタ
  ''' </summary>
  ''' <param name="isModifying">更新かどうか</param>
  Public Sub New(isModifying As Boolean)
    '
    ' この呼び出しはデザイナーで必要です。
    InitializeComponent()

    m_IsModifying = isModifying

  End Sub



  ''' <summary>ロードイベント
  ''' </summary>
  Private Sub YoshinDialog_Load(sender As System.Object, e As System.EventArgs) Handles Me.Load
    '
    If String.IsNullOrEmpty(LineGendogaku) Then
      '与信限度額の超過時
      Me.LabelMessage.Text = "売上額が取引限度額を超えました。"
      Me.FlowLayoutPanel.Controls.Remove(Me.TextBoxLineGendogaku)
    Else
      '警告ラインの取引額の超過時
      Me.LabelMessage.Text = "売上額が警告ラインの取引限度額を超えました。"
      Me.TextBoxLineGendogaku.Text = LineGendogaku

    End If

    Me.FlowLayoutPanel.Controls.Remove(Me.LabelImpossibleMessage)

    If m_IsModifying Then
      '更新時は文言を変更
      Me.LabelImpossibleMessage.Text = Me.LabelImpossibleMessage.Text.Replace(PCA.TSC.Kon.BusinessEntity.ItemText.Toroku, PCA.TSC.Kon.BusinessEntity.ItemText.Kosin)
    End If

    Me.ClientSize = New Size(Me.ClientSize.Width, Me.FlowLayoutPanel.Height + Me.ButtonOK.Height + Me.StatusBar.Height + 36)

  End Sub

  ''' <summary>コントロールを再配置、フォームサイズを再設定。
  ''' </summary>
  Protected Overrides Sub UpdateLayout()
    '
    MyBase.UpdateLayout()

    Me.ClientSize = New Size(Me.ClientSize.Width, Me.FlowLayoutPanel.Height + Me.ButtonOK.Height + Me.StatusBar.Height + 36)

  End Sub

End Class
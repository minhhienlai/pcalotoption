﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.PCAIF
Imports Sunloft.Common
Imports PCA.UITools
Imports PCA.Controls
Imports PCA.TSC.Kon.Tools
Imports PCA.TSC.Kon.Tools.PEKonInput

Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports PCA.TSC.Kon
Imports PCA
Imports System.Globalization

Public Class frmList

#Region " Const & Param"
  Private Const WIDTH_SHIP_DATE As Integer = 100
  Private Const WIDTH_SALES_DATE As Integer = 100
  Private Const WIDTH_CUS_CODE As Integer = 70
  Private Const WIDTH_CUS_NAME As Integer = 200
  Private Const WIDTH_PRODUCT_CODE As Integer = 70
  Private Const WIDTH_PRODUCT_NAME As Integer = 200
  Private Const WIDTH_IS_CONVERT As Integer = 50

  Private Const WIDTH_QUANTITY As Integer = 80
  Private Const WIDTH_UNIT As Integer = 50
  Private Const WIDTH_UNIT_PRICEG As Integer = 120
  Private Const WIDTH_UNIT_PRICE As Integer = 120
  Private Const WIDTH_LOT_NO As Integer = 150
  Private Const WIDTH_LOT_DETAILS As Integer = 100
  Private Const WIDTH_SLIP_NO As Integer = 150

  Private Const HEIGHT_HEADER As Integer = 25

  Private Const TEXT_TYPE As String = "TEXT_TYPE"
  Private Const CHECKBOX_TYPE As String = "CHECKBOX_TYPE"
  Private Const CALENDAR_TYPE As String = "CALENDAR_TYPE"

  Private Enum ColumnIndex
    ConvertFlag = 0
    ShipDate
    SalesDate
    SupCode
    SupName
    ProductCode
    ProductName
    Qty
    UnitName
    UnitPriceG
    UnitPrice
    SlipNo
    DetailNo
    LotNo
    Total
    MaxUnitPriceLength
  End Enum

  Private connector As IIntegratedApplication = Nothing
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private SL_LMBClass As SL_LMB
  Private m_conf As Sunloft.PcaConfig
  Private Results As ColumnResult() = {New ColumnResult}
  Private duplicatedCell(,) As Boolean 'If a cell content is the same as the one above (not display)

  Private arrLotDetails(10) As String
  Private m_isNoOfChecked As Integer = 0

  Private m_Controller() As InputSYKController
  Private m_SlipNoColumn As Integer
  Private m_intCurRowIndex As Integer = -1
  Private m_intCurColumnIndex As Integer = -1
  Private m_strCurCellValue As String = String.Empty
  Private m_isDataSet As Boolean = False

  Private m_isDateFocusedFlag As Boolean = False

#End Region

#Region "Initialize"

  Public Sub New()
    Try
      InitializeComponent()
      m_conf = New Sunloft.PcaConfig
      m_appClass.ConnectToPCA()
      connector = m_appClass.connector

      SL_LMBClass = New SL_LMB(connector)
      SL_LMBClass.ReadOnlyRow()
      arrLotDetails = {SL_LMBClass.sl_lmb_label1, SL_LMBClass.sl_lmb_label2, SL_LMBClass.sl_lmb_label3, SL_LMBClass.sl_lmb_label4, SL_LMBClass.sl_lmb_label5 _
                      , SL_LMBClass.sl_lmb_label6, SL_LMBClass.sl_lmb_label7, SL_LMBClass.sl_lmb_label8, SL_LMBClass.sl_lmb_label9, SL_LMBClass.sl_lmb_label10}

      m_SlipNoColumn = SL_LMBClass.sl_lmb_maxlotop + ColumnIndex.Total
      ldtShipDate.Date = Today
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    InitTable(True)
    MyBase.OnLoad(e)

    Search()
  End Sub

  Private Sub InitTable(Optional isEmpty As Boolean = False)
    Try
      dgv.Columns.Clear()

      If isEmpty Then
        InitTableColumn(TEXT_TYPE, ColumnName.Convert, WIDTH_IS_CONVERT, DataGridViewContentAlignment.MiddleCenter, , isEmpty) 'Do not display checkbox in Empty table
      Else
        InitTableColumn(CHECKBOX_TYPE, ColumnName.Convert, WIDTH_IS_CONVERT, DataGridViewContentAlignment.MiddleCenter, , isEmpty)
      End If
      InitTableColumn(TEXT_TYPE, ColumnName.ShipDate, WIDTH_SHIP_DATE, DataGridViewContentAlignment.MiddleCenter)
      InitTableColumn(CALENDAR_TYPE, ColumnName.SalesDate, WIDTH_SALES_DATE, DataGridViewContentAlignment.MiddleCenter, , False)
      InitTableColumn(TEXT_TYPE, ColumnName.CusCode, WIDTH_CUS_CODE, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.CusName, WIDTH_CUS_NAME, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.ProductCode, WIDTH_PRODUCT_CODE, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.ProductName, WIDTH_PRODUCT_NAME, DataGridViewContentAlignment.MiddleLeft)

      InitTableColumn(TEXT_TYPE, ColumnName.Quantity, WIDTH_QUANTITY, DataGridViewContentAlignment.MiddleRight)
      InitTableColumn(TEXT_TYPE, ColumnName.UnitName, WIDTH_UNIT, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.UnitPriceG, WIDTH_UNIT_PRICEG, DataGridViewContentAlignment.MiddleRight)
      InitTableColumn(TEXT_TYPE, ColumnName.UnitPrice, WIDTH_UNIT_PRICE, DataGridViewContentAlignment.MiddleRight, , isEmpty)
      InitTableColumn(TEXT_TYPE, ColumnName.SlipNo, WIDTH_SLIP_NO)
      InitTableColumn(TEXT_TYPE, ColumnName.DetailsID, WIDTH_SLIP_NO)
      dgv.Columns(ColumnName.SlipNo).Visible = False
      dgv.Columns(ColumnName.DetailsID).Visible = False
      InitTableColumn(TEXT_TYPE, ColumnName.LotNo, WIDTH_LOT_NO, DataGridViewContentAlignment.MiddleLeft)

      For intCount As Integer = 0 To SL_LMBClass.sl_lmb_maxlotop - 1
        InitTableColumn(TEXT_TYPE, arrLotDetails(intCount), WIDTH_LOT_DETAILS, DataGridViewContentAlignment.MiddleLeft)
      Next

      'Set editable column color
      SLCmnFunction.SetEditableColumnColor(dgv, ColumnIndex.SalesDate, Nothing)
      SLCmnFunction.SetEditableColumnColor(dgv, ColumnIndex.UnitPrice, Nothing)

      'Set header cell font color to white
      For intCount As Integer = 0 To m_SlipNoColumn - 1
        dgv.Columns(intCount).HeaderCell.Style.ForeColor = Color.White
      Next
      'Set header size (Default size is a bit small)
      dgv.ColumnHeadersHeight = HEIGHT_HEADER
      dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing

      If isEmpty Then
        dgv.Rows.Clear()
      End If
      'Set comma for quantity and unit price column
      dgv.Columns(ColumnIndex.Qty).DefaultCellStyle.Format = "N4"
      dgv.Columns(ColumnIndex.UnitPriceG).DefaultCellStyle.Format = "N4"
      dgv.Columns(ColumnIndex.UnitPrice).DefaultCellStyle.Format = "N4"

      'Set table not sortable
      For Each column As DataGridViewColumn In dgv.Columns
        column.SortMode = DataGridViewColumnSortMode.NotSortable
      Next

      PcaFunctionCommandExecute.Enabled = False
      PcaFunctionCommandSelectAll.Enabled = False
      PcaFunctionCommandUnselectAll.Enabled = False

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitTableColumn(strHeaderType As String, strHeaderText As String, intColumnwidth As Integer, _
                              Optional Alignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft, Optional strColumnname As String = "", _
                              Optional isReadonly As Boolean = True)
    Dim col As New DataGridViewColumn
    Select Case strHeaderType
      Case CALENDAR_TYPE
        col = New CalendarColumn()
      Case TEXT_TYPE
        col = New DataGridViewTextBoxColumn
      Case CHECKBOX_TYPE
        col = New DataGridViewCheckBoxColumn
      Case Else
        col = New DataGridViewTextBoxColumn
    End Select
    col.ReadOnly = isReadonly
    If strColumnname = String.Empty Then
      col.Name = strHeaderText
    Else
      col.Name = strColumnname
    End If
    col.Width = intColumnwidth
    col.DefaultCellStyle.Alignment = Alignment
    dgv.Columns.Add(col)
  End Sub
#End Region

#Region "PCA Command Manager"

  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command

    Dim commandItem As PcaCommandItem = e.CommandItem
    Try
      Select Case True
        Case commandItem Is PcaCommandItemClose
          Me.Close()
        Case commandItem Is PcaCommandItemSearch
          PcaFunctionBar1.Focus()
          Search()
        Case commandItem Is PcaCommandItemExecute
          PcaFunctionBar1.Focus() 'To validate all other controllers
          ExecuteConvert()
        Case commandItem Is PcaCommandItemSelectAll
          ldtShipDate.Focus()
          SelectAll()
        Case commandItem Is PcaCommandItemUnselectAll
          ldtShipDate.Focus()
          UnselectAll()
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region "Private Method"

  Private Sub SelectAll()
    For intCount As Integer = 0 To Results.Count - 1
      dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ConvertFlag).Value = True
    Next
    If Results.Length > 0 Then PcaFunctionCommandExecute.Enabled = True
    m_isNoOfChecked = Results.Count
  End Sub

  Private Sub UnselectAll()
    For intCount As Integer = 0 To Results.Count - 1
      dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ConvertFlag).Value = False
    Next
    PcaFunctionCommandExecute.Enabled = False
    m_isNoOfChecked = 0
  End Sub

  Private Sub Search()
    GetSearchResult()

    If Results Is Nothing Then
      SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, _
                                Me, PcaCommandItemSearch.CommandId)
      dgv.Columns.Clear()
      InitTable(True)
      PcaFunctionCommandExecute.Enabled = False
      PcaFunctionCommandSelectAll.Enabled = False
      PcaFunctionCommandUnselectAll.Enabled = False
      m_isNoOfChecked = 0
    Else
      DisplaySearchResult()
      PcaFunctionCommandExecute.Enabled = True
      PcaFunctionCommandSelectAll.Enabled = True
      PcaFunctionCommandUnselectAll.Enabled = True
      m_isNoOfChecked = Results.Count
    End If

    m_isDataSet = True

  End Sub

  Private Function SummarizeData() As SortResult()

    Dim dataTable As DataTable = New DataTable
    Dim sortResult As SortResult() = {New SortResult()}
    Dim DiffPriceCount As Integer = 0 '単価カウント
    Dim DiffSalesSlipCount As Integer = 0 '売上伝票カウント(同じ出荷伝票にある詳細は、売上が違う場合、違う売上伝票に振替される）
    'Sales slip after convert (Items on 1 shipping slip SL_SYKH with different sales date will be converted to different sales slip) 
    dataTable.Columns.Add("detailsID")
    dataTable.Columns.Add("sl_sykd_suryo")
    dataTable.Columns.Add("sl_sykd_arari")
    dataTable.Columns.Add("sl_sykd_genka")
    dataTable.Columns.Add("sl_sykd_kingaku")
    'Item to compare to decide summarization　比較の為アイテム
    dataTable.Columns.Add("slipNo")
    dataTable.Columns.Add("SalesDate")
    dataTable.Columns.Add("sl_sykd_scd")
    dataTable.Columns.Add("sl_sykd_mei")
    dataTable.Columns.Add("sl_sykd_kikaku")
    dataTable.Columns.Add("sl_sykd_color")
    dataTable.Columns.Add("sl_sykd_size")
    dataTable.Columns.Add("sl_sykd_souko")
    dataTable.Columns.Add("sl_sykd_tax")
    dataTable.Columns.Add("sl_sykd_komi")
    dataTable.Columns.Add("sl_sykd_iri")
    dataTable.Columns.Add("sl_sykd_tani")
    dataTable.Columns.Add("sl_sykd_tanka")
    dataTable.Columns.Add("sl_sykd_gentan")
    dataTable.Columns.Add("DiffPriceCount")
    dataTable.Columns.Add("DiffSalesSlipCount")

    For intCount As Integer = 0 To Results.Length - 1
      If CBool(dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ConvertFlag).Value) Then

        'Check sales date, shipping slip no -> Decide DiffSalesSlipCount, check product code, unit price -> Decide DiffPriceCount
        Dim m_SL_SYKDitem As SL_SYKD = New SL_SYKD(connector)
        m_SL_SYKDitem.ReadByID(CInt(dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.DetailNo).Value))

        If IsSameSalesSlipNumber(dataTable, intCount, DiffSalesSlipCount) = -1 Then
          'New slip
          DiffSalesSlipCount += 1
          DiffPriceCount += 1
        Else
          If IsSameUnitPriceNumber(dataTable, intCount, DiffSalesSlipCount, DiffPriceCount, m_SL_SYKDitem) = -1 Then
            'Old slip, new slip details
            DiffPriceCount += 1
          End If
        End If

        CreateNewRow(dataTable, Results, intCount, DiffSalesSlipCount, DiffPriceCount, m_SL_SYKDitem)
      End If
    Next

    'Sort marked data 
    Dim sortDataRow As DataRow()
    sortDataRow = dataTable.Select(String.Empty, "DiffSalesSlipCount, DiffPriceCount")

    'Summarize duplicate data
    For intDataRowCount As Integer = 0 To sortDataRow.Length - 1
      'If next row belongs to different sales slip or in the same sales slip with different slip details -> New row
      If intDataRowCount = 0 _
       OrElse (sortDataRow(intDataRowCount).Item("DiffSalesSlipCount").ToString.Trim <> sortDataRow(intDataRowCount - 1).Item("DiffSalesSlipCount").ToString.Trim _
       OrElse (sortDataRow(intDataRowCount).Item("DiffSalesSlipCount").ToString.Trim = sortDataRow(intDataRowCount - 1).Item("DiffSalesSlipCount").ToString.Trim _
       AndAlso sortDataRow(intDataRowCount).Item("DiffPriceCount").ToString.Trim <> sortDataRow(intDataRowCount - 1).Item("DiffPriceCount").ToString.Trim)) Then

        If intDataRowCount > 0 Then ReDim Preserve sortResult(sortResult.Count)
        Dim sortResultItem As SortResult = New SortResult
        sortResultItem.intSlipNo = CInt(sortDataRow(intDataRowCount).Item("slipNo"))
        sortResultItem.intDetailsID = CInt(sortDataRow(intDataRowCount).Item("detailsID"))
        sortResultItem.dteSalesDate = CDate(sortDataRow(intDataRowCount).Item("SalesDate"))
        sortResultItem.decUnitPrice = CDec(sortDataRow(intDataRowCount).Item("sl_sykd_tanka"))
        sortResultItem.decQty = CDec(sortDataRow(intDataRowCount).Item("sl_sykd_suryo"))
        sortResultItem.decAmount = CInt(sortDataRow(intDataRowCount).Item("sl_sykd_kingaku"))
        sortResultItem.decCost = CInt(sortDataRow(intDataRowCount).Item("sl_sykd_genka"))
        sortResultItem.decProfit = CInt(sortDataRow(intDataRowCount).Item("sl_sykd_arari"))
        sortResult(sortResult.Count - 1) = sortResultItem
      Else
        sortResult(sortResult.Count - 1).decQty += CDec(sortDataRow(intDataRowCount).Item("sl_sykd_suryo"))
        sortResult(sortResult.Count - 1).decAmount += CDec(sortDataRow(intDataRowCount).Item("sl_sykd_kingaku"))
        sortResult(sortResult.Count - 1).decCost += CDec(sortDataRow(intDataRowCount).Item("sl_sykd_genka"))
        sortResult(sortResult.Count - 1).decProfit += CDec(sortDataRow(intDataRowCount).Item("sl_sykd_arari"))
      End If
    Next
    Return sortResult
  End Function

  Public Sub CreateNewRow(Datatable As DataTable, m_result As ColumnResult(), intcount As Integer, DiffSalesSlipCount As Integer, ByRef DiffPriceCount As Integer, _
                          m_SL_SYKDitem As SL_SYKD)
    Dim dr As DataRow = Datatable.NewRow
    dr("slipNo") = m_result(intcount).SlipNo
    dr("detailsID") = m_result(intcount).DetailsID
    dr("sl_sykd_suryo") = m_result(intcount).Qty
    dr("sl_sykd_genka") = m_SL_SYKDitem.sl_sykd_genka
    dr("sl_sykd_kingaku") = CInt(CDec(dgv.Rows.Item(intcount).Cells.Item(ColumnIndex.UnitPrice).Value) * m_result(intcount).Qty) '再計算金額、原価金額、粗利
    dr("SalesDate") = dgv.Rows.Item(intcount).Cells.Item(ColumnIndex.SalesDate).Value
    dr("sl_sykd_scd") = m_SL_SYKDitem.sl_sykd_scd
    dr("sl_sykd_mei") = m_SL_SYKDitem.sl_sykd_mei
    dr("sl_sykd_kikaku") = m_SL_SYKDitem.sl_sykd_kikaku
    dr("sl_sykd_color") = m_SL_SYKDitem.sl_sykd_color
    dr("sl_sykd_size") = m_SL_SYKDitem.sl_sykd_size
    dr("sl_sykd_souko") = m_SL_SYKDitem.sl_sykd_souko
    dr("sl_sykd_tax") = m_SL_SYKDitem.sl_sykd_tax
    dr("sl_sykd_komi") = m_SL_SYKDitem.sl_sykd_komi
    dr("sl_sykd_iri") = m_SL_SYKDitem.sl_sykd_iri
    dr("sl_sykd_tani") = m_SL_SYKDitem.sl_sykd_tani
    dr("sl_sykd_tanka") = CDec(dgv.Rows.Item(intcount).Cells.Item(ColumnIndex.UnitPrice).Value)
    dr("sl_sykd_gentan") = m_SL_SYKDitem.sl_sykd_genka * m_result(intcount).Qty
    dr("sl_sykd_arari") = CInt(dr("sl_sykd_kingaku")) - CInt(dr("sl_sykd_genka"))
    dr("DiffSalesSlipCount") = DiffSalesSlipCount
    dr("DiffPriceCount") = DiffPriceCount
    Datatable.Rows.Add(dr)
  End Sub

  'Search if there's any row in Datatable that has the same slip no and sales date with current row (So that it can be summarize to one Sales Slip) (SYKH)
  Public Function IsSameSalesSlipNumber(datatable As DataTable, intCount As Integer, ByRef DiffSalesSlipCount As Integer) As Integer
    For Each dr As DataRow In datatable.Rows
      If CInt(dr("slipNo")) = CInt(dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.SlipNo).Value) AndAlso _
       CDate(dr("SalesDate")) = CDate(dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.SalesDate).Value) Then
        DiffSalesSlipCount = CInt(dr("DiffSalesSlipCount"))
        Return DiffSalesSlipCount
      End If
    Next
    Return -1
  End Function

  'Search if there's any row in Datatable that current row can summarize to one Sales Slip Details (SYKD)
  'In order to summarize to one Sales slip details, 2 rows must be in the same Sales slip and has the same product and has the same price
  Public Function IsSameUnitPriceNumber(datatable As DataTable, intCount As Integer, DiffSalesSlipCount As Integer, ByRef DiffPriceCount As Integer, _
                                        m_SL_SYKDitem As SL_SYKD) As Integer
    For Each dr As DataRow In datatable.Rows
      If CInt(dr("DiffSalesSlipCount")) = DiffSalesSlipCount AndAlso _
        dr("sl_sykd_scd").ToString = m_SL_SYKDitem.sl_sykd_scd.ToString AndAlso _
        dr("sl_sykd_mei").ToString = m_SL_SYKDitem.sl_sykd_mei AndAlso _
        dr("sl_sykd_kikaku").ToString = m_SL_SYKDitem.sl_sykd_kikaku AndAlso _
        dr("sl_sykd_color").ToString = m_SL_SYKDitem.sl_sykd_color AndAlso _
        dr("sl_sykd_size").ToString = m_SL_SYKDitem.sl_sykd_size AndAlso _
        dr("sl_sykd_souko").ToString = m_SL_SYKDitem.sl_sykd_souko AndAlso _
        CInt(dr("sl_sykd_tax")) = m_SL_SYKDitem.sl_sykd_tax AndAlso _
        CInt(dr("sl_sykd_komi")) = m_SL_SYKDitem.sl_sykd_komi AndAlso _
        CDec(dr("sl_sykd_iri")) = m_SL_SYKDitem.sl_sykd_iri AndAlso _
        dr("sl_sykd_tani").ToString = m_SL_SYKDitem.sl_sykd_tani AndAlso _
        CDec(dr("sl_sykd_gentan")) = m_SL_SYKDitem.sl_sykd_gentan AndAlso _
        CDec(dr("sl_sykd_tanka")) = CInt(dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitPrice).Value) Then

        DiffPriceCount = CInt(dr("DiffPriceCount"))
        Return DiffPriceCount
      End If
    Next
    Return -1
  End Function

  Private Function ExecuteConvert() As Boolean
    Try
      Dim result As Integer = MsgBox(SLConstants.ConfirmMessage.CONVERT_CONFIRM_MESSAGE, MsgBoxStyle.YesNo, "確認")

      If result <> DialogResult.Yes Then Return False

      Dim sortResult As SortResult() = SummarizeData()
      Dim intDetailCnt As Integer = 0
      Dim intHeadCnt As Integer = -1
      Dim SL_SYKHClass As SL_SYKH = New SL_SYKH(connector)
      Dim SL_SYKDArray As SL_SYKD() = {New SL_SYKD(connector)}
      Dim SMSClass = New SMS(connector)
      Dim SMSPClass = New SMSP(connector)
      Dim entity As PEKonInput
      Dim SL_SYKDClass = New SL_SYKD(connector)
      Dim intDataCount As Integer
      Dim isSuccess As Boolean = True

      Dim session As ICustomSession = connector.CreateTransactionalSession

      m_Controller = {New InputSYKController(Me)}

      For intCount As Integer = 0 To sortResult.Count - 1
        If intCount = 0 OrElse Not (sortResult(intCount).intSlipNo = sortResult(intCount - 1).intSlipNo AndAlso sortResult(intCount).dteSalesDate = sortResult(intCount - 1).dteSalesDate) Then 'Start a new slip
          intDetailCnt = 0
          intHeadCnt += 1

          ReDim Preserve m_Controller(intHeadCnt + 1)

          entity = m_Controller(intHeadCnt).CreateInitialEntity()
          'Create Header
          SL_SYKHClass.ReadByID(sortResult(intCount).intSlipNo)

          If intHeadCnt > 0 Then ReDim Preserve m_Controller(m_Controller.Count)
          m_Controller(m_Controller.Count - 1) = New InputSYKController(Me)

          m_Controller(intHeadCnt).CommonVariable.IApplication = connector
          m_Controller(intHeadCnt).UpdateActiveEntity(entity)

          m_Controller(intHeadCnt).ModifiedEntity.Header.Denku = DenkuType.Kake
          m_Controller(intHeadCnt).ModifiedEntity.Header.DenpyoHizuke = CInt(CDate(sortResult(intCount).dteSalesDate).ToString("yyyyMMdd"))
          m_Controller(intHeadCnt).ModifiedEntity.Header.Hizuke = CInt(CDate(sortResult(intCount).dteSalesDate).ToString("yyyyMMdd"))
          m_Controller(intHeadCnt).ModifiedEntity.Header.TorihikisakiCode = SL_SYKHClass.sl_sykh_tcd.Trim

          'Get 与信限度額
          Dim TMS As New TMS(connector)
          TMS.ReadByCode(m_Controller(intHeadCnt).ModifiedEntity.Header.TorihikisakiCode)
          m_Controller(intHeadCnt).ModifiedEntity.Header.YoshinGendogaku = TMS.tms_yosin

          If SL_SYKHClass.sl_sykh_datakbn = 1 Then
            m_Controller(intHeadCnt).ModifiedEntity.Header.MitsumoriJuchuKubun = MitsumoriJuchuKubunType.Juchu
            m_Controller(intHeadCnt).ModifiedEntity.Header.RendoDenpyoNo = SL_SYKHClass.sl_sykh_dataid
          End If
          m_Controller(intHeadCnt).ModifiedEntity.Header.DenpyoNo2 = SL_SYKHClass.sl_sykh_denno2.Trim

          m_Controller(intHeadCnt).ModifiedEntity.Header.TantosyaCode = SL_SYKHClass.sl_sykh_jtan.Trim
          m_Controller(intHeadCnt).ModifiedEntity.Header.BumonCode = SL_SYKHClass.sl_sykh_jbmn.Trim
          m_Controller(intHeadCnt).ModifiedEntity.Header.TekiyoCode = SL_SYKHClass.sl_sykh_tekcd.Trim
          m_Controller(intHeadCnt).ModifiedEntity.Header.Tekiyo = SL_SYKHClass.sl_sykh_tekmei.Trim
          m_Controller(intHeadCnt).ModifiedEntity.Header.ProCode = SL_SYKHClass.sl_sykh_pjcode.Trim
          m_Controller(intHeadCnt).ModifiedEntity.Header.YobiInt1 = SL_SYKHClass.sl_sykh_id
          m_Controller(intHeadCnt).ModifiedEntity.Header.YobiInt2 = SL_SYKHClass.sl_sykh_denno 'Get Shiping slip number
        End If

        SL_SYKDClass.sl_sykd_id = sortResult(intCount).intDetailsID
        SL_SYKDClass.ReadByID(SL_SYKDClass.sl_sykd_id)
        SMSClass.ReadByID(SL_SYKDClass.sl_sykd_scd)
        SMSPClass.ReadByID(SL_SYKDClass.sl_sykd_scd)

        If intHeadCnt = 0 AndAlso intDetailCnt = 0 Then
          SL_SYKDArray(0) = SL_SYKDClass
        Else
          intDataCount = SL_SYKDArray.Count
          ReDim Preserve SL_SYKDArray(intDataCount)
          SL_SYKDArray(intDataCount) = SL_SYKDClass
        End If

        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).SyohinCode = SL_SYKDClass.sl_sykd_scd.Trim
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).ZeiKubun = CType(SL_SYKDClass.sl_sykd_tax, ZeiKubunType)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).ZeikomiKubun = CType(SL_SYKDClass.sl_sykd_komi, ZeikomiKubnType)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).TankaKeta = CShort(SMSClass.sms_tketa)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).SuryoKeta = CShort(SMSClass.sms_sketa)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).SyohinMei = SL_SYKDClass.sl_sykd_mei.Trim
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).KikakuKataban = SL_SYKDClass.sl_sykd_kikaku.Trim
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Color = SL_SYKDClass.sl_sykd_color.Trim
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Size = SL_SYKDClass.sl_sykd_size.Trim
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).SokoCode = SL_SYKDClass.sl_sykd_souko
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Irisu = SL_SYKDClass.sl_sykd_iri
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Hakosu = SL_SYKDClass.sl_sykd_hako
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Suryo = Math.Round(sortResult(intCount).decQty, SMSClass.sms_sketa)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Tani = SL_SYKDClass.sl_sykd_tani
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Tanka = CDec(sortResult(intCount).decUnitPrice)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).GenTanka = CDec(SL_SYKDClass.sl_sykd_gentan)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).BaiTanka = CDec(SL_SYKDClass.sl_sykd_tanka)

        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Kingaku = CDec(sortResult(intCount).decAmount) 'SUM
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Genka = CDec(sortResult(intCount).decCost)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).BaikaKingaku = CDec(SL_SYKDClass.sl_sykd_kingaku)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Ararieki = CDec(sortResult(intCount).decProfit)
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).Biko = SL_SYKDClass.sl_sykd_biko
        m_Controller(intHeadCnt).ModifiedEntity.DetailList(intDetailCnt).ZeiRitsu = SL_SYKDClass.sl_sykd_rate
        'm_Controller.ModifiedEntity.DetailList(detailsCount).HikiateJuchuNo = m_SL_SYKHClass.sl_sykh_tcd

        'If isSuccess Then isSuccess = SL_SYKDArray(intDataCount).UpdateDetailsAfterConvert(session)
        If isSuccess Then isSuccess = SL_SYKHClass.UpdateDetailsAfterConvert(session)

        intDetailCnt += 1

      Next
      'Validate required condition, stop if one is not valid
      For intCount As Integer = 0 To intHeadCnt
        If Not ValidateRequiredCondition(m_Controller(intCount).ModifiedEntity, intCount, m_Controller(intHeadCnt).ModifiedEntity.Header.YobiInt2) Then Return False
      Next

      'Save all Slips
      For intCount As Integer = 0 To intHeadCnt
        If isSuccess AndAlso Not Create(m_Controller(intCount)) Then isSuccess = False
      Next

      If isSuccess Then
        session.Commit()
        session.Dispose()
      Else
        session.Rollback()
        session.Dispose()
      End If

      SLCmnFunction.ShowToolTip(SLConstants.CommonMessage.TRANSFER_SUCCESS_MESSAGE,
                                SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemExecute.CommandId)
      InitTable(True)

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function Create(item As InputSYKController) As Boolean

    Dim oldCursor As Cursor = Me.Cursor

    Try
      Me.Cursor = Cursors.WaitCursor

      '伝票入力エンティティから仕入伝票を設定
      Dim beInputSYK As BusinessEntity.BEInputSYK = ConvertKonInput.InputToSYK(item.ModifiedEntity)

      If Not (beInputSYK.InputSYKDList.BEInputSYKD.Count > 0) Then 'Hien 12/24 Xmas
        '明細がない時はエラー
        Return False
      End If

      item.AdjustDenpyo(beInputSYK)

      '登録
      Dim denpyoNo As Integer = 0
      Dim result As ResultSaveInputSYK = item.CreateBEInputSYK(beInputSYK, IntegratedApplicationMethodType.Create, connector)

      If result.Status = ApplicationIntegration.IntegratedStatus.Success Then
        denpyoNo = result.ArrayOfBEKonIntegrationResultOfBEInputSYK.BEKonIntegrationResultOfBEInputSYK(0).Target.InputSYKH.DenpyoNo
      End If

      '売上日をデフォルト日付に設定
      item.DefaultHizuke = item.ModifiedEntity.Header.DenpyoHizuke

      '初期化
      Dim newKonInput As PEKonInput = item.CreateInitialEntity()
      item.UpdateActiveEntity(newKonInput)
      '伝区にフォーカスを移動
      Me.ldtShipDate.Focus()

      Return True

    Finally
      'カーソルを元に戻す
      Me.Cursor = oldCursor

    End Try
    Return True
  End Function

  Private Sub GetSearchResult()
    Dim reader As ICustomDataReader = Nothing

    Try
      Dim selectCommand As ICustomCommand
      Dim shipDate As Integer = ldtShipDate.IntDate
      Dim isInclude As Integer
      Dim intCount As Integer = -1
      Dim AMS1 = New AMS1(connector)

      duplicatedCell = Nothing
      m_isNoOfChecked = 0
      Results = Nothing
      If chkIsInclude.Checked Then isInclude = 1 Else isInclude = 0 '出荷日以前で未振替のデータを含むかどうかチェック

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SYKH_SYKD_Pro02040")
      selectCommand.Parameters("@shipDate").SetValue(shipDate)
      selectCommand.Parameters("@isInclude").SetValue(isInclude)

      reader = selectCommand.ExecuteReader
      While reader.Read() = True

        intCount = intCount + 1
        ReDim Preserve Results(intCount)

        Results(intCount) = New ColumnResult

        Dim tempDate As Date
        If Date.TryParseExact(reader.GetValue("sl_sykh_uribi").ToString(), "yyyyMMdd", provider:=CultureInfo.CurrentCulture, style:=DateTimeStyles.None, result:=tempDate) Then
          Results(intCount).ShipDate = tempDate.ToShortDateString.Trim
        End If
        If Date.TryParseExact(reader.GetValue("sl_sykh_nouki").ToString(), "yyyyMMdd", provider:=CultureInfo.CurrentCulture, style:=DateTimeStyles.None, result:=tempDate) Then
          Results(intCount).SalesDate = tempDate
        End If

        Results(intCount).CusCode = reader.GetValue("sl_sykh_tcd").ToString().Trim
        Results(intCount).CusName = reader.GetValue("cms_mei1").ToString().Trim & " " & reader.GetValue("cms_mei2").ToString().Trim
        Results(intCount).ProductCode = reader.GetValue("sl_sykd_scd").ToString().Trim
        Results(intCount).ProductName = reader.GetValue("sl_sykd_mei").ToString().Trim

        Results(intCount).IsConvert = True

        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_sykd_suryo")) Then Results(intCount).Qty = CDec(reader.GetValue("sl_sykd_suryo"))

        Results(intCount).UnitName = reader.GetValue("sl_sykd_tani").ToString().Trim

        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_sykd_tanka")) Then Results(intCount).UnitPrice = CDec(reader.GetValue("sl_sykd_tanka"))

        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_sykd_gentan")) Then Results(intCount).UnitPriceG = CDec(reader.GetValue("sl_sykd_gentan"))

        Results(intCount).LotNo = reader.GetValue("sl_zdn_ulotno").ToString().Trim

        For intCountLotDetails As Integer = 1 To SL_LMBClass.sl_lmb_maxlotop
          Results(intCount).Lotdetails(intCountLotDetails - 1) = reader.GetValue("sl_zdn_ldmei" & intCountLotDetails.ToString).ToString()
        Next

        Results(intCount).SlipNo = CInt(reader.GetValue("sl_sykh_denno"))

        Results(intCount).QtyNoOfDecDigit = CInt(reader.GetValue("sms_sketa"))
        Results(intCount).UnitPriceNoOfDecDigit = CInt(reader.GetValue("sms_tketa"))
        Results(intCount).DetailsID = CInt(reader.GetValue("sl_sykd_id"))

      End While
      If Not Results Is Nothing Then duplicatedCell = New Boolean(Results.Length - 1, m_SlipNoColumn) {}

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
  End Sub

  Private Sub DisplaySearchResult()
    Try

      Dim SL_SMSClass As SL_SMS = New SL_SMS(connector)

      dgv.Columns.Clear()
      InitTable()

      DuplicateRow()

      For intCount As Integer = 0 To Results.Count - 1
        If intCount > dgv.Rows.Count - 1 AndAlso Not Results(intCount) Is Nothing Then dgv.Rows.Add()

        If Results(intCount) IsNot Nothing Then
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ConvertFlag).Value = Results(intCount).IsConvert
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ShipDate).Value = Results(intCount).ShipDate.Trim
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.SalesDate).Value = Results(intCount).SalesDate
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.SupCode).Value = Results(intCount).CusCode
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.SupName).Value = Results(intCount).CusName
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ProductCode).Value = Results(intCount).ProductCode
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ProductName).Value = Results(intCount).ProductName
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ConvertFlag).Value = Results(intCount).IsConvert
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.Qty).Value = Math.Round(CDec(Results(intCount).Qty), Results(intCount).QtyNoOfDecDigit).ToString
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitName).Value = Results(intCount).UnitName
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitPrice).Value = Format(CDec(Results(intCount).UnitPrice), SLCmnFunction.FormatComma(Results(intCount).UnitPriceNoOfDecDigit)).ToString
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitPriceG).Value = Format(CDec(Results(intCount).UnitPriceG), SLCmnFunction.FormatComma(Results(intCount).UnitPriceNoOfDecDigit)).ToString
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.LotNo).Value = Results(intCount).LotNo

          SL_SMSClass.ReadByProductCode(Results(intCount).ProductCode)

          For intCountLotDetails As Integer = 0 To SL_LMBClass.sl_lmb_maxlotop - 1
            dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.Total + intCountLotDetails).Value = Results(intCount).Lotdetails(intCountLotDetails)
          Next
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.SlipNo).Value = Results(intCount).SlipNo.ToString.Trim
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.DetailNo).Value = Results(intCount).DetailsID.ToString.Trim 'Details ID column
        End If
      Next

      CheckDuplicateCell()

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

  Private Sub CheckDuplicateCell()
    Try
      Dim QtyAdded As Boolean = False
      Dim decQty As Decimal = 0D
      Dim intAddBeginRow As Integer = 0
      Dim SMS_Class As SMS = New SMS(connector)

      For intRow As Integer = 1 To Results.Length - 1
        If Not Results(intRow) Is Nothing Then


          If dgv.Rows(intRow - 1).Cells(ColumnIndex.SlipNo).Value.ToString().Trim = dgv.Rows(intRow).Cells(ColumnIndex.SlipNo).Value.ToString().Trim Then
            duplicatedCell(intRow, ColumnIndex.ConvertFlag) = True
            duplicatedCell(intRow, ColumnIndex.ShipDate) = True
            duplicatedCell(intRow, ColumnIndex.SalesDate) = True
            duplicatedCell(intRow, ColumnIndex.SupCode) = True
            duplicatedCell(intRow, ColumnIndex.SupName) = True

          Else
            duplicatedCell(intRow, ColumnIndex.ConvertFlag) = False
            duplicatedCell(intRow, ColumnIndex.ShipDate) = False
            duplicatedCell(intRow, ColumnIndex.SalesDate) = False
            duplicatedCell(intRow, ColumnIndex.SupCode) = False
            duplicatedCell(intRow, ColumnIndex.SupName) = False

            'If intColumn = ColumnIndex.ConvertFlag Then m_noOfCheckBox = m_noOfCheckBox + 1
          End If

          'If the slip number and productcode/productname is the same
          'In the same slip and same product, only cells in first row  = true, others = false
          If dgv.Rows(intRow - 1).Cells(ColumnIndex.SlipNo).Value.ToString().Trim = dgv.Rows(intRow).Cells(ColumnIndex.SlipNo).Value.ToString().Trim AndAlso _
             (dgv.Rows(intRow - 1).Cells(ColumnIndex.ProductCode).Value.ToString().Trim = dgv.Rows(intRow).Cells(ColumnIndex.ProductCode).Value.ToString().Trim And _
              dgv.Rows(intRow - 1).Cells(ColumnIndex.ProductName).Value.ToString().Trim = dgv.Rows(intRow).Cells(ColumnIndex.ProductName).Value.ToString().Trim And _
              dgv.Rows(intRow - 1).Cells(ColumnIndex.LotNo).Value.ToString().Trim = dgv.Rows(intRow).Cells(ColumnIndex.LotNo).Value.ToString().Trim) Then
            duplicatedCell(intRow, ColumnIndex.ProductCode) = True
            duplicatedCell(intRow, ColumnIndex.ProductName) = True
          Else
            duplicatedCell(intRow, ColumnIndex.ProductCode) = False
            duplicatedCell(intRow, ColumnIndex.ProductName) = False
          End If

          'check same unitprice 
          
        End If
      Next
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub DuplicateRow()
    Try
      Dim duplicatedResult() As ColumnResult = {New ColumnResult}
      Dim intInsertRow As Integer = 0
      Dim decQty As Decimal = 0D
      Dim intLpc As Integer = 0

      ReDim duplicatedResult(0)
      For intLpc = 1 To Results.Length - 1
        If Results(intLpc - 1).SlipNo = Results(intLpc).SlipNo And Results(intLpc - 1).ProductCode = Results(intLpc).ProductCode And Results(intLpc - 1).ProductName = Results(intLpc).ProductName And _
             Results(intLpc - 1).UnitPrice = Results(intLpc).UnitPrice And Results(intLpc - 1).LotNo = Results(intLpc).LotNo Then

          decQty += Results(intLpc - 1).Qty

        Else
          ReDim Preserve duplicatedResult(intInsertRow)
          duplicatedResult(intInsertRow) = Results(intLpc - 1)
          If decQty > 0 Then decQty += Results(intLpc - 1).Qty : duplicatedResult(intInsertRow).Qty = decQty
          intInsertRow += 1
          decQty = 0D
        End If
      Next

      'last row
      ReDim Preserve duplicatedResult(intInsertRow)
      duplicatedResult(intInsertRow) = Results(intLpc - 1)
      If decQty > 0 Then decQty += Results(intLpc - 1).Qty : duplicatedResult(intInsertRow).Qty = decQty
      
      Results = duplicatedResult
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region " DataGridView Event"

  Private Sub dgv_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick
    Try
      If e.RowIndex > -1 AndAlso Not dgv.Rows(e.RowIndex).Cells(ColumnIndex.ConvertFlag).Value Is Nothing AndAlso _
     e.ColumnIndex = ColumnIndex.ConvertFlag AndAlso Not duplicatedCell(e.RowIndex, e.ColumnIndex) Then
        If dgv.Rows(e.RowIndex).Cells(ColumnIndex.ConvertFlag).Value.ToString = "False" Then
          m_isNoOfChecked = m_isNoOfChecked - 1
        Else
          m_isNoOfChecked = m_isNoOfChecked + 1
        End If

        If m_isNoOfChecked > 0 Then
          PcaFunctionCommandExecute.Enabled = True
        Else
          PcaFunctionCommandExecute.Enabled = False
        End If

      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub dgv_CurrentCellDirtyStateChanged(sender As System.Object, e As System.EventArgs) Handles dgv.CurrentCellDirtyStateChanged
    If dgv.IsCurrentCellDirty Then dgv.CommitEdit(DataGridViewDataErrorContexts.Commit)
  End Sub

  Private Sub dgv_CellEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEnter
    If (e.RowIndex = 0 AndAlso e.ColumnIndex = 0) OrElse duplicatedCell Is Nothing OrElse e.RowIndex > Results.Length - 1 Then Return

    If duplicatedCell(e.RowIndex, e.ColumnIndex) Then
      dgv.Rows(e.RowIndex).ReadOnly = True
    Else
      m_intCurColumnIndex = e.ColumnIndex
      m_intCurRowIndex = e.RowIndex
      If Not dgv.CurrentCell.Value Is Nothing Then m_strCurCellValue = dgv.CurrentCell.Value.ToString
    End If

  End Sub

#Region "  DataGridView AddHandler Method"

  Private Sub dgv_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgv.EditingControlShowing
    If m_intCurColumnIndex = ColumnIndex.UnitPrice Then
      AddHandler CType(e.Control, TextBox).KeyPress, AddressOf Cell_KeyPress
      AddHandler CType(e.Control, TextBox).KeyUp, AddressOf Cell_KeyUp
    End If
  End Sub

  Private Sub Cell_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
    If m_intCurColumnIndex = ColumnIndex.UnitPrice Then
      'If user input a character that is not numberic or not backspace
      If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = "." Or e.KeyChar = System.Convert.ToChar(Keys.Back)) Then
        e.Handled = True
        'If current value is already contains "." or Unit price's no of decimal digit = 0 (Must be integer)
      ElseIf (m_strCurCellValue.Contains(".") OrElse Results(m_intCurRowIndex).UnitPriceNoOfDecDigit = 0) AndAlso e.KeyChar = "." Then
        e.Handled = True
      End If
    End If
  End Sub

  Private Sub Cell_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs)
    If m_intCurColumnIndex = ColumnIndex.UnitPrice Then
      'Get current cell value
      Dim txtbox As TextBox = DirectCast(sender, TextBox)
      m_strCurCellValue = txtbox.Text
    End If
  End Sub

#End Region

  Private Sub dgv_CellValidated(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellValidated
    Dim decUnitPrice As Decimal = 0
    If m_intCurColumnIndex = ColumnIndex.UnitPrice Then
      If Not IsNumeric(m_strCurCellValue) OrElse CDbl(m_strCurCellValue) > Int32.MaxValue Then
        dgv.CurrentCell.Value = 0
      Else
        Decimal.TryParse(m_strCurCellValue, decUnitPrice)
        dgv.CurrentCell.Value = decUnitPrice.ToString(SLCmnFunction.FormatComma(Results(e.RowIndex).UnitPriceNoOfDecDigit))

      End If
      m_intCurColumnIndex = -1
      m_intCurRowIndex = -1
      m_strCurCellValue = String.Empty
    End If
  End Sub

  'Merge header cell
  Private Sub dgv_Paint(sender As Object, e As System.Windows.Forms.PaintEventArgs) Handles dgv.Paint
    Dim monthes As String() = {ColumnName.CusCode, ColumnName.ProductCode}
    Dim j As Integer = ColumnIndex.SupCode 'Merge 2-3 and 4-5
    While j < ColumnIndex.ProductName
      Dim r1 As Rectangle = dgv.GetCellDisplayRectangle(j, -1, True)
      Dim w2 As Integer = dgv.GetCellDisplayRectangle(j + 1, -1, True).Width
      r1.X += 1
      r1.Y += 1
      r1.Width = r1.Width + w2 - 2
      r1.Height = CInt(r1.Height - 1)
      e.Graphics.FillRectangle(New SolidBrush(dgv.ColumnHeadersDefaultCellStyle.BackColor), r1)
      Dim format As New StringFormat()
      format.Alignment = StringAlignment.Center
      format.LineAlignment = StringAlignment.Center
      e.Graphics.DrawString(monthes(j \ 2 - 1), dgv.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Color.White), r1, format)
      j += 2
    End While

  End Sub

  Private Sub dgv_Scroll(sender As Object, e As System.Windows.Forms.ScrollEventArgs) Handles dgv.Scroll
    Dim rtHeader As Rectangle = dgv.DisplayRectangle
    rtHeader.Height = CInt(dgv.ColumnHeadersHeight / 2)
    dgv.Invalidate(rtHeader)
  End Sub

  Private Sub dgv_CellPainting(sender As Object, e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgv.CellPainting
    Try
      If m_isDataSet AndAlso duplicatedCell IsNot Nothing Then

        If e.ColumnIndex <= m_SlipNoColumn AndAlso e.RowIndex > -1 Then
          Using gridBrush As Brush = New SolidBrush(Me.dgv.GridColor), backColorBrush As Brush = New SolidBrush(e.CellStyle.BackColor)

            Using gridLinePen As Pen = New Pen(gridBrush)

              ' Clear cell  
              e.Graphics.FillRectangle(backColorBrush, e.CellBounds)

              ' Draw line (bottom border and right border of current cell)  
              'If next row cell has different content, only draw bottom border line of current cell  
              If e.RowIndex < Results.Length - 1 Then
                If duplicatedCell Is Nothing OrElse Not duplicatedCell(e.RowIndex + 1, e.ColumnIndex) Then
                  e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1, e.CellBounds.Right - 1, e.CellBounds.Bottom - 1)
                End If
              Else
                e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1, e.CellBounds.Right - 1, e.CellBounds.Bottom - 1)
              End If

              ' Draw right border line of current cell  
              e.Graphics.DrawLine(gridLinePen, e.CellBounds.Right - 1, e.CellBounds.Top, e.CellBounds.Right - 1, e.CellBounds.Bottom)

              ' draw/fill content in current cell, and fill only one cell of multiple same cells  
              If Not e.Value Is Nothing Then
                If duplicatedCell(e.RowIndex, e.ColumnIndex) Then
                Else
                  e.PaintContent(e.CellBounds) ' Fill in checkbox column
                End If
              End If
              e.Handled = True
            End Using
          End Using
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region " Validate Method"

  Public Function ValidateRequiredCondition(ByVal peKonInput As PEKonInput, intCount As Integer, slipNo As Integer) As Boolean

    Try
      If Not ValidateHeader(peKonInput, intCount, slipNo) Then Return False
      If Not ValidateMeisai(peKonInput, intCount, slipNo) Then Return False

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

  Public Function ValidateFormItems(ByVal m_controller As InputSYKController()) As Boolean

    Try

      '請求済、在庫締切済の確認
      Dim intCount As Integer = 0
      For intLpc As Integer = 0 To m_controller.Count - 1
        If m_controller(intLpc) IsNot Nothing AndAlso m_controller(intLpc).ModifiedEntity IsNot Nothing Then
          intCount += 1
          m_controller(intLpc).Mode = InputDefines.ModeType.NewInputMode
          If Not m_controller(0).CheckSeiZaikoZumiAndPrompt(m_controller(intLpc).ModifiedEntity, False) Then Return False
        End If
      Next

      ReDim Preserve m_controller(intCount - 1)

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

  Private Function ValidateHeader(ByVal peKonInput As PEKonInput, intCount As Integer, slipNo As Integer) As Boolean

    Try

      If String.IsNullOrEmpty(peKonInput.Header.TorihikisakiCode) Then
        ShowMessage.OnExclamation(Me, String.Format(MessageDefines.Exclamation.NotInput, BusinessEntity.ItemText.Tokuisaki), "伝票番号：" & slipNo)
        Me.ldtShipDate.Focus()
        Return False
      End If

      '合計金額の桁をチェック
      If m_Controller(intCount).CheckGokeiKeta(peKonInput) = False Then
        ShowMessage.OnExclamation(Me, String.Format(MessageDefines.Exclamation.Overflow, "合計金額または合計原価"), "伝票番号：" & slipNo)
        Return False
      End If

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

  Private Function ValidateMeisai(ByRef peKonInput As PEKonInput, intCount As Integer, slipNo As Integer) As Boolean

    Try

      Dim edaban As Integer = 0
      'Edit Slip Input entity 
      For Each detail As PEKonInputDetail In peKonInput.DetailList
        'Set branch no.
        edaban += 1
        detail.Edaban = CShort(edaban)
        'Set table state
        detail.MeisaiState = KonCalc.JudgeSykdState(detail.SyohinCode, detail.RendoHeaderId, detail.RendoShiji, detail.MasterKubun, detail.Suryo)
      Next

      If m_Controller(intCount).Mode = InputDefines.ModeType.NewInputMode Then
        'In Create mode
        If peKonInput.Header.MitsumoriJuchuKubun = MitsumoriJuchuKubunType.Juchu AndAlso peKonInput.Header.RendoDenpyoNo <> 0 Then Return True

        If peKonInput.DetailList.Exists(Function(match) match.MeisaiState = peKonInput.MeisaiStateType.Valid) = False Then
          'If there is no details slip
          Me.ldtShipDate.Focus() 'hien 15/12/24 Xmas
          Return False
        End If
      End If

      If peKonInput IsNot Nothing AndAlso peKonInput.DetailList IsNot Nothing AndAlso peKonInput.DetailList.Count > 0 Then
        Dim errorMessage As String = String.Empty
        For i As Integer = 0 To peKonInput.DetailList.Count - 1
          If (peKonInput.DetailList(i).MasterKubun = MasterKubunType.Ippan OrElse peKonInput.DetailList(i).MasterKubun = MasterKubunType.Zatsuhin) _
          AndAlso (m_Controller(intCount).CheckFugoKingakuAndGenka(peKonInput.DetailList(i).Kingaku, peKonInput.DetailList(i).Genka, False) = False) Then

            If String.IsNullOrEmpty(errorMessage) Then
              errorMessage = "枝番 '" & peKonInput.DetailList(i).Edaban.ToString() & "'"
            Else
              errorMessage += ", '" & peKonInput.DetailList(i).Edaban.ToString() & "'"
            End If
          End If
        Next

        If errorMessage.Length <> 0 Then
          errorMessage += " の"
          ShowMessage.OnExclamation(Me, errorMessage & String.Format(MessageDefines.Exclamation.NotEqualSign, BusinessEntity.ItemText.Kingaku, BusinessEntity.ItemText.Genka), _
                                    "伝票番号：" & slipNo)

          Return False
        End If

      End If

      'Check number of digit in Unit price
      If Not m_Controller(intCount).CheckTankaKeta(peKonInput, peKonInput.DenpyoType.Uriage, slipNo) Then Return False

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

#End Region

#Region " Labeled Date Event"

  Private Sub ldtShipDate_Enter(sender As Object, e As System.EventArgs) Handles ldtShipDate.Enter
    m_isDateFocusedFlag = True
  End Sub

  Private Sub ldtShipDate_Leave(sender As System.Object, e As System.EventArgs) Handles ldtShipDate.Leave
    m_isDateFocusedFlag = False
  End Sub

#End Region

  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
    If (keyData And Keys.KeyCode) = Keys.Tab AndAlso m_isDateFocusedFlag Then Search()
    Return MyBase.ProcessDialogKey(keyData)
  End Function
  
End Class

Public Class ColumnResult

  Public Property ShipDate As String
  Public Property SalesDate As Date
  Public Property CusCode As String = String.Empty
  Public Property CusName As String = String.Empty
  Public Property ProductCode As String = String.Empty
  Public Property ProductName As String = String.Empty
  Public Property IsConvert As Boolean

  Public Property Qty As Decimal
  Public Property UnitName As String = String.Empty
  Public Property UnitPriceG As Decimal
  Public Property UnitPrice As Decimal
  Public Property LotNo As String = String.Empty

  Public Property Lotdetails() As String() = New String(10) {}

  Public Property SlipNo As Integer
  Public Property QtyNoOfDecDigit As Integer
  Public Property UnitPriceNoOfDecDigit As Integer
  Public Property DetailsID As Integer
End Class

Public Class ColumnName
  Public Const Convert As String = "売上"
  Public Const ShipDate As String = "出荷日"
  Public Const SalesDate As String = "売上日"
  Public Const CusCode As String = "売上先"
  Public Const CusName As String = "売上先名"
  Public Const ProductCode As String = "商品"
  Public Const ProductName As String = "商品名"
  Public Const Quantity As String = "数量"
  Public Const UnitName As String = "単位"
  Public Const UnitPriceG As String = "原価"
  Public Const UnitPrice As String = "単価"
  Public Const LotNo As String = "ロットNo"
  Public Const Lotdetails As String = "ロット詳細"
  Public Const Lotdetails1 As String = "ロット詳細1"
  Public Const Lotdetails2 As String = "ロット詳細2"
  Public Const Lotdetails3 As String = "ロット詳細3"
  Public Const Lotdetails4 As String = "ロット詳細4"
  Public Const Lotdetails5 As String = "ロット詳細5"
  Public Const Lotdetails6 As String = "ロット詳細6"
  Public Const Lotdetails7 As String = "ロット詳細7"
  Public Const Lotdetails8 As String = "ロット詳細8"
  Public Const Lotdetails9 As String = "ロット詳細9"
  Public Const Lotdetails10 As String = "ロット詳細10"
  Public Const SlipNo As String = "伝票番号"
  Public Const DetailsID As String = "明細ID"
End Class

Public Class SortResult
  Public Property intSlipNo As Integer
  Public Property intDetailsID As Integer
  Public Property dteSalesDate As Date
  Public Property decQty As Decimal
  Public Property decUnitPrice As Decimal
  Public Property decAmount As Decimal
  Public Property decCost As Decimal
  Public Property decProfit As Decimal
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class YoshinDialog
  Inherits PCA.UITools.PcaDialog

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.TextBoxYoshin = New PCA.Controls.PcaLabeledTextBox()
    Me.TextBoxUrikakekinZandaka = New PCA.Controls.PcaLabeledTextBox()
    Me.TextBoxTori = New PCA.Controls.PcaLabeledTextBox()
    Me.FlowLayoutPanel = New PCA.Controls.PcaFlowLayoutPanel()
    Me.TextBoxLineGendogaku = New PCA.Controls.PcaLabeledTextBox()
    Me.LabelMessage = New PCA.Controls.PcaLabel()
    Me.LabelImpossibleMessage = New PCA.Controls.PcaLabel()
    Me.FlowLayoutPanel.SuspendLayout()
    Me.SuspendLayout()
    '
    'ButtonOK
    '
    Me.ButtonOK.Location = New System.Drawing.Point(138, 160)
    Me.ButtonOK.Text = "続行(F5)"
    '
    'ButtonCancel
    '
    Me.ButtonCancel.Location = New System.Drawing.Point(216, 160)
    '
    'ButtonHelp
    '
    Me.ButtonHelp.Location = New System.Drawing.Point(216, 160)
    '
    'TextBoxYoshin
    '
    Me.TextBoxYoshin.ClientProcess = Nothing
    Me.TextBoxYoshin.Enabled = False
    Me.FlowLayoutPanel.SetFlowBreak(Me.TextBoxYoshin, True)
    Me.TextBoxYoshin.LabelText = "与信限度額 (A)"
    Me.TextBoxYoshin.Location = New System.Drawing.Point(0, 0)
    Me.TextBoxYoshin.Name = "TextBoxYoshin"
    Me.TextBoxYoshin.ReadOnly = True
    Me.TextBoxYoshin.Size = New System.Drawing.Size(240, 20)
    Me.TextBoxYoshin.TabIndex = 0
    Me.TextBoxYoshin.TabStop = False
    Me.TextBoxYoshin.Text = "0"
    Me.TextBoxYoshin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.TextBoxYoshin.TextBackColor = System.Drawing.SystemColors.Control
    '
    'TextBoxUrikakekinZandaka
    '
    Me.TextBoxUrikakekinZandaka.ClientProcess = Nothing
    Me.TextBoxUrikakekinZandaka.Enabled = False
    Me.FlowLayoutPanel.SetFlowBreak(Me.TextBoxUrikakekinZandaka, True)
    Me.TextBoxUrikakekinZandaka.LabelText = "売掛残高 (B)"
    Me.TextBoxUrikakekinZandaka.Location = New System.Drawing.Point(0, 19)
    Me.TextBoxUrikakekinZandaka.Name = "TextBoxUrikakekinZandaka"
    Me.TextBoxUrikakekinZandaka.ReadOnly = True
    Me.TextBoxUrikakekinZandaka.Size = New System.Drawing.Size(240, 20)
    Me.TextBoxUrikakekinZandaka.TabIndex = 1
    Me.TextBoxUrikakekinZandaka.TabStop = False
    Me.TextBoxUrikakekinZandaka.Text = "0"
    Me.TextBoxUrikakekinZandaka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.TextBoxUrikakekinZandaka.TextBackColor = System.Drawing.SystemColors.Control
    '
    'TextBoxTori
    '
    Me.TextBoxTori.ClientProcess = Nothing
    Me.TextBoxTori.Enabled = False
    Me.FlowLayoutPanel.SetFlowBreak(Me.TextBoxTori, True)
    Me.TextBoxTori.LabelText = "取引限度額 (A-B)"
    Me.TextBoxTori.Location = New System.Drawing.Point(0, 38)
    Me.TextBoxTori.Name = "TextBoxTori"
    Me.TextBoxTori.ReadOnly = True
    Me.TextBoxTori.Size = New System.Drawing.Size(240, 20)
    Me.TextBoxTori.TabIndex = 2
    Me.TextBoxTori.TabStop = False
    Me.TextBoxTori.Text = "0"
    Me.TextBoxTori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.TextBoxTori.TextBackColor = System.Drawing.SystemColors.Control
    '
    'FlowLayoutPanel
    '
    Me.FlowLayoutPanel.Controls.Add(Me.TextBoxYoshin)
    Me.FlowLayoutPanel.Controls.Add(Me.TextBoxUrikakekinZandaka)
    Me.FlowLayoutPanel.Controls.Add(Me.TextBoxTori)
    Me.FlowLayoutPanel.Controls.Add(Me.TextBoxLineGendogaku)
    Me.FlowLayoutPanel.Controls.Add(Me.LabelMessage)
    Me.FlowLayoutPanel.Controls.Add(Me.LabelImpossibleMessage)
    Me.FlowLayoutPanel.Location = New System.Drawing.Point(13, 13)
    Me.FlowLayoutPanel.Name = "FlowLayoutPanel"
    Me.FlowLayoutPanel.Size = New System.Drawing.Size(273, 135)
    Me.FlowLayoutPanel.TabIndex = 104
    '
    'TextBoxLineGendogaku
    '
    Me.TextBoxLineGendogaku.ClientProcess = Nothing
    Me.TextBoxLineGendogaku.Enabled = False
    Me.FlowLayoutPanel.SetFlowBreak(Me.TextBoxLineGendogaku, True)
    Me.TextBoxLineGendogaku.LabelText = "警告ラインの取引限度額"
    Me.TextBoxLineGendogaku.Location = New System.Drawing.Point(0, 57)
    Me.TextBoxLineGendogaku.Name = "TextBoxLineGendogaku"
    Me.TextBoxLineGendogaku.ReadOnly = True
    Me.TextBoxLineGendogaku.Size = New System.Drawing.Size(240, 20)
    Me.TextBoxLineGendogaku.TabIndex = 3
    Me.TextBoxLineGendogaku.TabStop = False
    Me.TextBoxLineGendogaku.Text = "0"
    Me.TextBoxLineGendogaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.TextBoxLineGendogaku.TextBackColor = System.Drawing.SystemColors.Control
    '
    'LabelMessage
    '
    Me.FlowLayoutPanel.SetFlowBreak(Me.LabelMessage, True)
    Me.LabelMessage.Location = New System.Drawing.Point(0, 96)
    Me.LabelMessage.Margin = New System.Windows.Forms.Padding(0, 20, 0, 0)
    Me.LabelMessage.MaxTextLength = 45
    Me.LabelMessage.Name = "LabelMessage"
    Me.LabelMessage.Size = New System.Drawing.Size(270, 20)
    Me.LabelMessage.TabIndex = 104
    Me.LabelMessage.Text = "売上額が取引限度額を超えました。"
    '
    'LabelImpossibleMessage
    '
    Me.LabelImpossibleMessage.Location = New System.Drawing.Point(0, 115)
    Me.LabelImpossibleMessage.MaxTextLength = 45
    Me.LabelImpossibleMessage.Name = "LabelImpossibleMessage"
    Me.LabelImpossibleMessage.Size = New System.Drawing.Size(270, 20)
    Me.LabelImpossibleMessage.TabIndex = 105
    Me.LabelImpossibleMessage.Text = "基本情報の設定で制限されていて登録できません。"
    '
    'YoshinDialog
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BottomControl = Me.FlowLayoutPanel
    Me.ClientSize = New System.Drawing.Size(300, 217)
    Me.Controls.Add(Me.FlowLayoutPanel)
    Me.HelpVisible = False
    Me.Name = "YoshinDialog"
    Me.OkText = "続行"
    Me.Text = "取引限度額チェック"
    Me.Controls.SetChildIndex(Me.ButtonOK, 0)
    Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
    Me.Controls.SetChildIndex(Me.ButtonHelp, 0)
    Me.Controls.SetChildIndex(Me.FlowLayoutPanel, 0)
    Me.FlowLayoutPanel.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents TextBoxYoshin As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents FlowLayoutPanel As PCA.Controls.PcaFlowLayoutPanel
  Friend WithEvents TextBoxUrikakekinZandaka As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents TextBoxTori As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents TextBoxLineGendogaku As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents LabelMessage As PCA.Controls.PcaLabel
  Friend WithEvents LabelImpossibleMessage As PCA.Controls.PcaLabel
End Class

﻿Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity
Imports System.ComponentModel
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports PCA.TSC.Kon.Tools
Imports PCA.TSC.Kon
Imports PCA
Imports System.Xml

Public Class InputSYKController

  Inherits TscInputControllerBase

#Region "プロパティ"

  ''' <summary>
  ''' 検索時一覧表示をするかどうかを取得、設定します。
  ''' </summary>
  Public Property IsSearchIchiran As Boolean

  ''' <summary>見積No/受注Noを取得、設定。
  ''' </summary>
  Public Property MitsumoriJuchuKubun() As PCA.TSC.Kon.BusinessEntity.Defines.MitsumoriJuchuKubunType = MitsumoriJuchuKubunType.Juchu

  ''' <summary>伝区の初期値値を取得、設定。
  ''' </summary>
  Public Property DefaultDenku() As PCA.TSC.Kon.BusinessEntity.Defines.DenkuType = DenkuType.Kake

  ''' <summary>
  ''' LeaveInputをキャンセルするかどうかを取得または設定します。
  ''' </summary>
  Public Property IsCancelLeavInput As Boolean

#End Region

  ''' <summary>コンストラクタ
  ''' </summary>
  Public Sub New(ByVal owner As IWin32Window)
    '
    '売上伝票入力用に基底クラスを実体化
    MyBase.New(owner, PEKonInput.DenpyoType.Uriage)

  End Sub

#Region "初期化"

  ''' <summary>コントローラクラスを初期化
  ''' </summary>
  Public Overrides Function Initialize(ByRef configXMLDoc As XmlDocument) As Boolean
    '
    'PCAクラウドAPIの準備、会社基本情報を読込み
    If MyBase.Initialize(configXMLDoc) = False Then
      Return False
    End If

    'マスターの先読込み（メモリ化）
    MyBase.MasterSms.NeedLoad = True        '商品
    MyBase.MasterSoko.NeedLoad = True       '倉庫
    MyBase.MasterBumon.NeedLoad = True      '部門
    MyBase.MasterTantosya.NeedLoad = True   '担当者
    MyBase.MasterProject.NeedLoad = True    'プロジェクト
    MyBase.MasterTekiyo.NeedLoad = True     '摘要
    MyBase.MasterTms.NeedLoad = True        '得意先
    MyBase.MasterYms.NeedLoad = True        '直送先
    MyBase.AreaUser.NeedLoad = True         '領域ユーザー
    If Not MyBase.LoadMaster() Then
      Return False
    End If

    '参照画面の表示項目の先読込み（メモリ化）
    MyBase.ReferSms.NeedLoad = True        '商品
    MyBase.ReferSoko.NeedLoad = True       '倉庫
    MyBase.ReferBumon.NeedLoad = True      '部門
    MyBase.ReferTantosya.NeedLoad = True   '担当者
    MyBase.ReferProject.NeedLoad = True    'プロジェクト
    MyBase.ReferTekiyo.NeedLoad = True     '摘要
    MyBase.ReferTms.NeedLoad = True        '得意先
    MyBase.ReferYms.NeedLoad = True        '直送先
    If Not MyBase.LoadReferItemsList() Then
      Return False
    End If

    '売上伝票のIDリストを初期化([検索]、[前移動]/[次移動]用)
    Me.ReferDenpyoList = New List(Of ReferItems)

    'デフォルト日付の設定
    Me.InitializeDefaultHizuke()

    '商魂のみの導入
    MyBase.IsOnlyKon = False

    Return True

  End Function


#End Region

#Region "売上伝票関連メソッド"

  ''' <summary>直近数日に登録更新した売上伝票の参照画面表示項目リストを作成
  ''' </summary>
  Public Sub LoadReferSyk(ByVal days As Double)

    Dim inputCondition As New BusinessEntity.InputSYKCondition
    '登録変更期間（開始）
    Dim kosinDateFrom As Date = DateTime.Now.AddDays(days * -1)
    inputCondition.KosinbiFrom = Tools.Convert.DateToISO8601(kosinDateFrom).ToString

    inputCondition.OrderType = PCA.TSC.Kon.BusinessEntity.Defines.ConditionDenpyoOrderType.Toroku

    '***upd***top 15.11.30
    'Dim referSykNykDialogTool As New Tools.ReferSykDialogTool(MyBase.ReferDialogCommonVariable)
    Dim referSykNykDialogTool As New Tools.ReferSykNykDialogTool(MyBase.ReferDialogCommonVariable)
    '***upd***btm 15.11.30

    Dim beInputSYKList As New List(Of BEInputSYK)

    beInputSYKList = MyBase.FindByConditionBEInputSYKList(inputCondition)

    If beInputSYKList.Count = 0 Then
      Me.ReferDenpyoList = New List(Of ReferItems)
    Else
      Me.ReferDenpyoList = referSykNykDialogTool.SettingReferItemsList(beInputSYKList)
    End If

  End Sub

  ''' <summary>
  ''' 売上伝票を取得します
  ''' </summary>
  ''' <param name="id">ID</param>
  ''' <returns>売上伝票</returns>
  ''' <remarks></remarks>
  Public Function GetBEInputSYK(ByVal id As Integer) As BEInputSYK
    '
    Dim beInputSYK As New BEInputSYK

    Try
      beInputSYK = MyBase.FindBEInputSYK(id)
      If beInputSYK.InputSYKH.Id = 0 Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(MessageDefines.Exclamation.DenpyoNotExist, BusinessEntity.ItemText.Uriage), _
                                  CommonVariable.BasicConfig.ProgramName)
        Return beInputSYK
      End If
      Return beInputSYK

    Catch ex As Exception
      Throw

    End Try

  End Function

  ''' <summary>
  ''' 売上伝票のロック情報
  ''' </summary>
  ''' <remarks></remarks>
  Private m_BELockKonDenpyoSYK As BusinessEntity.BELockKon = Nothing

  ''' <summary>
  ''' 売上伝票をロックします
  ''' </summary>
  ''' <param name="inputSYKId">伝票ID</param>
  ''' <returns>ロックに成功したらTrueを返します。</returns>
  ''' <remarks></remarks>
  Public Function LockInputSYK(ByVal inputSYKId As Integer) As Boolean

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputSYKTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon

    Try
      Target.TargetId = inputSYKId

      ResultLockSingleBE = Tools.LockKon.LockSingleBE(CommonVariable.IApplication, InputTool, Target, True)

      If ResultLockSingleBE.Status = IntegratedStatus.Success Then
        m_BELockKonDenpyoSYK = DirectCast(ResultLockSingleBE.BELockObject, BusinessEntity.BELockKon)
        Return True
      Else
        ShowMessage.OnExclamation(m_Owner, MessageDefines.Exclamation.CantLock & Environment.NewLine & _
         ResultLockSingleBE.ErrorCode & Environment.NewLine & ResultLockSingleBE.ErrorMessage, _
         CommonVariable.BasicConfig.ProgramName)
        Return False
      End If
    Catch ex As Exception
      TscExceptionBox.Show(m_Owner, ex, CommonVariable.BasicConfig.ProgramName, CommonVariable.BasicConfig.ProgramName)
      Return False
    End Try

  End Function

  ''' <summary>
  ''' 売上伝票をロックします
  ''' </summary>
  ''' <param name="inputSYKId">伝票ID</param>
  ''' <param name="errCode">(参照渡し)エラーコード</param>
  ''' <returns>ロックに成功したらTrueを返します。</returns>
  ''' <remarks></remarks>
  Public Function LockInputSYK(ByVal inputSYKId As Integer, ByRef errCode As String) As Boolean

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputSYKTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon

    Try
      Target.TargetId = inputSYKId

      ResultLockSingleBE = Tools.LockKon.LockSingleBE(CommonVariable.IApplication, InputTool, Target, True)

      If ResultLockSingleBE.Status = IntegratedStatus.Success Then
        m_BELockKonDenpyoSYK = DirectCast(ResultLockSingleBE.BELockObject, BusinessEntity.BELockKon)
        Return True
      Else
        ShowMessage.OnExclamation(m_Owner, MessageDefines.Exclamation.CantLock & Environment.NewLine & _
         ResultLockSingleBE.ErrorCode & Environment.NewLine & ResultLockSingleBE.ErrorMessage, CommonVariable.BasicConfig.ProgramName)
        errCode = ResultLockSingleBE.ErrorCode
        Return False
      End If
    Catch ex As Exception
      TscExceptionBox.Show(m_Owner, ex, CommonVariable.BasicConfig.ProgramName, CommonVariable.BasicConfig.ProgramName)
      Return False
    End Try

  End Function

  ''' <summary>
  ''' 売上伝票のロックを解除します。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub UnLockInputSYK()

    If IsNothing(m_BELockKonDenpyoSYK) OrElse m_BELockKonDenpyoSYK.Id = 0 Then
      'このPGでロックしていない
      Return
    End If

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputSYKTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon
    Dim LockKonTool As New BusinessEntity.LockKonTool

    ResultLockSingleBE = Tools.LockKon.UnlockSingleBE(CommonVariable.IApplication, InputTool, m_BELockKonDenpyoSYK)

    'ロック解除に失敗してもエラーにはなりません
    m_BELockKonDenpyoSYK = Nothing

  End Sub

  ''' <summary>
  ''' [前伝票]コマンドに呼ばれ、前伝票を取得します。
  ''' </summary>
  ''' <param name="selectedId">現在選択されている伝票のID</param>
  ''' <returns>前伝票</returns>
  ''' <remarks></remarks>
  Public Function GetPreviousDenp(ByVal selectedId As Integer) As BEInputSYK
    '
    Dim targetId As Integer = 0                 '前伝票のID
    Dim existPreviousDenp As Boolean = False    '前伝票が存在する場合はTrue
    Dim beInputSYK As New BEInputSYK

    If MyBase.ReferDenpyoList.Count > 0 Then
      '直近１か月以内に登録した売上伝票が1件以上存在
      If selectedId <= 0 Then
        '伝票未選択時(=新規入力時)
        targetId = CInt(MyBase.ReferDenpyoList(MyBase.ReferDenpyoList.Count - 1).Key)
        existPreviousDenp = True
      Else
        For index As Integer = MyBase.ReferDenpyoList.Count To 1 Step -1
          If selectedId = CInt(MyBase.ReferDenpyoList(index - 1).Key) Then
            If index > 1 Then
              targetId = CInt(MyBase.ReferDenpyoList(index - 2).Key)
              existPreviousDenp = True
            Else
              existPreviousDenp = False
            End If
            Exit For
          End If
        Next
      End If
    End If

    If existPreviousDenp Then
      beInputSYK = GetBEInputSYK(targetId)
    End If

    Return beInputSYK
  End Function

  ''' <summary>
  ''' [次伝票]コマンドに呼ばれ、次伝票を取得します。
  ''' </summary>
  ''' <param name="selectedId">現在選択されている伝票のID</param>
  ''' <returns>次伝票</returns>
  ''' <remarks></remarks>
  Public Function GetFollowingDenp(ByVal selectedId As Integer) As BEInputSYK
    Dim targetId As Integer = 0                 '次伝票のID
    Dim existPreviousDenp As Boolean = False    '次伝票が存在する場合はTrue
    Dim beInputSYK As New BEInputSYK
    If MyBase.ReferDenpyoList.Count > 0 Then
      '直近１か月以内に登録した売上伝票が1件以上存在
      If selectedId <= 0 Then
        '伝票未選択時(=新規入力時)
        existPreviousDenp = False
      Else
        For index As Integer = 0 To MyBase.ReferDenpyoList.Count
          If selectedId = CInt(MyBase.ReferDenpyoList(index).Key) Then
            If index = MyBase.ReferDenpyoList.Count - 1 Then
              existPreviousDenp = False
            Else
              targetId = CInt(MyBase.ReferDenpyoList(index + 1).Key)
              existPreviousDenp = True
            End If
            Exit For
          End If
        Next
      End If
    End If

    If existPreviousDenp Then
      beInputSYK = GetBEInputSYK(targetId)
    End If

    Return beInputSYK
  End Function

  ''' <summary>
  ''' 売上伝票を削除します
  ''' </summary>
  ''' <param name="beInputSYK">売上伝票</param>
  ''' <returns>処理結果</returns>
  ''' <remarks></remarks>
  Public Function EraseInputSYK(ByVal beInputSYK As BEInputSYK) As BEResultOfErase
    '
    Dim arrayOfBEInputSYK As List(Of BEInputSYK) = New List(Of BEInputSYK)

    arrayOfBEInputSYK.Add(beInputSYK)

    Dim saveParam As SaveParameter =
     New SaveParameter With {.TransactionScopeUsing = UsingType.UsingOneItem, .TransactionScope = Tools.Defines.TransactionScopeType.Whole}

    Return KonAPI.EraseInputSYK(CommonVariable.IApplication, arrayOfBEInputSYK, saveParam)
  End Function

  ''' <summary>
  ''' 伝票データを登録します。
  ''' </summary>
  ''' <param name="beInputSYK">売上伝票</param>
  ''' <param name="methodType">アプリケーション処理タイプ</param>
  ''' <returns>処理結果</returns>
  ''' <remarks></remarks>
  Public Function CreateBEInputSYK(ByVal beInputSYK As BEInputSYK, methodType As PCA.TSC.Kon.BusinessEntity.Defines.IntegratedApplicationMethodType, iApp As IIntegratedApplication) As TSC.Kon.Tools.ResultSaveInputSYK

    Dim arrayOfBEInputSYK As ArrayOfBEInputSYK = New ArrayOfBEInputSYK
    arrayOfBEInputSYK.BEInputSYK.Add(beInputSYK)
    Dim saveParam As SaveParameter =
     New SaveParameter With {.TransactionScopeUsing = UsingType.UsingOneItem, .TransactionScope = Tools.Defines.TransactionScopeType.Whole, .CalcTax = False}
    Return KonAPI.SaveInputSYK(iApp, methodType, arrayOfBEInputSYK, saveParam)

  End Function
#End Region

#Region "見積・受注関連メソッド"


  ''' <summary>
  ''' 受注伝票のロック情報の保持クラス
  ''' </summary>
  ''' <remarks></remarks>
  Private m_BELockKonDenpyoJUC As BusinessEntity.BELockKon = Nothing

  ''' <summary>
  ''' 受注伝票をロックします
  ''' </summary>
  ''' <param name="inputJUCId">受注伝票ID</param>
  ''' <returns>受注伝票のロックに成功したかどうか</returns>
  ''' <remarks></remarks>
  Public Function LockInputJUC(ByVal inputJUCId As Integer) As Boolean

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputJUCTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon

    Try
      Target.TargetId = inputJUCId

      ResultLockSingleBE = Tools.LockKon.LockSingleBE(CommonVariable.IApplication, InputTool, Target, True)

      If ResultLockSingleBE.Status = IntegratedStatus.Success Then
        m_BELockKonDenpyoJUC = DirectCast(ResultLockSingleBE.BELockObject, BusinessEntity.BELockKon)
        Return True
      Else
        ShowMessage.OnExclamation(m_Owner, MessageDefines.Exclamation.CantLock & Environment.NewLine & _
         ResultLockSingleBE.ErrorCode & Environment.NewLine & _
         ResultLockSingleBE.ErrorMessage, CommonVariable.BasicConfig.ProgramName)
        Return False
      End If
    Catch ex As Exception
      TscExceptionBox.Show(m_Owner, ex, CommonVariable.BasicConfig.ProgramName, CommonVariable.BasicConfig.ProgramName)
      Return False
    End Try

  End Function

  ''' <summary>
  ''' 受注伝票のロックを解除します。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub UnLockInputJUC()

    If IsNothing(m_BELockKonDenpyoJUC) OrElse m_BELockKonDenpyoJUC.Id = 0 Then
      'このPGでロックしていない
      Return
    End If

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputJUCTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon
    Dim LockKonTool As New BusinessEntity.LockKonTool

    ResultLockSingleBE = Tools.LockKon.UnlockSingleBE(CommonVariable.IApplication, InputTool, m_BELockKonDenpyoJUC)

    'ロック解除に失敗してもエラーにはなりません
    m_BELockKonDenpyoJUC = Nothing

  End Sub

#End Region

#Region "エンティティ操作"

  ''' <summary>
  ''' エンティティを初期化します。
  ''' </summary>
  ''' <returns>初期化されたエンティティ</returns>
  ''' <remarks></remarks>
  Public Function CreateInitialEntity() As PEKonInput
    '
    Dim beInputSYK As New BEInputSYK()
    Dim peKonInput As PEKonInput = ConvertKonInput.SYKToInput(beInputSYK, Nothing, Nothing, Nothing)

    ' 条件により初期値が変わる項目はここでセットする
    peKonInput.Header.DenpyoHizuke = Me.DefaultHizuke
    peKonInput.Header.Hizuke = Me.DefaultHizuke
    peKonInput.Header.Denku = DefaultDenku      ' 伝区
    peKonInput.Header.MitsumoriJuchuKubun = Me.MitsumoriJuchuKubun   ' 見積/受注No区分

    Return peKonInput

  End Function

  ''' <summary>
  ''' データの調整を行います。
  ''' </summary>
  ''' <param name="beInputSYK">調整対象の伝票データ</param>
  ''' <remarks></remarks>
  Public Sub AdjustDenpyo(ByVal beInputSYK As BEInputSYK)

    If beInputSYK.InputSYKH.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Kake Then
      '請求日をクリア
      beInputSYK.InputSYKH.Seikyubi = 0
    End If

    ' 見積受注区分の設定見直し
    If [Enum].IsDefined(GetType(PCA.TSC.Kon.BusinessEntity.Defines.MitsumoriJuchuKubunType), beInputSYK.InputSYKH.MitsumoriJuchuKubun) = False Then
      beInputSYK.InputSYKH.MitsumoriJuchuKubun = Me.MitsumoriJuchuKubun
    End If

    Dim tokuisaki As BEMasterTms = MyBase.FindBEMasterTms(beInputSYK.InputSYKH.TokuisakiCode)
    Dim seikyusaki As BEMasterTms = MyBase.FindSeikyusaki(tokuisaki)

    Dim needCalcZei As Boolean = False
    Dim needCalcTotal As Boolean = False
    Dim addEntries As New List(Of BEInputSYKD)

    For Each beInputSYKD As BEInputSYKD In beInputSYK.InputSYKDList.BEInputSYKD
      needCalcZei = False
      '金額、原価、売価金額の小数桁以下を丸める
      If KonCalc.CountSyosuKeta(beInputSYKD.Kingaku) > 0 Then
        beInputSYKD.Kingaku = KonCalc.Round(beInputSYKD.Kingaku, 0, beInputSYK.InputSYKH.KingakuHasu)
        needCalcZei = True
      End If
      If KonCalc.CountSyosuKeta(beInputSYKD.Genka) > 0 Then
        beInputSYKD.Genka = KonCalc.Round(beInputSYKD.Genka, 0, seikyusaki.KingakuHasu)
        needCalcZei = True
      End If
      If KonCalc.CountSyosuKeta(beInputSYKD.BaikaKingaku) > 0 Then
        beInputSYKD.BaikaKingaku = KonCalc.Round(beInputSYKD.BaikaKingaku, 0, beInputSYK.InputSYKH.KingakuHasu)
        needCalcZei = True
      End If

      If needCalcZei Then
        '各金額に変更があったら明細を再計算
        Dim sotoZeigaku As Decimal = beInputSYKD.SotoZeigaku
        Dim uchiZeigaku As Decimal = beInputSYKD.UchiZeigaku
        '消費税額
        KonCalc.CalcMeisaiZei(beInputSYK.InputSYKH.SyohizeiHasu, beInputSYK.InputSYKH.SyohizeiTsuchi, beInputSYKD.ZeiRitsu, beInputSYKD.ZeikomiKubun, beInputSYKD.Kingaku, _
                              sotoZeigaku, uchiZeigaku, _BEKihonJoho.ZeiHasuKurai)
        beInputSYKD.SotoZeigaku = sotoZeigaku
        beInputSYKD.UchiZeigaku = uchiZeigaku

        '粗利益
        beInputSYKD.Ararieki = KonCalc.CalcAraRieki(beInputSYKD.Genka, beInputSYKD.Kingaku, beInputSYKD.UchiZeigaku, beInputSYKD.MasterKubun, beInputSYKD.ZeiRitsu, _
                                                    CType(CShort(beInputSYK.InputSYKH.SyohizeiTsuchi), SyohizeiTsuchiType), beInputSYKD.ZeikomiKubun, _BEKihonJoho.ZeiHasuKurai, beInputSYK.InputSYKH.SyohizeiHasu)

        '合計も再計算
        needCalcTotal = True
      End If

      If beInputSYKD.JuchuHeaderId <> 0 _
      AndAlso beInputSYKD.SyukaShiji = PCA.TSC.Kon.BusinessEntity.Defines.RendoShijiType.Teisei _
      AndAlso beInputSYKD.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan _
      AndAlso beInputSYKD.Suryo = 0 Then
        '受注連動で訂正出荷で数量がゼロの場合は未出荷にする
        beInputSYKD.SyukaShiji = PCA.TSC.Kon.BusinessEntity.Defines.RendoShijiType.Mi
      End If

      Dim meisaiState As PEKonInput.MeisaiStateType _
          = KonCalc.JudgeSykdState(beInputSYKD.SyohinCode, beInputSYKD.JuchuHeaderId, beInputSYKD.SyukaShiji, beInputSYKD.MasterKubun, beInputSYKD.Suryo)
      If meisaiState = PEKonInput.MeisaiStateType.Valid _
      OrElse meisaiState = PEKonInput.MeisaiStateType.RendoInvalid Then
        '有効明細と受注からの未出荷明細のみ追加
        addEntries.Add(beInputSYKD)
      End If

    Next

    beInputSYK.InputSYKDList.BEInputSYKD.Clear()
    beInputSYK.InputSYKDList.BEInputSYKD.AddRange(addEntries)

    Dim beMasterYms As BEMasterYms = MyBase.FindBEMasterYms(beInputSYK.InputSYKH.ChokusosakiCode)

    If needCalcTotal Then
      '必要がある時、合計を再計算

      Dim curTotal As New PEKonInput()

      '再計算した明細のある伝票エンティティをクローン化
      curTotal = ConvertKonInput.SYKToInput(CType(beInputSYK.Clone(), BusinessEntity.BEInputSYK), tokuisaki, seikyusaki, beMasterYms)

      Me.CalcTotal(curTotal, False, Tools.PEKonInput.DenpyoType.Uriage)

      beInputSYK.InputSYKT.KingakuGokei = curTotal.Total.KingakuGokei
      beInputSYK.InputSYKT.SotoZeiGokei = curTotal.Total.SotoZeiGokei
      beInputSYK.InputSYKT.UchiZeiGokei = curTotal.Total.UchiZeiGokei
      beInputSYK.InputSYKT.Hasu = curTotal.Total.Hasu

      '税率別合計
      Dim oldInputSYKZList As New List(Of BEInputSykNykZ)
      oldInputSYKZList.AddRange(CType(beInputSYK.InputSYKZList, Global.System.Collections.Generic.IEnumerable(Of Global.PCA.TSC.Kon.BusinessEntity.BEInputSykNykZ)))

      Dim addInputSYK As New BEInputSYK()
      addInputSYK = ConvertKonInput.InputToSYK(curTotal)

      beInputSYK.InputSYKZList.BEInputSYKZ.Clear()
      beInputSYK.InputSYKZList.BEInputSYKZ.AddRange(CType(addInputSYK.InputSYKZList, Global.System.Collections.Generic.IEnumerable(Of Global.PCA.TSC.Kon.BusinessEntity.BEInputSykNykZ)))

      For Each old As BEInputSykNykZ In oldInputSYKZList
        Dim oldInputSYKZ As BEInputSykNykZ = old    '警告回避

        If addInputSYK.InputSYKZList.BEInputSYKZ.Exists(Function(match) _
            match.ZeiRitsu = oldInputSYKZ.ZeiRitsu) = False AndAlso (old.Hosei <> 0 OrElse old.HoseiHizuke <> 0) Then
          '税率の適用が無くなっても、補正済は旧伝票から移す
          old.KingakuGokei = 0
          old.SotoZeiGokei = 0
          old.UchiZeiGokei = 0
          old.Hasu = 0

          beInputSYK.InputSYKZList.BEInputSYKZ.Add(old)

        End If
      Next
    End If

  End Sub

#End Region

#Region "伝票コマンド（取得）"

  ''' <summary>伝票を再取得。
  ''' </summary>
  ''' <param name="entity">伝票入力エンティティ</param>
  ''' <param name="forUpdate">更新するためにロックを実行するならtrue。それ以外はfalse。</param>
  ''' <returns>再取得に成功したときTrueを返します。</returns>
  ''' <remarks></remarks>
  Public Function ReloadDenpyo(ByVal entity As PEKonInput, ByVal forUpdate As Boolean) As Boolean

    ' 伝票を取得していない場合は無視する
    If entity.Header.Id = 0 Then
      Return True
    End If

    ' 対象の伝票があった場合には現在ロックされている伝票を解除する
    Me.UnLockCore()

    Me.ModifiableEntity = False

    '伝票の取得
    'Dim beInputSYK As BEInputSYK = Me.FindBEInputSYK(entity.Header.Id)
    Dim beInputSYK As BEInputSYK = Me.GetBEInputSYK(entity.Header.Id)
    If IsNothing(beInputSYK) Then
      Return False
    End If

    '伝票をロック
    Dim errorCode As String = String.Empty
    Dim lockResult As Boolean = Me.LockInputSYK(entity.Header.Id, errorCode)
    If lockResult = False Then
      '失敗
      If errorCode = "BCH41924002" Then
        'ロック済
        entity.State = StateType.Locked
      ElseIf errorCode = "BCH41924003" Then
        '削除済
        entity.State = StateType.Sakujozumi
      Else
        entity.State = StateType.CanUpd
      End If

      Me.UpdateActiveEntity(entity)

      Return False
    End If

    Dim tokuisaki As BEMasterTms = MyBase.FindBEMasterTms(beInputSYK.InputSYKH.TokuisakiCode)
    Dim beMasterYms As BEMasterYms = MyBase.FindBEMasterYms(beInputSYK.InputSYKH.ChokusosakiCode)
    Dim seikyusaki As BEMasterTms = MyBase.FindSeikyusaki(tokuisaki)
    '伝票を伝票入力エンティティに設定
    Me.UpdateActiveEntity(ConvertKonInput.SYKToInput(beInputSYK, tokuisaki, seikyusaki, beMasterYms))

    Me.ModifiableEntity = True
    '修正可否を判定
    If beInputSYK.State = PCA.TSC.Kon.BusinessEntity.Defines.StateType.CanUpd _
    OrElse (beInputSYK.State = PCA.TSC.Kon.BusinessEntity.Defines.StateType.Seizumi AndAlso Me.BEKihonJoho.CantUpdSei = PCA.TSC.Kon.BusinessEntity.Defines.CantUpdType.Can) _
    OrElse (beInputSYK.State = PCA.TSC.Kon.BusinessEntity.Defines.StateType.ZaikoShimezumi AndAlso Me.BEKihonJoho.CantUpdZaikoShimekiri = PCA.TSC.Kon.BusinessEntity.Defines.CantUpdType.Can) _
    OrElse (beInputSYK.State = PCA.TSC.Kon.BusinessEntity.Defines.StateType.SeiZaikoShimezumi AndAlso Me.BEKihonJoho.CantUpdSei = PCA.TSC.Kon.BusinessEntity.Defines.CantUpdType.Can _
            AndAlso Me.BEKihonJoho.CantUpdZaikoShimekiri = PCA.TSC.Kon.BusinessEntity.Defines.CantUpdType.Can) Then
      '伝票が更新可能か、請求締切・在庫締切済でも会社基本上、更新が可能な時
      Me.ModifiableEntity = True
    Else
      Me.ModifiableEntity = False
    End If

    Return True

  End Function


  ''' <summary>
  ''' ロック解除（連動受注含む）を行います。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub UnLockCore()

    ' 受注連動していたら受注を解除
    UnLockInputJUC()

    UnLockInputSYK()

  End Sub

  ''' <summary>
  ''' ロック解除（受注のみ）を行います。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub UnLockJuchu()
    UnLockInputJUC()
  End Sub

#End Region

#Region "見積・受注連動"

  ''' <summary>
  ''' 見積伝票等の情報を伝票入力の伝票入力エンティティクラスに設定します。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="beInputMIT">見積伝票</param>
  ''' <param name="tokuisakiCode">得意先コード。得意先を設定しない場合は空文字列を渡してください。</param>
  ''' <remarks></remarks>
  Public Sub PrepareItemsFromMitsumori(ByRef peKonInput As PEKonInput, ByVal beInputMIT As BEInputMIT, ByVal tokuisakiCode As String)
    '
    Dim tokuisaki As New BEMasterTms
    Dim seikyusaki As New BEMasterTms

    'Id
    'Denku
    'DenpypHizuke
    'Hizuke
    'DenpyoNo
    '得意先項目
    If tokuisakiCode.Length = 0 Then
      '入力済み得意先と不一致か、未入力の時
      peKonInput.Header.TorihikisakiCode = beInputMIT.InputMITH.TokuisakiCode
      peKonInput.Header.AitesakiId = beInputMIT.InputMITH.AitesakiId
      peKonInput.Header.TorihikisakiMei1 = beInputMIT.InputMITH.TokuisakiMei1
      peKonInput.Header.TorihikisakiMei2 = beInputMIT.InputMITH.TokuisakiMei2
      peKonInput.Header.Jyusyo1 = beInputMIT.InputMITH.Jyusyo1
      peKonInput.Header.Jyusyo2 = beInputMIT.InputMITH.Jyusyo2
      peKonInput.Header.YubinBango = beInputMIT.InputMITH.YubinBango
      peKonInput.Header.TelNo = beInputMIT.InputMITH.TelNo
      peKonInput.Header.FAXNo = beInputMIT.InputMITH.FAXNo
      peKonInput.Header.Keisyo = beInputMIT.InputMITH.Keisyo
      peKonInput.Header.MailAddress = beInputMIT.InputMITH.MailAddress
      peKonInput.Header.TekiyoBaikaNo = beInputMIT.InputMITH.TekiyoBaikaNo
      peKonInput.Header.Kakeritsu = beInputMIT.InputMITH.Kakeritsu
      peKonInput.Header.ZeiKansan = beInputMIT.InputMITH.ZeiKansan
      peKonInput.Header.SyohizeiTsuchiKon = beInputMIT.InputMITH.SyohizeiTsuchi
      peKonInput.Header.KingakuHasu = beInputMIT.InputMITH.KingakuHasu
      peKonInput.Header.SyohizeiHasu = beInputMIT.InputMITH.SyohizeiHasu
      peKonInput.Header.BaiTankaKansan = beInputMIT.InputMITH.BaiTankaKansan
      '得意先を取得
      tokuisaki = MyBase.FindBEMasterTms(peKonInput.Header.TorihikisakiCode)
      peKonInput.Header.SyatenCode = tokuisaki.SyatenCode     '社店コード
      '
      peKonInput.Header.BunruiCode = tokuisaki.BunruiCode     '分類コード
      peKonInput.Header.DenpyoKubun = tokuisaki.DenpyoKubun   '伝票区分
      peKonInput.Header.StoreTorihikisakiCode = beInputMIT.InputMITH.StoreTorihikisakiCode
      '
      peKonInput.Header.SeisakiCode = beInputMIT.InputMITH.SeikyusakiCode
      '
      peKonInput.Header.SpotAitesakiId = beInputMIT.InputMITH.SpotAitesakiId
      '請求先を取得
      seikyusaki = MyBase.FindSeikyusaki(tokuisaki)
      peKonInput.Header.YoshinGendogaku = seikyusaki.YoshinGendogaku
      peKonInput.Header.SeiKikanTo = seikyusaki.SeikyuKikanTo
      peKonInput.Header.SeiShimebi = seikyusaki.SeikyuShimebi
      peKonInput.Header.MisyuZandaka = KonCalc.CalcMisyuZandaka(seikyusaki)
      '
      peKonInput.Header.GenkaHasu = tokuisaki.KingakuHasu         '原価端数
      peKonInput.Header.TorihikisakiComment = tokuisaki.Comment   'コメント
    End If
    '
    peKonInput.Header.MitsumoriJuchuKubun = MitsumoriJuchuKubunType.Mitsumori
    peKonInput.Header.RendoDenpyoNo = beInputMIT.InputMITH.MitsumoriNo
    peKonInput.Header.SenpoTantosyaId = beInputMIT.InputMITH.SenpoTantosyaId        '先方担当者
    peKonInput.Header.SenpoTantosyaMei = beInputMIT.InputMITH.SenpoTantosyaMei
    peKonInput.Header.BumonCode = beInputMIT.InputMITH.BumonCode                    '部門
    peKonInput.Header.BumonMei = beInputMIT.InputMITH.BumonMei
    peKonInput.Header.BumonSoko = beInputMIT.InputMITH.BumonSoko
    peKonInput.Header.TantosyaCode = beInputMIT.InputMITH.TantosyaCode              '担当者
    peKonInput.Header.TantosyaMei = beInputMIT.InputMITH.TantosyaMei
    peKonInput.Header.TekiyoCode = String.Empty                                     '摘要
    peKonInput.Header.Tekiyo = String.Empty
    peKonInput.Header.ProCode = beInputMIT.InputMITH.ProCode                        'プロジェクト
    peKonInput.Header.ProMei = beInputMIT.InputMITH.ProMei
    'DenpyoNo2
    peKonInput.Header.YobiShort1 = beInputMIT.InputMITH.YobiShort1
    peKonInput.Header.YobiShort2 = beInputMIT.InputMITH.YobiShort2
    peKonInput.Header.YobiShort3 = beInputMIT.InputMITH.YobiShort3
    peKonInput.Header.YobiInt1 = beInputMIT.InputMITH.YobiInt1
    peKonInput.Header.YobiInt2 = beInputMIT.InputMITH.YobiInt2
    'YobiInt3
    peKonInput.Header.YobiDecimal1 = beInputMIT.InputMITH.YobiDecimal1
    peKonInput.Header.YobiDecimal2 = beInputMIT.InputMITH.YobiDecimal2
    peKonInput.Header.YobiDecimal3 = beInputMIT.InputMITH.YobiDecimal3
    peKonInput.Header.YobiString1 = beInputMIT.InputMITH.YobiString1
    peKonInput.Header.YobiString2 = beInputMIT.InputMITH.YobiString2
    peKonInput.Header.YobiString3 = beInputMIT.InputMITH.YobiString3

    '明細
    peKonInput.DetailList.Clear()
    Dim detail As PEKonInputDetail

    For Each inputMITD As BEInputMITD In beInputMIT.InputMITDList.BEInputMITD

      detail = New PEKonInputDetail()

      'detail.Sequence
      'detail.Edaban
      'detail.State
      detail.MeisaiState = peKonInput.MeisaiStateType.Invalid
      detail.SyohinCode = inputMITD.SyohinCode
      detail.MasterKubun = inputMITD.MasterKubun
      detail.ZeiKubun = inputMITD.ZeiKubun
      detail.ZeikomiKubun = inputMITD.ZeikomiKubun
      detail.TankaKeta = inputMITD.TankaKeta
      detail.SuryoKeta = inputMITD.SuryoKeta
      detail.SyohinMei = inputMITD.SyohinMei
      detail.KikakuKataban = inputMITD.KikakuKataban
      detail.Color = inputMITD.Color
      detail.Size = inputMITD.Size
      detail.SokoCode = inputMITD.SokoCode
      detail.Ku = KuType.Tsujo    '区
      detail.Irisu = inputMITD.Irisu
      detail.Hakosu = inputMITD.Hakosu
      detail.Suryo = inputMITD.Suryo
      detail.Tani = inputMITD.Tani
      detail.Tanka = inputMITD.Tanka
      detail.GenTanka = inputMITD.GenTanka
      detail.BaiTanka = inputMITD.BaiTanka
      detail.Kingaku = inputMITD.Kingaku
      detail.Genka = inputMITD.Genka
      detail.BaikaKingaku = inputMITD.BaikaKingaku
      detail.Ararieki = inputMITD.Ararieki
      detail.RiekiRitsu = inputMITD.RiekiRitsu
      '入荷マーク
      If peKonInput.Header.Denku <> DenkuType.Keiyaku _
      AndAlso detail.MasterKubun = MasterKubunType.Ippan Then
        detail.KanMark = NyukaMarkType.Suru
      Else
        detail.KanMark = NyukaMarkType.Shinai
      End If
      detail.Biko = inputMITD.Biko
      detail.HyojunKakaku2 = inputMITD.HyojunKakaku2
      detail.HyojunKakaku = inputMITD.HyojunKakaku
      detail.ZeiRitsu = inputMITD.ZeiRitsu
      detail.SotoZeigaku = inputMITD.SotoZeigaku
      detail.UchiZeigaku = inputMITD.UchiZeigaku
      detail.SyokonKeisan = inputMITD.SyokonKeisan
      detail.SyohinKomoku1 = inputMITD.SyohinKomoku1
      detail.SyohinKomoku2 = inputMITD.SyohinKomoku2
      detail.SyohinKomoku3 = inputMITD.SyohinKomoku3
      detail.DenpyoKomoku1 = inputMITD.UriageKomoku1
      detail.DenpyoKomoku2 = inputMITD.UriageKomoku2
      detail.DenpyoKomoku3 = inputMITD.UriageKomoku3
      'RendoShiji
      'RendoHeaderId
      'RendoSequence
      detail.YobiShort1 = inputMITD.YobiShort1
      detail.YobiShort2 = inputMITD.YobiShort2
      detail.YobiShort3 = inputMITD.YobiShort3
      detail.YobiInt1 = inputMITD.YobiInt1
      detail.YobiInt2 = inputMITD.YobiInt2
      detail.YobiInt3 = inputMITD.YobiInt3
      detail.YobiDecimal1 = inputMITD.YobiDecimal1
      detail.YobiDecimal2 = inputMITD.YobiDecimal2
      detail.YobiDecimal3 = inputMITD.YobiDecimal3
      detail.YobiString1 = inputMITD.YobiString1
      detail.YobiString2 = inputMITD.YobiString2
      detail.YobiString3 = inputMITD.YobiString3
      detail.IrisuKeta = inputMITD.IrisuKeta
      detail.HakosuKeta = inputMITD.HakosuKeta
      detail.SuryoHasu = inputMITD.SuryoHasu
      '原価端数
      detail.GenkaHasu = peKonInput.Header.GenkaHasu

      peKonInput.DetailList.Add(detail)
    Next

    '合計
    peKonInput.Total.HeaderId = beInputMIT.InputMITT.HeaderId
    peKonInput.Total.KingakuGokei = beInputMIT.InputMITT.KingakuGokei
    peKonInput.Total.SotoZeiGokei = beInputMIT.InputMITT.SotoZeiGokei
    peKonInput.Total.UchiZeiGokei = beInputMIT.InputMITT.UchiZeiGokei

    '税率別合計
    Dim subtotal As New PEKonInputSubtotal()
    For Each inputMITZ As BEInputMitJucHacZ In beInputMIT.InputMITZList.BEInputMITZ
      subtotal = New PEKonInputSubtotal() With { _
          .HeaderId = inputMITZ.HeaderId, _
          .ZeiRitsu = inputMITZ.ZeiRitsu, _
          .KingakuGokei = inputMITZ.KingakuGokei, _
          .SotoZeiGokei = inputMITZ.SotoZeiGokei, _
          .UchiZeiGokei = inputMITZ.UchiZeiGokei _
      }
      peKonInput.SubtotalList.Add(subtotal)
    Next

    '合計を再計算
    Me.CalcTotal(peKonInput, True, Tools.PEKonInput.DenpyoType.Uriage)

  End Sub

  ''' <summary>
  ''' 受注伝票等の情報を伝票入力の伝票入力エンティティクラスに設定します。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="beInputJUC">受注伝票</param>
  ''' <param name="tokuisakiCode">得意先コード。得意先を設定しない場合は空文字列を渡してください。</param>
  ''' <remarks></remarks>
  Public Sub PrepareRendoFromJuchu(ByVal peKonInput As PEKonInput, ByVal beInputJUC As BEInputJUC, ByVal tokuisakiCode As String)
    '
    Dim tokuisaki As New BEMasterTms
    Dim seikyusaki As New BEMasterTms
    Dim beMasterYms As New BEMasterYms

    'Id
    'Denku
    'DenpypHizuke
    'Hizuke
    'DenpyoNo
    '得意先項目
    '得意先情報
    If tokuisakiCode.Length = 0 Then
      '入力済み得意先と不一致か、未入力の時
      peKonInput.Header.TorihikisakiCode = beInputJUC.InputJUCH.TokuisakiCode
      peKonInput.Header.AitesakiId = beInputJUC.InputJUCH.AitesakiId
      peKonInput.Header.TorihikisakiMei1 = beInputJUC.InputJUCH.TokuisakiMei1
      peKonInput.Header.TorihikisakiMei2 = beInputJUC.InputJUCH.TokuisakiMei2
      peKonInput.Header.Jyusyo1 = beInputJUC.InputJUCH.Jyusyo1
      peKonInput.Header.Jyusyo2 = beInputJUC.InputJUCH.Jyusyo2
      peKonInput.Header.YubinBango = beInputJUC.InputJUCH.YubinBango
      peKonInput.Header.TelNo = beInputJUC.InputJUCH.TelNo
      peKonInput.Header.FAXNo = beInputJUC.InputJUCH.FAXNo
      peKonInput.Header.Keisyo = beInputJUC.InputJUCH.Keisyo
      peKonInput.Header.MailAddress = beInputJUC.InputJUCH.MailAddress
      peKonInput.Header.TekiyoBaikaNo = beInputJUC.InputJUCH.TekiyoBaikaNo
      peKonInput.Header.Kakeritsu = beInputJUC.InputJUCH.Kakeritsu
      peKonInput.Header.ZeiKansan = beInputJUC.InputJUCH.ZeiKansan
      peKonInput.Header.SyohizeiTsuchiKon = beInputJUC.InputJUCH.SyohizeiTsuchi
      peKonInput.Header.KingakuHasu = beInputJUC.InputJUCH.KingakuHasu
      peKonInput.Header.SyohizeiHasu = beInputJUC.InputJUCH.SyohizeiHasu
      peKonInput.Header.BaiTankaKansan = CType(beInputJUC.InputJUCH.BaiTankaKansan, BaitankaKansanType)
      '得意先を取得
      tokuisaki = MyBase.FindBEMasterTms(peKonInput.Header.TorihikisakiCode)
      peKonInput.Header.SyatenCode = tokuisaki.SyatenCode     '社店コード
      '
      peKonInput.Header.BunruiCode = tokuisaki.BunruiCode     '分類コード
      peKonInput.Header.DenpyoKubun = tokuisaki.DenpyoKubun   '伝票区分
      peKonInput.Header.StoreTorihikisakiCode = beInputJUC.InputJUCH.StoreTorihikisakiCode
      '
      peKonInput.Header.SeisakiCode = beInputJUC.InputJUCH.SeikyusakiCode
      '
      peKonInput.Header.SpotAitesakiId = beInputJUC.InputJUCH.SpotAitesakiId
      If peKonInput.Header.TorihikisakiCode <> peKonInput.Header.SeisakiCode Then
        '得意先を設定
        seikyusaki = tokuisaki
      Else
        '請求先を取得
        seikyusaki = MyBase.FindBEMasterTms(peKonInput.Header.SeisakiCode)
      End If
      peKonInput.Header.YoshinGendogaku = seikyusaki.YoshinGendogaku
      peKonInput.Header.SeiKikanTo = seikyusaki.SeikyuKikanTo
      peKonInput.Header.SeiShimebi = seikyusaki.SeikyuShimebi
      peKonInput.Header.MisyuZandaka = KonCalc.CalcMisyuZandaka(seikyusaki)
      '
      peKonInput.Header.GenkaHasu = tokuisaki.KingakuHasu         '原価端数
      peKonInput.Header.TorihikisakiComment = tokuisaki.Comment   'コメント
    End If

    peKonInput.Header.MitsumoriJuchuKubun = MitsumoriJuchuKubunType.Juchu
    peKonInput.Header.RendoDenpyoNo = beInputJUC.InputJUCH.JuchuNo
    peKonInput.Header.SenpoTantosyaId = beInputJUC.InputJUCH.SenpoTantosyaId        '先方担当者
    peKonInput.Header.SenpoTantosyaMei = beInputJUC.InputJUCH.SenpoTantosyaMei
    peKonInput.Header.BumonCode = beInputJUC.InputJUCH.BumonCode                    '部門
    peKonInput.Header.BumonMei = beInputJUC.InputJUCH.BumonMei
    peKonInput.Header.BumonSoko = beInputJUC.InputJUCH.BumonSoko
    peKonInput.Header.TantosyaCode = beInputJUC.InputJUCH.TantosyaCode              '担当者
    peKonInput.Header.TantosyaMei = beInputJUC.InputJUCH.TantosyaMei
    peKonInput.Header.TekiyoCode = beInputJUC.InputJUCH.TekiyoCode                  '摘要
    peKonInput.Header.Tekiyo = beInputJUC.InputJUCH.Tekiyo
    peKonInput.Header.ProCode = beInputJUC.InputJUCH.ProCode                        'プロジェクト
    peKonInput.Header.ProMei = beInputJUC.InputJUCH.ProMei

    '直送先
    If beInputJUC.InputJUCH.ChokusosakiCode.Length <> 0 Then
      beMasterYms = MyBase.FindBEMasterYms(beInputJUC.InputJUCH.ChokusosakiCode)
      peKonInput.Header.ChokusosakiCode = beInputJUC.InputJUCH.ChokusosakiCode
      peKonInput.Header.ChokusosakiMei1 = beMasterYms.ChokusosakiMei1
      peKonInput.Header.ChokusosakiMei2 = beMasterYms.ChokusosakiMei2
      peKonInput.Header.ChokusosakiTantosyaId = beMasterYms.SenpoTantosyaId
      peKonInput.Header.ChokusosakiTantosyaMei = beMasterYms.SenpoTantosyaMei
      peKonInput.Header.ChokusosakiComment = beMasterYms.Comment
    Else
      peKonInput.Header.ChokusosakiCode = String.Empty
      peKonInput.Header.ChokusosakiMei1 = String.Empty
      peKonInput.Header.ChokusosakiMei2 = String.Empty
      peKonInput.Header.ChokusosakiFlag = ChokusosakiFlagType.Chokusosaki
      peKonInput.Header.ChokusosakiTantosyaId = 0
      peKonInput.Header.ChokusosakiTantosyaMei = String.Empty
      peKonInput.Header.ChokusosakiComment = String.Empty
    End If
    'DenpyoNo2
    peKonInput.Header.YobiShort1 = beInputJUC.InputJUCH.YobiShort1
    peKonInput.Header.YobiShort2 = beInputJUC.InputJUCH.YobiShort2
    peKonInput.Header.YobiShort3 = beInputJUC.InputJUCH.YobiShort3
    peKonInput.Header.YobiInt1 = beInputJUC.InputJUCH.YobiInt1
    peKonInput.Header.YobiInt2 = beInputJUC.InputJUCH.YobiInt2
    'YobiInt3
    peKonInput.Header.YobiDecimal1 = beInputJUC.InputJUCH.YobiDecimal1
    peKonInput.Header.YobiDecimal2 = beInputJUC.InputJUCH.YobiDecimal2
    peKonInput.Header.YobiDecimal3 = beInputJUC.InputJUCH.YobiDecimal3
    peKonInput.Header.YobiString1 = beInputJUC.InputJUCH.YobiString1
    peKonInput.Header.YobiString2 = beInputJUC.InputJUCH.YobiString2
    peKonInput.Header.YobiString3 = beInputJUC.InputJUCH.YobiString3

    '明細
    peKonInput.DetailList.Clear()
    Dim detail As PEKonInputDetail

    For Each inputJUCD As BEInputJUCD In beInputJUC.InputJUCDList.BEInputJUCD
      If inputJUCD.SyukazumiFlag = SyukazumiFlagType.Syuka Then
        '出荷済み明細
        Continue For
      End If

      detail = New PEKonInputDetail()

      'detail.Sequence = 0			'作成時は自動設定されます。
      'detail.Edaban = 0				'作成時は自動設定されます。
      'detail.State = 0
      detail.MeisaiState = peKonInput.MeisaiStateType.Invalid
      detail.SyohinCode = inputJUCD.SyohinCode
      detail.MasterKubun = inputJUCD.MasterKubun
      detail.ZeiKubun = inputJUCD.ZeiKubun
      detail.ZeikomiKubun = inputJUCD.ZeikomiKubun
      detail.TankaKeta = inputJUCD.TankaKeta
      detail.SuryoKeta = inputJUCD.SuryoKeta
      detail.SyohinMei = inputJUCD.SyohinMei
      detail.KikakuKataban = inputJUCD.KikakuKataban
      detail.Color = inputJUCD.Color
      detail.Size = inputJUCD.Size
      detail.SokoCode = inputJUCD.SokoCode
      detail.Ku = KuType.Tsujo    '区
      detail.Irisu = inputJUCD.Irisu
      detail.Hakosu = inputJUCD.Hakosu
      '売上数量＝受注数量－出荷累計
      detail.Suryo = inputJUCD.Suryo - inputJUCD.SyukaRuikei
      detail.Tani = inputJUCD.Tani
      detail.Tanka = inputJUCD.Tanka
      detail.GenTanka = inputJUCD.GenTanka
      detail.BaiTanka = inputJUCD.BaiTanka
      detail.Kingaku = inputJUCD.Kingaku
      detail.Genka = inputJUCD.Genka
      detail.BaikaKingaku = inputJUCD.BaikaKingaku
      detail.Ararieki = inputJUCD.Ararieki
      detail.RiekiRitsu = inputJUCD.RiekiRitsu
      detail.Biko = inputJUCD.Biko
      '入荷マーク
      If peKonInput.Header.Denku <> DenkuType.Keiyaku _
       AndAlso detail.MasterKubun = MasterKubunType.Ippan Then
        detail.KanMark = NyukaMarkType.Suru
      Else
        detail.KanMark = NyukaMarkType.Shinai
      End If
      detail.HyojunKakaku2 = inputJUCD.HyojunKakaku2
      detail.HyojunKakaku = inputJUCD.HyojunKakaku
      detail.ZeiRitsu = inputJUCD.ZeiRitsu
      detail.SotoZeigaku = inputJUCD.SotoZeigaku
      detail.UchiZeigaku = inputJUCD.UchiZeigaku
      detail.SyokonKeisan = inputJUCD.SyokonKeisan
      detail.SyohinKomoku1 = inputJUCD.SyohinKomoku1
      detail.SyohinKomoku2 = inputJUCD.SyohinKomoku2
      detail.SyohinKomoku3 = inputJUCD.SyohinKomoku3
      detail.DenpyoKomoku1 = inputJUCD.UriageKomoku1
      detail.DenpyoKomoku2 = inputJUCD.UriageKomoku2
      detail.DenpyoKomoku3 = inputJUCD.UriageKomoku3
      detail.RendoHeaderId = beInputJUC.InputJUCH.Id      '連動ヘッダーID
      detail.RendoSequence = inputJUCD.Sequence           '連動SEQ
      detail.RendoShiji = PCA.TSC.Kon.BusinessEntity.Defines.RendoShijiType.Zensu    '連動指示
      detail.YobiShort1 = inputJUCD.YobiShort1
      detail.YobiShort2 = inputJUCD.YobiShort2
      detail.YobiShort3 = inputJUCD.YobiShort3
      detail.YobiInt1 = inputJUCD.YobiInt1
      detail.YobiInt2 = inputJUCD.YobiInt2
      detail.YobiInt3 = inputJUCD.YobiInt3
      detail.YobiDecimal1 = inputJUCD.YobiDecimal1
      detail.YobiDecimal2 = inputJUCD.YobiDecimal2
      detail.YobiDecimal3 = inputJUCD.YobiDecimal3
      detail.YobiString1 = inputJUCD.YobiString1
      detail.YobiString2 = inputJUCD.YobiString2
      detail.YobiString3 = inputJUCD.YobiString3
      detail.IrisuKeta = inputJUCD.IrisuKeta
      detail.HakosuKeta = inputJUCD.HakosuKeta
      detail.SuryoHasu = inputJUCD.SuryoHasu
      '原価端数
      detail.GenkaHasu = peKonInput.Header.KingakuHasu

      peKonInput.DetailList.Add(detail)

    Next

    ' 合計
    peKonInput.Total.HeaderId = beInputJUC.InputJUCT.HeaderId
    peKonInput.Total.KingakuGokei = beInputJUC.InputJUCT.KingakuGokei
    peKonInput.Total.SotoZeiGokei = beInputJUC.InputJUCT.SotoZeiGokei
    peKonInput.Total.UchiZeiGokei = beInputJUC.InputJUCT.UchiZeiGokei

    ' 税率別合計
    Dim subtotal As New PEKonInputSubtotal()

    For Each inputJUCZ As BEInputMitJucHacZ In beInputJUC.InputJUCZList.InputJUCZ
      subtotal = New PEKonInputSubtotal() With { _
          .HeaderId = inputJUCZ.HeaderId, _
          .ZeiRitsu = inputJUCZ.ZeiRitsu, _
          .KingakuGokei = inputJUCZ.KingakuGokei, _
          .SotoZeiGokei = inputJUCZ.SotoZeiGokei, _
          .UchiZeiGokei = inputJUCZ.UchiZeiGokei _
      }
      peKonInput.SubtotalList.Add(subtotal)
    Next

    '合計を再計算
    'Me.CalcTotal(peKonInput, True, Tools.PEKonInput.DenpyoType.Uriage)
    MyBase.ReCalcDenpyo(peKonInput, True)

  End Sub

  ''' <summary>見積／受注Noから見積／受注伝票を取得し、伝票入力エンティティクラスに設定
  ''' </summary>
  ''' <param name="mitsumoriJuchuNo">見積／受注No</param>
  ''' <param name="peKonInput">セットする伝票</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="sameTokuisaki">入力済得意先コードと見積・受注伝票の得意先コードが不一致の時はFalse。</param>
  Public Function GetLinkMitJucDenpyo(ByVal mitsumoriJuchuNo As Integer, ByRef peKonInput As PEKonInput, ByVal needMessageBox As Boolean, ByRef sameTokuisaki As Boolean) As Boolean
    '
    If mitsumoriJuchuNo = 0 Then
      '見積／受注No未入力時
      Return True
    End If

    '得意先の変更確認の為に退避
    Dim oldTokuisakiCode As String = peKonInput.Header.TorihikisakiCode
    '得意先が同一か否か
    sameTokuisaki = True

    If peKonInput.Header.MitsumoriJuchuKubun = MitsumoriJuchuKubunType.Mitsumori Then
      '見積伝票を取得
      Dim beInputMIT As BEInputMIT = MyBase.FindByMitsumoriNo(mitsumoriJuchuNo, True)
      If beInputMIT.InputMITH.Id = 0 Then
        Return False
      End If

      If Not String.IsNullOrEmpty(oldTokuisakiCode) _
      AndAlso oldTokuisakiCode <> beInputMIT.InputMITH.TokuisakiCode Then
        '得意先コード入力済みで、かつ得意先コードが不一致の時
        sameTokuisaki = False
      End If
      '伝票入力エンティティに設定
      If sameTokuisaki Then
        Me.PrepareItemsFromMitsumori(peKonInput, beInputMIT, peKonInput.Header.TorihikisakiCode)
      Else
        Me.PrepareItemsFromMitsumori(peKonInput, beInputMIT, String.Empty)
      End If

    Else
      '受注伝票を取得
      Dim beInputJUC As BEInputJUC = MyBase.FindByJuchuNo(mitsumoriJuchuNo, True)
      If beInputJUC.InputJUCH.Id = 0 Then
        Return False
      End If

      ''◆20150519 ktsuda 取得時、受注伝票にロックをかける

      If IsNothing(beInputJUC.InputJUCDList.BEInputJUCD.Find(Function(match) _
                  (match.SyohinCode <> String.Empty AndAlso match.SyukazumiFlag = SyukazumiFlagType.Misyuka))) Then
        '未出荷の受注明細が存在しない時
        ShowMessage.OnExclamation(CType(Me, IWin32Window), _
                                  String.Format(MessageDefines.Exclamation.CantRendo, BusinessEntity.ItemText.Juchu, BusinessEntity.ItemText.Syuka), _
                                  CommonVariable.BasicConfig.ProgramName)
        Return False
      End If

      If Not String.IsNullOrEmpty(oldTokuisakiCode) _
      AndAlso oldTokuisakiCode <> beInputJUC.InputJUCH.TokuisakiCode Then
        '得意先コード入力済みで、かつ得意先コードが不一致の時
        sameTokuisaki = False
      End If

      '伝票入力の伝票入力エンティティクラスを設定
      If sameTokuisaki Then
        Me.PrepareRendoFromJuchu(peKonInput, beInputJUC, peKonInput.Header.TorihikisakiCode)
      Else
        Me.PrepareRendoFromJuchu(peKonInput, beInputJUC, String.Empty)
      End If

    End If

    'ローカルで保持する明細データコレクションを初期化
    Dim limit As Integer = InputDefines.MaximumDataCount - peKonInput.DetailList.Count
    For i As Integer = 0 To limit - 1
      peKonInput.DetailList.Add(New PEKonInputDetail())
    Next
    'Me.ModifiedEntity = peKonInput.Clone()

    Return True

  End Function

  ''' <summary>
  ''' 見積／受注Noがクリアされた場合の処理を行います
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <remarks></remarks>
  Public Sub ClearMitsumoriJuchuNo(ByVal peKonInput As PEKonInput)
    'Public Sub ClearedMitsumoriJuchuNo(ByVal peKonInput As PEKonInput)
    ' ロックを解除する
    Me.UnLockJuchu()

    peKonInput.Header.RendoDenpyoNo = 0

    ' スポット得意先を連動させていたらクリアする
    If peKonInput.Header.TorihikisakiCode = MakeText.MakeCommonCode(Me.BEKihonJoho.TokuisakiKeta) Then
      If peKonInput.Header.SpotAitesakiId > 0 Then
        peKonInput.Header.SpotAitesakiId = 0
      End If
    End If

    ' 全明細と直送先をクリアして再表示する
    peKonInput.Header.ChokusosakiCode = String.Empty
    peKonInput.Header.ChokusosakiMei1 = String.Empty
    peKonInput.Header.ChokusosakiMei2 = String.Empty
    peKonInput.Header.ChokusosakiComment = String.Empty

    For i As Integer = 0 To peKonInput.DetailList.Count - 1
      peKonInput.DetailList(i) = New PEKonInputDetail()
    Next
    ' 合計の再計算
    Me.CalcTotal(peKonInput, True, peKonInput.DenpyoType.Uriage)

    ' 画面更新
    'Me.UpdateLocalEntity(peKonInput)
    Me.ModifiedEntity = peKonInput.Clone()

  End Sub

#End Region

#Region "ツール"

  ''' <summary>与信限度額をチェックし、続行確認を表示。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <remarks></remarks>
  Public Function CheckYosinGendogakuAndPrompt(ByVal peKonInput As PEKonInput) As Boolean
    '
    Dim tokuisaki As BEMasterTms = MyBase.FindBEMasterTms(peKonInput.Header.TorihikisakiCode)
    Dim seikyusaki As BEMasterTms = FindSeikyusaki(tokuisaki)
    peKonInput.Header.MisyuZandaka = KonCalc.CalcMisyuZandaka(seikyusaki)

    If peKonInput.Header.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Kake _
    OrElse peKonInput.Header.YoshinGendogaku = 0D Then
      '掛売伝票以外、与信限度額未設定の時は対象外
      Return True
    End If

    Dim curGokei As Decimal = peKonInput.Total.KingakuGokei + peKonInput.Total.SotoZeiGokei + peKonInput.Total.Hosei
    'If Me.Mode = InputDefines.ModeType.ModifyMode Then
    '  '更新モード時
    '  If Me.SelectedEntity.Header.TorihikisakiCode = peKonInput.Header.TorihikisakiCode Then
    '    '得意先が未変更の時は、変更前の金額を減算
    '    curGokei = curGokei - (Me.SelectedEntity.Total.KingakuGokei + Me.SelectedEntity.Total.SotoZeiGokei + Me.SelectedEntity.Total.Hosei)
    '  End If
    'End If

    '与信限度額
    Dim yoshinGendogaku As Decimal = peKonInput.Header.YoshinGendogaku

    If (yoshinGendogaku - peKonInput.Header.MisyuZandaka) < curGokei Then
      '取引限度額チェック画面
      Dim dlg As New YoshinDialog(Me.Mode = InputDefines.ModeType.ModifyMode)

      '与信限度額、売掛残高、取引限度額
      dlg.YoshinGendogaku = EditToText.EditKingakuCell(peKonInput.Header.YoshinGendogaku, 0, False)
      dlg.UrikakekinZandaka = EditToText.EditKingakuCell(peKonInput.Header.MisyuZandaka, 0, False)
      dlg.TorihikiGendogaku = EditToText.EditKingakuCell(peKonInput.Header.YoshinGendogaku - peKonInput.Header.MisyuZandaka, 0, False)
      dlg.LineGendogaku = String.Empty

      '取引限度額チェック画面を表示し処理続行確認
      If dlg.ShowDialog() <> DialogResult.OK Then
        Return False
      End If
    Else
      '与信限度額自体の次に、警告ラインでチェック
      Return Me.CheckYosinGendoLineAndPrompt(peKonInput)
    End If

    Return True

  End Function

  ''' <summary>与信限度額を警告ラインでチェック、続行確認を表示。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  Public Function CheckYosinGendoLineAndPrompt(ByVal peKonInput As PEKonInput) As Boolean
    '
    If peKonInput.Header.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Kake _
    OrElse peKonInput.Header.YoshinGendogaku = 0D _
    OrElse MyBase.BEKihonJoho.YoshingendoLine = 0 Then
      '掛売伝票以外、与信限度額未設定、与信限度警告ライン未設定の時は対象外
      Return True
    End If

    Dim curGokei As Decimal = peKonInput.Total.KingakuGokei + peKonInput.Total.SotoZeiGokei + peKonInput.Total.Hosei
    'If MyBase.Mode = InputDefines.ModeType.ModifyMode Then
    '  '更新モード時
    '  If MyBase.SelectedEntity.Header.TorihikisakiCode = peKonInput.Header.TorihikisakiCode Then
    '    '得意先が未変更の時は、変更前の金額を減算
    '    curGokei = curGokei - (MyBase.SelectedEntity.Total.KingakuGokei + MyBase.SelectedEntity.Total.SotoZeiGokei + MyBase.SelectedEntity.Total.Hosei)
    '  End If
    'End If

    '基本情報の与信限度額警告ラインを乗算し警告ラインの取引限度額を算出
    Dim yoshinGendogaku As Decimal = peKonInput.Header.YoshinGendogaku * CDec(_BEKihonJoho.YoshingendoLine) / 1000D

    If (yoshinGendogaku - peKonInput.Header.MisyuZandaka) < curGokei Then
      '取引限度額チェック画面
      Dim dlg As New YoshinDialog(Me.Mode = InputDefines.ModeType.ModifyMode)

      '与信限度額、売掛残高、取引限度額、警告ラインの取引限度額
      dlg.YoshinGendogaku = EditToText.EditKingakuCell(peKonInput.Header.YoshinGendogaku, 0, False)
      dlg.UrikakekinZandaka = EditToText.EditKingakuCell(peKonInput.Header.MisyuZandaka, 0, False)
      dlg.TorihikiGendogaku = EditToText.EditKingakuCell(peKonInput.Header.YoshinGendogaku - peKonInput.Header.MisyuZandaka, 0, False)
      dlg.LineGendogaku = EditToText.EditKingakuCell(yoshinGendogaku - peKonInput.Header.MisyuZandaka, 0, False)

      Me.ShownMessage = True

      '取引限度額チェック画面を表示し処理続行確認
      If dlg.ShowDialog() <> DialogResult.OK Then
        Return False
      End If
    End If

    Return True

  End Function

  ''' <summary>伝区（区）により入荷マークを設定。
  ''' </summary>
  ''' <param name="detail">伝票入力エンティティ明細</param>
  ''' <param name="denku">伝区</param>
  Public Sub SetNyukaMarkByKu(ByRef detail As PEKonInputDetail, ByVal denku As PCA.TSC.Kon.BusinessEntity.Defines.DenkuType)
    '
    If detail.Ku = PCA.TSC.Kon.BusinessEntity.Defines.KuType.Tankateisei Then
      '区が単価訂正時
      detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Shinai
      Return
    End If

    '商品マスターを取得
    Dim beMasterSms As BEMasterSms = MyBase.FindBEMasterSms(detail.SyohinCode)

    Select Case beMasterSms.SystemKubun
      Case PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.KonKan, PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.Kan
        'システム区分が共用、仕入専用
        If detail.Ku = PCA.TSC.Kon.BusinessEntity.Defines.KuType.Tsujo Or detail.Ku = PCA.TSC.Kon.BusinessEntity.Defines.KuType.Henpin Then
          '区が通常、返品
          If denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Keiyaku Then
            '契約伝票以外
            detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Suru
          Else
            detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Shinai
          End If
        Else
          detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Shinai
        End If
      Case PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.Kon
        '売上専用
        detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Shinai
    End Select

  End Sub

  'Hien edited
  Public Overloads Function CheckGenkawareAndPrompt(ByVal m_controller As InputSYKController()) As Boolean

    Try
      '明細単位でのチェック
      Dim errorCodeList As New List(Of String)
      For intCount As Integer = 0 To m_controller.Count - 1
        If m_controller(intCount) Is Nothing Then Exit For
        Dim detailList = m_controller(intCount).ModifiedEntity.DetailList
        For i As Integer = 0 To detailList.Count - 1
          If Not String.IsNullOrEmpty(detailList(i).SyohinCode) _
          AndAlso ((detailList(i).MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan AndAlso detailList(i).Ku <> BusinessEntity.Defines.KuType.Tankateisei) _
                  OrElse detailList(i).MasterKubun = BusinessEntity.Defines.MasterKubunType.Zatsuhin) Then
            '一般商品の売上と返品、雑商品
            If Math.Abs(detailList(i).Kingaku) < Math.Abs(detailList(i).Genka) Then
              errorCodeList.Add(String.Format("伝票No：{0,2} の {1,2}行目：商品コード＝" & detailList(i).SyohinCode, m_controller(intCount).ModifiedEntity.Header.YobiInt2, detailList(i).Edaban))
            End If
          End If
        Next
      Next

      If errorCodeList IsNot Nothing AndAlso errorCodeList.Count > 0 Then
        Me.ShownMessage = True
        Dim action As String = (If(Me.Mode = InputDefines.ModeType.ModifyMode, BusinessEntity.ItemText.Kosin, BusinessEntity.ItemText.Toroku))
        Dim isOkVisible As Boolean = False
        Dim icon As Controls.PcaGuideLabel.Icons = Controls.PcaGuideLabel.Icons.Exclamation
        Dim errorMessage As String = String.Empty

        If _BEKihonJoho.CantUpdGenkaware = BusinessEntity.Defines.CantUpdType.Can Then
          '原価割れの伝票は登録・更新できる
          isOkVisible = True
          errorMessage = String.Format(MessageDefines.Question.SaveGenkaware, action)
          icon = Controls.PcaGuideLabel.Icons.Question
        Else
          '原価割れの伝票は登録・更新できない
          isOkVisible = False
          errorMessage = String.Format(MessageDefines.Exclamation.CantSaveGenkaware, action)
          icon = Controls.PcaGuideLabel.Icons.Exclamation
        End If


        Using listBoxDialog As New TscListBoxDialog(CommonVariable.BasicConfig.ProgramName, icon, errorMessage, 63, 2, errorCodeList)
          listBoxDialog.OkText = "続行"
          listBoxDialog.OkVisible = CStr(isOkVisible)
          listBoxDialog.CancelVisible = CStr(True)
          listBoxDialog.CancelText = (If(isOkVisible, "キャンセル", "閉じる"))
          listBoxDialog.Location = New Point(0, 0)
          If listBoxDialog.ShowDialog() = DialogResult.Cancel Then
            Return False
          End If
        End Using
      End If

      Return True
    Catch ex As Exception

      Return False
    End Try

  End Function


  ''' <summary>利益率割れチェックを行い、会社基本情報の設定に従い続行確認を表示。
  ''' （補足）エラー無しを含め、処理続行の時はTrueを返す。
  ''' </summary>
  ''' <param name="denpyoType">伝票種類</param>
  Public Overloads Function CheckSaiteiRiekiritsuAndPrompt(ByVal m_controller As InputSYKController(), ByVal denpyoType As PEKonInput.DenpyoType) As Boolean
    '
    '会社基本情報の最低利益率が未設定時はチェック対象外
    If _BEKihonJoho.SaiteiRiekiritsu = 0 Then
      Return True
    End If

    Dim ararieki As Decimal = 0D
    Dim zappi As Decimal = 0D
    Dim needCheck As Boolean = False
    Dim errorCodeList As New List(Of String)
    For intCount As Integer = 0 To m_controller.Count - 1

      For Each detail As PEKonInputDetail In m_controller(intCount).ModifiedEntity.DetailList
        If KonCalc.JudgeDetailState(denpyoType, detail) = Tools.PEKonInput.MeisaiStateType.Valid Then
          If detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan _
          OrElse detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Zatsuhin _
          OrElse detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Nebiki Then
            needCheck = True
          End If

          ararieki = ararieki + detail.Ararieki
          If detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Zappi Then
            '諸雑費は内税を除く
            zappi = zappi + (detail.Kingaku - detail.UchiZeigaku)
          End If
        End If
      Next

      '明細に諸雑費、記事以外の商品が無い時はチェック対象外
      If needCheck = False Then
        Return True
      End If

      Dim gokei As Decimal = m_controller(intCount).ModifiedEntity.Total.KingakuGokei - m_controller(intCount).ModifiedEntity.Total.UchiZeiGokei - zappi

      '会社基本情報の最低利益率は整数化してある為、10倍して桁合せ後に比較
      Dim riekiRitsu As Decimal = KonCalc.CalcRiekiRitsu(ararieki, gokei, 0) * 10D

      If riekiRitsu < CDec(_BEKihonJoho.SaiteiRiekiritsu) Then
        'If riekiRitsu < CDec(50) Then
        errorCodeList.Add(String.Format("伝票No：{0,2} ", m_controller(intCount).ModifiedEntity.Header.YobiInt2))
      End If

    Next

    If (_BEKihonJoho.CantUpdRiekiritsu = BusinessEntity.Defines.CantUpdType.Can) Then
      '続行確認
      Using listBoxDialog As New TscListBoxDialog(CommonVariable.BasicConfig.ProgramName, Controls.PcaGuideLabel.Icons.Question, String.Format(MessageDefines.Question.SaveZaikowareRiekiritsu, BusinessEntity.ItemText.Toroku), 63, 2, errorCodeList)
        listBoxDialog.OkText = "続行"
        listBoxDialog.OkVisible = CStr(True)
        listBoxDialog.CancelVisible = CStr(True)
        listBoxDialog.CancelText = (If(True, "キャンセル", "閉じる"))

        If listBoxDialog.ShowDialog() = DialogResult.Cancel Then
          Return False
        End If
      End Using
    Else
      '登録不可
      ShowMessage.OnExclamation(m_Owner, String.Format(MessageDefines.Exclamation.CantSaveRiekiritsu, BusinessEntity.ItemText.Toroku), CommonVariable.BasicConfig.ProgramName)
      Return False
    End If

    Return True

  End Function

  ''' <summary>小数桁を含む各単価の値範囲をチェック。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティクラス</param>
  ''' <param name="denpyoType">伝票入力種別</param>
  Public Overloads Function CheckTankaKeta(peKonInput As PEKonInput, denpyoType As Tools.PEKonInput.DenpyoType, slipNo As Integer) As Boolean
    '
    If peKonInput Is Nothing Or peKonInput.DetailList Is Nothing OrElse peKonInput.DetailList.Count = 0 Then
      Return True
    End If

    Dim errorMessage As String = String.Empty
    Dim kon As Boolean = False

    Select Case denpyoType
      Case Tools.PEKonInput.DenpyoType.Mitsumori, _
       Tools.PEKonInput.DenpyoType.Juchu, _
       Tools.PEKonInput.DenpyoType.Uriage
        '見積、受注、売上
        kon = True
    End Select

    For i As Integer = 0 To peKonInput.DetailList.Count - 1

      Dim detail As PEKonInputDetail = peKonInput.DetailList(i)

      If String.IsNullOrEmpty(detail.SyohinCode) OrElse detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Kiji Then
        Continue For
      End If

      Dim curMax As Decimal = 0
      Dim curMin As Decimal = 0
      Dim tankaKeta As Short = 0

      If detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan Then
        '一般商品は小数桁の設定値、以外は小数桁はゼロ固定
        tankaKeta = detail.TankaKeta
      End If

      Select Case tankaKeta
        Case 0
          curMax = InputDefines.MaximumTanka0
          curMin = InputDefines.MinimumTanka0
          Exit Select
        Case 1
          curMax = InputDefines.MaximumTanka1
          curMin = InputDefines.MinimumTanka1
          Exit Select
        Case 2
          curMax = InputDefines.MaximumTanka2
          curMin = InputDefines.MinimumTanka2
          Exit Select
        Case 3
          curMax = InputDefines.MaximumTanka3
          curMin = InputDefines.MinimumTanka3
          Exit Select
        Case 4
          curMax = InputDefines.MaximumTanka4
          curMin = InputDefines.MinimumTanka4
          Exit Select
      End Select

      If detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan Then
        '一般商品
        Select Case denpyoType
          Case Tools.PEKonInput.DenpyoType.Uriage, _
               Tools.PEKonInput.DenpyoType.Shire
            If (detail.Ku = BusinessEntity.Defines.KuType.Tankateisei AndAlso curMin > detail.Tanka) _
            OrElse (detail.Ku <> BusinessEntity.Defines.KuType.Tankateisei AndAlso 0 > detail.Tanka) _
            OrElse curMax < detail.Tanka Then
              '売上、仕入の単価訂正時のみ単価に負値を許す
              errorMessage = "単価が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If
          Case Else
            If (0 > detail.Tanka) OrElse (curMax < detail.Tanka) Then
              errorMessage = "単価が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If
        End Select

        If KonCalc.CountSyosuKeta(detail.Tanka) > CInt(tankaKeta) Then
          errorMessage = "単価が設定可能小数桁を超えている明細が存在するため登録できません。"
          Exit For
        End If

        If kon Then
          '販売管理のみ
          If (0 > detail.GenTanka) OrElse (curMax < detail.GenTanka) Then
            errorMessage = "原単価が設定可能範囲を超えている明細が存在するため登録できません。"
            Exit For
          End If

          If (curMin > detail.BaiTanka) OrElse (curMax < detail.BaiTanka) Then
            errorMessage = "売単価が設定可能範囲を超えている明細が存在するため登録できません。"
            Exit For
          End If

          If KonCalc.CountSyosuKeta(detail.GenTanka) > CInt(tankaKeta) Then
            errorMessage = "原単価が設定可能小数桁を超えている明細が存在するため登録できません。"
            Exit For
          End If

          If KonCalc.CountSyosuKeta(detail.BaiTanka) > CInt(tankaKeta) Then
            errorMessage = "売単価が設定可能小数桁を超えている明細が存在するため登録できません。"
            Exit For
          End If
        End If
      End If

      Select Case detail.MasterKubun
        Case BusinessEntity.Defines.MasterKubunType.Ippan, _
             BusinessEntity.Defines.MasterKubunType.Zatsuhin, _
             BusinessEntity.Defines.MasterKubunType.Zappi
          '一般商品、雑商品、諸雑費
          If kon Then
            '販売管理
            If 0 > detail.HyojunKakaku2 OrElse curMax < detail.HyojunKakaku2 Then
              errorMessage = "標準価格が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If curMin > detail.HyojunKakaku OrElse curMax < detail.HyojunKakaku Then
              errorMessage = "標準価格が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If KonCalc.CountSyosuKeta(detail.HyojunKakaku2) > CInt(tankaKeta) Then
              errorMessage = "標準価格が設定可能小数桁を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If KonCalc.CountSyosuKeta(detail.HyojunKakaku) > CInt(tankaKeta) Then
              errorMessage = "標準価格が設定可能小数桁を超えている明細が存在するため登録できません。"
              Exit For
            End If
          Else
            '仕入管理
            If (curMin > detail.HyojunShireTanka) OrElse (curMax < detail.HyojunShireTanka) Then
              errorMessage = "標準仕入単価が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If KonCalc.CountSyosuKeta(detail.HyojunShireTanka) > CInt(tankaKeta) Then
              errorMessage = "標準仕入単価が設定可能小数桁を超えている明細が存在するため登録できません。"
              Exit For
            End If

          End If
      End Select

    Next

    If Not String.IsNullOrEmpty(errorMessage) Then
      '何れかのエラーが存在する時
      ShowMessage.OnExclamation(m_Owner, errorMessage, "伝票No：" & slipNo)
      Return False
    End If

    Return True

  End Function

  ' Public Function CheckUriageAndPrompt(ByVal peKonInput As PEKonInput, ByVal checkType As Boolean, slipNo As Integer) As Boolean
  '
  '原価割れ、利益率割れのチェック、通知は常に行う
  '(チェックをする/しない/登録更新できないは次バージョン以降対応予定)
  ''' <summary>原価割れ、利益率割れ、与信限度額、在庫割れの各チェックと続行確認を実施。
  ''' （補足）
  ''' </summary>
  ''' <param name="m_controller"></param>
  ''' <param name="checkType">True=登録禁止、False=続行確認</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CheckUriageAndPrompt(ByVal m_controller As InputSYKController(), ByVal checkType As Boolean) As Boolean
    CommonVariable.BasicConfig = New PCA.TSC.Kon.Tools.IApplicationBasicConfig
    CommonVariable.BasicConfig.ProgramName = "出荷振替"

    If CheckGenkawareAndPrompt(m_controller) = False Then
      Return False
    End If

    '利益率割れをチェック
    If CheckSaiteiRiekiritsuAndPrompt(m_controller, PEKonInput.DenpyoType.Uriage) = False Then
      Return False
    End If

    '与信限度額をチェック
    For intCount As Integer = 0 To m_controller.Count - 1
      For intDetailsCount As Integer = 0 To m_controller(intCount).ModifiedEntity.DetailList.Count - 1
        If m_controller(intCount).ModifiedEntity.DetailList(intDetailsCount).SyohinCode <> "" Then
          m_controller(intCount).CalcMeisai(InputDefines.ChangedItem.Suryo, m_controller(intCount).ModifiedEntity, intDetailsCount, True, True, "")
        End If
      Next
      If CheckYosinGendogakuAndPrompt(m_controller(intCount).ModifiedEntity) = False Then
        Return False
      End If
    Next

    If  ((MyBase.BEKihonJoho.ZaikowareCheckUriage = BusinessEntity.Defines.ZaikowareCheckType.Cant AndAlso checkType = True) _
          OrElse (MyBase.BEKihonJoho.ZaikowareCheckUriage = BusinessEntity.Defines.ZaikowareCheckType.Check AndAlso checkType = False)) Then 'm_controller(intCount).ModifiedEntity.Header.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Keiyaku _   AndAlso
      '在庫割れをチェック
      If CheckZaikowareAndPrompt(m_controller, PEKonInput.DenpyoType.Uriage) = False Then
        Return False
      End If
    End If

    Return True

  End Function

  ''' <summary>
  ''' 登録前の在庫割れチェックを行います。基本情報に従いエラーか続行確認を出します。
  ''' </summary>
  ''' <param name="denpyoType">伝票種類</param>
  ''' <returns>在庫割れなし、または続行を選択したときTrueを返します。</returns>  '2016/03/01 Hien edited
  Public Overloads Function CheckZaikowareAndPrompt(ByVal m_controller As InputSYKController(), _
                                          ByVal denpyoType As Tools.PEKonInput.DenpyoType) As Boolean
    '
    'If _IsOnlyKon Then
    '  '商魂のみ導入時はチェック不要
    '  Return True
    'End If
    Dim errorMessageList As New List(Of String)
    Dim messageString As String = String.Empty
    Dim addLine As Integer = 0
    For intCount As Integer = 0 To m_controller.Count - 1
      Dim detailList = m_controller(intCount).ModifiedEntity.DetailList


      Dim zaikowareCheck As BusinessEntity.Defines.ZaikowareCheckType = JudgeZaikowareCheck(denpyoType)
      If zaikowareCheck = BusinessEntity.Defines.ZaikowareCheckType.NoCheck Then
        'チェック不要
        Return True
      End If

      '在庫割れを一括チェック
      Dim result As List(Of BEZaikosuNoSyokai) = Me.BatchCheckZaikoware(detailList, True, True, denpyoType)
      '***upd***top 15.12.04 *bug*
      'If result IsNot Nothing Then
      If result Is Nothing Then
        '***upd***btm 15.12.04 *bug*
        Return True
      End If

      Dim zaikowareList As New List(Of String)    '在庫割れ分のメッセージ文言リスト
      Dim chumonwareList As New List(Of String)   '注文点警告ライン割れ分のメッセージ文言リスト

      Me.ShownMessage = True

      For Each target In result
        Dim errorMessage As String = String.Empty

        If _BEKihonJoho.ZaikoKanri = BusinessEntity.Defines.KanriType.Suru Then
          '倉庫別在庫管理する
          errorMessage = "商品コード＝" & TscStringTool.PadRightSpace(target.SyohinCode, _BEKihonJoho.SyohinKeta) & ", 倉庫コード＝" & TscStringTool.PadRightSpace(target.SokoCode, _BEKihonJoho.SokoKeta)
        Else
          errorMessage = "商品コード＝" & TscStringTool.PadRightSpace(target.SyohinCode, _BEKihonJoho.SyohinKeta)
        End If

        If target.Chumonten = 0 Then
          ' 在庫割れ（zms_chu=0）
          zaikowareList.Add(errorMessage)
        Else
          ' 注文点警告ライン割れ（zms_chu=1）
          chumonwareList.Add(errorMessage)
        End If
      Next


      If zaikowareList.Count() > 0 AndAlso chumonwareList.Count() > 0 Then
        '両方の割れがある時はタイトルを付加
        errorMessageList.Add("伝票番号：" & m_controller(intCount).ModifiedEntity.Header.YobiInt2)
        errorMessageList.Add("■在庫不足")
        errorMessageList.AddRange(zaikowareList)
        errorMessageList.Add("■注文点警告ライン割れ")
        errorMessageList.AddRange(chumonwareList)
      Else
        If zaikowareList.Count > 0 Then
          errorMessageList.AddRange(zaikowareList)
        Else
          errorMessageList.AddRange(chumonwareList)
        End If
      End If

      Dim action As String = (If(Me.Mode = InputDefines.ModeType.ModifyMode, BusinessEntity.ItemText.Kosin, BusinessEntity.ItemText.Toroku))
      'チェックする（登録・更新は可）、または「注文点警告」のみの場合は「続行」あり
      Dim okVisible As Boolean = (zaikowareCheck = BusinessEntity.Defines.ZaikowareCheckType.Check OrElse zaikowareList.Count() = 0)

      If zaikowareList.Count() = 0 Then
        messageString = String.Format("{0}しようとする伝票に、注文点警告ラインを下回った商品が存在します。" & vbCrLf & "ご注意ください。", action)
      Else
        If zaikowareCheck = BusinessEntity.Defines.ZaikowareCheckType.Check Then
          messageString = String.Format("{0}しようとする伝票に、在庫数が不足している商品が存在します。" & vbCrLf & "{0}してもよろしいですか？", action)
        Else
          messageString = String.Format("{0}しようとする伝票に、在庫数が不足している商品が存在します。" & vbCrLf & "基本情報の設定で制限されていて{0}できません。", action)
        End If

        If chumonwareList.Count() <> 0 Then
          ' 注文点メッセージも追加
          messageString = messageString & vbCrLf & "注文点警告ラインを下回った商品もあります。ご注意ください。"
          addLine += 1
        End If
      End If
    Next
    Using listBoxDialog As New TscListBoxDialog(CommonVariable.BasicConfig.ProgramName, _
      (PCA.Controls.PcaGuideLabel.Icons.Question), _
      messageString, _
      70, _
      2 + addLine, errorMessageList)
      listBoxDialog.OkText = "続行"
      listBoxDialog.OkVisible = CStr(True)
      listBoxDialog.CancelVisible = CStr(True)
      listBoxDialog.CancelText = "キャンセル"
      listBoxDialog.StartPosition = FormStartPosition.CenterParent
      If listBoxDialog.ShowDialog() = DialogResult.Cancel Then
        Return False
      End If
    End Using

    Return True
  End Function

  ''' <summary>
  ''' 請求済、在庫締切済の登録・更新かどうかをチェックします。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ、削除時はnothingでも可</param>
  ''' <param name="isDelete">削除時チェックかどうか</param>
  ''' <returns>true:続行、false:中止</returns>
  ''' <remarks></remarks>
  Public Overloads Function CheckSeiZaikoZumiAndPrompt(ByVal peKonInput As PEKonInput, ByVal isDelete As Boolean) As Boolean
    '
    Dim seiText As String = String.Empty
    Dim errorType As Integer = 0

    '請求、精算の何れかを設定
    If MyBase.SystemKubun = BusinessEntity.Defines.SystemKubunType.Kan Then
      seiText = BusinessEntity.ItemText.Seikyu
    Else
      seiText = BusinessEntity.ItemText.Seisan
    End If

    If Me.Mode = InputDefines.ModeType.NewInputMode AndAlso isDelete = False Then
      '新規登録時、入力中の伝票入力エンティティをチェック
      If IsSeiShimeZumi(peKonInput) Then
        '請求済チェック
        errorType = 1
      End If
      If IsZaikoShimeZumi(peKonInput) Then
        '在庫締切済チェック
        errorType = errorType + 2
      End If

    ElseIf Me.Mode = InputDefines.ModeType.ModifyMode AndAlso isDelete = False Then
      '更新時、入力中と入力前の伝票入力エンティティをチェック
      If IsSeiShimeZumi(peKonInput) _
      OrElse IsSeiShimeZumi(SelectedEntity) Then
        '請求済チェック
        errorType = 1
      End If

      If IsZaikoShimeZumi(peKonInput) _
      OrElse IsZaikoShimeZumi(SelectedEntity) Then
        ' 在庫締切済チェック
        errorType = errorType + 2
      End If

    ElseIf isDelete = True Then
      '削除時、入力前の伝票入力エンティティをチェック
      If IsSeiShimeZumi(SelectedEntity) Then
        '請求済チェック
        errorType = 1
      End If

      If IsZaikoShimeZumi(SelectedEntity) Then
        '在庫締切済チェック
        errorType = errorType + 2
      End If
    Else
      Return True
    End If

    '請求（精算）締、在庫締切以前の伝票
    If errorType = 0 Then
      '削除時は通常の確認メッセージ
      If isDelete = True Then
        Dim dialogResult As DialogResult = ShowMessage.OnQuestion(m_Owner, MessageDefines.Question.DeleteDenpyo, CommonVariable.BasicConfig.ProgramName)
        Return (dialogResult = Windows.Forms.DialogResult.Yes)
      Else
        Return True
      End If
    End If

    '基本情報での制限
    If errorType = 1 AndAlso _BEKihonJoho.CantUpdSei = BusinessEntity.Defines.CantUpdType.Cant Then
      '＜請求（精算）済＞
      ShowMessage.OnExclamation(m_Owner, String.Format(MessageDefines.Information.CantUpdateSei, seiText), CommonVariable.BasicConfig.ProgramName)
      Return False
    ElseIf errorType = 2 AndAlso _BEKihonJoho.CantUpdZaikoShimekiri = BusinessEntity.Defines.CantUpdType.Cant Then
      '＜在庫締切済＞
      ShowMessage.OnExclamation(m_Owner, MessageDefines.Information.CantUpdateZaikoShimekiri, CommonVariable.BasicConfig.ProgramName)
      Return False
    ElseIf errorType = 3 AndAlso (_BEKihonJoho.CantUpdSei = BusinessEntity.Defines.CantUpdType.Cant _
     OrElse _BEKihonJoho.CantUpdZaikoShimekiri = BusinessEntity.Defines.CantUpdType.Cant) Then
      '＜請求（精算）済、在庫締切済＞
      ShowMessage.OnExclamation(m_Owner, String.Format(MessageDefines.Information.CantUpdateSeiZaiko, seiText), CommonVariable.BasicConfig.ProgramName)
      Return False
    End If

    Dim dlgResult As DialogResult = DialogResult.None
    '続行確認
    If Me.Mode = InputDefines.ModeType.NewInputMode AndAlso isDelete = False Then
      '新規登録時
      Me.ShownMessage = True
      Select Case errorType
        Case 1
          '＜請求（精算）済＞
          dlgResult = ShowMessage.OnQuestion(m_Owner, String.Format(MessageDefines.Question.CreateSeizumi, seiText), CommonVariable.BasicConfig.ProgramName)
          Return (dlgResult = Windows.Forms.DialogResult.Yes)

        Case 2
          '＜在庫締切済＞
          dlgResult = ShowMessage.OnQuestion(m_Owner, MessageDefines.Question.CreateZaikoshimekiri, CommonVariable.BasicConfig.ProgramName)
          Return (dlgResult = Windows.Forms.DialogResult.Yes)

        Case Else
          '＜請求（精算）済、在庫締切済＞
          dlgResult = ShowMessage.OnQuestion(m_Owner, String.Format(MessageDefines.Question.CreateSeiZaiko, seiText), CommonVariable.BasicConfig.ProgramName)
          Return (dlgResult = Windows.Forms.DialogResult.Yes)
      End Select

    ElseIf Me.Mode = InputDefines.ModeType.ModifyMode AndAlso isDelete = False Then
      '更新時
      Me.ShownMessage = True
      Select Case errorType
        Case 1
          ' ＜請求（精算）済＞
          dlgResult = ShowMessage.OnQuestion(m_Owner, String.Format(MessageDefines.Question.ModifySeizumi, seiText), CommonVariable.BasicConfig.ProgramName)
          Return (dlgResult = Windows.Forms.DialogResult.Yes)

        Case 2
          ' ＜在庫（精算）締切済＞
          dlgResult = ShowMessage.OnQuestion(m_Owner, MessageDefines.Question.ModifyZaikoshimekiri, CommonVariable.BasicConfig.ProgramName)
          Return (dlgResult = Windows.Forms.DialogResult.Yes)

        Case Else
          ' ＜請求（精算）済、在庫締切済＞
          dlgResult = ShowMessage.OnQuestion(m_Owner, String.Format(MessageDefines.Question.ModifySeiZaiko, seiText), CommonVariable.BasicConfig.ProgramName)
          Return (dlgResult = Windows.Forms.DialogResult.Yes)

      End Select
    ElseIf isDelete = True Then
      ' 削除
      Select Case errorType
        Case 1
          ' ＜請求（精算）済＞
          dlgResult = ShowMessage.OnQuestion(m_Owner, String.Format(MessageDefines.Question.EraseSeizumi, seiText), CommonVariable.BasicConfig.ProgramName)
          Return (dlgResult = Windows.Forms.DialogResult.Yes)

        Case 2
          ' ＜在庫（精算）締切済＞
          dlgResult = ShowMessage.OnQuestion(m_Owner, MessageDefines.Question.EraseZaikozumi, CommonVariable.BasicConfig.ProgramName)
          Return (dlgResult = Windows.Forms.DialogResult.Yes)

        Case Else
          ' ＜請求（精算）済、在庫締切済＞
          dlgResult = ShowMessage.OnQuestion(m_Owner, String.Format(MessageDefines.Question.EraseSeiZaiko, seiText), CommonVariable.BasicConfig.ProgramName)
          Return (dlgResult = Windows.Forms.DialogResult.Yes)
      End Select
    End If

    Return True

  End Function
#End Region

#Region "Private function from MyBase"

  ''' <summary>伝票種類により、会社基本情報の在庫割れチェックの項目値を得る。
  ''' </summary>
  ''' <param name="denpyoType">伝票種類</param>
  Private Function JudgeZaikowareCheck(ByVal denpyoType As PEKonInput.DenpyoType) As BusinessEntity.Defines.ZaikowareCheckType
    '
    Dim zaikowareCheck As BusinessEntity.Defines.ZaikowareCheckType = BusinessEntity.Defines.ZaikowareCheckType.NoCheck

    Select Case denpyoType
      Case PEKonInput.DenpyoType.Mitsumori
        '見積伝票
        zaikowareCheck = _BEKihonJoho.ZaikowareCheckMitsumori
      Case PEKonInput.DenpyoType.Juchu
        '受注伝票
        zaikowareCheck = _BEKihonJoho.ZaikowareCheckJuchu
      Case PEKonInput.DenpyoType.Uriage
        '売上伝票
        zaikowareCheck = _BEKihonJoho.ZaikowareCheckUriage
    End Select

    Return zaikowareCheck

  End Function


  ''' <summary>
  ''' 在庫割れチェックを一括で行い、不足があれば商品コードと倉庫コードをBEZMSで返します。
  ''' 登録チェックする場合のみ警告ラインでのチェックも行います。
  ''' 同一在庫は集計してチェックします。
  ''' </summary>
  ''' <param name="detailList">伝票入力エンティティ明細リスト</param>
  ''' <param name="isSum">集計してチェックするかどうか</param>
  ''' <param name="isRegist">登録チェックするかどうか</param>
  ''' <param name="denpyoType">伝票種類</param>
  ''' <returns>Nothing:問題なし、Nothing以外:在庫不足（BEZMSの商品コードと倉庫コードに対象、"Chumonten"をエラーフラグとして使用{0:在庫割れ、1:警告ライン割れ}）</returns>
  Private Function BatchCheckZaikoware(ByVal detailList As List(Of PEKonInputDetail), _
                                       ByVal isSum As Boolean, _
                                       ByVal isRegist As Boolean, _
                                       ByVal denpyoType As Tools.PEKonInput.DenpyoType) As List(Of BEZaikosuNoSyokai)
    '
    'If _IsOnlyKon OrElse detailList Is Nothing OrElse detailList.Count = 0 Then
    '  '商魂のみ導入時、明細が未入力時は、チェック不要
    '  Return Nothing
    'End If

    If NeedCheckingZaikowareOnDenpyo(denpyoType) = False Then
      '会社基本情報上で在庫割れチェック対象外
      Return Nothing
    End If

    ' チェック対象を集計して抜き出す
    Dim zaikoCheckList As List(Of ZaikoCheckEntity) = Me.MakeZaikowareList(detailList)
    If zaikoCheckList Is Nothing OrElse zaikoCheckList.Count = 0 Then
      Return Nothing
    End If

    ' 伝票訂正の場合は旧伝票から差分を求める（見積は対象外。振替はヘッダー倉庫が修正前の出荷倉庫と同じなら）
    If Me.Mode = InputDefines.ModeType.ModifyMode AndAlso isSum AndAlso denpyoType <> PEKonInput.DenpyoType.Mitsumori Then

      For Each zaikoCheck1 As ZaikoCheckEntity In zaikoCheckList
        Dim zaikoCheck As ZaikoCheckEntity = zaikoCheck1    '警告回避のため

        ' 旧伝票に存在するか確認
        Dim oldDetailList As List(Of PEKonInputDetail) = Nothing

        oldDetailList = Me.SelectedEntity.DetailList.FindAll(Function(target) _
           TscStringTool.Equals(target.SyohinCode, zaikoCheck.SyohinCode, m_JapaneseCollation) _
           AndAlso TscStringTool.Equals(target.SokoCode, zaikoCheck.SokoCode, m_JapaneseCollation) _
           AndAlso target.MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan _
           AndAlso target.Ku <> BusinessEntity.Defines.KuType.Tankateisei)

        If oldDetailList IsNot Nothing AndAlso oldDetailList.Count > 0 Then
          ' 修正前の数量を戻す
          For Each oldDetail As PEKonInputDetail In oldDetailList
            zaikoCheck.Zaikosu += oldDetail.Suryo
          Next
        End If
      Next

    End If

    ' 在庫割れをチェック（集計するかどうかで分ける）
    Dim result As New List(Of BEZaikosuNoSyokai)()
    If isSum Then
      ' 合算してチェック
      For i As Integer = 0 To zaikoCheckList.Count - 1
        Dim cnt As Integer = i      '警告回避

        ' 在庫数割れを先にチェックする
        Dim zaikowareLine As Decimal = 0
        If zaikowareLine > zaikoCheckList(i).Zaikosu - zaikoCheckList(i).Suryo Then
          ' 有効在庫数より下回ったらfalse（フラグ：注文点=0）
          result.Add(New BEZaikosuNoSyokai With { _
            .SyohinCode = zaikoCheckList(i).SyohinCode, _
            .SokoCode = zaikoCheckList(i).SokoCode, _
            .Chumonten = 0 _
           })
        Else
          ' 警告ライン有り＆登録時なら警告ラインでチェックする
          If _BEKihonJoho.ZaikowareLine <> 0 AndAlso isRegist Then
            ' 在庫割れチェック用エンティティをリストから取得
            Dim zms As ZaikoCheckEntity = zaikoCheckList.Find(Function(target) TscStringTool.Equals(target.SyohinCode, zaikoCheckList(cnt).SyohinCode, m_JapaneseCollation) _
               AndAlso TscStringTool.Equals(target.SokoCode, zaikoCheckList(cnt).SokoCode, m_JapaneseCollation))

            If zms IsNot Nothing AndAlso zms.Chumonten <> 0 Then
              ' 注文点×在庫割れライン。※ZaikowareLineは整数化して保存してある
              zaikowareLine = CDec(zms.Chumonten) * CDec(_BEKihonJoho.ZaikowareLine) / 1000D

              ' 警告ライン以下だったらfalse（フラグ：注文点=1）
              If zaikowareLine >= zaikoCheckList(i).Zaikosu - zaikoCheckList(i).Suryo Then
                result.Add(New BEZaikosuNoSyokai With { _
                  .SyohinCode = zaikoCheckList(i).SyohinCode, _
                  .SokoCode = zaikoCheckList(i).SokoCode, _
                  .Chumonten = 1 _
                 })
              End If
            End If
          End If
        End If
      Next
    Else
      ' 明細毎にチェック
      For i As Integer = 0 To detailList.Count - 1
        Dim cnt As Integer = i      '警告回避

        Dim sokoCode As String = detailList(i).SokoCode
        ' ターゲットリストから取得
        Dim zaikoCheck As ZaikoCheckEntity =
         zaikoCheckList.Find(Function(match) TscStringTool.Equals(match.SyohinCode, detailList(cnt).SyohinCode, m_JapaneseCollation) _
                                             AndAlso TscStringTool.Equals(match.SokoCode, sokoCode, m_JapaneseCollation))

        If zaikoCheck IsNot Nothing Then
          ' 在庫数割れを先にチェックする
          Dim zaikowareLine As Decimal = 0
          If zaikowareLine > zaikoCheck.Zaikosu - detailList(i).Suryo Then
            ' 有効在庫数より下回ったらfalse（フラグ：注文点=0）
            result.Add(New BEZaikosuNoSyokai With { _
               .SyohinCode = zaikoCheckList(i).SyohinCode, _
               .SokoCode = sokoCode, _
               .Chumonten = 0 _
              })

          Else
            ' 警告ライン有り＆登録時なら警告ラインでチェックする
            If _BEKihonJoho.ZaikowareLine <> 0 AndAlso isRegist Then
              ' 在庫割れチェック用エンティティをリストから取得
              Dim zms As ZaikoCheckEntity = zaikoCheckList.Find(Function(match) TscStringTool.Equals(match.SyohinCode, detailList(cnt).SyohinCode, m_JapaneseCollation) _
                                                                                AndAlso TscStringTool.Equals(match.SokoCode, sokoCode, m_JapaneseCollation))

              If zms IsNot Nothing AndAlso zms.Chumonten <> 0 Then
                ' 注文点×在庫割れライン。※ZaikowareLineは整数化して保存してある
                zaikowareLine = CDec(zms.Chumonten) * CDec(_BEKihonJoho.ZaikowareLine) / 1000D

                ' 警告ライン以下だったらfalse（フラグ：注文点=1）
                If zaikowareLine >= zaikoCheck.Zaikosu - detailList(i).Suryo Then
                  result.Add(New BEZaikosuNoSyokai With { _
                    .SyohinCode = zaikoCheckList(i).SyohinCode, _
                    .SokoCode = sokoCode, _
                    .Chumonten = 1 _
                   })
                End If
              End If
            End If
          End If
        End If
      Next
    End If

    If result Is Nothing OrElse result.Count = 0 Then
      Return Nothing
    Else
      result.Sort(Function(x, y) Me.CompareZaikoItems(x, y))

      Return result
    End If
  End Function

  ''' <summary>会社基本情報により、伝票単位で在庫割れチェックの要否を判定。
  ''' </summary>
  ''' <param name="denpyoType">伝票種類</param>
  Private Function NeedCheckingZaikowareOnDenpyo(ByVal denpyoType As PEKonInput.DenpyoType) As Boolean
    '
    Dim zaikowareCheck As BusinessEntity.Defines.ZaikowareCheckType = JudgeZaikowareCheck(denpyoType)

    If zaikowareCheck <> BusinessEntity.Defines.ZaikowareCheckType.NoCheck Then
      Return True
    End If
    Return False

  End Function

  ''' <summary>
  ''' 一括在庫割れチェックを行う明細を絞り込み、在庫割れチェックエンティティクラスのリストを作成します。
  ''' </summary>
  ''' <param name="detailList">明細のリスト</param>
  ''' <returns>在庫割れチェックエンティティクラスのリスト</returns>
  ''' <remarks></remarks>
  Private Function MakeZaikowareList(ByVal detailList As List(Of PEKonInputDetail)) As List(Of ZaikoCheckEntity)
    '
    '在庫割れチェック対象明細を抜き出す
    '商品コード入力済みで、かつ一般商品で、かつ数量ゼロ以外で、単価訂正以外
    'Dim tempList As List(Of PEKonInputDetail) =
    '        detailList.FindAll(Function(match) _
    '                           String.IsNullOrEmpty(match.SyohinCode) = False _
    '                           AndAlso match.MasterKubun = Defines.MasterKubunType.Ippan _
    '                           AndAlso match.Suryo <> 0 _
    '                           AndAlso match.Ku <> Defines.KuType.Tankateisei)
    Dim tempList As List(Of PEKonInputDetail) =
        detailList.FindAll(Function(match) NeedCheckingZaikowareOnMeisai(match) = True)
    ' 対象明細が無いとき
    If tempList Is Nothing OrElse tempList.Count = 0 Then
      Return Nothing
    End If

    ' チェック対象明細リストを作成する
    Dim targetDetailList As New List(Of PEKonInputDetail)

    For Each detail As PEKonInputDetail In tempList

      Dim beMasterSms As BEMasterSms = MyBase.FindBEMasterSms(detail.SyohinCode)
      If beMasterSms.SyohinCode.Length <> 0 _
      AndAlso beMasterSms.SystemKubun <> BusinessEntity.Defines.MasterSmsSystemKubunType.Kon _
      AndAlso beMasterSms.MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan _
      AndAlso beMasterSms.ZaikoKanri = BusinessEntity.Defines.KanriType.Suru _
      AndAlso detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan _
      AndAlso detail.Ku <> BusinessEntity.Defines.KuType.Tankateisei Then
        ' チェック対象明細
        If detail IsNot Nothing Then
          targetDetailList.Add(detail)
        End If
      End If
    Next

    ' チェック対象明細が無ければ終了
    If targetDetailList.Count = 0 Then
      Return Nothing
    End If

    ' 在庫数の照会の集計条件を作成
    Dim zmsnConditionList As New List(Of ZaikosuNoSyokaiCondition)()

    For Each detail As PEKonInputDetail In targetDetailList
      Dim detail1 As PEKonInputDetail = detail    '警告回避
      If IsNothing(zmsnConditionList.Find(Function(match) match.SyohinCode = detail1.SyohinCode AndAlso match.SokoCode = detail1.SokoCode)) Then
        zmsnConditionList.Add(New ZaikosuNoSyokaiCondition With {.SyohinCode = detail.SyohinCode, _
        .SokoCode = detail.SokoCode, _
        .SelectedSyohinKubun = 0, _
        .SelectedSyohinKubunUsing = BusinessEntity.Defines.UsingType.UsingOneItem})
      End If
    Next

    ' 在庫情報を取得
    Dim zmsnList As New List(Of BEZaikosuNoSyokai)
    For Each condition In zmsnConditionList
      Dim zmsn As BEZaikosuNoSyokai = CalculateZaikosuNoSyokai(condition.SyohinCode, condition.SokoCode)
      zmsnList.Add(zmsn)
    Next

    '在庫割れチェックエンティティクラスの作成
    Dim zaikoCheckList As New List(Of ZaikoCheckEntity)()
    For Each zmsn As BEZaikosuNoSyokai In zmsnList
      zaikoCheckList.Add(New ZaikoCheckEntity With {.SyohinCode = zmsn.SyohinCode, .SokoCode = zmsn.SokoCode, .Zaikosu = zmsn.Gokei, .Chumonten = zmsn.Chumonten})
    Next

    Dim beInputJUC As BEInputJUC = Nothing  '受注伝票

    '新規入力、かつ受注伝票と連動している場合は取得
    Dim targetDetail As PEKonInputDetail = targetDetailList.Find(Function(target) target.RendoHeaderId > 0)
    Dim juchuHeaderId = 0
    If targetDetail IsNot Nothing Then
      juchuHeaderId = targetDetail.RendoHeaderId
    End If
    If Me.Mode = InputDefines.ModeType.NewInputMode AndAlso (juchuHeaderId > 0) Then
      'beInputJUC = FindInputJUC(juchuHeaderId, False)
      beInputJUC = MyBase.FindBEInputJUC(juchuHeaderId)
    End If

    ' (売上)数量を設定
    For Each detail As PEKonInputDetail In targetDetailList
      Dim detail1 As PEKonInputDetail = detail    '警告回避
      Dim listIndex As Integer =
       targetDetailList.FindIndex(Function(match) TscStringTool.Equals(match.SyohinCode, detail1.SyohinCode, m_JapaneseCollation) _
        AndAlso TscStringTool.Equals(match.SokoCode, detail1.SokoCode, m_JapaneseCollation))

      zaikoCheckList(listIndex).Suryo += detail1.Suryo

      If beInputJUC IsNot Nothing AndAlso detail1.RendoHeaderId > 0 AndAlso detail1.RendoSequence > 0 Then
        ' 新規入力、かつ受注伝票と連動している場合は在庫数から受注伝票分の数量を戻す
        ' (beInputJUC が Nothing ではないときは新規入力、かつ受注伝票と連動している)
        For Each jucDetail As BEInputJUCD In beInputJUC.InputJUCDList.BEInputJUCD
          If jucDetail.Sequence = detail1.RendoSequence Then
            zaikoCheckList(listIndex).Zaikosu += jucDetail.Suryo
            Exit For
          End If
        Next

      End If
    Next

    Return zaikoCheckList
  End Function

  ''' <summary>商品マスター参照前に、明細単位で在庫割れチェックの要否を判定。
  ''' </summary>
  ''' <param name="detail">伝票入力エンティティ明細</param>
  Private Function NeedCheckingZaikowareOnMeisai(ByVal detail As PEKonInputDetail) As Boolean
    '
    '商品コード設定済で、かつ一般商品で、かつ数量が入力済で、かつ単価訂正以外は在庫割れチェック対象
    Return (String.IsNullOrEmpty(detail.SyohinCode) = False _
            AndAlso detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan _
            AndAlso detail.Suryo <> 0 _
            AndAlso detail.Ku <> BusinessEntity.Defines.KuType.Tankateisei)

  End Function

#End Region
End Class


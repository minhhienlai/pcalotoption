﻿Imports PCA.Controls
Public Class SLPcaLabel
  Inherits PcaLabel
  Property MustInput As Boolean
End Class

Public Class SLPcaComboBox
  Inherits PcaComboBox
  Property MustInput As Boolean
End Class

Public Class SLPcaTextBox
  Inherits PcaTextBox
  Property MustInput As Boolean
End Class

Public Class SLPcaNumberBox
  Inherits PcaNumberBox
  Property MustInput As Boolean
End Class

Public Class SLPcaMoneyBox
  Inherits PcaMoneyBox
  Property MustInput As Boolean
End Class

Public Class SLPcaDate
  Inherits PcaDate
  Property MustInput As Boolean
End Class

Public Class SLPcaLabeledTextBox
  Inherits PcaLabeledTextBox
  Property MustInput As Boolean
End Class

Public Class SLPcaLabeledNumberBox
  Inherits PcaLabeledNumberBox
  Property MustInput As Boolean
End Class

Public Class SLPcaLabeledMoneyBox
  Inherits PcaLabeledMoneyBox
  Property MustInput As Boolean
End Class

Public Class SLPcaLabeledComboBox
  Inherits PcaLabeledComboBox
  Property MustInput As Boolean
End Class

Public Class SLPcaLabeledDate
  Inherits PcaLabeledDate
  Property MustInput As Boolean
End Class

﻿
Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports Sunloft.PCAIF


Imports System.ComponentModel
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports PCA.TSC.Kon
Imports PCA
Imports System.Globalization
Imports System.Text


Public Class frmShipLotList

  Private Enum ColumnIndex
    ShipType = 0
    ShipDate
    SlipNo
    ShipDestCode
    ShipName
    ProductCode
    ProductName
    LotNo
    Qty
    UnitName
    UnitPrice
    Note
    Shoumi
    ID
    DeliveryDate
    UnitPrice_Original
    UnitPrice_Integrated
  End Enum

  Private m_conf As Sunloft.PcaConfig
  Private DisplayData As ColumnResult() = {New ColumnResult}

  Private connector As IIntegratedApplication = Nothing
  Private AppClass As ConnectToPCA = New ConnectToPCA()
  Private MasterDialog As SLMasterSearchDialog
  Private m_AMS1Class As AMS1
  Private m_SL_LMBClass As SL_LMB

  Private m_CurItem As Integer
  Private m_isSearch As Boolean = False

  Private arrLotDetails(10) As String
  Private m_SlipNoColumn As Integer
  Private m_isKeyDowned As Boolean = False
  Private m_curString As String = String.Empty

  Private m_strFromDate As String = String.Empty
  Private m_strToDate As String = String.Empty
  Private m_strFromShipCode As String = String.Empty
  Private m_strToShipCode As String = String.Empty
  Private m_intEnter As Integer = 0 'Resolve problem after select ShipDesCode seat then DoSearch and can't select differant seat

  Private Const RPT_FILENAME As String = "ShipLotList02020.rpt" ' Form to Preview/Print
  Private Const RPT_TITLE As String = "出荷ロット一覧表"

  Private Const WIDTH_SHIP_DEVISION As Integer = 90
  Private Const WIDTH_DATE As Integer = 100
  Private Const WIDTH_SLIP_NO As Integer = 80
  Private Const WIDTH_DEST_CODE As Integer = 70
  Private Const WIDTH_SHIP_NAME As Integer = 350
  Private Const WIDTH_PRODUCT_CODE As Integer = 50
  Private Const WIDTH_PRODUCT_NAME As Integer = 200
  Private Const WIDTH_LOT_NO As Integer = 100
  Private Const WIDTH_QUANTITY As Integer = 150
  Private Const WIDTH_UNIT As Integer = 50
  Private Const WIDTH_UNIT_PRICE As Integer = 150
  Private Const WIDTH_NOTE As Integer = 100
  Private Const WIDTH_SHOUMI As Integer = 100
  Private Const WIDTH_LOT_DETAILS As Integer = 150
  Private Const WIDTH_ID As Integer = 100
  Private Const WIDTH_DELIVERY_DATE As Integer = 100
  Private Const WIDTH_ORIGINAL_UNITPRICE As Integer = 100
  Private Const WIDTH_INTEGRATED_UNITPRICE As Integer = 100

  Private Const HEIGHT_HEADER As Integer = 25

  Private Const NUMBER_TYPE As String = "Number"
  Private Const TEXT_TYPE As String = "String"
  Private Const DATE_TYPE As String = "Date"
  Private Const CHECKBOX_TYPE As String = "CHECKBOX_TYPE"

  Private Const FROM_CODE_TEXT As Integer = 0
  Private Const TO_CODE_TEXT As Integer = 1
  Private Const TOTAL_COLUMN As Integer = 17 'Total column standing in front of column 備考
  Private Const CSV_FILE_NAME As String = "出荷ロット一覧表"
  Private Const PROGRAM_TO_CALL As String = "\Pro02010.exe" 'Click table cell -> Display slip

  Public Sub New()

    InitializeComponent()

    AppClass.ConnectToPCA()
    connector = AppClass.connector

    m_conf = New Sunloft.PcaConfig

    m_AMS1Class = New AMS1(connector)
    m_AMS1Class.ReadAll()

    m_SL_LMBClass = New SL_LMB(connector)
    m_SL_LMBClass.ReadOnlyRow()

    arrLotDetails = {m_SL_LMBClass.sl_lmb_label1, m_SL_LMBClass.sl_lmb_label2, m_SL_LMBClass.sl_lmb_label3, m_SL_LMBClass.sl_lmb_label4, m_SL_LMBClass.sl_lmb_label5, _
                     m_SL_LMBClass.sl_lmb_label6, m_SL_LMBClass.sl_lmb_label7, m_SL_LMBClass.sl_lmb_label8, m_SL_LMBClass.sl_lmb_label9, m_SL_LMBClass.sl_lmb_label10}

    m_SlipNoColumn = m_SL_LMBClass.sl_lmb_maxlotop + TOTAL_COLUMN

    'Init result grid
    InitTable()

    MasterDialog = New SLMasterSearchDialog(connector)

    'Disable refer command (F8)
    PcaCommandItemRefer.Enabled = False
    PcaCommandItemPrint.Enabled = False
    PcaCommandItemExport.Enabled = False
    PcaCommandItemPreview.Enabled = False
  End Sub
#Region "InitTable"
  ''' <summary>
  ''' set InitTable, header columns name
  ''' </summary>
  ''' <param name="isEmpty">Clear search result array</param>
  Private Sub InitTable(Optional ByVal isEmpty As Boolean = False)
    Try
      dgv.Columns.Clear()

      InitTableColumn(TEXT_TYPE, ColumnName.ShipType, WIDTH_SHIP_DEVISION, DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      InitTableColumn(TEXT_TYPE, ColumnName.ShipDate, WIDTH_DATE, DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      InitTableColumn(TEXT_TYPE, ColumnName.SlipNo, WIDTH_SLIP_NO, DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      InitTableColumn(TEXT_TYPE, ColumnName.ShipDestCode, WIDTH_DEST_CODE, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.ShipName, WIDTH_SHIP_NAME, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.ProductCode, WIDTH_PRODUCT_CODE, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.ProductName, WIDTH_PRODUCT_NAME, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.LotNo, WIDTH_LOT_NO, DataGridViewContentAlignment.MiddleLeft, , True, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.Qty, WIDTH_QUANTITY, DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      InitTableColumn(TEXT_TYPE, ColumnName.UnitName, WIDTH_UNIT, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.UnitPrice, WIDTH_UNIT_PRICE, DataGridViewContentAlignment.MiddleRight, , True, DataGridViewContentAlignment.MiddleRight)
      InitTableColumn(TEXT_TYPE, ColumnName.Note, WIDTH_NOTE, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.Shoumi, WIDTH_SHOUMI, DataGridViewContentAlignment.MiddleCenter, , True, DataGridViewContentAlignment.MiddleCenter)
      InitTableColumn(TEXT_TYPE, ColumnName.ID, WIDTH_ID, DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter, False)
      InitTableColumn(TEXT_TYPE, ColumnName.DlvDate, WIDTH_DELIVERY_DATE, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft, False)
      InitTableColumn(TEXT_TYPE, ColumnName.UnitPriceOrigin, WIDTH_ORIGINAL_UNITPRICE, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft, False)
      InitTableColumn(TEXT_TYPE, ColumnName.UnitPriceIntegrated, WIDTH_INTEGRATED_UNITPRICE, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft, False)
      For intCount As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        InitTableColumn(TEXT_TYPE, arrLotDetails(intCount), WIDTH_LOT_DETAILS, DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      Next

      'Set header cell font color to white
      For intCount As Integer = 0 To m_SlipNoColumn - 1
        dgv.Columns(intCount).HeaderCell.Style.ForeColor = Color.White
      Next
      'Set header size (Default size is a bit small)
      dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
      dgv.ColumnHeadersHeight = HEIGHT_HEADER
      dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing

      If isEmpty Then
        DisplayData = Nothing 'Clear search result array
        dgv.Rows.Clear()
      End If
      'Set comma for quantity and unit price column

      'Set table not sortable
      For Each column As DataGridViewColumn In dgv.Columns
        column.SortMode = DataGridViewColumnSortMode.NotSortable
      Next

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub
  ''' <summary>
  ''' set alignment header
  ''' </summary>
  ''' <param name="strHeaderType">Header Type</param>
  ''' <param name="strheaderText">Header Text</param>
  ''' <param name="intcolumnwidth">Width Column</param>
  ''' <param name="alignment">Alignment Header Columns</param>
  ''' <param name="strcolumnname">Column Name</param>
  ''' <param name="isReadonly">isReadonly</param>
  ''' <param name="headerAlignment">Header Alignment</param>
  ''' <param name="isvisible">Visible</param>
  ''' <remarks></remarks>
  Private Sub InitTableColumn(ByVal strHeaderType As String, ByVal strHeaderText As String, ByVal intColumnwidth As Integer, _
                              Optional ByVal alignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft, _
                              Optional ByVal strColumnname As String = "", Optional ByVal isReadonly As Boolean = True, _
                              Optional ByVal headerAlignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft, _
                              Optional ByVal isVisible As Boolean = True)
    Dim col As New DataGridViewColumn
    Select Case strHeaderType
      Case TEXT_TYPE
        col = New DataGridViewTextBoxColumn
      Case CHECKBOX_TYPE
        col = New DataGridViewCheckBoxColumn
      Case Else
        col = New DataGridViewTextBoxColumn
    End Select
    col.ReadOnly = isReadonly
    If strColumnname = String.Empty Then
      col.Name = strHeaderText
    Else
      col.Name = strColumnname
    End If
    col.Width = intColumnwidth
    col.DefaultCellStyle.Alignment = alignment
    col.HeaderCell.Style.Alignment = headerAlignment
    col.Visible = isVisible
    col.DefaultCellStyle.FormatProvider = CultureInfo.CreateSpecificCulture("ja-JP")
    dgv.Columns.Add(col)
  End Sub
#End Region


#Region " PCA command Manager"
  '***********************************************************																														
  'Name          : PcaCommandManager1_Command
  'Content       : Event called when PCACommandManager is called (PCA Function Bar button is pressed/Select from Menu, press F～ key)
  'Return Value  : 
  'Argument      : sender As Object, e As PCA.Controls.CommandItemEventArgs
  'Created date  : 2016/03/07  by Cuong V. Le
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub PcaCommandManager1_Command(ByVal sender As System.Object, ByVal e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try
      Dim commandItem As PCA.Controls.PcaCommandItem = e.CommandItem

      Select Case True
        'Case commandItem Is PcaCommandItemHelp 'F1
        'do nothing
        Case commandItem Is PcaCommandItemSearch 'F5
          Search()
        Case commandItem Is PcaCommandItemPrint 'F9
          'print data
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.ProductCode, 0)) Then
            If MsgBox(RPT_TITLE & "を" & SLConstants.ConfirmMessage.PRINT_CONFIRM_MESSAGE, MsgBoxStyle.OkCancel, "確認") = MsgBoxResult.Ok Then PrintData()
          End If
        Case commandItem Is PcaCommandItemPreview 'Shift + F9
          'preview
          PrintData(True)
        Case commandItem Is PcaCommandItemExport 'F10
          'do export
          If OutPutCSV() Then SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, "完了", ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemExport.CommandId)

        Case commandItem Is PcaCommandItemClose 'F12
          Me.Close()

        Case commandItem Is PcaCommandItemRefer
          If m_isKeyDowned Then m_isKeyDowned = False : Exit Sub
          DisplayReferScreen(Nothing, m_CurItem)
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub frmTransLotList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
    If e.KeyCode = Keys.F8 Then m_isKeyDowned = True
  End Sub
#End Region

#Region " Print/Preview"

  Private Function PrintData(Optional ByVal isPreview As Boolean = False) As Boolean
    Try
      Dim ReportName As String = RPT_FILENAME
      Dim ReportTitle As String = RPT_TITLE
      Dim rpt As New Sunloft.Windows.Forms.SLCrystalReport
      Dim RpParam As New Dictionary(Of String, Object)
      Dim editedDataTable As DataTable = Nothing


      rpt.ReportFile = IO.Path.GetDirectoryName(Application.ExecutablePath) & "\" & ReportName
      EditPrintData(editedDataTable)
      rpt.DataSource = editedDataTable
      ' 出力開始
      If isPreview Then
        rpt.Preview(ReportTitle, True)
      Else
        rpt.PrintToPrinter()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
    Return True
  End Function

#End Region

#Region " EditPrintData"

  Private Function EditPrintData(ByRef argEditedData As DataTable) As Boolean
    Dim editData As New DataSet1.DataTable1DataTable
    Dim editRow As DataRow
    Dim intLpc As Integer = 0

    Try
      While intLpc < dgv.Rows.Count AndAlso Not dgv(ColumnName.ProductCode, intLpc) Is Nothing
        With dgv
          editRow = editData.NewRow
          If m_strFromDate <> String.Empty OrElse m_strToDate <> String.Empty Then editRow.Item("Condition") = "出荷日: " & m_strFromDate & " ～ " & m_strToDate & "      "

          If m_strFromShipCode.Trim.Length > 0 OrElse m_strToShipCode.Trim.Length > 0 Then
            editRow.Item("Condition") = editRow.Item("Condition").ToString & "出荷先: " & m_strFromShipCode & " ～ " & m_strToShipCode
          End If

          If dgv(ColumnName.ShipType, intLpc).Value.ToString = "通常出荷" Then
            editRow.Item("ShipDevision") = "出荷"
          Else
            editRow.Item("ShipDevision") = dgv(ColumnName.ShipType, intLpc).Value.ToString
          End If

          editRow.Item("ShipDate") = dgv(ColumnName.ShipDate, intLpc).Value.ToString
          'editRow.Item("SlipNo") = dgv(ColumnName.slipNo, intLpc).Value.ToString
          editRow.Item("Delivery_date") = dgv(ColumnName.DlvDate, intLpc).Value.ToString
          editRow.Item("ShipDestCode") = dgv(ColumnName.ShipDestCode, intLpc).Value.ToString
          editRow.Item("ShipName") = dgv(ColumnName.ShipName, intLpc).Value.ToString
          editRow.Item("ProductCode") = dgv(ColumnName.ProductCode, intLpc).Value.ToString
          editRow.Item("ProductName") = dgv(ColumnName.ProductName, intLpc).Value.ToString
          editRow.Item("Quantity") = dgv(ColumnName.Qty, intLpc).Value.ToString
          editRow.Item("Unit") = dgv(ColumnName.UnitName, intLpc).Value.ToString
          editRow.Item("Original_unitprice") = dgv(ColumnName.UnitPriceOrigin, intLpc).Value.ToString
          editRow.Item("UnitPrice") = dgv(ColumnName.UnitPrice, intLpc).Value.ToString
          editRow.Item("Integrated_unitprice") = dgv(ColumnName.UnitPriceIntegrated, intLpc).Value.ToString
          'editRow.Item("Shoumi") = dgv(ColumnName.shoumi, intLpc).Value.ToString
          'editRow.Item("Note") = dgv(ColumnName.note, intLpc).Value.ToString
          editRow.Item("LotNo") = dgv(ColumnName.LotNo, intLpc).Value.ToString
          editData.Rows.Add(editRow)
        End With
        intLpc = intLpc + 1
      End While
      argEditedData = editData
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try


  End Function

#End Region

#Region " OutPutCSV"
  Private Function OutPutCSV() As Boolean
    Dim strSaveFileName As String = String.Empty
    Dim strColumns() As String
    Dim sbCsv As New StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .FileName = CSV_FILE_NAME
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        .OverwritePrompt = True

        Dim res As System.Windows.Forms.DialogResult = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False

        strSaveFileName = .FileName

      End With

      strColumns = {ColumnName.ShipType, ColumnName.ShipDate, ColumnName.SlipNo, ColumnName.ShipDestCode, ColumnName.ShipName, ColumnName.ProductCode, _
                    ColumnName.ProductName, ColumnName.LotNo, ColumnName.Qty, ColumnName.UnitPrice, ColumnName.UnitName, ColumnName.Shoumi, ColumnName.Note}

      SLCmnFunction.OutPutCSVFromDataGridView(strColumns, dgv, strSaveFileName, dgv.RowCount)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function
#End Region

#Region " Control Events"
  Private Sub rsetShipCode_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rsetShipCode.FromCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_curString = rsetShipCode.FromCodeText
    m_CurItem = FROM_CODE_TEXT
  End Sub

  Private Sub rsetShipCode_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rsetShipCode.Leave
    PcaCommandItemRefer.Enabled = False
    If m_intEnter = 1 Then
      rsetShipCode.FocusTo()
      m_intEnter = 0
    End If
  End Sub

  Private Sub rsetShipCode_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rsetShipCode.ToCodeTextEnter
    m_isSearch = False
    PcaCommandItemRefer.Enabled = True
    m_curString = rsetShipCode.ToCodeText
    m_CurItem = TO_CODE_TEXT
  End Sub

  Private Sub rsetShipCode_ToCodeTextValidated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rsetShipCode.ToCodeTextValidated
    If m_isSearch Then
      m_isSearch = False
      Search()
    End If
  End Sub

  Private Sub rsetShipCode_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles rsetShipCode.FromCodeTextValidating2
    ValidateShippingDesCode(e, rsetShipCode.FromCodeText)
  End Sub

  Private Sub rsetShipCode_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles rsetShipCode.ToCodeTextValidating2
    ValidateShippingDesCode(e, rsetShipCode.ToCodeText)
  End Sub

  Private Sub rsetShipCode_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles rsetShipCode.ClickFromReferButton2
    DisplayReferScreen(e, FROM_CODE_TEXT)
  End Sub

  Private Sub rsetShipCode_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles rsetShipCode.ClickToReferButton2
    DisplayReferScreen(e, TO_CODE_TEXT)
  End Sub

#End Region

#Region " Search"

  Private Sub Search()
    dgv.Focus()
    'Get Search Result
    GetSearchResult()
    'Display Search Result into grid
    DisplaySearchResult()
  End Sub

#End Region

#Region " Private Method"
  '***********************************************************																														
  'Name          : validateShippingDesCode
  'Content       : Event called when rsetShipCode (From, To Leave event)
  'Return Value  : 
  'Argument      : e As PCA.Controls.ValidatingEventArgs, CodeText As String
  'Created date  : 2016/03/09  by Cuong V. Le
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub ValidateShippingDesCode(ByVal e As PCA.Controls.ValidatingEventArgs, ByRef CodeText As String)
    Try
      If CodeText.TrimEnd().Length > m_AMS1Class.ams1_CustomerLength Then
        e.Cancel = True
        CodeText = m_curString
        Return
      End If
      Dim shippingDesCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_ShippingDesLength)
      If Not String.IsNullOrEmpty(shippingDesCode) AndAlso m_curString = shippingDesCode Then
        CodeText = shippingDesCode
        'No changes
        Return
      End If

      If String.IsNullOrEmpty(shippingDesCode) Then Return

      'validate customer code
      Dim beMasterTms As PCA.TSC.Kon.BusinessEntity.BEMasterTms = MasterDialog.FindBEMasterTms(shippingDesCode)

      If Not beMasterTms.TokuisakiCode.Length > 0 Then
        'If there's error
        e.Cancel = True
        CodeText = m_curString
        Return
      Else
        CodeText = shippingDesCode
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#Region "  GetSearchResult"
  '***********************************************************																														
  'Name          : GetSearchResult
  'Content       : Event called when the user clicks search button
  '                make a query into database to get the result
  'Return Value  : 
  'Argument      : 
  'Created date  : 2016/03/10  by Cuong V. Le
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub GetSearchResult()
    Try

      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim intFromDate As Integer = rdtShipDate.FromIntDate
      Dim intToDate As Integer = rdtShipDate.ToIntDate
      Dim intFromShipCode As String = rsetShipCode.FromCodeText
      Dim intToShipCode As String = rsetShipCode.ToCodeText
      Dim intCount As Integer = -1

      rsetShipCode.FocusFrom()
      rsetShipCode.FocusTo()

      Array.Clear(DisplayData, 0, DisplayData.Length)

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SYKH_Pro02020")
      selectCommand.Parameters("@fromShipDate").SetValue(intFromDate)
      selectCommand.Parameters("@toShipDate").SetValue(intToDate)
      selectCommand.Parameters("@fromShipDesCode").SetValue(intFromShipCode)
      selectCommand.Parameters("@toShipDesCode").SetValue(intToShipCode)
      reader = selectCommand.ExecuteReader

      While reader.Read()
        intCount = intCount + 1
        ReDim Preserve DisplayData(intCount)
        DisplayData(intCount) = New ColumnResult
        If CDbl(reader.GetValue("sl_sykh_return").ToString) = SLConstants.ShipmentKbn.NORMAL_SHIPPING_CODE Then
          DisplayData(intCount).ShipType = SLConstants.ShipmentKbn.NORMAL_SHIPPING
        ElseIf CDbl(reader.GetValue("sl_sykh_return").ToString) = SLConstants.ShipmentKbn.RETURN_GOODS_CODE Then
          DisplayData(intCount).ShipType = SLConstants.ShipmentKbn.RETURN_GOODS
        End If

        DisplayData(intCount).ShipDate = Date.ParseExact(reader.GetValue("sl_sykh_uribi").ToString(), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")

        'Slip No
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_sykh_denno")) Then DisplayData(intCount).SlipNo = CInt(reader.GetValue("sl_sykh_denno"))

        DisplayData(intCount).ShipDestCode = reader.GetValue("sl_sykh_tcd").ToString()
        DisplayData(intCount).ShipName = reader.GetValue("cms_mei1").ToString().TrimEnd & " " & reader.GetValue("cms_mei2").ToString().TrimEnd
        DisplayData(intCount).ProductCode = reader.GetValue("sl_sykd_scd").ToString()
        DisplayData(intCount).ProductName = reader.GetValue("sl_sykd_mei").ToString().TrimEnd
        DisplayData(intCount).LotNo = reader.GetValue("sl_zdn_ulotno").ToString()
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_sykd_suryo")) Then DisplayData(intCount).Qty = CDec(reader.GetValue("sl_sykd_suryo"))
        DisplayData(intCount).UnitName = reader.GetValue("sl_sykd_tani").ToString()
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_sykd_tanka")) Then DisplayData(intCount).UnitPrice = CDec(reader.GetValue("sl_sykd_tanka"))
        DisplayData(intCount).Note = reader.GetValue("sl_sykd_biko").ToString()
        If reader.GetValue("sl_zdn_bbdate").ToString() <> "0" Then
          DisplayData(intCount).Shoumi = Date.ParseExact(reader.GetValue("sl_zdn_bbdate").ToString(), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        Else
          DisplayData(intCount).Shoumi = String.Empty
        End If
        DisplayData(intCount).ID = CInt(reader.GetValue("sl_sykh_id").ToString())
        For intCountLotDetails As Integer = 1 To m_SL_LMBClass.sl_lmb_maxlotop
          DisplayData(intCount).Lotdetails(intCountLotDetails - 1) = reader.GetValue("sl_zdn_ldmei" & intCountLotDetails.ToString).ToString()
        Next

        DisplayData(intCount).QtyNoOfDecDigit = CInt(reader.GetValue("sms_sketa"))
        DisplayData(intCount).UnitPriceNoOfDecDigit = CInt(reader.GetValue("sms_tketa"))

        DisplayData(intCount).DeliveryDate = Date.ParseExact(reader.GetValue("sl_sykh_nouki").ToString(), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        DisplayData(intCount).OriginalUnitprice = CDec(reader.GetValue("sl_sykd_gentan"))
        DisplayData(intCount).AmountOfMoney = CDec(DisplayData(intCount).Qty * DisplayData(intCount).UnitPrice)
      End While
      reader.Close()
      reader.Dispose()

      If Not DisplayData Is Nothing AndAlso Not DisplayData(0) Is Nothing Then
        PcaCommandItemPrint.Enabled = True
        PcaCommandItemExport.Enabled = True
        PcaCommandItemPreview.Enabled = True

        m_strFromDate = Date.ParseExact(intFromDate.ToString, "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        m_strToDate = Date.ParseExact(intToDate.ToString, "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        m_strFromShipCode = intFromShipCode.ToString
        m_strToShipCode = intToShipCode.ToString
      Else
        PcaCommandItemPrint.Enabled = False
        PcaCommandItemExport.Enabled = False
        PcaCommandItemPreview.Enabled = False
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, "確認", ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, 5)
      End If
      rdtShipDate.FocusFrom()
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

#End Region

#Region "  DisplaySearchResult"
  '***********************************************************																														
  'Name          : DisplaySearchResult
  'Content       : Display search result into table
  'Return Value  : 
  'Argument      : 
  'Created date  : 2016/03/10  by Cuong V. Le
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub DisplaySearchResult()
    Try
      dgv.Columns.Clear()
      InitTable()

      For intCount As Integer = 0 To DisplayData.Length - 1
        If Not SLCmnFunction.checkObjectNothingEmpty(DisplayData(intCount)) Then
          dgv.Rows.Add(New DataGridViewRow)

          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ShipType).Value = DisplayData(intCount).ShipType
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ShipDate).Value = DisplayData(intCount).ShipDate
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.SlipNo).Value = DisplayData(intCount).SlipNo
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ShipDestCode).Value = DisplayData(intCount).ShipDestCode
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ShipName).Value = DisplayData(intCount).ShipName
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ProductCode).Value = DisplayData(intCount).ProductCode
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ProductName).Value = DisplayData(intCount).ProductName
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.LotNo).Value = DisplayData(intCount).LotNo
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.Qty).Value = SLCmnFunction.formatNumberOfDigit(CDec(DisplayData(intCount).Qty), DisplayData(intCount).QtyNoOfDecDigit)
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitName).Value = DisplayData(intCount).UnitName
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitPrice).Value = SLCmnFunction.formatNumberOfDigit(CDec(DisplayData(intCount).UnitPrice), DisplayData(intCount).UnitPriceNoOfDecDigit)
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.Note).Value = DisplayData(intCount).Note
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.Shoumi).Value = DisplayData(intCount).Shoumi
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ID).Value = DisplayData(intCount).ID
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.DeliveryDate).Value = DisplayData(intCount).DeliveryDate
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitPrice_Original).Value = SLCmnFunction.formatNumberOfDigit(DisplayData(intCount).OriginalUnitprice, DisplayData(intCount).UnitPriceNoOfDecDigit)
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitPrice_Integrated).Value = SLCmnFunction.formatNumberOfDigit(DisplayData(intCount).AmountOfMoney)
          For intCountLotDetails As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
            dgv.Rows.Item(intCount).Cells.Item(TOTAL_COLUMN + intCountLotDetails).Value = DisplayData(intCount).Lotdetails(intCountLotDetails)
          Next
        End If
      Next

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

#End Region

  '***********************************************************																														
  'Name          : DisplayReferScreen
  'Content       : Display refer screen when the user selects from CodeSet field
  'Return Value  : 
  'Argument      : 
  'Created date  : 2016/03/10  by Cuong V. Le
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub DisplayReferScreen(ByVal e As System.ComponentModel.CancelEventArgs, ByRef Item As Integer)
    Dim newCode As String = String.Empty
    Dim location As Point = PCA.TSC.Kon.Tools.ControlTool.GetDialogLocation(rsetShipCode.FromReferButton)
    newCode = MasterDialog.ShowReferTmsDialog(If(Item = FROM_CODE_TEXT, rsetShipCode.FromCodeText, rsetShipCode.ToCodeText), location)
    If String.IsNullOrEmpty(newCode) Then
      If Not e Is Nothing Then e.Cancel = True
    Else
      If m_curString = newCode Then
        'No changes
        Return
      End If
      Dim beMasterTms As PCA.TSC.Kon.BusinessEntity.BEMasterTms = MasterDialog.FindBEMasterTms(newCode)
      If Not beMasterTms.TokuisakiCode.Length > 0 Then
        'Validate failed
        If Not e Is Nothing Then e.Cancel = True
        If Item = FROM_CODE_TEXT Then
          rsetShipCode.FromCodeText = m_curString
        Else
          rsetShipCode.ToCodeText = m_curString
        End If
        Return
      End If
      If Item = FROM_CODE_TEXT Then
        rsetShipCode.FromCodeText = newCode
      Else
        rsetShipCode.ToCodeText = newCode
      End If
    End If
    m_intEnter = 1
  End Sub
#End Region

  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
    '
    Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
    Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子

    If (keyCode = Keys.Tab OrElse keyCode = Keys.Enter) Then
      'Validate if user press Enter/Tab +  Anykey except Shift, →, ↓
      m_isSearch = True
    Else
      m_isSearch = False
    End If

    Return MyBase.ProcessDialogKey(keyData)

  End Function

  Private Sub frmShipLotList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not AppClass.isAttach Then
      connector.LogOffSystem()
    End If
  End Sub

  '***********************************************************																														
  'Name          : dgv_CellDoubleClick
  'Content       : Display lot detail when the user double clicks on a row in the table
  'Return Value  : 
  'Argument      : sender As System.Object, args As System.Windows.Forms.DataGridViewCellEventArgs
  'Created date  : 2016/03/10  by Cuong V. Le
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub dgv_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellDoubleClick
    If e.RowIndex = -1 OrElse dgv.Rows(e.RowIndex).Cells(ColumnIndex.ID).Value Is Nothing Then Return

    SLCmnFunction.StartUpInputExe(IO.Path.GetDirectoryName(Application.ExecutablePath) & PROGRAM_TO_CALL, AppClass.EncryptedLoginString, dgv.Rows(e.RowIndex).Cells(ColumnIndex.ID).Value.ToString)
  End Sub
End Class

'***********************************************************																														
'Name          : columnName
'Content       : This class defines header properties for the table that will be used do display result search
'Return Value  : 
'Created date  : 2016/03/08  by Cuong V. Le
'Modified date :        by       Content: 																											
'***********************************************************
Public Class ColumnName
  Public Const ShipType As String = "区分"
  Public Const ShipDate As String = "出荷日"
  Public Const SlipNo As String = "伝票番号"
  Public Const ShipDestCode As String = "出荷先"
  Public Const ShipName As String = "出荷先名"
  Public Const ProductCode As String = "商品"
  Public Const ProductName As String = "商品名"
  Public Const LotNo As String = "ロットNo"
  Public Const Qty As String = "数量"
  Public Const UnitName As String = "単位"
  Public Const UnitPrice As String = "単価"
  Public Const Note As String = "備考"
  Public Const Shoumi As String = "賞味期限"

  Public Const Lotdetails As String = "ロット詳細情報"
  Public Const Lotdetails1 As String = "ロット詳細情報1"
  Public Const Lotdetails2 As String = "ロット詳細情報2"
  Public Const Lotdetails3 As String = "ロット詳細情報3"
  Public Const Lotdetails4 As String = "ロット詳細情報4"
  Public Const Lotdetails5 As String = "ロット詳細情報5"
  Public Const Lotdetails6 As String = "ロット詳細情報6"
  Public Const Lotdetails7 As String = "ロット詳細情報7"
  Public Const Lotdetails8 As String = "ロット詳細情報8"
  Public Const Lotdetails9 As String = "ロット詳細情報9"
  Public Const Lotdetails10 As String = "ロット詳細情報10"
  Public Const ID As String = "ID"
  Public Const DlvDate As String = "delivery"
  Public Const UnitPriceOrigin As String = "original"
  Public Const UnitPriceIntegrated As String = "integrated"
End Class

Public Class ColumnResult
  Public Property ShipType As String = String.empty
  Public Property ShipDate As String = String.empty
  Public Property SlipNo As Integer
  Public Property ShipDestCode As String = String.empty
  Public Property ShipName As String = String.empty
  Public Property ProductCode As String = String.empty
  Public Property ProductName As String = String.empty
  Public Property LotNo As String = String.Empty
  Public Property Qty As Decimal
  Public Property UnitName As String = String.empty
  Public Property UnitPrice As Decimal
  Public Property Note As String = String.empty
  Public Property Shoumi As String = String.empty
  Public Property LotMoreInfo As String = String.empty
  Public Property QtyNoOfDecDigit As Integer
  Public Property UnitPriceNoOfDecDigit As Integer
  Public Property Lotdetails() As String() = New String(9) {}
  Public Property ID As Integer = 0
  Public Property DeliveryDate As String = String.empty
  Public Property OriginalUnitprice As Decimal
  Public Property AmountOfMoney As Decimal
End Class

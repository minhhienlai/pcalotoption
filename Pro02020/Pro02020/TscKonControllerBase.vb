﻿Imports PCA.TSC.Kon.Tools
Imports PCA.TSC.Kon.BusinessEntity
Imports PCA.TSC.Kon
Imports System.Xml

Public Class TscKonControllerBase

  Public Property CommonVariable As PCA.TSC.Kon.Tools.CommonVariable = New PCA.TSC.Kon.Tools.CommonVariable

  Public Property ConfigFileName As String = String.Empty

  Public Property ConfigMiddleNode As String = String.Empty

  ''' <summary>文字列の照合順序。日本語辞書順の時はTrue、バイナリ順の時はFalseです。
  ''' </summary>
  Protected m_JapaneseCollation As Boolean = True

  ''' <summary>フォーム
  ''' </summary>
  Protected m_Owner As IWin32Window

  ''' <summary>会社基本情報
  ''' </summary>
  Public ReadOnly Property BEKihonJoho() As PCA.TSC.Kon.BusinessEntity.BEKihonJoho
    Get
      Return _BEKihonJoho
    End Get
  End Property
  Protected _BEKihonJoho As New BEKihonJoho

  ''' <summary>取得状況
  ''' </summary>
  Public Class LoadStatus
    ''' <summary>常駐要否
    ''' </summary>
    Public Property NeedLoad As Boolean = False
    ''' <summary>ロード済
    ''' </summary>
    Public Property Loaded As Boolean = False
  End Class

  ''' <summary>コンストラクタ
  ''' </summary>
  Public Sub New(ByVal owner As IWin32Window)
    m_Owner = owner
  End Sub

  ''' <summary>
  ''' </summary>
  ''' <param name="configXMLDoc"></param>
  Public Function PrepareApplicationBasic(ByRef configXMLDoc As XmlDocument) As Boolean

    If PCA.TSC.Kon.Tools.KonAPI.PrepareIApplication(DirectCast(CommonVariable.IFactory, PCA.ApplicationIntegration.IIntegrationFactory), _
                                        True, _
                                        m_Owner, _
                                        _ConfigFileName, _
                                        _ConfigMiddleNode, _
                                        _CommonVariable.IApplication, _
                                        _CommonVariable.ProgramLockId, _
                                        _CommonVariable.BasicConfig, _
                                        configXMLDoc) = False Then
      '
      Return False
    End If

    If _CommonVariable.IApplication.Edition = "SaaS" Then
      '文字列比較はバイナリ順
      m_JapaneseCollation = False
    Else
      '文字列比較は日本語辞書順
      m_JapaneseCollation = True
    End If

    '会社基本情報
    _BEKihonJoho = Tools.KonAPI.LoadBEKihonJoho(CType(CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication))

    Return True

  End Function

  ''' <summary>
  ''' </summary>
  Public Sub TerminateApplicationBasic()
    '
    If CommonVariable.BasicConfig.PcaMenuIn = False Then
      'メニューインされていない時
      If CommonVariable.ProgramLockId <> 0 Then
        'ロック済みの時、解除
        Tools.CloudAPI.EndProgramLock(CommonVariable.IApplication, CommonVariable.ProgramLockId)
      End If
      'ログオフ
      CommonVariable.IApplication.LogOffSystem()
    End If

  End Sub

  ''' <summary>各マスター全件をリストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadMaster() As Boolean
    '
    If _MasterSms.NeedLoad = True Then          '商品
      If LoadBEMasterSms() = False Then
        Return False
      End If
    End If
    If _MasterSoko.NeedLoad = True Then         '倉庫
      If LoadBEMasterSoko() = False Then
        Return False
      End If
    End If
    If _MasterBumon.NeedLoad = True Then        '部門
      If LoadBEMasterBumon() = False Then
        Return False
      End If
    End If
    If _MasterTantosya.NeedLoad = True Then     '担当者
      If LoadBEMasterTantosya() = False Then
        Return False
      End If
    End If
    If _MasterProject.NeedLoad = True Then      'プロジェクト
      If LoadBEMasterProject() = False Then
        Return False
      End If
    End If
    If _MasterTekiyo.NeedLoad = True Then       '摘要
      If LoadBEMasterTekiyo() = False Then
        Return False
      End If
    End If
    If _MasterTms.NeedLoad = True Then          '得意先
      If LoadBEMasterTms() = False Then
        Return False
      End If
    End If
    If _MasterYms.NeedLoad = True Then          '直送先
      If LoadBEMasterYms() = False Then
        Return False
      End If
    End If
    If _MasterRms.NeedLoad = True Then          '仕入先
      If LoadBEMasterRms() = False Then
        Return False
      End If
    End If
    If _AreaUser.NeedLoad = True Then           '領域ユーザー
      If LoadBEAreaUser() = False Then
        Return False
      End If
    End If

    Return True

  End Function

#Region "マスター取得"

#Region "   商品マスター取得"

  '商品マスター・取得状況
  Public Property MasterSms As New LoadStatus

  ''' <summary>商品マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterSmsList As New List(Of BusinessEntity.BEMasterSms)

  ''' <summary>商品マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterSms() As Boolean
    '
    If _MasterSms.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterSmsList.Clear()
      _MasterSms.Loaded = False
    End If

    'マスターを全件取得
    _BEMasterSmsList = FindBEMasterSmsList(String.Empty)
    _MasterSms.Loaded = True

    Return True

  End Function

  ''' <summary>商品マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterSmsList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterSms)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterSmsTool As New PCA.TSC.Kon.BusinessEntity.MasterSmsTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterSms As New PCA.TSC.Kon.BusinessEntity.BEMasterSms
    Dim beMasterSmsList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterSms)

    If code.Length <> 0 Then
      If _MasterSms.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterSmsList.Find(Function(s) Tools.TscStringTool.Equals(s.SyohinCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterSmsTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterSms = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterSms)
        beMasterSmsList.Add(beMasterSms)
      End If
    Else
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterSmsTool, findParameter)
      'API経由でテーブルから全件取得
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterSms = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterSms)
          beMasterSmsList.Add(beMasterSms)
        Next
      End If
    End If

    Return beMasterSmsList

  End Function

  ''' <summary>商品マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件</param>
  Public Function FindBEMasterSms(ByVal code As String) As BusinessEntity.BEMasterSms
    '
    Dim beMasterSms As New BusinessEntity.BEMasterSms
    Dim beMasterSmsList As New List(Of BusinessEntity.BEMasterSms)

    If code.Length <> 0 Then
      beMasterSmsList = FindBEMasterSmsList(code)
      If Not (beMasterSmsList Is Nothing) AndAlso beMasterSmsList.Count > 0 Then
        beMasterSms = beMasterSmsList.Item(0)
      End If
    End If

    Return beMasterSms

  End Function

#End Region

#Region "   倉庫マスター取得"

  '倉庫マスター・取得状況
  Public Property MasterSoko As New LoadStatus

  ''' <summary>倉庫マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterSokoList As New List(Of BusinessEntity.BEMasterSoko)

  ''' <summary>倉庫マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterSoko() As Boolean
    '
    If _MasterSoko.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterSokoList.Clear()
      _MasterSoko.Loaded = False
    End If

    _BEMasterSokoList = FindBEMasterSokoList(String.Empty)
    _MasterSoko.Loaded = True

    Return True

  End Function

  ''' <summary>倉庫マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterSokoList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterSoko)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterSokoTool As New PCA.TSC.Kon.BusinessEntity.MasterSokoTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterSoko As New PCA.TSC.Kon.BusinessEntity.BEMasterSoko
    Dim beMasterSokoList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterSoko)

    If code.Length <> 0 Then
      If _MasterSoko.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterSokoList.Find(Function(s) Tools.TscStringTool.Equals(s.SokoCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterSokoTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterSoko = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterSoko)
        beMasterSokoList.Add(beMasterSoko)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterSokoTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterSoko = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterSoko)
          beMasterSokoList.Add(beMasterSoko)
        Next
      End If
    End If

    Return beMasterSokoList

  End Function

  ''' <summary>倉庫マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterSoko(ByVal code As String) As BusinessEntity.BEMasterSoko
    '
    Dim beMasterSoko As New BusinessEntity.BEMasterSoko
    Dim beMasterSokoList As New List(Of BusinessEntity.BEMasterSoko)

    If code.Length <> 0 Then
      beMasterSokoList = FindBEMasterSokoList(code)
      If Not (beMasterSokoList Is Nothing) AndAlso beMasterSokoList.Count > 0 Then
        beMasterSoko = beMasterSokoList.Item(0)
      End If
    End If

    Return beMasterSoko

  End Function

#End Region

#Region "   部門マスター取得"

  '部門マスター・取得状況
  Public Property MasterBumon As New LoadStatus

  ''' <summary>部門マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterBumonList As New List(Of BusinessEntity.BEMasterBumon)

  ''' <summary>部門マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterBumon() As Boolean
    '
    If _MasterBumon.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterBumonList.Clear()
      _MasterBumon.Loaded = False
    End If

    _BEMasterBumonList = FindBEMasterBumonList(String.Empty)
    _MasterBumon.Loaded = True

    Return True

  End Function

  ''' <summary>部門マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterBumonList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterBumon)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterBumonTool As New PCA.TSC.Kon.BusinessEntity.MasterBumonTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterBumon As New PCA.TSC.Kon.BusinessEntity.BEMasterBumon
    Dim beMasterBumonList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterBumon)

    If code.Length <> 0 Then
      If _MasterBumon.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterBumonList.Find(Function(s) Tools.TscStringTool.Equals(s.BumonCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterBumonTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterBumon = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterBumon)
        beMasterBumonList.Add(beMasterBumon)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterBumonTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterBumon = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterBumon)
          beMasterBumonList.Add(beMasterBumon)
        Next
      End If
    End If

    Return beMasterBumonList

  End Function

  ''' <summary>部門マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterBumon(ByVal code As String) As BusinessEntity.BEMasterBumon
    '
    Dim beMasterBumon As New BusinessEntity.BEMasterBumon
    Dim beMasterBumonList As New List(Of BusinessEntity.BEMasterBumon)

    If code.Length <> 0 Then
      beMasterBumonList = FindBEMasterBumonList(code)
      If Not (beMasterBumonList Is Nothing) AndAlso beMasterBumonList.Count > 0 Then
        beMasterBumon = beMasterBumonList.Item(0)
      End If
    End If

    Return beMasterBumon

  End Function

#End Region

#Region "   担当者マスター取得"

  '担当者マスター・取得状況
  Public Property MasterTantosya As New LoadStatus

  ''' <summary>担当者マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterTantosyaList As New List(Of BusinessEntity.BEMasterTantosya)

  ''' <summary>担当者マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterTantosya() As Boolean
    '
    If _MasterTantosya.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterTantosyaList.Clear()
      _MasterTantosya.Loaded = False
    End If

    _BEMasterTantosyaList = FindBEMasterTantosyaList(String.Empty)
    _MasterTantosya.Loaded = True

    Return True

  End Function

  ''' <summary>担当者マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterTantosyaList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTantosya)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterTantosyaTool As New PCA.TSC.Kon.BusinessEntity.MasterTantosyaTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterTantosya As New PCA.TSC.Kon.BusinessEntity.BEMasterTantosya
    Dim beMasterTantosyaList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTantosya)

    If code.Length <> 0 Then
      If _MasterTantosya.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterTantosyaList.Find(Function(s) Tools.TscStringTool.Equals(s.TantosyaCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterTantosyaTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterTantosya = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterTantosya)
        beMasterTantosyaList.Add(beMasterTantosya)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterTantosyaTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterTantosya = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterTantosya)
          beMasterTantosyaList.Add(beMasterTantosya)
        Next
      End If
    End If

    Return beMasterTantosyaList

  End Function

  ''' <summary>担当者マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterTantosya(ByVal code As String) As BusinessEntity.BEMasterTantosya
    '
    Dim beMasterTantosya As New BusinessEntity.BEMasterTantosya
    Dim beMasterTantosyaList As New List(Of BusinessEntity.BEMasterTantosya)

    If code.Length <> 0 Then
      beMasterTantosyaList = FindBEMasterTantosyaList(code)
      If Not (beMasterTantosyaList Is Nothing) AndAlso beMasterTantosyaList.Count > 0 Then
        beMasterTantosya = beMasterTantosyaList.Item(0)
      End If
    End If

    Return beMasterTantosya

  End Function

#End Region

#Region "   プロジェクトマスター取得"

  'プロジェクトマスター・取得状況
  Public Property MasterProject As New LoadStatus

  ''' <summary>プロジェクトマスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterProjectList As New List(Of BusinessEntity.BEMasterProject)

  ''' <summary>プロジェクトマスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterProject() As Boolean
    '
    If _MasterProject.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterProjectList.Clear()
      _MasterProject.Loaded = False
    End If

    _BEMasterProjectList = FindBEMasterProjectList(String.Empty)
    _MasterProject.Loaded = True

    Return True

  End Function

  ''' <summary>プロジェクトマスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterProjectList(ByVal code As String) As List(Of BusinessEntity.BEMasterProject)
    '
    Dim findParameter As New Tools.FindParameter
    Dim masterProjectTool As New BusinessEntity.MasterProjectTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterProject As New BusinessEntity.BEMasterProject
    Dim beMasterProjectList As New List(Of BusinessEntity.BEMasterProject)

    If code.Length <> 0 Then
      If _MasterProject.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterProjectList.Find(Function(s) Tools.TscStringTool.Equals(s.ProCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterProjectTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterProject = DirectCast(beObject, BusinessEntity.BEMasterProject)
        beMasterProjectList.Add(beMasterProject)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterProjectTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterProject = DirectCast(beObjectList(i), BusinessEntity.BEMasterProject)
          beMasterProjectList.Add(beMasterProject)
        Next
      End If
    End If

    Return beMasterProjectList

  End Function

  ''' <summary>プロジェクトマスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterProject(ByVal code As String) As BusinessEntity.BEMasterProject
    '
    Dim beMasterProject As New BusinessEntity.BEMasterProject
    Dim beMasterProjectList As New List(Of BusinessEntity.BEMasterProject)

    If code.Length <> 0 Then
      beMasterProjectList = FindBEMasterProjectList(code)
      If Not (beMasterProjectList Is Nothing) AndAlso beMasterProjectList.Count > 0 Then
        beMasterProject = beMasterProjectList.Item(0)
      End If
    End If

    Return beMasterProject

  End Function

#End Region

#Region "   得意先マスター取得"

  '得意先マスター・取得状況
  Public Property MasterTms As New LoadStatus

  ''' <summary>得意先マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterTmsList As New List(Of BusinessEntity.BEMasterTms)

  ''' <summary>得意先マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterTms() As Boolean
    '
    If _MasterTms.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterTmsList.Clear()
      _MasterTms.Loaded = False
    End If

    _BEMasterTmsList = FindBEMasterTmsList(String.Empty)
    _MasterTms.Loaded = True

    Return True

  End Function

  ''' <summary>得意先マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterTmsList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTms)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterTmsTool As New PCA.TSC.Kon.BusinessEntity.MasterTmsTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterTms As New PCA.TSC.Kon.BusinessEntity.BEMasterTms
    Dim beMasterTmsList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTms)

    If code.Length <> 0 Then
      If _MasterTms.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterTmsList.Find(Function(s) Tools.TscStringTool.Equals(s.TokuisakiCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterTmsTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterTms = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterTms)
        beMasterTmsList.Add(beMasterTms)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterTmsTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterTms = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterTms)
          beMasterTmsList.Add(beMasterTms)
        Next
      End If
    End If

    Return beMasterTmsList

  End Function

  ''' <summary>得意先マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterTms(ByVal code As String) As BusinessEntity.BEMasterTms
    '
    Dim beMasterTms As New BusinessEntity.BEMasterTms
    Dim beMasterTmsList As New List(Of BusinessEntity.BEMasterTms)

    If code.Length <> 0 Then
      beMasterTmsList = FindBEMasterTmsList(code)
      If Not (beMasterTmsList Is Nothing) AndAlso beMasterTmsList.Count > 0 Then
        beMasterTms = beMasterTmsList.Item(0)
      End If
    End If

    Return beMasterTms

  End Function

#End Region

#Region "   直送先マスター取得"

  '直送先マスター・取得状況
  Public Property MasterYms As New LoadStatus

  ''' <summary>直送先マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterYmsList As New List(Of BusinessEntity.BEMasterYms)

  ''' <summary>直送先マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterYms() As Boolean
    '
    If _MasterYms.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterYmsList.Clear()
      _MasterYms.Loaded = False
    End If

    _BEMasterYmsList = FindBEMasterYmsList(String.Empty)
    _MasterYms.Loaded = True

    Return True

  End Function

  ''' <summary>直送先マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterYmsList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterYms)
    '
    Dim findParameter As New Tools.FindParameter
    Dim masterYmsTool As New PCA.TSC.Kon.BusinessEntity.MasterYmsTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterYms As New PCA.TSC.Kon.BusinessEntity.BEMasterYms
    Dim beMasterYmsList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterYms)

    If code.Length <> 0 Then
      If _MasterYms.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterYmsList.Find(Function(s) Tools.TscStringTool.Equals(s.ChokusosakiCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterYmsTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterYms = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterYms)
        beMasterYmsList.Add(beMasterYms)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterYmsTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterYms = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterYms)
          beMasterYmsList.Add(beMasterYms)
        Next
      End If
    End If

    Return beMasterYmsList

  End Function

  ''' <summary>直送先マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterYms(ByVal code As String) As BusinessEntity.BEMasterYms
    '
    Dim beMasterYms As New BusinessEntity.BEMasterYms
    Dim beMasterYmsList As New List(Of BusinessEntity.BEMasterYms)

    If code.Length <> 0 Then
      beMasterYmsList = FindBEMasterYmsList(code)
      If Not (beMasterYmsList Is Nothing) AndAlso beMasterYmsList.Count > 0 Then
        beMasterYms = beMasterYmsList.Item(0)
      End If
    End If

    Return beMasterYms

  End Function

#End Region

#Region "   仕入先マスター取得"

  '仕入先マスター・取得状況
  Public Property MasterRms As New LoadStatus

  ''' <summary>仕入先マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterRmsList As New List(Of BusinessEntity.BEMasterRms)

  ''' <summary>仕入先マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterRms() As Boolean
    '
    If _MasterRms.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterRmsList.Clear()
      _MasterRms.Loaded = False
    End If

    _BEMasterRmsList = FindBEMasterRmsList(String.Empty)
    _MasterRms.Loaded = True

    Return True

  End Function

  ''' <summary>仕入先マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterRmsList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterRms)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterRmsTool As New PCA.TSC.Kon.BusinessEntity.MasterRmsTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterRms As New PCA.TSC.Kon.BusinessEntity.BEMasterRms
    Dim beMasterRmsList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterRms)

    If code.Length <> 0 Then
      If _MasterRms.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterRmsList.Find(Function(s) Tools.TscStringTool.Equals(s.ShiresakiCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterRmsTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterRms = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterRms)
        beMasterRmsList.Add(beMasterRms)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterRmsTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterRms = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterRms)
          beMasterRmsList.Add(beMasterRms)
        Next
      End If
    End If

    Return beMasterRmsList

  End Function

  ''' <summary>仕入先マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterRms(ByVal code As String) As BusinessEntity.BEMasterRms
    '
    Dim beMasterRms As New BusinessEntity.BEMasterRms
    Dim beMasterRmsList As New List(Of BusinessEntity.BEMasterRms)

    If code.Length <> 0 Then
      beMasterRmsList = FindBEMasterRmsList(code)
      If Not (beMasterRmsList Is Nothing) AndAlso beMasterRmsList.Count > 0 Then
        beMasterRms = beMasterRmsList.Item(0)
      End If
    End If

    Return beMasterRms

  End Function


#End Region

#Region "   摘要マスター取得"

  '摘要マスター・取得状況
  Public Property MasterTekiyo As New LoadStatus

  ''' <summary>摘要先マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterTekiyoList As New List(Of BusinessEntity.BEMasterTekiyo)

  ''' <summary>摘要マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterTekiyo() As Boolean
    '
    If _MasterTekiyo.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterTekiyoList.Clear()
      _MasterTekiyo.Loaded = False
    End If

    _BEMasterTekiyoList = FindBEMasterTekiyoList(0, String.Empty)
    _MasterTekiyo.Loaded = True

    Return True

  End Function

  ''' <summary>摘要マスターを取得
  ''' </summary>
  ''' <param name="kubunId">区分ID。ゼロの場合は全件取得</param>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterTekiyoList(ByVal kubunId As PCA.TSC.Kon.BusinessEntity.Defines.KubunIdType, ByVal code As String) As List(Of BusinessEntity.BEMasterTekiyo)
    '
    Dim findParameter As New Tools.FindParameter
    Dim masterTekiyoTool As New PCA.TSC.Kon.BusinessEntity.MasterTekiyoTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterTekiyo As New PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo
    Dim beMasterTekiyoList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo)

    If kubunId <> 0 AndAlso code.Length <> 0 Then
      If _MasterTekiyo.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterTekiyoList.Find(Function(s) s.KubunId = kubunId And Tools.TscStringTool.Equals(s.TekiyoCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.KubunIdUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.KubunId = kubunId
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterTekiyoTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterTekiyo = DirectCast(beObject, BusinessEntity.BEMasterTekiyo)
        beMasterTekiyoList.Add(beMasterTekiyo)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), masterTekiyoTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterTekiyo = DirectCast(beObjectList(i), BusinessEntity.BEMasterTekiyo)
          beMasterTekiyoList.Add(beMasterTekiyo)
        Next
      End If
    End If

    Return beMasterTekiyoList

  End Function

  ''' <summary>摘要マスターを1件取得
  ''' </summary>
  ''' <param name="kubunId">区分ID。ゼロの場合はゼロ件取得</param>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterTekiyo(ByVal kubunId As BusinessEntity.Defines.KubunIdType, ByVal code As String) As BusinessEntity.BEMasterTekiyo
    '
    Dim beMasterTekiyo As New BusinessEntity.BEMasterTekiyo
    Dim beMasterTekiyoList As New List(Of BusinessEntity.BEMasterTekiyo)

    If code.Length <> 0 Then
      beMasterTekiyoList = FindBEMasterTekiyoList(kubunId, code)
      If Not (beMasterTekiyoList Is Nothing) AndAlso beMasterTekiyoList.Count > 0 Then
        beMasterTekiyo = beMasterTekiyoList.Item(0)
      End If
    End If

    Return beMasterTekiyo

  End Function


#End Region

#Region "   領域ユーザーマスター取得"

  '領域ユーザーマスター・取得状況
  Public Property AreaUser As New LoadStatus

  ''' <summary>領域ユーザーマスター全件リストプロパティ
  ''' </summary>
  Public Property BEAreaUserList As New List(Of BusinessEntity.BEAreaUser)

  ''' <summary>領域ユーザーマスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEAreaUser() As Boolean
    '
    If _AreaUser.Loaded = True Then
      'ロード済の時はクリア
      _BEAreaUserList.Clear()
      _AreaUser.Loaded = False
    End If

    _BEAreaUserList = FindBEAreaUserList(0)
    _AreaUser.Loaded = True

    Return True

  End Function

  ''' <summary>領域ユーザーマスターを取得
  ''' </summary>
  ''' <param name="id">ID。ゼロの場合は全件取得</param>
  Public Function FindBEAreaUserList(ByVal id As Integer) As List(Of BusinessEntity.BEAreaUser)
    '
    Dim findParameter As New Tools.FindParameter
    Dim areaUserTool As New BusinessEntity.AreaUserTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beAreaUser As New BusinessEntity.BEAreaUser
    Dim beAreaUserList As New List(Of BusinessEntity.BEAreaUser)

    If id <> 0 Then
      If _AreaUser.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEAreaUserList.Find(Function(s) s.Id = id)
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.IdUsing = BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Id = id
        beObject = Tools.KonAPI.FindUniqueBE(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), areaUserTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beAreaUser = DirectCast(beObject, BusinessEntity.BEAreaUser)
        beAreaUserList.Add(beAreaUser)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(_CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), areaUserTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beAreaUser = DirectCast(beObjectList(i), BusinessEntity.BEAreaUser)
          beAreaUserList.Add(beAreaUser)
        Next
      End If
    End If

    Return beAreaUserList

  End Function

  ''' <summary>領域ユーザーマスターを1件取得
  ''' </summary>
  ''' <param name="id">ID。ゼロの場合はゼロ件取得</param>
  Public Function FindBEAreaUser(ByVal id As Integer) As BusinessEntity.BEAreaUser
    '
    Dim beAreaUser As New BusinessEntity.BEAreaUser
    Dim beAreaUserList As New List(Of BusinessEntity.BEAreaUser)

    If id <> 0 Then
      beAreaUserList = FindBEAreaUserList(id)
      If Not (beAreaUserList Is Nothing) AndAlso beAreaUserList.Count > 0 Then
        beAreaUser = beAreaUserList.Item(0)
      End If
    End If

    Return beAreaUser

  End Function


#End Region

#End Region

#Region "伝票取得"

#Region "   売上伝票取得"

  ''' <summary>売上伝票を指示条件にて取得。
  ''' </summary>
  ''' <param name="inputSYKCondition">conditionパラメータ</param>
  Public Function FindByConditionBEInputSYKList(ByVal inputSYKCondition As BusinessEntity.InputSYKCondition) As List(Of BEInputSYK)
    '
    Dim inputSYKTool As New BusinessEntity.InputSYKTool
    Dim inputSYKconditionTool As New BusinessEntity.InputSYKConditionTool
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beInputSYK As New BusinessEntity.BEInputSYK
    Dim beInputSYKList As New List(Of BusinessEntity.BEInputSYK)

    beObjectList = Tools.KonAPI.FindByConditionBE(CType(CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), inputSYKTool, inputSYKCondition, inputSYKconditionTool)

    If Not beObjectList Is Nothing Then
      For i As Integer = 0 To beObjectList.Count - 1
        beInputSYK = DirectCast(beObjectList(i), BusinessEntity.BEInputSYK)
        beInputSYKList.Add(beInputSYK)
      Next
    End If

    Return beInputSYKList

  End Function

  ''' <summary>売上伝票をIdにて取得。
  ''' </summary>
  ''' <param name="id">伝票ヘッダーID。ゼロの場合はゼロ件取得。</param>
  Public Function FindBEInputSYK(ByVal id As Integer) As BusinessEntity.BEInputSYK
    '
    Dim inputSYKCondition As New BusinessEntity.InputSYKCondition
    Dim beInputSYKList As New List(Of BusinessEntity.BEInputSYK)
    Dim beInputSYK As New BusinessEntity.BEInputSYK

    If id <> 0 Then
      inputSYKCondition.Id = id
      beInputSYKList = FindByConditionBEInputSYKList(inputSYKCondition)
      If Not (beInputSYKList Is Nothing) AndAlso beInputSYKList.Count > 0 Then
        beInputSYK = beInputSYKList.Item(0)
      End If
    End If

    Return beInputSYK

  End Function

#End Region

#Region "   受注伝票取得"

  ''' <summary>受注伝票を指示条件にて取得。
  ''' </summary>
  ''' <param name="inputJUCCondition">conditionパラメータ</param>
  Public Function FindByConditionBEInputJUCList(ByVal inputJUCCondition As BusinessEntity.InputJUCCondition) As List(Of BEInputJUC)
    '
    Dim inputJUCTool As New BusinessEntity.InputJUCTool
    Dim inputJUCconditionTool As New BusinessEntity.InputJUCConditionTool
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beInputJUC As New BusinessEntity.BEInputJUC
    Dim beInputJUCList As New List(Of BusinessEntity.BEInputJUC)

    beObjectList = Tools.KonAPI.FindByConditionBE(CType(CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), inputJUCTool, inputJUCCondition, inputJUCconditionTool)

    If Not beObjectList Is Nothing Then
      For i As Integer = 0 To beObjectList.Count - 1
        beInputJUC = DirectCast(beObjectList(i), BusinessEntity.BEInputJUC)
        beInputJUCList.Add(beInputJUC)
      Next
    End If

    Return beInputJUCList

  End Function

  ''' <summary>受注伝票をIdにて取得。
  ''' </summary>
  ''' <param name="id">伝票ヘッダーID。ゼロの場合はゼロ件取得。</param>
  Public Function FindBEInputJUC(ByVal id As Integer) As BusinessEntity.BEInputJUC
    '
    Dim inputJUCCondition As New BusinessEntity.InputJUCCondition
    Dim beInputJUCList As New List(Of BusinessEntity.BEInputJUC)
    Dim beInputJUC As New BusinessEntity.BEInputJUC

    If id <> 0 Then
      inputJUCCondition.Id = id
      beInputJUCList = FindByConditionBEInputJUCList(inputJUCCondition)
      If Not (beInputJUCList Is Nothing) AndAlso beInputJUCList.Count > 0 Then
        beInputJUC = beInputJUCList.Item(0)
      End If
    End If

    Return beInputJUC

  End Function

#End Region

#Region "   見積伝票取得"

  ''' <summary>見積伝票を指示条件にて取得。
  ''' </summary>
  ''' <param name="inputMITCondition">conditionパラメータ</param>
  Public Function FindByConditionBEInputMITList(ByVal inputMITCondition As BusinessEntity.InputMITCondition) As List(Of BEInputMIT)
    '
    Dim inputMITTool As New BusinessEntity.InputMITTool
    Dim inputMITconditionTool As New BusinessEntity.InputMITConditionTool
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beInputMIT As New BusinessEntity.BEInputMIT
    Dim beInputMITList As New List(Of BusinessEntity.BEInputMIT)

    beObjectList = Tools.KonAPI.FindByConditionBE(CType(CommonVariable.IApplication, PCA.ApplicationIntegration.IntegratedApplication), inputMITTool, inputMITCondition, inputMITconditionTool)

    If Not beObjectList Is Nothing Then
      For i As Integer = 0 To beObjectList.Count - 1
        beInputMIT = DirectCast(beObjectList(i), BusinessEntity.BEInputMIT)
        beInputMITList.Add(beInputMIT)
      Next
    End If

    Return beInputMITList

  End Function

  ''' <summary>見積伝票をIdにて取得。
  ''' </summary>
  ''' <param name="id">伝票ヘッダーID。ゼロの場合はゼロ件取得。</param>
  Public Function FindBEInputMIT(ByVal id As Integer) As BusinessEntity.BEInputMIT
    '
    Dim inputMITCondition As New BusinessEntity.InputMITCondition
    Dim beInputMITList As New List(Of BusinessEntity.BEInputMIT)
    Dim beInputMIT As New BusinessEntity.BEInputMIT

    If id <> 0 Then
      inputMITCondition.Id = id
      beInputMITList = FindByConditionBEInputMITList(inputMITCondition)
      If Not (beInputMITList Is Nothing) AndAlso beInputMITList.Count > 0 Then
        beInputMIT = beInputMITList.Item(0)
      End If
    End If

    Return beInputMIT

  End Function

#End Region

#End Region

End Class

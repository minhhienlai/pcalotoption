﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmShipLotList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmShipLotList))
    Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
    Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrint = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPreview = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandExport = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPrint = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPreview = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemExport = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.ファイルFToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.印刷PToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.印刷プレビューVToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.出力OToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.閉じるXToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.編集EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.再集計RToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.参照UToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.ヘルプHToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.目次CToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.rsetShipCode = New PCA.Controls.PcaRangeCodeSet()
    Me.rdtShipDate = New PCA.Controls.PcaRangeDate()
    Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.dgv = New System.Windows.Forms.DataGridView()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandSearch, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandPrint, Me.PcaFunctionCommandPreview, Me.PcaFunctionCommandExport, Me.PcaFunctionCommandClose})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 539)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1098, 28)
    Me.PcaFunctionBar1.TabIndex = 5
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    '
    'PcaFunctionCommandSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSearch, Me.PcaCommandItemSearch)
    Me.PcaFunctionCommandSearch.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandSearch.Tag = Nothing
    Me.PcaFunctionCommandSearch.Text = "再集計"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    '
    'PcaFunctionCommandPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrint, Me.PcaCommandItemPrint)
    Me.PcaFunctionCommandPrint.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPrint.Tag = Nothing
    Me.PcaFunctionCommandPrint.Text = "印刷"
    '
    'PcaFunctionCommandPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPreview, Me.PcaCommandItemPreview)
    Me.PcaFunctionCommandPreview.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPreview.ModifierKeys = PCA.Controls.FunctionModifierKeys.Shift
    Me.PcaFunctionCommandPreview.Tag = Nothing
    Me.PcaFunctionCommandPreview.Text = "プレビュー"
    '
    'PcaFunctionCommandExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandExport, Me.PcaCommandItemExport)
    Me.PcaFunctionCommandExport.FunctionKey = PCA.Controls.FunctionKey.F10
    Me.PcaFunctionCommandExport.Tag = Nothing
    Me.PcaFunctionCommandExport.Text = "出力"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemSearch, Me.PcaCommandItemRefer, Me.PcaCommandItemPrint, Me.PcaCommandItemPreview, Me.PcaCommandItemExport, Me.PcaCommandItemClose})
    Me.PcaCommandManager1.Parent = Me
    '
    'PcaCommandItemSearch
    '
    Me.PcaCommandItemSearch.CommandName = "CommandItemSearch"
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandName = "CommandRefer"
    '
    'PcaCommandItemPrint
    '
    Me.PcaCommandItemPrint.CommandName = "CommandItemPrint"
    '
    'PcaCommandItemPreview
    '
    Me.PcaCommandItemPreview.CommandName = "CommandItemPreview"
    '
    'PcaCommandItemExport
    '
    Me.PcaCommandItemExport.CommandId = 10
    Me.PcaCommandItemExport.CommandName = "CommandItemExport"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandName = "CommandItemClose"
    '
    'ファイルFToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ファイルFToolStripMenuItem, Nothing)
    Me.ファイルFToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.印刷PToolStripMenuItem, Me.印刷プレビューVToolStripMenuItem, Me.出力OToolStripMenuItem, Me.閉じるXToolStripMenuItem})
    Me.ファイルFToolStripMenuItem.Name = "ファイルFToolStripMenuItem"
    Me.ファイルFToolStripMenuItem.ShortcutKeyDisplayString = "F"
    Me.ファイルFToolStripMenuItem.Size = New System.Drawing.Size(96, 20)
    Me.ファイルFToolStripMenuItem.Text = "ファイル(&F)"
    '
    '印刷PToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.印刷PToolStripMenuItem, Me.PcaCommandItemPrint)
    Me.印刷PToolStripMenuItem.Name = "印刷PToolStripMenuItem"
    Me.印刷PToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
    Me.印刷PToolStripMenuItem.Text = "印刷(&P)"
    '
    '印刷プレビューVToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.印刷プレビューVToolStripMenuItem, Me.PcaCommandItemPreview)
    Me.印刷プレビューVToolStripMenuItem.Name = "印刷プレビューVToolStripMenuItem"
    Me.印刷プレビューVToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
    Me.印刷プレビューVToolStripMenuItem.Text = "プレビュー(&V)"
    '
    '出力OToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.出力OToolStripMenuItem, Me.PcaCommandItemExport)
    Me.出力OToolStripMenuItem.Name = "出力OToolStripMenuItem"
    Me.出力OToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
    Me.出力OToolStripMenuItem.Text = "出力(&O)"
    '
    '閉じるXToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.閉じるXToolStripMenuItem, Me.PcaCommandItemClose)
    Me.閉じるXToolStripMenuItem.Name = "閉じるXToolStripMenuItem"
    Me.閉じるXToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
    Me.閉じるXToolStripMenuItem.Text = "閉じる(&X)"
    '
    '編集EToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.編集EToolStripMenuItem, Nothing)
    Me.編集EToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.再集計RToolStripMenuItem, Me.参照UToolStripMenuItem})
    Me.編集EToolStripMenuItem.Name = "編集EToolStripMenuItem"
    Me.編集EToolStripMenuItem.Size = New System.Drawing.Size(68, 20)
    Me.編集EToolStripMenuItem.Text = "編集(&E)"
    '
    '再集計RToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.再集計RToolStripMenuItem, Me.PcaCommandItemSearch)
    Me.再集計RToolStripMenuItem.Name = "再集計RToolStripMenuItem"
    Me.再集計RToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
    Me.再集計RToolStripMenuItem.Text = "再集計(&R)"
    '
    '参照UToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.参照UToolStripMenuItem, Me.PcaCommandItemRefer)
    Me.参照UToolStripMenuItem.Name = "参照UToolStripMenuItem"
    Me.参照UToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
    Me.参照UToolStripMenuItem.Text = "参照(&U)"
    '
    'ヘルプHToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ヘルプHToolStripMenuItem, Nothing)
    Me.ヘルプHToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.目次CToolStripMenuItem})
    Me.ヘルプHToolStripMenuItem.Name = "ヘルプHToolStripMenuItem"
    Me.ヘルプHToolStripMenuItem.Size = New System.Drawing.Size(82, 20)
    Me.ヘルプHToolStripMenuItem.Text = "ヘルプ(&H)"
    '
    '目次CToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.目次CToolStripMenuItem, Me.PcaCommandItemHelp)
    Me.目次CToolStripMenuItem.Name = "目次CToolStripMenuItem"
    Me.目次CToolStripMenuItem.Size = New System.Drawing.Size(123, 22)
    Me.目次CToolStripMenuItem.Text = "目次(&C)"
    '
    'ToolStripButton1
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButton1, Me.PcaCommandItemClose)
    Me.ToolStripButton1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
    Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButton1.Name = "ToolStripButton1"
    Me.ToolStripButton1.Size = New System.Drawing.Size(53, 41)
    Me.ToolStripButton1.Text = "閉じる"
    Me.ToolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButton2
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButton2, Me.PcaCommandItemHelp)
    Me.ToolStripButton2.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
    Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButton2.Name = "ToolStripButton2"
    Me.ToolStripButton2.Size = New System.Drawing.Size(53, 41)
    Me.ToolStripButton2.Text = "ヘルプ"
    Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButton3
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButton3, Me.PcaCommandItemPrint)
    Me.ToolStripButton3.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
    Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButton3.Name = "ToolStripButton3"
    Me.ToolStripButton3.Size = New System.Drawing.Size(39, 41)
    Me.ToolStripButton3.Text = "印刷"
    Me.ToolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButton4
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButton4, Me.PcaCommandItemPreview)
    Me.ToolStripButton4.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
    Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButton4.Name = "ToolStripButton4"
    Me.ToolStripButton4.Size = New System.Drawing.Size(81, 41)
    Me.ToolStripButton4.Text = "プレビュー"
    Me.ToolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButton5
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButton5, Me.PcaCommandItemExport)
    Me.ToolStripButton5.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
    Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButton5.Name = "ToolStripButton5"
    Me.ToolStripButton5.Size = New System.Drawing.Size(39, 41)
    Me.ToolStripButton5.Text = "出力"
    Me.ToolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButton6
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButton6, Me.PcaCommandItemSearch)
    Me.ToolStripButton6.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
    Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButton6.Name = "ToolStripButton6"
    Me.ToolStripButton6.Size = New System.Drawing.Size(53, 41)
    Me.ToolStripButton6.Text = "再集計"
    Me.ToolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ファイルFToolStripMenuItem, Me.編集EToolStripMenuItem, Me.ヘルプHToolStripMenuItem})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(1098, 24)
    Me.MenuStrip1.TabIndex = 9
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.AutoSize = False
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton3, Me.ToolStripButton4, Me.ToolStripButton5, Me.ToolStripButton6, Me.ToolStripButton2})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1098, 44)
    Me.ToolStrip1.TabIndex = 10
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'rsetShipCode
    '
    Me.rsetShipCode.AutoTopMargin = False
    Me.rsetShipCode.ClientProcess = Nothing
    Me.rsetShipCode.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.rsetShipCode.LimitLength = 26
    Me.rsetShipCode.Location = New System.Drawing.Point(396, 77)
    Me.rsetShipCode.MaxCodeLength = 10
    Me.rsetShipCode.MaxLabelLength = 10
    Me.rsetShipCode.MaxNameLength = 0
    Me.rsetShipCode.Name = "rsetShipCode"
    Me.rsetShipCode.RangeGroupBoxText = "出荷先"
    Me.rsetShipCode.RangeGroupBoxVisible = False
    Me.rsetShipCode.Size = New System.Drawing.Size(287, 22)
    Me.rsetShipCode.TabIndex = 2
    Me.rsetShipCode.ToGroupBoxText = ""
    '
    'rdtShipDate
    '
    Me.rdtShipDate.AutoTopMargin = False
    Me.rdtShipDate.ClientProcess = Nothing
    Me.rdtShipDate.DayLabel = ""
    Me.rdtShipDate.DisplaySlash = True
    Me.rdtShipDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.rdtShipDate.Holidays = Nothing
    Me.rdtShipDate.Location = New System.Drawing.Point(9, 77)
    Me.rdtShipDate.MaxLabelLength = 10
    Me.rdtShipDate.MaxTextLength = 14
    Me.rdtShipDate.MonthLabel = "/"
    Me.rdtShipDate.Name = "rdtShipDate"
    Me.rdtShipDate.RangeGroupBoxText = "出荷日"
    Me.rdtShipDate.RangeGroupBoxVisible = False
    Me.rdtShipDate.Size = New System.Drawing.Size(301, 22)
    Me.rdtShipDate.TabIndex = 1
    Me.rdtShipDate.YearLabel = "/"
    '
    'dgv
    '
    Me.dgv.AllowUserToAddRows = False
    Me.dgv.AllowUserToDeleteRows = False
    Me.dgv.AllowUserToResizeRows = False
    Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dgv.BackgroundColor = System.Drawing.SystemColors.Control
    DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
    DataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue
    DataGridViewCellStyle1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Window
    DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
    DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
    DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
    Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
    Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
    Me.dgv.EnableHeadersVisualStyles = False
    Me.dgv.Location = New System.Drawing.Point(9, 113)
    Me.dgv.Name = "dgv"
    DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
    DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Highlight
    DataGridViewCellStyle2.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
    DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
    DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
    DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
    Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle2
    Me.dgv.RowHeadersVisible = False
    Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
    Me.dgv.Size = New System.Drawing.Size(1073, 413)
    Me.dgv.TabIndex = 11
    '
    'frmShipLotList
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1098, 567)
    Me.Controls.Add(Me.dgv)
    Me.Controls.Add(Me.rdtShipDate)
    Me.Controls.Add(Me.rsetShipCode)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1100, 500)
    Me.Name = "frmShipLotList"
    Me.Text = "出荷ロット一覧表"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandPrint As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandExport As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandPreview As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents ファイルFToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 印刷PToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 印刷プレビューVToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 出力OToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 閉じるXToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 編集EToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 再集計RToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 参照UToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ヘルプHToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 目次CToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents rdtShipDate As PCA.Controls.PcaRangeDate
  Friend WithEvents rsetShipCode As PCA.Controls.PcaRangeCodeSet
    Friend WithEvents PcaCommandItemSearch As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemPrint As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemPreview As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemExport As PCA.Controls.PcaCommandItem
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
End Class

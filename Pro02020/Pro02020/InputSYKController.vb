﻿Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity
Imports System.ComponentModel
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports PCA.TSC.Kon
Imports PCA.TSC.Kon.Tools
Imports PCA
Imports System.Xml

Public Class InputSYKController

  Inherits TscInputControllerBase

#Region "プロパティ"

  ''' <summary>
  ''' 検索時一覧表示をするかどうかを取得、設定します。
  ''' </summary>
  Public Property IsSearchIchiran As Boolean

  ''' <summary>
  ''' 見積No/受注Noの切り替えを取得、設定します。
  ''' </summary>
  Public Property MJKbnInputMode() As PCA.TSC.Kon.BusinessEntity.Defines.MitsumoriJuchuKubunType = BusinessEntity.Defines.MitsumoriJuchuKubunType.Juchu

  ''' <summary>伝区の初期値値を取得、設定。
  ''' </summary>
  Public Property DefaultDenku() As PCA.TSC.Kon.BusinessEntity.Defines.DenkuType = BusinessEntity.Defines.DenkuType.Kake

  ''' <summary>
  ''' LeaveInputをキャンセルするかどうかを取得または設定します。
  ''' </summary>
  Public Property IsCancelLeavInput As Boolean

#End Region

  ''' <summary>コンストラクタ
  ''' </summary>
  Public Sub New(ByVal owner As IWin32Window)
    '
    '売上伝票入力用に基底クラスを実体化
    MyBase.New(owner, PEKonInput.DenpyoType.Hachu)

  End Sub

#Region "初期化"

  ''' <summary>コントローラクラスを初期化
  ''' </summary>
  Public Overrides Function Initialize(ByRef configXMLDoc As XmlDocument) As Boolean
    '
    'PCAクラウドAPIの準備、会社基本情報を読込み
    If MyBase.Initialize(configXMLDoc) = False Then
      Return False
    End If

    'マスターの先読込み（メモリ化）
    MyBase.MasterSms.NeedLoad = True        '商品
    MyBase.MasterSoko.NeedLoad = True       '倉庫
    MyBase.MasterBumon.NeedLoad = True      '部門
    MyBase.MasterTantosya.NeedLoad = True   '担当者
    MyBase.MasterProject.NeedLoad = True    'プロジェクト
    MyBase.MasterTekiyo.NeedLoad = True     '摘要
    MyBase.MasterTms.NeedLoad = True        '得意先
    MyBase.MasterRms.NeedLoad = True        '得意先
    MyBase.MasterYms.NeedLoad = True        '直送先
    MyBase.AreaUser.NeedLoad = True         '領域ユーザー
    If Not MyBase.LoadMaster() Then
      Return False
    End If

    '参照画面の表示項目の先読込み（メモリ化）
    MyBase.ReferSms.NeedLoad = True        '商品
    MyBase.ReferSoko.NeedLoad = True       '倉庫
    MyBase.ReferBumon.NeedLoad = True      '部門
    MyBase.ReferTantosya.NeedLoad = True   '担当者
    MyBase.ReferProject.NeedLoad = True    'プロジェクト
    MyBase.ReferTekiyo.NeedLoad = True     '摘要
    MyBase.ReferTms.NeedLoad = True        '得意先
    MyBase.ReferYms.NeedLoad = True        '直送先
    If Not MyBase.LoadReferItemsList() Then
      Return False
    End If

    '売上伝票のIDリストを初期化([検索]、[前移動]/[次移動]用)
    Me.SearchResultList = New List(Of ReferItems)

    'デフォルト日付の設定
    Me.InitializeDefaultHizuke()

    Return True

  End Function


#End Region

#Region "売上伝票関連メソッド"

  ''' <summary>
  ''' 売上伝票を取得します
  ''' </summary>
  ''' <param name="id">ID</param>
  ''' <returns>売上伝票</returns>
  ''' <remarks></remarks>
  Public Function GetBEInputSYK(ByVal id As Integer) As BEInputSYK
    '
    Dim beInputSYK As New BEInputSYK

    Try
      beInputSYK = MyBase.FindBEInputSYK(id)
      If beInputSYK.InputSYKH.Id = 0 Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(MessageDefines.Exclamation.DenpyoNotExist, BusinessEntity.ItemText.Uriage), _
                                  CommonVariable.BasicConfig.ProgramName)
        Return beInputSYK
      End If
      Return beInputSYK

    Catch ex As Exception
      Throw

    End Try

  End Function

  ''' <summary>
  ''' 売上伝票のロック情報
  ''' </summary>
  ''' <remarks></remarks>
  Private m_BELockKonDenpyoSYK As BusinessEntity.BELockKon = Nothing

  ''' <summary>
  ''' 売上伝票をロックします
  ''' </summary>
  ''' <param name="inputSYKId">伝票ID</param>
  ''' <returns>ロックに成功したらTrueを返します。</returns>
  ''' <remarks></remarks>
  Public Function LockInputSYK(ByVal inputSYKId As Integer) As Boolean

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputSYKTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon

    Try
      Target.TargetId = inputSYKId

      ResultLockSingleBE = Tools.LockKon.LockSingleBE(CommonVariable.IApplication, InputTool, Target, True)

      If ResultLockSingleBE.Status = IntegratedStatus.Success Then
        m_BELockKonDenpyoSYK = DirectCast(ResultLockSingleBE.BELockObject, BusinessEntity.BELockKon)
        Return True
      Else
        ShowMessage.OnExclamation(m_Owner, Tools.MessageDefines.Exclamation.CantLock & Environment.NewLine & _
         ResultLockSingleBE.ErrorCode & Environment.NewLine & ResultLockSingleBE.ErrorMessage, _
         CommonVariable.BasicConfig.ProgramName)
        Return False
      End If
    Catch ex As Exception
      TscExceptionBox.Show(m_Owner, ex, CommonVariable.BasicConfig.ProgramName, CommonVariable.BasicConfig.ProgramName)
      Return False
    End Try

  End Function

  ''' <summary>
  ''' 売上伝票をロックします
  ''' </summary>
  ''' <param name="inputSYKId">伝票ID</param>
  ''' <param name="errCode">(参照渡し)エラーコード</param>
  ''' <returns>ロックに成功したらTrueを返します。</returns>
  ''' <remarks></remarks>
  Public Function LockInputSYK(ByVal inputSYKId As Integer, ByRef errCode As String) As Boolean

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputSYKTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon

    Try
      Target.TargetId = inputSYKId

      ResultLockSingleBE = Tools.LockKon.LockSingleBE(CommonVariable.IApplication, InputTool, Target, True)

      If ResultLockSingleBE.Status = IntegratedStatus.Success Then
        m_BELockKonDenpyoSYK = DirectCast(ResultLockSingleBE.BELockObject, BusinessEntity.BELockKon)
        Return True
      Else
        ShowMessage.OnExclamation(m_Owner, MessageDefines.Exclamation.CantLock & Environment.NewLine & _
         ResultLockSingleBE.ErrorCode & Environment.NewLine & ResultLockSingleBE.ErrorMessage, CommonVariable.BasicConfig.ProgramName)
        errCode = ResultLockSingleBE.ErrorCode
        Return False
      End If
    Catch ex As Exception
      TscExceptionBox.Show(m_Owner, ex, CommonVariable.BasicConfig.ProgramName, CommonVariable.BasicConfig.ProgramName)
      Return False
    End Try

  End Function

  ''' <summary>
  ''' 売上伝票のロックを解除します。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub UnLockInputSYK()

    If IsNothing(m_BELockKonDenpyoSYK) OrElse m_BELockKonDenpyoSYK.Id = 0 Then
      'このPGでロックしていない
      Return
    End If

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputSYKTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon
    Dim LockKonTool As New BusinessEntity.LockKonTool

    ResultLockSingleBE = Tools.LockKon.UnlockSingleBE(CommonVariable.IApplication, InputTool, m_BELockKonDenpyoSYK)

    'ロック解除に失敗してもエラーにはなりません
    m_BELockKonDenpyoSYK = Nothing

  End Sub

  ''' <summary>
  ''' [前伝票]コマンドに呼ばれ、前伝票を取得します。
  ''' </summary>
  ''' <param name="selectedId">現在選択されている伝票のID</param>
  ''' <returns>前伝票</returns>
  ''' <remarks></remarks>
  Public Function GetPreviousDenp(ByVal selectedId As Integer) As BEInputSYK
    '
    Dim targetId As Integer = 0                 '前伝票のID
    Dim existPreviousDenp As Boolean = False    '前伝票が存在する場合はTrue
    Dim beInputSYK As New BEInputSYK

    If Me.SearchResultList.Count > 0 Then
      '直近１か月以内に登録した売上伝票が1件以上存在
      If selectedId <= 0 Then
        '伝票未選択時(=新規入力時)
        targetId = CInt(Me.SearchResultList(Me.SearchResultList.Count - 1).Key)
        existPreviousDenp = True
      Else
        For index As Integer = Me.SearchResultList.Count To 1 Step -1
          If selectedId = CInt(Me.SearchResultList(index - 1).Key) Then
            If index > 1 Then
              targetId = CInt(Me.SearchResultList(index - 2).Key)
              existPreviousDenp = True
            Else
              existPreviousDenp = False
            End If
            Exit For
          End If
        Next
      End If
    End If

    If existPreviousDenp Then
      beInputSYK = GetBEInputSYK(targetId)
    End If

    Return beInputSYK
  End Function

  ''' <summary>
  ''' [次伝票]コマンドに呼ばれ、次伝票を取得します。
  ''' </summary>
  ''' <param name="selectedId">現在選択されている伝票のID</param>
  ''' <returns>次伝票</returns>
  ''' <remarks></remarks>
  Public Function GetFollowingDenp(ByVal selectedId As Integer) As BEInputSYK
    Dim targetId As Integer = 0                 '次伝票のID
    Dim existPreviousDenp As Boolean = False    '次伝票が存在する場合はTrue
    Dim beInputSYK As New BEInputSYK
    If Me.SearchResultList.Count > 0 Then
      '直近１か月以内に登録した売上伝票が1件以上存在
      If selectedId <= 0 Then
        '伝票未選択時(=新規入力時)
        existPreviousDenp = False
      Else
        For index As Integer = 0 To Me.SearchResultList.Count
          If selectedId = CInt(Me.SearchResultList(index).Key) Then
            If index = Me.SearchResultList.Count - 1 Then
              existPreviousDenp = False
            Else
              targetId = CInt(Me.SearchResultList(index + 1).Key)
              existPreviousDenp = True
            End If
            Exit For
          End If
        Next
      End If
    End If

    If existPreviousDenp Then
      beInputSYK = GetBEInputSYK(targetId)
    End If

    Return beInputSYK
  End Function

  ''' <summary>
  ''' 売上伝票を削除します
  ''' </summary>
  ''' <param name="beInputSYK">売上伝票</param>
  ''' <returns>処理結果</returns>
  ''' <remarks></remarks>
  Public Function EraseInputSYK(ByVal beInputSYK As BEInputSYK) As BEResultOfErase
    '
    Dim arrayOfBEInputSYK As List(Of BEInputSYK) = New List(Of BEInputSYK)

    arrayOfBEInputSYK.Add(beInputSYK)

    Dim saveParam As SaveParameter =
     New SaveParameter With {.TransactionScopeUsing = BusinessEntity.Defines.UsingType.UsingOneItem, .TransactionScope = Tools.Defines.TransactionScopeType.Whole}

    Return KonAPI.EraseInputSYK(CommonVariable.IApplication, arrayOfBEInputSYK, saveParam)
  End Function

  ''' <summary>
  ''' 伝票データを登録します。
  ''' </summary>
  ''' <param name="beInputSYK">売上伝票</param>
  ''' <param name="methodType">アプリケーション処理タイプ</param>
  ''' <returns>処理結果</returns>
  ''' <remarks></remarks>
  Public Function CreateBEInputSYK(ByVal beInputSYK As BEInputSYK, methodType As PCA.TSC.Kon.BusinessEntity.Defines.IntegratedApplicationMethodType) As TSC.Kon.Tools.ResultSaveInputSYK

    Dim arrayOfBEInputSYK As ArrayOfBEInputSYK = New ArrayOfBEInputSYK
    arrayOfBEInputSYK.BEInputSYK.Add(beInputSYK)
    Dim saveParam As SaveParameter =
     New SaveParameter With {.TransactionScopeUsing = BusinessEntity.Defines.UsingType.UsingOneItem, .TransactionScope = Tools.Defines.TransactionScopeType.Whole, .CalcTax = False}
    Return KonAPI.SaveInputSYK(CommonVariable.IApplication, methodType, arrayOfBEInputSYK, saveParam)

  End Function
#End Region

#Region "見積・受注関連メソッド"


  ''' <summary>
  ''' 受注伝票のロック情報の保持クラス
  ''' </summary>
  ''' <remarks></remarks>
  Private m_BELockKonDenpyoJUC As BusinessEntity.BELockKon = Nothing

  ''' <summary>
  ''' 受注伝票をロックします
  ''' </summary>
  ''' <param name="inputJUCId">受注伝票ID</param>
  ''' <returns>受注伝票のロックに成功したかどうか</returns>
  ''' <remarks></remarks>
  Public Function LockInputJUC(ByVal inputJUCId As Integer) As Boolean

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputJUCTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon

    Try
      Target.TargetId = inputJUCId

      ResultLockSingleBE = Tools.LockKon.LockSingleBE(CommonVariable.IApplication, InputTool, Target, True)

      If ResultLockSingleBE.Status = IntegratedStatus.Success Then
        m_BELockKonDenpyoJUC = DirectCast(ResultLockSingleBE.BELockObject, BusinessEntity.BELockKon)
        Return True
      Else
        ShowMessage.OnExclamation(m_Owner, MessageDefines.Exclamation.CantLock & Environment.NewLine & _
         ResultLockSingleBE.ErrorCode & Environment.NewLine & _
         ResultLockSingleBE.ErrorMessage, CommonVariable.BasicConfig.ProgramName)
        Return False
      End If
    Catch ex As Exception
      TscExceptionBox.Show(m_Owner, ex, CommonVariable.BasicConfig.ProgramName, CommonVariable.BasicConfig.ProgramName)
      Return False
    End Try

  End Function

  ''' <summary>
  ''' 受注伝票のロックを解除します。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub UnLockInputJUC()

    If IsNothing(m_BELockKonDenpyoJUC) OrElse m_BELockKonDenpyoJUC.Id = 0 Then
      'このPGでロックしていない
      Return
    End If

    Dim Target As New BusinessEntity.BELockKon
    Dim InputTool As New BusinessEntity.InputJUCTool
    Dim ResultLockSingleBE As New Tools.ResultLockSingleBE
    Dim BELockKon As New BusinessEntity.BELockKon
    Dim LockKonTool As New BusinessEntity.LockKonTool

    ResultLockSingleBE = Tools.LockKon.UnlockSingleBE(CommonVariable.IApplication, InputTool, m_BELockKonDenpyoJUC)

    'ロック解除に失敗してもエラーにはなりません
    m_BELockKonDenpyoJUC = Nothing

  End Sub

#End Region

#Region "エンティティ操作"

  ''' <summary>
  ''' エンティティを初期化します。
  ''' </summary>
  ''' <returns>初期化されたエンティティ</returns>
  ''' <remarks></remarks>
  Public Function CreateInitialEntity() As PEKonInput
    'Public Function InitializePEKonInputforSYK() As PEKonInput
    '
    Dim beInputSYK As New BEInputSYK()
    Dim peKonInput As PEKonInput = ConvertKonInput.SYKToInput(beInputSYK, Nothing, Nothing, Nothing)

    ' 条件により初期値が変わる項目はここでセットする
    peKonInput.Header.DenpyoHizuke = Me.DefaultHizuke
    peKonInput.Header.Hizuke = Me.DefaultHizuke
    peKonInput.Header.Denku = DefaultDenku      ' 伝区
    peKonInput.Header.MitsumoriJuchuKubun = Me.MJKbnInputMode   ' 見積/受注No区分

    Return peKonInput

  End Function

  ''' <summary>
  ''' データの調整を行います。
  ''' </summary>
  ''' <param name="beInputSYK">調整対象の伝票データ</param>
  ''' <remarks></remarks>
  Public Sub AdjustDenpyo(ByVal beInputSYK As BEInputSYK)

    If beInputSYK.InputSYKH.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Kake Then
      '請求日をクリア
      beInputSYK.InputSYKH.Seikyubi = 0
    End If

    ' 見積受注区分の設定見直し
    If [Enum].IsDefined(GetType(PCA.TSC.Kon.BusinessEntity.Defines.MitsumoriJuchuKubunType), beInputSYK.InputSYKH.MitsumoriJuchuKubun) = False Then
      beInputSYK.InputSYKH.MitsumoriJuchuKubun = Me.MJKbnInputMode
    End If

    Dim tokuisaki As BEMasterTms = MyBase.FindBEMasterTms(beInputSYK.InputSYKH.TokuisakiCode)
    Dim seikyusaki As BEMasterTms = MyBase.FindSeikyusaki(tokuisaki)

    Dim needCalcZei As Boolean = False
    Dim needCalcTotal As Boolean = False
    Dim addEntries As New List(Of BEInputSYKD)

    For Each beInputSYKD As BEInputSYKD In beInputSYK.InputSYKDList.BEInputSYKD
      needCalcZei = False
      '金額、原価、売価金額の小数桁以下を丸める
      If KonCalc.CountSyosuKeta(beInputSYKD.Kingaku) > 0 Then
        beInputSYKD.Kingaku = KonCalc.Round(beInputSYKD.Kingaku, 0, beInputSYK.InputSYKH.KingakuHasu)
        needCalcZei = True
      End If
      If KonCalc.CountSyosuKeta(beInputSYKD.Genka) > 0 Then
        beInputSYKD.Genka = KonCalc.Round(beInputSYKD.Genka, 0, seikyusaki.KingakuHasu)
        needCalcZei = True
      End If
      If KonCalc.CountSyosuKeta(beInputSYKD.BaikaKingaku) > 0 Then
        beInputSYKD.BaikaKingaku = KonCalc.Round(beInputSYKD.BaikaKingaku, 0, beInputSYK.InputSYKH.KingakuHasu)
        needCalcZei = True
      End If

      If needCalcZei Then
        '各金額に変更があったら明細を再計算
        Dim sotoZeigaku As Decimal = beInputSYKD.SotoZeigaku
        Dim uchiZeigaku As Decimal = beInputSYKD.UchiZeigaku
        '消費税額
        KonCalc.CalcMeisaiZei(beInputSYK.InputSYKH.SyohizeiHasu, beInputSYK.InputSYKH.SyohizeiTsuchi, beInputSYKD.ZeiRitsu, beInputSYKD.ZeikomiKubun, beInputSYKD.Kingaku, _
                              sotoZeigaku, uchiZeigaku, _BEKihonJoho.ZeiHasuKurai)
        beInputSYKD.SotoZeigaku = sotoZeigaku
        beInputSYKD.UchiZeigaku = uchiZeigaku

        '粗利益
        beInputSYKD.Ararieki = KonCalc.CalcAraRieki(beInputSYKD.Genka, beInputSYKD.Kingaku, beInputSYKD.UchiZeigaku, beInputSYKD.MasterKubun, beInputSYKD.ZeiRitsu, _
                                                    CType(CShort(beInputSYK.InputSYKH.SyohizeiTsuchi), SyohizeiTsuchiType), beInputSYKD.ZeikomiKubun, _BEKihonJoho.ZeiHasuKurai, beInputSYK.InputSYKH.SyohizeiHasu)

        '合計も再計算
        needCalcTotal = True
      End If

      If beInputSYKD.JuchuHeaderId <> 0 _
      AndAlso beInputSYKD.SyukaShiji = PCA.TSC.Kon.BusinessEntity.Defines.RendoShijiType.Teisei _
      AndAlso beInputSYKD.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan _
      AndAlso beInputSYKD.Suryo = 0 Then
        '受注連動で訂正出荷で数量がゼロの場合は未出荷にする
        beInputSYKD.SyukaShiji = PCA.TSC.Kon.BusinessEntity.Defines.RendoShijiType.Mi
      End If

      Dim meisaiState As PEKonInput.MeisaiStateType _
          = KonCalc.JudgeSykdState(beInputSYKD.SyohinCode, beInputSYKD.JuchuHeaderId, beInputSYKD.SyukaShiji, beInputSYKD.MasterKubun, beInputSYKD.Suryo)
      If meisaiState = PEKonInput.MeisaiStateType.Valid _
      OrElse meisaiState = PEKonInput.MeisaiStateType.RendoInvalid Then
        '有効明細と受注からの未出荷明細のみ追加
        addEntries.Add(beInputSYKD)
      End If

    Next

    beInputSYK.InputSYKDList.BEInputSYKD.Clear()
    beInputSYK.InputSYKDList.BEInputSYKD.AddRange(addEntries)

    Dim beMasterYms As BEMasterYms = MyBase.FindBEMasterYms(beInputSYK.InputSYKH.ChokusosakiCode)

    If needCalcTotal Then
      '必要がある時、合計を再計算

      Dim curTotal As New PEKonInput()

      '再計算した明細のある伝票エンティティをクローン化
      curTotal = ConvertKonInput.SYKToInput(CType(beInputSYK.Clone(), BusinessEntity.BEInputSYK), tokuisaki, seikyusaki, beMasterYms)

      Me.CalcTotal(curTotal, False, Tools.PEKonInput.DenpyoType.Uriage)

      beInputSYK.InputSYKT.KingakuGokei = curTotal.Total.KingakuGokei
      beInputSYK.InputSYKT.SotoZeiGokei = curTotal.Total.SotoZeiGokei
      beInputSYK.InputSYKT.UchiZeiGokei = curTotal.Total.UchiZeiGokei
      beInputSYK.InputSYKT.Hasu = curTotal.Total.Hasu

      '税率別合計
      Dim oldInputSYKZList As New List(Of BEInputSykNykZ)
      oldInputSYKZList.AddRange(CType(beInputSYK.InputSYKZList, Global.System.Collections.Generic.IEnumerable(Of Global.PCA.TSC.Kon.BusinessEntity.BEInputSykNykZ)))

      Dim addInputSYK As New BEInputSYK()
      addInputSYK = ConvertKonInput.InputToSYK(curTotal)

      beInputSYK.InputSYKZList.BEInputSYKZ.Clear()
      beInputSYK.InputSYKZList.BEInputSYKZ.AddRange(CType(addInputSYK.InputSYKZList, Global.System.Collections.Generic.IEnumerable(Of Global.PCA.TSC.Kon.BusinessEntity.BEInputSykNykZ)))

      For Each old As BEInputSykNykZ In oldInputSYKZList
        Dim oldInputSYKZ As BEInputSykNykZ = old    '警告回避

        If addInputSYK.InputSYKZList.BEInputSYKZ.Exists(Function(match) _
            match.ZeiRitsu = oldInputSYKZ.ZeiRitsu) = False AndAlso (old.Hosei <> 0 OrElse old.HoseiHizuke <> 0) Then
          '税率の適用が無くなっても、補正済は旧伝票から移す
          old.KingakuGokei = 0
          old.SotoZeiGokei = 0
          old.UchiZeiGokei = 0
          old.Hasu = 0

          beInputSYK.InputSYKZList.BEInputSYKZ.Add(old)

        End If
      Next
    End If

  End Sub

#End Region

#Region "伝票コマンド（取得）"

  ''' <summary>伝票を再取得。
  ''' </summary>
  ''' <param name="entity">伝票入力エンティティ</param>
  ''' <param name="forUpdate">更新するためにロックを実行するならtrue。それ以外はfalse。</param>
  ''' <returns>再取得に成功したときTrueを返します。</returns>
  ''' <remarks></remarks>
  Public Function ReloadDenpyo(ByVal entity As PEKonInput, ByVal forUpdate As Boolean) As Boolean

    ' 伝票を取得していない場合は無視する
    If entity.Header.Id = 0 Then
      Return True
    End If

    ' 対象の伝票があった場合には現在ロックされている伝票を解除する
    Me.UnLockCore()

    Me.ModifiableEntity = False

    '伝票の取得
    'Dim beInputSYK As BEInputSYK = Me.FindBEInputSYK(entity.Header.Id)
    Dim beInputSYK As BEInputSYK = Me.GetBEInputSYK(entity.Header.Id)
    If IsNothing(beInputSYK) Then
      Return False
    End If

    '伝票をロック
    Dim errorCode As String = String.Empty
    Dim lockResult As Boolean = Me.LockInputSYK(entity.Header.Id, errorCode)
    If lockResult = False Then
      '失敗
      If errorCode = "BCH41924002" Then
        'ロック済
        entity.State = StateType.Locked
      ElseIf errorCode = "BCH41924003" Then
        '削除済
        entity.State = StateType.Sakujozumi
      Else
        entity.State = StateType.CanUpd
      End If

      Me.UpdateActiveEntity(entity)

      Return False
    End If

    Dim tokuisaki As BEMasterTms = MyBase.FindBEMasterTms(beInputSYK.InputSYKH.TokuisakiCode)
    Dim beMasterYms As BEMasterYms = MyBase.FindBEMasterYms(beInputSYK.InputSYKH.ChokusosakiCode)
    Dim seikyusaki As BEMasterTms = MyBase.FindSeikyusaki(tokuisaki)
    '伝票を伝票入力エンティティに設定
    Me.UpdateActiveEntity(ConvertKonInput.SYKToInput(beInputSYK, tokuisaki, seikyusaki, beMasterYms))

    Me.ModifiableEntity = True
    '修正可否を判定
    If beInputSYK.State = PCA.TSC.Kon.BusinessEntity.Defines.StateType.CanUpd _
    OrElse (beInputSYK.State = PCA.TSC.Kon.BusinessEntity.Defines.StateType.Seizumi AndAlso Me.BEKihonJoho.CantUpdSei = PCA.TSC.Kon.BusinessEntity.Defines.CantUpdType.Can) _
    OrElse (beInputSYK.State = PCA.TSC.Kon.BusinessEntity.Defines.StateType.ZaikoShimezumi AndAlso Me.BEKihonJoho.CantUpdZaikoShimekiri = PCA.TSC.Kon.BusinessEntity.Defines.CantUpdType.Can) _
    OrElse (beInputSYK.State = PCA.TSC.Kon.BusinessEntity.Defines.StateType.SeiZaikoShimezumi AndAlso Me.BEKihonJoho.CantUpdSei = PCA.TSC.Kon.BusinessEntity.Defines.CantUpdType.Can _
            AndAlso Me.BEKihonJoho.CantUpdZaikoShimekiri = PCA.TSC.Kon.BusinessEntity.Defines.CantUpdType.Can) Then
      '伝票が更新可能か、請求締切・在庫締切済でも会社基本上、更新が可能な時
      Me.ModifiableEntity = True
    Else
      Me.ModifiableEntity = False
    End If

    Return True

  End Function


  ''' <summary>
  ''' ロック解除（連動受注含む）を行います。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub UnLockCore()

    ' 受注連動していたら受注を解除
    UnLockInputJUC()

    UnLockInputSYK()

  End Sub

  ''' <summary>
  ''' ロック解除（受注のみ）を行います。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub UnLockJuchu()
    UnLockInputJUC()
  End Sub

#End Region

#Region "見積・受注連動"

  ''' <summary>
  ''' 見積伝票等の情報を伝票入力の統一エンティティクラスに設定します。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="beInputMIT">見積伝票</param>
  ''' <param name="tokuisakiCode">得意先コード。得意先を設定しない場合は空文字列を渡してください。</param>
  ''' <remarks></remarks>
  Public Sub PrepareItemsFromMitsumori(ByRef peKonInput As PEKonInput, ByVal beInputMIT As BEInputMIT, ByVal tokuisakiCode As String)
    '
    Dim tokuisaki As New BEMasterTms
    Dim seikyusaki As New BEMasterTms

    'Id
    'Denku
    'DenpypHizuke
    'Hizuke
    'DenpyoNo
    '得意先項目
    If tokuisakiCode.Length = 0 Then
      '入力済み得意先と不一致か、未入力の時
      peKonInput.Header.TorihikisakiCode = beInputMIT.InputMITH.TokuisakiCode
      peKonInput.Header.AitesakiId = beInputMIT.InputMITH.AitesakiId
      peKonInput.Header.TorihikisakiMei1 = beInputMIT.InputMITH.TokuisakiMei1
      peKonInput.Header.TorihikisakiMei2 = beInputMIT.InputMITH.TokuisakiMei2
      peKonInput.Header.Jyusyo1 = beInputMIT.InputMITH.Jyusyo1
      peKonInput.Header.Jyusyo2 = beInputMIT.InputMITH.Jyusyo2
      peKonInput.Header.YubinBango = beInputMIT.InputMITH.YubinBango
      peKonInput.Header.TelNo = beInputMIT.InputMITH.TelNo
      peKonInput.Header.FAXNo = beInputMIT.InputMITH.FAXNo
      peKonInput.Header.Keisyo = beInputMIT.InputMITH.Keisyo
      peKonInput.Header.MailAddress = beInputMIT.InputMITH.MailAddress
      peKonInput.Header.TekiyoBaikaNo = beInputMIT.InputMITH.TekiyoBaikaNo
      peKonInput.Header.Kakeritsu = beInputMIT.InputMITH.Kakeritsu
      peKonInput.Header.ZeiKansan = beInputMIT.InputMITH.ZeiKansan
      peKonInput.Header.SyohizeiTsuchiKon = beInputMIT.InputMITH.SyohizeiTsuchi
      peKonInput.Header.KingakuHasu = beInputMIT.InputMITH.KingakuHasu
      peKonInput.Header.SyohizeiHasu = beInputMIT.InputMITH.SyohizeiHasu
      peKonInput.Header.BaiTankaKansan = beInputMIT.InputMITH.BaiTankaKansan
      '得意先を取得
      tokuisaki = MyBase.FindBEMasterTms(peKonInput.Header.TorihikisakiCode)
      peKonInput.Header.SyatenCode = tokuisaki.SyatenCode     '社店コード
      '
      peKonInput.Header.BunruiCode = tokuisaki.BunruiCode     '分類コード
      peKonInput.Header.DenpyoKubun = tokuisaki.DenpyoKubun   '伝票区分
      peKonInput.Header.StoreTorihikisakiCode = beInputMIT.InputMITH.StoreTorihikisakiCode
      '
      peKonInput.Header.SeisakiCode = beInputMIT.InputMITH.SeikyusakiCode
      '
      peKonInput.Header.SpotAitesakiId = beInputMIT.InputMITH.SpotAitesakiId
      '請求先を取得
      seikyusaki = MyBase.FindSeikyusaki(tokuisaki)
      peKonInput.Header.YoshinGendogaku = seikyusaki.YoshinGendogaku
      peKonInput.Header.SeiKikanTo = seikyusaki.SeikyuKikanTo
      peKonInput.Header.SeiShimebi = seikyusaki.SeikyuShimebi
      peKonInput.Header.MisyuZandaka = KonCalc.CalcMisyuZandaka(seikyusaki)
      '
      peKonInput.Header.GenkaHasu = tokuisaki.KingakuHasu         '原価端数
      peKonInput.Header.TorihikisakiComment = tokuisaki.Comment   'コメント
    End If
    '
    peKonInput.Header.MitsumoriJuchuKubun = MitsumoriJuchuKubunType.Mitsumori
    peKonInput.Header.RendoDenpyoNo = beInputMIT.InputMITH.MitsumoriNo
    peKonInput.Header.SenpoTantosyaId = beInputMIT.InputMITH.SenpoTantosyaId        '先方担当者
    peKonInput.Header.SenpoTantosyaMei = beInputMIT.InputMITH.SenpoTantosyaMei
    peKonInput.Header.BumonCode = beInputMIT.InputMITH.BumonCode                    '部門
    peKonInput.Header.BumonMei = beInputMIT.InputMITH.BumonMei
    peKonInput.Header.BumonSoko = beInputMIT.InputMITH.BumonSoko
    peKonInput.Header.TantosyaCode = beInputMIT.InputMITH.TantosyaCode              '担当者
    peKonInput.Header.TantosyaMei = beInputMIT.InputMITH.TantosyaMei
    peKonInput.Header.TekiyoCode = String.Empty                                     '摘要
    peKonInput.Header.Tekiyo = String.Empty
    peKonInput.Header.ProCode = beInputMIT.InputMITH.ProCode                        'プロジェクト
    peKonInput.Header.ProMei = beInputMIT.InputMITH.ProMei
    'DenpyoNo2
    peKonInput.Header.YobiShort1 = beInputMIT.InputMITH.YobiShort1
    peKonInput.Header.YobiShort2 = beInputMIT.InputMITH.YobiShort2
    peKonInput.Header.YobiShort3 = beInputMIT.InputMITH.YobiShort3
    peKonInput.Header.YobiInt1 = beInputMIT.InputMITH.YobiInt1
    peKonInput.Header.YobiInt2 = beInputMIT.InputMITH.YobiInt2
    'YobiInt3
    peKonInput.Header.YobiDecimal1 = beInputMIT.InputMITH.YobiDecimal1
    peKonInput.Header.YobiDecimal2 = beInputMIT.InputMITH.YobiDecimal2
    peKonInput.Header.YobiDecimal3 = beInputMIT.InputMITH.YobiDecimal3
    peKonInput.Header.YobiString1 = beInputMIT.InputMITH.YobiString1
    peKonInput.Header.YobiString2 = beInputMIT.InputMITH.YobiString2
    peKonInput.Header.YobiString3 = beInputMIT.InputMITH.YobiString3

    '明細
    peKonInput.DetailList.Clear()
    Dim detail As PEKonInputDetail

    For Each inputMITD As BEInputMITD In beInputMIT.InputMITDList.BEInputMITD

      detail = New PEKonInputDetail()

      'detail.Sequence
      'detail.Edaban
      'detail.State
      detail.MeisaiState = peKonInput.MeisaiStateType.Invalid
      detail.SyohinCode = inputMITD.SyohinCode
      detail.MasterKubun = inputMITD.MasterKubun
      detail.ZeiKubun = inputMITD.ZeiKubun
      detail.ZeikomiKubun = inputMITD.ZeikomiKubun
      detail.TankaKeta = inputMITD.TankaKeta
      detail.SuryoKeta = inputMITD.SuryoKeta
      detail.SyohinMei = inputMITD.SyohinMei
      detail.KikakuKataban = inputMITD.KikakuKataban
      detail.Color = inputMITD.Color
      detail.Size = inputMITD.Size
      detail.SokoCode = inputMITD.SokoCode
      detail.Ku = KuType.Tsujo    '区
      detail.Irisu = inputMITD.Irisu
      detail.Hakosu = inputMITD.Hakosu
      detail.Suryo = inputMITD.Suryo
      detail.Tani = inputMITD.Tani
      detail.Tanka = inputMITD.Tanka
      detail.GenTanka = inputMITD.GenTanka
      detail.BaiTanka = inputMITD.BaiTanka
      detail.Kingaku = inputMITD.Kingaku
      detail.Genka = inputMITD.Genka
      detail.BaikaKingaku = inputMITD.BaikaKingaku
      detail.Ararieki = inputMITD.Ararieki
      detail.RiekiRitsu = inputMITD.RiekiRitsu
      '入荷マーク
      If peKonInput.Header.Denku <> DenkuType.Keiyaku _
      AndAlso detail.MasterKubun = MasterKubunType.Ippan Then
        detail.KanMark = NyukaMarkType.Suru
      Else
        detail.KanMark = NyukaMarkType.Shinai
      End If
      detail.Biko = inputMITD.Biko
      detail.HyojunKakaku2 = inputMITD.HyojunKakaku2
      detail.HyojunKakaku = inputMITD.HyojunKakaku
      detail.ZeiRitsu = inputMITD.ZeiRitsu
      detail.SotoZeigaku = inputMITD.SotoZeigaku
      detail.UchiZeigaku = inputMITD.UchiZeigaku
      detail.SyokonKeisan = inputMITD.SyokonKeisan
      detail.SyohinKomoku1 = inputMITD.SyohinKomoku1
      detail.SyohinKomoku2 = inputMITD.SyohinKomoku2
      detail.SyohinKomoku3 = inputMITD.SyohinKomoku3
      detail.DenpyoKomoku1 = inputMITD.UriageKomoku1
      detail.DenpyoKomoku2 = inputMITD.UriageKomoku2
      detail.DenpyoKomoku3 = inputMITD.UriageKomoku3
      'RendoShiji
      'RendoHeaderId
      'RendoSequence
      detail.YobiShort1 = inputMITD.YobiShort1
      detail.YobiShort2 = inputMITD.YobiShort2
      detail.YobiShort3 = inputMITD.YobiShort3
      detail.YobiInt1 = inputMITD.YobiInt1
      detail.YobiInt2 = inputMITD.YobiInt2
      detail.YobiInt3 = inputMITD.YobiInt3
      detail.YobiDecimal1 = inputMITD.YobiDecimal1
      detail.YobiDecimal2 = inputMITD.YobiDecimal2
      detail.YobiDecimal3 = inputMITD.YobiDecimal3
      detail.YobiString1 = inputMITD.YobiString1
      detail.YobiString2 = inputMITD.YobiString2
      detail.YobiString3 = inputMITD.YobiString3
      detail.IrisuKeta = inputMITD.IrisuKeta
      detail.HakosuKeta = inputMITD.HakosuKeta
      detail.SuryoHasu = inputMITD.SuryoHasu
      '原価端数
      detail.GenkaHasu = peKonInput.Header.GenkaHasu

      peKonInput.DetailList.Add(detail)
    Next

    '合計
    peKonInput.Total.HeaderId = beInputMIT.InputMITT.HeaderId
    peKonInput.Total.KingakuGokei = beInputMIT.InputMITT.KingakuGokei
    peKonInput.Total.SotoZeiGokei = beInputMIT.InputMITT.SotoZeiGokei
    peKonInput.Total.UchiZeiGokei = beInputMIT.InputMITT.UchiZeiGokei

    '税率別合計
    Dim subtotal As New PEKonInputSubtotal()
    For Each inputMITZ As BEInputMitJucHacZ In beInputMIT.InputMITZList.BEInputMITZ
      subtotal = New PEKonInputSubtotal() With { _
          .HeaderId = inputMITZ.HeaderId, _
          .ZeiRitsu = inputMITZ.ZeiRitsu, _
          .KingakuGokei = inputMITZ.KingakuGokei, _
          .SotoZeiGokei = inputMITZ.SotoZeiGokei, _
          .UchiZeiGokei = inputMITZ.UchiZeiGokei _
      }
      peKonInput.SubtotalList.Add(subtotal)
    Next

    '合計を再計算
    Me.CalcTotal(peKonInput, True, Tools.PEKonInput.DenpyoType.Uriage)

  End Sub

  ''' <summary>
  ''' 受注伝票等の情報を伝票入力の統一エンティティクラスに設定します。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="beInputJUC">受注伝票</param>
  ''' <param name="tokuisakiCode">得意先コード。得意先を設定しない場合は空文字列を渡してください。</param>
  ''' <remarks></remarks>
  Public Sub PrepareRendoFromJuchu(ByVal peKonInput As PEKonInput, ByVal beInputJUC As BEInputJUC, ByVal tokuisakiCode As String)
    '
    Dim tokuisaki As New BEMasterTms
    Dim seikyusaki As New BEMasterTms
    Dim beMasterYms As New BEMasterYms

    'Id
    'Denku
    'DenpypHizuke
    'Hizuke
    'DenpyoNo
    '得意先項目
    '得意先情報
    If tokuisakiCode.Length = 0 Then
      '入力済み得意先と不一致か、未入力の時
      peKonInput.Header.TorihikisakiCode = beInputJUC.InputJUCH.TokuisakiCode
      peKonInput.Header.AitesakiId = beInputJUC.InputJUCH.AitesakiId
      peKonInput.Header.TorihikisakiMei1 = beInputJUC.InputJUCH.TokuisakiMei1
      peKonInput.Header.TorihikisakiMei2 = beInputJUC.InputJUCH.TokuisakiMei2
      peKonInput.Header.Jyusyo1 = beInputJUC.InputJUCH.Jyusyo1
      peKonInput.Header.Jyusyo2 = beInputJUC.InputJUCH.Jyusyo2
      peKonInput.Header.YubinBango = beInputJUC.InputJUCH.YubinBango
      peKonInput.Header.TelNo = beInputJUC.InputJUCH.TelNo
      peKonInput.Header.FAXNo = beInputJUC.InputJUCH.FAXNo
      peKonInput.Header.Keisyo = beInputJUC.InputJUCH.Keisyo
      peKonInput.Header.MailAddress = beInputJUC.InputJUCH.MailAddress
      peKonInput.Header.TekiyoBaikaNo = beInputJUC.InputJUCH.TekiyoBaikaNo
      peKonInput.Header.Kakeritsu = beInputJUC.InputJUCH.Kakeritsu
      peKonInput.Header.ZeiKansan = beInputJUC.InputJUCH.ZeiKansan
      peKonInput.Header.SyohizeiTsuchiKon = beInputJUC.InputJUCH.SyohizeiTsuchi
      peKonInput.Header.KingakuHasu = beInputJUC.InputJUCH.KingakuHasu
      peKonInput.Header.SyohizeiHasu = beInputJUC.InputJUCH.SyohizeiHasu
      peKonInput.Header.BaiTankaKansan = CType(beInputJUC.InputJUCH.BaiTankaKansan, BaitankaKansanType)
      '得意先を取得
      tokuisaki = MyBase.FindBEMasterTms(peKonInput.Header.TorihikisakiCode)
      peKonInput.Header.SyatenCode = tokuisaki.SyatenCode     '社店コード
      '
      peKonInput.Header.BunruiCode = tokuisaki.BunruiCode     '分類コード
      peKonInput.Header.DenpyoKubun = tokuisaki.DenpyoKubun   '伝票区分
      peKonInput.Header.StoreTorihikisakiCode = beInputJUC.InputJUCH.StoreTorihikisakiCode
      '
      peKonInput.Header.SeisakiCode = beInputJUC.InputJUCH.SeikyusakiCode
      '
      peKonInput.Header.SpotAitesakiId = beInputJUC.InputJUCH.SpotAitesakiId
      If peKonInput.Header.TorihikisakiCode <> peKonInput.Header.SeisakiCode Then
        '得意先を設定
        seikyusaki = tokuisaki
      Else
        '請求先を取得
        seikyusaki = MyBase.FindBEMasterTms(peKonInput.Header.SeisakiCode)
      End If
      peKonInput.Header.YoshinGendogaku = seikyusaki.YoshinGendogaku
      peKonInput.Header.SeiKikanTo = seikyusaki.SeikyuKikanTo
      peKonInput.Header.SeiShimebi = seikyusaki.SeikyuShimebi
      peKonInput.Header.MisyuZandaka = KonCalc.CalcMisyuZandaka(seikyusaki)
      '
      peKonInput.Header.GenkaHasu = tokuisaki.KingakuHasu         '原価端数
      peKonInput.Header.TorihikisakiComment = tokuisaki.Comment   'コメント
    End If

    peKonInput.Header.MitsumoriJuchuKubun = MitsumoriJuchuKubunType.Juchu
    peKonInput.Header.RendoDenpyoNo = beInputJUC.InputJUCH.JuchuNo
    peKonInput.Header.SenpoTantosyaId = beInputJUC.InputJUCH.SenpoTantosyaId        '先方担当者
    peKonInput.Header.SenpoTantosyaMei = beInputJUC.InputJUCH.SenpoTantosyaMei
    peKonInput.Header.BumonCode = beInputJUC.InputJUCH.BumonCode                    '部門
    peKonInput.Header.BumonMei = beInputJUC.InputJUCH.BumonMei
    peKonInput.Header.BumonSoko = beInputJUC.InputJUCH.BumonSoko
    peKonInput.Header.TantosyaCode = beInputJUC.InputJUCH.TantosyaCode              '担当者
    peKonInput.Header.TantosyaMei = beInputJUC.InputJUCH.TantosyaMei
    peKonInput.Header.TekiyoCode = beInputJUC.InputJUCH.TekiyoCode                  '摘要
    peKonInput.Header.Tekiyo = beInputJUC.InputJUCH.Tekiyo
    peKonInput.Header.ProCode = beInputJUC.InputJUCH.ProCode                        'プロジェクト
    peKonInput.Header.ProMei = beInputJUC.InputJUCH.ProMei

    '直送先
    If beInputJUC.InputJUCH.ChokusosakiCode.Length <> 0 Then
      beMasterYms = MyBase.FindBEMasterYms(beInputJUC.InputJUCH.ChokusosakiCode)
      peKonInput.Header.ChokusosakiCode = beInputJUC.InputJUCH.ChokusosakiCode
      peKonInput.Header.ChokusosakiMei1 = beMasterYms.ChokusosakiMei1
      peKonInput.Header.ChokusosakiMei2 = beMasterYms.ChokusosakiMei2
      peKonInput.Header.ChokusosakiTantosyaId = beMasterYms.SenpoTantosyaId
      peKonInput.Header.ChokusosakiTantosyaMei = beMasterYms.SenpoTantosyaMei
      peKonInput.Header.ChokusosakiComment = beMasterYms.Comment
    Else
      peKonInput.Header.ChokusosakiCode = String.Empty
      peKonInput.Header.ChokusosakiMei1 = String.Empty
      peKonInput.Header.ChokusosakiMei2 = String.Empty
      peKonInput.Header.ChokusosakiFlag = ChokusosakiFlagType.Chokusosaki
      peKonInput.Header.ChokusosakiTantosyaId = 0
      peKonInput.Header.ChokusosakiTantosyaMei = String.Empty
      peKonInput.Header.ChokusosakiComment = String.Empty
    End If
    'DenpyoNo2
    peKonInput.Header.YobiShort1 = beInputJUC.InputJUCH.YobiShort1
    peKonInput.Header.YobiShort2 = beInputJUC.InputJUCH.YobiShort2
    peKonInput.Header.YobiShort3 = beInputJUC.InputJUCH.YobiShort3
    peKonInput.Header.YobiInt1 = beInputJUC.InputJUCH.YobiInt1
    peKonInput.Header.YobiInt2 = beInputJUC.InputJUCH.YobiInt2
    'YobiInt3
    peKonInput.Header.YobiDecimal1 = beInputJUC.InputJUCH.YobiDecimal1
    peKonInput.Header.YobiDecimal2 = beInputJUC.InputJUCH.YobiDecimal2
    peKonInput.Header.YobiDecimal3 = beInputJUC.InputJUCH.YobiDecimal3
    peKonInput.Header.YobiString1 = beInputJUC.InputJUCH.YobiString1
    peKonInput.Header.YobiString2 = beInputJUC.InputJUCH.YobiString2
    peKonInput.Header.YobiString3 = beInputJUC.InputJUCH.YobiString3

    '明細
    peKonInput.DetailList.Clear()
    Dim detail As PEKonInputDetail

    For Each inputJUCD As BEInputJUCD In beInputJUC.InputJUCDList.BEInputJUCD
      If inputJUCD.SyukazumiFlag = SyukazumiFlagType.Syuka Then
        '出荷済み明細
        Continue For
      End If

      detail = New PEKonInputDetail()

      'detail.Sequence = 0			'作成時は自動設定されます。
      'detail.Edaban = 0				'作成時は自動設定されます。
      'detail.State = 0
      detail.MeisaiState = peKonInput.MeisaiStateType.Invalid
      detail.SyohinCode = inputJUCD.SyohinCode
      detail.MasterKubun = inputJUCD.MasterKubun
      detail.ZeiKubun = inputJUCD.ZeiKubun
      detail.ZeikomiKubun = inputJUCD.ZeikomiKubun
      detail.TankaKeta = inputJUCD.TankaKeta
      detail.SuryoKeta = inputJUCD.SuryoKeta
      detail.SyohinMei = inputJUCD.SyohinMei
      detail.KikakuKataban = inputJUCD.KikakuKataban
      detail.Color = inputJUCD.Color
      detail.Size = inputJUCD.Size
      detail.SokoCode = inputJUCD.SokoCode
      detail.Ku = KuType.Tsujo    '区
      detail.Irisu = inputJUCD.Irisu
      detail.Hakosu = inputJUCD.Hakosu
      '売上数量＝受注数量－出荷累計
      detail.Suryo = inputJUCD.Suryo - inputJUCD.SyukaRuikei
      detail.Tani = inputJUCD.Tani
      detail.Tanka = inputJUCD.Tanka
      detail.GenTanka = inputJUCD.GenTanka
      detail.BaiTanka = inputJUCD.BaiTanka
      detail.Kingaku = inputJUCD.Kingaku
      detail.Genka = inputJUCD.Genka
      detail.BaikaKingaku = inputJUCD.BaikaKingaku
      detail.Ararieki = inputJUCD.Ararieki
      detail.RiekiRitsu = inputJUCD.RiekiRitsu
      detail.Biko = inputJUCD.Biko
      '入荷マーク
      If peKonInput.Header.Denku <> DenkuType.Keiyaku _
       AndAlso detail.MasterKubun = MasterKubunType.Ippan Then
        detail.KanMark = NyukaMarkType.Suru
      Else
        detail.KanMark = NyukaMarkType.Shinai
      End If
      detail.HyojunKakaku2 = inputJUCD.HyojunKakaku2
      detail.HyojunKakaku = inputJUCD.HyojunKakaku
      detail.ZeiRitsu = inputJUCD.ZeiRitsu
      detail.SotoZeigaku = inputJUCD.SotoZeigaku
      detail.UchiZeigaku = inputJUCD.UchiZeigaku
      detail.SyokonKeisan = inputJUCD.SyokonKeisan
      detail.SyohinKomoku1 = inputJUCD.SyohinKomoku1
      detail.SyohinKomoku2 = inputJUCD.SyohinKomoku2
      detail.SyohinKomoku3 = inputJUCD.SyohinKomoku3
      detail.DenpyoKomoku1 = inputJUCD.UriageKomoku1
      detail.DenpyoKomoku2 = inputJUCD.UriageKomoku2
      detail.DenpyoKomoku3 = inputJUCD.UriageKomoku3
      detail.RendoHeaderId = beInputJUC.InputJUCH.Id      '連動ヘッダーID
      detail.RendoSequence = inputJUCD.Sequence           '連動SEQ
      detail.RendoShiji = PCA.TSC.Kon.BusinessEntity.Defines.RendoShijiType.Zensu    '連動指示
      detail.YobiShort1 = inputJUCD.YobiShort1
      detail.YobiShort2 = inputJUCD.YobiShort2
      detail.YobiShort3 = inputJUCD.YobiShort3
      detail.YobiInt1 = inputJUCD.YobiInt1
      detail.YobiInt2 = inputJUCD.YobiInt2
      detail.YobiInt3 = inputJUCD.YobiInt3
      detail.YobiDecimal1 = inputJUCD.YobiDecimal1
      detail.YobiDecimal2 = inputJUCD.YobiDecimal2
      detail.YobiDecimal3 = inputJUCD.YobiDecimal3
      detail.YobiString1 = inputJUCD.YobiString1
      detail.YobiString2 = inputJUCD.YobiString2
      detail.YobiString3 = inputJUCD.YobiString3
      detail.IrisuKeta = inputJUCD.IrisuKeta
      detail.HakosuKeta = inputJUCD.HakosuKeta
      detail.SuryoHasu = inputJUCD.SuryoHasu
      '原価端数
      detail.GenkaHasu = peKonInput.Header.KingakuHasu

      peKonInput.DetailList.Add(detail)

    Next

    ' 合計
    peKonInput.Total.HeaderId = beInputJUC.InputJUCT.HeaderId
    peKonInput.Total.KingakuGokei = beInputJUC.InputJUCT.KingakuGokei
    peKonInput.Total.SotoZeiGokei = beInputJUC.InputJUCT.SotoZeiGokei
    peKonInput.Total.UchiZeiGokei = beInputJUC.InputJUCT.UchiZeiGokei

    ' 税率別合計
    Dim subtotal As New PEKonInputSubtotal()

    For Each inputJUCZ As BEInputMitJucHacZ In beInputJUC.InputJUCZList.InputJUCZ
      subtotal = New PEKonInputSubtotal() With { _
          .HeaderId = inputJUCZ.HeaderId, _
          .ZeiRitsu = inputJUCZ.ZeiRitsu, _
          .KingakuGokei = inputJUCZ.KingakuGokei, _
          .SotoZeiGokei = inputJUCZ.SotoZeiGokei, _
          .UchiZeiGokei = inputJUCZ.UchiZeiGokei _
      }
      peKonInput.SubtotalList.Add(subtotal)
    Next

    '合計を再計算
    'Me.CalcTotal(peKonInput, True, Tools.PEKonInput.DenpyoType.Uriage)
    MyBase.ReCalcDenpyo(peKonInput, True)

  End Sub

  ''' <summary>見積／受注Noから見積／受注伝票を取得し、伝票入力エンティティクラスに設定
  ''' </summary>
  ''' <param name="mitsumoriJuchuNo">見積／受注No</param>
  ''' <param name="peKonInput">セットする伝票</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="sameTokuisaki">入力済得意先コードと見積・受注伝票の得意先コードが不一致の時はFalse。</param>
  Public Function GetLinkMitJucDenpyo(ByVal mitsumoriJuchuNo As Integer, ByRef peKonInput As PEKonInput, ByVal needMessageBox As Boolean, ByRef sameTokuisaki As Boolean) As Boolean
    '
    If mitsumoriJuchuNo = 0 Then
      '見積／受注No未入力時
      Return True
    End If

    '得意先の変更確認の為に退避
    Dim oldTokuisakiCode As String = peKonInput.Header.TorihikisakiCode
    '得意先が同一か否か
    sameTokuisaki = True

    If peKonInput.Header.MitsumoriJuchuKubun = MitsumoriJuchuKubunType.Mitsumori Then
      '見積伝票を取得
      Dim beInputMIT As BEInputMIT = MyBase.FindByMitsumoriNo(mitsumoriJuchuNo, True)
      If beInputMIT.InputMITH.Id = 0 Then
        Return False
      End If

      If Not String.IsNullOrEmpty(oldTokuisakiCode) _
      AndAlso oldTokuisakiCode <> beInputMIT.InputMITH.TokuisakiCode Then
        '得意先コード入力済みで、かつ得意先コードが不一致の時
        sameTokuisaki = False
      End If
      '伝票入力エンティティに設定
      If sameTokuisaki Then
        Me.PrepareItemsFromMitsumori(peKonInput, beInputMIT, peKonInput.Header.TorihikisakiCode)
      Else
        Me.PrepareItemsFromMitsumori(peKonInput, beInputMIT, String.Empty)
      End If

    Else
      '受注伝票を取得
      Dim beInputJUC As BEInputJUC = MyBase.FindByJuchuNo(mitsumoriJuchuNo, True)
      If beInputJUC.InputJUCH.Id = 0 Then
        Return False
      End If

      ''◆20150519 ktsuda 取得時、受注伝票にロックをかける

      If IsNothing(beInputJUC.InputJUCDList.BEInputJUCD.Find(Function(match) _
                  (match.SyohinCode <> String.Empty AndAlso match.SyukazumiFlag = SyukazumiFlagType.Misyuka))) Then
        '未出荷の受注明細が存在しない時
        ShowMessage.OnExclamation(CType(Me, IWin32Window), _
                                  String.Format(MessageDefines.Exclamation.CantRendo, BusinessEntity.ItemText.Juchu, BusinessEntity.ItemText.Syuka), _
                                  CommonVariable.BasicConfig.ProgramName)
        Return False
      End If

      If Not String.IsNullOrEmpty(oldTokuisakiCode) _
      AndAlso oldTokuisakiCode <> beInputJUC.InputJUCH.TokuisakiCode Then
        '得意先コード入力済みで、かつ得意先コードが不一致の時
        sameTokuisaki = False
      End If

      '伝票入力の統一エンティティクラスを設定
      If sameTokuisaki Then
        Me.PrepareRendoFromJuchu(peKonInput, beInputJUC, peKonInput.Header.TorihikisakiCode)
      Else
        Me.PrepareRendoFromJuchu(peKonInput, beInputJUC, String.Empty)
      End If

    End If

    'ローカルで保持する明細データコレクションを初期化
    Dim limit As Integer = InputDefines.MaximumDataCount - peKonInput.DetailList.Count
    For i As Integer = 0 To limit - 1
      peKonInput.DetailList.Add(New PEKonInputDetail())
    Next
    'Me.ModifiedEntity = peKonInput.Clone()

    Return True

  End Function

  ''' <summary>
  ''' 見積／受注Noがクリアされた場合の処理を行います
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <remarks></remarks>
  Public Sub ClearMitsumoriJuchuNo(ByVal peKonInput As PEKonInput)
    'Public Sub ClearedMitsumoriJuchuNo(ByVal peKonInput As PEKonInput)
    ' ロックを解除する
    Me.UnLockJuchu()

    peKonInput.Header.RendoDenpyoNo = 0

    ' スポット得意先を連動させていたらクリアする
    If peKonInput.Header.TorihikisakiCode = MakeText.MakeCommonCode(Me.BEKihonJoho.TokuisakiKeta) Then
      If peKonInput.Header.SpotAitesakiId > 0 Then
        peKonInput.Header.SpotAitesakiId = 0
      End If
    End If

    ' 全明細と直送先をクリアして再表示する
    peKonInput.Header.ChokusosakiCode = String.Empty
    peKonInput.Header.ChokusosakiMei1 = String.Empty
    peKonInput.Header.ChokusosakiMei2 = String.Empty
    peKonInput.Header.ChokusosakiComment = String.Empty

    For i As Integer = 0 To peKonInput.DetailList.Count - 1
      peKonInput.DetailList(i) = New PEKonInputDetail()
    Next
    ' 合計の再計算
    Me.CalcTotal(peKonInput, True, peKonInput.DenpyoType.Uriage)

    ' 画面更新
    'Me.UpdateLocalEntity(peKonInput)
    Me.ModifiedEntity = peKonInput.Clone()

  End Sub

#End Region

#Region "ツール群"
  ''' <summary>伝区（区）により入荷マークを設定。
  ''' </summary>
  ''' <param name="detail">伝票入力エンティティ明細</param>
  ''' <param name="denku">伝区</param>
  Public Sub SetNyukaMarkByKu(ByRef detail As PEKonInputDetail, ByVal denku As PCA.TSC.Kon.BusinessEntity.Defines.DenkuType)
    '
    If detail.Ku = PCA.TSC.Kon.BusinessEntity.Defines.KuType.Tankateisei Then
      '区が単価訂正時
      detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Shinai
      Return
    End If

    '商品マスターを取得
    Dim beMasterSms As BEMasterSms = MyBase.FindBEMasterSms(detail.SyohinCode)

    Select Case beMasterSms.SystemKubun
      Case PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.KonKan, PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.Kan
        'システム区分が共用、仕入専用
        If detail.Ku = PCA.TSC.Kon.BusinessEntity.Defines.KuType.Tsujo Or detail.Ku = PCA.TSC.Kon.BusinessEntity.Defines.KuType.Henpin Then
          '区が通常、返品
          If denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Keiyaku Then
            '契約伝票以外
            detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Suru
          Else
            detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Shinai
          End If
        Else
          detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Shinai
        End If
      Case PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.Kon
        '売上専用
        detail.KanMark = PCA.TSC.Kon.BusinessEntity.Defines.NyukaMarkType.Shinai
    End Select

  End Sub


#End Region

End Class


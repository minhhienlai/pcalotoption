﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports PCA.Controls
Imports Sunloft.PCAIF
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports PCA.TSC.Kon
Imports PCA

Public Class frmStocktaking
  Private connector As IIntegratedApplication = Nothing

  'Table Column's width Constants
  Private Const CHECK_LENGTH As Short = 5
  Private Const SLIPNO_LENGTH As Short = 10
  Private Const TYPE_LENGTH As Integer = 10
  Private Const DATE_LENGTH As Short = 13
  Private Const CODE_LENGTH As Short = 13
  Private Const PRODUCTNAME_LENGTH As Short = 40
  Private Const WHNAME_LENGTH As Short = 14
  Private Const REASONNAME_LENGTH As Short = 20
  Private Const LOTNO_LENGTH As Short = 20
  Private Const LOTDETAIL_LENGTH As Short = 20
  Private Const QUANTITY_LENGTH As Integer = 10

  'Save to common lib?
  Private m_IsF12CloseFlag As Boolean = True
  Private m_IsClosingFlag As Boolean = False
  Private m_IntRowIndex As Integer = -1
  Private m_IntRowCount As Integer = 0
  Private m_SL_LMBClass As SL_LMB
  Private m_AMS1Class As AMS1

  Private m_conf As Sunloft.PcaConfig
  Private MasterDialog As SLMasterSearchDialog
  Private m_appClass As ConnectToPCA = New ConnectToPCA()

  Private m_isAllowKeyDown As Boolean = False
  Private m_isRangeToFocus As Boolean = False
  Private m_isMasterSearched As Boolean = False
  Private m_strEditingValue As String = String.Empty
  Private m_CurrentColumn As String = String.Empty
  Private m_ifEnterPress As Boolean = False
  Private m_CellEvent As PCA.Controls.CellEventArgs = Nothing

  Private Const PG_NAME As String = "棚卸一覧表"
  Private Const RPT_FILETITLE_INSPECT As String = "棚卸調査表"
  Private Const RPT_FILETITLE_ST As String = "棚卸表"
  Private Const RPT_FILENAME_INSPECT As String = "TSInspect.rpt"
  Private Const RPT_FILENAME_ST As String = "StockTaking.rpt"

  Private m_strCondWHCode As String
  Private m_strCondWHName As String
  Private m_intCondDate As Integer

#Region "Initialize Methods"

  Public Sub New()
    Try
      InitializeComponent()

      'Connect to PCA
      m_appClass.ConnectToPCA()
      connector = m_appClass.connector

      m_SL_LMBClass = New SL_LMB(connector)
      m_SL_LMBClass.ReadOnlyRow()
      m_AMS1Class = New AMS1(connector)
      m_AMS1Class.ReadAll()
      MasterDialog = New SLMasterSearchDialog(connector)
      m_conf = New Sunloft.PcaConfig


    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  Private Sub InitTable()
    Try
      With tblTable
        .DataRowCount = 999

        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader()
        InitTableBody()

        .UpdateTable()

        .BodyColumns.DefaultStyle.BackColor = Color.White
        .BodyColumns.DefaultAltStyle.BackColor = Color.White
        .DataRowCount = 0
      End With
    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try

  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    Try
      Dim configXMLDoc As New System.Xml.XmlDocument

      InitTable()
      rsetProduct.LimitLength = m_AMS1Class.ams1_ProductLength
      rsetProduct.MaxCodeLength = m_AMS1Class.ams1_ProductLength + 1
      rsetWarehouse.LimitLength = m_AMS1Class.ams1_WarehouseLength
      rsetWarehouse.MaxCodeLength = m_AMS1Class.ams1_WarehouseLength + 1

      rdoPrintType.SelectedIndex = 2

      PcaFunctionCommandRefer.Enabled = False
      PcaFunctionCommandOutput.Enabled = False
      PcaFunctionCommandPrint.Enabled = False
      PcaFunctionCommandPreview.Enabled = False
      PcaFunctionCommandCarryOver.Enabled = False
      m_IsF12CloseFlag = True
      ldtDate.IntDate = SLCmnFunction.GetLastDay

      DisplayStockTakingSlip()

      MyBase.OnLoad(e)
    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  Private Sub SetLotDetailName()
    Try
      ColumnName.LotName1 = m_SL_LMBClass.sl_lmb_label1
      ColumnName.LotName2 = m_SL_LMBClass.sl_lmb_label2
      ColumnName.LotName3 = m_SL_LMBClass.sl_lmb_label3
      ColumnName.LotName4 = m_SL_LMBClass.sl_lmb_label4
      ColumnName.LotName5 = m_SL_LMBClass.sl_lmb_label5
      ColumnName.LotName6 = m_SL_LMBClass.sl_lmb_label6
      ColumnName.LotName7 = m_SL_LMBClass.sl_lmb_label7
      ColumnName.LotName8 = m_SL_LMBClass.sl_lmb_label8
      ColumnName.LotName9 = m_SL_LMBClass.sl_lmb_label9
      ColumnName.LotName10 = m_SL_LMBClass.sl_lmb_label10
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>Init Table Header when create Table</summary>
  ''' <remarks></remarks>
  ''' 
  Private Sub InitTableHeader()
    Try
      tblTable.HeadTextHeight = 1
      tblTable.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
      tblTable.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell
      Dim intLpc As Integer = 0
      Dim strLotDetails As String()
      Dim strHeaderName As String = String.Empty

      SetLotDetailName()

      strLotDetails = {ColumnName.LotName1, ColumnName.LotName2, ColumnName.LotName3, ColumnName.LotName4, ColumnName.LotName5 _
                      , ColumnName.LotName6, ColumnName.LotName7, ColumnName.LotName8, ColumnName.LotName9, ColumnName.LotName10}

      strHeaderName = Replace(ColumnName.WareHouseCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_WarehouseLength + 2 + WHNAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(strHeaderName)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      strHeaderName = Replace(ColumnName.ProductCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_ProductLength + 2 + PRODUCTNAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(strHeaderName)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.WHDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.WHDate)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.Type, TYPE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Type)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.LotNo, LOTNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.LotNo)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitSize, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.UnitSize)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.CaseNumber, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.CaseNumber)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.Number, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Number)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.StockTakingNumber, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.StockTakingNumber)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.Quantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Quantity)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.StockTakingQuantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.StockTakingQuantity)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitName, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.UnitName)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitPrice, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.UnitPrice)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.StockPrice, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.StockPrice)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      strHeaderName = Replace(ColumnName.ReasonCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_MemoLength + 2 + REASONNAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(strHeaderName)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      If m_SL_LMBClass.sl_lmb_sdlflg = 1 Then
        column = New PcaColumn(ColumnName.Shukka, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareFixTextCell(ColumnName.Shukka)
        column.Cells.Add(cell)
        tblTable.HeadColumns.Add(column)
      End If

      If m_SL_LMBClass.sl_lmb_ubdflg = 1 Then
        column = New PcaColumn(ColumnName.Shiyou, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareFixTextCell(ColumnName.Shiyou)
        column.Cells.Add(cell)
        tblTable.HeadColumns.Add(column)
      End If

      If m_SL_LMBClass.sl_lmb_bbdflg = 1 Then
        column = New PcaColumn(ColumnName.Shoumi, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareFixTextCell(ColumnName.Shoumi)
        column.Cells.Add(cell)
        tblTable.HeadColumns.Add(column)
      End If

      If m_SL_LMBClass.sl_lmb_expflg = 1 Then
        column = New PcaColumn(ColumnName.Shouhi, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareFixTextCell(ColumnName.Shouhi)
        column.Cells.Add(cell)
        tblTable.HeadColumns.Add(column)
      End If

      For intLpc = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        column = New PcaColumn(strLotDetails(intLpc), LOTDETAIL_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareFixTextCell(strLotDetails(intLpc))
        column.Cells.Add(cell)
        tblTable.HeadColumns.Add(column)
      Next

      'Invisible column (From Lot Details screen 
      InitHeaderColumn(column, cell, 0, ColumnName.SlipNo, ColumnName.SlipNo)
      InitHeaderColumn(column, cell, 0, ColumnName.StockID, ColumnName.StockID)
      InitHeaderColumn(column, cell, 0, ColumnName.PrevNumber, ColumnName.PrevNumber)
      InitHeaderColumn(column, cell, 0, ColumnName.PrevQuantity, ColumnName.PrevQuantity)

    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  ''' <summary>Init a Header column</summary>
  ''' <param name="column">PcaColumnControl</param>
  ''' <param name="cell">PcaCellControl</param>
  ''' <param name="columnWidth">columnWidth</param>
  ''' <param name="cellName">cellName</param>
  ''' <param name="columnName">columnName</param>
  ''' <remarks></remarks>
  ''' 
  Private Sub InitHeaderColumn(ByVal column As PcaColumn, ByVal cell As PcaCell, ByVal columnWidth As Integer, ByVal cellName As String, ByVal columnName As String)
    Try
      column = New PcaColumn(columnName, columnWidth)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(cellName, columnWidth, 1, 0.0F, 0.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>InitTableBody when create Table</summary>
  ''' <remarks></remarks>
  Private Sub InitTableBody()
    Try
      tblTable.BodyTextHeight = 1

      tblTable.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
      tblTable.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
      tblTable.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
      tblTable.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell
      Dim intLpc As Integer = 0
      Dim strLotDetails As String()
      Dim strHeaderName As String = String.Empty
      Dim SL_LMB As New SL_LMB(connector)
      SL_LMB.ReadOnlyRow()

      strLotDetails = {ColumnName.LotName1, ColumnName.LotName2, ColumnName.LotName3, ColumnName.LotName4, ColumnName.LotName5 _
                      , ColumnName.LotName6, ColumnName.LotName7, ColumnName.LotName8, ColumnName.LotName9, ColumnName.LotName10}


      strHeaderName = Replace(ColumnName.WareHouseCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_WarehouseLength + 2 + WHNAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.WareHouseCode, m_AMS1Class.ams1_WarehouseLength + 2, 1, 0.0F, 0.0F, strHeaderName)
      cell.EditMode = False
      cell.Enabled = False
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.WareHouseName, WHNAME_LENGTH, 1, m_AMS1Class.ams1_WarehouseLength + 2, 0.0F, strHeaderName)
      cell.EditMode = False
      cell.Enabled = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      strHeaderName = Replace(ColumnName.ProductCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_ProductLength + 2 + PRODUCTNAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.ProductCode, m_AMS1Class.ams1_ProductLength + 2, 1, 0.0F, 0.0F, strHeaderName)
      cell.EditMode = False
      cell.Enabled = False
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.ProductName, PRODUCTNAME_LENGTH, 1, m_AMS1Class.ams1_ProductLength + 2, 0.0F, strHeaderName)
      cell.EditMode = False
      cell.Enabled = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.WHDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.WHDate, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.WHDate)
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.Type, TYPE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.Type, TYPE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Type)
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.LotNo, LOTNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.LotNo, LOTNO_LENGTH, 1, 0.0F, 0.0F, ColumnName.LotNo)
      cell.Type = CellType.Text
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitSize, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.UnitSize, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.UnitSize)
      cell.Type = CellType.Number
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.CaseNumber, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.CaseNumber, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.CaseNumber)
      cell.Type = CellType.Number
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.Number, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.Number, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.Number)
      cell.Type = CellType.Number
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.StockTakingNumber, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.StockTakingNumber, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.StockTakingNumber)
      cell.Type = CellType.Number
      cell.EditMode = True
      cell.Enabled = True
      cell.CalculatorEnabled = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.Quantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.Quantity, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.Quantity)
      cell.Type = CellType.Number
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.StockTakingQuantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.StockTakingQuantity, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.StockTakingQuantity)
      cell.Type = CellType.Number
      cell.DecimalDigit = 4
      cell.EditMode = True
      cell.Enabled = True
      cell.CalculatorEnabled = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitName, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.UnitName, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.UnitName)
      cell.EditMode = False
      cell.CellStyle.Alignment = StringAlignment.Near
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitPrice, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.UnitPrice, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.UnitPrice)
      cell.Type = CellType.Number
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.StockPrice, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.StockPrice, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.StockPrice)
      cell.Type = CellType.Number
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      strHeaderName = Replace(ColumnName.ReasonCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_MemoLength + 2 + REASONNAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.ReasonCode, m_AMS1Class.ams1_MemoLength + 2, 1, 0.0F, 0.0F, strHeaderName)
      cell.EditMode = True
      cell.Enabled = True
      cell.HasRefButton = True
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.ReasonName, REASONNAME_LENGTH, 1, m_AMS1Class.ams1_MemoLength + 2, 0.0F, strHeaderName)
      cell.EditMode = False
      cell.Enabled = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      If m_SL_LMBClass.sl_lmb_sdlflg = 1 Then
        column = New PcaColumn(ColumnName.Shukka, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareTextCell(ColumnName.Shukka, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Shukka)
        cell.EditMode = False
        cell.Enabled = False
        cell.CellStyle.Alignment = StringAlignment.Center
        column.Cells.Add(cell)
        tblTable.BodyColumns.Add(column)
      End If

      If m_SL_LMBClass.sl_lmb_ubdflg = 1 Then
        column = New PcaColumn(ColumnName.Shiyou, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareTextCell(ColumnName.Shiyou, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Shiyou)
        cell.EditMode = False
        cell.Enabled = False
        cell.CellStyle.Alignment = StringAlignment.Center
        column.Cells.Add(cell)
        tblTable.BodyColumns.Add(column)
      End If

      If m_SL_LMBClass.sl_lmb_bbdflg = 1 Then
        column = New PcaColumn(ColumnName.Shoumi, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareTextCell(ColumnName.Shoumi, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Shoumi)
        column.Cells.Add(cell)
        cell.EditMode = False
        cell.Enabled = False
        cell.CellStyle.Alignment = StringAlignment.Center
        tblTable.BodyColumns.Add(column)
      End If

      If m_SL_LMBClass.sl_lmb_expflg = 1 Then
        column = New PcaColumn(ColumnName.Shouhi, DATE_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareTextCell(ColumnName.Shouhi, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Shouhi)
        column.Cells.Add(cell)
        cell.EditMode = False
        cell.Enabled = False
        cell.CellStyle.Alignment = StringAlignment.Center
        tblTable.BodyColumns.Add(column)
      End If

      For intLpc = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        column = New PcaColumn(strLotDetails(intLpc), LOTDETAIL_LENGTH)
        column.CanResize = True
        cell = tblTable.PrepareTextCell(strLotDetails(intLpc), LOTDETAIL_LENGTH, 1, 0.0F, 0.0F, strLotDetails(intLpc))
        cell.EditMode = False
        cell.Enabled = False
        column.Cells.Add(cell)
        tblTable.BodyColumns.Add(column)
      Next

      'Invisible column from Lot Details screen

      InitBodyColumnText(column, cell, ColumnName.SlipNo, ColumnName.SlipNo, 0, 1, ColumnName.SlipNo, False)
      InitBodyColumnText(column, cell, ColumnName.StockID, ColumnName.StockID, 0, 1, ColumnName.StockID, False)
      InitBodyColumnText(column, cell, ColumnName.PrevNumber, ColumnName.PrevNumber, 0, 1, ColumnName.PrevNumber, False)
      InitBodyColumnText(column, cell, ColumnName.PrevQuantity, ColumnName.PrevQuantity, 0, 1, ColumnName.PrevQuantity, False)

    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  ''' <summary>Init Text column when create Table</summary>
  ''' <param name="column"></param>
  ''' <param name="cell"></param>
  ''' <param name="columnName"></param>
  ''' <param name="cellname"></param>
  ''' <param name="cellwidth"></param>
  ''' <param name="cellHeight"></param>
  ''' <param name="headerName"></param>
  ''' <param name="editable"></param>
  ''' <remarks></remarks>
  Private Sub InitBodyColumnText(column As PcaColumn, cell As PcaCell, columnName As String, cellname As String, cellwidth As Short, cellHeight As Short, headerName As String, Optional ByVal editable As Boolean = True)
    'Create column text
    column = New PcaColumn(columnName, cellwidth)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(cellname, cellwidth, cellHeight, 0, 0, headerName)
    cell.EditMode = editable
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)
  End Sub

#End Region


#Region "PcaTable Methods"

  Private Sub tblTable_CellValueChanged(sender As Object, args As PCA.Controls.EditingCellEventArgs) Handles tblTable.CellValueChanged
    Try
      Select Case args.CellName
        Case ColumnName.ReasonCode
          tblTable.SetCellValue(m_IntRowIndex, ColumnName.ReasonName, String.Empty)
        Case ColumnName.StockTakingNumber, ColumnName.StockTakingQuantity
          If args.EditingValue.ToString.Trim.Length > 0 AndAlso CDec(args.EditingValue) > 0D Then
            m_IsF12CloseFlag = False
          End If
      End Select
      m_CurrentColumn = args.CellName

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub tblTable_EnterInputRow(sender As System.Object, args As PCA.Controls.InputRowEventArgs) Handles tblTable.EnterInputRow
    Try
      m_IntRowIndex = args.RowIndex
      '*** Setting DecimalDigit

      Dim SMSClass As New SMS(connector)
      Dim cell As PcaCell = tblTable.BodyColumns.GetCell(ColumnName.StockTakingQuantity)

      SMSClass.ReadByID(tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode).ToString)
      cell.DecimalDigit = CShort(SMSClass.sms_sketa)
      cell.FormatDecimal = True
      cell.Enabled = True
      
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return
    End Try

  End Sub

  Private Sub tblTable_ClickRefButton(sender As Object, args As PCA.Controls.CellEventArgs) Handles tblTable.ClickRefButton
    SearchItem(tblTable, args, Nothing)
  End Sub

  Private Sub tblTable_ClickF8Key(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.ClickF8Key
    SearchItem(tblTable, args)
  End Sub
  Private Sub tblTable_GotCellFocus(sender As Object, args As PCA.Controls.CellEventArgs) Handles tblTable.GotCellFocus

    
    m_CellEvent = args
    If args.CellName = ColumnName.ReasonCode Then
      PcaFunctionCommandRefer.Enabled = True
    Else
      PcaFunctionCommandRefer.Enabled = False
    End If
  End Sub

  Private Sub tblTable_ValidatingCell(sender As System.Object, args As PCA.Controls.CancelCellEventArgs) Handles tblTable.ValidatingCell

    Dim myTable As TSC.Kon.Tools.TscMeisaiTable = DirectCast(sender, TSC.Kon.Tools.TscMeisaiTable)
    Dim intLpc As Integer = 0

    Try
      Dim Rectangle = tblTable.GetCellRectangle(tblTable, m_IntRowIndex, args.CellName)
      Select Case args.CellName
        Case ColumnName.StockTakingNumber
          Dim intNumber As Integer = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Number))
          If IsNumeric(args.NewValue.ToString) AndAlso CInt(args.NewValue) > intNumber Then
            Rectangle.Y += tblTable.Top
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.STOCKTAKING_NUMBER_OVER, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, 1000, Rectangle, ToolTip1, Me)
            If CInt(args.NewValue) > 0 Then tblTable.SetCellValue(m_IntRowIndex, ColumnName.StockTakingNumber, CInt(args.NewValue))
            args.Cancel = True
          Else
            If IsNumeric(args.NewValue.ToString) AndAlso CInt(args.NewValue) > 0 Then m_IsF12CloseFlag = False
          End If

        Case ColumnName.StockTakingQuantity
          Dim decQuantity As Decimal = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Quantity))
          If IsNumeric(args.NewValue.ToString) AndAlso CDec(args.NewValue) > decQuantity Then
            Rectangle.Y += tblTable.Top
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.STOCKTAKING_QUANTITY_OVER, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, 1000, Rectangle, ToolTip1, Me)
            If Val(args.NewValue) > 0D Then tblTable.SetCellValue(m_IntRowIndex, ColumnName.StockTakingQuantity, Val(args.NewValue))
            args.Cancel = True
          Else
            If Val(args.NewValue) > 0D Then tblTable.SetCellValue(m_IntRowIndex, ColumnName.StockTakingQuantity, Val(args.NewValue))
            If IsNumeric(args.NewValue.ToString) AndAlso CDec(args.NewValue) > 0D Then m_IsF12CloseFlag = False
          End If

        Case ColumnName.ReasonCode
          Dim strReasonCode As String = args.NewValue.ToString
          If strReasonCode.Length = 0 Then Return
          Dim newCode As String = String.Empty
          strReasonCode = strReasonCode.PadLeft(m_AMS1Class.ams1_MemoLength, "0"c)

          Dim beMasterMemo As PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo = MasterDialog.FindBEMasterTekiyo(KubunIdType.TekiyoKubun1, strReasonCode)

          If beMasterMemo.TekiyoCode.Length > 0 Then
            tblTable.SetCellValue(m_IntRowIndex, ColumnName.ReasonCode, beMasterMemo.TekiyoCode)
            tblTable.SetCellValue(m_IntRowIndex, ColumnName.ReasonName, beMasterMemo.TekiyoMei)
          Else
            args.Cancel = True
          End If

      End Select

      If args.KeyData = Keys.Enter Then m_ifEnterPress = True Else m_ifEnterPress = False

    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  Private Sub tblTable_ValidatedCell(sender As Object, args As PCA.Controls.CellEventArgs) Handles tblTable.ValidatedCell
    Try
      Dim decSTQuantity As Decimal = 0D
      Dim decUnitPrice As Decimal = 0D
      Dim intStockPrice As Integer = 0
      Dim SMSClass As New SMS(connector)
      SMSClass.ReadByID(tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode).ToString)

      If args.CellName = ColumnName.StockTakingQuantity Then
        If Not tblTable.GetCellValue(m_IntRowIndex, args.CellName).ToString = String.Empty Then

          decSTQuantity = CDec(tblTable.GetCellValue(m_IntRowIndex, args.CellName))
          decUnitPrice = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPrice))
          intStockPrice = CInt(Math.Floor(decSTQuantity) * decUnitPrice)
          tblTable.SetCellValue(m_IntRowIndex, args.CellName, Format(decSTQuantity, SLCmnFunction.FormatComma(SMSClass.sms_sketa)))
          tblTable.SetCellValue(m_IntRowIndex, ColumnName.StockPrice, Format(intStockPrice, "#,0"))

        End If
      End If

      If m_ifEnterPress Then
        If m_IntRowIndex < tblTable.DataRowCount - 1 Then
          tblTable.SetCellFocus(m_IntRowIndex + 1, args.CellName)
        Else
          tblTable.SetCellFocus(m_IntRowIndex, args.CellName)
        End If
        m_ifEnterPress = False
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

#End Region

#Region "Private Methods"

  ''' <summary>Search of each master</summary>
  ''' <param name="argPcaRangeCodeSet"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub SearchItem(ByVal argPcaRangeCodeSet As PcaRangeCodeSet, Optional e As System.ComponentModel.CancelEventArgs = Nothing, Optional ByVal isCodeFrom As Boolean = True)

    Try

      m_isMasterSearched = True
      '参照画面を表示
      Dim newCode As String = String.Empty
      Dim location As Point
      Dim strTargetText As String = String.Empty

      If isCodeFrom Then
        strTargetText = argPcaRangeCodeSet.FromCodeText
        location = Tools.ControlTool.GetDialogLocation(argPcaRangeCodeSet.FromReferButton)
      Else
        strTargetText = argPcaRangeCodeSet.ToCodeText
        location = Tools.ControlTool.GetDialogLocation(argPcaRangeCodeSet.ToReferButton)
      End If

      If argPcaRangeCodeSet Is rsetProduct Then
        newCode = MasterDialog.ShowReferSmsDialog(strTargetText, location)
      Else
        newCode = MasterDialog.ShowReferSokoDialog(strTargetText, location)
      End If

      If newCode.Length > 0 Then

        If isCodeFrom Then
          argPcaRangeCodeSet.FromCodeText = newCode
          If argPcaRangeCodeSet.ToCodeText.Trim.Length = 0 Then argPcaRangeCodeSet.ToCodeText = newCode
        Else
          argPcaRangeCodeSet.ToCodeText = newCode
        End If

        Return
      Else
        If e IsNot Nothing Then e.Cancel = True
        argPcaRangeCodeSet.Focus()
        Return
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>Search of each master</summary>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub SearchItem(ByVal argTbltable As TSC.Kon.Tools.TscMeisaiTable, args As PCA.Controls.CellEventArgs, Optional e As System.ComponentModel.CancelEventArgs = Nothing)

    Try

      '摘要コード
      Dim selectedItem As String = argTbltable.GetCellValue(args.Point.Y, args.CellName).ToString()

      '参照画面を表示
      Dim newCode As String = String.Empty
      Dim location As Point = Tools.ControlTool.GetDialogLocation(argTbltable.RefButton)
      newCode = MasterDialog.ShowReferTekiyoDialog(String.Empty, location)

      Dim beMasterMemo As PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo = MasterDialog.FindBEMasterTekiyo(KubunIdType.TekiyoKubun7, newCode)

      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
        If beMasterMemo.TekiyoCode.Length > 0 Then
          'Set data to screen after validate
          argTbltable.SetCellValue(args.Point.Y, args.CellName, beMasterMemo.TekiyoCode)
          argTbltable.SetCellValue(args.Point.Y, ColumnName.ReasonName, beMasterMemo.TekiyoMei)
          argTbltable.SetCellFocus(args.Point.Y, args.CellName)
        Else
          'Validate failed
          If Not e Is Nothing Then e.Cancel = True
          argTbltable.SetCellFocus(args.Point.Y, args.CellName)
        End If
      End If


    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>Display CarryOver SlipList</summary>
  ''' <returns></returns>
  ''' <remarks>日付、伝票種類をもとに棚卸可能な伝票を取得します</remarks>
  Private Function DisplayStockTakingSlip() As Boolean

    Dim intRowCount As Integer = 0
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader
    Dim intNumber As Integer = 0
    Dim decQuantity As Decimal = 0D
    Dim intStockPlace As Integer = 0
    Dim intSlipType As Integer = 0
    Dim intQttPerCase As Integer = 0
    Dim intWHNum As Integer = 0
    Dim decWHQty As Decimal = 0D
    Dim SMSClass As SMS = New SMS(connector)

    Dim strLotDetails() As String

    strLotDetails = {ColumnName.LotName1, ColumnName.LotName2, ColumnName.LotName3, ColumnName.LotName4, ColumnName.LotName5 _
                    , ColumnName.LotName6, ColumnName.LotName7, ColumnName.LotName8, ColumnName.LotName9, ColumnName.LotName10}

    Try
      tblTable.DataRowCount = 99
      tblTable.BodyRowCount = 99
      tblTable.ClearCellValues()
      ldtDate.Focus()
      'Get details data
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZDN_Pro04080")
      selectCommand.Parameters("@ProductCodeFrom").SetValue(rsetProduct.FromCodeText)
      selectCommand.Parameters("@ProductCodeTo").SetValue(rsetProduct.ToCodeText)
      selectCommand.Parameters("@WHCodeFrom").SetValue(rsetWarehouse.FromCodeText)
      selectCommand.Parameters("@WHCodeTo").SetValue(rsetWarehouse.ToCodeText)
      selectCommand.Parameters("@TargetDate").SetValue(ldtDate.IntDate.SerializeTarget)
      reader = selectCommand.ExecuteReader
      intRowCount = 0

      While reader.Read()
        If intRowCount = 99 Then Exit While

        With tblTable

          SMSClass.ReadByID(reader.GetValue("sl_zdn_scd").ToString)

          'Set Print Condition
          m_strCondWHCode = reader.GetValue("sl_zdn_souko").ToString
          m_strCondWHName = reader.GetValue("ems_str").ToString
          m_intCondDate = ldtDate.IntDate

          intQttPerCase = CInt(reader.GetValue("sl_zdn_iri"))
          decQuantity = CDec(reader.GetValue("sl_zdn_nyukosuryo")) - CDec(reader.GetValue("AllocateQuantity"))
          .SetCellValue(intRowCount, ColumnName.WareHouseCode, reader.GetValue("sl_zdn_souko"))
          .SetCellValue(intRowCount, ColumnName.WareHouseName, reader.GetValue("ems_str"))

          .SetCellValue(intRowCount, ColumnName.ProductCode, reader.GetValue("sl_zdn_scd"))
          .SetCellValue(intRowCount, ColumnName.ProductName, reader.GetValue("sms_mei"))
          .SetCellValue(intRowCount, ColumnName.WHDate, Format(reader.GetValue("sl_zdn_uribi"), "0000/00/00"))
          If Not IsDBNull(reader.GetValue("sl_zdn_datakbn")) Then
            intSlipType = CInt(reader.GetValue("sl_zdn_datakbn"))
          Else
            intSlipType = 0
          End If
          .SetCellValue(intRowCount, ColumnName.Type, SLCmnFunction.GetZDNSlipTypeName(DirectCast(intSlipType, SLConstants.SL_ZDNSlipType)))
          .SetCellValue(intRowCount, ColumnName.LotNo, reader.GetValue("sl_zdn_ulotno"))

          If intQttPerCase = 0 Then
            .SetCellValue(intRowCount, ColumnName.UnitSize, String.Empty)
            .SetCellValue(intRowCount, ColumnName.CaseNumber, String.Empty)
          Else
            .SetCellValue(intRowCount, ColumnName.UnitSize, Format(intQttPerCase, "#,0"))
            .SetCellValue(intRowCount, ColumnName.CaseNumber, Format(CInt(decQuantity / intQttPerCase), "#,0"))
          End If

          Integer.TryParse(reader.GetValue("sl_zdn_nyukohonsu").ToString, intWHNum)
          If CInt(reader.GetValue("sl_zadj_date")) = ldtDate.IntDate Then
            .SetCellValue(intRowCount, ColumnName.Number, Format(intWHNum, "#,0"))
          Else
            .SetCellValue(intRowCount, ColumnName.Number, Format(intWHNum - CInt(reader.GetValue("AllocateNumber")), "#,0"))
          End If
          .SetCellValue(intRowCount, ColumnName.StockTakingNumber, Format(intWHNum - CInt(reader.GetValue("AllocateNumber")), "#,0"))

          Decimal.TryParse(reader.GetValue("sl_zdn_nyukosuryo").ToString, decWHQty)
          If CInt(reader.GetValue("sl_zadj_date")) = ldtDate.IntDate Then
            .SetCellValue(intRowCount, ColumnName.Quantity, Format(decWHQty, SLCmnFunction.FormatComma(SMSClass.sms_sketa)))
          Else
            .SetCellValue(intRowCount, ColumnName.Quantity, Format(decWHQty - CDec(reader.GetValue("AllocateQuantity")), SLCmnFunction.FormatComma(SMSClass.sms_sketa)))
          End If

          .SetCellValue(intRowCount, ColumnName.StockTakingQuantity, Format(intWHNum - CDec(reader.GetValue("AllocateQuantity")), SLCmnFunction.FormatComma(SMSClass.sms_sketa)))

          .SetCellValue(intRowCount, ColumnName.UnitName, reader.GetValue("sms_tani"))
          .SetCellValue(intRowCount, ColumnName.UnitPrice, Val(reader.GetValue("sl_zdn_tanka")).ToString("#,0.####"))
          intStockPlace = SLCmnFunction.Truncate(CDec(reader.GetValue("sl_zdn_tanka")) * decQuantity, 1)

          .SetCellValue(intRowCount, ColumnName.StockPrice, intStockPlace.ToString("#,0"))
          .SetCellValue(intRowCount, ColumnName.ReasonCode, reader.GetValue("sl_zadj_tekcd"))
          .SetCellValue(intRowCount, ColumnName.ReasonName, reader.GetValue("sl_zadj_tekmei"))

          If m_SL_LMBClass.sl_lmb_sdlflg = 1 And Not reader.GetValue("sl_zdn_sdldate") Is Nothing AndAlso CInt(reader.GetValue("sl_zdn_sdldate")) > 0 Then
            .SetCellValue(intRowCount, ColumnName.Shukka, Format(CInt(reader.GetValue("sl_zdn_sdldate").ToString), "0000/00/00"))
          Else
            .SetCellValue(intRowCount, ColumnName.Shukka, String.Empty)
          End If

          If m_SL_LMBClass.sl_lmb_ubdflg = 1 And Not reader.GetValue("sl_zdn_ubdate") Is Nothing AndAlso CInt(reader.GetValue("sl_zdn_ubdate")) > 0 Then
            .SetCellValue(intRowCount, ColumnName.Shiyou, Format(CInt(reader.GetValue("sl_zdn_ubdate").ToString), "0000/00/00"))
          Else
            .SetCellValue(intRowCount, ColumnName.Shiyou, String.Empty)
          End If

          If m_SL_LMBClass.sl_lmb_bbdflg = 1 And Not reader.GetValue("sl_zdn_bbdate") Is Nothing AndAlso CInt(reader.GetValue("sl_zdn_bbdate")) > 0 Then
            .SetCellValue(intRowCount, ColumnName.Shoumi, Format(CInt(reader.GetValue("sl_zdn_bbdate").ToString), "0000/00/00"))
          Else
            .SetCellValue(intRowCount, ColumnName.Shoumi, String.Empty)
          End If

          If m_SL_LMBClass.sl_lmb_expflg = 1 And Not reader.GetValue("sl_zdn_expdate") Is Nothing AndAlso CInt(reader.GetValue("sl_zdn_expdate")) > 0 Then
            .SetCellValue(intRowCount, ColumnName.Shouhi, Format(CInt(reader.GetValue("sl_zdn_expdate").ToString), "0000/00/00"))
          Else
            .SetCellValue(intRowCount, ColumnName.Shouhi, String.Empty)
          End If

          For intLpc = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
            .SetCellValue(intRowCount, strLotDetails(intLpc), reader.GetValue("sl_zdn_ldmei" & intLpc + 1).ToString.Trim)
          Next

          'hidden
          tblTable.SetCellValue(intRowCount, ColumnName.SlipNo, reader.GetValue("sl_zdn_id"))
          If reader.GetValue("sl_zadj_id") IsNot Nothing AndAlso CInt(reader.GetValue("sl_zadj_id")) > 0 Then
            tblTable.SetCellValue(intRowCount, ColumnName.StockID, reader.GetValue("sl_zadj_id").ToString)
          Else
            tblTable.SetCellValue(intRowCount, ColumnName.StockID, String.Empty)
          End If
          tblTable.SetCellValue(intRowCount, ColumnName.PrevNumber, reader.GetValue("sl_zadj_honsu"))
          tblTable.SetCellValue(intRowCount, ColumnName.PrevQuantity, reader.GetValue("sl_zadj_suryo"))

          intRowCount += 1

        End With
      End While
      If intRowCount > 0 Then m_IntRowCount = intRowCount - 1 Else m_IntRowCount = 0
      m_IsF12CloseFlag = True

      If intRowCount = 0 Then
        PcaFunctionCommandCarryOver.Enabled = False
        PcaFunctionCommandPrint.Enabled = False
        PcaFunctionCommandPreview.Enabled = False
        PcaFunctionCommandOutput.Enabled = False
        Return False
      Else
        PcaFunctionCommandCarryOver.Enabled = True
        PcaFunctionCommandPrint.Enabled = True
        PcaFunctionCommandPreview.Enabled = True
        PcaFunctionCommandOutput.Enabled = True
        Return True
      End If

    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
      Return False
    Finally
      tblTable.DataRowCount = intRowCount
      tblTable.BodyRowCount = intRowCount
    End Try

    Return True
  End Function

  ''' <summary>Register StockdData</summary>
  ''' <returns></returns>
  ''' <remarks>在庫データへ登録します。</remarks>
  Private Function RegisterSlip(Optional ByVal isPrint As Boolean = False) As Boolean
    Dim session As ICustomSession = Nothing

    Try
      Dim intLpc As Integer = 0
      Dim dlgResult As DialogResult
      Dim isSuccess As Boolean = True
      Dim intNumber As Integer = 0
      Dim decQuantity As Decimal = 0D
      Dim strReasonCode As String = String.Empty
      Dim SL_ZADJ As New SL_ZADJ(connector)
      Dim SL_ZHK As New SL_ZHK(connector)
      Dim SL_ZDN As New SL_ZDN(connector)
      Dim SL_ZLM As New SL_ZLM(connector)
      Dim intCount As Integer = 0
      If isPrint Then
        dlgResult = Windows.Forms.DialogResult.Yes
      Else
        dlgResult = MessageBox.Show(SLConstants.ConfirmMessage.REGISTER_CONFIRM_MESSAGE, "登録確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
      End If
      If dlgResult = Windows.Forms.DialogResult.No Then Return False

      session = connector.CreateTransactionalSession

      For intLpc = 0 To tblTable.DataRowCount - 1

        intNumber = 0 : decQuantity = 0D
        Integer.TryParse(tblTable.GetCellValue(intLpc, ColumnName.StockTakingNumber).ToString, intNumber)
        Decimal.TryParse(tblTable.GetCellValue(intLpc, ColumnName.StockTakingQuantity).ToString, decQuantity)

        If System.Math.Abs(decQuantity - CDec(tblTable.GetCellValue(intLpc, ColumnName.Quantity))) = 0 Then
          Continue For
        End If
        strReasonCode = tblTable.GetCellValue(intLpc, ColumnName.ReasonCode).ToString

        If tblTable.GetCellValue(intLpc, ColumnName.StockID).ToString.Trim.Length > 0 Then

          SL_ZADJ.sl_zadj_id = CInt(tblTable.GetCellValue(intLpc, ColumnName.StockID))
          isSuccess = SL_ZADJ.Delete(session)
          If isSuccess Then
            SL_ZHK.sl_zhk_zdn_id = CInt(tblTable.GetCellValue(intLpc, ColumnName.SlipNo))
            SL_ZHK.sl_zhk_datakbn = SLConstants.SL_ZHKSlipType.WarehouseAdjustment
            SL_ZHK.sl_zhk_dataid = CInt(tblTable.GetCellValue(intLpc, ColumnName.StockID))
            SL_ZHK.sl_zhk_seq = 1
            isSuccess = SL_ZHK.DeleteInventoryWithSEQ(session)
          End If

          If isSuccess Then
            SetDBZDNValues(intLpc, SL_ZDN, SL_ZADJ, True)
            isSuccess = SL_ZDN.UpdateNumberQuantity(False, session)
          End If

        Else
          isSuccess = True
        End If

        SetDBZAJDValues(intLpc, SL_ZADJ)
        If isSuccess And (intNumber > 0 Or decQuantity > 0 Or strReasonCode.Trim.Length > 0) Then
          isSuccess = SL_ZADJ.Insert(session)
          tblTable.SetCellValue(intLpc, ColumnName.StockID, SL_ZADJ.sl_zadj_id)
        End If

        SetDBZHKValues(intLpc, SL_ZHK, SL_ZADJ)
        If isSuccess And (intNumber > 0 Or decQuantity > 0) Then
          isSuccess = SL_ZHK.Insert(session)
        End If

        SetDBZDNValues(intLpc, SL_ZDN, SL_ZADJ, False)
        If isSuccess Then
          isSuccess = SL_ZDN.UpdateNumberQuantity(False, session)
        End If


        'delete sl_zlm
        SL_ZLM.sl_zlm_month = ldtDate.IntDate
        SL_ZLM.sl_zlm_whFrom = rsetWarehouse.FromCodeText
        SL_ZLM.sl_zlm_whTo = rsetWarehouse.ToCodeText
        SL_ZLM.sl_zlm_ProductFrom = rsetProduct.FromCodeText
        SL_ZLM.sl_zlm_ProductTo = rsetProduct.ToCodeText
        If isSuccess Then isSuccess = SL_ZLM.Delete(session)

        'insert sl_zlm
        SetDBZLMValues(intLpc, SL_ZLM, SL_ZADJ)
        If isSuccess Then isSuccess = SL_ZLM.Insert(session)

        If isSuccess Then
          session.Commit()
        End If

        If Not isSuccess Then Exit For
        intCount += 1
      Next
      If isSuccess Then
        SLCmnFunction.ShowToolTip(SLConstants.CommonMessage.SUCCESS_MESSAGE, SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
      Else
        session.Rollback()
        session.Dispose()
        Return False
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      session.Rollback()
      session.Dispose()
      Return False
    Finally

    End Try

    Return True

  End Function

  ''' <summary>Setting DB Values[SL_ZADJ]</summary>
  ''' <param name="intLpc">Current Row</param>
  ''' <param name="SL_ZADJ">SL_ZADJ Class</param>
  Private Sub SetDBZAJDValues(ByVal intLpc As Integer, ByRef SL_ZADJ As SL_ZADJ)
    Try
      Dim decQuantity As Decimal = 0D
      Dim intNumber As Integer = 0
      If tblTable.GetCellValue(intLpc, ColumnName.StockTakingQuantity).ToString.Trim.Length > 0 Then
        decQuantity = CDec(tblTable.GetCellValue(intLpc, ColumnName.StockTakingQuantity))
      End If
      If tblTable.GetCellValue(intLpc, ColumnName.StockTakingNumber).ToString.Trim.Length > 0 Then
        intNumber = CInt(tblTable.GetCellValue(intLpc, ColumnName.StockTakingNumber))
      End If

      With SL_ZADJ
        .sl_zadj_denno2 = String.Empty
        .sl_zadj_date = ldtDate.IntDate
        .sl_zadj_scd = tblTable.GetCellValue(intLpc, ColumnName.ProductCode).ToString
        .sl_zadj_souko = tblTable.GetCellValue(intLpc, ColumnName.WareHouseCode).ToString

        If decQuantity - CDec(tblTable.GetCellValue(intLpc, ColumnName.Quantity)) > 0D Then
          .sl_zadj_iokbn = SLConstants.SL_ZADJType.StocktakingPlus
        Else
          .sl_zadj_iokbn = SLConstants.SL_ZADJType.StocktakingMinus
        End If
        .sl_zadj_suryo = System.Math.Abs(decQuantity - CDec(tblTable.GetCellValue(intLpc, ColumnName.Quantity)))
        .sl_zadj_honsu = System.Math.Abs(intNumber - CInt(tblTable.GetCellValue(intLpc, ColumnName.Number)))
        .sl_zadj_tekcd = tblTable.GetCellValue(intLpc, ColumnName.ReasonCode).ToString
        .sl_zadj_tekmei = tblTable.GetCellValue(intLpc, ColumnName.ReasonName).ToString
        .sl_zadj_zdn_id = CInt(tblTable.GetCellValue(intLpc, ColumnName.SlipNo))
        .sl_zadj_insuser = connector.UserId
        .sl_zadj_insdate = Now
        .sl_zadj_upduser = Nothing
        .sl_zadj_upddate = Nothing

      End With

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>Setting DB Values[SL_ZHK]</summary>
  ''' <param name="intLpc">Current Row</param>
  ''' <param name="SL_ZHK">[SL_ZHK] Class</param>
  ''' <param name="SL_ZADJ">[SL_ZADJ] Class</param>
  Private Sub SetDBZHKValues(ByVal intLpc As Integer, ByRef SL_ZHK As SL_ZHK, ByVal SL_ZADJ As SL_ZADJ)
    Try
      With SL_ZHK
        .sl_zhk_zdn_id = CInt(tblTable.GetCellValue(intLpc, ColumnName.SlipNo))
        .sl_zhk_datakbn = SLConstants.SL_ZHKSlipType.WarehouseAdjustment
        .sl_zhk_dataid = SL_ZADJ.sl_zadj_id
        .sl_zhk_seq = 1
        .sl_zhk_date = ldtDate.IntDate
        If SL_ZADJ.sl_zadj_iokbn = SLConstants.SL_ZADJType.StocktakingPlus Then
          .sl_zhk_honsu = SL_ZADJ.sl_zadj_honsu * -1
          .sl_zhk_suryo = SL_ZADJ.sl_zadj_suryo * -1
        Else
          .sl_zhk_honsu = SL_ZADJ.sl_zadj_honsu
          .sl_zhk_suryo = SL_ZADJ.sl_zadj_suryo
        End If
        .sl_zhk_insuser = connector.UserId
        .sl_zhk_insdate = Now
        .sl_zhk_upduser = Nothing
        .sl_zhk_upddate = Nothing
      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>Setting DB Values[SL_ZDN]</summary>
  ''' <param name="intLpc">Current Row</param>
  ''' <param name="SL_ZDN">[SL_ZDN] Class</param>
  ''' <param name="SL_ZADJ">[SL_ZADJ] Class</param>
  Private Sub SetDBZDNValues(ByVal intLpc As Integer, ByRef SL_ZDN As SL_ZDN, ByVal SL_ZADJ As SL_ZADJ, ByVal isExsistStockID As Boolean)
    Try
      Dim intCoeff As Integer = 0
      With SL_ZDN
        .sl_zdn_id = CInt(tblTable.GetCellValue(intLpc, ColumnName.SlipNo))

        If isExsistStockID Then
          '一度在庫を戻す

          If SL_ZADJ.sl_zadj_iokbn = SLConstants.SL_ZADJType.StocktakingPlus Then
            intCoeff = 1
          Else
            intCoeff = -1
          End If
          .sl_zdn_honsu = CInt(tblTable.GetCellValue(intLpc, ColumnName.PrevNumber)) * intCoeff
          .sl_zdn_suryo = CDec(tblTable.GetCellValue(intLpc, ColumnName.PrevQuantity)) * intCoeff
          .sl_zdn_nyukohonsu = CInt(tblTable.GetCellValue(intLpc, ColumnName.PrevNumber)) * intCoeff
          .sl_zdn_nyukosuryo = CDec(tblTable.GetCellValue(intLpc, ColumnName.PrevQuantity)) * intCoeff
        Else
          If SL_ZADJ.sl_zadj_iokbn = SLConstants.SL_ZADJType.StocktakingPlus Then
            intCoeff = -1
          Else
            intCoeff = 1
          End If
          .sl_zdn_honsu = SL_ZADJ.sl_zadj_honsu * intCoeff
          .sl_zdn_suryo = SL_ZADJ.sl_zadj_suryo * intCoeff
          .sl_zdn_nyukohonsu = SL_ZADJ.sl_zadj_honsu * intCoeff
          .sl_zdn_nyukosuryo = SL_ZADJ.sl_zadj_suryo * intCoeff
        End If

      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>Setting DB Values[SL_ZLM]</summary>
  ''' <param name="intLpc">Current Row</param>
  ''' <param name="SL_ZLM">[SL_ZLM] Class</param>
  ''' <param name="SL_ZADJClass">[SL_ZADJClass] Class</param>
  Private Sub SetDBZLMValues(ByVal intLpc As Integer, ByRef SL_ZLM As SL_ZLM, ByVal SL_ZADJClass As SL_ZADJ)
    Try

      With SL_ZLM
        .sl_zadj_iokbn = SL_ZADJClass.sl_zadj_iokbn
        .sl_zlm_id = 0 'auto increment
        .sl_zlm_month = CInt(Strings.Left(ldtDate.IntDate.SerializeTarget.ToString, 6))
        .sl_zlm_zdn_id = CInt(tblTable.GetCellValue(intLpc, ColumnName.StockID))
        .sl_zlm_souko = tblTable.GetCellValue(intLpc, ColumnName.WareHouseCode).ToString
        .sl_zlm_honsu = 0 'the last time number
        .sl_zlm_suryo = 0 'the last time quantity
        .sl_zlm_zaikohonsu = SL_ZADJClass.sl_zadj_honsu
        .sl_zlm_zaikosuryo = SL_ZADJClass.sl_zadj_suryo
        .sl_zlm_tanka = CDec(tblTable.GetCellValue(intLpc, ColumnName.UnitPrice))
        .sl_zlm_kingaku = .sl_zlm_tanka * .sl_zlm_zaikosuryo
        .sl_zlm_insuser = connector.UserId
        .sl_zlm_insdate = Now
        .sl_zlm_upduser = Nothing
        .sl_zlm_upddate = Nothing

      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean

    If (keyData And Keys.KeyCode) = Keys.Tab And m_isAllowKeyDown Then
      If DisplayStockTakingSlip() Then
        tblTable.Focus()
      Else
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSlipSearch.CommandId)
        ldtDate.Focus()
      End If
    End If
    Return MyBase.ProcessDialogKey(keyData)
  End Function

  ''' <summary>Print</summary>
  ''' <returns>Success：True Failure：False</returns>
  ''' <param name="isPreview">True:Preview</param>
  ''' <remarks></remarks>
  Private Function PrintData(Optional ByVal isPreview As Boolean = False) As Boolean

    Try
      Dim ReportName As String = String.Empty
      Dim ReportTitle As String = String.Empty
      If rdoPrintType.SelectedIndex = 2 Then
        ReportName = RPT_FILENAME_ST
        ReportTitle = RPT_FILETITLE_ST
      Else
        ReportName = RPT_FILENAME_INSPECT
        ReportTitle = RPT_FILETITLE_INSPECT
      End If

      Dim rpt As New Sunloft.Windows.Forms.SLCrystalReport
      Dim RpParam As New Dictionary(Of String, Object)
      Dim editedDataTable As DataTable = Nothing

      rpt.ReportFile = IO.Path.GetDirectoryName(Application.ExecutablePath) & "\" & ReportName
      EditPrintData(editedDataTable)
      rpt.DataSource = editedDataTable
      ' 出力開始
      If isPreview Then
        rpt.Preview(ReportTitle, True)
      Else
        rpt.PrintToPrinter()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True
  End Function

  ''' <summary>Insert the print data to the DataTable</summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EditPrintData(ByRef argEditedData As DataTable) As Boolean

    Dim editData As New DataSet1.DataTable1DataTable

    Dim editRow As DataRow
    Dim intLpc As Integer = 0
    Dim intDigit As Integer = 0

    Try
      While tblTable.GetCellValue(intLpc, ColumnName.SlipNo) IsNot Nothing AndAlso tblTable.GetCellValue(intLpc, ColumnName.SlipNo).ToString.Trim.Length > 0

        If intDigit < 0 Then intDigit = 0
        With tblTable
          editRow = editData.NewRow
          editRow.Item("WHCode") = m_strCondWHCode
          editRow.Item("WHName") = m_strCondWHName.Trim
          editRow.Item("ProductCode") = .GetCellValue(intLpc, ColumnName.ProductCode)
          editRow.Item("ProductName") = .GetCellValue(intLpc, ColumnName.ProductName)
          editRow.Item("LotNo") = .GetCellValue(intLpc, ColumnName.LotNo)
          If tblTable.GetCellValue(intLpc, ColumnName.UnitSize).ToString = String.Empty Then
            editRow.Item("UnitSize") = 0
          Else
            editRow.Item("UnitSize") = tblTable.GetCellValue(intLpc, ColumnName.UnitSize)
          End If

          editRow.Item("Number") = CInt(.GetCellValue(intLpc, ColumnName.Number))
          If .GetCellValue(intLpc, ColumnName.StockTakingNumber).ToString = String.Empty Then
            editRow.Item("STNumber") = 0
          Else
            editRow.Item("STNumber") = CInt(.GetCellValue(intLpc, ColumnName.StockTakingNumber))
          End If

          If .GetCellValue(intLpc, ColumnName.StockTakingNumber).ToString = String.Empty Then
            editRow.Item("DiffNumber") = 0
          Else
            editRow.Item("DiffNumber") = CInt(.GetCellValue(intLpc, ColumnName.StockTakingNumber)) - CInt(.GetCellValue(intLpc, ColumnName.Number))
          End If

          editRow.Item("Quantity") = CDec(.GetCellValue(intLpc, ColumnName.Quantity))
          editRow.Item("UnitName") = .GetCellValue(intLpc, ColumnName.UnitName)
          If .GetCellValue(intLpc, ColumnName.StockTakingQuantity).ToString = String.Empty Then
            editRow.Item("STQuantity") = 0
          Else
            editRow.Item("STQuantity") = CDec(.GetCellValue(intLpc, ColumnName.StockTakingQuantity))
          End If
          If .GetCellValue(intLpc, ColumnName.StockTakingNumber).ToString = String.Empty Then
            editRow.Item("DiffQuantity") = 0D
          Else
            editRow.Item("DiffQuantity") = CDec(.GetCellValue(intLpc, ColumnName.StockTakingQuantity)) - CDec(.GetCellValue(intLpc, ColumnName.Quantity))
          End If
          editRow.Item("UnitPrice") = CDec(.GetCellValue(intLpc, ColumnName.UnitPrice))
          editRow.Item("StockPrice") = CInt(.GetCellValue(intLpc, ColumnName.StockPrice))
          editRow.Item("Date") = Format(m_intCondDate, "0000年00月00日")
          editRow.Item("FirstFlag") = IIf(rdoPrintType.SelectedIndex = 0, True, False)

          editData.Rows.Add(editRow)
        End With
        intLpc += 1
      End While

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
    argEditedData = editData
    Return True
  End Function

  ''' <summary>The output of the display contents</summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function OutPutCSV() As Boolean

    Dim strSaveFileName As String = String.Empty
    Dim strColumns() As String
    Dim strLotDetails() As String
    Dim sbCsv As New System.Text.StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        .OverwritePrompt = True
        .FileName = PG_NAME

        Dim res As System.Windows.Forms.DialogResult = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False

        strSaveFileName = .FileName

      End With


      strLotDetails = {ColumnName.LotName1, ColumnName.LotName2, ColumnName.LotName3, ColumnName.LotName4, ColumnName.LotName5 _
                      , ColumnName.LotName6, ColumnName.LotName7, ColumnName.LotName8, ColumnName.LotName9, ColumnName.LotName10}

      strColumns = {ColumnName.WareHouseCode, ColumnName.WareHouseName, ColumnName.ProductCode, ColumnName.ProductName, ColumnName.WHDate _
                   , ColumnName.Type, ColumnName.LotNo, ColumnName.UnitSize, ColumnName.CaseNumber, ColumnName.Number, ColumnName.StockTakingNumber _
                   , ColumnName.Quantity, ColumnName.StockTakingQuantity, ColumnName.UnitName, ColumnName.UnitPrice, ColumnName.StockPrice _
                   , ColumnName.ReasonCode, ColumnName.ReasonName}

      If m_SL_LMBClass.sl_lmb_sdlflg = 1 Then ReDim Preserve strColumns(strColumns.Length) : strColumns(strColumns.Length - 1) = ColumnName.Shukka
      If m_SL_LMBClass.sl_lmb_ubdflg = 1 Then ReDim Preserve strColumns(strColumns.Length) : strColumns(strColumns.Length - 1) = ColumnName.Shiyou
      If m_SL_LMBClass.sl_lmb_bbdflg = 1 Then ReDim Preserve strColumns(strColumns.Length) : strColumns(strColumns.Length - 1) = ColumnName.Shoumi
      If m_SL_LMBClass.sl_lmb_expflg = 1 Then ReDim Preserve strColumns(strColumns.Length) : strColumns(strColumns.Length - 1) = ColumnName.Shouhi

      For intLpc = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        ReDim Preserve strColumns(strColumns.Length)
        strColumns(strColumns.Length - 1) = strLotDetails(intLpc)
      Next

      SLCmnFunction.OutPutCSV(strColumns, tblTable, strSaveFileName)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function

  ''' <summary>
  ''' check save contents 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InputValidate() As Boolean
    Try

      Dim Rectangle As System.Drawing.Rectangle
      ldtDate.Focus()


      Select Case m_CurrentColumn
        Case ColumnName.StockTakingNumber
          'Number
          Dim intNumber As Integer = 0
          Rectangle = tblTable.GetCellRectangle(tblTable, m_IntRowIndex, ColumnName.StockTakingNumber)
          If tblTable.GetCellValue(m_IntRowIndex, ColumnName.StockTakingNumber).ToString.Trim.Length > 0 Then
            intNumber = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.StockTakingNumber))
          End If

          If CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Number).ToString) < intNumber Then
            Rectangle.Y += tblTable.Top
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.STOCKTAKING_NUMBER_OVER, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, 1000, Rectangle, ToolTip1, Me)
            tblTable.SetCellFocus(m_IntRowIndex, ColumnName.StockTakingNumber)
            Return False
          End If

        Case ColumnName.StockTakingQuantity
          'Quantity
          Dim decQuantity As Decimal = 0D
          Rectangle = tblTable.GetCellRectangle(tblTable, m_IntRowIndex, ColumnName.StockTakingQuantity)
          If tblTable.GetCellValue(m_IntRowIndex, ColumnName.StockTakingQuantity).ToString.Trim.Length > 0 Then
            decQuantity = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.StockTakingQuantity))
          End If

          If CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Quantity).ToString) < decQuantity Then
            Rectangle.Y += tblTable.Top
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.STOCKTAKING_QUANTITY_OVER, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, 1000, Rectangle, ToolTip1, Me)
            tblTable.SetCellFocus(m_IntRowIndex, ColumnName.StockTakingQuantity)
            Return False
          End If


        Case ColumnName.ReasonCode
          'Reason
          Dim strReasonCode As String = tblTable.GetCellValue(m_IntRowIndex, ColumnName.ReasonCode).ToString
          If strReasonCode.Length > 0 Then
            strReasonCode = strReasonCode.PadLeft(m_AMS1Class.ams1_MemoLength, "0"c)

            Dim beMasterMemo As PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo = MasterDialog.FindBEMasterTekiyo(KubunIdType.TekiyoKubun1, strReasonCode)

            If beMasterMemo.TekiyoCode.Length > 0 Then
              tblTable.SetCellValue(m_IntRowIndex, ColumnName.ReasonCode, beMasterMemo.TekiyoCode)
              tblTable.SetCellValue(m_IntRowIndex, ColumnName.ReasonName, beMasterMemo.TekiyoMei)
            Else
              Return False
            End If
          End If
      End Select

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try


    Return True
  End Function

  ''' <summary>execute carryover</summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ExecuteCarryOver() As Boolean

    Try
      If m_IntRowIndex < 0 Then m_IntRowIndex = 0
      ldtDate.Focus()
      Dim intLastDate As Integer = SLCmnFunction.GetLastDay(ldtDate.IntDate.Year, ldtDate.IntDate.Month)
      Dim SL_LMB = New SL_LMB(connector)
      Dim Rectangle = tblTable.GetCellRectangle(tblTable, m_IntRowIndex, ColumnName.StockTakingQuantity)

      If ldtDate.IntDate <> intLastDate Then
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.CANNOT_CARRYOVER, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemCarryOver.CommandId)
        ldtDate.Focus()
        Return False
      End If

      Rectangle.Y += tblTable.Top
      For intLpc As Integer = 0 To tblTable.DataRowCount - 1
        If tblTable.GetCellValue(intLpc, ColumnName.StockTakingQuantity).ToString = String.Empty Then
          SLCmnFunction.ShowToolTip("数量を" & SLConstants.NotifyMessage.INPUT_REQUIRED, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, Rectangle, ToolTip1, Me)
          tblTable.SetCellFocus(intLpc, ColumnName.StockTakingQuantity)
          Return False
        End If
      Next

      SL_LMB.sl_lmb_zcutoff_date = ldtDate.IntDate
      SL_LMB.Update()

      Return True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function
#End Region

#Region "Finalize Methods"

  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then
      connector.LogOffSystem()
    End If
  End Sub

#End Region

#Region "PcaCommandManager Methods"

  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try
      Dim commandItem As PcaCommandItem = e.CommandItem
      Select Case commandItem.CommandName

        Case PcaCommandItemHelp.CommandName


        Case PcaCommandItemCarryOver.CommandName 'F4
          If ExecuteCarryOver() Then
            SLCmnFunction.ShowToolTip(SLConstants.CommonMessage.SUCCESS_CARRYOVER, SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemCarryOver.CommandId)
          End If

        Case PcaCommandItemSlipSearch.CommandName 'F5

          If DisplayStockTakingSlip() Then
            tblTable.Focus()
          Else
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSlipSearch.CommandId)
            ldtDate.Focus()
          End If

        Case PcaCommandItemRefer.CommandName

          Select Case ActiveControl.Name
            Case rsetProduct.Name, rsetWarehouse.Name
              SearchItem(DirectCast(Me.ActiveControl, PcaRangeCodeSet), , Not m_isRangeToFocus)
            Case tblTable.Name
              If m_CellEvent IsNot Nothing Then SearchItem(tblTable, m_CellEvent, Nothing)
          End Select

        Case PcaCommandItemPrint.CommandName 'F9
          Dim strReportName As String
          If rdoPrintType.SelectedIndex = 2 Then
            strReportName = RPT_FILETITLE_ST
          Else
            strReportName = RPT_FILETITLE_INSPECT
          End If
          If MsgBox(strReportName & "を" & SLConstants.ConfirmMessage.PRINT_CONFIRM_MESSAGE, MsgBoxStyle.OkCancel, SLConstants.NotifyMessage.TITLE_MESSAGE) = MsgBoxResult.Ok Then
            If InputValidate() AndAlso RegisterSlip(True) Then
              PrintData()
            End If
          End If

        Case PcaCommandItemPreview.CommandName 'F9
          If InputValidate() AndAlso RegisterSlip(True) Then
            PrintData(True)
          End If

        Case PcaCommandItemOutput.CommandName 'F10
          If OutPutCSV() Then
            SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, commandItem.CommandId)
          End If

        Case PcaCommandItemSave.CommandName 'F12

          If InputValidate() AndAlso RegisterSlip() Then
            SLCmnFunction.ShowToolTip(SLConstants.CommonMessage.SUCCESS_MESSAGE, SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
            If DisplayStockTakingSlip() Then
              tblTable.Focus()
            Else
              SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSlipSearch.CommandId)
              ldtDate.Focus()
            End If
          End If

        Case PcaCommandItemClose.CommandName 'F12

          m_IsClosingFlag = True

          Me.Close()

      End Select
    Catch ex As Exception
      DisplayBox.ShowError(ex.ToString)
    End Try
  End Sub

  ''' <summary>PcaCommandManager1_UpdateCommandUI</summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks>F12 = Close / Register</remarks>
  Private Sub PcaCommandManager1_UpdateCommandUI(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.UpdateCommandUI
    Dim commandItem As PcaCommandItem = e.CommandItem
    'Change F12 caption+function between 
    If commandItem Is PcaCommandItemSave Then
      commandItem.Enabled = Not m_IsF12CloseFlag
    ElseIf commandItem Is PcaCommandItemClose Then
      commandItem.Enabled = m_IsF12CloseFlag
    End If
    PcaFunctionCommandClose.Enabled = m_IsF12CloseFlag
    PcaFunctionCommandSave.Enabled = Not m_IsF12CloseFlag

    ToolStripMenuItemRefer.Enabled = PcaFunctionCommandRefer.Enabled
    ToolStripMenuItemClose.Enabled = PcaFunctionCommandClose.Enabled
    ToolStripMenuItemSave.Enabled = PcaFunctionCommandSave.Enabled
    ToolStripMenuItemCarryOver.Enabled = PcaFunctionCommandCarryOver.Enabled
    ToolStripMenuItemPrint.Enabled = PcaFunctionCommandPrint.Enabled
    ToolStripMenuItemPreview.Enabled = PcaFunctionCommandPreview.Enabled
    ToolStripMenuItemOutPut.Enabled = PcaFunctionCommandOutput.Enabled

    ToolStripButtonCarryOver.Enabled = PcaFunctionCommandCarryOver.Enabled
    ToolStripButtonSave.Enabled = PcaFunctionCommandSave.Enabled
    ToolStripButtonPrint.Enabled = PcaFunctionCommandPrint.Enabled
    ToolStripButtonPreview.Enabled = PcaFunctionCommandPreview.Enabled
    ToolStripButtonOutput.Enabled = PcaFunctionCommandOutput.Enabled

    ToolStripButtonClose.Enabled = True
    ToolStripMenuItemClose.Enabled = True
  End Sub
#End Region

#Region " PcaRangeCodeSet Methods"
  Private Sub PcaRangeCodeSetFrom_ClickReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles rsetProduct.ClickFromReferButton2, _
                                                                                                                             rsetWarehouse.ClickFromReferButton2

    Dim objRangeCodeSet As PcaRangeCodeSet = DirectCast(sender, PcaRangeCodeSet)
    If m_isMasterSearched Then m_isMasterSearched = False : Return
    SearchItem(objRangeCodeSet, e)
    m_isMasterSearched = False
  End Sub

  Private Sub PcaRangeCodeSetTo_ClickReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles rsetProduct.ClickToReferButton2, _
                                                                                                                             rsetWarehouse.ClickToReferButton2
    Dim objRangeCodeSet As PcaRangeCodeSet = DirectCast(sender, PcaRangeCodeSet)
    If m_isMasterSearched Then m_isMasterSearched = False : Return
    SearchItem(objRangeCodeSet, e, False)
    m_isMasterSearched = False
  End Sub

  Private Sub PcaRangeCodeSet_Enter(sender As System.Object, e As System.EventArgs) Handles rsetProduct.Enter, rsetWarehouse.Enter
    PcaFunctionCommandRefer.Enabled = True
  End Sub

  Private Sub PcaRangeCodeSet_Leave(sender As System.Object, e As System.EventArgs) Handles rsetProduct.Leave, rsetWarehouse.Leave
    PcaFunctionCommandRefer.Enabled = False
  End Sub

  Private Sub PcaRangeCodeSet_ToCodeTextEnter(sender As System.Object, e As System.EventArgs) Handles rsetProduct.ToCodeTextEnter, rsetWarehouse.ToCodeTextEnter
    If Me.ActiveControl.Name = rsetProduct.Name Then m_isAllowKeyDown = True

    m_isRangeToFocus = True
  End Sub

  Private Sub PcaRangeCodeSet_FromCodeTextEnter(sender As System.Object, e As System.EventArgs) Handles rsetProduct.FromCodeTextEnter, rsetWarehouse.FromCodeTextEnter
    m_isRangeToFocus = False
  End Sub

  Private Sub PcaRangeCodeSet_ToCodeTextLeave(sender As System.Object, e As System.EventArgs) Handles rsetProduct.ToCodeTextLeave
    m_isAllowKeyDown = False
  End Sub
#End Region

  
End Class

''' <summary>ColumnName</summary>
''' <remarks></remarks>
Friend Class ColumnName
  Public Const WareHouseCode As String = "倉庫コード"
  Public Const WareHouseName As String = "倉庫名"
  Public Const ProductCode As String = "商品コード"
  Public Const ProductName As String = "商品名"
  Public Const WHDate As String = "日付"
  Public Const Type As String = "区分"
  Public Const LotNo As String = "ロットNo"
  Public Const UnitSize As String = "入数"
  Public Const CaseNumber As String = "ケース数"
  Public Const Number As String = "本数"
  Public Const StockTakingNumber As String = "棚卸本数"
  Public Const Quantity As String = "数量"
  Public Const StockTakingQuantity As String = "棚卸数量"
  Public Const UnitName As String = "単位"
  Public Const UnitPrice As String = "単価"
  Public Const StockPrice As String = "在庫金額"
  Public Const ReasonCode As String = "理由コード"
  Public Const ReasonName As String = "理由名"
  Public Const Shukka As String = "出荷期限"
  Public Const Shiyou As String = "使用期限"
  Public Const Shoumi As String = "賞味期限"
  Public Const Shouhi As String = "消費期限"
  Public Shared Property LotName1 As String = "ロット詳細名1"
  Public Shared Property LotName2 As String = "ロット詳細名2"
  Public Shared Property LotName3 As String = "ロット詳細名3"
  Public Shared Property LotName4 As String = "ロット詳細名4"
  Public Shared Property LotName5 As String = "ロット詳細名5"
  Public Shared Property LotName6 As String = "ロット詳細名6"
  Public Shared Property LotName7 As String = "ロット詳細名7"
  Public Shared Property LotName8 As String = "ロット詳細名8"
  Public Shared Property LotName9 As String = "ロット詳細名9"
  Public Shared Property LotName10 As String = "ロット詳細名10"

  'Hidden
  Public Const SlipNo As String = "伝票番号"
  Public Const StockID As String = "在庫調整ID"
  Public Const PrevNumber As String = "修正前本数"
  Public Const PrevQuantity As String = "修正前数量"

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStocktaking
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStocktaking))
    Me.ServiceController1 = New System.ServiceProcess.ServiceController()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.ToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSave = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPreview = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemOutPut = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSlipSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemCarryOver = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRefer = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSave = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPrint = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPreview = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonShow = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonOutput = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonCarryOver = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.ldtDate = New Sunloft.PCAControls.SLPcaLabeledDate()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSlipSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandCarryOver = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrint = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPreview = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandOutput = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSave = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemSave = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPrint = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPreview = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemOutput = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSlipSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemCarryOver = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.tblTable = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.rsetProduct = New PCA.Controls.PcaRangeCodeSet()
    Me.rsetWarehouse = New PCA.Controls.PcaRangeCodeSet()
    Me.rdoPrintType = New PCA.Controls.PcaMultiRadioButton()
    Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'ToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemFile, Nothing)
    Me.ToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSave, Me.ToolStripMenuItemPrint, Me.ToolStripMenuItemPreview, Me.ToolStripMenuItemOutPut, Me.ToolStripMenuItemClose})
    Me.ToolStripMenuItemFile.Name = "ToolStripMenuItemFile"
    Me.ToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.ToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSave, Me.PcaCommandItemSave)
    Me.ToolStripMenuItemSave.Name = "ToolStripMenuItemSave"
    Me.ToolStripMenuItemSave.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemSave.Text = "登録(&S)"
    Me.ToolStripMenuItemSave.ToolTipText = "在庫データを登録します。"
    '
    'ToolStripMenuItemPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPrint, Me.PcaCommandItemPrint)
    Me.ToolStripMenuItemPrint.Name = "ToolStripMenuItemPrint"
    Me.ToolStripMenuItemPrint.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPrint.Text = "印刷(&P)"
    Me.ToolStripMenuItemPrint.ToolTipText = "明細画面を印刷します。"
    '
    'ToolStripMenuItemPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPreview, Me.PcaCommandItemPreview)
    Me.ToolStripMenuItemPreview.Name = "ToolStripMenuItemPreview"
    Me.ToolStripMenuItemPreview.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPreview.Text = "印刷プレビュー(&V)"
    Me.ToolStripMenuItemPreview.ToolTipText = "明細画面のプレピューを行います。"
    '
    'ToolStripMenuItemOutPut
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemOutPut, Me.PcaCommandItemOutput)
    Me.ToolStripMenuItemOutPut.Name = "ToolStripMenuItemOutPut"
    Me.ToolStripMenuItemOutPut.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemOutPut.Text = "出力(&O)"
    Me.ToolStripMenuItemOutPut.ToolTipText = "明細画面をCSV出力します。"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    Me.ToolStripMenuItemClose.ToolTipText = "プログラムを終了します。"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSlipSearch, Me.ToolStripMenuItemCarryOver, Me.ToolStripMenuItemRefer})
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemSlipSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSlipSearch, Me.PcaCommandItemSlipSearch)
    Me.ToolStripMenuItemSlipSearch.Name = "ToolStripMenuItemSlipSearch"
    Me.ToolStripMenuItemSlipSearch.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSlipSearch.Text = "再表示(&R)"
    Me.ToolStripMenuItemSlipSearch.ToolTipText = "明細を表示します。"
    '
    'ToolStripMenuItemCarryOver
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemCarryOver, Me.PcaCommandItemCarryOver)
    Me.ToolStripMenuItemCarryOver.Name = "ToolStripMenuItemCarryOver"
    Me.ToolStripMenuItemCarryOver.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemCarryOver.Text = "繰越(&T)"
    Me.ToolStripMenuItemCarryOver.ToolTipText = "次月への繰越を行います。"
    '
    'ToolStripMenuItemRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRefer, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemRefer.Name = "ToolStripMenuItemRefer"
    Me.ToolStripMenuItemRefer.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemRefer.Text = "参照(&U)"
    Me.ToolStripMenuItemRefer.ToolTipText = "マスタを参照します。"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    Me.ToolStripMenuItemHelp.ToolTipText = "ヘルプを表示します。"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContent.Text = "目次(&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonSave, Me.ToolStripButtonPrint, Me.ToolStripButtonPreview, Me.ToolStripButtonShow, Me.ToolStripButtonOutput, Me.ToolStripButtonCarryOver, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1008, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Image = Global.Pro04080.My.Resources.Resources._Exit
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonClose.ToolTipText = "プログラムを終了します。"
    '
    'ToolStripButtonSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSave, Me.PcaCommandItemSave)
    Me.ToolStripButtonSave.Image = Global.Pro04080.My.Resources.Resources.save
    Me.ToolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSave.Name = "ToolStripButtonSave"
    Me.ToolStripButtonSave.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonSave.Text = "登録"
    Me.ToolStripButtonSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonSave.ToolTipText = "登録を行います。"
    '
    'ToolStripButtonPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPrint, Me.PcaCommandItemPrint)
    Me.ToolStripButtonPrint.Image = CType(resources.GetObject("ToolStripButtonPrint.Image"), System.Drawing.Image)
    Me.ToolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPrint.Name = "ToolStripButtonPrint"
    Me.ToolStripButtonPrint.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonPrint.Text = "印刷"
    Me.ToolStripButtonPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonPrint.ToolTipText = "印刷を行います。"
    '
    'ToolStripButtonPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPreview, Me.PcaCommandItemPreview)
    Me.ToolStripButtonPreview.Image = Global.Pro04080.My.Resources.Resources.preview
    Me.ToolStripButtonPreview.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPreview.Name = "ToolStripButtonPreview"
    Me.ToolStripButtonPreview.Size = New System.Drawing.Size(81, 34)
    Me.ToolStripButtonPreview.Text = "プレビュー"
    Me.ToolStripButtonPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonPreview.ToolTipText = "プレビューを行います。"
    '
    'ToolStripButtonShow
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonShow, Me.PcaCommandItemSlipSearch)
    Me.ToolStripButtonShow.Image = Global.Pro04080.My.Resources.Resources.Search
    Me.ToolStripButtonShow.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonShow.Name = "ToolStripButtonShow"
    Me.ToolStripButtonShow.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonShow.Text = "再表示"
    Me.ToolStripButtonShow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonShow.ToolTipText = "再表示を行います。"
    '
    'ToolStripButtonOutput
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonOutput, Me.PcaCommandItemOutput)
    Me.ToolStripButtonOutput.Image = Global.Pro04080.My.Resources.Resources.export
    Me.ToolStripButtonOutput.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonOutput.Name = "ToolStripButtonOutput"
    Me.ToolStripButtonOutput.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonOutput.Text = "出力"
    Me.ToolStripButtonOutput.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonOutput.ToolTipText = "CSV出力を行います。"
    '
    'ToolStripButtonCarryOver
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonCarryOver, Me.PcaCommandItemCarryOver)
    Me.ToolStripButtonCarryOver.Image = Global.Pro04080.My.Resources.Resources.trans
    Me.ToolStripButtonCarryOver.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonCarryOver.Name = "ToolStripButtonCarryOver"
    Me.ToolStripButtonCarryOver.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonCarryOver.Text = "繰越"
    Me.ToolStripButtonCarryOver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonCarryOver.ToolTipText = "繰越を行います。"
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Image = Global.Pro04080.My.Resources.Resources.help
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonHelp.ToolTipText = "ヘルプを表示します。"
    '
    'ldtDate
    '
    Me.ldtDate.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.ldtDate.ClientProcess = Nothing
    Me.ldtDate.DisplaySlash = True
    Me.ldtDate.EmptyIfDisabled = True
    Me.ldtDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtDate.Holidays = Nothing
    Me.ldtDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.ldtDate.LabelText = "日付"
    Me.ldtDate.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtDate.Location = New System.Drawing.Point(12, 102)
    Me.ldtDate.MaxLabelLength = 15
    Me.ldtDate.MaxTextLength = 15
    Me.ldtDate.MustInput = False
    Me.ldtDate.Name = "ldtDate"
    Me.ldtDate.Size = New System.Drawing.Size(210, 22)
    Me.ldtDate.TabIndex = 2
    Me.ldtDate.Tag = ""
    Me.ToolTip1.SetToolTip(Me.ldtDate, "検索対象の年月日を入力してください。")
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 708)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1008, 22)
    Me.StatusStrip1.TabIndex = 25
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandSlipSearch, Me.PcaFunctionCommandClose, Me.PcaFunctionCommandCarryOver, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandPrint, Me.PcaFunctionCommandPreview, Me.PcaFunctionCommandOutput, Me.PcaFunctionCommandSave})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 680)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1008, 28)
    Me.PcaFunctionBar1.TabIndex = 44
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    Me.PcaFunctionCommandHelp.ToolTipText = "ヘルプを表示します"
    '
    'PcaFunctionCommandSlipSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSlipSearch, Me.PcaCommandItemSlipSearch)
    Me.PcaFunctionCommandSlipSearch.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandSlipSearch.Tag = Nothing
    Me.PcaFunctionCommandSlipSearch.Text = "再表示"
    Me.PcaFunctionCommandSlipSearch.ToolTipText = "棚卸可能な伝票を表示します。"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    Me.PcaFunctionCommandClose.ToolTipText = "プログラムを終了します。"
    '
    'PcaFunctionCommandCarryOver
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandCarryOver, Me.PcaCommandItemCarryOver)
    Me.PcaFunctionCommandCarryOver.FunctionKey = PCA.Controls.FunctionKey.F4
    Me.PcaFunctionCommandCarryOver.Tag = Nothing
    Me.PcaFunctionCommandCarryOver.Text = "繰越"
    Me.PcaFunctionCommandCarryOver.ToolTipText = "次月への在庫繰越を行います。"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    Me.PcaFunctionCommandRefer.ToolTipText = "マスタを参照します。"
    '
    'PcaFunctionCommandPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrint, Me.PcaCommandItemPrint)
    Me.PcaFunctionCommandPrint.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPrint.Tag = Nothing
    Me.PcaFunctionCommandPrint.Text = "印刷"
    Me.PcaFunctionCommandPrint.ToolTipText = "印刷をします。"
    '
    'PcaFunctionCommandPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPreview, Me.PcaCommandItemPreview)
    Me.PcaFunctionCommandPreview.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPreview.ModifierKeys = PCA.Controls.FunctionModifierKeys.Shift
    Me.PcaFunctionCommandPreview.Tag = Nothing
    Me.PcaFunctionCommandPreview.Text = "プレビュー"
    Me.PcaFunctionCommandPreview.ToolTipText = "プレビューを行います。"
    '
    'PcaFunctionCommandOutput
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandOutput, Me.PcaCommandItemOutput)
    Me.PcaFunctionCommandOutput.FunctionKey = PCA.Controls.FunctionKey.F10
    Me.PcaFunctionCommandOutput.Tag = Nothing
    Me.PcaFunctionCommandOutput.Text = "出力"
    Me.PcaFunctionCommandOutput.ToolTipText = "CSVへの出力を行います。"
    '
    'PcaFunctionCommandSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSave, Me.PcaCommandItemSave)
    Me.PcaFunctionCommandSave.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandSave.Tag = Nothing
    Me.PcaFunctionCommandSave.Text = "登録"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemCarryOver, Me.PcaCommandItemSlipSearch, Me.PcaCommandItemRefer, Me.PcaCommandItemPrint, Me.PcaCommandItemPreview, Me.PcaCommandItemClose, Me.PcaCommandItemOutput, Me.PcaCommandItemSave})
    Me.PcaCommandManager1.Parent = Me
    '
    'PcaCommandItemSave
    '
    Me.PcaCommandItemSave.CommandId = 12
    Me.PcaCommandItemSave.CommandName = "Save"
    '
    'PcaCommandItemPrint
    '
    Me.PcaCommandItemPrint.CommandId = 9
    Me.PcaCommandItemPrint.CommandName = "Print"
    '
    'PcaCommandItemPreview
    '
    Me.PcaCommandItemPreview.CommandId = 9
    Me.PcaCommandItemPreview.CommandName = "Preview"
    '
    'PcaCommandItemOutput
    '
    Me.PcaCommandItemOutput.CommandId = 10
    Me.PcaCommandItemOutput.CommandName = "Output"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    Me.PcaCommandItemClose.CommandName = "Close"
    '
    'PcaCommandItemSlipSearch
    '
    Me.PcaCommandItemSlipSearch.CommandId = 5
    Me.PcaCommandItemSlipSearch.CommandName = "Search"
    '
    'PcaCommandItemCarryOver
    '
    Me.PcaCommandItemCarryOver.CommandId = 4
    Me.PcaCommandItemCarryOver.CommandName = "CarryOver"
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandId = 8
    Me.PcaCommandItemRefer.CommandName = "Refer"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    '
    'tblTable
    '
    Me.tblTable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tblTable.BEKihonJoho = CType(resources.GetObject("tblTable.BEKihonJoho"), PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblTable.ContextMenu = Nothing
    Me.tblTable.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.tblTable.HeadContextMenu = Nothing
    Me.tblTable.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblTable.Location = New System.Drawing.Point(12, 127)
    Me.tblTable.Name = "tblTable"
    Me.tblTable.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblTable.Size = New System.Drawing.Size(984, 529)
    Me.tblTable.TabIndex = 5
    Me.tblTable.UpDownKeyAsTab = True
    '
    'rsetProduct
    '
    Me.rsetProduct.AutoTopMargin = False
    Me.rsetProduct.ClientProcess = Nothing
    Me.rsetProduct.Cursor = System.Windows.Forms.Cursors.Default
    Me.rsetProduct.FillZeroFlag = True
    Me.rsetProduct.LimitLength = 4
    Me.rsetProduct.Location = New System.Drawing.Point(616, 102)
    Me.rsetProduct.MaxCodeLength = 5
    Me.rsetProduct.MaxLabelLength = 15
    Me.rsetProduct.MaxNameLength = 0
    Me.rsetProduct.Name = "rsetProduct"
    Me.rsetProduct.RangeGroupBoxText = "商品"
    Me.rsetProduct.RangeGroupBoxVisible = False
    Me.rsetProduct.Size = New System.Drawing.Size(252, 22)
    Me.rsetProduct.TabIndex = 4
    Me.rsetProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ToolTip1.SetToolTip(Me.rsetProduct, "商品を選択してください。")
    '
    'rsetWarehouse
    '
    Me.rsetWarehouse.AutoTopMargin = False
    Me.rsetWarehouse.ClientProcess = Nothing
    Me.rsetWarehouse.Cursor = System.Windows.Forms.Cursors.Default
    Me.rsetWarehouse.FillZeroFlag = True
    Me.rsetWarehouse.LimitLength = 4
    Me.rsetWarehouse.Location = New System.Drawing.Point(296, 102)
    Me.rsetWarehouse.MaxCodeLength = 5
    Me.rsetWarehouse.MaxLabelLength = 15
    Me.rsetWarehouse.MaxNameLength = 0
    Me.rsetWarehouse.Name = "rsetWarehouse"
    Me.rsetWarehouse.RangeGroupBoxText = "倉庫"
    Me.rsetWarehouse.RangeGroupBoxVisible = False
    Me.rsetWarehouse.Size = New System.Drawing.Size(252, 22)
    Me.rsetWarehouse.TabIndex = 3
    Me.rsetWarehouse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ToolTip1.SetToolTip(Me.rsetWarehouse, "倉庫を選択してください。")
    '
    'rdoPrintType
    '
    Me.rdoPrintType.AutoTopMargin = False
    Me.rdoPrintType.BindingEntity = Nothing
    Me.rdoPrintType.ClientProcess = Nothing
    Me.rdoPrintType.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "棚卸調査票(初回)", 0, "棚卸調査票(初回)"))
    Me.rdoPrintType.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "棚卸調査表", 0, "棚卸調査表"))
    Me.rdoPrintType.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "棚卸表", 0, "棚卸表"))
    Me.rdoPrintType.Location = New System.Drawing.Point(12, 81)
    Me.rdoPrintType.MaxLabelLength = 15
    Me.rdoPrintType.Name = "rdoPrintType"
    Me.rdoPrintType.Size = New System.Drawing.Size(424, 22)
    Me.rdoPrintType.TabIndex = 1
    Me.rdoPrintType.TargetProperty = Nothing
    Me.rdoPrintType.Text = "印刷種類"
    Me.rdoPrintType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdoPrintType.ToolTip = "印刷種類を選択してください。"
    Me.rdoPrintType.XMargin = 10
    '
    'frmStocktaking
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1008, 730)
    Me.Controls.Add(Me.rsetWarehouse)
    Me.Controls.Add(Me.rsetProduct)
    Me.Controls.Add(Me.rdoPrintType)
    Me.Controls.Add(Me.tblTable)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.ldtDate)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = true
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1024, 726)
    Me.Name = "frmStocktaking"
    Me.Text = "棚卸処理"
    Me.MenuStrip1.ResumeLayout(false)
    Me.MenuStrip1.PerformLayout
    Me.ToolStrip1.ResumeLayout(false)
    Me.ToolStrip1.PerformLayout
    Me.ResumeLayout(false)
    Me.PerformLayout

End Sub
  Friend WithEvents ServiceController1 As System.ServiceProcess.ServiceController
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents ToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSave As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonCarryOver As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonShow As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemSlipSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandSlipSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents tblTable As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents ldtDate As Sunloft.PCAControls.SLPcaLabeledDate
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents ToolStripMenuItemSlipSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents PcaCommandItemCarryOver As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPrint As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPreview As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemOutput As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandCarryOver As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandPrint As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandPreview As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandOutput As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolStripButtonPrint As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPreview As System.Windows.Forms.ToolStripButton
  Friend WithEvents PcaCommandItemSave As PCA.Controls.PcaCommandItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPrint As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPreview As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemOutPut As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemCarryOver As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemRefer As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents PcaFunctionCommandSave As PCA.Controls.PcaFunctionCommand
  Friend WithEvents rsetProduct As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents rdoPrintType As PCA.Controls.PcaMultiRadioButton
  Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
  Friend WithEvents rsetWarehouse As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents ToolStripButtonSave As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonOutput As System.Windows.Forms.ToolStripButton

End Class

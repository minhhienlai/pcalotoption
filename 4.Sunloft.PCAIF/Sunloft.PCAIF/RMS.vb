﻿Imports PCA.ApplicationIntegration
Public Class RMS

  Public Property rms_id As Integer = 0
  Public Property rms_tcd As String = String.Empty
  Public Property rms_cmsid As Integer = 0 '相手先マスタへのリンク
  Public Property rms_fmsid As Integer = 0 '先方担当者マスタへのリンク
  Public Property rms_tkbn1 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property rms_tkbn2 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property rms_tkbn3 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property rms_tkbn4 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property rms_tkbn5 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property rms_ttan As String = Str(0).PadLeft(26, CChar("0"))
  Public Property rms_bmn As String = String.Empty

  ''' <summary>performance management</summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks>0:management 1:not management</remarks>
  Public Property rms_jsk As Integer = 0

  ''' <summary>tax calculation</summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks>0:Unnecessary 1:tax included calc 2:tax excluded calc</remarks>
  Public Property rms_kan As Integer = 0
  Public Property rms_oid As Integer = 0
  Public Property rms_ocd As String = String.Empty
  Public Property rms_smb As Integer = 0
  Public Property rms_kai1 As Integer = 0
  Public Property rms_kaik As Integer = 0
  Public Property rms_kai2 As Integer = 0
  Public Property rms_kaib As Integer = 0
  Public Property rms_kaih As Integer = 0
  Public Property rms_khasu As Integer = 0
  Public Property rms_hasu As Integer = 0
  Public Property rms_tax As Integer = 0
  Public Property rms_kanamei As String = String.Empty
  Public Property rms_bankaccountid As Integer = 0
  Public Property rms_bankid As Integer = 0
  Public Property rms_bankbranchid As Integer = 0
  Public Property rms_kkbn As Integer = 0
  Public Property rms_koza As String = String.Empty
  Public Property rms_futan As Integer = 0
  Public Property rms_tskbn As Integer = 0
  Public Property rms_tesu As Integer = 0
  Public Property rms_skamoku As String = String.Empty
  Public Property rms_hkamoku As String = String.Empty
  Public Property rms_kosin As Integer = 0

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication
  Private reader As ICustomDataReader
  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function ReadByCode(ByVal strCode As String) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_RMS")
      selectCommand.Parameters("@rms_tcd").SetValue(strCode)

      reader = selectCommand.ExecuteReader
      If reader.Read Then
        Me.rms_id = CInt(reader.GetValue("rms_id"))
        Me.rms_tcd = reader.GetValue("rms_tcd").ToString
        Me.rms_cmsid = CInt(reader.GetValue("rms_cmsid"))
        Me.rms_fmsid = CInt(reader.GetValue("rms_fmsid"))
        Me.rms_tkbn1 = reader.GetValue("rms_tkbn1").ToString
        Me.rms_tkbn2 = reader.GetValue("rms_tkbn2").ToString
        Me.rms_tkbn3 = reader.GetValue("rms_tkbn3").ToString
        Me.rms_tkbn4 = reader.GetValue("rms_tkbn4").ToString
        Me.rms_tkbn5 = reader.GetValue("rms_tkbn5").ToString
        Me.rms_ttan = reader.GetValue("rms_ttan").ToString
        Me.rms_bmn = reader.GetValue("rms_bmn").ToString
        Me.rms_jsk = CInt(reader.GetValue("rms_jsk"))
        Me.rms_kan = CInt(reader.GetValue("rms_kan"))
        Me.rms_oid = CInt(reader.GetValue("rms_oid"))
        Me.rms_ocd = reader.GetValue("rms_ocd").ToString
        Me.rms_smb = CInt(reader.GetValue("rms_smb"))
        Me.rms_kai1 = CInt(reader.GetValue("rms_kai1"))
        Me.rms_kaik = CInt(reader.GetValue("rms_kaik"))
        Me.rms_kai2 = CInt(reader.GetValue("rms_kai2"))
        Me.rms_kaib = CInt(reader.GetValue("rms_kaib"))
        Me.rms_kaih = CInt(reader.GetValue("rms_kaih"))
        Me.rms_khasu = CInt(reader.GetValue("rms_khasu"))
        Me.rms_hasu = CInt(reader.GetValue("rms_hasu"))
        Me.rms_tax = CInt(reader.GetValue("rms_tax"))
        Me.rms_kanamei = reader.GetValue("rms_kanamei").ToString
        Me.rms_bankaccountid = CInt(reader.GetValue("rms_bankaccountid"))
        Me.rms_bankid = CInt(reader.GetValue("rms_bankid"))
        Me.rms_bankbranchid = CInt(reader.GetValue("rms_bankbranchid"))
        Me.rms_kkbn = CInt(reader.GetValue("rms_kkbn"))
        Me.rms_koza = reader.GetValue("rms_koza").ToString
        Me.rms_futan = CInt(reader.GetValue("rms_futan"))
        Me.rms_tskbn = CInt(reader.GetValue("rms_tskbn"))
        Me.rms_tesu = CInt(reader.GetValue("rms_tesu"))
        Me.rms_skamoku = reader.GetValue("rms_skamoku").ToString
        Me.rms_hkamoku = reader.GetValue("rms_hkamoku").ToString
        Me.rms_kosin = CInt(reader.GetValue("rms_kosin"))
        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

End Class

﻿Imports PCA.ApplicationIntegration
Public Class EMST

  Public Property emst_kbn As String = String.Empty
  Public Property emst_str As String = String.Empty
  Public Property emst_bmn As String = String.Empty
  Public Property emst_kosin As Integer = 0
  Public Property emst_upddate As DateTime = Now

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function ReadByCode(ByVal strCode As String) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand

    Try

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_EMST")
      selectCommand.Parameters("@emst_kbn").SetValue(strCode)

      reader = selectCommand.ExecuteReader
      If reader.Read Then
        Me.emst_kbn = reader.GetValue("emst_kbn").ToString
        Me.emst_str = reader.GetValue("emst_str").ToString
        Me.emst_bmn = reader.GetValue("emst_bmn").ToString
        Me.emst_kosin = CInt(reader.GetValue("emst_kosin"))
        Me.emst_upddate = CDate(reader.GetValue("emst_upddate"))
        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

End Class

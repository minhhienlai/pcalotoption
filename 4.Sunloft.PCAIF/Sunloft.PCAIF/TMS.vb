﻿Imports PCA.ApplicationIntegration
Public Class TMS

  Public Property tms_id As Integer = 0
  Public Property tms_tcd As String = String.Empty
  Public Property tms_cmsid As Integer = 0
  Public Property tms_fmsid As Integer = 0
  Public Property tms_tkbn1 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property tms_tkbn2 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property tms_tkbn3 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property tms_tkbn4 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property tms_tkbn5 As String = Str(0).PadLeft(12, CChar("0"))
  Public Property tms_ttan As String = Str(0).PadLeft(26, CChar("0"))
  Public Property tms_bmn As String = String.Empty
  Public Property tms_bno As Integer = 0
  Public Property tms_kake As Integer = 0
  Public Property tms_jsk As Integer = 0
  Public Property tms_kancyo As Integer = 0
  Public Property tms_kan As Integer = 0
  Public Property tms_cyoku As Integer = 0
  Public Property tms_nyosi As Integer = 0
  Public Property tms_njisya As Integer = 0
  Public Property tms_oid As Integer = 0
  Public Property tms_ocd As String = String.Empty
  Public Property tms_smb As Integer = 0
  Public Property tms_kai1 As Integer = 0
  Public Property tms_kaik As Integer = 0
  Public Property tms_kai2 As Integer = 0
  Public Property tms_kaib As Integer = 0
  Public Property tms_kaih As Integer = 0
  Public Property tms_yosin As Integer = 0
  Public Property tms_khasu As Integer = 0
  Public Property tms_hasu As Integer = 0
  Public Property tms_tax As Integer = 0
  Public Property tms_syosi As Integer = 0
  Public Property tms_sjisya As Integer = 0
  Public Property tms_skamoku As String = String.Empty
  Public Property tms_hkamoku As String = String.Empty

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub



  Public Function ReadByCode(ByVal strCode As String) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_TMS")
      selectCommand.Parameters("@tms_tcd").SetValue(strCode)

      reader = selectCommand.ExecuteReader
      If reader.Read Then
        Me.tms_id = CInt(reader.GetValue("tms_id"))
        Me.tms_tcd = reader.GetValue("tms_tcd").ToString
        Me.tms_cmsid = CInt(reader.GetValue("tms_cmsid"))
        Me.tms_fmsid = CInt(reader.GetValue("tms_fmsid"))
        Me.tms_tkbn1 = reader.GetValue("tms_tkbn1").ToString
        Me.tms_tkbn2 = reader.GetValue("tms_tkbn2").ToString
        Me.tms_tkbn3 = reader.GetValue("tms_tkbn3").ToString
        Me.tms_tkbn4 = reader.GetValue("tms_tkbn4").ToString
        Me.tms_tkbn5 = reader.GetValue("tms_tkbn5").ToString
        Me.tms_ttan = reader.GetValue("tms_ttan").ToString
        Me.tms_bmn = reader.GetValue("tms_bmn").ToString
        Me.tms_bno = CInt(reader.GetValue("tms_bno"))
        Me.tms_kake = CInt(reader.GetValue("tms_kake"))

        Me.tms_jsk = CInt(reader.GetValue("tms_jsk"))
        Me.tms_kancyo = CInt(reader.GetValue("tms_kancyo"))
        Me.tms_kan = CInt(reader.GetValue("tms_kan"))

        Me.tms_cyoku = CInt(reader.GetValue("tms_cyoku"))
        Me.tms_nyosi = CInt(reader.GetValue("tms_nyosi"))
        Me.tms_njisya = CInt(reader.GetValue("tms_njisya"))

        Me.tms_oid = CInt(reader.GetValue("tms_oid"))
        Me.tms_ocd = reader.GetValue("tms_ocd").ToString
        Me.tms_smb = CInt(reader.GetValue("tms_smb"))
        Me.tms_kai1 = CInt(reader.GetValue("tms_kai1"))
        Me.tms_kaik = CInt(reader.GetValue("tms_kaik"))
        Me.tms_kai2 = CInt(reader.GetValue("tms_kai2"))
        Me.tms_kaib = CInt(reader.GetValue("tms_kaib"))
        Me.tms_kaih = CInt(reader.GetValue("tms_kaih"))
        Me.tms_yosin = CInt(reader.GetValue("tms_yosin"))
        Me.tms_khasu = CInt(reader.GetValue("tms_khasu"))
        Me.tms_hasu = CInt(reader.GetValue("tms_hasu"))
        Me.tms_tax = CInt(reader.GetValue("tms_tax"))
        'Me.tms_syosi = CInt(reader.GetValue("tms_syosi"))
        'Me.tms_sjisya = CInt(reader.GetValue("tms_sjisya"))
        Me.tms_skamoku = reader.GetValue("tms_skamoku").ToString
        Me.tms_hkamoku = reader.GetValue("tms_hkamoku").ToString

        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function NoticeTax(argTMS As TMS) As String
    Dim retString As String = String.Empty

    Try
      Select Case argTMS.tms_tax
        Case 0
          retString = "免税"
        Case 1
          retString = "請求書一括"
        Case 2
          retString = "納品書毎"
        Case 3
          retString = "明細単位"
      End Select

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)

    End Try

    Return retString
  End Function

End Class

﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Common

Public Class SL_GDO
  Inherits clsPCAIFBase

  Public Property sl_gdo_id As Int32
  Public Property sl_gdo_title As String
  Public Property sl_gdo_template_name As String
  Public Property sl_gdo_use_date As Int16
  Public Property sl_gdo_date_title As String
  Public Property sl_gdo_date_name As String
  Public Property sl_gdo_use_code1 As Int16
  Public Property sl_gdo_code_title1 As String
  Public Property sl_gdo_code_name1 As String
  Public Property sl_gdo_code_search_idx1 As New SLConstants.GDOSearchIndex
  Public Property sl_gdo_use_code2 As Int16
  Public Property sl_gdo_code_title2 As String
  Public Property sl_gdo_code_name2 As String
  Public Property sl_gdo_code_search_idx2 As New SLConstants.GDOSearchIndex
  Public Property sl_gdo_use_text1 As Int16
  Public Property sl_gdo_text_title1 As String
  Public Property sl_gdo_text_name1 As String
  Public Property sl_gdo_use_text2 As Int16
  Public Property sl_gdo_text_title2 As String
  Public Property sl_gdo_text_name2 As String
  Public Property sl_gdo_order As Int32
  Public Property sl_gdo_use_flag As Int16

  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete() As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_GDO")
      selectCommand.Parameters("@sl_gdo_id").SetValue(sl_gdo_id)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Try
        Dim isSuccess As Boolean = True
        Try
          Dim selectCommand As ICustomCommand
          selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_GDO")
          selectCommand.Parameters("@sl_gdo_id").SetValue(sl_gdo_id)
          selectCommand.ExecuteNonQuery()
        Catch ex As Exception
          Sunloft.Message.DisplayBox.ShowCritical(ex)
          isSuccess = False
        End Try
        Return isSuccess
        Return True
      Catch ex As Exception
        Sunloft.Message.DisplayBox.ShowCritical(ex)
        Return False
      Finally
      End Try
      Return True

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function ReadByID(ByVal intID As Integer) As Boolean
    Try
      Dim objRet As SL_GDO()

      objRet = ReadAll(intID)
      If objRet Is Nothing Then Return False

      With Me
        .sl_gdo_id = objRet(0).sl_gdo_id
        .sl_gdo_template_name = objRet(0).sl_gdo_template_name

        .sl_gdo_use_date = objRet(0).sl_gdo_use_date
        .sl_gdo_date_title = objRet(0).sl_gdo_date_title
        .sl_gdo_date_name = objRet(0).sl_gdo_date_name

        .sl_gdo_use_code1 = objRet(0).sl_gdo_use_code1
        .sl_gdo_code_title1 = objRet(0).sl_gdo_code_title1
        .sl_gdo_code_name1 = objRet(0).sl_gdo_code_name1
        .sl_gdo_code_search_idx1 = objRet(0).sl_gdo_code_search_idx1

        .sl_gdo_use_code2 = objRet(0).sl_gdo_use_code2
        .sl_gdo_code_title2 = objRet(0).sl_gdo_code_title2
        .sl_gdo_code_name2 = objRet(0).sl_gdo_code_name2
        .sl_gdo_code_search_idx2 = objRet(0).sl_gdo_code_search_idx2

        .sl_gdo_use_text1 = objRet(0).sl_gdo_use_text1
        .sl_gdo_text_title1 = objRet(0).sl_gdo_text_title1
        .sl_gdo_text_name1 = objRet(0).sl_gdo_text_name1

        .sl_gdo_use_text2 = objRet(0).sl_gdo_use_text2
        .sl_gdo_text_title2 = objRet(0).sl_gdo_text_title2
        .sl_gdo_text_name2 = objRet(0).sl_gdo_text_name2

        .sl_gdo_order = objRet(0).sl_gdo_order
        .sl_gdo_use_flag = objRet(0).sl_gdo_use_flag

      End With

      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function


  Public Function ReadAll(Optional ByVal intID As Integer = -1, Optional ByVal isOnlyUse As Boolean = True) As SL_GDO()
    Try
      Dim SL_GDOList As SL_GDO()
      Dim reader As ICustomDataReader
      Dim intCount = 0
      Dim selectCommand As ICustomCommand
      Dim intOnlyUse As Integer = If(isOnlyUse, 1, 0)

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_GDO")
      If intID > 0 Then selectCommand.Parameters("@sl_gdo_id").SetValue(intID)
      selectCommand.Parameters("@sl_gdo_use_flag").SetValue(intOnlyUse)

      reader = selectCommand.ExecuteReader
      ReDim SL_GDOList(1)

      SL_GDOList(0) = New SL_GDO(connector)

      While reader.Read()
        ReDim Preserve SL_GDOList(intCount + 1)
        SL_GDOList(intCount) = New SL_GDO(connector)
        If IsNumeric(reader.GetValue("sl_gdo_id")) Then SL_GDOList(intCount).sl_gdo_id = CInt(reader.GetValue("sl_gdo_id"))
        SL_GDOList(intCount).sl_gdo_title = reader.GetValue("sl_gdo_title").ToString
        SL_GDOList(intCount).sl_gdo_template_name = reader.GetValue("sl_gdo_template_name").ToString

        SL_GDOList(intCount).sl_gdo_use_date = CShort(reader.GetValue("sl_gdo_use_date"))
        SL_GDOList(intCount).sl_gdo_date_title = reader.GetValue("sl_gdo_date_title").ToString
        SL_GDOList(intCount).sl_gdo_date_name = reader.GetValue("sl_gdo_date_name").ToString

        SL_GDOList(intCount).sl_gdo_use_code1 = CShort(reader.GetValue("sl_gdo_use_code1"))
        SL_GDOList(intCount).sl_gdo_code_title1 = reader.GetValue("sl_gdo_code_title1").ToString
        SL_GDOList(intCount).sl_gdo_code_name1 = reader.GetValue("sl_gdo_code_name1").ToString
        SL_GDOList(intCount).sl_gdo_code_search_idx1 = DirectCast(reader.GetValue("sl_gdo_code_search_idx1"), SLConstants.GDOSearchIndex)


        SL_GDOList(intCount).sl_gdo_use_code2 = CShort(reader.GetValue("sl_gdo_use_code2"))
        SL_GDOList(intCount).sl_gdo_code_title2 = reader.GetValue("sl_gdo_code_title2").ToString
        SL_GDOList(intCount).sl_gdo_code_name2 = reader.GetValue("sl_gdo_code_name2").ToString
        SL_GDOList(intCount).sl_gdo_code_search_idx2 = DirectCast(reader.GetValue("sl_gdo_code_search_idx2"), SLConstants.GDOSearchIndex)

        SL_GDOList(intCount).sl_gdo_use_text1 = CShort(reader.GetValue("sl_gdo_use_text1"))
        SL_GDOList(intCount).sl_gdo_text_title1 = reader.GetValue("sl_gdo_text_title1").ToString
        SL_GDOList(intCount).sl_gdo_text_name1 = reader.GetValue("sl_gdo_text_name1").ToString

        SL_GDOList(intCount).sl_gdo_use_text2 = CShort(reader.GetValue("sl_gdo_use_text2"))
        SL_GDOList(intCount).sl_gdo_text_title2 = reader.GetValue("sl_gdo_text_title2").ToString
        SL_GDOList(intCount).sl_gdo_text_name2 = reader.GetValue("sl_gdo_text_name2").ToString

        SL_GDOList(intCount).sl_gdo_order = CInt(reader.GetValue("sl_gdo_order"))
        SL_GDOList(intCount).sl_gdo_use_flag = CShort(reader.GetValue("sl_gdo_use_flag"))
        intCount += 1
      End While
      reader.Close()
      reader.Dispose()
      If intCount = 0 Then
        ReDim SL_GDOList(0)
      Else
        ReDim Preserve SL_GDOList(intCount - 1)
      End If

      Return SL_GDOList
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
    End Try
  End Function

  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim intMaxID As Integer = -1
    Dim selectCommand As ICustomCommand
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_GDO_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        sl_gdo_id = CInt(reader.GetValue("sl_gdo_id"))
      End If
      Return sl_gdo_id + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

End Class

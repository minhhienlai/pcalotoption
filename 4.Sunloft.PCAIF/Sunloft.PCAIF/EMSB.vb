﻿Imports PCA.ApplicationIntegration
Public Class EMSB

  Public Property emsb_kbn As String = String.Empty
  Public Property emsb_str As String = String.Empty
  Public Property emsb_souko As String = String.Empty
  Public Property emsb_siwake As String = String.Empty
  Public Property emsb_kosin As Integer = 0
  Public Property emsb_upddate As DateTime = Now

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication


  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function ReadByCode(ByVal strCode As String) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_EMSB")
      selectCommand.Parameters("@emsb_kbn").SetValue(strCode)

      reader = selectCommand.ExecuteReader
      If reader.Read Then
        Me.emsb_kbn = reader.GetValue("emsb_kbn").ToString
        Me.emsb_str = reader.GetValue("emsb_str").ToString
        Me.emsb_souko = reader.GetValue("emsb_souko").ToString
        Me.emsb_siwake = reader.GetValue("emsb_siwake").ToString
        Me.emsb_kosin = CInt(reader.GetValue("emsb_kosin"))
        Me.emsb_upddate = CDate(reader.GetValue("emsb_upddate"))
        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

End Class

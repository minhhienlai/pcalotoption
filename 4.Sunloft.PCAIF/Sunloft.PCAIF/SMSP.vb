﻿Imports PCA.ApplicationIntegration
Public Class SMSP
  Private selectCommand As ICustomCommand
  Private reader As ICustomDataReader
  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Property smsp_tax As Integer
  Public Property smsp_komi As Integer
  Public Property smsp_hyo As Decimal
  Public Property smsp_gen As Decimal
  Public Property smsp_bai1 As Decimal
  Public Property smsp_bai2 As Decimal
  Public Property smsp_bai3 As Decimal
  Public Property smsp_bai4 As Decimal
  Public Property smsp_bai5 As Decimal
  Public Property smsp_sitan As Decimal

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function ReadByID(ByVal productCode As String) As Boolean
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SMSP")
      selectCommand.Parameters("@smsp_scd").SetValue(productCode)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        smsp_komi = CInt(reader.GetValue("smsp_komi"))
        smsp_tax = CInt(reader.GetValue("smsp_tax"))
        smsp_hyo = CDec(reader.GetValue("smsp_hyo"))
        smsp_gen = CDec(reader.GetValue("smsp_gen"))
        smsp_bai1 = CDec(reader.GetValue("smsp_bai1"))
        smsp_bai2 = CDec(reader.GetValue("smsp_bai2"))
        smsp_bai3 = CDec(reader.GetValue("smsp_bai3"))
        smsp_bai4 = CDec(reader.GetValue("smsp_bai4"))
        smsp_bai5 = CDec(reader.GetValue("smsp_bai5"))
        smsp_sitan = CDec(reader.GetValue("smsp_sitan"))
        Return True
      Else
        Return False
      End If

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

End Class

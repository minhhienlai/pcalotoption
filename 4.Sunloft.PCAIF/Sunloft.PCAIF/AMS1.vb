﻿Imports PCA.ApplicationIntegration
Imports System.Text
Public Class AMS1
  Inherits clsPCAIFBase
  'ams1_int0 = 1 record
  '基本情報
  Public Property ams1_key As Integer = 0
  Public Property ams1_ProductLength As Integer = 0
  Public Property ams1_CustomerLength As Integer = 0
  Public Property ams1_SupplierLength As Integer = 0

  Public Property ams1_DirectDeliveryLength As Integer = 0
  Public Property ams1_ShippingDesLength As Integer = 0
  Public Property ams1_DepartmentLength As Integer = 0
  Public Property ams1_PersonLength As Integer = 0
  Public Property ams1_ProductTypeLength As Integer = 0
  Public Property ams1_CustomerTypeLength As Integer = 0
  Public Property ams1_SupplierTypeLength As Integer = 0
  Public Property ams1_MemoLength As Integer = 0
  Public Property ams1_WarehouseLength As Integer = 0

  Public Property ams1_TimeStart As Integer = 0
  Public Property ams1_TimeEnd As Integer = 0
    Public Property ams1_cutOffDate As Integer = 0

    Public Property companyName As String
    Public Property companyPostalCode As String
    Public Property companyAddress1 As String
    Public Property companyAddress2 As String
    Public Property companyTel As String
    Public Property companyFAX As String

  'ams1_int0 = 2 record
  '区分分類名称
  Public Property ams1_TypeName As String

  Public Property CustomerType1 As String
  Public Property CustomerType2 As String
  Public Property CustomerType3 As String
  Public Property CustomerType4 As String
  Public Property CustomerType5 As String

  Public Property ProductType1 As String
  Public Property ProductType2 As String
  Public Property ProductType3 As String
  Public Property ProductType4 As String
  Public Property ProductType5 As String

  Public Property SupplierType1 As String
  Public Property SupplierType2 As String
  Public Property SupplierType3 As String
  Public Property SupplierType4 As String
  Public Property SupplierType5 As String

  'ams1_int0 = 8 record
  '項目の名称
  Public Property ams1_ItemName As String

  Public Property UnitName As String
  Public Property QttPerCaseName As String
  Public Property NoOfCaseName As String
  Public Property SpecName As String
  Public Property ColorName As String

  Public Property SizeName As String
  Public Property ProjectName As String
  Public Property ProductCode2Name As String
  Public Property ProductCode3Name As String
  Public Property Estimation2Name As String

  Public Property OrderedNo2Name As String
  Public Property SalesSlipNo2Name As String
  Public Property OrderNo2Name As String
  Public Property PurchaseSlipNo2Name As String
  Public Property CompanyCode1Name As String
  Public Property CompanyCode2Name As String
  Public Property ProductName2Name As String

  Private Property selectCommand As ICustomCommand = Nothing
  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private Property myConnector As IIntegratedApplication = Nothing

  Private Const BASIC_INFO As Integer = 1
  Private Const TYPE_NAME As Integer = 2
  Private Const ITEM_NAME As Integer = 8

  Public Sub New(ByVal app As IIntegratedApplication)
    Try
      myConnector = app
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function ReadByID(ByVal intID As Integer) As Boolean 'Only 1 row exists
    Return Nothing
  End Function

  Public Shared Function SubstringByte(ByVal value As String, ByVal startIndex As Integer, ByVal length As Integer) As String
    Dim sjisEnc As Encoding = Encoding.GetEncoding("Shift_JIS")
    Dim byteArray() As Byte = sjisEnc.GetBytes(value)

    If byteArray.Length < startIndex + 1 Then
      Return ""
    End If

    If byteArray.Length < startIndex + length Then
      length = byteArray.Length - startIndex
    End If

    Dim cut As String = sjisEnc.GetString(byteArray, startIndex, length)

    ' 最初の文字が全角の途中で切れていた場合はカット
    Dim left As String = sjisEnc.GetString(byteArray, 0, startIndex + 1)
    Dim first As Char = value(left.Length - 1)
    If 0 < cut.Length AndAlso Not first = cut(0) Then
      cut = cut.Substring(1)
    End If

    ' 最後の文字が全角の途中で切れていた場合はカット
    left = sjisEnc.GetString(byteArray, 0, startIndex + length)

    Dim last As Char = value(left.Length - 1)
    If 0 < cut.Length AndAlso Not last = cut(cut.Length - 1) Then
      cut = cut.Substring(0, cut.Length - 1)
    End If

    Return cut
  End Function

  Public Function ReadAll() As Boolean
    Dim reader As ICustomDataReader
    selectCommand = myConnector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_AMS1")
    reader = selectCommand.ExecuteReader
    Try
      While reader.Read()

        ams1_key = CInt(reader.GetValue("ams1_intg0"))

        If ams1_key = BASIC_INFO Then

          ams1_ProductLength = CInt(reader.GetValue("ams1_intg3"))
          ams1_CustomerLength = CInt(reader.GetValue("ams1_intg4"))
          ams1_SupplierLength = CInt(reader.GetValue("ams1_intg5"))

          ams1_DirectDeliveryLength = CInt(reader.GetValue("ams1_intg51"))
          ams1_ShippingDesLength = CInt(reader.GetValue("ams1_intg52"))
          ams1_DepartmentLength = CInt(reader.GetValue("ams1_intg53"))
          ams1_PersonLength = CInt(reader.GetValue("ams1_intg54"))
          ams1_ProductTypeLength = CInt(reader.GetValue("ams1_intg55"))
          ams1_CustomerTypeLength = CInt(reader.GetValue("ams1_intg56"))
          ams1_SupplierTypeLength = CInt(reader.GetValue("ams1_intg57"))
          ams1_MemoLength = CInt(reader.GetValue("ams1_intg58"))
          ams1_WarehouseLength = CInt(reader.GetValue("ams1_intg59"))

          ams1_TimeStart = CInt(reader.GetValue("ams1_lng1"))
          ams1_TimeEnd = CInt(reader.GetValue("ams1_lng2"))
          ams1_cutOffDate = CInt(reader.GetValue("ams1_lng16"))

                    ams1_TypeName = reader.GetValue("ams1_str").ToString
                    companyName = SubstringByte(ams1_TypeName, 98, 40)
                    companyPostalCode = SubstringByte(ams1_TypeName, 178, 8)
                    companyAddress1 = SubstringByte(ams1_TypeName, 186, 40)
                    companyAddress2 = SubstringByte(ams1_TypeName, 226, 40)
                    companyTel = SubstringByte(ams1_TypeName, 346, 26)
                    companyFAX = SubstringByte(ams1_TypeName, 372, 26)

        ElseIf ams1_key = TYPE_NAME Then
          ams1_TypeName = reader.GetValue("ams1_str").ToString

          CustomerType1 = SubstringByte(ams1_TypeName, 0, 10)
          CustomerType2 = SubstringByte(ams1_TypeName, 10, 10)
          CustomerType3 = SubstringByte(ams1_TypeName, 20, 10)
          CustomerType4 = SubstringByte(ams1_TypeName, 30, 10)
          CustomerType5 = SubstringByte(ams1_TypeName, 40, 10)

          ProductType1 = SubstringByte(ams1_TypeName, 50, 10)
          ProductType2 = SubstringByte(ams1_TypeName, 60, 10)
          ProductType3 = SubstringByte(ams1_TypeName, 70, 10)
          ProductType4 = SubstringByte(ams1_TypeName, 80, 10)
          ProductType5 = SubstringByte(ams1_TypeName, 90, 10)

          SupplierType1 = SubstringByte(ams1_TypeName, 100, 10)
          SupplierType2 = SubstringByte(ams1_TypeName, 110, 10)
          SupplierType3 = SubstringByte(ams1_TypeName, 120, 10)
          SupplierType4 = SubstringByte(ams1_TypeName, 130, 10)
          SupplierType5 = SubstringByte(ams1_TypeName, 140, 10)

        ElseIf ams1_key = ITEM_NAME Then

          ams1_ItemName = reader.GetValue("ams1_str").ToString()

          UnitName = SubstringByte(ams1_ItemName, 0, 4)
          QttPerCaseName = SubstringByte(ams1_ItemName, 4, 4)
          NoOfCaseName = SubstringByte(ams1_ItemName, 8, 4)
          SpecName = SubstringByte(ams1_ItemName, 12, 10)
          ColorName = SubstringByte(ams1_ItemName, 22, 7)

          SizeName = SubstringByte(ams1_ItemName, 29, 5)
          ProjectName = SubstringByte(ams1_ItemName, 34, 10)
          ProductCode2Name = SubstringByte(ams1_ItemName, 44, 10)
          ProductCode3Name = SubstringByte(ams1_ItemName, 54, 10)
          Estimation2Name = SubstringByte(ams1_ItemName, 64, 10)

          OrderedNo2Name = SubstringByte(ams1_ItemName, 74, 10)
          SalesSlipNo2Name = SubstringByte(ams1_ItemName, 84, 10)
          OrderNo2Name = SubstringByte(ams1_ItemName, 94, 10)
          PurchaseSlipNo2Name = SubstringByte(ams1_ItemName, 104, 10)
          CompanyCode1Name = SubstringByte(ams1_ItemName, 114, 10)
          CompanyCode2Name = SubstringByte(ams1_ItemName, 124, 10)
          ProductName2Name = SubstringByte(ams1_ItemName, 134, 10)

        End If

      End While
      reader.Close()
      reader.Dispose()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function


  Public Overrides Function GetNewID() As Integer
    Return Nothing
  End Function
End Class

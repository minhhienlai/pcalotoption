﻿Imports PCA.ApplicationIntegration
Public Class SL_GDOD
  Inherits clsPCAIFBase
  Public Property isExisted As Boolean = False
  Public Property sl_gdod_id As Int32
  Public Property sl_gdod_userid As String
  Public Property sl_gdod_hid As Int32
  Public Property sl_gdod_item_name As String
  Public Property sl_gdod_order As Int32
  Public Property sl_gdod_select_flag As Int16

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Sub New(ByRef myApp As IIntegratedApplication)
    connector = myApp
  End Sub

#Region " INSERT"

  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_GDOD")
      If sl_gdod_id < 0 Then sl_gdod_id = GetNewID() + sl_gdod_order - 1

      selectCommand.Parameters("@sl_gdod_id").SetValue(sl_gdod_id)
      selectCommand.Parameters("@sl_gdod_userid").SetValue(sl_gdod_userid)
      selectCommand.Parameters("@sl_gdod_hid").SetValue(sl_gdod_hid)
      selectCommand.Parameters("@sl_gdod_item_name").SetValue(sl_gdod_item_name)
      selectCommand.Parameters("@sl_gdod_order").SetValue(sl_gdod_order)
      selectCommand.Parameters("@sl_gdod_select_flag").SetValue(sl_gdod_select_flag)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region " UPDATE"

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try

      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region " DELETE"
  Public Overrides Function Delete() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      Dim intDeletedRow As Integer
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_GDOD")

      selectCommand.Parameters("@sl_gdod_userid").SetValue(sl_gdod_userid)
      selectCommand.Parameters("@sl_gdod_id").SetValue(sl_gdod_id)
      selectCommand.Parameters("@sl_gdod_hid").SetValue(sl_gdod_hid)
      intDeletedRow = selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try

  End Function

#End Region

#Region "SELECT"
  Public Overrides Function ReadByID(ByVal intID As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_GDOD")
      selectCommand.Parameters("@sl_gdod_id").SetValue(intID)
      selectCommand.Parameters("@sl_gdod_userid").SetValue(String.Empty)
      selectCommand.Parameters("@sl_gdod_hid").SetValue(-1)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        With Me
          .sl_gdod_id = CInt(reader.GetValue("sl_gdod_id"))
          .sl_gdod_userid = reader.GetValue("sl_gdod_userid").ToString
          .sl_gdod_hid = CInt(reader.GetValue("sl_gdod_hid"))
          .sl_gdod_userid = reader.GetValue("sl_gdod_item_name").ToString
          .sl_gdod_hid = CInt(reader.GetValue("sl_gdod_order"))
          .sl_gdod_hid = CShort(reader.GetValue("sl_gdo_select_flag"))
        End With
      End If

      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      reader.Close()
      reader.Dispose()
    End Try
  End Function

  ''' <summary>
  ''' select column data(selected / order by) for pro06020
  ''' </summary>
  ''' <param name="strUserID">user id</param>
  ''' <param name="intGDOID">sl_dgo_id</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ReadColsData(ByVal strUserID As String, ByVal intGDOID As Integer) As SL_GDOD()
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_GDOD")
      selectCommand.Parameters("@sl_gdod_id").SetValue(-1)
      selectCommand.Parameters("@sl_gdod_userid").SetValue(strUserID)
      selectCommand.Parameters("@sl_gdod_hid").SetValue(intGDOID)
      Return ReadAllByCond(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try
  End Function


  Public Function ReadByHeaderID() As SL_GDOD()
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_GDOD_FROM_HID")
      selectCommand.Parameters("@sl_gdod_hid").SetValue(sl_gdod_hid)
      Return ReadAllByCond(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try

  End Function

  Private Function ReadAllByCond(ByRef selectCommand As ICustomCommand) As SL_GDOD()
    Dim reader As ICustomDataReader = Nothing
    Try

      reader = selectCommand.ExecuteReader
      Dim myTableDetails As SL_GDOD() = {New SL_GDOD(connector)}
      Dim intBranchCount As Integer = 0
      While reader.Read()

        ReDim Preserve myTableDetails(intBranchCount + 1)
        myTableDetails(intBranchCount) = New SL_GDOD(connector)

        With myTableDetails(intBranchCount)
          .isExisted = True
          .sl_gdod_id = CInt(reader.GetValue("sl_gdod_id"))
          .sl_gdod_userid = reader.GetValue("sl_gdod_userid").ToString
          .sl_gdod_hid = CInt(reader.GetValue("sl_gdod_hid"))
          .sl_gdod_item_name = reader.GetValue("sl_gdod_item_name").ToString
          .sl_gdod_order = CInt(reader.GetValue("sl_gdod_order"))
          .sl_gdod_select_flag = CShort(reader.GetValue("sl_gdod_select_flag"))
        End With
        intBranchCount += 1
      End While

      If intBranchCount > 0 Then ReDim Preserve myTableDetails(intBranchCount - 1)
      Return myTableDetails
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

#End Region

#Region " Other Methods"
  Public Overrides Function GetNewID() As Integer
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim intMaxID As Integer = -1
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_GDOD_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intMaxID = CInt(reader.GetValue("sl_gdod_id"))

      Return intMaxID + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

#End Region

End Class

﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Common

Public Class SL_NYKH
  Inherits clsPCAIFBase

#Region " Declare"

  Public Property sl_nykh_id As Integer
  Public Property sl_nykh_denno As Integer
  Public Property sl_nykh_return As Integer
  Public Property sl_nykh_uribi As Integer
  Public Property sl_nykh_tcd As String
  Public Property sl_nykh_jtan As String
  Public Property sl_nykh_tekcd As String
  Public Property sl_nykh_tekmei As String
  Public Property sl_nykh_hno As Integer
  Public Property sl_nykh_sno As Integer
  Public Property sl_nykh_denno2 As String
  Public Property sl_nykh_jbmn As String
  Public Property sl_nykh_pjcode As String
  Public Property sl_nykh_datakbn As Integer
  Public Property sl_nykh_dataid As Integer
  Public Property sl_nykh_insuser As String
  Public Property sl_nykh_insdate As DateTime = Nothing
  Public Property sl_nykh_upduser As String
  Public Property sl_nykh_upddate As DateTime = Nothing

  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Private Const NOT_CONVERTED As Integer = 0
  Private Const ALREADY_CONVERTED As Integer = 1

#End Region

#Region " Constructor"

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

#End Region
  
#Region " Insert/Update"

  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      RegisterHeaders(True, session)
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      RegisterHeaders(False, session)
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function RegisterHeaders(isCreate As Boolean, ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      If isCreate Then
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_NYKH")
      Else
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_NYKH")
      End If
      selectCommand.Parameters("@sl_nykh_id").SetValue(sl_nykh_id)
      selectCommand.Parameters("@sl_nykh_denno").SetValue(sl_nykh_denno)
      selectCommand.Parameters("@sl_nykh_return").SetValue(sl_nykh_return)
      selectCommand.Parameters("@sl_nykh_uribi").SetValue(sl_nykh_uribi)
      selectCommand.Parameters("@sl_nykh_tcd").SetValue(sl_nykh_tcd)
      selectCommand.Parameters("@sl_nykh_jtan").SetValue(sl_nykh_jtan)
      selectCommand.Parameters("@sl_nykh_tekcd").SetValue(sl_nykh_tekcd)
      selectCommand.Parameters("@sl_nykh_tekmei").SetValue(sl_nykh_tekmei)
      selectCommand.Parameters("@sl_nykh_hno").SetValue(sl_nykh_hno)
      selectCommand.Parameters("@sl_nykh_sno").SetValue(sl_nykh_sno)
      selectCommand.Parameters("@sl_nykh_denno2").SetValue(sl_nykh_denno2)
      selectCommand.Parameters("@sl_nykh_jbmn").SetValue(sl_nykh_jbmn)
      selectCommand.Parameters("@sl_nykh_pjcode").SetValue(sl_nykh_pjcode)
      selectCommand.Parameters("@sl_nykh_datakbn").SetValue(sl_nykh_datakbn)
      selectCommand.Parameters("@sl_nykh_dataid").SetValue(sl_nykh_dataid)
      If isCreate Then
        selectCommand.Parameters("@sl_nykh_insuser").SetValue(sl_nykh_insuser)
        selectCommand.Parameters("@sl_nykh_insdate").SetValue(sl_nykh_insdate)
      Else
        selectCommand.Parameters("@sl_nykh_upduser").SetValue(sl_nykh_upduser)
        selectCommand.Parameters("@sl_nykh_upddate").SetValue(sl_nykh_upddate)
      End If
      selectCommand.ExecuteNonQuery()
      Return (True)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function UpdateDetailsAfterConvert(ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_NYKH_Convert")
      selectCommand.Parameters("@sl_nykh_denno").SetValue(sl_nykh_denno)
      selectCommand.Parameters("@sl_nykd_convert").SetValue(ALREADY_CONVERTED)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function
 
#End Region

#Region " Delete"

  Public Overrides Function Delete() As Boolean
    Try
      Dim session As ICustomSession = connector.CreateTransactionalSession
      Dim isSuccess As Boolean = Me.DeleteHeader(session)
      If isSuccess Then
        isSuccess = DeleteDetails(session)
      End If
      If isSuccess Then
        session.Commit()
        session.Dispose()
        Return True
      Else
        session.Rollback()
        session.Dispose()
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function DeleteHeader(ByRef session As ICustomSession) As Boolean
    Try
      Dim isSuccess As Boolean = True
      Try
        Dim selectCommand As ICustomCommand
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_NYKH")
        selectCommand.Parameters("@sl_nykh_id").SetValue(sl_nykh_id)
        selectCommand.ExecuteNonQuery()
      Catch ex As Exception
        Sunloft.Message.DisplayBox.ShowCritical(ex)
        isSuccess = False
      End Try
      Return isSuccess
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function DeleteDetails(ByRef session As ICustomSession) As Boolean
    Dim isSuccess As Boolean = True
    Try
      Dim selectCommand As ICustomCommand
      'Delete data in Warehousing Details Data SL_NYKD and Current Inventory Details Data SL_ZDN
      'Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      'Get all Details ID, delete corresponding data in SL_ZDN
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKD")
      selectCommand.Parameters("@sl_nykd_hid").SetValue(sl_nykh_id)
      reader = selectCommand.ExecuteReader

      While reader.Read() = True
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_ZDN")
        selectCommand.Parameters("@sl_zdn_dataid").SetValue(reader.GetValue("sl_nykd_id"))
        selectCommand.Parameters("@sl_zdn_datakbn").SetValue(SLConstants.SL_ZDNSlipType.Warehousing)
        selectCommand.ExecuteNonQuery()
      End While
      reader.Close()
      reader.Dispose()

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_NYKD_FROM_HID")
      selectCommand.Parameters("@sl_nykd_hid").SetValue(sl_nykh_id)
      selectCommand.ExecuteNonQuery()

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try

      Return True

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region " Read"

  Public Overrides Function ReadByID(ByVal intSlipNo As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKH")
      selectCommand.Parameters("@sl_nykh_denno").SetValue(intSlipNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        'Get header data to class
        If IsNumeric(reader.GetValue("sl_nykh_id")) Then
          sl_nykh_id = CInt(reader.GetValue("sl_nykh_id"))
        End If
        If IsNumeric(reader.GetValue("sl_nykh_denno")) Then
          sl_nykh_denno = CInt(reader.GetValue("sl_nykh_denno"))
        End If

        sl_nykh_denno2 = reader.GetValue("sl_nykh_denno2").ToString

        If IsNumeric(reader.GetValue("sl_nykh_return")) Then
          sl_nykh_return = CInt(reader.GetValue("sl_nykh_return"))
        End If

        If IsNumeric(reader.GetValue("sl_nykh_uribi")) Then
          sl_nykh_uribi = CInt(reader.GetValue("sl_nykh_uribi"))
        End If

        sl_nykh_tcd = reader.GetValue("sl_nykh_tcd").ToString

        If IsNumeric(reader.GetValue("sl_nykh_hno")) Then
          sl_nykh_hno = CInt(reader.GetValue("sl_nykh_hno"))
        End If

        If IsNumeric(reader.GetValue("sl_nykh_sno")) Then
          sl_nykh_sno = CInt(reader.GetValue("sl_nykh_sno"))
        End If

        sl_nykh_jtan = reader.GetValue("sl_nykh_jtan").ToString
        sl_nykh_jbmn = reader.GetValue("sl_nykh_jbmn").ToString
        sl_nykh_tekcd = reader.GetValue("sl_nykh_tekcd").ToString
        sl_nykh_tekmei = reader.GetValue("sl_nykh_tekmei").ToString
        sl_nykh_pjcode = reader.GetValue("sl_nykh_pjcode").ToString

        If Not Sunloft.Common.SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_nykh_datakbn")) Then sl_nykh_datakbn = CInt(reader.GetValue("sl_nykh_datakbn"))
        If Not Sunloft.Common.SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_nykh_datakbn")) Then sl_nykh_dataid = CInt(reader.GetValue("sl_nykh_dataid"))

        sl_nykh_insuser = reader.GetValue("sl_nykh_insuser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_nykh_insdate").ToString) Then
          sl_nykh_insdate = CDate(reader.GetValue("sl_nykh_insdate"))
        End If

        sl_nykh_upduser = reader.GetValue("sl_nykh_upduser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_nykh_upddate").ToString) Then
          sl_nykh_upddate = CDate(reader.GetValue("sl_nykh_upddate"))
        End If
        Return True
      Else
        Return False
      End If
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function ReadAll() As Collection

    Dim reader As ICustomDataReader = Nothing
    Try
      Dim result As Collection = New Collection
      Dim SL_NYKHitem As SL_NYKH
      Dim intCount = 0
      Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKH")
      reader = selectCommand.ExecuteReader
      While reader.Read() = True
        SL_NYKHitem = New SL_NYKH(Nothing)
        SL_NYKHitem.sl_nykh_id = Integer.Parse(reader.GetValue("sl_nykh_id").ToString)
        SL_NYKHitem.sl_nykh_denno = Integer.Parse(reader.GetValue("sl_nykh_denno").ToString)
        SL_NYKHitem.sl_nykh_return = Integer.Parse(reader.GetValue("sl_nykh_return").ToString)
        SL_NYKHitem.sl_nykh_uribi = Integer.Parse(reader.GetValue("sl_nykh_uribi").ToString)
        SL_NYKHitem.sl_nykh_tcd = reader.GetValue("sl_nykh_tcd").ToString
        SL_NYKHitem.sl_nykh_jtan = reader.GetValue("sl_nykh_jtan").ToString
        SL_NYKHitem.sl_nykh_tekcd = reader.GetValue("sl_nykh_tekcd").ToString
        SL_NYKHitem.sl_nykh_tekmei = reader.GetValue("sl_nykh_tekmei").ToString
        SL_NYKHitem.sl_nykh_hno = Integer.Parse(reader.GetValue("sl_nykh_hno").ToString)
        SL_NYKHitem.sl_nykh_sno = Integer.Parse(reader.GetValue("sl_nykh_sno").ToString)
        SL_NYKHitem.sl_nykh_denno2 = reader.GetValue("sl_nykh_denno2").ToString
        SL_NYKHitem.sl_nykh_jbmn = reader.GetValue("sl_nykh_jbmn").ToString
        SL_NYKHitem.sl_nykh_pjcode = reader.GetValue("sl_nykh_pjcode").ToString
        SL_NYKHitem.sl_nykh_datakbn = Integer.Parse(reader.GetValue("sl_nykh_datakbn").ToString)
        SL_NYKHitem.sl_nykh_dataid = Integer.Parse(reader.GetValue("sl_nykh_dataid").ToString)
        SL_NYKHitem.sl_nykh_insuser = reader.GetValue("sl_nykh_insuser").ToString.Trim
        If Not IsDBNull(reader.GetValue("sl_nykh_insdate")) Then
          SL_NYKHitem.sl_nykh_insdate = DateTime.Parse(reader.GetValue("sl_nykh_insdate").ToString)
        End If
        SL_NYKHitem.sl_nykh_upduser = reader.GetValue("sl_nykh_upduser").ToString.Trim
        If Not IsDBNull(reader.GetValue("sl_nykh_upddate")) Then
          SL_NYKHitem.sl_nykh_upddate = DateTime.Parse(reader.GetValue("sl_nykh_upddate").ToString)
        End If
        result.Add(SL_NYKHitem)
      End While
      Return result
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

#End Region

#Region " Other Method"

  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim intMaxID As Integer = -1
    Dim selectCommand As ICustomCommand
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_NYKH_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        sl_nykh_id = CInt(reader.GetValue("sl_nykh_id"))
      End If
      Return sl_nykh_id + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function GetNewSlipID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim intMaxID As Integer = -1
    Dim selectCommand As ICustomCommand
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_NYKH_SLIPID")
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        sl_nykh_denno = CInt(reader.GetValue("sl_nykh_denno"))
      End If
      Return sl_nykh_denno + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function GetPrevSlipNo(ByVal intCurrentDenno As Integer) As Integer
    Dim reader As ICustomDataReader
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKH_PREVIOUS_SLIP")
    selectCommand.Parameters("@sl_nykh_denno").SetValue(intCurrentDenno)
    reader = selectCommand.ExecuteReader
    If reader.Read() = True Then
      sl_nykh_denno = CInt(reader.GetValue("sl_nykh_denno"))
      reader.Close()
      reader.Dispose()
      Return sl_nykh_denno
    End If
    reader.Close()
    reader.Dispose()
    Return 0
  End Function

  Public Function GetNextSlipNo(ByVal intCurrentDenno As Integer) As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand

    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKH_NEXT_SLIP")
      selectCommand.Parameters("@sl_nykh_denno").SetValue(intCurrentDenno)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then sl_nykh_denno = CInt(reader.GetValue("sl_nykh_denno"))

      Return sl_nykh_denno
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return 0
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try

  End Function

  Public Function CheckConvertedSlip() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intRetSlipNo As Integer = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKH_IS_CONVERTED")
      selectCommand.Parameters("@sl_nykh_id").SetValue(sl_nykh_id)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intRetSlipNo = CInt(reader.GetValue("nykh_denno"))

      Return intRetSlipNo
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return 0
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

#End Region

End Class

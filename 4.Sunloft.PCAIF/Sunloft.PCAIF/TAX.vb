﻿Imports PCA.ApplicationIntegration
Public Class TAX
  Public Property tax_rate1 As Integer
  Public Property tax_rate2 As Integer
  Public Property tax_rate3 As Integer
  Public Property tax_rate4 As Integer
  Public Property tax_rate5 As Integer
  Public Property tax_rate6 As Integer
  Public Property tax_rate7 As Integer
  Public Property tax_rate8 As Integer
  Public Property tax_rate9 As Integer
  Private selectCommand As ICustomCommand
  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication
  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Sub ReadTaxrateByDate(ByVal intDate As Integer)
    Dim result As Collection = New Collection
    Dim reader As ICustomDataReader
    Dim intCount = 0
    selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_TAX")
    selectCommand.Parameters("@intDate").SetValue(intDate)
    reader = selectCommand.ExecuteReader
    While reader.Read() = True
      tax_rate1 = CInt(reader.GetValue("tax_rate1"))
      tax_rate2 = CInt(reader.GetValue("tax_rate2"))
      tax_rate3 = CInt(reader.GetValue("tax_rate3"))
      tax_rate4 = CInt(reader.GetValue("tax_rate4"))
      tax_rate5 = CInt(reader.GetValue("tax_rate5"))
      tax_rate6 = CInt(reader.GetValue("tax_rate6"))
      tax_rate7 = CInt(reader.GetValue("tax_rate7"))
      tax_rate8 = CInt(reader.GetValue("tax_rate8"))
      tax_rate9 = CInt(reader.GetValue("tax_rate9"))
      reader.Close()
      reader.Dispose()
      Return
    End While
  End Sub
End Class

﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Common

Public Class SL_SYKH
  Inherits clsPCAIFBase

#Region " Declare"
  Public Property sl_sykh_id As Integer
  Public Property sl_sykh_denno As Integer
  Public Property sl_sykh_return As Integer
  Public Property sl_sykh_uribi As Integer
  Public Property sl_sykh_nouki As Integer
  Public Property sl_sykh_tcd As String
  Public Property sl_sykh_carrier As String
  Public Property sl_sykh_jtan As String
  Public Property sl_sykh_tekcd As String
  Public Property sl_sykh_tekmei As String
  Public Property sl_sykh_denno2 As String
  Public Property sl_sykh_pjcode As String
  Public Property sl_sykh_jbmn As String
  Public Property sl_sykh_datakbn As Sunloft.Common.SLConstants.SL_SYKH_RelaySlipType
  Public Property sl_sykh_dataid As Integer
  Public Property sl_sykh_insuser As String
  Public Property sl_sykh_insdate As DateTime = Nothing
  Public Property sl_sykh_upduser As String
  Public Property sl_sykh_upddate As DateTime = Nothing

  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Private Const NOT_CONVERTED As Integer = 0
  Private Const ALREADY_CONVERTED As Integer = 1

#End Region

#Region " Constructor"

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

#End Region

#Region " Insert/Update"

  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      Return RegisterHeaders(True, session)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      Return RegisterHeaders(False, session)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function RegisterHeaders(isCreate As Boolean, ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      If isCreate Then
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_SYKH")
      Else
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_SYKH")
      End If
      selectCommand.Parameters("@sl_sykh_id").SetValue(sl_sykh_id)
      selectCommand.Parameters("@sl_sykh_denno").SetValue(sl_sykh_denno)
      selectCommand.Parameters("@sl_sykh_return").SetValue(sl_sykh_return)
      selectCommand.Parameters("@sl_sykh_uribi").SetValue(sl_sykh_uribi)
      selectCommand.Parameters("@sl_sykh_nouki").SetValue(sl_sykh_nouki)
      selectCommand.Parameters("@sl_sykh_tcd").SetValue(sl_sykh_tcd)
      selectCommand.Parameters("@sl_sykh_carrier").SetValue(sl_sykh_carrier)
      selectCommand.Parameters("@sl_sykh_jtan").SetValue(sl_sykh_jtan)
      selectCommand.Parameters("@sl_sykh_tekcd").SetValue(sl_sykh_tekcd)
      selectCommand.Parameters("@sl_sykh_tekmei").SetValue(sl_sykh_tekmei)
      selectCommand.Parameters("@sl_sykh_denno2").SetValue(sl_sykh_denno2)

      selectCommand.Parameters("@sl_sykh_pjcode").SetValue(sl_sykh_pjcode)
      selectCommand.Parameters("@sl_sykh_jbmn").SetValue(sl_sykh_jbmn)
      selectCommand.Parameters("@sl_sykh_datakbn").SetValue(sl_sykh_datakbn)
      selectCommand.Parameters("@sl_sykh_dataid").SetValue(sl_sykh_dataid)
      If isCreate Then
        selectCommand.Parameters("@sl_sykh_insuser").SetValue(sl_sykh_insuser)
        selectCommand.Parameters("@sl_sykh_insdate").SetValue(sl_sykh_insdate)
      Else
        selectCommand.Parameters("@sl_sykh_upduser").SetValue(sl_sykh_upduser)
        selectCommand.Parameters("@sl_sykh_upddate").SetValue(sl_sykh_upddate)
      End If
      selectCommand.ExecuteNonQuery()
      Return (True)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function UpdateDetailsAfterConvert(ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_SYKH_Convert")
      selectCommand.Parameters("@sl_sykh_denno").SetValue(sl_sykh_denno)
      selectCommand.Parameters("@sl_sykd_convert").SetValue(ALREADY_CONVERTED)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

#End Region

#Region " Delete"

  Public Overrides Function Delete() As Boolean
    Try
      Dim session As ICustomSession = connector.CreateTransactionalSession
      Dim isSuccess As Boolean = Me.DeleteHeader(session)
      If isSuccess Then
        isSuccess = DeleteDetails(session)
      End If
      If isSuccess Then
        session.Commit()
        session.Dispose()
        Return True
      Else
        session.Rollback()
        session.Dispose()
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  ''' Delete Header Data 
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns>none</returns>
  ''' <remarks>Delete Header Data (Table)</remarks>
  Private Function DeleteHeader(ByRef session As ICustomSession) As Boolean
    Try
      Dim isSuccess As Boolean = True
      Try
        Dim selectCommand As ICustomCommand
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_SYKH")
        selectCommand.Parameters("@sl_sykh_id").SetValue(sl_sykh_id)
        selectCommand.ExecuteNonQuery()
      Catch ex As Exception
        Sunloft.Message.DisplayBox.ShowCritical(ex)
        isSuccess = False
      End Try
      Return isSuccess
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  ''' Delete Details
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns>IsSuccess</returns>
  ''' <remarks>Delete Details Data (Table)</remarks>
  Private Function DeleteDetails(ByRef session As ICustomSession) As Boolean
    Dim isSuccess As Boolean = True
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_SYKD_FROM_HID")
      selectCommand.Parameters("@sl_sykd_hid").SetValue(sl_sykh_id)
      selectCommand.ExecuteNonQuery()

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function

  ''' <summary>
  ''' Delete
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns>IsSuccess</returns>
  ''' <remarks>Delete Data (In Edit mode, Delete -> Insert new data. Or when user chooses to delete data)</remarks>
  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try

      Return True

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region " READ"

  Public Overrides Function ReadByID(ByVal intSlipNo As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SYKH")
      selectCommand.Parameters("@sl_sykh_denno").SetValue(intSlipNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        'Get header data to class
        If IsNumeric(reader.GetValue("sl_sykh_id")) Then sl_sykh_id = CInt(reader.GetValue("sl_sykh_id"))

        If IsNumeric(reader.GetValue("sl_sykh_denno")) Then sl_sykh_denno = CInt(reader.GetValue("sl_sykh_denno"))

        If IsNumeric(reader.GetValue("sl_sykh_return")) Then sl_sykh_return = CInt(reader.GetValue("sl_sykh_return"))
        If IsNumeric(reader.GetValue("sl_sykh_uribi")) Then sl_sykh_uribi = CInt(reader.GetValue("sl_sykh_uribi"))
        If IsNumeric(reader.GetValue("sl_sykh_nouki")) Then sl_sykh_nouki = CInt(reader.GetValue("sl_sykh_nouki"))

        sl_sykh_tcd = reader.GetValue("sl_sykh_tcd").ToString
        sl_sykh_carrier = reader.GetValue("sl_sykh_carrier").ToString
        sl_sykh_jtan = reader.GetValue("sl_sykh_jtan").ToString
        sl_sykh_tekcd = reader.GetValue("sl_sykh_tekcd").ToString
        sl_sykh_tekmei = reader.GetValue("sl_sykh_tekmei").ToString
        sl_sykh_denno2 = reader.GetValue("sl_sykh_denno2").ToString
        sl_sykh_pjcode = reader.GetValue("sl_sykh_pjcode").ToString
        sl_sykh_jbmn = reader.GetValue("sl_sykh_jbmn").ToString

        If Not Sunloft.Common.SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_sykh_datakbn")) Then
          sl_sykh_datakbn = CType(reader.GetValue("sl_sykh_datakbn"), Sunloft.Common.SLConstants.SL_SYKH_RelaySlipType)
        End If

        If Not Sunloft.Common.SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_sykh_dataid")) Then sl_sykh_dataid = CInt(reader.GetValue("sl_sykh_dataid"))

        sl_sykh_insuser = reader.GetValue("sl_sykh_insuser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_sykh_insdate").ToString) Then
          sl_sykh_insdate = CDate(reader.GetValue("sl_sykh_insdate"))
        End If

        sl_sykh_upduser = reader.GetValue("sl_sykh_upduser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_sykh_upddate").ToString) Then
          sl_sykh_upddate = CDate(reader.GetValue("sl_sykh_upddate"))
        End If
        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function ReadAll() As Collection
    Try
      Dim result As Collection = New Collection
      Dim SL_SYKHitem As SL_SYKH
      Dim reader As ICustomDataReader
      Dim intCount = 0
      Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SYKH")
      reader = selectCommand.ExecuteReader
      While reader.Read() = True
        SL_SYKHitem = New SL_SYKH(Nothing)
        SL_SYKHitem.sl_sykh_id = Integer.Parse(reader.GetValue("sl_sykh_id").ToString)
        SL_SYKHitem.sl_sykh_denno = Integer.Parse(reader.GetValue("sl_sykh_denno").ToString)
        SL_SYKHitem.sl_sykh_return = Integer.Parse(reader.GetValue("sl_sykh_return").ToString)
        SL_SYKHitem.sl_sykh_uribi = Integer.Parse(reader.GetValue("sl_sykh_uribi").ToString)
        SL_SYKHitem.sl_sykh_nouki = Integer.Parse(reader.GetValue("sl_sykh_nouki").ToString)
        SL_SYKHitem.sl_sykh_tcd = reader.GetValue("sl_sykh_tcd").ToString
        SL_SYKHitem.sl_sykh_carrier = reader.GetValue("sl_sykh_carrier").ToString
        SL_SYKHitem.sl_sykh_jtan = reader.GetValue("sl_sykh_jtan").ToString
        SL_SYKHitem.sl_sykh_tekcd = reader.GetValue("sl_sykh_tekcd").ToString
        SL_SYKHitem.sl_sykh_tekmei = reader.GetValue("sl_sykh_tekmei").ToString
        SL_SYKHitem.sl_sykh_denno2 = reader.GetValue("sl_sykh_denno2").ToString
        SL_SYKHitem.sl_sykh_pjcode = reader.GetValue("sl_sykh_pjcode").ToString
        SL_SYKHitem.sl_sykh_jbmn = reader.GetValue("sl_sykh_jbmn").ToString
        SL_SYKHitem.sl_sykh_datakbn = CType(reader.GetValue("sl_sykh_datakbn"), Sunloft.Common.SLConstants.SL_SYKH_RelaySlipType)
        SL_SYKHitem.sl_sykh_dataid = Integer.Parse(reader.GetValue("sl_sykh_dataid").ToString)
        SL_SYKHitem.sl_sykh_insuser = reader.GetValue("sl_sykh_insuser").ToString.Trim
        If Not IsDBNull(reader.GetValue("sl_sykh_insdate")) Then
          SL_SYKHitem.sl_sykh_insdate = DateTime.Parse(reader.GetValue("sl_sykh_insdate").ToString)
        End If
        SL_SYKHitem.sl_sykh_upduser = reader.GetValue("sl_sykh_upduser").ToString.Trim
        If Not IsDBNull(reader.GetValue("sl_sykh_upddate")) Then
          SL_SYKHitem.sl_sykh_upddate = DateTime.Parse(reader.GetValue("sl_sykh_upddate").ToString)
        End If
        result.Add(SL_SYKHitem)
      End While
      reader.Close()
      reader.Dispose()
      Return result
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
    End Try
  End Function

#End Region

#Region " Other Method"

  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim intMaxID As Integer = -1
    Dim selectCommand As ICustomCommand
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_SYKH_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        sl_sykh_id = CInt(reader.GetValue("sl_sykh_id"))
      End If
      Return sl_sykh_id + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return 0
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function GetNewSlipID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim intMaxID As Integer = -1
    Dim selectCommand As ICustomCommand
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_SYKH_SLIPID")
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        sl_sykh_denno = CInt(reader.GetValue("sl_sykh_denno"))
      End If
      Return sl_sykh_denno + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return 0
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function GetPrevSlipNo(ByVal intCurrentDenno As Integer) As Integer
    Dim reader As ICustomDataReader
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SYKH_PREVIOUS_SLIP")
    selectCommand.Parameters("@sl_sykh_denno").SetValue(intCurrentDenno)
    reader = selectCommand.ExecuteReader
    If reader.Read() = True Then
      sl_sykh_denno = CInt(reader.GetValue("sl_sykh_denno"))
      reader.Close()
      reader.Dispose()
      Return sl_sykh_denno
    End If
    reader.Close()
    reader.Dispose()
    Return 0
  End Function

  Public Function GetNextSlipNo(ByVal intCurrentDenno As Integer) As Integer
    Dim reader As ICustomDataReader
    Dim selectCommand As ICustomCommand
    selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SYKH_NEXT_SLIP")
    selectCommand.Parameters("@sl_sykh_denno").SetValue(intCurrentDenno)
    reader = selectCommand.ExecuteReader
    If reader.Read() = True Then
      sl_sykh_denno = CInt(reader.GetValue("sl_sykh_denno"))
      reader.Close()
      reader.Dispose()
      Return sl_sykh_denno
    End If
    reader.Close()
    reader.Dispose()
    Return 0
  End Function

  Public Function CheckConvertedSlip() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intRetSlipNo As Integer = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SYKH_IS_CONVERTED")
      selectCommand.Parameters("@sl_sykh_id").SetValue(sl_sykh_id)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intRetSlipNo = CInt(reader.GetValue("sykh_denno"))

      Return intRetSlipNo
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return 0
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

#End Region

End Class

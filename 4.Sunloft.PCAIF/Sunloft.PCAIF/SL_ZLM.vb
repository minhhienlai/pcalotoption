﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Common
Imports Sunloft.Message

Public Class SL_ZLM
  Inherits clsPCAIFBase
  Private connector As IIntegratedApplication

  Public Property sl_zlm_id As Int32 = 0
  Public Property sl_zlm_month As Int32 = 0
  Public Property sl_zlm_zdn_id As Int32 = 0
  Public Property sl_zlm_souko As String = String.Empty
  Public Property sl_zlm_honsu As Decimal = 0D
  Public Property sl_zlm_suryo As Decimal = 0D
  Public Property sl_zlm_nyukohonsu As Decimal = 0D
  Public Property sl_zlm_nyukosuryo As Decimal = 0D
  Public Property sl_zlm_syukohonsu As Decimal = 0D
  Public Property sl_zlm_syukosuryo As Decimal = 0D
  Public Property sl_zlm_zaikohonsu As Decimal = 0D
  Public Property sl_zlm_zaikosuryo As Decimal = 0D
  Public Property sl_zlm_tanka As Decimal = 0D
  Public Property sl_zlm_kingaku As Decimal = 0D
  Public Property sl_zlm_insuser As String = String.Empty
  Public Property sl_zlm_insdate As DateTime
  Public Property sl_zlm_upduser As String = String.Empty
  Public Property sl_zlm_upddate As DateTime

  Private _int_zadj_iokbn As SLConstants.SL_ZADJType = 0

  Public Property sl_zlm_whFrom As String = String.Empty
  Public Property sl_zlm_whTo As String = String.Empty
  Public Property sl_zlm_ProductFrom As String = String.Empty
  Public Property sl_zlm_ProductTo As String = String.Empty
  Public WriteOnly Property sl_zadj_iokbn As SLConstants.SL_ZADJType
    Set(value As SLConstants.SL_ZADJType)
      _int_zadj_iokbn = value
    End Set
  End Property

  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Public Sub New(ByRef myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  ''' <summary>Get New Slip ID</summary>
  ''' <returns>New Slip ID</returns>
  ''' <remarks></remarks>
  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intMaxId As Integer = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_ZLM_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intMaxId = CInt(reader.GetValue("sl_zlm_id"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
    Return intMaxId + 1
  End Function

  ''' <summary>get last date number and quantity</summary>
  ''' <remarks></remarks>
  Private Sub GetLastDataNumbers()
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_LASTDATA_SL_ZLM")
      reader = selectCommand.ExecuteReader
      If reader.Read Then
        Me.sl_zlm_suryo = CDec(reader.GetValue("sl_zlm_suryo"))
        Me.sl_zlm_honsu = CInt(reader.GetValue("sl_zlm_honsu"))
        Me.sl_zlm_nyukohonsu = CDec(reader.GetValue("cur_honsu"))
        Me.sl_zlm_nyukosuryo = CInt(reader.GetValue("cur_suryo"))
        Me.sl_zlm_syukohonsu = CDec(reader.GetValue("whhonsu"))
        Me.sl_zlm_syukosuryo = CInt(reader.GetValue("whsuryo"))
      Else
        Me.sl_zlm_suryo = 0
        Me.sl_zlm_honsu = 0
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Sub

#Region "Insert"

  Public Overrides Function Insert() As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_ZLM")
      sl_zlm_id = GetNewID()

      selectCommand.Parameters("@sl_zlm_id").SetValue(sl_zlm_id)
      selectCommand.Parameters("@sl_zlm_month").SetValue(sl_zlm_month)
      selectCommand.Parameters("@sl_zlm_zdn_id").SetValue(sl_zlm_zdn_id)
      selectCommand.Parameters("@sl_zlm_souko").SetValue(sl_zlm_souko)
      selectCommand.Parameters("@sl_zlm_honsu").SetValue(sl_zlm_honsu)
      selectCommand.Parameters("@sl_zlm_suryo").SetValue(sl_zlm_suryo)
      selectCommand.Parameters("@sl_zlm_nyukohonsu").SetValue(sl_zlm_nyukohonsu)
      selectCommand.Parameters("@sl_zlm_nyukosuryo").SetValue(sl_zlm_nyukosuryo)
      selectCommand.Parameters("@sl_zlm_syukohonsu").SetValue(sl_zlm_syukohonsu)
      selectCommand.Parameters("@sl_zlm_syukosuryo").SetValue(sl_zlm_syukosuryo)
      selectCommand.Parameters("@sl_zlm_zaikohonsu").SetValue(sl_zlm_zaikohonsu)
      selectCommand.Parameters("@sl_zlm_zaikosuryo").SetValue(sl_zlm_zaikosuryo)
      selectCommand.Parameters("@sl_zlm_tanka").SetValue(sl_zlm_tanka)
      selectCommand.Parameters("@sl_zlm_kingaku").SetValue(sl_zlm_kingaku)
      selectCommand.Parameters("@sl_zlm_insuser").SetValue(sl_zlm_insuser)
      selectCommand.Parameters("@sl_zlm_insdate").SetValue(sl_zlm_insdate)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>Insert[SL_ZLM]</summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks>在庫調整データ(SL_ZLM)に登録します。</remarks>
  Public Overrides Function Insert(ByRef session As PCA.ApplicationIntegration.ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_ZLM")
      sl_zlm_id = GetNewID()
      GetLastDataNumbers()

      selectCommand.Parameters("@sl_zlm_id").SetValue(sl_zlm_id)
      selectCommand.Parameters("@sl_zlm_month").SetValue(sl_zlm_month)
      selectCommand.Parameters("@sl_zlm_zdn_id").SetValue(sl_zlm_zdn_id)
      selectCommand.Parameters("@sl_zlm_souko").SetValue(sl_zlm_souko)
      selectCommand.Parameters("@sl_zlm_honsu").SetValue(sl_zlm_honsu)
      selectCommand.Parameters("@sl_zlm_suryo").SetValue(sl_zlm_suryo)
      If _int_zadj_iokbn = SLConstants.SL_ZADJType.StocktakingPlus Then
        selectCommand.Parameters("@sl_zlm_nyukohonsu").SetValue(sl_zlm_nyukohonsu)
        selectCommand.Parameters("@sl_zlm_nyukosuryo").SetValue(sl_zlm_nyukosuryo)
      Else
        selectCommand.Parameters("@sl_zlm_nyukohonsu").SetValue(0)
        selectCommand.Parameters("@sl_zlm_nyukosuryo").SetValue(0)
      End If
      selectCommand.Parameters("@sl_zlm_syukohonsu").SetValue(sl_zlm_syukohonsu)
      selectCommand.Parameters("@sl_zlm_syukosuryo").SetValue(sl_zlm_syukosuryo)
      selectCommand.Parameters("@sl_zlm_zaikohonsu").SetValue(sl_zlm_zaikohonsu)
      selectCommand.Parameters("@sl_zlm_zaikosuryo").SetValue(sl_zlm_zaikosuryo)
      selectCommand.Parameters("@sl_zlm_tanka").SetValue(sl_zlm_tanka)
      selectCommand.Parameters("@sl_zlm_kingaku").SetValue(sl_zlm_kingaku)
      selectCommand.Parameters("@sl_zlm_insuser").SetValue(sl_zlm_insuser)
      selectCommand.Parameters("@sl_zlm_insdate").SetValue(sl_zlm_insdate)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "Update"
  Public Overrides Function Update() As Boolean
    Return False
  End Function

  ''' <summary>Update[SL_ZLM]</summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks>月別ロット在庫データ(SL_ZLM)を更新します。</remarks>
  Public Overrides Function Update(ByRef session As PCA.ApplicationIntegration.ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_ZLM")
      selectCommand.Parameters("@sl_zlm_id").SetValue(sl_zlm_id)
      selectCommand.Parameters("@sl_zlm_month").SetValue(sl_zlm_month)
      selectCommand.Parameters("@sl_zlm_zdn_id").SetValue(sl_zlm_zdn_id)
      selectCommand.Parameters("@sl_zlm_souko").SetValue(sl_zlm_souko)
      selectCommand.Parameters("@sl_zlm_honsu").SetValue(sl_zlm_honsu)
      selectCommand.Parameters("@sl_zlm_suryo").SetValue(sl_zlm_suryo)
      selectCommand.Parameters("@sl_zlm_nyukohonsu").SetValue(sl_zlm_honsu)
      selectCommand.Parameters("@sl_zlm_nyukosuryo").SetValue(sl_zlm_suryo)
      selectCommand.Parameters("@sl_zlm_syukohonsu").SetValue(sl_zlm_honsu)
      selectCommand.Parameters("@sl_zlm_syukosuryo").SetValue(sl_zlm_suryo)
      selectCommand.Parameters("@sl_zlm_zaikohonsu").SetValue(sl_zlm_honsu)
      selectCommand.Parameters("@sl_zlm_zaikosuryo").SetValue(sl_zlm_suryo)
      selectCommand.Parameters("@sl_zlm_tanka").SetValue(sl_zlm_tanka)
      selectCommand.Parameters("@sl_zlm_kingaku").SetValue(sl_zlm_kingaku)
      selectCommand.Parameters("@sl_zlm_upduser").SetValue(sl_zlm_upduser)
      selectCommand.Parameters("@sl_zlm_upddate").SetValue(sl_zlm_upddate)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "Delete"

  Public Overrides Function Delete() As Boolean
    Return False
  End Function

  ''' <summary>在庫調整データ(SL_ZLM)から削除します。</summary>
  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_ZLM")
      selectCommand.Parameters("@sl_zlm_month").SetValue(sl_zlm_month)
      selectCommand.Parameters("@sl_zlm_soukoF").SetValue(sl_zlm_whFrom)
      selectCommand.Parameters("@sl_zlm_soukoT").SetValue(sl_zlm_whTo)
      selectCommand.Parameters("@sl_zdn_productF").SetValue(sl_zlm_ProductFrom)
      selectCommand.Parameters("@sl_zdn_productT").SetValue(sl_zlm_ProductTo)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "SELECT"

  Public Overrides Function ReadByID(intID As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZLM")
    Dim isSuccess As Boolean = False

    Try
      selectCommand.Parameters("@sl_zlm_id").SetValue(intID)
      reader = selectCommand.ExecuteReader

      If reader.Read() Then

        If IsNumeric(reader.GetValue("sl_zlm_id")) Then sl_zlm_id = CInt(reader.GetValue("sl_zlm_id"))
        sl_zlm_month = CInt(reader.GetValue("sl_zlm_month"))
        sl_zlm_zdn_id = CInt(reader.GetValue("sl_zlm_zdn_id").ToString)
        sl_zlm_souko = reader.GetValue("sl_zlm_souko").ToString
        sl_zlm_honsu = CInt(reader.GetValue("sl_zlm_honsu").ToString)
        sl_zlm_suryo = CDec(reader.GetValue("sl_zlm_suryo").ToString)
        sl_zlm_nyukohonsu = CInt(reader.GetValue("sl_zlm_nyukohonsu").ToString)
        sl_zlm_nyukosuryo = CDec(reader.GetValue("sl_zlm_nyukosuryo").ToString)
        sl_zlm_syukohonsu = CInt(reader.GetValue("sl_zlm_syukohonsu").ToString)
        sl_zlm_syukosuryo = CDec(reader.GetValue("sl_zlm_syukosuryo").ToString)
        sl_zlm_zaikohonsu = CInt(reader.GetValue("sl_zlm_zaikohonsu").ToString)
        sl_zlm_zaikosuryo = CDec(reader.GetValue("sl_zlm_zaikosuryo").ToString)
        sl_zlm_insuser = reader.GetValue("sl_zlm_insuser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_zlm_insdate").ToString) Then
          sl_zlm_insdate = CDate(reader.GetValue("sl_zlm_insdate"))
        Else
          sl_zlm_insdate = Nothing
        End If

        sl_zlm_upduser = reader.GetValue("sl_zlm_upduser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_zlm_upddate").ToString) Then
          sl_zlm_upddate = CDate(reader.GetValue("sl_zlm_upddate"))
        Else
          sl_zlm_upddate = Nothing
        End If

        isSuccess = True
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    Finally
      reader.Close()
      reader.Dispose()

    End Try
    Return isSuccess
  End Function

#End Region
End Class

﻿Imports PCA.ApplicationIntegration
Public Class SMS
  'Add to shared const
  Private Const TAX_INCLUDED As String = Sunloft.Common.SLConstants.TAX.TAX_INCLUDED
  Private Const TAX_EXCLUDED As String = Sunloft.Common.SLConstants.TAX.TAX_EXCLUDED
  Private Const TAX_INCLUDED_CODE As Integer = Sunloft.Common.SLConstants.TAX.TAX_INCLUDED_CODE
  Private Const TAX_EXCLUDED_CODE As Integer = Sunloft.Common.SLConstants.TAX.TAX_EXCLUDED_CODE

  Public Property sms_scd As String = ""
  Public Property sms_mei As String = ""
  Public Property sms_kikaku As String = ""
  Public Property sms_color As String = ""
  Public Property sms_size As String = ""
  Public Property sms_souko As String = ""
  Public Property sms_iri As Integer = 0
  Public Property sms_sketa As Integer = 0
  Public Property sms_tketa As Integer = 0
  Public Property sms_tani As String = ""
  Public Property smsp_tax As Integer = 0
  Public Property smsp_komi As String = ""
  Public Property smsp_sitan As Integer = 0
  Public Property smsp_gentan As Integer = 0
  Public Property sms_AllItemName As String = String.Empty
  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function ReadByID(ByVal productCode As String) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      Dim intCount = 0
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SMS")
      selectCommand.Parameters("@sms_scd").SetValue(productCode)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        sms_scd = reader.GetValue("sms_scd").ToString().Trim
        sms_mei = reader.GetValue("sms_mei").ToString().Trim
        sms_kikaku = reader.GetValue("sms_kikaku").ToString().Trim
        sms_color = reader.GetValue("sms_color").ToString().Trim
        sms_size = reader.GetValue("sms_size").ToString().Trim
        sms_souko = reader.GetValue("sms_souko").ToString().Trim
        sms_iri = CInt(reader.GetValue("sms_iri"))
        sms_sketa = CInt(reader.GetValue("sms_sketa"))
        sms_tani = reader.GetValue("sms_tani").ToString().Trim
        smsp_sitan = CInt(reader.GetValue("smsp_sitan"))
        sms_tketa = CInt(reader.GetValue("sms_tketa"))
        smsp_gentan = CInt(reader.GetValue("smsp_gen"))
        smsp_tax = CInt(reader.GetValue("smsp_tax"))
        sms_AllItemName = sms_mei & (" " & sms_kikaku).TrimEnd & (" " & sms_color).TrimEnd & (" " & sms_size).TrimEnd
        Select Case reader.GetValue("smsp_komi").ToString().Trim
          Case TAX_EXCLUDED_CODE.ToString
            smsp_komi = TAX_EXCLUDED
          Case TAX_INCLUDED_CODE.ToString
            smsp_komi = TAX_INCLUDED
        End Select
        Return True
      Else
        Return False
      End If

    Catch ex As Exception
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try

    
  End Function

End Class

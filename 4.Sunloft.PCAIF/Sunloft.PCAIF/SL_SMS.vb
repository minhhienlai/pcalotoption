﻿Imports PCA.ApplicationIntegration
Public Class SL_SMS
  Inherits clsPCAIFBase
  Public Property sl_sms_id As Integer
  Public Property sl_sms_scd As String = ""
  Public Property sl_sms_lkbn As Integer = 0
  Public Property sl_sms_bbmini As Integer
  Public Property sl_sms_bbdini As Integer
  Public Property sl_sms_sdlmini As Integer
  Public Property sl_sms_sdldini As Integer
  Public Property sl_sms_ubmini As Integer
  Public Property sl_sms_ubdini As Integer
  Public Property sl_sms_expmini As Integer
  Public Property sl_sms_expdini As Integer
  Public Property sl_sms_gw As Decimal
  Public Property InsertedCnt As Integer

  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function ReadByProductCode(ByVal productCode As String) As Boolean
    Dim result As Collection = New Collection
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intCount = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SMS")
      selectCommand.Parameters("@sl_sms_scd").SetValue(productCode)
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        sl_sms_scd = reader.GetValue("sl_sms_scd").ToString.Trim
        sl_sms_lkbn = CInt(reader.GetValue("sl_sms_lkbn"))
        sl_sms_bbmini = CInt(reader.GetValue("sl_sms_bbmini"))
        sl_sms_bbdini = CInt(reader.GetValue("sl_sms_bbdini"))
        sl_sms_sdlmini = CInt(reader.GetValue("sl_sms_sdlmini"))
        sl_sms_sdldini = CInt(reader.GetValue("sl_sms_sdldini"))
        sl_sms_ubmini = CInt(reader.GetValue("sl_sms_ubmini"))
        sl_sms_ubdini = CInt(reader.GetValue("sl_sms_ubdini"))
        sl_sms_expmini = CInt(reader.GetValue("sl_sms_expmini"))
        sl_sms_expdini = CInt(reader.GetValue("sl_sms_expdini"))
        sl_sms_gw = CDec(reader.GetValue("sl_sms_gw"))
        Return True
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
    Return False
  End Function

  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Try
      Dim intMaxID As Integer = -1
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_SMS_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        intMaxID = CInt(reader.GetValue("sl_sms_id"))
      End If
      Return intMaxID + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Try
      If sl_sms_lkbn = 1 Then
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_SMS")
        selectCommand.Parameters("@sl_sms_id").SetValue(GetNewID() + InsertedCnt)
        selectCommand.Parameters("@sl_sms_scd").SetValue(sl_sms_scd)
        selectCommand.Parameters("@sl_sms_lkbn").SetValue(sl_sms_lkbn)
        selectCommand.Parameters("@sl_sms_bbmini").SetValue(sl_sms_bbmini)
        selectCommand.Parameters("@sl_sms_bbdini").SetValue(sl_sms_bbdini)
        selectCommand.Parameters("@sl_sms_sdlmini").SetValue(sl_sms_sdlmini)
        selectCommand.Parameters("@sl_sms_sdldini").SetValue(sl_sms_sdldini)
        selectCommand.Parameters("@sl_sms_ubmini").SetValue(sl_sms_ubmini)
        selectCommand.Parameters("@sl_sms_ubdini").SetValue(sl_sms_ubdini)
        selectCommand.Parameters("@sl_sms_expmini").SetValue(sl_sms_expmini)
        selectCommand.Parameters("@sl_sms_expdini").SetValue(sl_sms_expdini)
        selectCommand.Parameters("@sl_sms_gw").SetValue(sl_sms_gw)
        selectCommand.ExecuteNonQuery()
      End If
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Try
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_SMS")
      selectCommand.Parameters("@sl_sms_scd").SetValue(sl_sms_scd)
      selectCommand.Parameters("@sl_sms_lkbn").SetValue(sl_sms_lkbn)
      selectCommand.Parameters("@sl_sms_bbmini").SetValue(sl_sms_bbmini)
      selectCommand.Parameters("@sl_sms_bbdini").SetValue(sl_sms_bbdini)
      selectCommand.Parameters("@sl_sms_sdlmini").SetValue(sl_sms_sdlmini)
      selectCommand.Parameters("@sl_sms_sdldini").SetValue(sl_sms_sdldini)
      selectCommand.Parameters("@sl_sms_ubmini").SetValue(sl_sms_ubmini)
      selectCommand.Parameters("@sl_sms_ubdini").SetValue(sl_sms_ubdini)
      selectCommand.Parameters("@sl_sms_expmini").SetValue(sl_sms_expmini)
      selectCommand.Parameters("@sl_sms_expdini").SetValue(sl_sms_expdini)
      selectCommand.Parameters("@sl_sms_gw").SetValue(sl_sms_gw)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Try
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_SMS")
      selectCommand.Parameters("@sl_sms_scd").SetValue(sl_sms_scd)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function ReadByID(ByVal intID As Integer) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

End Class

﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Common
Imports Sunloft.Message

Public Class SL_WHT
  Inherits clsPCAIFBase
  Private connector As IIntegratedApplication

  Public Property sl_wht_id As Integer = 0
  Public Property sl_wht_denno As Integer = 0
  Public Property sl_wht_denno2 As String = String.Empty
  Public Property sl_wht_date As Integer = 0
  Public Property sl_wht_dlvdate As Integer = 0
  Public Property sl_wht_scd As String = String.Empty
  Public Property sl_wht_soukof As String = String.Empty
  Public Property sl_wht_soukot As String = String.Empty
  Public Property sl_wht_suryo As Decimal = 0
  Public Property sl_wht_honsu As Decimal = 0
  Public Property sl_wht_tekcd As String = String.Empty
  Public Property sl_wht_tekmei As String = String.Empty
  Public Property sl_wht_insuser As String = ""
  Public Property sl_wht_insdate As DateTime
  Public Property sl_wht_upduser As String = ""
  Public Property sl_wht_upddate As DateTime
  Public Property sl_wht_soukoMei As String = ""

  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Public Sub New(ByRef myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function GetAllocatedOnlyTrans(ByRef curAllocatedNumber As Decimal, ByRef curAllocatedQuantity As Decimal, ByVal intSlipNo As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing

    Try
      Dim selectCommand As ICustomCommand

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_ALLOCATED_FOR_TRANS")
      selectCommand.Parameters("@SlipNo").SetValue(intSlipNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        curAllocatedNumber = CDec(reader.GetValue("sum_allocation_number"))
        curAllocatedQuantity = CDec(reader.GetValue("sum_allocation_quantity"))
      End If
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
      reader.Close()
      reader.Dispose()
    End Try
  End Function

  ''' <summary>Get New Slip ID</summary>
  ''' <returns>New Slip ID</returns>
  ''' <remarks></remarks>
  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intMaxId As Integer = -1
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_WHT_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intMaxId = CInt(reader.GetValue("sl_wht_id"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intMaxId + 1
  End Function

  ''' <summary>Get New Slip No</summary>
  ''' <returns>New Slip No</returns>
  ''' <remarks></remarks>
  Private Function GetNewSlipNo() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intMaxNo As Integer = -1
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_WHT_NO")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intMaxNo = CInt(reader.GetValue("sl_wht_denno"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intMaxNo + 1

  End Function

  ''' <summary>Get Next Slip No</summary>  
  ''' <param name="intNextSlipNo"></param>
  ''' <returns>Next Slip No</returns>
  ''' <remarks></remarks>
  Public Function GetNextSlipNo(ByVal intNextSlipNo As Integer) As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intNextNo As Integer = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_WHT_NEXT_SLIP")
      selectCommand.Parameters("@SlipNo").SetValue(intNextSlipNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intNextNo = CInt(reader.GetValue("sl_wht_denno"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intNextNo
  End Function

  ''' <summary>Get Previous Slip No</summary>
  ''' <param name="intPrevSlipNo"></param>
  ''' <returns>Previous Slip No</returns>
  ''' <remarks></remarks>
  Public Function GetPrevSlipNo(ByVal intPrevSlipNo As Integer) As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intPrevNo As Integer = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_WHT_PREVIOUS_SLIP")
      selectCommand.Parameters("@SlipNo").SetValue(intPrevSlipNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intPrevNo = CInt(reader.GetValue("sl_wht_denno"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intPrevNo
  End Function

#Region "Insert"

  Public Overrides Function Insert() As Boolean
    Return False
  End Function

  ''' <summary>Insert[SL_WHT]</summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks>倉庫移動データ(SL_WHT)に登録します。</remarks>
  Public Overrides Function Insert(ByRef session As PCA.ApplicationIntegration.ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_WHT")
      sl_wht_id = GetNewID()
      sl_wht_denno = GetNewSlipNo()

      selectCommand.Parameters("@sl_wht_id").SetValue(sl_wht_id)
      selectCommand.Parameters("@sl_wht_denno").SetValue(sl_wht_denno)
      selectCommand.Parameters("@sl_wht_denno2").SetValue(sl_wht_denno2)
      selectCommand.Parameters("@sl_wht_date").SetValue(sl_wht_date)
      selectCommand.Parameters("@sl_wht_dlvdate").SetValue(sl_wht_dlvdate)
      selectCommand.Parameters("@sl_wht_scd").SetValue(sl_wht_scd)
      selectCommand.Parameters("@sl_wht_soukof").SetValue(sl_wht_soukof)
      selectCommand.Parameters("@sl_wht_soukot").SetValue(sl_wht_soukot)
      selectCommand.Parameters("@sl_wht_suryo").SetValue(sl_wht_suryo)
      selectCommand.Parameters("@sl_wht_honsu").SetValue(sl_wht_honsu)
      selectCommand.Parameters("@sl_wht_tekcd").SetValue(sl_wht_tekcd)
      selectCommand.Parameters("@sl_wht_tekmei").SetValue(sl_wht_tekmei)
      selectCommand.Parameters("@sl_wht_insuser").SetValue(sl_wht_insuser)
      selectCommand.Parameters("@sl_wht_insdate").SetValue(sl_wht_insdate)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "Update"
  Public Overrides Function Update() As Boolean
    Return False
  End Function

  ''' <summary>Update[SL_WHT]</summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks>倉庫移動データ(SL_WHT)を更新します。</remarks>
  Public Overrides Function Update(ByRef session As PCA.ApplicationIntegration.ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_WHT")
      selectCommand.Parameters("@sl_wht_id").SetValue(sl_wht_id)
      selectCommand.Parameters("@sl_wht_denno").SetValue(sl_wht_denno)
      selectCommand.Parameters("@sl_wht_denno2").SetValue(sl_wht_denno2)
      selectCommand.Parameters("@sl_wht_date").SetValue(sl_wht_date)
      selectCommand.Parameters("@sl_wht_dlvdate").SetValue(sl_wht_dlvdate)
      selectCommand.Parameters("@sl_wht_scd").SetValue(sl_wht_scd)
      selectCommand.Parameters("@sl_wht_soukof").SetValue(sl_wht_soukof)
      selectCommand.Parameters("@sl_wht_soukot").SetValue(sl_wht_soukot)
      selectCommand.Parameters("@sl_wht_suryo").SetValue(sl_wht_suryo)
      selectCommand.Parameters("@sl_wht_honsu").SetValue(sl_wht_honsu)

      selectCommand.Parameters("@sl_wht_tekcd").SetValue(sl_wht_tekcd)
      selectCommand.Parameters("@sl_wht_tekmei").SetValue(sl_wht_tekmei)

      selectCommand.Parameters("@sl_wht_upduser").SetValue(sl_wht_upduser)
      selectCommand.Parameters("@sl_wht_upddate").SetValue(sl_wht_upddate)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "Delete"

  Public Overrides Function Delete() As Boolean
    Return False
  End Function

  ''' <summary>倉庫移動データ(SL_WHT)から削除します。</summary>
  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_WHT")
      selectCommand.Parameters("@sl_wht_id").SetValue(sl_wht_id)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "SELECT"

  Public Function ReadByNoOnlyTrans(intSlipNo As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_WHT_BY_NO_Pro04030")
    Dim isSuccess As Boolean = False

    Try
      selectCommand.Parameters("@SlipNo").SetValue(intSlipNo)
      reader = selectCommand.ExecuteReader

      If reader.Read() Then

        sl_wht_id = CInt(reader.GetValue("sl_wht_id"))
        sl_wht_denno = CInt(reader.GetValue("sl_wht_denno"))

        sl_wht_denno2 = reader.GetValue("sl_wht_denno2").ToString

        sl_wht_date = CInt(reader.GetValue("sl_wht_date"))
        sl_wht_dlvdate = CInt(reader.GetValue("sl_wht_dlvdate"))

        sl_wht_scd = reader.GetValue("sl_wht_scd").ToString
        sl_wht_soukof = reader.GetValue("sl_wht_soukof").ToString
        sl_wht_soukot = reader.GetValue("sl_wht_soukot").ToString

        sl_wht_honsu = CInt(reader.GetValue("sl_wht_honsu").ToString)
        sl_wht_suryo = CDec(reader.GetValue("sl_wht_suryo").ToString)

        sl_wht_tekcd = reader.GetValue("sl_wht_tekcd").ToString
        sl_wht_tekmei = reader.GetValue("sl_wht_tekmei").ToString

        sl_wht_insuser = reader.GetValue("sl_wht_insuser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_wht_insdate").ToString) Then
          sl_wht_insdate = CDate(reader.GetValue("sl_wht_insdate"))
        Else
          sl_wht_insdate = Nothing
        End If

        sl_wht_upduser = reader.GetValue("sl_wht_upduser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_wht_upddate").ToString) Then
          sl_wht_upddate = CDate(reader.GetValue("sl_wht_upddate"))
        Else
          sl_wht_upddate = Nothing
        End If

        isSuccess = True
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    Finally
      reader.Close()
      reader.Dispose()

    End Try
    Return isSuccess
  End Function

  Public Overrides Function ReadByID(intID As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_WHT")
    Dim isSuccess As Boolean = False

    Try
      selectCommand.Parameters("@sl_wht_id").SetValue(intID)
      reader = selectCommand.ExecuteReader

      If reader.Read() Then

        If IsNumeric(reader.GetValue("sl_wht_id")) Then sl_wht_id = CInt(reader.GetValue("sl_wht_id"))
        If IsNumeric(reader.GetValue("sl_wht_denno")) Then sl_wht_denno = CInt(reader.GetValue("sl_wht_denno"))

        sl_wht_denno2 = reader.GetValue("sl_wht_denno2").ToString

        sl_wht_date = CInt(reader.GetValue("sl_wht_date"))
        sl_wht_dlvdate = CInt(reader.GetValue("sl_wht_dlvdate"))

        sl_wht_scd = reader.GetValue("sl_wht_scd").ToString
        sl_wht_soukof = reader.GetValue("sl_wht_soukof").ToString
        sl_wht_soukot = reader.GetValue("sl_wht_soukot").ToString

        sl_wht_honsu = CInt(reader.GetValue("sl_wht_honsu").ToString)
        sl_wht_suryo = CDec(reader.GetValue("sl_wht_suryo").ToString)

        sl_wht_tekcd = reader.GetValue("sl_wht_tekcd").ToString
        sl_wht_tekmei = reader.GetValue("sl_wht_tekmei").ToString

        sl_wht_insuser = reader.GetValue("sl_wht_insuser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_wht_insdate").ToString) Then
          sl_wht_insdate = CDate(reader.GetValue("sl_wht_insdate"))
        Else
          sl_wht_insdate = Nothing
        End If

        sl_wht_upduser = reader.GetValue("sl_wht_upduser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_wht_upddate").ToString) Then
          sl_wht_upddate = CDate(reader.GetValue("sl_wht_upddate"))
        Else
          sl_wht_upddate = Nothing
        End If

        isSuccess = True
      Else
        isSuccess = False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    Finally
      reader.Close()
      reader.Dispose()

    End Try
    Return isSuccess
  End Function

#End Region
End Class

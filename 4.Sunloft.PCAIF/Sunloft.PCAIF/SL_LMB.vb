﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Common

Public Class SL_LMB
  Inherits clsPCAIFBase
  Public Property sl_lmb_id As Integer = 0 'ID = 0 if there's no data, otherwise = 1

  ''' <summary>Shipment Deadline flag(Shukka)</summary>
  Public Property sl_lmb_sdlflg As Integer = 1

  ''' <summary>Use-by Date flag(Shiyou)</summary>
  Public Property sl_lmb_ubdflg As Integer = 1

  ''' <summary>Best before date flag(Shoumi)</summary>
  Public Property sl_lmb_bbdflg As Integer = 1

  ''' <summary>Expiration date flag(Shouhi)</summary>
  Public Property sl_lmb_expflg As Integer = 1

  ''' <summary>Shipment Deadline allocation type(Shukka)</summary>
  Public Property sl_lmb_sdlhikiate As Integer = -1

  ''' <summary>Use-by Date allocation type(Shiyou)</summary>
  Public Property sl_lmb_udbhikiate As Integer = -1

  ''' <summary>Best before date allocation type(Shoumi)</summary>
  Public Property sl_lmb_bbdhikiate As Integer = -1

  ''' <summary>Expiration date allocation type(Shouhi)</summary>
  Public Property sl_lmb_exphikiate As Integer = -1

  Public Property sl_lmb_maxlotop As Integer
  Public Property sl_lmb_autohikiate As Integer
  Public Property sl_lmb_hikiatesort As Integer = 0
  Public Property sl_lmb_autolot As SLConstants.SetLotType = SLConstants.SetLotType.Auto
  Public Property sl_lmb_lotfmt As String
  Public Property sl_lmb_lotime As Integer = 0
  Public Property sl_lmb_label1 As String
  Public Property sl_lmb_label2 As String
  Public Property sl_lmb_label3 As String
  Public Property sl_lmb_label4 As String
  Public Property sl_lmb_label5 As String
  Public Property sl_lmb_label6 As String
  Public Property sl_lmb_label7 As String
  Public Property sl_lmb_label8 As String
  Public Property sl_lmb_label9 As String
  Public Property sl_lmb_label10 As String
  Public Property sl_lmb_zcutoff_date As Integer = 0
  Public Property sl_lmb_tcd As String
  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Const FIXED_ONLY_ID = 1

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Overrides Function Insert() As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_LMB")
      Return Register(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_LMB")
      Return Register(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_LMB")
      Return Register(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function Register(ByVal selectCommand As ICustomCommand) As Boolean
    Try
      selectCommand.Parameters("@sl_lmb_id").SetValue(FIXED_ONLY_ID)
      selectCommand.Parameters("@sl_lmb_sdlflg").SetValue(sl_lmb_sdlflg)
      selectCommand.Parameters("@sl_lmb_ubdflg").SetValue(sl_lmb_ubdflg)
      selectCommand.Parameters("@sl_lmb_bbdflg").SetValue(sl_lmb_bbdflg)
      selectCommand.Parameters("@sl_lmb_expflg").SetValue(sl_lmb_expflg)
      selectCommand.Parameters("@sl_lmb_sdlhikiate").SetValue(sl_lmb_sdlhikiate)
      selectCommand.Parameters("@sl_lmb_udbhikiate").SetValue(sl_lmb_udbhikiate)
      selectCommand.Parameters("@sl_lmb_bbdhikiate").SetValue(sl_lmb_bbdhikiate)
      selectCommand.Parameters("@sl_lmb_exphikiate").SetValue(sl_lmb_exphikiate)
      selectCommand.Parameters("@sl_lmb_maxlotop").SetValue(sl_lmb_maxlotop)
      selectCommand.Parameters("@sl_lmb_autohikiate").SetValue(sl_lmb_autohikiate)
      selectCommand.Parameters("@sl_lmb_hikiatesort").SetValue(sl_lmb_hikiatesort)
      selectCommand.Parameters("@sl_lmb_lotfmt").SetValue(sl_lmb_lotfmt)
      selectCommand.Parameters("@sl_lmb_lotime").SetValue(sl_lmb_lotime)
      selectCommand.Parameters("@sl_lmb_autolot").SetValue(Integer.Parse(CStr(sl_lmb_autolot)))
      selectCommand.Parameters("@sl_lmb_label1").SetValue(sl_lmb_label1)
      selectCommand.Parameters("@sl_lmb_label2").SetValue(sl_lmb_label2)
      selectCommand.Parameters("@sl_lmb_label3").SetValue(sl_lmb_label3)
      selectCommand.Parameters("@sl_lmb_label4").SetValue(sl_lmb_label4)
      selectCommand.Parameters("@sl_lmb_label5").SetValue(sl_lmb_label5)
      selectCommand.Parameters("@sl_lmb_label6").SetValue(sl_lmb_label6)
      selectCommand.Parameters("@sl_lmb_label7").SetValue(sl_lmb_label7)
      selectCommand.Parameters("@sl_lmb_label8").SetValue(sl_lmb_label8)
      selectCommand.Parameters("@sl_lmb_label9").SetValue(sl_lmb_label9)
      selectCommand.Parameters("@sl_lmb_label10").SetValue(sl_lmb_label10)
      selectCommand.Parameters("@sl_lmb_zcutoff_date").SetValue(sl_lmb_zcutoff_date)
      selectCommand.Parameters("@sl_lmb_tcd").SetValue(sl_lmb_tcd)

      selectCommand.ExecuteNonQuery()
      Return True

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Public Overrides Function Delete() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function ReadByID(ByVal intID As Integer) As Boolean
    Return Nothing
  End Function

  Public Function ReadOnlyRow() As Boolean 'Return true if there's data
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intCount = 0
    Dim intSetLotType As Integer = 1
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_LMB")
      reader = selectCommand.ExecuteReader
      'Read the only row
      If reader.Read() Then
        sl_lmb_id = Integer.Parse(reader.GetValue("sl_lmb_id").ToString)
        sl_lmb_sdlflg = Integer.Parse(reader.GetValue("sl_lmb_sdlflg").ToString)
        sl_lmb_ubdflg = Integer.Parse(reader.GetValue("sl_lmb_ubdflg").ToString)
        sl_lmb_bbdflg = Integer.Parse(reader.GetValue("sl_lmb_bbdflg").ToString)
        sl_lmb_expflg = Integer.Parse(reader.GetValue("sl_lmb_expflg").ToString)
        sl_lmb_sdlhikiate = Integer.Parse(reader.GetValue("sl_lmb_sdlhikiate").ToString)
        sl_lmb_udbhikiate = Integer.Parse(reader.GetValue("sl_lmb_udbhikiate").ToString)
        sl_lmb_bbdhikiate = Integer.Parse(reader.GetValue("sl_lmb_bbdhikiate").ToString)
        sl_lmb_exphikiate = Integer.Parse(reader.GetValue("sl_lmb_exphikiate").ToString)
        sl_lmb_maxlotop = Integer.Parse(reader.GetValue("sl_lmb_maxlotop").ToString)
        sl_lmb_autohikiate = Integer.Parse(reader.GetValue("sl_lmb_autohikiate").ToString)
        sl_lmb_hikiatesort = Integer.Parse(reader.GetValue("sl_lmb_hikiatesort").ToString)
        If Integer.TryParse(reader.GetValue("sl_lmb_autolot").ToString, intSetLotType) Then
          If intSetLotType = 1 Then
            sl_lmb_autolot = SLConstants.SetLotType.Auto
          Else
            sl_lmb_autolot = SLConstants.SetLotType.Manual
          End If
        End If

        sl_lmb_lotfmt = reader.GetValue("sl_lmb_lotfmt").ToString.Trim
        sl_lmb_lotime = Integer.Parse(reader.GetValue("sl_lmb_lotime").ToString)
        sl_lmb_label1 = reader.GetValue("sl_lmb_label1").ToString.Trim
        sl_lmb_label2 = reader.GetValue("sl_lmb_label2").ToString.Trim
        sl_lmb_label3 = reader.GetValue("sl_lmb_label3").ToString.Trim
        sl_lmb_label4 = reader.GetValue("sl_lmb_label4").ToString.Trim
        sl_lmb_label5 = reader.GetValue("sl_lmb_label5").ToString.Trim
        sl_lmb_label6 = reader.GetValue("sl_lmb_label6").ToString.Trim
        sl_lmb_label7 = reader.GetValue("sl_lmb_label7").ToString.Trim
        sl_lmb_label8 = reader.GetValue("sl_lmb_label8").ToString.Trim
        sl_lmb_label9 = reader.GetValue("sl_lmb_label9").ToString.Trim
        sl_lmb_label10 = reader.GetValue("sl_lmb_label10").ToString.Trim
        If Int32.TryParse(reader.GetValue("sl_lmb_zcutoff_date").ToString, sl_lmb_zcutoff_date) Then
          sl_lmb_zcutoff_date = CInt(reader.GetValue("sl_lmb_zcutoff_date").ToString)
        End If

        sl_lmb_tcd = reader.GetValue("sl_lmb_tcd").ToString.Trim
        Return True
      End If
      Return False
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Overrides Function GetNewID() As Integer
    Try
      Return FIXED_ONLY_ID
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try
  End Function


End Class

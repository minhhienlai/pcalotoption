﻿Imports PCA.ApplicationIntegration
Public Class PJMS

  Public Property pjms_dkbn As Integer = 0
  Public Property pjms_code As String = String.Empty
  Public Property pjms_name As String = String.Empty
  Public Property pjms_start As Integer = 0
  Public Property pjms_end As Integer = 0
  Public Property pjms_state As Integer = 0
  Public Property pjms_kosin As Integer = 0
  Public Property pjms_insuser As Integer = 0
  Public Property pjms_inspg As String = String.Empty
  Public Property pjms_insdate As DateTime = Now
  Public Property pjms_upduser As Integer = 0
  Public Property pjms_updpg As String = String.Empty
  Public Property pjms_upddate As DateTime = Now
  Public Property pjms_ys1 As Short = 0
  Public Property pjms_ys2 As Short = 0
  Public Property pjms_ys3 As Short = 0
  Public Property pjms_yi1 As Integer = 0
  Public Property pjms_yi2 As Integer = 0
  Public Property pjms_yi3 As Integer = 0
  Public Property pjms_ym1 As Integer = 0
  Public Property pjms_ym2 As Integer = 0
  Public Property pjms_ym3 As Integer = 0
  Public Property pjms_yc1 As String = String.Empty
  Public Property pjms_yc2 As String = String.Empty
  Public Property pjms_yc3 As String = String.Empty

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function ReadByCode(ByVal strCode As String) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_PJMS")
      selectCommand.Parameters("@pjms_code").SetValue(strCode)

      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        Me.pjms_dkbn = CInt(reader.GetValue("pjms_dkbn"))
        Me.pjms_code = reader.GetValue("pjms_code").ToString
        Me.pjms_name = reader.GetValue("pjms_name").ToString
        Me.pjms_start = CInt(reader.GetValue("pjms_start"))
        Me.pjms_end = CInt(reader.GetValue("pjms_end"))
        Me.pjms_state = CInt(reader.GetValue("pjms_state"))
        Me.pjms_kosin = CInt(reader.GetValue("pjms_kosin"))
        Me.pjms_insuser = CInt(reader.GetValue("pjms_insuser"))
        Me.pjms_inspg = reader.GetValue("pjms_inspg").ToString
        Me.pjms_insdate = CDate(reader.GetValue("pjms_insdate"))
        Me.pjms_upduser = CInt(reader.GetValue("pjms_upduser"))
        Me.pjms_updpg = reader.GetValue("pjms_updpg").ToString
        Me.pjms_upddate = CDate(reader.GetValue("pjms_upddate"))
        Me.pjms_ys1 = CShort(reader.GetValue("pjms_ys1"))
        Me.pjms_ys2 = CShort(reader.GetValue("pjms_ys2"))
        Me.pjms_ys3 = CShort(reader.GetValue("pjms_ys3"))
        Me.pjms_yi1 = CInt(reader.GetValue("pjms_yi1"))
        Me.pjms_yi2 = CInt(reader.GetValue("pjms_yi2"))
        Me.pjms_yi3 = CInt(reader.GetValue("pjms_yi3"))
        Me.pjms_ym1 = CInt(reader.GetValue("pjms_ym1"))
        Me.pjms_ym2 = CInt(reader.GetValue("pjms_ym2"))
        Me.pjms_ym3 = CInt(reader.GetValue("pjms_ym3"))
        Me.pjms_yc1 = reader.GetValue("pjms_yc1").ToString
        Me.pjms_yc2 = reader.GetValue("pjms_yc2").ToString
        Me.pjms_yc3 = reader.GetValue("pjms_yc3").ToString

        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

End Class

﻿Imports PCA.ApplicationIntegration
Public Class SL_NYKD
  Inherits clsPCAIFBase
  Public Property isExisted As Boolean = True
  Public Property sl_nykd_id As Integer
  Public Property sl_nykd_hid As Integer
  Public Property sl_nykd_eda As Integer
  Public Property sl_nykd_nkbn As Integer
  Public Property sl_nykd_scd As String
  Public Property sl_nykd_mei As String
  Public Property sl_nykd_kikaku As String
  Public Property sl_nykd_color As String
  Public Property sl_nykd_size As String
  Public Property sl_nykd_souko As String
  Public Property sl_nykd_tax As Integer
  Public Property sl_nykd_komi As Integer
  Public Property sl_nykd_iri As Integer
  Public Property sl_nykd_hako As Integer
  Public Property sl_nykd_suryo As Double
  Public Property sl_nykd_honsu As Integer
  Public Property sl_nykd_tani As String
  Public Property sl_nykd_tanka As Decimal
  Public Property sl_nykd_kingaku As Double
  Public Property sl_nykd_biko As String
  Public Property sl_nykd_rate As Integer
  Public Property sl_nykd_lotno As String
  Public Property sl_nykd_ulotno As String
  Public Property sl_nykd_alotno As String
  Public Property sl_nykd_kanrino As String
  Public Property sl_nykd_sdldate As Integer
  Public Property sl_nykd_ubdate As Integer
  Public Property sl_nykd_bbdate As Integer
  Public Property sl_nykd_expdate As Integer
  Public Property sl_nykd_ldid1 As String
  Public Property sl_nykd_ldid2 As String
  Public Property sl_nykd_ldid3 As String
  Public Property sl_nykd_ldid4 As String
  Public Property sl_nykd_ldid5 As String
  Public Property sl_nykd_ldid6 As String
  Public Property sl_nykd_ldid7 As String
  Public Property sl_nykd_ldid8 As String
  Public Property sl_nykd_ldid9 As String
  Public Property sl_nykd_ldid10 As String
  Public Property sl_nykd_ldmei1 As String
  Public Property sl_nykd_ldmei2 As String
  Public Property sl_nykd_ldmei3 As String
  Public Property sl_nykd_ldmei4 As String
  Public Property sl_nykd_ldmei5 As String
  Public Property sl_nykd_ldmei6 As String
  Public Property sl_nykd_ldmei7 As String
  Public Property sl_nykd_ldmei8 As String
  Public Property sl_nykd_ldmei9 As String
  Public Property sl_nykd_ldmei10 As String
  Public Property sl_nykd_convert As Integer

  Public Property intCurrentNumber As Integer 'Get from SL_ZDN
  Public Property dblCurrentQuantity As Decimal

  Public Property intInventoryID As Integer

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Private Const NOT_CONVERTED As Integer = 0
  Private Const ALREADY_CONVERTED As Integer = 1


  Public Sub New(ByRef myApp As IIntegratedApplication)
    connector = myApp
  End Sub

#Region "INSERT"

  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      RegisterDetails(False, session)
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "UPDATE"

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      RegisterDetails(True, session)
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  ''' update convert flag ,other contents
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function UpdateDetailsAfterConvert(ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Try
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_NYKD_Convert")
      selectCommand.Parameters("@sl_nykd_id").SetValue(sl_nykd_id)
      selectCommand.Parameters("@sl_nykd_convert").SetValue(ALREADY_CONVERTED)
      selectCommand.Parameters("@sl_nykd_tanka").SetValue(sl_nykd_tanka)
      selectCommand.Parameters("@sl_nykd_ulotno").SetValue(sl_nykd_ulotno)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function
#End Region

#Region "DELETE"
  Public Overrides Function Delete() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_NYKD")
      selectCommand.Parameters("@sl_nykd_id").SetValue(sl_nykd_id)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "SELECT"
  Public Overrides Function ReadByID(ByVal intDetailsID As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKD_ZDN")
      selectCommand.Parameters("@sl_nykd_id").SetValue(intDetailsID)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then

        sl_nykd_id = CInt(reader.GetValue("sl_nykd_id"))
        sl_nykd_hid = CInt(reader.GetValue("sl_nykd_hid"))
        sl_nykd_eda = CInt(reader.GetValue("sl_nykd_eda")) + 1
        If Not IsDBNull(reader.GetValue("sl_nykd_nkbn")) Then sl_nykd_nkbn = CInt(reader.GetValue("sl_nykd_nkbn"))
        sl_nykd_scd = reader.GetValue("sl_nykd_scd").ToString.Trim
        sl_nykd_mei = reader.GetValue("sl_nykd_mei").ToString.Trim
        sl_nykd_kikaku = reader.GetValue("sl_nykd_kikaku").ToString.Trim
        sl_nykd_color = reader.GetValue("sl_nykd_color").ToString.Trim
        sl_nykd_size = reader.GetValue("sl_nykd_size").ToString.Trim
        sl_nykd_souko = reader.GetValue("sl_nykd_souko").ToString.Trim
        sl_nykd_komi = CInt(reader.GetValue("sl_nykd_komi"))
        sl_nykd_rate = CInt(reader.GetValue("sl_nykd_rate"))
        sl_nykd_iri = CInt(reader.GetValue("sl_nykd_iri"))
        sl_nykd_hako = CInt(reader.GetValue("sl_nykd_hako"))
        sl_nykd_suryo = CDec(reader.GetValue("sl_nykd_suryo"))
        sl_nykd_honsu = CInt(reader.GetValue("sl_nykd_honsu"))
        sl_nykd_tani = reader.GetValue("sl_nykd_tani").ToString.Trim
        sl_nykd_tanka = CDec(reader.GetValue("sl_nykd_tanka"))
        sl_nykd_kingaku = CDbl(reader.GetValue("sl_nykd_kingaku"))
        sl_nykd_biko = reader.GetValue("sl_nykd_biko").ToString.Trim
        sl_nykd_lotno = reader.GetValue("sl_nykd_lotno").ToString.Trim
        sl_nykd_ulotno = reader.GetValue("sl_nykd_ulotno").ToString.Trim
        sl_nykd_alotno = reader.GetValue("sl_nykd_alotno").ToString.Trim
        sl_nykd_kanrino = reader.GetValue("sl_nykd_kanrino").ToString.Trim
        sl_nykd_sdldate = CInt(reader.GetValue("sl_nykd_sdldate"))
        sl_nykd_ubdate = CInt(reader.GetValue("sl_nykd_ubdate"))
        sl_nykd_bbdate = CInt(reader.GetValue("sl_nykd_bbdate"))
        sl_nykd_expdate = CInt(reader.GetValue("sl_nykd_expdate"))
        intCurrentNumber = CInt(reader.GetValue("sl_zdn_honsu"))
        dblCurrentQuantity = CDec(reader.GetValue("sl_zdn_suryo"))

        sl_nykd_ldid1 = reader.GetValue("sl_nykd_ldid1").ToString.Trim
        sl_nykd_ldid2 = reader.GetValue("sl_nykd_ldid2").ToString.Trim
        sl_nykd_ldid3 = reader.GetValue("sl_nykd_ldid3").ToString.Trim
        sl_nykd_ldid4 = reader.GetValue("sl_nykd_ldid4").ToString.Trim
        sl_nykd_ldid5 = reader.GetValue("sl_nykd_ldid5").ToString.Trim
        sl_nykd_ldid6 = reader.GetValue("sl_nykd_ldid6").ToString.Trim
        sl_nykd_ldid7 = reader.GetValue("sl_nykd_ldid7").ToString.Trim
        sl_nykd_ldid8 = reader.GetValue("sl_nykd_ldid8").ToString.Trim
        sl_nykd_ldid9 = reader.GetValue("sl_nykd_ldid9").ToString.Trim
        sl_nykd_ldid10 = reader.GetValue("sl_nykd_ldid10").ToString.Trim

        sl_nykd_ldmei1 = reader.GetValue("sl_nykd_ldmei1").ToString
        sl_nykd_ldmei2 = reader.GetValue("sl_nykd_ldmei2").ToString
        sl_nykd_ldmei3 = reader.GetValue("sl_nykd_ldmei3").ToString
        sl_nykd_ldmei4 = reader.GetValue("sl_nykd_ldmei4").ToString
        sl_nykd_ldmei5 = reader.GetValue("sl_nykd_ldmei5").ToString
        sl_nykd_ldmei6 = reader.GetValue("sl_nykd_ldmei6").ToString
        sl_nykd_ldmei7 = reader.GetValue("sl_nykd_ldmei7").ToString
        sl_nykd_ldmei8 = reader.GetValue("sl_nykd_ldmei8").ToString
        sl_nykd_ldmei9 = reader.GetValue("sl_nykd_ldmei9").ToString
        sl_nykd_ldmei10 = reader.GetValue("sl_nykd_ldmei10").ToString

        intDetailsID = CInt(reader.GetValue("sl_nykd_id"))
        intInventoryID = CInt(reader.GetValue("sl_zdn_id"))
      End If
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      reader.Close()
      reader.Dispose()
    End Try
  End Function

  Public Function ReadAllFromHeaderIDToConvert() As SL_NYKD()
    Dim selectCommand As ICustomCommand
    selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKD_Pro03030")
    Return ReadAllFromHeaderID(selectCommand)
  End Function

  Public Function ReadByHeaderID() As SL_NYKD()
    Dim selectCommand As ICustomCommand
    selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKD_ZDN_FROM_HID")
    Return ReadAllFromHeaderID(selectCommand)
  End Function

  Public Function ReadByHeaderIDRet() As SL_NYKD()
    Dim selectCommand As ICustomCommand
    selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKD_ZHN_FROM_HID")
    Return ReadAllFromHeaderID(selectCommand)
  End Function

  Private Function ReadAllFromHeaderID(ByRef selectCommand As ICustomCommand) As SL_NYKD()
    Dim reader As ICustomDataReader = Nothing
    Try
      selectCommand.Parameters("@sl_nykd_hid").SetValue(sl_nykd_hid)
      reader = selectCommand.ExecuteReader
      Dim myTableDetails As SL_NYKD() = {New SL_NYKD(connector)}
      Dim intBranchCount As Integer = -1
      While reader.Read()
        intBranchCount = intBranchCount + 1
        ReDim Preserve myTableDetails(intBranchCount)
        myTableDetails(intBranchCount) = New SL_NYKD(connector)
        myTableDetails(intBranchCount).sl_nykd_id = CInt(reader.GetValue("sl_nykd_id"))
        myTableDetails(intBranchCount).sl_nykd_hid = CInt(reader.GetValue("sl_nykd_hid"))
        myTableDetails(intBranchCount).sl_nykd_eda = CInt(reader.GetValue("sl_nykd_eda")) + 1
        If Not IsDBNull(reader.GetValue("sl_nykd_nkbn")) Then myTableDetails(intBranchCount).sl_nykd_nkbn = CInt(reader.GetValue("sl_nykd_nkbn"))
        myTableDetails(intBranchCount).sl_nykd_scd = reader.GetValue("sl_nykd_scd").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_mei = reader.GetValue("sl_nykd_mei").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_kikaku = reader.GetValue("sl_nykd_kikaku").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_color = reader.GetValue("sl_nykd_color").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_size = reader.GetValue("sl_nykd_size").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_souko = reader.GetValue("sl_nykd_souko").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_tax = CInt(reader.GetValue("sl_nykd_tax"))
        myTableDetails(intBranchCount).sl_nykd_komi = CInt(reader.GetValue("sl_nykd_komi"))
        myTableDetails(intBranchCount).sl_nykd_rate = CInt(reader.GetValue("sl_nykd_rate"))
        myTableDetails(intBranchCount).sl_nykd_iri = CInt(reader.GetValue("sl_nykd_iri"))
        myTableDetails(intBranchCount).sl_nykd_hako = CInt(reader.GetValue("sl_nykd_hako"))
        myTableDetails(intBranchCount).sl_nykd_suryo = CDec(reader.GetValue("sl_nykd_suryo"))
        myTableDetails(intBranchCount).sl_nykd_honsu = CInt(reader.GetValue("sl_nykd_honsu"))
        myTableDetails(intBranchCount).sl_nykd_tani = reader.GetValue("sl_nykd_tani").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_tanka = CDec(reader.GetValue("sl_nykd_tanka"))
        myTableDetails(intBranchCount).sl_nykd_kingaku = CDbl(reader.GetValue("sl_nykd_kingaku"))
        myTableDetails(intBranchCount).sl_nykd_biko = reader.GetValue("sl_nykd_biko").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_lotno = reader.GetValue("sl_nykd_lotno").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ulotno = reader.GetValue("sl_nykd_ulotno").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_alotno = reader.GetValue("sl_nykd_alotno").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_kanrino = reader.GetValue("sl_nykd_kanrino").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_sdldate = CInt(reader.GetValue("sl_nykd_sdldate"))
        myTableDetails(intBranchCount).sl_nykd_ubdate = CInt(reader.GetValue("sl_nykd_ubdate"))
        myTableDetails(intBranchCount).sl_nykd_bbdate = CInt(reader.GetValue("sl_nykd_bbdate"))
        myTableDetails(intBranchCount).sl_nykd_expdate = CInt(reader.GetValue("sl_nykd_expdate"))
        myTableDetails(intBranchCount).intCurrentNumber = CInt(reader.GetValue("sl_zdn_honsu"))
        myTableDetails(intBranchCount).dblCurrentQuantity = CDec(reader.GetValue("sl_zdn_suryo"))

        myTableDetails(intBranchCount).sl_nykd_ldid1 = reader.GetValue("sl_nykd_ldid1").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ldid2 = reader.GetValue("sl_nykd_ldid2").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ldid3 = reader.GetValue("sl_nykd_ldid3").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ldid4 = reader.GetValue("sl_nykd_ldid4").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ldid5 = reader.GetValue("sl_nykd_ldid5").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ldid6 = reader.GetValue("sl_nykd_ldid6").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ldid7 = reader.GetValue("sl_nykd_ldid7").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ldid8 = reader.GetValue("sl_nykd_ldid8").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ldid9 = reader.GetValue("sl_nykd_ldid9").ToString.Trim
        myTableDetails(intBranchCount).sl_nykd_ldid10 = reader.GetValue("sl_nykd_ldid10").ToString.Trim

        myTableDetails(intBranchCount).sl_nykd_ldmei1 = reader.GetValue("sl_nykd_ldmei1").ToString
        myTableDetails(intBranchCount).sl_nykd_ldmei2 = reader.GetValue("sl_nykd_ldmei2").ToString
        myTableDetails(intBranchCount).sl_nykd_ldmei3 = reader.GetValue("sl_nykd_ldmei3").ToString
        myTableDetails(intBranchCount).sl_nykd_ldmei4 = reader.GetValue("sl_nykd_ldmei4").ToString
        myTableDetails(intBranchCount).sl_nykd_ldmei5 = reader.GetValue("sl_nykd_ldmei5").ToString
        myTableDetails(intBranchCount).sl_nykd_ldmei6 = reader.GetValue("sl_nykd_ldmei6").ToString
        myTableDetails(intBranchCount).sl_nykd_ldmei7 = reader.GetValue("sl_nykd_ldmei7").ToString
        myTableDetails(intBranchCount).sl_nykd_ldmei8 = reader.GetValue("sl_nykd_ldmei8").ToString
        myTableDetails(intBranchCount).sl_nykd_ldmei9 = reader.GetValue("sl_nykd_ldmei9").ToString
        myTableDetails(intBranchCount).sl_nykd_ldmei10 = reader.GetValue("sl_nykd_ldmei10").ToString

        myTableDetails(intBranchCount).intInventoryID = CInt(reader.GetValue("sl_zdn_id"))
        myTableDetails(intBranchCount).sl_nykd_convert = CInt(reader.GetValue("sl_nykd_convert"))
      End While

      Return myTableDetails
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function GetNumberQuantity(ByVal strProductCode As String, ByVal strWHCode As String, ByRef intNumber As Integer, ByRef decQuantity As Decimal) As Boolean
    Try

      Dim reader As ICustomDataReader = Nothing
      Try
        Dim selectCommand As ICustomCommand

        selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKD_NUMQTY")
        selectCommand.Parameters("@sl_nykd_scd").SetValue(strProductCode)
        selectCommand.Parameters("@sl_nykd_souko").SetValue(strWHCode)

        reader = selectCommand.ExecuteReader
        If reader.Read() Then

          intNumber = CInt(reader.GetValue("sl_nykd_honsu"))
          decQuantity = CDec(reader.GetValue("sl_nykd_suryo"))

        End If
        Return True
      Catch ex As Exception
        Sunloft.Message.DisplayBox.ShowCritical(ex)
        Return False
      Finally
        reader.Close()
        reader.Dispose()
      End Try

      Return True
    Catch ex As Exception
      Return False
    End Try
  End Function

#End Region

#Region "OTHER FUNCTION"
  Public Overrides Function GetNewID() As Integer
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim intMaxID As Integer = -1
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_NYKD_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        intMaxID = CInt(reader.GetValue("sl_nykd_id"))
      End If
      Return intMaxID + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Private Function RegisterDetails(isDetailsExisted As Boolean, ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      If isDetailsExisted Then
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_NYKD")
      Else
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_NYKD")
        If Not sl_nykd_id > 0 Then
          sl_nykd_id = GetNewID() 'If sl_nykd_id >0 -> Register more than 1 row in a session -> Do not use GetNewID
        End If
      End If
      selectCommand.Parameters("@sl_nykd_id").SetValue(sl_nykd_id)
      selectCommand.Parameters("@sl_nykd_hid").SetValue(sl_nykd_hid)
      selectCommand.Parameters("@sl_nykd_eda").SetValue(sl_nykd_eda)
      selectCommand.Parameters("@sl_nykd_nkbn").SetValue(sl_nykd_nkbn)
      selectCommand.Parameters("@sl_nykd_scd").SetValue(sl_nykd_scd)
      selectCommand.Parameters("@sl_nykd_mei").SetValue(sl_nykd_mei)
      selectCommand.Parameters("@sl_nykd_kikaku").SetValue(sl_nykd_kikaku)
      selectCommand.Parameters("@sl_nykd_color").SetValue(sl_nykd_color)
      selectCommand.Parameters("@sl_nykd_size").SetValue(sl_nykd_size)
      selectCommand.Parameters("@sl_nykd_souko").SetValue(sl_nykd_souko)
      selectCommand.Parameters("@sl_nykd_tax").SetValue(sl_nykd_tax)
      selectCommand.Parameters("@sl_nykd_komi").SetValue(sl_nykd_komi)
      selectCommand.Parameters("@sl_nykd_iri").SetValue(sl_nykd_iri)
      selectCommand.Parameters("@sl_nykd_hako").SetValue(sl_nykd_hako)
      selectCommand.Parameters("@sl_nykd_suryo").SetValue(sl_nykd_suryo)
      selectCommand.Parameters("@sl_nykd_honsu").SetValue(sl_nykd_honsu)
      selectCommand.Parameters("@sl_nykd_tani").SetValue(sl_nykd_tani)
      selectCommand.Parameters("@sl_nykd_tanka").SetValue(sl_nykd_tanka)
      selectCommand.Parameters("@sl_nykd_kingaku").SetValue(sl_nykd_kingaku)
      selectCommand.Parameters("@sl_nykd_biko").SetValue(sl_nykd_biko)
      selectCommand.Parameters("@sl_nykd_rate").SetValue(sl_nykd_rate)
      selectCommand.Parameters("@sl_nykd_lotno").SetValue(sl_nykd_lotno)
      selectCommand.Parameters("@sl_nykd_ulotno").SetValue(sl_nykd_ulotno)
      selectCommand.Parameters("@sl_nykd_alotno").SetValue(sl_nykd_alotno)
      selectCommand.Parameters("@sl_nykd_kanrino").SetValue(sl_nykd_kanrino)
      selectCommand.Parameters("@sl_nykd_sdldate").SetValue(sl_nykd_sdldate)
      selectCommand.Parameters("@sl_nykd_ubdate").SetValue(sl_nykd_ubdate)
      selectCommand.Parameters("@sl_nykd_bbdate").SetValue(sl_nykd_bbdate)
      selectCommand.Parameters("@sl_nykd_expdate").SetValue(sl_nykd_expdate)
      selectCommand.Parameters("@sl_nykd_ldid1").SetValue(sl_nykd_ldid1)
      selectCommand.Parameters("@sl_nykd_ldid2").SetValue(sl_nykd_ldid2)
      selectCommand.Parameters("@sl_nykd_ldid3").SetValue(sl_nykd_ldid3)
      selectCommand.Parameters("@sl_nykd_ldid4").SetValue(sl_nykd_ldid4)
      selectCommand.Parameters("@sl_nykd_ldid5").SetValue(sl_nykd_ldid5)
      selectCommand.Parameters("@sl_nykd_ldid6").SetValue(sl_nykd_ldid6)
      selectCommand.Parameters("@sl_nykd_ldid7").SetValue(sl_nykd_ldid7)
      selectCommand.Parameters("@sl_nykd_ldid8").SetValue(sl_nykd_ldid8)
      selectCommand.Parameters("@sl_nykd_ldid9").SetValue(sl_nykd_ldid9)
      selectCommand.Parameters("@sl_nykd_ldid10").SetValue(sl_nykd_ldid10)
      selectCommand.Parameters("@sl_nykd_ldmei1").SetValue(sl_nykd_ldmei1)
      selectCommand.Parameters("@sl_nykd_ldmei2").SetValue(sl_nykd_ldmei2)
      selectCommand.Parameters("@sl_nykd_ldmei3").SetValue(sl_nykd_ldmei3)
      selectCommand.Parameters("@sl_nykd_ldmei4").SetValue(sl_nykd_ldmei4)
      selectCommand.Parameters("@sl_nykd_ldmei5").SetValue(sl_nykd_ldmei5)
      selectCommand.Parameters("@sl_nykd_ldmei6").SetValue(sl_nykd_ldmei6)
      selectCommand.Parameters("@sl_nykd_ldmei7").SetValue(sl_nykd_ldmei7)
      selectCommand.Parameters("@sl_nykd_ldmei8").SetValue(sl_nykd_ldmei8)
      selectCommand.Parameters("@sl_nykd_ldmei9").SetValue(sl_nykd_ldmei9)
      selectCommand.Parameters("@sl_nykd_ldmei10").SetValue(sl_nykd_ldmei10)
      selectCommand.Parameters("@sl_nykd_convert").SetValue(sl_nykd_convert)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function
#End Region

End Class

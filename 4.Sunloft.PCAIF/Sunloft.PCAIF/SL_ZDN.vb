﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Common


Public Class SL_ZDN

  Inherits clsPCAIFBase

  Public Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Property sl_zdn_id As Integer = 0
  Public Property sl_zdn_datakbn As Integer = 0
  Public Property sl_zdn_dataid As Integer = 0
  Public Property sl_zdn_denno As Integer = 0
  Public Property sl_zdn_lotno As String = String.Empty
  Public Property sl_zdn_ulotno As String = String.Empty
  Public Property sl_zdn_alotno As String = String.Empty
  Public Property sl_zdn_scd As String = String.Empty
  Public Property sl_zdn_souko As String = String.Empty

  Public Property sl_zdn_uribi As Integer = 0
  Public Property sl_zdn_honsu As Integer = 0
  Public Property sl_zdn_suryo As Decimal = 0D
  Public Property sl_zdn_nyukohonsu As Integer = -1
  Public Property sl_zdn_nyukosuryo As Decimal = -1D
  Public Property sl_zdn_tanka As Decimal = 0D
  Public Property sl_zdn_tax As Integer = 0
  Public Property sl_zdn_kanrino As String = String.Empty

  Public Property sl_zdn_sdldate As Integer = 0
  Public Property sl_zdn_ubdate As Integer = 0
  Public Property sl_zdn_bbdate As Integer = 0
  Public Property sl_zdn_expdate As Integer = 0

  Public Property sl_zdn_ldid1 As String = String.Empty
  Public Property sl_zdn_ldid2 As String = String.Empty
  Public Property sl_zdn_ldid3 As String = String.Empty
  Public Property sl_zdn_ldid4 As String = String.Empty
  Public Property sl_zdn_ldid5 As String = String.Empty
  Public Property sl_zdn_ldid6 As String = String.Empty
  Public Property sl_zdn_ldid7 As String = String.Empty
  Public Property sl_zdn_ldid8 As String = String.Empty
  Public Property sl_zdn_ldid9 As String = String.Empty
  Public Property sl_zdn_ldid10 As String = String.Empty
  Public Property sl_zdn_ldmei1 As String = String.Empty
  Public Property sl_zdn_ldmei2 As String = String.Empty
  Public Property sl_zdn_ldmei3 As String = String.Empty
  Public Property sl_zdn_ldmei4 As String = String.Empty
  Public Property sl_zdn_ldmei5 As String = String.Empty
  Public Property sl_zdn_ldmei6 As String = String.Empty
  Public Property sl_zdn_ldmei7 As String = String.Empty
  Public Property sl_zdn_ldmei8 As String = String.Empty
  Public Property sl_zdn_ldmei9 As String = String.Empty
  Public Property sl_zdn_ldmei10 As String = String.Empty
  Public Property sl_zdn_insuser As String = String.Empty
  Public Property sl_zdn_insdate As DateTime
  Public Property sl_zdn_upduser As String = String.Empty
  Public Property sl_zdn_upddate As DateTime
  Public Property sl_zdn_relay_zdnid As Integer = 0
  Public Property sl_zdn_iri As Decimal = 0D

  'It is not used everyone
  Public Property sl_zdn_soukoMei As String = String.Empty
  'Only Trans
  Public Property sl_nykh_tcd As String = String.Empty
  Public Property sl_cms_tnm As String = String.Empty
  'Only StockTaking
  Public Property sl_zadj_id As Integer = 0
  Public Property sl_zadj_honsu As Integer = 0
  Public Property sl_zadj_suryo As Decimal = 0D
  Public Property sl_zadj_tekcd As String = String.Empty
  Public Property sl_zadj_tekmei As String = String.Empty
  Public Property sms_mei As String = String.Empty
  Public Property sms_tani As String = String.Empty
  Public Property AllocateNumber As Integer = 0
  Public Property AllocateQuantity As Decimal = 0D

  Private CalledProgram As UseProgram

  Private Enum UseProgram
    Usually = 0
    Trans
    StockTaking
  End Enum

  Public Sub New(ByRef myApp As IIntegratedApplication)
    connector = myApp
    CalledProgram = UseProgram.Usually
  End Sub

#Region "New ID,No,LotNo"

  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intMaxID As Integer = -1
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_ZDN_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intMaxID = CInt(reader.GetValue("sl_zdn_id"))
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intMaxID + 1

  End Function

  Public Function GetNewUserLotID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim strUserLot As Integer = -1
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_ZDN_USER_LOT")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        Dim intRetValue As Integer
        If Integer.TryParse(reader.GetValue("sl_zdn_ulotno").ToString, intRetValue) Then strUserLot = intRetValue
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try

    Return strUserLot + 1

  End Function

  Public Function GetNewSysLotID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim strSysLot As Integer = -1
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_ZDN_SYS_LOT")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then strSysLot = CInt(reader.GetValue("sl_zdn_lotno"))

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try

    Return strSysLot + 1

  End Function

  ''' <summary>get new slipno</summary>
  ''' <returns>new slipno</returns>
  ''' <remarks>最大伝票番号を取得します。</remarks>
  ''' 
  Public Function GetNewSlipNo() As Integer

    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intSlipNo As Integer = -1
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_ZDN_NO")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intSlipNo = CInt(reader.GetValue("sl_zdn_denno"))

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intSlipNo + 1

  End Function
#End Region

#Region " Insert"
  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean

    Try
      Dim selectCommand As ICustomCommand
      Dim SL_LMB As New SL_LMB(connector)
      SL_LMB.ReadOnlyRow()

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_ZDN")
      selectCommand.Parameters("@sl_zdn_insuser").SetValue(connector.UserId)
      selectCommand.Parameters("@sl_zdn_insdate").SetValue(Now)
      If SL_LMB.sl_lmb_autolot = SLConstants.SetLotType.Manual Then
        selectCommand.Parameters("@sl_zdn_ulotno").SetValue(sl_zdn_ulotno)
      Else
        selectCommand.Parameters("@sl_zdn_ulotno").SetValue(GetNewUserLotID)
      End If
      Register(selectCommand)
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function GetZHKAllocated(ByRef sum_sl_zhk_honsu As Integer, ByRef sum_sl_zhk_suryo As Decimal, sl_nykd_id As Integer) As Boolean
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZHK_ALLOCATED")
      selectCommand.Parameters("@sl_nykd_id").SetValue(sl_nykd_id)
      selectCommand.Parameters("@sl_zdn_datakbn").SetValue(SLConstants.SL_ZDNSlipType.Warehousing)
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        sum_sl_zhk_honsu = CInt(reader.GetValue("sum_sl_zhk_honsu"))
        sum_sl_zhk_suryo = CDec(reader.GetValue("sum_sl_zhk_suryo"))
      End If
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      reader.Close()
      reader.Dispose()
    End Try
  End Function

  Public Function RegisterInventoryFromTrans(ByVal intLpc As Integer, ByRef session As ICustomSession) As Boolean
    Try

      Dim selectCommand As ICustomCommand
      Dim intRet As Integer = 0
      Dim intNewID As Integer = GetNewID() + intLpc - 1
      Me.sl_zdn_id = intNewID
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_ZDN")

      selectCommand.Parameters("@sl_zdn_id").SetValue(intNewID)
      selectCommand.Parameters("@sl_zdn_dataid").SetValue(sl_zdn_dataid)
      selectCommand.Parameters("@sl_zdn_datakbn").SetValue(sl_zdn_datakbn)
      selectCommand.Parameters("@sl_zdn_denno").SetValue(sl_zdn_denno)
      selectCommand.Parameters("@sl_zdn_lotno").SetValue(CInt(GetNewSysLotID()))
      selectCommand.Parameters("@sl_zdn_ulotno").SetValue(sl_zdn_ulotno)
      selectCommand.Parameters("@sl_zdn_alotno").SetValue(sl_zdn_alotno)
      selectCommand.Parameters("@sl_zdn_scd").SetValue(sl_zdn_scd)
      selectCommand.Parameters("@sl_zdn_souko").SetValue(sl_zdn_souko)
      selectCommand.Parameters("@sl_zdn_uribi").SetValue(sl_zdn_uribi)
      selectCommand.Parameters("@sl_zdn_honsu").SetValue(sl_zdn_honsu)
      selectCommand.Parameters("@sl_zdn_suryo").SetValue(sl_zdn_suryo)
      selectCommand.Parameters("@sl_zdn_nyukohonsu").SetValue(sl_zdn_nyukohonsu)
      selectCommand.Parameters("@sl_zdn_nyukosuryo").SetValue(sl_zdn_nyukosuryo)
      selectCommand.Parameters("@sl_zdn_tanka").SetValue(sl_zdn_tanka)
      selectCommand.Parameters("@sl_zdn_tax").SetValue(sl_zdn_tax)
      selectCommand.Parameters("@sl_zdn_kanrino").SetValue(sl_zdn_kanrino)
      selectCommand.Parameters("@sl_zdn_sdldate").SetValue(sl_zdn_sdldate)
      selectCommand.Parameters("@sl_zdn_ubdate").SetValue(sl_zdn_ubdate)
      selectCommand.Parameters("@sl_zdn_bbdate").SetValue(sl_zdn_bbdate)
      selectCommand.Parameters("@sl_zdn_expdate").SetValue(sl_zdn_expdate)
      selectCommand.Parameters("@sl_zdn_ldid1").SetValue(sl_zdn_ldid1)
      selectCommand.Parameters("@sl_zdn_ldid2").SetValue(sl_zdn_ldid2)
      selectCommand.Parameters("@sl_zdn_ldid3").SetValue(sl_zdn_ldid3)
      selectCommand.Parameters("@sl_zdn_ldid4").SetValue(sl_zdn_ldid4)
      selectCommand.Parameters("@sl_zdn_ldid5").SetValue(sl_zdn_ldid5)
      selectCommand.Parameters("@sl_zdn_ldid6").SetValue(sl_zdn_ldid6)
      selectCommand.Parameters("@sl_zdn_ldid7").SetValue(sl_zdn_ldid7)
      selectCommand.Parameters("@sl_zdn_ldid8").SetValue(sl_zdn_ldid8)
      selectCommand.Parameters("@sl_zdn_ldid9").SetValue(sl_zdn_ldid9)
      selectCommand.Parameters("@sl_zdn_ldid10").SetValue(sl_zdn_ldid10)
      selectCommand.Parameters("@sl_zdn_ldmei1").SetValue(sl_zdn_ldmei1)
      selectCommand.Parameters("@sl_zdn_ldmei2").SetValue(sl_zdn_ldmei2)
      selectCommand.Parameters("@sl_zdn_ldmei3").SetValue(sl_zdn_ldmei3)
      selectCommand.Parameters("@sl_zdn_ldmei4").SetValue(sl_zdn_ldmei4)
      selectCommand.Parameters("@sl_zdn_ldmei5").SetValue(sl_zdn_ldmei5)
      selectCommand.Parameters("@sl_zdn_ldmei6").SetValue(sl_zdn_ldmei6)
      selectCommand.Parameters("@sl_zdn_ldmei7").SetValue(sl_zdn_ldmei7)
      selectCommand.Parameters("@sl_zdn_ldmei8").SetValue(sl_zdn_ldmei8)
      selectCommand.Parameters("@sl_zdn_ldmei9").SetValue(sl_zdn_ldmei9)
      selectCommand.Parameters("@sl_zdn_ldmei10").SetValue(sl_zdn_ldmei10)
      selectCommand.Parameters("@sl_zdn_relay_zdnid").SetValue(sl_zdn_relay_zdnid)
      selectCommand.Parameters("@sl_zdn_iri").SetValue(sl_zdn_iri)

      selectCommand.Parameters("@sl_zdn_insuser").SetValue(connector.UserId)
      selectCommand.Parameters("@sl_zdn_insdate").SetValue(Now)

      intRet = selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "Update"
  ''' <summary>現在本数及び現在個数の更新をします。</summary>
  ''' <returns></returns>
  ''' <remarks>在庫詳細現在データ(SL_ZDN)の現在本数及び現在個数を更新します。</remarks>
  ''' 
  Public Function UpdateNumberQuantity(ByVal isRelayID As Boolean, ByRef session As ICustomSession) As Boolean
    Try

      Dim selectCommand As ICustomCommand
      Dim intRet As Integer = 0

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_ZDN_Zaiko")
      If isRelayID Then
        selectCommand.Parameters("@sl_zdn_id").SetValue(sl_zdn_relay_zdnid)
      Else
        selectCommand.Parameters("@sl_zdn_id").SetValue(sl_zdn_id)
      End If
      selectCommand.Parameters("@sl_zdn_honsu").SetValue(sl_zdn_nyukohonsu)
      selectCommand.Parameters("@sl_zdn_suryo").SetValue(sl_zdn_nyukosuryo)

      intRet = selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
    Return True
  End Function

  ''' <summary>現在本数及び現在個数、在庫日付の更新をします。</summary>
  ''' <returns></returns>
  ''' <remarks>在庫詳細現在データ(SL_ZDN)の現在本数及び現在個数を更新します。</remarks>
  ''' 
  Public Function UpdateNumberQtyDate(ByVal isRelayID As Boolean, ByRef session As ICustomSession) As Boolean
    Try

      Dim selectCommand As ICustomCommand
      Dim intRet As Integer = 0

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_ZDN_Zaiko")
      If isRelayID Then
        selectCommand.Parameters("@sl_zdn_id").SetValue(sl_zdn_relay_zdnid)
      Else
        selectCommand.Parameters("@sl_zdn_id").SetValue(sl_zdn_id)
      End If
      selectCommand.Parameters("@sl_zdn_id").SetValue(sl_zdn_id)
      selectCommand.Parameters("@sl_zdn_honsu").SetValue(sl_zdn_nyukohonsu)
      selectCommand.Parameters("@sl_zdn_suryo").SetValue(sl_zdn_nyukosuryo)

      intRet = selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
    Return True
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Try
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_ZDN")
      selectCommand.Parameters("@sl_zdn_upduser").SetValue(connector.UserId)
      selectCommand.Parameters("@sl_zdn_upddate").SetValue(Now)
      selectCommand.Parameters("@sl_zdn_ulotno").SetValue(sl_zdn_ulotno)
      Register(selectCommand)
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function
#End Region

#Region "Delete"
  Public Overrides Function Delete() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      Dim intRet As Integer = 0

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_ZDN")
      selectCommand.Parameters("@sl_zdn_dataid").SetValue(sl_zdn_dataid)
      selectCommand.Parameters("@sl_zdn_datakbn").SetValue(sl_zdn_datakbn)
      intRet = selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function
#End Region

#Region "Read[Select] Function"

  Public Overrides Function ReadByID(ByVal intID As Integer) As Boolean
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZDN")

    Try
      selectCommand.Parameters("@sl_zdn_id").SetValue(intID)
      Return SetProperty(selectCommand)

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>Read From ZDN by ProductCode,WarehouseCode(Only TransferProgram)</summary>
  ''' <param name="strProductCode"></param>
  ''' <param name="strWHCode"></param>
  ''' <returns></returns>
  ''' <remarks>(Only TransferProgram)</remarks>
  Public Function ReadByProductWHOnlyTrans(ByVal strProductCode As String, ByVal strWHCode As String) As SL_ZDN()
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_STOCK_LIST")

    Try
      CalledProgram = UseProgram.Trans
      selectCommand.Parameters("@ProductCode").SetValue(strProductCode)
      selectCommand.Parameters("@WHCode").SetValue(strWHCode)

      Return SetProp(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try
  End Function

  ''' <summary>Read From ZDN by SlipNo</summary>
  ''' <param name="intSlipNo"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ReadBySlipNo(ByVal intSlipNo As Integer) As SL_ZDN()
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZDN")

    Try
      selectCommand.Parameters("@sl_zdn_denno").SetValue(intSlipNo)

      Return SetProp(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try
  End Function

  ''' <summary>
  ''' Search Product Info (Not Lot Management)
  ''' </summary>
  ''' <param name="strProductCode"></param>
  ''' <param name="strWHCode"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ReadNotLotManagement(ByVal strProductCode As String, ByVal strWHCode As String) As Boolean
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZDN_NOT_MANAGEMENT")
    Dim reader As ICustomDataReader = Nothing
    Dim isExists As Boolean
    Dim intRetNum As Integer = 0
    Dim decRetQty As Decimal = 0D

    Try
      selectCommand.Parameters("@sl_zdn_scd").SetValue(strProductCode)
      selectCommand.Parameters("@sl_zdn_souko").SetValue(strWHCode)
      reader = selectCommand.ExecuteReader

      If reader.Read Then
        isExists = True
        Integer.TryParse(reader.GetValue("sl_zdn_honsu").ToString, intRetNum)
        Decimal.TryParse(reader.GetValue("sl_zdn_suryo").ToString, decRetQty)
        Me.sl_zdn_honsu = intRetNum
        Me.sl_zdn_suryo = decRetQty
      Else
        isExists = False
      End If

      Return isExists
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try

  End Function

  ''' <summary>Read From ZDN by ID(Only TransferProgram)</summary>
  ''' <param name="intID"></param>
  ''' <param name="strProductCode"></param>
  ''' <param name="strWHCode"></param>
  ''' <returns></returns>
  ''' <remarks>(Only TransferProgram)</remarks>
  Public Function ReadByIDOnlyTrans(ByVal intID As Integer, ByVal strProductCode As String, ByVal strWHCode As String) As SL_ZDN()
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZDN_Pro04030")

    Try
      CalledProgram = UseProgram.Trans
      selectCommand.Parameters("@ID").SetValue(intID)
      selectCommand.Parameters("@ProductCode").SetValue(strProductCode)
      selectCommand.Parameters("@WHCode").SetValue(strWHCode)

      Return SetProp(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try
  End Function

  ''' <summary>Read Data by date</summary>
  ''' <param name="strProductCode"></param>
  ''' <param name="intDateFrom"></param>
  ''' <param name="intDateTo"></param>
  ''' <param name="strWhCode"></param>
  ''' <param name="strLotno"></param>
  ''' <returns></returns>
  ''' <remarks></remarks> 
  Public Function ReadByProductDate(ByVal strProductCode As String, ByVal intDateFrom As Integer, intDateTo As Integer, Optional strWhCode As String = "", Optional strLotno As String = "") As SL_ZDN()

    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZDN_INVENTORY_SEARCH")
    Dim reader As ICustomDataReader = Nothing

    Try


      selectCommand.Parameters("@sl_zdn_scd").SetValue(strProductCode)
      selectCommand.Parameters("@sl_zdn_souko").SetValue(strWhCode)
      selectCommand.Parameters("@sl_zdn_uribiFrom").SetValue(intDateFrom)
      selectCommand.Parameters("@sl_zdn_uribiTo").SetValue(intDateTo)
      selectCommand.Parameters("@sl_zdn_ulotno").SetValue(strLotno)

      Return SetProp(selectCommand)

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try

  End Function

  Public Function ReadStockTaking(ByVal strProductCodeFrom As String, ByVal strProductCodeTo As String, ByVal strWHCode As String, ByVal intTargetDate As Integer) As SL_ZDN()
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZDN_Pro04080")

    Try
      CalledProgram = UseProgram.StockTaking
      selectCommand.Parameters("@ProductCodeFrom").SetValue(strProductCodeFrom)
      selectCommand.Parameters("@ProductCodeTo").SetValue(strProductCodeTo)
      selectCommand.Parameters("@WHCode").SetValue(strWHCode)
      selectCommand.Parameters("@TargetDate").SetValue(intTargetDate)

      Return SetProp(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try

  End Function

  Public Function CheckLotExists(ByVal strProductCode As String, ByVal strWarehouse As String, ByVal strLotNo As String) As Boolean
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZDN_EXISTS_LOT")

    Try
      selectCommand.Parameters("@sl_zdn_scd").SetValue(strProductCode)
      selectCommand.Parameters("@sl_zdn_souko").SetValue(strWarehouse)
      selectCommand.Parameters("@sl_zdn_ulotno").SetValue(strLotNo)

      Return SetProperty(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try
  End Function

  ''' <summary>ReadingData</summary>
  ''' <param name="selectCommand">sqlCommand</param>
  ''' <returns></returns>
  ''' <remarks>ZDNClass</remarks>
  Private Function SetProp(ByVal selectCommand As ICustomCommand) As SL_ZDN()
    Dim reader As ICustomDataReader = Nothing
    Dim EMSClass As EMS

    Try
      reader = selectCommand.ExecuteReader

      Dim myTableDetails As SL_ZDN() = {New SL_ZDN(connector)}
      EMSClass = New EMS(connector)
      Dim intBranchCount As Integer = -1

      While reader.Read()
        intBranchCount += 1
        ReDim Preserve myTableDetails(intBranchCount)
        myTableDetails(intBranchCount) = New SL_ZDN(connector)

        ValidateAndSave(reader.GetValue("sl_zdn_id"), myTableDetails(intBranchCount).sl_zdn_id)
        ValidateAndSave(reader.GetValue("sl_zdn_datakbn"), myTableDetails(intBranchCount).sl_zdn_datakbn)
        ValidateAndSave(reader.GetValue("sl_zdn_dataid"), myTableDetails(intBranchCount).sl_zdn_dataid)
        ValidateAndSave(reader.GetValue("sl_zdn_denno"), myTableDetails(intBranchCount).sl_zdn_denno)

        ValidateAndSave(reader.GetValue("sl_zdn_lotno"), myTableDetails(intBranchCount).sl_zdn_lotno)
        ValidateAndSave(reader.GetValue("sl_zdn_ulotno"), myTableDetails(intBranchCount).sl_zdn_ulotno)
        ValidateAndSave(reader.GetValue("sl_zdn_alotno"), myTableDetails(intBranchCount).sl_zdn_alotno)
        ValidateAndSave(reader.GetValue("sl_zdn_scd"), myTableDetails(intBranchCount).sl_zdn_scd)
        ValidateAndSave(reader.GetValue("sl_zdn_souko"), myTableDetails(intBranchCount).sl_zdn_souko)
        If Not myTableDetails(intBranchCount).sl_zdn_souko = String.Empty Then
          myTableDetails(intBranchCount).sl_zdn_soukoMei = EMSClass.ReadKbnMasterByIDAndKbn(SLConstants.EMS.Warehouse, myTableDetails(intBranchCount).sl_zdn_souko)
        End If

        ValidateAndSave(reader.GetValue("sl_zdn_uribi"), myTableDetails(intBranchCount).sl_zdn_uribi)
        ValidateAndSave(reader.GetValue("sl_zdn_honsu"), myTableDetails(intBranchCount).sl_zdn_honsu)
        ValidateAndSave(reader.GetValue("sl_zdn_suryo"), myTableDetails(intBranchCount).sl_zdn_suryo)

        ValidateAndSave(reader.GetValue("sl_zdn_nyukohonsu"), myTableDetails(intBranchCount).sl_zdn_nyukohonsu)
        ValidateAndSave(reader.GetValue("sl_zdn_nyukosuryo"), myTableDetails(intBranchCount).sl_zdn_nyukosuryo)
        ValidateAndSave(reader.GetValue("sl_zdn_tanka"), myTableDetails(intBranchCount).sl_zdn_tanka)
        ValidateAndSave(reader.GetValue("sl_zdn_tax"), myTableDetails(intBranchCount).sl_zdn_tax)

        ValidateAndSave(reader.GetValue("sl_zdn_kanrino"), myTableDetails(intBranchCount).sl_zdn_kanrino)
        ValidateAndSave(reader.GetValue("sl_zdn_sdldate"), myTableDetails(intBranchCount).sl_zdn_sdldate)
        ValidateAndSave(reader.GetValue("sl_zdn_ubdate"), myTableDetails(intBranchCount).sl_zdn_ubdate)
        ValidateAndSave(reader.GetValue("sl_zdn_bbdate"), myTableDetails(intBranchCount).sl_zdn_bbdate)
        ValidateAndSave(reader.GetValue("sl_zdn_expdate"), myTableDetails(intBranchCount).sl_zdn_expdate)

        ValidateAndSave(reader.GetValue("sl_zdn_ldid1"), myTableDetails(intBranchCount).sl_zdn_ldid1)
        ValidateAndSave(reader.GetValue("sl_zdn_ldid2"), myTableDetails(intBranchCount).sl_zdn_ldid2)
        ValidateAndSave(reader.GetValue("sl_zdn_ldid3"), myTableDetails(intBranchCount).sl_zdn_ldid3)
        ValidateAndSave(reader.GetValue("sl_zdn_ldid4"), myTableDetails(intBranchCount).sl_zdn_ldid4)
        ValidateAndSave(reader.GetValue("sl_zdn_ldid5"), myTableDetails(intBranchCount).sl_zdn_ldid5)
        ValidateAndSave(reader.GetValue("sl_zdn_ldid6"), myTableDetails(intBranchCount).sl_zdn_ldid6)
        ValidateAndSave(reader.GetValue("sl_zdn_ldid7"), myTableDetails(intBranchCount).sl_zdn_ldid7)
        ValidateAndSave(reader.GetValue("sl_zdn_ldid8"), myTableDetails(intBranchCount).sl_zdn_ldid8)
        ValidateAndSave(reader.GetValue("sl_zdn_ldid9"), myTableDetails(intBranchCount).sl_zdn_ldid9)
        ValidateAndSave(reader.GetValue("sl_zdn_ldid10"), myTableDetails(intBranchCount).sl_zdn_ldid10)

        ValidateAndSave(reader.GetValue("sl_zdn_ldmei1"), myTableDetails(intBranchCount).sl_zdn_ldmei1)
        ValidateAndSave(reader.GetValue("sl_zdn_ldmei2"), myTableDetails(intBranchCount).sl_zdn_ldmei2)
        ValidateAndSave(reader.GetValue("sl_zdn_ldmei3"), myTableDetails(intBranchCount).sl_zdn_ldmei3)
        ValidateAndSave(reader.GetValue("sl_zdn_ldmei4"), myTableDetails(intBranchCount).sl_zdn_ldmei4)
        ValidateAndSave(reader.GetValue("sl_zdn_ldmei5"), myTableDetails(intBranchCount).sl_zdn_ldmei5)
        ValidateAndSave(reader.GetValue("sl_zdn_ldmei6"), myTableDetails(intBranchCount).sl_zdn_ldmei6)
        ValidateAndSave(reader.GetValue("sl_zdn_ldmei7"), myTableDetails(intBranchCount).sl_zdn_ldmei7)
        ValidateAndSave(reader.GetValue("sl_zdn_ldmei8"), myTableDetails(intBranchCount).sl_zdn_ldmei8)
        ValidateAndSave(reader.GetValue("sl_zdn_ldmei9"), myTableDetails(intBranchCount).sl_zdn_ldmei9)
        ValidateAndSave(reader.GetValue("sl_zdn_ldmei10"), myTableDetails(intBranchCount).sl_zdn_ldmei10)
        ValidateAndSave(reader.GetValue("sl_zdn_relay_zdnid"), myTableDetails(intBranchCount).sl_zdn_relay_zdnid)
        ValidateAndSave(reader.GetValue("sl_zdn_iri"), myTableDetails(intBranchCount).sl_zdn_iri)
        ValidateAndSave(reader.GetValue("sl_zdn_insuser"), myTableDetails(intBranchCount).sl_zdn_insuser)
        ValidateAndSave(reader.GetValue("sl_zdn_upduser"), myTableDetails(intBranchCount).sl_zdn_upduser)
        ValidateAndSave(reader.GetValue("sl_zdn_insdate"), myTableDetails(intBranchCount).sl_zdn_insdate)
        ValidateAndSave(reader.GetValue("sl_zdn_upddate"), myTableDetails(intBranchCount).sl_zdn_upddate)

        'other
        If CalledProgram <> UseProgram.Usually Then SetPropOthers(reader, myTableDetails(intBranchCount))

      End While
      If intBranchCount < 0 Then myTableDetails = Nothing

      Return myTableDetails
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      reader.Close()
      reader.Dispose()
    End Try
  End Function

  ''' <summary>Reading Data (Only one)</summary>
  ''' <param name="selectCommand"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SetProperty(ByVal selectCommand As ICustomCommand) As Boolean
    Dim reader As ICustomDataReader = Nothing

    Try
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        With Me
          .sl_zdn_id = CInt(reader.GetValue("sl_zdn_id"))
          .sl_zdn_uribi = CInt(reader.GetValue("sl_zdn_uribi"))
          .sl_zdn_denno = CInt(reader.GetValue("sl_zdn_denno"))
          .sl_zdn_ulotno = reader.GetValue("sl_zdn_ulotno").ToString
          If IsDBNull(reader.GetValue("sl_zdn_alotno")) Then
            .sl_zdn_alotno = String.Empty
          Else
            .sl_zdn_alotno = reader.GetValue("sl_zdn_alotno").ToString
          End If
          .sl_zdn_honsu = CInt(reader.GetValue("sl_zdn_honsu"))
          .sl_zdn_suryo = CDec(reader.GetValue("sl_zdn_suryo"))

          If IsDBNull(reader.GetValue("sl_zdn_nyukohonsu")) Then
            .sl_zdn_nyukohonsu = -1
          Else
            .sl_zdn_nyukohonsu = CInt(reader.GetValue("sl_zdn_nyukohonsu"))
          End If
          If IsDBNull(reader.GetValue("sl_zdn_nyukosuryo")) Then
            .sl_zdn_nyukosuryo = -1
          Else
            .sl_zdn_nyukosuryo = CDec(reader.GetValue("sl_zdn_nyukosuryo"))
          End If

          .sl_zdn_sdldate = CInt(reader.GetValue("sl_zdn_sdldate"))
          .sl_zdn_ubdate = CInt(reader.GetValue("sl_zdn_ubdate"))
          .sl_zdn_bbdate = CInt(reader.GetValue("sl_zdn_bbdate"))
          .sl_zdn_expdate = CInt(reader.GetValue("sl_zdn_expdate"))
          If Not IsDBNull(reader.GetValue("sl_nykh_tcd")) Then
            .sl_nykh_tcd = reader.GetValue("sl_nykh_tcd").ToString
          End If
          If Not IsDBNull(reader.GetValue("sl_cms_tnm")) Then
            .sl_cms_tnm = reader.GetValue("sl_cms_tnm").ToString
          End If

          If Not IsDBNull(reader.GetValue("sl_zdn_datakbn")) Then .sl_zdn_datakbn = CInt(reader.GetValue("sl_zdn_datakbn"))

          .sl_zdn_tanka = CDec(reader.GetValue("sl_zdn_tanka"))
          .sl_zdn_ldid1 = reader.GetValue("sl_zdn_ldid1").ToString
          .sl_zdn_ldid2 = reader.GetValue("sl_zdn_ldid2").ToString
          .sl_zdn_ldid3 = reader.GetValue("sl_zdn_ldid3").ToString
          .sl_zdn_ldid4 = reader.GetValue("sl_zdn_ldid4").ToString
          .sl_zdn_ldid5 = reader.GetValue("sl_zdn_ldid5").ToString
          .sl_zdn_ldid6 = reader.GetValue("sl_zdn_ldid6").ToString
          .sl_zdn_ldid7 = reader.GetValue("sl_zdn_ldid7").ToString
          .sl_zdn_ldid8 = reader.GetValue("sl_zdn_ldid8").ToString
          .sl_zdn_ldid9 = reader.GetValue("sl_zdn_ldid9").ToString
          .sl_zdn_ldid10 = reader.GetValue("sl_zdn_ldid10").ToString
          .sl_zdn_ldmei1 = reader.GetValue("sl_zdn_ldmei1").ToString
          .sl_zdn_ldmei2 = reader.GetValue("sl_zdn_ldmei2").ToString
          .sl_zdn_ldmei3 = reader.GetValue("sl_zdn_ldmei3").ToString
          .sl_zdn_ldmei4 = reader.GetValue("sl_zdn_ldmei4").ToString
          .sl_zdn_ldmei5 = reader.GetValue("sl_zdn_ldmei5").ToString
          .sl_zdn_ldmei6 = reader.GetValue("sl_zdn_ldmei6").ToString
          .sl_zdn_ldmei7 = reader.GetValue("sl_zdn_ldmei7").ToString
          .sl_zdn_ldmei8 = reader.GetValue("sl_zdn_ldmei8").ToString
          .sl_zdn_ldmei9 = reader.GetValue("sl_zdn_ldmei9").ToString
          .sl_zdn_ldmei10 = reader.GetValue("sl_zdn_ldmei10").ToString

          .sl_zdn_tax = CInt(reader.GetValue("sl_zdn_tax"))
          .sl_zdn_kanrino = reader.GetValue("sl_zdn_kanrino").ToString
          .sl_zdn_lotno = reader.GetValue("sl_zdn_lotno").ToString
          .sl_zdn_id = CInt(reader.GetValue("sl_zdn_id"))
          .sl_zdn_iri = CInt(reader.GetValue("sl_zdn_iri"))

          If IsDBNull(reader.GetValue("sl_zdn_relay_zdnid")) Then
            .sl_zdn_relay_zdnid = 0
          Else
            .sl_zdn_relay_zdnid = CInt(reader.GetValue("sl_zdn_relay_zdnid"))
          End If
        End With
        Return True
      Else
        Return False
      End If

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      reader.Close()
      reader.Dispose()
    End Try
  End Function

  ''' <summary>
  ''' Setting Property except default program
  ''' </summary>
  ''' <param name="reader"></param>
  ''' <param name="tableDetail"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SetPropOthers(ByVal reader As ICustomDataReader, ByRef tableDetail As SL_ZDN) As Boolean

    Try
      Select Case CalledProgram
        Case UseProgram.Trans
          ValidateAndSave(reader.GetValue("sl_nykh_tcd"), tableDetail.sl_nykh_tcd)
          ValidateAndSave(reader.GetValue("sl_cms_tnm"), tableDetail.sl_cms_tnm)
        Case UseProgram.StockTaking
          ValidateAndSave(reader.GetValue("sms_mei"), tableDetail.sms_mei)
          ValidateAndSave(reader.GetValue("sms_tani"), tableDetail.sms_tani)
          ValidateAndSave(reader.GetValue("AllocateNumber"), tableDetail.AllocateNumber)
          ValidateAndSave(reader.GetValue("AllocateQuantity"), tableDetail.AllocateQuantity)
          ValidateAndSave(reader.GetValue("sl_zadj_id"), tableDetail.sl_zadj_id)
          ValidateAndSave(reader.GetValue("sl_zadj_honsu"), tableDetail.sl_zadj_honsu)
          ValidateAndSave(reader.GetValue("sl_zadj_suryo"), tableDetail.sl_zadj_suryo)
          ValidateAndSave(reader.GetValue("sl_zadj_tekcd"), tableDetail.sl_zadj_tekcd)
          ValidateAndSave(reader.GetValue("sl_zadj_tekmei"), tableDetail.sl_zadj_tekmei)
      End Select
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

#End Region

#Region "Check Function"

  Public Sub ValidateAndSave(value As Object, ByRef item As Integer)
    If Not Sunloft.Common.SLCmnFunction.checkObjectNothingEmpty(value) AndAlso IsNumeric(value) Then
      item = CInt(value.ToString)
    End If
  End Sub

  Public Sub ValidateAndSave(value As Object, ByRef item As String)
    If Not Sunloft.Common.SLCmnFunction.checkObjectNothingEmpty(value) Then
      item = value.ToString
    End If
  End Sub

  Public Sub ValidateAndSave(value As Object, ByRef item As Decimal)
    If Not Sunloft.Common.SLCmnFunction.checkObjectNothingEmpty(value) AndAlso IsNumeric(value) Then
      item = CDec(value.ToString)
    End If
  End Sub

  Public Sub ValidateAndSave(value As Object, ByRef item As DateTime)
    If Not Sunloft.Common.SLCmnFunction.checkObjectNothingEmpty(value) Then
      item = CDate(value)
    End If
  End Sub
#End Region

  Private Function Register(ByRef selectCommand As ICustomCommand) As Boolean
    Try
      selectCommand.Parameters("@sl_zdn_id").SetValue(sl_zdn_id)
      selectCommand.Parameters("@sl_zdn_dataid").SetValue(sl_zdn_dataid)
      selectCommand.Parameters("@sl_zdn_datakbn").SetValue(SLConstants.SL_ZDNSlipType.Warehousing)
      selectCommand.Parameters("@sl_zdn_denno").SetValue(sl_zdn_denno)
      selectCommand.Parameters("@sl_zdn_lotno").SetValue(sl_zdn_lotno)
      selectCommand.Parameters("@sl_zdn_alotno").SetValue(sl_zdn_alotno)
      selectCommand.Parameters("@sl_zdn_scd").SetValue(sl_zdn_scd)
      selectCommand.Parameters("@sl_zdn_souko").SetValue(sl_zdn_souko)
      selectCommand.Parameters("@sl_zdn_uribi").SetValue(sl_zdn_uribi)
      selectCommand.Parameters("@sl_zdn_honsu").SetValue(sl_zdn_honsu)
      selectCommand.Parameters("@sl_zdn_suryo").SetValue(sl_zdn_suryo)
      selectCommand.Parameters("@sl_zdn_nyukohonsu").SetValue(sl_zdn_nyukohonsu)
      selectCommand.Parameters("@sl_zdn_nyukosuryo").SetValue(sl_zdn_nyukosuryo)
      selectCommand.Parameters("@sl_zdn_tanka").SetValue(sl_zdn_tanka)
      selectCommand.Parameters("@sl_zdn_tax").SetValue(sl_zdn_tax)
      selectCommand.Parameters("@sl_zdn_kanrino").SetValue(sl_zdn_kanrino)
      selectCommand.Parameters("@sl_zdn_sdldate").SetValue(sl_zdn_sdldate)
      selectCommand.Parameters("@sl_zdn_ubdate").SetValue(sl_zdn_ubdate)
      selectCommand.Parameters("@sl_zdn_bbdate").SetValue(sl_zdn_bbdate)
      selectCommand.Parameters("@sl_zdn_expdate").SetValue(sl_zdn_expdate)
      selectCommand.Parameters("@sl_zdn_ldid1").SetValue(sl_zdn_ldid1)
      selectCommand.Parameters("@sl_zdn_ldid2").SetValue(sl_zdn_ldid2)
      selectCommand.Parameters("@sl_zdn_ldid3").SetValue(sl_zdn_ldid3)
      selectCommand.Parameters("@sl_zdn_ldid4").SetValue(sl_zdn_ldid4)
      selectCommand.Parameters("@sl_zdn_ldid5").SetValue(sl_zdn_ldid5)
      selectCommand.Parameters("@sl_zdn_ldid6").SetValue(sl_zdn_ldid6)
      selectCommand.Parameters("@sl_zdn_ldid7").SetValue(sl_zdn_ldid7)
      selectCommand.Parameters("@sl_zdn_ldid8").SetValue(sl_zdn_ldid8)
      selectCommand.Parameters("@sl_zdn_ldid9").SetValue(sl_zdn_ldid9)
      selectCommand.Parameters("@sl_zdn_ldid10").SetValue(sl_zdn_ldid10)
      selectCommand.Parameters("@sl_zdn_ldmei1").SetValue(sl_zdn_ldmei1)
      selectCommand.Parameters("@sl_zdn_ldmei2").SetValue(sl_zdn_ldmei2)
      selectCommand.Parameters("@sl_zdn_ldmei3").SetValue(sl_zdn_ldmei3)
      selectCommand.Parameters("@sl_zdn_ldmei4").SetValue(sl_zdn_ldmei4)
      selectCommand.Parameters("@sl_zdn_ldmei5").SetValue(sl_zdn_ldmei5)
      selectCommand.Parameters("@sl_zdn_ldmei6").SetValue(sl_zdn_ldmei6)
      selectCommand.Parameters("@sl_zdn_ldmei7").SetValue(sl_zdn_ldmei7)
      selectCommand.Parameters("@sl_zdn_ldmei8").SetValue(sl_zdn_ldmei8)
      selectCommand.Parameters("@sl_zdn_ldmei9").SetValue(sl_zdn_ldmei9)
      selectCommand.Parameters("@sl_zdn_ldmei10").SetValue(sl_zdn_ldmei10)
      selectCommand.Parameters("@sl_zdn_relay_zdnid").SetValue(sl_zdn_relay_zdnid)
      selectCommand.Parameters("@sl_zdn_iri").SetValue(sl_zdn_iri)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function
End Class
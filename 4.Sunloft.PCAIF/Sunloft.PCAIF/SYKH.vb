﻿Imports PCA.ApplicationIntegration
Public Class SYKH

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function GetID(ByVal intSlipNo As Integer) As Integer
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim intRetID As Integer = 0
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SYKH")
      selectCommand.Parameters("@sykh_jno").SetValue(intSlipNo)

      reader = selectCommand.ExecuteReader
      If reader.Read Then intRetID = CInt(reader.GetValue("sykh_id"))

      Return intRetID
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return 0
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function GetSlipNo(ByVal intID As Integer) As Integer
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim intRetID As Integer = 0
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SYKH")
      selectCommand.Parameters("@sykh_id").SetValue(intID)

      reader = selectCommand.ExecuteReader
      If reader.Read Then intRetID = CInt(reader.GetValue("sykh_jno"))

      Return intRetID
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return 0
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

End Class

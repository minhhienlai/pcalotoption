﻿Imports PCA.ApplicationIntegration
Public Class SL_LMD
  Inherits clsPCAIFBase

  Public Property sl_lmd_id As Integer
  Public Property sl_lmd_ldcd As String
  Public Property sl_lmd_mei As String
  Public Property number As Integer '(1 ->10)
  Public Property isBeingUsed As Boolean = False

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication


  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Overrides Function Insert() As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_LMD" & number.ToString)
      selectCommand.Parameters("@sl_lmd_ldcd").SetValue(sl_lmd_ldcd)
      sl_lmd_id = GetNewID()
      Return Register(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_LMD" & number.ToString)
      Return Register(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function Register(ByVal selectCommand As ICustomCommand) As Boolean
    Try
      selectCommand.Parameters("@sl_lmd_id").SetValue(sl_lmd_id)
      selectCommand.Parameters("@sl_lmd_mei").SetValue(sl_lmd_mei)

      selectCommand.ExecuteNonQuery()
      Return True

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Public Overrides Function Delete() As Boolean
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      isBeingUsed = False
      selectCommand = connector.CreateIsolatedCommand((m_conf.PcaAddinID.Trim & "%" & "GET_SL_LMD" & number & "_IN_NYKH_ZDN"))
      selectCommand.Parameters("@sl_lmd_ldcd").SetValue(sl_lmd_ldcd)
      reader = selectCommand.ExecuteReader
      'Read the only row
      While reader.Read()
        isBeingUsed = True
      End While

      If Not isBeingUsed Then
        selectCommand = connector.CreateIsolatedCommand((m_conf.PcaAddinID.Trim & "%" & "DEL_SL_LMD" & number))
        selectCommand.Parameters("@sl_lmd_id").SetValue(sl_lmd_id)
        selectCommand.ExecuteNonQuery()
      End If

      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function ReadByID(ByVal intID As Integer) As Boolean
    Return Nothing
  End Function

  Public Function ReadAll() As SL_LMD() 'Return true if there's data
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intCount = -1
    Dim result As SL_LMD() = {New SL_LMD(connector)}
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_LMD" & number)
      reader = selectCommand.ExecuteReader
      'Read the only row
      While reader.Read()
        intCount = intCount + 1
        ReDim Preserve result(intCount)
        result(intCount) = New SL_LMD(connector)
        result(intCount).sl_lmd_id = CInt(reader.GetValue("sl_lmd_id"))
        result(intCount).sl_lmd_ldcd = reader.GetValue("sl_lmd_ldcd").ToString
        result(intCount).sl_lmd_mei = reader.GetValue("sl_lmd_mei").ToString
        result(intCount).number = number
      End While
      If result(0).sl_lmd_ldcd Is Nothing Then
        result = {}
      End If
      Return result
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intMaxID As Integer = -1
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_LMD" & number)
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        intMaxID = CInt(reader.GetValue("sl_lmd_id"))
      End If
      Return intMaxID + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function


End Class

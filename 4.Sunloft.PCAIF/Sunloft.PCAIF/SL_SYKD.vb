﻿Imports PCA.ApplicationIntegration
Public Class SL_SYKD
  Inherits clsPCAIFBase
  Public Property isExisted As Boolean = True
  Public Property sl_sykd_id As Integer
  Public Property sl_sykd_hid As Integer
  Public Property sl_sykd_eda As Integer
  Public Property sl_sykd_skbn As Integer
  Public Property sl_sykd_scd As String
  Public Property sl_sykd_mei As String
  Public Property sl_sykd_kikaku As String
  Public Property sl_sykd_color As String
  Public Property sl_sykd_size As String
  Public Property sl_sykd_souko As String
  Public Property sl_sykd_tax As Integer
  Public Property sl_sykd_komi As Integer
  Public Property sl_sykd_iri As Integer
  Public Property sl_sykd_hako As Integer
  Public Property sl_sykd_suryo As Double
  Public Property sl_sykd_honsu As Integer
  Public Property sl_sykd_tani As String
  Public Property sl_sykd_tanka As Decimal
  Public Property sl_sykd_kingaku As Double
  Public Property sl_sykd_gentan As Decimal
  Public Property sl_sykd_genka As Double
  Public Property sl_sykd_arari As Integer
  Public Property sl_sykd_biko As String
  Public Property sl_sykd_rate As Integer

  Public Property sl_zdn_ulotno As String = String.Empty
  Public Property sl_zdn_id As Integer

  Public Property intCurrentNumber As Integer 'Get from SL_ZDN
  Public Property dblCurrentQuantity As Decimal

  Public Property sl_sykd_convert As Integer

  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Private Const NOT_CONVERTED As Integer = 0
  Private Const ALREADY_CONVERTED As Integer = 1


  Public Sub New(ByRef myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      Return RegisterDetails(False, session)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      Return RegisterDetails(True, session)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function UpdateDetailsAfterConvert(ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_SYKD_Convert")
      selectCommand.Parameters("@sl_sykd_id").SetValue(sl_sykd_id)
      selectCommand.Parameters("@sl_sykd_convert").SetValue(ALREADY_CONVERTED)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Overrides Function Delete() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_SYKD")
      selectCommand.Parameters("@sl_sykd_id").SetValue(sl_sykd_id)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function DeleteByID(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_SYKD_FROM_HID")
      selectCommand.Parameters("@sl_sykd_hid").SetValue(sl_sykd_hid)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function ReadByID(ByVal intDetailsID As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SYKD")
      selectCommand.Parameters("@sl_sykd_id").SetValue(intDetailsID)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then

        sl_sykd_id = CInt(reader.GetValue("sl_sykd_id"))
        sl_sykd_hid = CInt(reader.GetValue("sl_sykd_hid"))
        sl_sykd_eda = CInt(reader.GetValue("sl_sykd_eda")) + 1
        If Not IsDBNull(reader.GetValue("sl_sykd_skbn")) Then sl_sykd_skbn = CInt(reader.GetValue("sl_sykd_skbn"))
        sl_sykd_scd = reader.GetValue("sl_sykd_scd").ToString.Trim
        sl_sykd_mei = reader.GetValue("sl_sykd_mei").ToString.Trim
        sl_sykd_kikaku = reader.GetValue("sl_sykd_kikaku").ToString.Trim
        sl_sykd_color = reader.GetValue("sl_sykd_color").ToString.Trim
        sl_sykd_size = reader.GetValue("sl_sykd_size").ToString.Trim
        sl_sykd_souko = reader.GetValue("sl_sykd_souko").ToString.Trim
        If Not IsDBNull(reader.GetValue("sl_sykd_tax")) Then sl_sykd_tax = CInt(reader.GetValue("sl_sykd_tax"))
        If Not IsDBNull(reader.GetValue("sl_sykd_komi")) Then sl_sykd_komi = CInt(reader.GetValue("sl_sykd_komi"))
        If Not IsDBNull(reader.GetValue("sl_sykd_iri")) Then sl_sykd_iri = CInt(reader.GetValue("sl_sykd_iri"))
        If Not IsDBNull(reader.GetValue("sl_sykd_hako")) Then sl_sykd_hako = CInt(reader.GetValue("sl_sykd_hako"))
        If Not IsDBNull(reader.GetValue("sl_sykd_suryo")) Then sl_sykd_suryo = CDec(reader.GetValue("sl_sykd_suryo"))
        If Not IsDBNull(reader.GetValue("sl_sykd_honsu")) Then sl_sykd_honsu = CInt(reader.GetValue("sl_sykd_honsu"))
        sl_sykd_tani = reader.GetValue("sl_sykd_tani").ToString.Trim
        If Not IsDBNull(reader.GetValue("sl_sykd_tanka")) Then sl_sykd_tanka = CDec(reader.GetValue("sl_sykd_tanka"))
        If Not IsDBNull(reader.GetValue("sl_sykd_kingaku")) Then sl_sykd_kingaku = CDec(reader.GetValue("sl_sykd_kingaku"))
        If Not IsDBNull(reader.GetValue("sl_sykd_gentan")) Then sl_sykd_gentan = CDec(reader.GetValue("sl_sykd_gentan"))
        If Not IsDBNull(reader.GetValue("sl_sykd_genka")) Then sl_sykd_genka = CDec(reader.GetValue("sl_sykd_genka"))
        If Not IsDBNull(reader.GetValue("sl_sykd_arari")) Then sl_sykd_arari = CInt(reader.GetValue("sl_sykd_arari"))
        sl_sykd_biko = reader.GetValue("sl_sykd_biko").ToString.Trim
        If Not IsDBNull(reader.GetValue("sl_sykd_rate")) Then sl_sykd_rate = CInt(reader.GetValue("sl_sykd_rate"))
        If Not IsDBNull(reader.GetValue("sl_sykd_convert")) Then sl_sykd_rate = CInt(reader.GetValue("sl_sykd_convert"))
        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Function ReadByHeaderID() As SL_SYKD()
    Dim selectCommand As ICustomCommand
    selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_SYKD_ZDN_FROM_HID")
    Return ReadAllFromHeaderID(selectCommand)
  End Function

  Private Function ReadAllFromHeaderID(ByRef selectCommand As ICustomCommand) As SL_SYKD()
    Dim reader As ICustomDataReader = Nothing
    Try
      selectCommand.Parameters("@sl_sykd_hid").SetValue(sl_sykd_hid)
      reader = selectCommand.ExecuteReader
      Dim myTableDetails As SL_SYKD() = {New SL_SYKD(connector)}
      Dim intBranchCount As Integer = -1
      While reader.Read()
        intBranchCount = intBranchCount + 1
        ReDim Preserve myTableDetails(intBranchCount)
        myTableDetails(intBranchCount) = New SL_SYKD(connector)
        myTableDetails(intBranchCount).sl_sykd_id = CInt(reader.GetValue("sl_sykd_id"))
        myTableDetails(intBranchCount).sl_sykd_hid = CInt(reader.GetValue("sl_sykd_hid"))
        myTableDetails(intBranchCount).sl_sykd_eda = CInt(reader.GetValue("sl_sykd_eda")) + 1
        myTableDetails(intBranchCount).sl_sykd_skbn = CInt(reader.GetValue("sl_sykd_skbn"))
        myTableDetails(intBranchCount).sl_sykd_scd = reader.GetValue("sl_sykd_scd").ToString.Trim
        myTableDetails(intBranchCount).sl_sykd_mei = reader.GetValue("sl_sykd_mei").ToString.Trim
        myTableDetails(intBranchCount).sl_sykd_kikaku = reader.GetValue("sl_sykd_kikaku").ToString.Trim
        myTableDetails(intBranchCount).sl_sykd_color = reader.GetValue("sl_sykd_color").ToString.Trim
        myTableDetails(intBranchCount).sl_sykd_size = reader.GetValue("sl_sykd_size").ToString.Trim
        myTableDetails(intBranchCount).sl_sykd_souko = reader.GetValue("sl_sykd_souko").ToString.Trim
        myTableDetails(intBranchCount).sl_sykd_tax = CInt(reader.GetValue("sl_sykd_tax"))
        myTableDetails(intBranchCount).sl_sykd_komi = CInt(reader.GetValue("sl_sykd_komi"))
        myTableDetails(intBranchCount).sl_sykd_rate = CInt(reader.GetValue("sl_sykd_rate"))
        myTableDetails(intBranchCount).sl_sykd_iri = CInt(reader.GetValue("sl_sykd_iri"))
        myTableDetails(intBranchCount).sl_sykd_hako = CInt(reader.GetValue("sl_sykd_hako"))
        myTableDetails(intBranchCount).sl_sykd_suryo = CDec(reader.GetValue("sl_sykd_suryo"))
        myTableDetails(intBranchCount).sl_sykd_honsu = CInt(reader.GetValue("sl_sykd_honsu"))
        myTableDetails(intBranchCount).sl_sykd_tani = reader.GetValue("sl_sykd_tani").ToString.Trim
        myTableDetails(intBranchCount).sl_sykd_tanka = CDec(reader.GetValue("sl_sykd_tanka"))
        myTableDetails(intBranchCount).sl_sykd_kingaku = CDbl(reader.GetValue("sl_sykd_kingaku"))
        myTableDetails(intBranchCount).sl_sykd_gentan = CDec(reader.GetValue("sl_sykd_gentan"))
        myTableDetails(intBranchCount).sl_sykd_genka = CDbl(reader.GetValue("sl_sykd_genka"))
        myTableDetails(intBranchCount).sl_sykd_biko = reader.GetValue("sl_sykd_biko").ToString.Trim
        myTableDetails(intBranchCount).sl_sykd_arari = CInt(reader.GetValue("sl_sykd_arari"))
        myTableDetails(intBranchCount).sl_zdn_ulotno = reader.GetValue("sl_zdn_ulotno").ToString

        myTableDetails(intBranchCount).sl_zdn_id = CInt(reader.GetValue("sl_zdn_id"))
        myTableDetails(intBranchCount).sl_sykd_convert = CInt(reader.GetValue("sl_sykd_convert"))

      End While

      Return myTableDetails
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Overrides Function GetNewID() As Integer
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim intMaxID As Integer = -1
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_SYKD_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        intMaxID = CInt(reader.GetValue("sl_sykd_id"))
      End If
      Return intMaxID + 1
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Private Function RegisterDetails(isDetailsExisted As Boolean, ByRef session As ICustomSession) As Boolean
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Try
      If isDetailsExisted Then
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_SYKD")
      Else
        selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_SYKD")
        If Not sl_sykd_id > 0 Then
          sl_sykd_id = GetNewID() + sl_sykd_eda 'If sl_sykd_id >0 -> Register more than 1 row in a session -> Do not use GetNewID
        End If
      End If
      selectCommand.Parameters("@sl_sykd_id").SetValue(sl_sykd_id)
      selectCommand.Parameters("@sl_sykd_hid").SetValue(sl_sykd_hid)
      selectCommand.Parameters("@sl_sykd_eda").SetValue(sl_sykd_eda)
      selectCommand.Parameters("@sl_sykd_skbn").SetValue(sl_sykd_skbn)
      selectCommand.Parameters("@sl_sykd_scd").SetValue(sl_sykd_scd)
      selectCommand.Parameters("@sl_sykd_mei").SetValue(sl_sykd_mei)
      selectCommand.Parameters("@sl_sykd_kikaku").SetValue(sl_sykd_kikaku)
      selectCommand.Parameters("@sl_sykd_color").SetValue(sl_sykd_color)
      selectCommand.Parameters("@sl_sykd_size").SetValue(sl_sykd_size)
      selectCommand.Parameters("@sl_sykd_souko").SetValue(sl_sykd_souko)
      selectCommand.Parameters("@sl_sykd_tax").SetValue(sl_sykd_tax)
      selectCommand.Parameters("@sl_sykd_komi").SetValue(sl_sykd_komi)
      selectCommand.Parameters("@sl_sykd_iri").SetValue(sl_sykd_iri)
      selectCommand.Parameters("@sl_sykd_hako").SetValue(sl_sykd_hako)
      selectCommand.Parameters("@sl_sykd_suryo").SetValue(sl_sykd_suryo)
      selectCommand.Parameters("@sl_sykd_honsu").SetValue(sl_sykd_honsu)
      selectCommand.Parameters("@sl_sykd_tani").SetValue(sl_sykd_tani)
      selectCommand.Parameters("@sl_sykd_tanka").SetValue(sl_sykd_tanka)
      selectCommand.Parameters("@sl_sykd_kingaku").SetValue(sl_sykd_kingaku)
      selectCommand.Parameters("@sl_sykd_gentan").SetValue(sl_sykd_gentan)
      selectCommand.Parameters("@sl_sykd_genka").SetValue(sl_sykd_genka)
      selectCommand.Parameters("@sl_sykd_arari").SetValue(sl_sykd_arari)
      selectCommand.Parameters("@sl_sykd_biko").SetValue(sl_sykd_biko)
      selectCommand.Parameters("@sl_sykd_rate").SetValue(sl_sykd_rate)
      selectCommand.Parameters("@sl_sykd_convert").SetValue(sl_sykd_convert)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

End Class

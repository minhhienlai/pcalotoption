﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Common
Public Class SL_ZHK
  Inherits clsPCAIFBase
  Private connector As IIntegratedApplication

  Public Property sl_zhk_id As Integer = 0
  Public Property sl_zhk_zdn_id As Integer = 0
  Public Property sl_zhk_datakbn As Integer = 0
  Public Property sl_zhk_dataid As Integer = 0
  Public Property sl_zhk_seq As Integer = 0
  Public Property sl_zhk_date As Integer = 0
  Public Property sl_zhk_suryo As Decimal = 0
  Public Property sl_zhk_honsu As Decimal = 0
  Public Property sl_zhk_insuser As String = ""
  Public Property sl_zhk_insdate As DateTime
  Public Property sl_zhk_upduser As String = ""
  Public Property sl_zhk_upddate As DateTime
  Public Property intRowCount As Integer = 0

  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig

  Public Sub New(ByRef myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  ''' <summary>Get New Slip ID</summary>
  ''' <returns>New Slip ID</returns>
  ''' <remarks></remarks>
  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intMaxID As Integer = -1
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_ZHK_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intMaxID = CInt(reader.GetValue("sl_zhk_id"))
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intMaxID + 1

  End Function

  Public Overrides Function Insert() As Boolean
    Try
      Return False
    Catch ex As Exception
      Return False
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try

      Dim selectCommand As ICustomCommand
      Dim intRet As Integer = 0

      sl_zhk_id = GetNewID()
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_ZHK")
      selectCommand.Parameters("@sl_zhk_id").SetValue(sl_zhk_id + sl_zhk_seq - 1 + intRowCount)

      selectCommand.Parameters("@sl_zhk_zdn_id").SetValue(sl_zhk_zdn_id)
      selectCommand.Parameters("@sl_zhk_datakbn").SetValue(sl_zhk_datakbn)
      selectCommand.Parameters("@sl_zhk_dataid").SetValue(sl_zhk_dataid)
      selectCommand.Parameters("@sl_zhk_seq").SetValue(sl_zhk_seq)
      selectCommand.Parameters("@sl_zhk_date").SetValue(sl_zhk_date)
      selectCommand.Parameters("@sl_zhk_honsu").SetValue(sl_zhk_honsu)
      selectCommand.Parameters("@sl_zhk_suryo").SetValue(sl_zhk_suryo)

      selectCommand.Parameters("@sl_zdn_insuser").SetValue(sl_zhk_insuser)
      selectCommand.Parameters("@sl_zdn_insdate").SetValue(sl_zhk_insdate)


      intRet = selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Return False
    Catch ex As Exception
      Return False
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      Dim intRet As Integer = 0

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_ZHK")
      Return ExecuteUpdate(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function UpdateByUNQKey(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      Dim intRet As Integer = 0

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_ZHK_BY_UNQ")
      Return ExecuteUpdate(selectCommand)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function ExecuteUpdate(ByVal selectCommand As ICustomCommand) As Boolean
    Try
      selectCommand.Parameters("@sl_zhk_id").SetValue(sl_zhk_id)

      selectCommand.Parameters("@sl_zhk_zdn_id").SetValue(sl_zhk_zdn_id)
      selectCommand.Parameters("@sl_zhk_datakbn").SetValue(sl_zhk_datakbn)
      selectCommand.Parameters("@sl_zhk_dataid").SetValue(sl_zhk_dataid)
      selectCommand.Parameters("@sl_zhk_seq").SetValue(sl_zhk_seq)
      selectCommand.Parameters("@sl_zhk_date").SetValue(sl_zhk_date)
      selectCommand.Parameters("@sl_zhk_honsu").SetValue(sl_zhk_honsu)
      selectCommand.Parameters("@sl_zhk_suryo").SetValue(sl_zhk_suryo)

      selectCommand.Parameters("@sl_zdn_upduser").SetValue(sl_zhk_upduser)
      selectCommand.Parameters("@sl_zdn_upddate").SetValue(sl_zhk_upddate)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Public Overrides Function Delete() As Boolean
    Return False
  End Function

  ''' <summary>Delete [SL_ZHK]</summary>
  Public Overrides Function Delete(ByRef session As PCA.ApplicationIntegration.ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_ZHK")
      selectCommand.Parameters("@sl_zhk_id").SetValue(sl_zhk_id)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

  ''' <summary>Delete [SL_ZHK]</summary>
  Public Function DeleteInventory(ByRef session As ICustomSession, ByVal isUseDataid As Boolean, ByVal isUseZdnID As Boolean) As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_ZHK")
      If isUseZdnID Then selectCommand.Parameters("@sl_zhk_zdn_id").SetValue(sl_zhk_zdn_id)

      selectCommand.Parameters("@sl_zhk_datakbn").SetValue(sl_zhk_datakbn)
      If isUseDataid Then selectCommand.Parameters("@sl_zhk_dataid").SetValue(sl_zhk_dataid)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>Delete [SL_ZHK]</summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function DeleteInventoryWithSEQ(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_ZHK")
      selectCommand.Parameters("@sl_zhk_zdn_id").SetValue(sl_zhk_zdn_id)
      selectCommand.Parameters("@sl_zhk_datakbn").SetValue(sl_zhk_datakbn)
      selectCommand.Parameters("@sl_zhk_dataid").SetValue(sl_zhk_dataid)
      selectCommand.Parameters("@sl_zhk_seq").SetValue(sl_zhk_seq)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>Read [SL_ZHK] From ID</summary>
  ''' <param name="intID">sl_zhk_id(在庫引当ID)</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function ReadByID(intID As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZHK")
    Dim isSuccess As Boolean = False

    Try
      selectCommand.Parameters("@sl_zhk_id").SetValue(intID)
      reader = selectCommand.ExecuteReader

      If reader.Read() Then

        sl_zhk_id = CInt(reader.GetValue("sl_zhk_id"))
        sl_zhk_zdn_id = CInt(reader.GetValue("sl_zhk_zdn_id"))
        sl_zhk_datakbn = CInt(reader.GetValue("sl_zhk_datakbn"))
        sl_zhk_dataid = CInt(reader.GetValue("sl_zhk_dataid"))
        sl_zhk_seq = CInt(reader.GetValue("sl_zhk_seq"))
        sl_zhk_date = CInt(reader.GetValue("sl_zhk_date"))
        sl_zhk_suryo = CInt(reader.GetValue("sl_zhk_suryo"))
        sl_zhk_honsu = CInt(reader.GetValue("sl_zhk_honsu"))
        sl_zhk_insuser = reader.GetValue("sl_zdn_insuser").ToString
        If Not IsDBNull(reader.GetValue("sl_zdn_insdate")) Then sl_zhk_insdate = DateTime.Parse(reader.GetValue("sl_zdn_insdate").ToString)

        If Not IsDBNull(reader.GetValue("sl_zdn_upduser")) Then sl_zhk_upduser = reader.GetValue("sl_zdn_upduser").ToString
        If Not IsDBNull(reader.GetValue("sl_zdn_upddate")) Then sl_zhk_upddate = DateTime.Parse(reader.GetValue("sl_zdn_upddate").ToString)

        isSuccess = True
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    Finally
      reader.Close()
      reader.Dispose()

    End Try
    Return isSuccess
  End Function

  ''' <summary>
  ''' Conditional Read [SL_ZHK] 
  ''' </summary>
  ''' <param name="intZdnID">sl_zhk_zdn_id : 在庫ID</param>
  ''' <param name="intDataKbn">sl_zhk_datakbn : 出庫区分</param>
  ''' <param name="intDataID">sl_zhk_dataid : 出庫区分明細ID</param>
  ''' <param name="intSEQ">SEQ</param>
  ''' <returns>if success table object, and if that fails nothing</returns>
  ''' <remarks></remarks>
  Public Function ReadByCondition(ByVal intZdnID As Integer, ByVal intDataKbn As Sunloft.Common.SLConstants.SL_ZHKSlipType, ByVal intDataID As Integer, Optional ByVal intSEQ As Integer = -1) As SL_ZHK()

    Dim reader As ICustomDataReader
    Dim myTableDetails As SL_ZHK() = {New SL_ZHK(connector)}
    Dim intBranchCount As Integer = 0
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZHK")
    selectCommand.Parameters("@sl_zhk_zdn_id").SetValue(intZdnID)
    selectCommand.Parameters("@sl_zhk_datakbn").SetValue(intDataKbn)
    selectCommand.Parameters("@sl_zhk_dataid").SetValue(intDataID)
    selectCommand.Parameters("@sl_zhk_seq").SetValue(intSEQ)
    reader = selectCommand.ExecuteReader
    Try

      While reader.Read()
        myTableDetails(intBranchCount) = New SL_ZHK(connector)

        With myTableDetails(intBranchCount)
          .sl_zhk_id = CInt(reader.GetValue("sl_zhk_id"))
          .sl_zhk_zdn_id = CInt(reader.GetValue("sl_zhk_zdn_id"))
          .sl_zhk_datakbn = CInt(reader.GetValue("sl_zhk_datakbn"))
          .sl_zhk_dataid = CInt(reader.GetValue("sl_zhk_dataid"))
          .sl_zhk_seq = CInt(reader.GetValue("sl_zhk_seq"))
          .sl_zhk_date = CInt(reader.GetValue("sl_zhk_date"))
          .sl_zhk_suryo = CInt(reader.GetValue("sl_zhk_suryo"))
          .sl_zhk_honsu = CInt(reader.GetValue("sl_zhk_honsu"))
          .sl_zhk_insuser = reader.GetValue("sl_zdn_insuser").ToString
          If Not IsDBNull(reader.GetValue("sl_zdn_insdate")) Then .sl_zhk_insdate = DateTime.Parse(reader.GetValue("sl_zdn_insdate").ToString)

          If Not IsDBNull(reader.GetValue("sl_zdn_upduser")) Then .sl_zhk_upduser = reader.GetValue("sl_zdn_upduser").ToString
          If Not IsDBNull(reader.GetValue("sl_zdn_upddate")) Then .sl_zhk_upddate = DateTime.Parse(reader.GetValue("sl_zdn_upddate").ToString)
        End With

        intBranchCount = intBranchCount + 1
        ReDim Preserve myTableDetails(intBranchCount)
      End While

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      myTableDetails = Nothing
    Finally
      reader.Close()
      reader.Dispose()

    End Try
    Return myTableDetails

  End Function
End Class

﻿Imports PCA.ApplicationIntegration
Public Class EMS
  Inherits clsPCAIFBase
  Private selectCommand As ICustomCommand
  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication

  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Overrides Function Insert() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Insert(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Update(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete() As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Overrides Function ReadByID(ByVal intID As Integer) As Boolean
    Try
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function ReadKbnMasterByIDAndKbn(ByVal ems_id As Sunloft.Common.SLConstants.EMS, ems_kbn As String) As String
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Dim intCount = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_EMS")
      selectCommand.Parameters("@ems_id").SetValue(ems_id)
      selectCommand.Parameters("@ems_kbn").SetValue(ems_kbn)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        Dim strResult As String = reader.GetValue("ems_str").ToString
        reader.Close()
        reader.Dispose()
        Return strResult
      End If
      Return String.Empty
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return String.Empty
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Public Overrides Function GetNewID() As Integer
    Return Nothing
  End Function
End Class

﻿Option Strict On
Option Explicit On

Imports PCA.ApplicationIntegration

Public MustInherit Class clsPCAIFBase
  Implements ICloneable           'Cloneメソッドの実装

  Public Function Clone() As Object Implements System.ICloneable.Clone

    Return Me.MemberwiseClone()
  End Function


  Public MustOverride Function Insert() As Boolean

  Public MustOverride Function Insert(ByRef session As ICustomSession) As Boolean

  Public MustOverride Function Update() As Boolean

  Public MustOverride Function Update(ByRef session As ICustomSession) As Boolean

  Public MustOverride Function Delete() As Boolean

  Public MustOverride Function Delete(ByRef session As ICustomSession) As Boolean

  Public MustOverride Function ReadByID(ByVal intID As Integer) As Boolean

  Public MustOverride Function GetNewID() As Integer

End Class
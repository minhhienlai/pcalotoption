﻿Imports PCA.ApplicationIntegration
Imports PCA.Kon.Integration
Imports Sunloft.PcaConfig
Imports System.Windows.Forms
Imports System.Environment
Imports System.Reflection
Imports System.Xml

Public Class ConnectToPCA
  Public Property connector As IIntegratedApplication
  Public Property EncryptedLoginString As String
  Public Property argumentContent As String
  Public Property isAttach As Boolean

  Public Function ConnectToPCA(Optional loginDataString As String = "") As IIntegratedApplication
    Dim conf = New Sunloft.PcaConfig
    Dim factory As New PCA.Kon.Integration.KonIntegrationFactory
    Dim result As PCA.ApplicationIntegration.IIntegratedResult
    Dim app As IIntegratedApplication
    Dim LoginString As String
    Dim i As Integer = 0

    '----- 連携対象アプリケーションがインストールされているかを検索 -----
    result = factory.FindIntegratedApplications("")
    If result.Status = IntegratedStatus.Failure Then
      MsgBox(result.ErrorMessage)
      Return Nothing
      Exit Function
    End If
    factory.AppId = conf.PcaFactoryAppID
    '----- 連携対象アプリケーションを作成 -----
    app = factory.CreateApplication
    Try
      If loginDataString = "" Then
        LoginString = GetCommandLine(0)
      Else
        LoginString = loginDataString 'Called from other Sunloft program
      End If

      If Not String.IsNullOrEmpty(LoginString) AndAlso LoginString <> "1" Then
        isAttach = True
        ' Split the string on the space character
        Dim parts As String() = LoginString.Split(New Char() {" "c})
        If parts.Length > 0 Then EncryptedLoginString = parts(0)
        If parts.Length > 1 Then argumentContent = parts(1)

        '----- アドインをアタッチする -----
        app.AttachAddin(EncryptedLoginString)
        GetLogOnUser(app)

      Else

        '----- 連携対象アプリケーションを作成 -----
        app = factory.CreateApplication
        app.IntegratedSecurity = False
        app.UserId = conf.PcaLoginUserID          '★ユーザーID
        app.Password = conf.PcaLoginPassword      '★パスワード
        app.ProgramId = conf.PcaAppProgramID
        app.ProgramName = conf.PcaAppProgramName

        '----- 商魂にログインする -----
        result = app.LogOnSystem
        'app.LogOnSystemにてログインエラーは吐き出してくれるので megboxは使用しない。
        If result.Status = IntegratedStatus.Failure Then Return Nothing

        '----- データ領域を指定する -----
        result = app.SelectDataArea(conf.PcaSelectDataArea) '★データ領域番号
        If result.Status = IntegratedStatus.Failure Then MsgBox(result.ErrorMessage) : Return Nothing

      End If

      '----- 連携先アプリケーションにおいてSQLコマンドを実行するためのセッションを作成 -----

      connector = app

      Return app
    Catch ex As Exception
      MessageBox.Show(ex.Message, "PCAにアクセスできません。", MessageBoxButtons.OK, MessageBoxIcon.Error)
      Return Nothing
    End Try
  End Function

  Public Shared Function ConnectToPCACalledPG() As IIntegratedApplication
    Dim conf = New Sunloft.PcaConfig
    Dim factory As New PCA.Kon.Integration.KonIntegrationFactory
    Dim result As PCA.ApplicationIntegration.IIntegratedResult
    Dim app As IIntegratedApplication
    Dim i As Integer = 0

    '----- 連携対象アプリケーションがインストールされているかを検索 -----
    result = factory.FindIntegratedApplications("")
    If result.Status = IntegratedStatus.Failure Then
      MsgBox(result.ErrorMessage)
      Return Nothing
      Exit Function
    End If
    factory.AppId = conf.PcaFactoryAppID
    '----- 連携対象アプリケーションを作成 -----
    app = factory.CreateApplication
    Try
      '----- 連携対象アプリケーションを作成 -----
      app = factory.CreateApplication
      app.IntegratedSecurity = False
      app.UserId = conf.PcaLoginUserID          '★ユーザーID
      app.Password = conf.PcaLoginPassword      '★パスワード
      app.ProgramId = conf.PcaAppProgramID
      app.ProgramName = conf.PcaAppProgramName

      '----- 商魂にログインする -----
      result = app.LogOnSystem
      'app.LogOnSystemにてログインエラーは吐き出してくれるので megboxは使用しない。
      If result.Status = IntegratedStatus.Failure Then Return Nothing

      '----- データ領域を指定する -----
      result = app.SelectDataArea(conf.PcaSelectDataArea) '★データ領域番号
      If result.Status = IntegratedStatus.Failure Then MsgBox(result.ErrorMessage) : Return Nothing

      Return app
    Catch ex As Exception
      MessageBox.Show(ex.Message, "PCAにアクセスできません。", MessageBoxButtons.OK, MessageBoxIcon.Error)
      Return Nothing
    End Try
  End Function

  Private Function GetCommandLine(ByVal argID As Integer) As String
    'Dim cmdArr() As String
    Dim cmdArr As String()
    Dim arg_val As String = ""
    Try
      'コマンドライン取得
      cmdArr = GetCommandLineArgs()
      Dim cmdItem As String()
      Dim arr_cnt As Integer = UBound(cmdArr)

      If (UBound(cmdArr) > 0) Then
        ' ;区切りで分割して配列に格納する
        Dim cmdRow As String() = cmdArr(1).Split(";"c)
        If cmdRow.Length < argID + 1 Then
          Return String.Empty
        End If
        If (cmdRow(argID) = String.Empty) Then
          Return String.Empty
        Else
          cmdItem = cmdRow(argID).Split("="c)
          If (UBound(cmdItem) = 0) Then
            Return cmdItem(0)
          Else
            Return cmdItem(1)
          End If
        End If
      End If

    Catch ex As Exception
      MessageBox.Show(ex.Message, "コマンドライン取得エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Try

    Return ""
  End Function

  Private Sub GetLogOnUser(ByRef app As PCA.ApplicationIntegration.IIntegratedApplication)
    Try

      Dim xDocument As XmlDocument = New XmlDocument
      Dim xRoot As XmlElement
      Dim xDataList As XmlNodeList

      xDocument.LoadXml(app.GetLogOnUser.BusinessValue)
      xRoot = xDocument.DocumentElement
      xDataList = xRoot.GetElementsByTagName("UserId")
      For Each xName As XmlElement In xDataList
        app.UserId = xName.FirstChild.Value
      Next

    Catch ex As Exception
      MessageBox.Show(ex.Message, "ログイン情報取得エラー(メニューからの呼び出し)", MessageBoxButtons.OK, MessageBoxIcon.Error)
      Return
    End Try

  End Sub
End Class

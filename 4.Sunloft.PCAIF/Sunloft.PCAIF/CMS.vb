﻿Imports PCA.ApplicationIntegration
Public Class CMS

  Public Property cms_id As Integer = 0
  Public Property cms_tms As Integer = 0
  Public Property cms_rms As Integer = 0
  Public Property cms_xms As Integer = 0
  Public Property cms_yms As Integer = 0
  Public Property cms_zero As Integer = 0
  Public Property cms_mei1 As String = String.Empty
  Public Property cms_mei2 As String = String.Empty
  Public Property cms_kanaidx As String = String.Empty
  Public Property cms_keisyo As String = String.Empty
  Public Property cms_mail As String = String.Empty
  Public Property cms_ad1 As String = String.Empty
  Public Property cms_ad2 As String = String.Empty
  Public Property cms_tel As String = String.Empty
  Public Property cms_fax As String = String.Empty
  Public Property cms_mailad As String = String.Empty
  Public Property cms_kosin As Integer = 0
  Public Property cms_insuser As Integer = 0
  Public Property cms_inspg As String = String.Empty
  Public Property cms_insdate As DateTime = Now
  Public Property cms_upduser As Integer = 0
  Public Property cms_updpg As String = String.Empty
  Public Property cms_upddate As DateTime = Now
  Public Property cms_ys1 As Integer = 0
  Public Property cms_yi1 As Integer = 0
  Public Property cms_ym1 As Integer = 0
  Public Property cms_yc1 As String = String.Empty
  Public Property cms_yc2 As String = String.Empty
  Public Property cms_yc3 As String = String.Empty
  Public Property cms_ccd1 As String = String.Empty
  Public Property cms_ccd2 As String = String.Empty
  Public Property cms_comment As String = String.Empty
  Public Property cms_hojin As String = String.Empty


  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private connector As IIntegratedApplication


  Public Sub New(ByVal myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  Public Function ReadByID(ByVal intID As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_CMS")
      selectCommand.Parameters("@cms_id").SetValue(intID)

      reader = selectCommand.ExecuteReader
      If reader.Read Then
        Me.cms_id = CInt(reader.GetValue("cms_id"))
        Me.cms_tms = CInt(reader.GetValue("cms_tms"))
        Me.cms_rms = CInt(reader.GetValue("cms_rms"))
        Me.cms_xms = CInt(reader.GetValue("cms_xms"))
        Me.cms_yms = CInt(reader.GetValue("cms_yms"))
        Me.cms_zero = CInt(reader.GetValue("cms_zero"))
        Me.cms_mei1 = reader.GetValue("cms_mei1").ToString
        Me.cms_mei2 = reader.GetValue("cms_mei2").ToString
        Me.cms_kanaidx = reader.GetValue("cms_kanaidx").ToString
        Me.cms_keisyo = reader.GetValue("cms_keisyo").ToString
        Me.cms_mail = reader.GetValue("cms_mail").ToString
        Me.cms_ad1 = reader.GetValue("cms_ad1").ToString
        Me.cms_ad2 = reader.GetValue("cms_ad2").ToString
        Me.cms_tel = reader.GetValue("cms_tel").ToString
        Me.cms_fax = reader.GetValue("cms_fax").ToString
        Me.cms_mailad = reader.GetValue("cms_mailad").ToString
        Me.cms_kosin = CInt(reader.GetValue("cms_kosin"))
        Me.cms_insuser = CInt(reader.GetValue("cms_insuser"))
        Me.cms_inspg = reader.GetValue("cms_inspg").ToString
        Me.cms_insdate = CDate(reader.GetValue("cms_insdate"))
        Me.cms_upduser = CInt(reader.GetValue("cms_upduser"))
        Me.cms_updpg = reader.GetValue("cms_updpg").ToString
        Me.cms_upddate = CDate(reader.GetValue("cms_upddate"))
        Me.cms_ys1 = CInt(reader.GetValue("cms_ys1"))
        Me.cms_yi1 = CInt(reader.GetValue("cms_yi1"))
        Me.cms_ym1 = CInt(reader.GetValue("cms_ym1"))
        Me.cms_yc1 = reader.GetValue("cms_yc1").ToString
        Me.cms_yc2 = reader.GetValue("cms_yc2").ToString
        Me.cms_yc3 = reader.GetValue("cms_yc3").ToString
        Me.cms_ccd1 = reader.GetValue("cms_ccd1").ToString
        Me.cms_ccd2 = reader.GetValue("cms_ccd2").ToString
        Me.cms_comment = reader.GetValue("cms_comment").ToString
        Me.cms_hojin = reader.GetValue("cms_hojin").ToString
        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

End Class

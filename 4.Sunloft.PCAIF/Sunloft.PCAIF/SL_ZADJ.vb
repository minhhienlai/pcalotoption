﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Common
Imports Sunloft.Message

Public Class SL_ZADJ
  Inherits clsPCAIFBase
  Private connector As IIntegratedApplication

  Public Property sl_zadj_id As Int32 = 0
  Public Property sl_zadj_denno As Int32 = 0
  Public Property sl_zadj_denno2 As String = String.Empty
  Public Property sl_zadj_date As Int32 = 0
  Public Property sl_zadj_scd As String = String.Empty
  Public Property sl_zadj_souko As String = String.Empty
  Public Property sl_zadj_iokbn As Sunloft.Common.SLConstants.SL_ZADJType
  Public Property sl_zadj_suryo As Decimal = 0D
  Public Property sl_zadj_honsu As Integer = 0
  Public Property sl_zadj_tekcd As String = String.Empty
  Public Property sl_zadj_tekmei As String = String.Empty
  Public Property sl_zadj_zdn_id As Int32 = 0
  Public Property sl_zadj_insuser As String = ""
  Public Property sl_zadj_insdate As DateTime
  Public Property sl_zadj_upduser As String = ""
  Public Property sl_zadj_upddate As DateTime

  Private Property m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Public Sub New(ByRef myApp As IIntegratedApplication)
    connector = myApp
  End Sub

  ''' <summary>Get New Slip ID</summary>
  ''' <returns>New Slip ID</returns>
  ''' <remarks></remarks>
  Public Overrides Function GetNewID() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intMaxId As Integer = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_ZADJ_ID")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intMaxId = CInt(reader.GetValue("sl_zadj_id"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intMaxId + 1
  End Function

  ''' <summary>Get New Slip No</summary>
  ''' <returns>New Slip No</returns>
  ''' <remarks></remarks>
  Private Function GetNewSlipNo() As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intMaxNo As Integer = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_MAX_SL_ZADJ_NO")
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intMaxNo = CInt(reader.GetValue("sl_zadj_denno"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intMaxNo + 1

  End Function

  ''' <summary>
  ''' Get Max SlipNo
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetMaxSlipNo() As Integer
    Dim intMaxNo As Integer = 0
    Try
      intMaxNo = GetNewSlipNo() - 1
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
    Return intMaxNo
  End Function

  ''' <summary>Get Next Slip No</summary>  
  ''' <param name="intNextSlipNo"></param>
  ''' <returns>Next Slip No</returns>
  ''' <remarks></remarks>
  Public Function GetNextSlipNo(ByVal intNextSlipNo As Integer) As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intNextNo As Integer = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZADJ_NEXT_SLIP")
      selectCommand.Parameters("@SlipNo").SetValue(intNextSlipNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intNextNo = CInt(reader.GetValue("sl_zadj_denno"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intNextNo
  End Function

  ''' <summary>Get Previous Slip No</summary>
  ''' <param name="intPrevSlipNo"></param>
  ''' <returns>Previous Slip No</returns>
  ''' <remarks></remarks>
  Public Function GetPrevSlipNo(ByVal intPrevSlipNo As Integer) As Integer
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand
    Dim intPrevNo As Integer = 0
    Try
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZADJ_PREVIOUS_SLIP")
      selectCommand.Parameters("@SlipNo").SetValue(intPrevSlipNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then intPrevNo = CInt(reader.GetValue("sl_zadj_denno"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      reader.Close()
      reader.Dispose()
    End Try
    Return intPrevNo
  End Function

#Region "Insert"

  Public Overrides Function Insert() As Boolean
    Return False
  End Function

  ''' <summary>Insert[SL_ZADJ]</summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks>在庫調整データ(SL_ZADJ)に登録します。</remarks>
  Public Overrides Function Insert(ByRef session As PCA.ApplicationIntegration.ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "INS_SL_ZADJ")
      sl_zadj_id = GetNewID()
      sl_zadj_denno = GetNewSlipNo()

      selectCommand.Parameters("@sl_zadj_id").SetValue(sl_zadj_id)
      selectCommand.Parameters("@sl_zadj_denno").SetValue(sl_zadj_denno)
      selectCommand.Parameters("@sl_zadj_denno2").SetValue(sl_zadj_denno2)
      selectCommand.Parameters("@sl_zadj_date").SetValue(sl_zadj_date)
      selectCommand.Parameters("@sl_zadj_scd").SetValue(sl_zadj_scd)
      selectCommand.Parameters("@sl_zadj_souko").SetValue(sl_zadj_souko)
      selectCommand.Parameters("@sl_zadj_iokbn").SetValue(sl_zadj_iokbn)
      selectCommand.Parameters("@sl_zadj_suryo").SetValue(sl_zadj_suryo)
      selectCommand.Parameters("@sl_zadj_honsu").SetValue(sl_zadj_honsu)
      selectCommand.Parameters("@sl_zadj_tekcd").SetValue(sl_zadj_tekcd)
      selectCommand.Parameters("@sl_zadj_tekmei").SetValue(sl_zadj_tekmei)
      selectCommand.Parameters("@sl_zadj_zdn_id").SetValue(sl_zadj_zdn_id)
      selectCommand.Parameters("@sl_zadj_insuser").SetValue(sl_zadj_insuser)
      selectCommand.Parameters("@sl_zadj_insdate").SetValue(sl_zadj_insdate)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "Update"
  Public Overrides Function Update() As Boolean
    Return False
  End Function

  ''' <summary>Update[SL_ZADJ]</summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks>在庫調整データ(SL_ZADJ)を更新します。</remarks>
  Public Overrides Function Update(ByRef session As PCA.ApplicationIntegration.ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "UPD_SL_ZADJ")
      selectCommand.Parameters("@sl_zadj_id").SetValue(sl_zadj_id)
      selectCommand.Parameters("@sl_zadj_denno").SetValue(sl_zadj_denno)
      selectCommand.Parameters("@sl_zadj_denno2").SetValue(sl_zadj_denno2)
      selectCommand.Parameters("@sl_zadj_date").SetValue(sl_zadj_date)
      selectCommand.Parameters("@sl_zadj_scd").SetValue(sl_zadj_scd)
      selectCommand.Parameters("@sl_zadj_souko").SetValue(sl_zadj_souko)
      selectCommand.Parameters("@sl_zadj_suryo").SetValue(sl_zadj_suryo)
      selectCommand.Parameters("@sl_zadj_honsu").SetValue(sl_zadj_honsu)
      selectCommand.Parameters("@sl_zadj_iokbn").SetValue(sl_zadj_iokbn)
      selectCommand.Parameters("@sl_zadj_tekcd").SetValue(sl_zadj_tekcd)
      selectCommand.Parameters("@sl_zadj_tekmei").SetValue(sl_zadj_tekmei)
      selectCommand.Parameters("@sl_zadj_zdn_id").SetValue(sl_zadj_zdn_id)
      selectCommand.Parameters("@sl_zadj_upduser").SetValue(sl_zadj_upduser)
      selectCommand.Parameters("@sl_zadj_upddate").SetValue(sl_zadj_upddate)

      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "Delete"

  Public Overrides Function Delete() As Boolean
    Return False
  End Function

  ''' <summary>在庫調整データ(SL_ZADJ)から削除します。</summary>
  Public Overrides Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Dim selectCommand As ICustomCommand

      selectCommand = session.CreateCommand(m_conf.PcaAddinID.Trim & "%" & "DEL_SL_ZADJ")
      selectCommand.Parameters("@sl_zadj_id").SetValue(sl_zadj_id)
      selectCommand.ExecuteNonQuery()
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region "SELECT"

  Public Overrides Function ReadByID(intSlipNo As Integer) As Boolean
    Dim reader As ICustomDataReader = Nothing
    Dim selectCommand As ICustomCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZADJ")
    Dim isSuccess As Boolean = False

    Try
      selectCommand.Parameters("@sl_zadj_denno").SetValue(intSlipNo)
      reader = selectCommand.ExecuteReader

      If reader.Read() Then

        If IsNumeric(reader.GetValue("sl_zadj_id")) Then sl_zadj_id = CInt(reader.GetValue("sl_zadj_id"))
        If IsNumeric(reader.GetValue("sl_zadj_denno")) Then sl_zadj_denno = CInt(reader.GetValue("sl_zadj_denno"))

        sl_zadj_denno2 = reader.GetValue("sl_zadj_denno2").ToString

        sl_zadj_date = CInt(reader.GetValue("sl_zadj_date"))

        sl_zadj_scd = reader.GetValue("sl_zadj_scd").ToString
        sl_zadj_souko = reader.GetValue("sl_zadj_souko").ToString

        sl_zadj_honsu = CInt(reader.GetValue("sl_zadj_honsu").ToString)
        sl_zadj_suryo = CDec(reader.GetValue("sl_zadj_suryo").ToString)
        If IsNumeric(reader.GetValue("sl_zadj_zdn_id")) Then sl_zadj_zdn_id = CInt(reader.GetValue("sl_zadj_zdn_id"))

        sl_zadj_iokbn = CType(reader.GetValue("sl_zadj_iokbn").ToString, SLConstants.SL_ZADJType)

        sl_zadj_tekcd = reader.GetValue("sl_zadj_tekcd").ToString
        sl_zadj_tekmei = reader.GetValue("sl_zadj_tekmei").ToString

        sl_zadj_insuser = reader.GetValue("sl_zadj_insuser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_zadj_insdate").ToString) Then
          sl_zadj_insdate = CDate(reader.GetValue("sl_zadj_insdate"))
        Else
          sl_zadj_insdate = Nothing
        End If

        sl_zadj_upduser = reader.GetValue("sl_zadj_upduser").ToString

        If Not String.IsNullOrEmpty(reader.GetValue("sl_zadj_upddate").ToString) Then
          sl_zadj_upddate = CDate(reader.GetValue("sl_zadj_upddate"))
        Else
          sl_zadj_upddate = Nothing
        End If

        isSuccess = True
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    Finally
      reader.Close()
      reader.Dispose()

    End Try
    Return isSuccess
  End Function

#End Region
End Class

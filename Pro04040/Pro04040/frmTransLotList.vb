﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports Sunloft.PCAIF
Imports PCA.Controls
Imports System.ComponentModel
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon


Public Class frmTransLotList
  Private connector As IIntegratedApplication = Nothing
  
  Private m_RowIndex As Integer = -1
  Private m_conf As Sunloft.PcaConfig

  Private m_isKeyDowned As Boolean = False
  Private m_isCodeFrom As Boolean = False
  Private MasterDialog As SLMasterSearchDialog
  Private appClass As ConnectToPCA = New ConnectToPCA()
  Private m_SL_LMBClass As SL_LMB
  Private m_AMS1Class As AMS1
  Private m_loginString As String

  'Table Column's width Constants
  Private Const DATE_LENGTH As Short = 13
  Private Const SLIPNO_LENGTH As Short = 10
  Private Const PRODUCT_CODE_LENGTH As Short = 5
  Private Const PRODUCT_NAME_LENGTH As Short = 40
  Private Const WAREHOUSE_CODE_LENGTH As Short = 5
  Private Const WAREHOUSE_NAME_LENGTH As Short = 20
  Private Const QUANTITY_LENGTH As Short = 10
  Private Const NUMBER_LENGTH As Short = 6
  Private Const UNIT_LENGTH As Short = 6
  Private Const LOTNO_LENGTH As Short = 20
  Private Const LOTDETAIL_LENGTH As Short = 20

  Private Const TRANS_PG_NAME As String = "\Pro04030.exe"
  Private Const PG_NAME As String = "振替ロット一覧表"

  'About Print Constants
  Private Const RPT_FILENAME As String = "TransLotList.rpt"

  Private m_intCondDateFrom As Integer
  Private m_intCondDateTo As Integer

#Region "Initialize"
  Public Sub New()

    ' この呼び出しはデザイナーで必要です。
    InitializeComponent()
    ' InitializeComponent() 呼び出しの後で初期化を追加します。
    appClass.ConnectToPCA()
    connector = appClass.connector

    m_conf = New Sunloft.PcaConfig
    m_SL_LMBClass = New SL_LMB(connector)
    m_SL_LMBClass.ReadOnlyRow()
    m_AMS1Class = New AMS1(connector)
    m_AMS1Class.ReadAll()
    MasterDialog = New SLMasterSearchDialog(connector)

    m_loginString = appClass.EncryptedLoginString

    InitTable()

    'Functionkey Enable Setting
    InitFunctionKey()

    InitMenuButton()

    'Controls String length setting
    rsetProduct.LimitLength = m_AMS1Class.ams1_ProductLength
    rsetFromWarehouse.LimitLength = m_AMS1Class.ams1_WarehouseLength
    rsetToWarehouse.LimitLength = m_AMS1Class.ams1_WarehouseLength

    ToolStripMenuItemRefer.Enabled = False

    tblTable.Enabled = True
    rdtMoveDate.Focus()

    ShowTransLotList()

  End Sub


  ''' <summary>Initialize Table</summary>
  ''' <remarks></remarks>
  Private Sub InitTable()
    Try
      With tblTable
        .DataRowCount = 99

        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader()     'ヘッダー部
        InitTableBody()       'ボディ部

        .UpdateTable()

        .BodyColumns.DefaultStyle.BackColor = Color.White
        .BodyColumns.DefaultAltStyle.BackColor = Color.White

      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>ファンクションキーの初期化</summary>
  ''' <remarks></remarks>
  Private Sub InitFunctionKey()
    Try

      PcaFunctionCommandHelp.Enabled = True
      PcaFunctionCommandSearch.Enabled = True
      PcaFunctionCommandRefer.Enabled = False
      PcaFunctionCommandPrint.Enabled = False
      PcaFunctionCommandPreview.Enabled = False
      PcaFunctionCommandOutPut.Enabled = False
      PcaFunctionCommandClose.Enabled = True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>メニューボタンの初期化</summary>
  ''' <remarks></remarks>
  Private Sub InitMenuButton()
    Try

      ToolStripButtonHelp.Enabled = True
      ToolStripButtonSearch.Enabled = True
      ToolStripButtonPrint.Enabled = False
      ToolStripButtonPreview.Enabled = False
      ToolStripButtonOutPut.Enabled = False
      ToolStripButtonClose.Enabled = True

      ToolStripMenuItemHelp.Enabled = True
      ToolStripMenuItemSearch.Enabled = True
      ToolStripMenuItemRefer.Enabled = False
      ToolStripMenuItemPrint.Enabled = False
      ToolStripMenuItemPreview.Enabled = False
      ToolStripMenuItemOutPut.Enabled = False
      ToolStripMenuItemClose.Enabled = True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>起動時のフォーム初期化処理</summary>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub OnLoad(e As System.EventArgs)

    Dim configXMLDoc As New System.Xml.XmlDocument

    Me.tblTable.SelectColumnsMode = True
    Me.tblTable.InputFrameMode = True
    Me.tblTable.AutoInputMode = True

    MyBase.OnLoad(e)

  End Sub

  ''' <summary>一覧画面のヘッダ初期化</summary>
  ''' <remarks></remarks>
  Private Sub InitTableHeader()
    tblTable.HeadTextHeight = 1
    tblTable.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
    tblTable.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

    ColumnName.Unit = m_AMS1Class.UnitName

    Dim column As PcaColumn
    Dim cell As PcaCell
    Dim intLpc As Integer = 0
    Dim strLotDetails As String()
    Dim strHeaderName As String = String.Empty

    strLotDetails = {ColumnName.LotName1, ColumnName.LotName2, ColumnName.LotName3, ColumnName.LotName4, ColumnName.LotName5 _
                    , ColumnName.LotName6, ColumnName.LotName7, ColumnName.LotName8, ColumnName.LotName9, ColumnName.LotName10}

    column = New PcaColumn(ColumnName.MoveDate, DATE_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(ColumnName.MoveDate)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    column = New PcaColumn(ColumnName.ArrivalDate, DATE_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(ColumnName.ArrivalDate)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    column = New PcaColumn(ColumnName.SlipNo, SLIPNO_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(ColumnName.SlipNo)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    strHeaderName = Replace(ColumnName.ProductCode, "コード", String.Empty)
    column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_ProductLength + 2 + PRODUCT_NAME_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(strHeaderName)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    strHeaderName = Replace(ColumnName.WarehouseFromCode, "コード", String.Empty)
    column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_WarehouseLength + 2 + WAREHOUSE_NAME_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(strHeaderName)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    strHeaderName = Replace(ColumnName.WarehouseToCode, "コード", String.Empty)
    column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_WarehouseLength + 2 + WAREHOUSE_NAME_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(strHeaderName)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    column = New PcaColumn(ColumnName.Number, NUMBER_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(ColumnName.Number)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    column = New PcaColumn(ColumnName.Quantity, QUANTITY_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(ColumnName.Quantity)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    column = New PcaColumn(ColumnName.Unit, UNIT_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(ColumnName.Unit)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    column = New PcaColumn(ColumnName.LotNo, LOTNO_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(ColumnName.LotNo)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)

    If m_SL_LMBClass.sl_lmb_sdlflg = 1 Then
      column = New PcaColumn(ColumnName.SdlDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.SdlDate)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)
    End If

    If m_SL_LMBClass.sl_lmb_ubdflg = 1 Then
      column = New PcaColumn(ColumnName.UbdDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.UbdDate)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)
    End If

    If m_SL_LMBClass.sl_lmb_bbdflg = 1 Then
      column = New PcaColumn(ColumnName.BbDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.BbDate)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)
    End If

    If m_SL_LMBClass.sl_lmb_expflg = 1 Then
      column = New PcaColumn(ColumnName.ExpDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.ExpDate)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)
    End If

    For intLpc = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
      column = New PcaColumn(strLotDetails(intLpc), LOTDETAIL_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(strLotDetails(intLpc))
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)
    Next

    'hidden
    InitHeaderColumn(column, cell, 0, ColumnName.MoveWarehouseID, ColumnName.MoveWarehouseID)
    InitHeaderColumn(column, cell, 0, ColumnName.SlipNo2, ColumnName.SlipNo2)
    InitHeaderColumn(column, cell, 0, ColumnName.Summary, ColumnName.Summary)

  End Sub


  ''' <summary>非表示列の設定</summary>
  ''' <param name="column"></param>
  ''' <param name="cell"></param>
  ''' <param name="columnWidth"></param>
  ''' <param name="cellName"></param>
  ''' <param name="columnName"></param>
  ''' <remarks></remarks>
  Private Sub InitHeaderColumn(ByVal column As PcaColumn, ByVal cell As PcaCell, ByVal columnWidth As Integer, ByVal cellName As String, ByVal columnName As String)
    column = New PcaColumn(columnName, columnWidth)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(cellName, columnWidth, 3, 0.0F, 0.0F)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)
  End Sub

  ''' <summary>一覧画面の明細部初期化</summary>
  ''' <remarks></remarks>
  Private Sub InitTableBody()
    tblTable.BodyTextHeight = 1
    '奇数行、偶数行は各デフォルト色とする
    tblTable.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
    tblTable.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
    tblTable.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
    tblTable.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

    Dim column As PcaColumn
    Dim cell As PcaCell
    Dim intLpc As Integer = 0
    Dim strLotDetails As String()
    Dim strHeaderName As String = String.Empty

    strLotDetails = {ColumnName.LotName1, ColumnName.LotName2, ColumnName.LotName3, ColumnName.LotName4, ColumnName.LotName5 _
                    , ColumnName.LotName6, ColumnName.LotName7, ColumnName.LotName8, ColumnName.LotName9, ColumnName.LotName10}

    column = New PcaColumn(ColumnName.MoveDate, DATE_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(ColumnName.MoveDate, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.MoveDate)
    cell.EditMode = False
    cell.Enabled = False
    cell.CellStyle.Alignment = StringAlignment.Center
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    column = New PcaColumn(ColumnName.ArrivalDate, DATE_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(ColumnName.ArrivalDate, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.ArrivalDate)
    cell.EditMode = False
    cell.Enabled = False
    cell.CellStyle.Alignment = StringAlignment.Center
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    column = New PcaColumn(ColumnName.SlipNo, SLIPNO_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(ColumnName.SlipNo, SLIPNO_LENGTH, 1, 0.0F, 0.0F, ColumnName.SlipNo)
    cell.EditMode = False
    cell.Enabled = False
    cell.CellStyle.Alignment = StringAlignment.Center
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    strHeaderName = Replace(ColumnName.ProductCode, "コード", String.Empty)
    column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_ProductLength + 2 + PRODUCT_NAME_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(ColumnName.ProductCode, m_AMS1Class.ams1_ProductLength + 2, 1, 0.0F, 0.0F, strHeaderName)
    cell.EditMode = False
    cell.Enabled = False
    column.Cells.Add(cell)
    cell = tblTable.PrepareTextCell(ColumnName.ProductName, PRODUCT_NAME_LENGTH, 1, m_AMS1Class.ams1_ProductLength + 2, 0.0F, strHeaderName)
    cell.EditMode = False
    cell.Enabled = False
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    strHeaderName = Replace(ColumnName.WarehouseFromCode, "コード", String.Empty)
    column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_WarehouseLength + 2 + WAREHOUSE_NAME_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(ColumnName.WarehouseFromCode, m_AMS1Class.ams1_WarehouseLength + 2, 1, 0.0F, 0.0F, strHeaderName)
    cell.EditMode = False
    cell.Enabled = False
    column.Cells.Add(cell)
    cell = tblTable.PrepareTextCell(ColumnName.WarehouseFromName, WAREHOUSE_NAME_LENGTH, 1, m_AMS1Class.ams1_WarehouseLength + 2, 0.0F, strHeaderName)
    cell.EditMode = False
    cell.Enabled = False
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    strHeaderName = Replace(ColumnName.WarehouseToCode, "コード", String.Empty)
    column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_WarehouseLength + 2 + WAREHOUSE_NAME_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(ColumnName.WarehouseToCode, m_AMS1Class.ams1_WarehouseLength + 2, 1, 0.0F, 0.0F, strHeaderName)
    cell.EditMode = False
    cell.Enabled = False
    column.Cells.Add(cell)
    cell = tblTable.PrepareTextCell(ColumnName.WarehouseToName, WAREHOUSE_NAME_LENGTH, 1, m_AMS1Class.ams1_WarehouseLength + 2, 0.0F, strHeaderName)
    cell.EditMode = False
    cell.Enabled = False
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    column = New PcaColumn(ColumnName.Number, NUMBER_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareNumberCell(ColumnName.Number, NUMBER_LENGTH, 1, 0.0F, 0.0F, ColumnName.Number)
    cell.EditMode = False
    cell.Enabled = False
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    column = New PcaColumn(ColumnName.Quantity, QUANTITY_LENGTH)
    column.CanResize = False
    cell = tblTable.PrepareTextCell(ColumnName.Quantity, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.Quantity)
    cell.EditMode = False
    cell.Enabled = False
    cell.FormatDecimal = True

    cell.CellStyle.Alignment = StringAlignment.Far
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    column = New PcaColumn(ColumnName.Unit, UNIT_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(ColumnName.Unit, UNIT_LENGTH, 1, 0.0F, 0.0F, ColumnName.Unit)
    cell.EditMode = False
    cell.Enabled = False
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    column = New PcaColumn(ColumnName.LotNo, LOTNO_LENGTH)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(ColumnName.LotNo, LOTNO_LENGTH, 1, 0.0F, 0.0F, ColumnName.LotNo)
    cell.EditMode = False
    cell.Enabled = False
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)

    If m_SL_LMBClass.sl_lmb_SdlFlg = 1 Then
      column = New PcaColumn(ColumnName.SdlDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.SdlDate, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.SdlDate)
      cell.EditMode = False
      cell.Enabled = False
      cell.CellStyle.Alignment = StringAlignment.Center
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)
    End If

    If m_SL_LMBClass.sl_lmb_UbdFlg = 1 Then
      column = New PcaColumn(ColumnName.UbdDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.UbdDate, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.UbdDate)
      cell.EditMode = False
      cell.Enabled = False
      cell.CellStyle.Alignment = StringAlignment.Center
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)
    End If

    If m_SL_LMBClass.sl_lmb_BbdFlg = 1 Then
      column = New PcaColumn(ColumnName.BbDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.BbDate, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.BbDate)
      column.Cells.Add(cell)
      cell.EditMode = False
      cell.Enabled = False
      cell.CellStyle.Alignment = StringAlignment.Center
      tblTable.BodyColumns.Add(column)
    End If

    If m_SL_LMBClass.sl_lmb_ExpFlg = 1 Then
      column = New PcaColumn(ColumnName.ExpDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.ExpDate, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.ExpDate)
      column.Cells.Add(cell)
      cell.EditMode = False
      cell.Enabled = False
      cell.CellStyle.Alignment = StringAlignment.Center
      tblTable.BodyColumns.Add(column)
    End If

    For intLpc = 0 To m_SL_LMBClass.sl_lmb_MaxLotOp - 1
      column = New PcaColumn(strLotDetails(intLpc), LOTDETAIL_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(strLotDetails(intLpc), LOTDETAIL_LENGTH, 1, 0.0F, 0.0F, strLotDetails(intLpc))
      cell.EditMode = False
      cell.Enabled = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)
    Next

    'hidden
    InitBodyColumn(column, cell, ColumnName.MoveWarehouseID, ColumnName.MoveWarehouseID, 0, 1, ColumnName.MoveWarehouseID, False)
    InitBodyColumn(column, cell, ColumnName.SlipNo2, ColumnName.SlipNo2, 0, 1, ColumnName.SlipNo2, False)
    InitBodyColumn(column, cell, ColumnName.Summary, ColumnName.Summary, 0, 1, ColumnName.Summary, False)

  End Sub

  ''' <summary>非表示列の設定</summary>
  ''' <param name="column"></param>
  ''' <param name="cell"></param>
  ''' <param name="columnName"></param>
  ''' <param name="cellname"></param>
  ''' <param name="cellwidth"></param>
  ''' <param name="cellHeight"></param>
  ''' <param name="headerName"></param>
  ''' <param name="editable"></param>
  ''' <remarks></remarks>
  Private Sub InitBodyColumn(column As PcaColumn, cell As PcaCell, columnName As String, cellname As String, cellwidth As Short, cellHeight As Short, headerName As String, Optional ByVal editable As Boolean = True)
    column = New PcaColumn(columnName, cellwidth)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(cellname, cellwidth, cellHeight, 0, 0, headerName)
    cell.EditMode = editable
    cell.HideSelection = True
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)
  End Sub

#End Region

#Region "PcaCommandManager (Include Function Event)"

  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Dim commandItem As PcaCommandItem = e.CommandItem
    Select Case commandItem.CommandName
      Case PcaCommandItemHelp.CommandName 'F1

      Case PcaCommandItemSearch.CommandName 'F5

        If m_isKeyDowned Then m_isKeyDowned = False : Exit Sub

        ShowTransLotList()

      Case PcaCommandItemRefer.CommandName 'F8

        If m_isKeyDowned Then m_isKeyDowned = False : Exit Sub

        Dim ojbCtrl = Me.ActiveControl

        If ojbCtrl.GetType.Name = rsetFromWarehouse.GetType.Name Then SearchItem(DirectCast(ojbCtrl, PcaRangeCodeSet), , m_isCodeFrom)

      Case PcaCommandItemPrint.CommandName 'F9

        If tblTable.GetCellValue(0, ColumnName.MoveWarehouseID).ToString.Trim.Length > 0 Then
          If MsgBox(PG_NAME & "を" & SLConstants.ConfirmMessage.PRINT_CONFIRM_MESSAGE, MsgBoxStyle.OkCancel, SLConstants.NotifyMessage.TITLE_MESSAGE) = MsgBoxResult.Ok Then
            PrintData()
          End If
        End If

      Case PcaCommandItemPreview.CommandName 'Shift + F9

        PrintData(True)

      Case PcaCommandItemOutPut.CommandName 'F10

        If OutPutCSV() Then
          Sunloft.Common.SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, _
                                                   SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, commandItem.CommandId)
        End If

      Case PcaCommandItemClose.CommandName 'F12
        LogOffClose()

    End Select
  End Sub

#End Region

#Region "PcaTable"
  Private Sub tblTable_EnterInputRow(sender As Object, args As PCA.Controls.InputRowEventArgs) Handles tblTable.EnterInputRow
    m_RowIndex = args.RowIndex
  End Sub

  Private Sub tblTable_DoubleClickCell(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.DoubleClickCell
    If Not tblTable.GetCellValue(args.Point.Y, ColumnName.SlipNo) Is Nothing AndAlso tblTable.GetCellValue(args.Point.Y, ColumnName.SlipNo).ToString.Trim <> String.Empty Then
      SLCmnFunction.StartUpInputExe(IO.Path.GetDirectoryName(Application.ExecutablePath) & TRANS_PG_NAME, m_loginString, tblTable.GetCellValue(args.Point.Y, ColumnName.SlipNo).ToString)
    End If
  End Sub

#End Region

#Region "PcaRangeCodeSet"
  Private Sub PcaRangeCodeSet_ClickReferButton2From(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles rsetProduct.ClickFromReferButton2, _
                                                                                                                                 rsetFromWarehouse.ClickFromReferButton2, _
                                                                                                                                 rsetToWarehouse.ClickFromReferButton2

    Dim objRangeCodeSet As PcaRangeCodeSet = DirectCast(sender, PcaRangeCodeSet)
    SearchItem(objRangeCodeSet, e)
  End Sub

  Private Sub PcaRangeCodeSet_ClickReferButton2To(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles rsetProduct.ClickToReferButton2, _
                                                                                                                             rsetFromWarehouse.ClickToReferButton2, _
                                                                                                                             rsetToWarehouse.ClickToReferButton2

    Dim objRangeCodeSet As PcaRangeCodeSet = DirectCast(sender, PcaRangeCodeSet)
    SearchItem(objRangeCodeSet, e, False)
  End Sub


  Private Sub PcaRangeCodeSetFrom_Enter(sender As System.Object, e As System.EventArgs) Handles rsetProduct.FromCodeTextEnter, _
                                                                                                rsetFromWarehouse.FromCodeTextEnter, _
                                                                                                rsetToWarehouse.FromCodeTextEnter
    PcaFunctionCommandRefer.Enabled = True
    ToolStripMenuItemRefer.Enabled = True
    m_isCodeFrom = True
  End Sub

  Private Sub PcaRangeCodeSetTo_Enter(sender As System.Object, e As System.EventArgs) Handles rsetProduct.ToCodeTextEnter, _
                                                                                              rsetFromWarehouse.ToCodeTextEnter, _
                                                                                              rsetToWarehouse.ToCodeTextEnter
    PcaFunctionCommandRefer.Enabled = True
    ToolStripMenuItemRefer.Enabled = True
    m_isCodeFrom = False
  End Sub

  Private Sub PcaRangeCodeSet_Leave(sender As System.Object, e As System.EventArgs) Handles rsetProduct.Leave, rsetFromWarehouse.Leave, rsetToWarehouse.Leave

    Dim objRangeCodeSet As PcaRangeCodeSet = DirectCast(sender, PcaRangeCodeSet)

    PcaFunctionCommandRefer.Enabled = False
    ToolStripMenuItemRefer.Enabled = False

  End Sub

  Private Sub PcaRangeCodeSet_ReturnPress(sender As System.Object, e As System.EventArgs) Handles rsetToWarehouse.ToCodeSetReturnPress
    ShowTransLotList()
  End Sub
#End Region

#Region "Form Events"
  Private Sub frmTransLotList_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
    If e.KeyCode = Keys.F8 Then m_isKeyDowned = True
  End Sub
#End Region

#Region "Private Method"

  ''' <summary>Search of each master</summary>
  ''' <param name="argPcaRangeCodeSet"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub SearchItem(ByVal argPcaRangeCodeSet As PcaRangeCodeSet, Optional e As System.ComponentModel.CancelEventArgs = Nothing, Optional ByVal isCodeFrom As Boolean = True)

    Try
      
      If argPcaRangeCodeSet Is rsetProduct Then

        '参照画面を表示
        Dim newCode As String = String.Empty
        Dim location As Point = Tools.ControlTool.GetDialogLocation(Me.tblTable.RefButton)
        Dim strTargetText As String = String.Empty
        Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms

        If isCodeFrom Then
          strTargetText = argPcaRangeCodeSet.FromCodeText
        Else
          strTargetText = argPcaRangeCodeSet.ToCodeText
        End If

        newCode = MasterDialog.ShowReferSmsDialog(strTargetText, location)

        beMasterSms = MasterDialog.FindBEMasterSms(newCode)

        If beMasterSms.SyohinCode.Length > 0 Then

          If isCodeFrom Then
            argPcaRangeCodeSet.FromCodeText = beMasterSms.SyohinCode
          Else
            argPcaRangeCodeSet.ToCodeText = beMasterSms.SyohinCode
          End If

          Me.SelectNextControl(argPcaRangeCodeSet, True, True, False, False)
          Return
        Else
          If e IsNot Nothing Then e.Cancel = True
          argPcaRangeCodeSet.Focus()
          Return
        End If
        

      Else
        '倉庫コード

        '参照画面を表示
        Dim newCode As String = String.Empty
        Dim location As Point = Tools.ControlTool.GetDialogLocation(Me.tblTable.RefButton)
        Dim strTargetText As String = String.Empty
        Dim beMasterSoko As PCA.TSC.Kon.BusinessEntity.BEMasterSoko

        If isCodeFrom Then
          strTargetText = argPcaRangeCodeSet.FromCodeText
        Else
          strTargetText = argPcaRangeCodeSet.ToCodeText
        End If

        newCode = MasterDialog.ShowReferSokoDialog(strTargetText, location)

        beMasterSoko = MasterDialog.FindBEMasterSoko(newCode)

        If beMasterSoko.SokoCode.Length > 0 Then
          If isCodeFrom Then
            argPcaRangeCodeSet.FromCodeText = beMasterSoko.SokoCode
          Else
            argPcaRangeCodeSet.ToCodeText = beMasterSoko.SokoCode
          End If

          Me.SelectNextControl(argPcaRangeCodeSet, True, True, False, False)
          Return
        Else
          If e IsNot Nothing Then e.Cancel = True
          argPcaRangeCodeSet.Focus()
          Return
        End If

        
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>Get and display the transfer lot list </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ShowTransLotList() As Boolean
    Dim isSuccess As Boolean = True
    Dim reader As ICustomDataReader
    Dim cmdselectCommand As ICustomCommand
    Dim intRow As Integer = 0
    Dim intLpc As Integer = 0

    Dim strLotDetails() As String

    strLotDetails = {ColumnName.LotName1, ColumnName.LotName2, ColumnName.LotName3, ColumnName.LotName4, ColumnName.LotName5 _
                    , ColumnName.LotName6, ColumnName.LotName7, ColumnName.LotName8, ColumnName.LotName9, ColumnName.LotName10}

    Try

      ExchangeCodes(rsetProduct)
      ExchangeCodes(rsetFromWarehouse)
      ExchangeCodes(rsetToWarehouse)

      cmdselectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_TransLotList")

      tblTable.Focus()

      cmdselectCommand.Parameters("@moveDateStart").SetValue(rdtMoveDate.FromIntDate.SerializeTarget)
      cmdselectCommand.Parameters("@moveDateEnd").SetValue(rdtMoveDate.ToIntDate.SerializeTarget)
      m_intCondDateFrom = rdtMoveDate.FromIntDate
      m_intCondDateTo = rdtMoveDate.ToIntDate

      cmdselectCommand.Parameters("@productCodeStart").SetValue(rsetProduct.FromCodeText.Trim)
      cmdselectCommand.Parameters("@productCodeEnd").SetValue(rsetProduct.ToCodeText.Trim)

      cmdselectCommand.Parameters("@warehouseFromCodeStart").SetValue(rsetFromWarehouse.FromCodeText.Trim)
      cmdselectCommand.Parameters("@warehouseFromCodeEnd").SetValue(rsetFromWarehouse.ToCodeText.Trim)

      cmdselectCommand.Parameters("@warehouseToCodeStart").SetValue(rsetToWarehouse.FromCodeText.Trim)
      cmdselectCommand.Parameters("@warehouseToCodeEnd").SetValue(rsetToWarehouse.ToCodeText.Trim)

      reader = cmdselectCommand.ExecuteReader

      tblTable.DataRowCount = 99
      tblTable.BodyRowCount = 99
      tblTable.ClearCellValues()
      While reader.Read()
        With tblTable

          .SetCellValue(intRow, ColumnName.MoveDate, Format(CInt(reader.GetValue("MoveDate").ToString), "0000/00/00"))
          .SetCellValue(intRow, ColumnName.ArrivalDate, Format(CInt(reader.GetValue("ArrivalDate").ToString), "0000/00/00"))

          .SetCellValue(intRow, ColumnName.SlipNo, reader.GetValue("SlipNo").ToString.Trim)
          .SetCellValue(intRow, ColumnName.ProductCode, reader.GetValue("ProductCode").ToString.Trim)
          .SetCellValue(intRow, ColumnName.ProductName, reader.GetValue("ProductName").ToString.Trim)
          .SetCellValue(intRow, ColumnName.WarehouseFromCode, reader.GetValue("WarehouseFromCode").ToString.Trim)
          .SetCellValue(intRow, ColumnName.WarehouseFromName, reader.GetValue("WarehouseFromName").ToString.Trim)
          .SetCellValue(intRow, ColumnName.WarehouseToCode, reader.GetValue("WarehouseToCode").ToString.Trim)
          .SetCellValue(intRow, ColumnName.WarehouseToName, reader.GetValue("WarehouseToName").ToString.Trim)
          .SetCellValue(intRow, ColumnName.Number, CInt(reader.GetValue("Honsu")).ToString("#,0.####"))
          .SetCellValue(intRow, ColumnName.Quantity, Val(reader.GetValue("Quantity")).ToString("#,0.####"))
          .SetCellValue(intRow, ColumnName.Unit, reader.GetValue("Unit").ToString.Trim)
          .SetCellValue(intRow, ColumnName.LotNo, reader.GetValue("LotNo").ToString.Trim)

          If m_SL_LMBClass.sl_lmb_sdlflg = 1 And Not reader.GetValue("SdlDate") Is Nothing AndAlso CInt(reader.GetValue("SdlDate")) > 0 Then
            .SetCellValue(intRow, ColumnName.SdlDate, Format(CInt(reader.GetValue("SdlDate").ToString), "0000/00/00"))
          Else
            .SetCellValue(intRow, ColumnName.SdlDate, String.Empty)
          End If

          If m_SL_LMBClass.sl_lmb_ubdflg = 1 And Not reader.GetValue("UbDate") Is Nothing AndAlso CInt(reader.GetValue("UbDate")) > 0 Then
            .SetCellValue(intRow, ColumnName.UbdDate, Format(CInt(reader.GetValue("UbDate").ToString), "0000/00/00"))
          Else
            .SetCellValue(intRow, ColumnName.UbdDate, String.Empty)
          End If

          If m_SL_LMBClass.sl_lmb_bbdflg = 1 And Not reader.GetValue("BbDate") Is Nothing AndAlso CInt(reader.GetValue("BbDate")) > 0 Then
            .SetCellValue(intRow, ColumnName.BbDate, Format(CInt(reader.GetValue("BbDate").ToString), "0000/00/00"))
          Else
            .SetCellValue(intRow, ColumnName.BbDate, String.Empty)
          End If

          If m_SL_LMBClass.sl_lmb_expflg = 1 And Not reader.GetValue("ExpDate") Is Nothing AndAlso CInt(reader.GetValue("ExpDate")) > 0 Then
            .SetCellValue(intRow, ColumnName.ExpDate, Format(CInt(reader.GetValue("ExpDate").ToString), "0000/00/00"))
          Else
            .SetCellValue(intRow, ColumnName.ExpDate, String.Empty)
          End If

          For intLpc = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
            .SetCellValue(intRow, strLotDetails(intLpc), reader.GetValue("LotDetail" & intLpc + 1).ToString.Trim)
          Next
          .SetCellValue(intRow, ColumnName.MoveWarehouseID, reader.GetValue("MoveID").ToString.Trim)
          .SetCellValue(intRow, ColumnName.SlipNo2, reader.GetValue("SlipNo2").ToString.Trim)
          .SetCellValue(intRow, ColumnName.Summary, reader.GetValue("Summary").ToString.Trim)

          intRow = intRow + 1
        End With

      End While

      If intRow = 0 Then
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, PG_NAME, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSearch.CommandId)

        ChangeEnabledControls(False)
        rdtMoveDate.Focus()
      Else
        ChangeEnabledControls(True)
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False

    Finally
      tblTable.DataRowCount = intRow
      tblTable.BodyRowCount = intRow
    End Try

    Return isSuccess
  End Function

  ''' <summary>Insert the print data to the DataTable</summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EditPrintData(ByRef argEditedData As DataTable) As Boolean

    Dim editData As New DataSet1.DataTable1DataTable
    Dim editRow As DataRow
    Dim intLpc As Integer = 0
    Dim intDigit As Integer = 0

    Try
      While tblTable.GetCellValue(intLpc, ColumnName.MoveWarehouseID) IsNot Nothing AndAlso _
            tblTable.GetCellValue(intLpc, ColumnName.MoveWarehouseID).ToString.Trim.Length > 0

        intDigit = System.Math.Abs(CDec(tblTable.GetCellValue(intLpc, ColumnName.Quantity)) - CInt(tblTable.GetCellValue(intLpc, ColumnName.Quantity))).ToString.Length - 2
        If intDigit < 0 Then intDigit = 0
        With tblTable
          editRow = editData.NewRow
          editRow.Item("ConditionMoveDate") = rdtMoveDate.RangeGroupBoxText & " : " & Format(m_intCondDateFrom, "0000/00/00") & " ～ " & Format(m_intCondDateTo, "0000/00/00")
          editRow.Item("MoveDate") = .GetCellValue(intLpc, ColumnName.MoveDate)
          editRow.Item("ArrivalDate") = .GetCellValue(intLpc, ColumnName.ArrivalDate)
          editRow.Item("SlipNo") = .GetCellValue(intLpc, ColumnName.SlipNo)
          editRow.Item("SlipNo2") = .GetCellValue(intLpc, ColumnName.SlipNo2)
          editRow.Item("ProductCode") = .GetCellValue(intLpc, ColumnName.ProductCode)
          editRow.Item("ProductName") = .GetCellValue(intLpc, ColumnName.ProductName)
          editRow.Item("WarehouseFromCode") = .GetCellValue(intLpc, ColumnName.WarehouseFromCode)
          editRow.Item("WarehouseFromName") = .GetCellValue(intLpc, ColumnName.WarehouseFromName)
          editRow.Item("WarehouseToCode") = .GetCellValue(intLpc, ColumnName.WarehouseToCode)
          editRow.Item("WarehouseToName") = .GetCellValue(intLpc, ColumnName.WarehouseToName)
          editRow.Item("Number") = .GetCellValue(intLpc, ColumnName.Number)
          editRow.Item("Quantity") = tblTable.GetCellValue(intLpc, ColumnName.Quantity)
          editRow.Item("Unit") = .GetCellValue(intLpc, ColumnName.Unit)
          editRow.Item("LotNo") = .GetCellValue(intLpc, ColumnName.LotNo)
          editRow.Item("Summary") = .GetCellValue(intLpc, ColumnName.Summary)

          editRow.Item("Digit") = intDigit

          editData.Rows.Add(editRow)
        End With
        intLpc += 1
      End While

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
    argEditedData = editData
    Return True
  End Function

  ''' <summary>Print</summary>
  ''' <returns>Success：True Failure：False</returns>
  ''' <param name="isPreview">True:Preview</param>
  ''' <remarks></remarks>
  Private Function PrintData(Optional ByVal isPreview As Boolean = False) As Boolean

    Try
      Dim ReportName As String = RPT_FILENAME
      Dim ReportTitle As String = PG_NAME
      Dim rpt As New Sunloft.Windows.Forms.SLCrystalReport
      'Dim PriSet As New PrinterSettings()
      'Dim PgSet As New PageSettings()
      Dim RpParam As New Dictionary(Of String, Object)
      Dim editedDataTable As DataTable = Nothing


      rpt.ReportFile = IO.Path.GetDirectoryName(Application.ExecutablePath) & "\" & ReportName
      EditPrintData(editedDataTable)
      rpt.DataSource = editedDataTable
      ' 出力開始
      If isPreview Then
        rpt.Preview(ReportTitle, True)
      Else
        rpt.PrintToPrinter()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True
  End Function

  ''' <summary>The output of the display contents</summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function OutPutCSV() As Boolean

    Dim strSaveFileName As String = String.Empty
    Dim strColumns() As String
    Dim strLotDetails() As String
    Dim sbCsv As New System.Text.StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        .OverwritePrompt = True
        .FileName = PG_NAME

        Dim res As System.Windows.Forms.DialogResult = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False

        strSaveFileName = .FileName

      End With


      strLotDetails = {ColumnName.LotName1, ColumnName.LotName2, ColumnName.LotName3, ColumnName.LotName4, ColumnName.LotName5 _
                      , ColumnName.LotName6, ColumnName.LotName7, ColumnName.LotName8, ColumnName.LotName9, ColumnName.LotName10}

      strColumns = {ColumnName.MoveDate, ColumnName.ArrivalDate, ColumnName.SlipNo, ColumnName.ProductCode, ColumnName.ProductName _
                   , ColumnName.WarehouseFromCode, ColumnName.WarehouseFromName, ColumnName.WarehouseToCode, ColumnName.WarehouseToName _
                   , ColumnName.Number, ColumnName.Quantity, ColumnName.Unit, ColumnName.LotNo}

      If m_SL_LMBClass.sl_lmb_sdlflg = 1 Then ReDim Preserve strColumns(strColumns.Length) : strColumns(strColumns.Length - 1) = ColumnName.SdlDate
      If m_SL_LMBClass.sl_lmb_ubdflg = 1 Then ReDim Preserve strColumns(strColumns.Length) : strColumns(strColumns.Length - 1) = ColumnName.UbdDate
      If m_SL_LMBClass.sl_lmb_bbdflg = 1 Then ReDim Preserve strColumns(strColumns.Length) : strColumns(strColumns.Length - 1) = ColumnName.BbDate
      If m_SL_LMBClass.sl_lmb_expflg = 1 Then ReDim Preserve strColumns(strColumns.Length) : strColumns(strColumns.Length - 1) = ColumnName.ExpDate

      For intLpc = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        ReDim Preserve strColumns(strColumns.Length)
        strColumns(strColumns.Length - 1) = strLotDetails(intLpc)
      Next

      Sunloft.Common.SLCmnFunction.OutPutCSV(strColumns, tblTable, strSaveFileName)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function

  ''' <summary>Replacement Search Codes</summary>
  ''' <param name="argPcaRangeCodeSet"></param>
  ''' <remarks></remarks>
  Private Sub ExchangeCodes(ByVal argPcaRangeCodeSet As PcaRangeCodeSet)
    Dim strTempString As String = String.Empty

    Try

      If argPcaRangeCodeSet.FromCodeText < argPcaRangeCodeSet.ToCodeText Then Return

      strTempString = argPcaRangeCodeSet.FromCodeText
      argPcaRangeCodeSet.FromCodeText = argPcaRangeCodeSet.ToCodeText
      argPcaRangeCodeSet.ToCodeText = strTempString

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>Change Control's Enabled Property</summary>
  Private Sub ChangeEnabledControls(ByVal isEnabled As Boolean)
    PcaFunctionCommandPrint.Enabled = isEnabled
    PcaFunctionCommandPreview.Enabled = isEnabled
    PcaFunctionCommandOutPut.Enabled = isEnabled
    ToolStripButtonPrint.Enabled = isEnabled
    ToolStripButtonPreview.Enabled = isEnabled
    ToolStripButtonOutPut.Enabled = isEnabled
    ToolStripMenuItemPrint.Enabled = isEnabled
    ToolStripMenuItemPreview.Enabled = isEnabled
    ToolStripMenuItemOutPut.Enabled = isEnabled
  End Sub

#End Region

#Region "Finalize"
  Private Sub LogOffClose()
    Me.Close()
  End Sub

  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not appClass.isAttach Then
      connector.LogOffSystem()
    End If
  End Sub
#End Region
End Class

''' <summary>Column Name Class</summary>
''' <remarks></remarks>
''' 
Friend Class ColumnName

  Public Const MoveDate As String = "移動日"
  Public Const ArrivalDate As String = "着日"
  Public Const SlipNo As String = "伝票番号"
  Public Const SlipNo2 As String = "伝票番号2"
  Public Const ProductCode As String = "商品コード"
  Public Const ProductName As String = "商品名"
  Public Const WarehouseFromCode As String = "移動元倉庫コード"
  Public Const WarehouseFromName As String = "移動元倉庫名"
  Public Const WarehouseToCode As String = "移動先倉庫コード"
  Public Const WarehouseToName As String = "移動先倉庫名"
  Public Const Number As String = "本数"
  Public Const Quantity As String = "数量"
  Public Shared Property Unit As String = "単位"
  Public Const LotNo As String = "ロットNo"
  Public Const SdlDate As String = "出荷期限"
  Public Const UbdDate As String = "使用期限"
  Public Const BbDate As String = "賞味期限"
  Public Const ExpDate As String = "消費期限"
  Public Const LotName1 As String = "ロット詳細名1"
  Public Const LotName2 As String = "ロット詳細名2"
  Public Const LotName3 As String = "ロット詳細名3"
  Public Const LotName4 As String = "ロット詳細名4"
  Public Const LotName5 As String = "ロット詳細名5"
  Public Const LotName6 As String = "ロット詳細名6"
  Public Const LotName7 As String = "ロット詳細名7"
  Public Const LotName8 As String = "ロット詳細名8"
  Public Const LotName9 As String = "ロット詳細名9"
  Public Const LotName10 As String = "ロット詳細名10"
  Public Const MoveWarehouseID As String = "倉庫移動ID"
  Public Const Summary As String = "摘要"
End Class

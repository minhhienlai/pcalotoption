﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransLotList
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransLotList))
    Me.ServiceController1 = New System.ServiceProcess.ServiceController()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.ToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPreview = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemOutPut = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRefer = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContents = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPrint = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPreview = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonOutPut = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSearch = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrint = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPreview = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandOutPut = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemPrint = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPreview = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemOutPut = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.tblTable = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.rsetToWarehouse = New PCA.Controls.PcaRangeCodeSet()
    Me.rdtMoveDate = New PCA.Controls.PcaRangeDate()
    Me.rsetFromWarehouse = New PCA.Controls.PcaRangeCodeSet()
    Me.rsetProduct = New PCA.Controls.PcaRangeCodeSet()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'ToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemFile, Nothing)
    Me.ToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemPrint, Me.ToolStripMenuItemPreview, Me.ToolStripMenuItemOutPut, Me.ToolStripMenuItemClose})
    Me.ToolStripMenuItemFile.Name = "ToolStripMenuItemFile"
    Me.ToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.ToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPrint, Me.PcaCommandItemPrint)
    Me.ToolStripMenuItemPrint.Name = "ToolStripMenuItemPrint"
    Me.ToolStripMenuItemPrint.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPrint.Text = "印刷(&P)"
    '
    'ToolStripMenuItemPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPreview, Me.PcaCommandItemPreview)
    Me.ToolStripMenuItemPreview.Name = "ToolStripMenuItemPreview"
    Me.ToolStripMenuItemPreview.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPreview.Text = "印刷プレビュー(&V)"
    '
    'ToolStripMenuItemOutPut
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemOutPut, Me.PcaCommandItemOutPut)
    Me.ToolStripMenuItemOutPut.Name = "ToolStripMenuItemOutPut"
    Me.ToolStripMenuItemOutPut.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemOutPut.Text = "出力(&O)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSearch, Me.ToolStripMenuItemRefer})
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSearch.Text = "再表示(&R)"
    '
    'ToolStripMenuItemRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRefer, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemRefer.Name = "ToolStripMenuItemRefer"
    Me.ToolStripMenuItemRefer.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemRefer.Text = "参照(&U)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContents})
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContents
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContents, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContents.Name = "ToolStripMenuItemContents"
    Me.ToolStripMenuItemContents.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContents.Text = "目次(&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonPrint, Me.ToolStripButtonPreview, Me.ToolStripButtonOutPut, Me.ToolStripButtonSearch, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1008, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Image = Global.Pro04040.My.Resources.Resources._Exit
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPrint, Me.PcaCommandItemPrint)
    Me.ToolStripButtonPrint.Image = CType(resources.GetObject("ToolStripButtonPrint.Image"), System.Drawing.Image)
    Me.ToolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPrint.Name = "ToolStripButtonPrint"
    Me.ToolStripButtonPrint.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonPrint.Text = "印刷"
    Me.ToolStripButtonPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPreview, Me.PcaCommandItemPreview)
    Me.ToolStripButtonPreview.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPreview.Name = "ToolStripButtonPreview"
    Me.ToolStripButtonPreview.Size = New System.Drawing.Size(81, 34)
    Me.ToolStripButtonPreview.Text = "プレビュー"
    Me.ToolStripButtonPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonOutPut
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonOutPut, Me.PcaCommandItemOutPut)
    Me.ToolStripButtonOutPut.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonOutPut.Name = "ToolStripButtonOutPut"
    Me.ToolStripButtonOutPut.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonOutPut.Text = "出力"
    Me.ToolStripButtonOutPut.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSearch, Me.PcaCommandItemSearch)
    Me.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSearch.Name = "ToolStripButtonSearch"
    Me.ToolStripButtonSearch.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonSearch.Text = "再表示"
    Me.ToolStripButtonSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"), System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 708)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1008, 22)
    Me.StatusStrip1.TabIndex = 25
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandSearch, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandPrint, Me.PcaFunctionCommandPreview, Me.PcaFunctionCommandOutPut, Me.PcaFunctionCommandClose})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 680)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1008, 28)
    Me.PcaFunctionBar1.TabIndex = 44
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    Me.PcaFunctionCommandHelp.ToolTipText = "ヘルプを表示します。"
    '
    'PcaFunctionCommandSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSearch, Me.PcaCommandItemSearch)
    Me.PcaFunctionCommandSearch.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandSearch.Tag = Nothing
    Me.PcaFunctionCommandSearch.Text = "再表示"
    Me.PcaFunctionCommandSearch.ToolTipText = "条件を元に検索を行います。"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    Me.PcaFunctionCommandRefer.ToolTipText = "各マスタを参照します。"
    '
    'PcaFunctionCommandPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrint, Me.PcaCommandItemPrint)
    Me.PcaFunctionCommandPrint.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPrint.Tag = Nothing
    Me.PcaFunctionCommandPrint.Text = "印刷"
    Me.PcaFunctionCommandPrint.ToolTipText = "印刷を行います。"
    '
    'PcaFunctionCommandPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPreview, Me.PcaCommandItemPreview)
    Me.PcaFunctionCommandPreview.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPreview.ModifierKeys = PCA.Controls.FunctionModifierKeys.Shift
    Me.PcaFunctionCommandPreview.Tag = Nothing
    Me.PcaFunctionCommandPreview.Text = "プレビュー"
    Me.PcaFunctionCommandPreview.ToolTipText = "プレビューを行います。"
    '
    'PcaFunctionCommandOutPut
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandOutPut, Me.PcaCommandItemOutPut)
    Me.PcaFunctionCommandOutPut.FunctionKey = PCA.Controls.FunctionKey.F10
    Me.PcaFunctionCommandOutPut.Tag = Nothing
    Me.PcaFunctionCommandOutPut.Text = "出力"
    Me.PcaFunctionCommandOutPut.ToolTipText = "CSV出力を行います。"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    Me.PcaFunctionCommandClose.ToolTipText = "プログラムを終了します。"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemSearch, Me.PcaCommandItemRefer, Me.PcaCommandItemPrint, Me.PcaCommandItemPreview, Me.PcaCommandItemOutPut, Me.PcaCommandItemClose})
    Me.PcaCommandManager1.Parent = Me
    '
    'PcaCommandItemPrint
    '
    Me.PcaCommandItemPrint.CommandId = 9
    Me.PcaCommandItemPrint.CommandName = "Print"
    '
    'PcaCommandItemPreview
    '
    Me.PcaCommandItemPreview.CommandId = 9
    Me.PcaCommandItemPreview.CommandName = "Preview"
    '
    'PcaCommandItemOutPut
    '
    Me.PcaCommandItemOutPut.CommandId = 10
    Me.PcaCommandItemOutPut.CommandName = "OutPut"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    Me.PcaCommandItemClose.CommandName = "Close"
    '
    'PcaCommandItemSearch
    '
    Me.PcaCommandItemSearch.CommandId = 5
    Me.PcaCommandItemSearch.CommandName = "Search"
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandId = 8
    Me.PcaCommandItemRefer.CommandName = "Refer"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    Me.PcaCommandItemHelp.CommandName = "Help"
    '
    'tblTable
    '
    Me.tblTable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tblTable.BEKihonJoho = CType(resources.GetObject("tblTable.BEKihonJoho"), PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblTable.ContextMenu = Nothing
    Me.tblTable.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.tblTable.HeadContextMenu = Nothing
    Me.tblTable.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblTable.Location = New System.Drawing.Point(23, 133)
    Me.tblTable.Name = "tblTable"
    Me.tblTable.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblTable.Size = New System.Drawing.Size(958, 524)
    Me.tblTable.TabIndex = 6
    '
    'rsetToWarehouse
    '
    Me.rsetToWarehouse.AutoTopMargin = False
    Me.rsetToWarehouse.ClientProcess = Nothing
    Me.rsetToWarehouse.FillZeroFlag = True
    Me.rsetToWarehouse.LimitLength = 4
    Me.rsetToWarehouse.Location = New System.Drawing.Point(687, 99)
    Me.rsetToWarehouse.MaxCodeLength = 5
    Me.rsetToWarehouse.MaxLabelLength = 15
    Me.rsetToWarehouse.MaxNameLength = 0
    Me.rsetToWarehouse.Name = "rsetToWarehouse"
    Me.rsetToWarehouse.RangeGroupBoxText = "移動先倉庫"
    Me.rsetToWarehouse.RangeGroupBoxVisible = False
    Me.rsetToWarehouse.Size = New System.Drawing.Size(252, 22)
    Me.rsetToWarehouse.TabIndex = 5
    Me.rsetToWarehouse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'rdtMoveDate
    '
    Me.rdtMoveDate.AutoTopMargin = False
    Me.rdtMoveDate.ClientProcess = Nothing
    Me.rdtMoveDate.DisplaySlash = True
    Me.rdtMoveDate.Holidays = Nothing
    Me.rdtMoveDate.Location = New System.Drawing.Point(23, 77)
    Me.rdtMoveDate.MaxLabelLength = 15
    Me.rdtMoveDate.Name = "rdtMoveDate"
    Me.rdtMoveDate.RangeGroupBoxText = "移動日"
    Me.rdtMoveDate.RangeGroupBoxVisible = False
    Me.rdtMoveDate.Size = New System.Drawing.Size(338, 22)
    Me.rdtMoveDate.TabIndex = 2
    Me.rdtMoveDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'rsetFromWarehouse
    '
    Me.rsetFromWarehouse.AutoTopMargin = False
    Me.rsetFromWarehouse.ClientProcess = Nothing
    Me.rsetFromWarehouse.FillZeroFlag = True
    Me.rsetFromWarehouse.LimitLength = 4
    Me.rsetFromWarehouse.Location = New System.Drawing.Point(361, 99)
    Me.rsetFromWarehouse.MaxCodeLength = 5
    Me.rsetFromWarehouse.MaxLabelLength = 15
    Me.rsetFromWarehouse.MaxNameLength = 0
    Me.rsetFromWarehouse.Name = "rsetFromWarehouse"
    Me.rsetFromWarehouse.RangeGroupBoxText = "移動元倉庫"
    Me.rsetFromWarehouse.RangeGroupBoxVisible = False
    Me.rsetFromWarehouse.Size = New System.Drawing.Size(252, 22)
    Me.rsetFromWarehouse.TabIndex = 4
    Me.rsetFromWarehouse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'rsetProduct
    '
    Me.rsetProduct.AutoTopMargin = False
    Me.rsetProduct.ClientProcess = Nothing
    Me.rsetProduct.FillZeroFlag = True
    Me.rsetProduct.LimitLength = 4
    Me.rsetProduct.Location = New System.Drawing.Point(23, 99)
    Me.rsetProduct.MaxCodeLength = 5
    Me.rsetProduct.MaxLabelLength = 15
    Me.rsetProduct.MaxNameLength = 0
    Me.rsetProduct.Name = "rsetProduct"
    Me.rsetProduct.RangeGroupBoxText = "商品"
    Me.rsetProduct.RangeGroupBoxVisible = False
    Me.rsetProduct.Size = New System.Drawing.Size(252, 22)
    Me.rsetProduct.TabIndex = 3
    Me.rsetProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'frmTransLotList
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1008, 730)
    Me.Controls.Add(Me.rsetProduct)
    Me.Controls.Add(Me.rsetFromWarehouse)
    Me.Controls.Add(Me.rdtMoveDate)
    Me.Controls.Add(Me.rsetToWarehouse)
    Me.Controls.Add(Me.tblTable)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = true
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1024, 768)
    Me.Name = "frmTransLotList"
    Me.Text = "振替ロット一覧表"
    Me.MenuStrip1.ResumeLayout(false)
    Me.MenuStrip1.PerformLayout
    Me.ToolStrip1.ResumeLayout(false)
    Me.ToolStrip1.PerformLayout
    Me.ResumeLayout(false)
    Me.PerformLayout

End Sub
  Friend WithEvents ServiceController1 As System.ServiceProcess.ServiceController
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents ToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPrint As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemRefer As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContents As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPreview As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPrint As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonOutPut As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonSearch As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPrint As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPreview As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemOutPut As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandPrint As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents tblTable As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolStripMenuItemPreview As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemOutPut As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents PcaFunctionCommandPreview As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandOutPut As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents rsetProduct As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents rsetFromWarehouse As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents rdtMoveDate As PCA.Controls.PcaRangeDate
  Friend WithEvents rsetToWarehouse As PCA.Controls.PcaRangeCodeSet

End Class

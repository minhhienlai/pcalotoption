﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmList
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmList))
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.ToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemExport = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ロット伝票起動LToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonExport = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSearch = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.rdt1Date = New PCA.Controls.PcaRangeDate()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandLotSlipStart = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandExport = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemExport = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemLotSlipStart = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
    Me.tblResult1 = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.TabControl1 = New System.Windows.Forms.TabControl()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.TabPage2 = New System.Windows.Forms.TabPage()
    Me.rdt2SalesDate = New PCA.Controls.PcaRangeDate()
    Me.tblResult2 = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.TabPage3 = New System.Windows.Forms.TabPage()
    Me.rdt3PurchaseDate = New PCA.Controls.PcaRangeDate()
    Me.tblResult3 = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.MenuStrip1.SuspendLayout
    Me.ToolStrip1.SuspendLayout
    Me.TabControl1.SuspendLayout
    Me.TabPage1.SuspendLayout
    Me.TabPage2.SuspendLayout
    Me.TabPage3.SuspendLayout
    Me.SuspendLayout
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(1176, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'ToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemFile, Nothing)
    Me.ToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemExport, Me.ToolStripMenuItemClose})
    Me.ToolStripMenuItemFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.ToolStripMenuItemFile.Name = "ToolStripMenuItemFile"
    Me.ToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.ToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemExport, Me.PcaCommandItemExport)
    Me.ToolStripMenuItemExport.Name = "ToolStripMenuItemExport"
    Me.ToolStripMenuItemExport.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemExport.Text = "出力(&O)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSearch, Me.ロット伝票起動LToolStripMenuItem})
    Me.ToolStripMenuItemEdit.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(158, 22)
    Me.ToolStripMenuItemSearch.Text = "再表示(&R)"
    '
    'ロット伝票起動LToolStripMenuItem
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ロット伝票起動LToolStripMenuItem, Me.PcaCommandItemLotSlipStart)
    Me.ロット伝票起動LToolStripMenuItem.Name = "ロット伝票起動LToolStripMenuItem"
    Me.ロット伝票起動LToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
    Me.ロット伝票起動LToolStripMenuItem.Text = "伝票起動（&L)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(130, 22)
    Me.ToolStripMenuItemContent.Text = "目次（&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonExport, Me.ToolStripButtonSearch, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1176, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"),System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonExport, Me.PcaCommandItemExport)
    Me.ToolStripButtonExport.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.ToolStripButtonExport.Image = CType(resources.GetObject("ToolStripButtonExport.Image"),System.Drawing.Image)
    Me.ToolStripButtonExport.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonExport.Name = "ToolStripButtonExport"
    Me.ToolStripButtonExport.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonExport.Text = "出力"
    Me.ToolStripButtonExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSearch, Me.PcaCommandItemSearch)
    Me.ToolStripButtonSearch.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.ToolStripButtonSearch.Image = Global.Pro06010.My.Resources.Resources.Search
    Me.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSearch.Name = "ToolStripButtonSearch"
    Me.ToolStripButtonSearch.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonSearch.Text = "再表示"
    Me.ToolStripButtonSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"),System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'rdt1Date
    '
    Me.rdt1Date.AutoTopMargin = false
    Me.rdt1Date.ClientProcess = Nothing
    Me.rdt1Date.DayLabel = ""
    Me.rdt1Date.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.rdt1Date.Holidays = Nothing
    Me.rdt1Date.Location = New System.Drawing.Point(19, 15)
    Me.rdt1Date.MaxLabelLength = 10
    Me.rdt1Date.MonthLabel = "/"
    Me.rdt1Date.Name = "rdt1Date"
    Me.rdt1Date.RangeBorder = true
    Me.rdt1Date.RangeGroupBoxText = "日付"
    Me.rdt1Date.RangeGroupBoxVisible = false
    Me.rdt1Date.Size = New System.Drawing.Size(331, 22)
    Me.rdt1Date.TabIndex = 2
    Me.rdt1Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdt1Date.YearLabel = "/"
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 769)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1176, 22)
    Me.StatusStrip1.TabIndex = 4
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandSearch, Me.PcaFunctionCommandLotSlipStart, Me.PcaFunctionCommandExport, Me.PcaFunctionCommandClose})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 741)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1176, 28)
    Me.PcaFunctionBar1.TabIndex = 5
    Me.PcaFunctionBar1.TabStop = false
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    '
    'PcaFunctionCommandSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSearch, Me.PcaCommandItemSearch)
    Me.PcaFunctionCommandSearch.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandSearch.Tag = Nothing
    Me.PcaFunctionCommandSearch.Text = "再表示"
    '
    'PcaFunctionCommandLotSlipStart
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandLotSlipStart, Me.PcaCommandItemLotSlipStart)
    Me.PcaFunctionCommandLotSlipStart.FunctionKey = PCA.Controls.FunctionKey.F7
    Me.PcaFunctionCommandLotSlipStart.Tag = Nothing
    Me.PcaFunctionCommandLotSlipStart.Text = "伝票起動"
    '
    'PcaFunctionCommandExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandExport, Me.PcaCommandItemExport)
    Me.PcaFunctionCommandExport.FunctionKey = PCA.Controls.FunctionKey.F10
    Me.PcaFunctionCommandExport.Tag = Nothing
    Me.PcaFunctionCommandExport.Text = "出力"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemSearch, Me.PcaCommandItemLotSlipStart, Me.PcaCommandItemExport, Me.PcaCommandItemClose})
    '
    'PcaCommandItemExport
    '
    Me.PcaCommandItemExport.CommandId = 10
    Me.PcaCommandItemExport.CommandName = "Export"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandName = "CommandClose"
    '
    'PcaCommandItemSearch
    '
    Me.PcaCommandItemSearch.CommandId = 5
    Me.PcaCommandItemSearch.CommandName = "CommandSearch"
    '
    'PcaCommandItemLotSlipStart
    '
    Me.PcaCommandItemLotSlipStart.CommandName = "CommandLotSlipStart"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandName = "CommandHelp"
    '
    'tblResult1
    '
    Me.tblResult1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
    Me.tblResult1.BEKihonJoho = CType(resources.GetObject("tblResult1.BEKihonJoho"),PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblResult1.ContextMenu = Nothing
    Me.tblResult1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.tblResult1.HeadContextMenu = Nothing
    Me.tblResult1.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblResult1.Location = New System.Drawing.Point(16, 60)
    Me.tblResult1.Name = "tblResult1"
    Me.tblResult1.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblResult1.Size = New System.Drawing.Size(1122, 566)
    Me.tblResult1.TabIndex = 6
    '
    'TabControl1
    '
    Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
    Me.TabControl1.Controls.Add(Me.TabPage1)
    Me.TabControl1.Controls.Add(Me.TabPage2)
    Me.TabControl1.Controls.Add(Me.TabPage3)
    Me.TabControl1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.TabControl1.Location = New System.Drawing.Point(12, 76)
    Me.TabControl1.Name = "TabControl1"
    Me.TabControl1.SelectedIndex = 0
    Me.TabControl1.Size = New System.Drawing.Size(1152, 659)
    Me.TabControl1.TabIndex = 7
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.rdt1Date)
    Me.TabPage1.Controls.Add(Me.tblResult1)
    Me.TabPage1.Location = New System.Drawing.Point(4, 23)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(1144, 632)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "データ警告情報"
    Me.TabPage1.UseVisualStyleBackColor = true
    '
    'TabPage2
    '
    Me.TabPage2.Controls.Add(Me.rdt2SalesDate)
    Me.TabPage2.Controls.Add(Me.tblResult2)
    Me.TabPage2.Location = New System.Drawing.Point(4, 23)
    Me.TabPage2.Name = "TabPage2"
    Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage2.Size = New System.Drawing.Size(1144, 632)
    Me.TabPage2.TabIndex = 1
    Me.TabPage2.Text = "売上引当"
    Me.TabPage2.UseVisualStyleBackColor = true
    '
    'rdt2SalesDate
    '
    Me.rdt2SalesDate.AutoTopMargin = false
    Me.rdt2SalesDate.ClientProcess = Nothing
    Me.rdt2SalesDate.DayLabel = ""
    Me.rdt2SalesDate.Holidays = Nothing
    Me.rdt2SalesDate.Location = New System.Drawing.Point(19, 15)
    Me.rdt2SalesDate.MaxLabelLength = 10
    Me.rdt2SalesDate.MonthLabel = "/"
    Me.rdt2SalesDate.Name = "rdt2SalesDate"
    Me.rdt2SalesDate.RangeBorder = true
    Me.rdt2SalesDate.RangeGroupBoxText = "売上日"
    Me.rdt2SalesDate.RangeGroupBoxVisible = false
    Me.rdt2SalesDate.Size = New System.Drawing.Size(331, 22)
    Me.rdt2SalesDate.TabIndex = 7
    Me.rdt2SalesDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdt2SalesDate.YearLabel = "/"
    '
    'tblResult2
    '
    Me.tblResult2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
    Me.tblResult2.BEKihonJoho = CType(resources.GetObject("tblResult2.BEKihonJoho"),PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblResult2.ContextMenu = Nothing
    Me.tblResult2.HeadContextMenu = Nothing
    Me.tblResult2.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblResult2.Location = New System.Drawing.Point(19, 60)
    Me.tblResult2.Name = "tblResult2"
    Me.tblResult2.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblResult2.Size = New System.Drawing.Size(1122, 566)
    Me.tblResult2.TabIndex = 8
    '
    'TabPage3
    '
    Me.TabPage3.Controls.Add(Me.rdt3PurchaseDate)
    Me.TabPage3.Controls.Add(Me.tblResult3)
    Me.TabPage3.Location = New System.Drawing.Point(4, 23)
    Me.TabPage3.Name = "TabPage3"
    Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage3.Size = New System.Drawing.Size(1144, 632)
    Me.TabPage3.TabIndex = 2
    Me.TabPage3.Text = "仕入ロット未入力"
    Me.TabPage3.UseVisualStyleBackColor = true
    '
    'rdt3PurchaseDate
    '
    Me.rdt3PurchaseDate.AutoTopMargin = false
    Me.rdt3PurchaseDate.ClientProcess = Nothing
    Me.rdt3PurchaseDate.DayLabel = ""
    Me.rdt3PurchaseDate.Holidays = Nothing
    Me.rdt3PurchaseDate.Location = New System.Drawing.Point(19, 15)
    Me.rdt3PurchaseDate.MaxLabelLength = 10
    Me.rdt3PurchaseDate.MonthLabel = "/"
    Me.rdt3PurchaseDate.Name = "rdt3PurchaseDate"
    Me.rdt3PurchaseDate.RangeBorder = true
    Me.rdt3PurchaseDate.RangeGroupBoxText = "仕入日"
    Me.rdt3PurchaseDate.RangeGroupBoxVisible = false
    Me.rdt3PurchaseDate.Size = New System.Drawing.Size(331, 22)
    Me.rdt3PurchaseDate.TabIndex = 7
    Me.rdt3PurchaseDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdt3PurchaseDate.YearLabel = "/"
    '
    'tblResult3
    '
    Me.tblResult3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
    Me.tblResult3.BEKihonJoho = CType(resources.GetObject("tblResult3.BEKihonJoho"),PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblResult3.ContextMenu = Nothing
    Me.tblResult3.HeadContextMenu = Nothing
    Me.tblResult3.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblResult3.Location = New System.Drawing.Point(19, 60)
    Me.tblResult3.Name = "tblResult3"
    Me.tblResult3.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblResult3.Size = New System.Drawing.Size(1122, 566)
    Me.tblResult3.TabIndex = 8
    '
    'frmList
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 13!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1176, 791)
    Me.Controls.Add(Me.TabControl1)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10!)
    Me.KeyPreview = true
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1192, 829)
    Me.Name = "frmList"
    Me.Text = "未引当・警告情報一覧"
    Me.MenuStrip1.ResumeLayout(false)
    Me.MenuStrip1.PerformLayout
    Me.ToolStrip1.ResumeLayout(false)
    Me.ToolStrip1.PerformLayout
    Me.TabControl1.ResumeLayout(false)
    Me.TabPage1.ResumeLayout(false)
    Me.TabPage2.ResumeLayout(false)
    Me.TabPage3.ResumeLayout(false)
    Me.ResumeLayout(false)
    Me.PerformLayout

End Sub
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents ToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemExport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonExport As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonSearch As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemLotSlipStart As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemExport As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents rdt1Date As PCA.Controls.PcaRangeDate
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandLotSlipStart As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandExport As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents tblResult1 As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
  Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
  Friend WithEvents rdt2SalesDate As PCA.Controls.PcaRangeDate
  Friend WithEvents tblResult2 As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
  Friend WithEvents rdt3PurchaseDate As PCA.Controls.PcaRangeDate
  Friend WithEvents tblResult3 As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents ロット伝票起動LToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class

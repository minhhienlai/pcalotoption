﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports PCA.Controls
Imports PCA.TSC.Kon.Tools
Imports Sunloft.PCAIF
Imports Sunloft.Common
Imports PCA.ApplicationIntegration

Public Class frmList
  Private Const WIDTH_SLIPID As Single = 0
  'Data Warning table (Table Result 1) column width
  Private Const WIDTH_TB1_TYPE As Single = 7
  Private Const WIDTH_TB1_SLIP_NO As Single = 10
  Private Const WIDTH_TB1_SLIP_DATE As Single = 15
  Private Const WIDTH_TB1_PARTNER_CODE As Single = 7
  Private Const WIDTH_TB1_PARTNER_NAME As Single = 30
  Private Const WIDTH_TB1_PRODUCT_CODE As Single = 7
  Private Const WIDTH_TB1_PRODUCT_NAME As Single = 30
  Private Const WIDTH_TB1_WARNING_MESSAGE As Single = 75

  'UnAllocated table (Table Result 2) column width
  Private Const WIDTH_TB2_SLIP_NO As Single = 7
  Private Const WIDTH_TB2_SLIP_DATE As Single = 10
  Private Const WIDTH_TB2_PARTNER_CODE As Single = 10
  Private Const WIDTH_TB2_PARTNER_NAME As Single = 30
  Private Const WIDTH_TB2_PRODUCT_CODE As Single = 10
  Private Const WIDTH_TB2_PRODUCT_NAME As Single = 30
  Private Const WIDTH_TB2_QUANTITY As Single = 10

  'UnInputted table (Table Result 3) column width
  Private Const WIDTH_TB3_SLIP_NO As Single = 7
  Private Const WIDTH_TB3_SLIP_DATE As Single = 10
  Private Const WIDTH_TB3_PARTNER_CODE As Single = 10
  Private Const WIDTH_TB3_PARTNER_NAME As Single = 30
  Private Const WIDTH_TB3_PRODUCT_CODE As Single = 10
  Private Const WIDTH_TB3_PRODUCT_NAME As Single = 30
  Private Const WIDTH_TB3_QUANTITY As Single = 10

  'Table cell type
  Private Const NUMBER_TYPE As String = "Number"
  Private Const TEXT_TYPE As String = "String"

  'Login - Initial parameter
  Property connector As IIntegratedApplication = Nothing
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private m_SL_LMBClass As SL_LMB
  Private m_AMS1Class As AMS1
  Private m_conf As Sunloft.PcaConfig
  Private m_loginString As String

  'Slip display/Export parameter
  Private Const PROGRAM_TO_CALL As String = "\Pro03010.exe" 'Click table cell -> Display slip

  'Tab const
  Private Const DATA_REPORT_TAB As Integer = 0
  Private Const UNALLOCATED_TAB As Integer = 1
  Private Const UNINPUTTED_TAB As Integer = 2

  'Error message code
  Private Const SYKD_NULL As Integer = 1
  Private Const SYKD_CONFLICT_QUANTITY As Integer = 2
  Private Const NYKD_NULL As Integer = 3
  Private Const NYKD_CONFLICT_QUANTITY As Integer = 4

  'Result object
  Private m_ReportResult As SearchResult() = {New SearchResult}

#Region "Initialize"

  Public Sub New()
    Try
      InitializeComponent()
      m_appClass.ConnectToPCA()
      connector = m_appClass.connector

      m_loginString = m_appClass.EncryptedLoginString

      m_SL_LMBClass = New SL_LMB(connector)
      m_SL_LMBClass.ReadOnlyRow()
      m_AMS1Class = New AMS1(connector)
      m_AMS1Class.ReadAll()
      m_conf = New Sunloft.PcaConfig

      InitTable(tblResult1)
      InitTable(tblResult2)
      InitTable(tblResult3)

      PcaFunctionCommandLotSlipStart.Enabled = False
      PcaFunctionCommandExport.Enabled = False

      rdt1Date.FromDate = Today.AddMonths(-6)
      rdt2SalesDate.FromDate = Today.AddMonths(-6)
      rdt3PurchaseDate.FromDate = Today.AddMonths(-6)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitTable(tablename As TscMeisaiTable)
    Try
      With tablename
        .DataRowCount = 99

        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader(tablename)
        InitTableBody(tablename)

        .UpdateTable()

        tablename.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
        tablename.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
        tablename.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
        tablename.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitTableHeader(tablename As TscMeisaiTable)
    tablename.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
    tablename.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

    Select Case True
      Case tablename Is tblResult1
        InitHeaderColumn(tblResult1, ReportTableColumn.type, WIDTH_TB1_TYPE, ReportTableColumn.type, , StringAlignment.Center)
        InitHeaderColumn(tblResult1, ReportTableColumn.slipNo, WIDTH_TB1_SLIP_NO, ReportTableColumn.slipNo)
        InitHeaderColumn(tblResult1, ReportTableColumn.slipDate, WIDTH_TB1_SLIP_DATE, ReportTableColumn.slipDate, , StringAlignment.Center)
        InitHeaderColumn(tblResult1, ReportTableColumn.partnerCode, WIDTH_TB1_PARTNER_CODE, ReportTableColumn.Code)
        InitHeaderColumn(tblResult1, ReportTableColumn.partnerName, WIDTH_TB1_PARTNER_NAME, ReportTableColumn.partnerName)
        InitHeaderColumn(tblResult1, ReportTableColumn.productCode, WIDTH_TB1_PRODUCT_CODE, ReportTableColumn.Code)
        InitHeaderColumn(tblResult1, ReportTableColumn.productName, WIDTH_TB1_PRODUCT_NAME, ReportTableColumn.productName)
        InitHeaderColumn(tblResult1, ReportTableColumn.warningMessage, WIDTH_TB1_WARNING_MESSAGE, ReportTableColumn.warningMessage)
      Case tablename Is tblResult2
        InitHeaderColumn(tblResult2, UnAllocatedTableColumn.slipNo, WIDTH_TB2_SLIP_NO, UnAllocatedTableColumn.slipNo)
        InitHeaderColumn(tblResult2, UnAllocatedTableColumn.slipDate, WIDTH_TB2_SLIP_DATE, UnAllocatedTableColumn.slipDate, , StringAlignment.Center)
        InitHeaderColumn(tblResult2, UnAllocatedTableColumn.partnerCode, WIDTH_TB2_PARTNER_CODE, UnAllocatedTableColumn.Code)
        InitHeaderColumn(tblResult2, UnAllocatedTableColumn.partnerName, WIDTH_TB2_PARTNER_NAME, UnAllocatedTableColumn.partnerName)
        InitHeaderColumn(tblResult2, UnAllocatedTableColumn.productCode, WIDTH_TB2_PRODUCT_CODE, UnAllocatedTableColumn.Code)
        InitHeaderColumn(tblResult2, UnAllocatedTableColumn.productName, WIDTH_TB2_PRODUCT_NAME, UnAllocatedTableColumn.productName)
        InitHeaderColumn(tblResult2, UnAllocatedTableColumn.quantity, WIDTH_TB2_QUANTITY, UnAllocatedTableColumn.quantity, , StringAlignment.Far)
      Case tablename Is tblResult3
        InitHeaderColumn(tblResult3, UnInputtedTableColumn.slipNo, WIDTH_TB3_SLIP_NO, UnInputtedTableColumn.slipNo)
        InitHeaderColumn(tblResult3, UnInputtedTableColumn.slipDate, WIDTH_TB3_SLIP_DATE, UnInputtedTableColumn.slipDate, , StringAlignment.Center)
        InitHeaderColumn(tblResult3, UnInputtedTableColumn.partnerCode, WIDTH_TB3_PARTNER_CODE, UnInputtedTableColumn.Code)
        InitHeaderColumn(tblResult3, UnInputtedTableColumn.partnerName, WIDTH_TB3_PARTNER_NAME, UnInputtedTableColumn.partnerName)
        InitHeaderColumn(tblResult3, UnInputtedTableColumn.productCode, WIDTH_TB3_PRODUCT_CODE, UnInputtedTableColumn.Code)
        InitHeaderColumn(tblResult3, UnInputtedTableColumn.productName, WIDTH_TB3_PRODUCT_NAME, UnInputtedTableColumn.productName)
        InitHeaderColumn(tblResult3, UnInputtedTableColumn.quantity, WIDTH_TB3_QUANTITY, UnInputtedTableColumn.quantity, , StringAlignment.Far)

    End Select

  End Sub
  Private Sub InitHeaderColumn(tablename As TscMeisaiTable, headerName As String, size As Single, headerText As String, Optional isResize As Boolean = True, Optional alignment As System.Drawing.StringAlignment = StringAlignment.Near)
    Dim column As PcaColumn
    Dim cell As PcaCell
    column = New PcaColumn(headerName, size)
    column.CanResize = isResize
    cell = tablename.PrepareFixTextCell(headerText)
    cell.CellStyle.Alignment = alignment
    column.Cells.Add(cell)
    tablename.HeadColumns.Add(column)
  End Sub
  Private Sub InitTableBody(tablename As TscMeisaiTable)
    tablename.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
    tablename.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
    tablename.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
    tablename.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

    Select Case True

      Case tablename Is tblResult1
        InitBodyColumn(tblResult1, ReportTableColumn.type, ReportTableColumn.type, WIDTH_TB1_TYPE, TEXT_TYPE, , , StringAlignment.Center)
        InitBodyColumn(tblResult1, ReportTableColumn.slipNo, ReportTableColumn.slipNo, WIDTH_TB1_SLIP_NO, NUMBER_TYPE)
        InitBodyColumn(tblResult1, ReportTableColumn.slipDate, ReportTableColumn.slipDate, WIDTH_TB1_SLIP_DATE, TEXT_TYPE, , , StringAlignment.Center)
        InitBodyColumn(tblResult1, ReportTableColumn.partnerCode, ReportTableColumn.partnerCode, WIDTH_TB1_PARTNER_CODE, TEXT_TYPE)
        InitBodyColumn(tblResult1, ReportTableColumn.partnerName, ReportTableColumn.partnerName, WIDTH_TB1_PARTNER_NAME, TEXT_TYPE)
        InitBodyColumn(tblResult1, ReportTableColumn.productCode, ReportTableColumn.productCode, WIDTH_TB1_PRODUCT_CODE, TEXT_TYPE)
        InitBodyColumn(tblResult1, ReportTableColumn.productName, ReportTableColumn.productName, WIDTH_TB1_PRODUCT_NAME, TEXT_TYPE)
        InitBodyColumn(tblResult1, ReportTableColumn.warningMessage, ReportTableColumn.warningMessage, WIDTH_TB1_WARNING_MESSAGE, TEXT_TYPE)

        InitBodyColumn(tblResult1, ReportTableColumn.LotSlipHeaderID, ReportTableColumn.LotSlipHeaderID, WIDTH_SLIPID, NUMBER_TYPE)

      Case tablename Is tblResult2
        InitBodyColumn(tblResult2, UnAllocatedTableColumn.slipNo, UnAllocatedTableColumn.slipNo, WIDTH_TB2_SLIP_NO, NUMBER_TYPE)
        InitBodyColumn(tblResult2, UnAllocatedTableColumn.slipDate, UnAllocatedTableColumn.slipDate, WIDTH_TB2_SLIP_DATE, TEXT_TYPE, , , StringAlignment.Center)
        InitBodyColumn(tblResult2, UnAllocatedTableColumn.partnerCode, UnAllocatedTableColumn.partnerCode, WIDTH_TB2_PARTNER_CODE, TEXT_TYPE)
        InitBodyColumn(tblResult2, UnAllocatedTableColumn.partnerName, UnAllocatedTableColumn.partnerName, WIDTH_TB2_PARTNER_NAME, TEXT_TYPE)
        InitBodyColumn(tblResult2, UnAllocatedTableColumn.productCode, UnAllocatedTableColumn.productCode, WIDTH_TB2_PRODUCT_CODE, TEXT_TYPE)
        InitBodyColumn(tblResult2, UnAllocatedTableColumn.productName, UnAllocatedTableColumn.productName, WIDTH_TB2_PRODUCT_NAME, TEXT_TYPE)
        InitBodyColumn(tblResult2, UnAllocatedTableColumn.quantity, UnAllocatedTableColumn.quantity, WIDTH_TB2_QUANTITY, NUMBER_TYPE, , , StringAlignment.Far)

        InitBodyColumn(tblResult2, UnAllocatedTableColumn.LotSlipHeaderID, UnAllocatedTableColumn.LotSlipHeaderID, WIDTH_SLIPID, NUMBER_TYPE)

      Case tablename Is tblResult3
        InitBodyColumn(tblResult3, UnInputtedTableColumn.slipNo, UnInputtedTableColumn.slipNo, WIDTH_TB3_SLIP_NO, NUMBER_TYPE)
        InitBodyColumn(tblResult3, UnInputtedTableColumn.slipDate, UnInputtedTableColumn.slipDate, WIDTH_TB3_SLIP_DATE, TEXT_TYPE, , , StringAlignment.Center)
        InitBodyColumn(tblResult3, UnInputtedTableColumn.partnerCode, UnInputtedTableColumn.partnerCode, WIDTH_TB3_PARTNER_CODE, TEXT_TYPE)
        InitBodyColumn(tblResult3, UnInputtedTableColumn.partnerName, UnInputtedTableColumn.partnerName, WIDTH_TB3_PARTNER_NAME, TEXT_TYPE)
        InitBodyColumn(tblResult3, UnInputtedTableColumn.productCode, UnInputtedTableColumn.productCode, WIDTH_TB3_PRODUCT_CODE, TEXT_TYPE)
        InitBodyColumn(tblResult3, UnInputtedTableColumn.productName, UnInputtedTableColumn.productName, WIDTH_TB3_PRODUCT_NAME, TEXT_TYPE)
        InitBodyColumn(tblResult3, UnInputtedTableColumn.quantity, UnInputtedTableColumn.quantity, WIDTH_TB3_QUANTITY, NUMBER_TYPE, , , StringAlignment.Far)

        InitBodyColumn(tblResult3, UnAllocatedTableColumn.LotSlipHeaderID, UnAllocatedTableColumn.LotSlipHeaderID, WIDTH_SLIPID, NUMBER_TYPE)
    End Select

  End Sub
  Private Sub InitBodyColumn(tablename As TscMeisaiTable, columnName As String, headerName As String, size As Single, type As String, Optional editable As Boolean = False, _
                             Optional isResize As Boolean = True, Optional alignment As System.Drawing.StringAlignment = StringAlignment.Near)
    Dim column As PcaColumn
    Dim cell As PcaCell

    column = New PcaColumn(columnName, size)
    column.CanResize = isResize

    Select Case type
      Case NUMBER_TYPE
        cell = tablename.PrepareNumberCell(columnName, size, 1, 0, 0, headerName)
      Case TEXT_TYPE
        cell = tablename.PrepareTextCell(columnName, size, 1, 0, 0, headerName)
      Case Else
        cell = tablename.PrepareTextCell(columnName, size, 1, 0, 0, headerName)
    End Select

    cell.CellStyle.Alignment = alignment
    cell.EditMode = False
    column.Cells.Add(cell)
    tablename.BodyColumns.Add(column)
  End Sub
  Protected Overrides Sub OnLoad(e As System.EventArgs)
    Try
      Dim configXMLDoc As New System.Xml.XmlDocument
      MyBase.OnLoad(e)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region "PCA Command Manager"
  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Dim commandItem As PcaCommandItem = e.CommandItem
    Select Case True
      Case commandItem Is PcaCommandItemClose
        Me.Close()
      Case commandItem Is PcaCommandItemSearch
        Select Case TabControl1.SelectedIndex

          Case DATA_REPORT_TAB
            DoSearch(tblResult1, m_ReportResult)
          Case UNALLOCATED_TAB
            DoSearch(tblResult2, m_ReportResult)
          Case UNINPUTTED_TAB
            DoSearch(tblResult3, m_ReportResult)

        End Select
      Case commandItem Is PcaCommandItemLotSlipStart

      Case commandItem Is PcaCommandItemExport

        OutPutCSV()

    End Select

  End Sub
#End Region

#Region "Private Method"


  Private Function OutPutCSV() As Boolean


    Dim strSaveFileName As String = String.Empty
    Dim strColumns() As String
    Dim sbCsv As New System.Text.StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        .OverwritePrompt = True

        Dim res As System.Windows.Forms.DialogResult = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False

        strSaveFileName = .FileName

      End With

      Select Case TabControl1.SelectedIndex

        Case DATA_REPORT_TAB
          strColumns = {ReportTableColumn.type, ReportTableColumn.slipNo, ReportTableColumn.slipDate, ReportTableColumn.partnerCode, ReportTableColumn.partnerName, ReportTableColumn.productCode, ReportTableColumn.productName, ReportTableColumn.warningMessage}
          SLCmnFunction.OutPutCSV(strColumns, tblResult1, strSaveFileName)

        Case UNALLOCATED_TAB
          strColumns = {UnAllocatedTableColumn.slipNo, UnAllocatedTableColumn.slipDate, UnAllocatedTableColumn.partnerCode, UnAllocatedTableColumn.partnerName, UnAllocatedTableColumn.productCode, UnAllocatedTableColumn.productName, UnAllocatedTableColumn.quantity}
          SLCmnFunction.OutPutCSV(strColumns, tblResult2, strSaveFileName)

        Case UNINPUTTED_TAB
          strColumns = {UnInputtedTableColumn.slipNo, UnInputtedTableColumn.slipDate, UnInputtedTableColumn.partnerCode, UnInputtedTableColumn.partnerName, UnInputtedTableColumn.productCode, UnInputtedTableColumn.productName, UnInputtedTableColumn.quantity}
          SLCmnFunction.OutPutCSV(strColumns, tblResult3, strSaveFileName)

      End Select

      'Dim PosDisplay As System.Drawing.Point = System.Windows.Forms.Cursor.Position
      'Dim PosForm As System.Drawing.Point = Me.PointToClient(PosDisplay)
      SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemExport.CommandId)
      Return True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

  Private Sub DoSearch(ByVal tablename As TscMeisaiTable, ByRef result As SearchResult())

    tablename.Focus()
    tablename.ClearCellValues()

    result = GetSearchResult()
    DisplaySearchResult(result)

  End Sub

  Private Function GetSearchResult() As SearchResult()
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Dim fromDate As Integer
    Dim toDate As Integer
    Dim intCount As Integer = -1
    Dim m_result As SearchResult() = Nothing
    Try
      If Not m_result Is Nothing Then Array.Clear(m_result, 0, m_result.Length)
      Select Case TabControl1.SelectedIndex

        Case DATA_REPORT_TAB

          fromDate = rdt1Date.FromIntDate
          toDate = rdt1Date.ToIntDate

          selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_Report_Pro06010")
          selectCommand.Parameters("@fromDate").SetValue(fromDate)
          selectCommand.Parameters("@toDate").SetValue(toDate)
          reader = selectCommand.ExecuteReader

          While reader.Read() = True
            If m_result Is Nothing Then m_result = {New SearchResult}
            intCount = intCount + 1
            ReDim Preserve m_result(intCount)
            m_result(intCount) = New SearchResult
            m_result(intCount).type = reader.GetValue("datatype").ToString().TrimEnd
            m_result(intCount).slipNo = CInt(reader.GetValue("slipNo").ToString())
            m_result(intCount).slipDate = Date.ParseExact(reader.GetValue("slipDate").ToString(), "yyyyMMdd", provider:=Globalization.CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
            m_result(intCount).partnerCode = reader.GetValue("partnerCode").ToString().TrimEnd
            m_result(intCount).partnerName = reader.GetValue("cms_mei1").ToString().TrimEnd & " " & reader.GetValue("cms_mei2").ToString().TrimEnd
            m_result(intCount).productCode = reader.GetValue("productCode").ToString().TrimEnd
            m_result(intCount).productName = reader.GetValue("sms_mei").ToString() & RTrim(" " + reader.GetValue("sms_kikaku").ToString()) + RTrim(" " + reader.GetValue("sms_color").ToString()) + RTrim(" " + reader.GetValue("sms_size").ToString())
            If IsDBNull(reader.GetValue("PCA_Header_ID")) Then
              m_result(intCount).warningMessage = ErrorMessage(NYKD_NULL, m_result(intCount).slipNo)
            Else
              m_result(intCount).warningMessage = ErrorMessage(NYKD_CONFLICT_QUANTITY, m_result(intCount).slipNo)
            End If
            m_result(intCount).LotSlipHeaderID = CInt(reader.GetValue("LotSlipHeaderID"))
          End While

        Case UNALLOCATED_TAB
          fromDate = rdt2SalesDate.FromIntDate
          toDate = rdt2SalesDate.ToIntDate

          selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_Unallocated_SYKH_SYKD")

          selectCommand.Parameters("@fromDate").SetValue(fromDate)
          selectCommand.Parameters("@toDate").SetValue(toDate)
          reader = selectCommand.ExecuteReader

          While reader.Read() = True
            If m_result Is Nothing Then m_result = {New SearchResult}
            intCount = intCount + 1
            ReDim Preserve m_result(intCount)
            m_result(intCount) = New SearchResult
            m_result(intCount).slipNo = CInt(reader.GetValue("sykh_denno"))
            m_result(intCount).slipDate = Date.ParseExact(reader.GetValue("sykh_uribi").ToString(), "yyyyMMdd", provider:=Globalization.CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
            m_result(intCount).partnerCode = reader.GetValue("sykh_tcd").ToString().Trim
            m_result(intCount).partnerName = Trim(reader.GetValue("cms_mei1").ToString().TrimEnd & " " & reader.GetValue("cms_mei2").ToString().TrimEnd)
            m_result(intCount).productCode = reader.GetValue("sykd_scd").ToString().Trim
            m_result(intCount).productName = Trim(reader.GetValue("sms_mei").ToString() & RTrim(" " + reader.GetValue("sms_kikaku").ToString()) + RTrim(" " + reader.GetValue("sms_color").ToString()) + RTrim(" " + reader.GetValue("sms_size").ToString()))
            m_result(intCount).quantity = Math.Round(CDec(reader.GetValue("sykd_suryo")), CInt(reader.GetValue("sms_sketa")))

          End While

        Case UNINPUTTED_TAB
          fromDate = rdt3PurchaseDate.FromIntDate
          toDate = rdt3PurchaseDate.ToIntDate

          selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_UnInputted_NYKH_NYKD")

          selectCommand.Parameters("@fromDate").SetValue(fromDate)
          selectCommand.Parameters("@toDate").SetValue(toDate)
          reader = selectCommand.ExecuteReader

          While reader.Read() = True
            If m_result Is Nothing Then m_result = {New SearchResult}
            intCount = intCount + 1
            ReDim Preserve m_result(intCount)
            m_result(intCount) = New SearchResult
            m_result(intCount).slipNo = CInt(reader.GetValue("nykh_denno"))
            'm_result(intCount).slipNo = CInt(reader.GetValue("sl_nykh_id"))
            m_result(intCount).slipDate = Date.ParseExact(reader.GetValue("nykh_uribi").ToString(), "yyyyMMdd", provider:=Globalization.CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
            m_result(intCount).partnerCode = reader.GetValue("nykh_tcd").ToString().Trim
            m_result(intCount).partnerName = Trim(reader.GetValue("cms_mei1").ToString().TrimEnd & " " & reader.GetValue("cms_mei2").ToString().TrimEnd)
            m_result(intCount).productCode = reader.GetValue("nykd_scd").ToString().Trim
            m_result(intCount).productName = Trim(reader.GetValue("sms_mei").ToString() & RTrim(" " + reader.GetValue("sms_kikaku").ToString()) + RTrim(" " + reader.GetValue("sms_color").ToString()) + RTrim(" " + reader.GetValue("sms_size").ToString()))
            m_result(intCount).quantity = Math.Round(CDec(reader.GetValue("nykd_suryo")), CInt(reader.GetValue("sms_sketa")))

          End While

      End Select

      If Not m_result Is Nothing Then
        PcaFunctionCommandExport.Enabled = True
      Else
        PcaFunctionCommandExport.Enabled = False
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSearch.CommandId)
      End If

      Return m_result
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If Not reader Is Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  Private Function ErrorMessage(errorcode As Integer, slipNo As Integer) As String
    Select Case errorcode
      Case 1
        Return "該当商品が売上伝票から削除されています。(売上伝票No:" & slipNo & ")"
      Case 2
        Return "出荷振替後に売上数が変更されています。(売上伝票No:" & slipNo & ")"
      Case 3
        Return "該当商品が仕入伝票から削除されています。(仕入伝票No:" & slipNo & ")"
      Case 4
        Return "入荷振替後に仕入数が変更されています。(仕入伝票No:" & slipNo & ")"
      Case Else
        Return Nothing
    End Select
  End Function

  Private Sub DisplaySearchResult(m_result As SearchResult())
    Try
      If m_result Is Nothing Then Return
      Select Case TabControl1.SelectedIndex

        Case DATA_REPORT_TAB
          For intCount As Integer = 0 To m_result.Length - 1
            If Not SLCmnFunction.checkObjectNothingEmpty(m_result(intCount)) Then
              tblResult1.SetCellValue(intCount, ReportTableColumn.type, m_result(intCount).type)
              tblResult1.SetCellValue(intCount, ReportTableColumn.slipNo, m_result(intCount).slipNo)
              tblResult1.SetCellValue(intCount, ReportTableColumn.slipDate, m_result(intCount).slipDate)
              tblResult1.SetCellValue(intCount, ReportTableColumn.partnerCode, m_result(intCount).partnerCode)
              tblResult1.SetCellValue(intCount, ReportTableColumn.partnerName, m_result(intCount).partnerName)
              tblResult1.SetCellValue(intCount, ReportTableColumn.productCode, m_result(intCount).productCode)
              tblResult1.SetCellValue(intCount, ReportTableColumn.productName, m_result(intCount).productName)
              tblResult1.SetCellValue(intCount, ReportTableColumn.warningMessage, m_result(intCount).warningMessage)
              tblResult1.SetCellValue(intCount, ReportTableColumn.LotSlipHeaderID, m_result(intCount).LotSlipHeaderID)
            End If
          Next
          tblResult1.DataRowCount = m_result.Length

        Case UNALLOCATED_TAB

          For intCount As Integer = 0 To m_result.Length - 1
            If Not SLCmnFunction.checkObjectNothingEmpty(m_result(intCount)) Then
              tblResult2.SetCellValue(intCount, UnAllocatedTableColumn.slipNo, m_result(intCount).slipNo)
              tblResult2.SetCellValue(intCount, UnAllocatedTableColumn.slipDate, m_result(intCount).slipDate)
              tblResult2.SetCellValue(intCount, UnAllocatedTableColumn.partnerCode, m_result(intCount).partnerCode)
              tblResult2.SetCellValue(intCount, UnAllocatedTableColumn.partnerName, m_result(intCount).partnerName)
              tblResult2.SetCellValue(intCount, UnAllocatedTableColumn.productCode, m_result(intCount).productCode)
              tblResult2.SetCellValue(intCount, UnAllocatedTableColumn.productName, m_result(intCount).productName)
              tblResult2.SetCellValue(intCount, UnAllocatedTableColumn.quantity, m_result(intCount).quantity)
              tblResult2.SetCellValue(intCount, UnAllocatedTableColumn.LotSlipHeaderID, m_result(intCount).LotSlipHeaderID)
            End If
          Next
          tblResult2.DataRowCount = m_result.Length

        Case UNINPUTTED_TAB

          For intCount As Integer = 0 To m_result.Length - 1
            If Not SLCmnFunction.checkObjectNothingEmpty(m_result(intCount)) Then
              tblResult3.SetCellValue(intCount, UnInputtedTableColumn.slipNo, m_result(intCount).slipNo)
              tblResult3.SetCellValue(intCount, UnInputtedTableColumn.slipDate, m_result(intCount).slipDate)
              tblResult3.SetCellValue(intCount, UnInputtedTableColumn.partnerCode, m_result(intCount).partnerCode)
              tblResult3.SetCellValue(intCount, UnInputtedTableColumn.partnerName, m_result(intCount).partnerName)
              tblResult3.SetCellValue(intCount, UnInputtedTableColumn.productCode, m_result(intCount).productCode)
              tblResult3.SetCellValue(intCount, UnInputtedTableColumn.productName, m_result(intCount).productName)
              tblResult3.SetCellValue(intCount, UnInputtedTableColumn.quantity, m_result(intCount).quantity)
              tblResult3.SetCellValue(intCount, UnInputtedTableColumn.LotSlipHeaderID, m_result(intCount).LotSlipHeaderID)
            End If
          Next
          tblResult3.DataRowCount = m_result.Length

      End Select

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

#End Region

#Region "TSC Meisai Table"
  Private Sub tblResult1_DoubleClickCell(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblResult1.DoubleClickCell
    SLCmnFunction.StartUpInputExe(IO.Path.GetDirectoryName(Application.ExecutablePath) & PROGRAM_TO_CALL, m_loginString, tblResult1.GetCellValue(args.Point.Y, ReportTableColumn.slipNo).ToString)

  End Sub

  Private Sub tblResult1_EnterInputRow(sender As System.Object, args As PCA.Controls.InputRowEventArgs) Handles tblResult1.EnterInputRow
    Select Case TabControl1.SelectedIndex

      Case DATA_REPORT_TAB
        If Not SLCmnFunction.checkObjectNothingEmpty(tblResult1.GetCellValue(args.RowIndex, ReportTableColumn.slipNo)) Then
          PcaFunctionCommandLotSlipStart.Enabled = True
        End If
      Case UNALLOCATED_TAB
        If Not SLCmnFunction.checkObjectNothingEmpty(tblResult2.GetCellValue(args.RowIndex, UnAllocatedTableColumn.slipNo)) Then
          PcaFunctionCommandLotSlipStart.Enabled = True
        End If
      Case UNINPUTTED_TAB
        If Not SLCmnFunction.checkObjectNothingEmpty(tblResult3.GetCellValue(args.RowIndex, UnInputtedTableColumn.slipNo)) Then
          PcaFunctionCommandLotSlipStart.Enabled = True
        End If

    End Select

  End Sub

  Private Sub tblResult1_Leave(sender As System.Object, e As System.EventArgs) Handles tblResult1.Leave, tblResult2.Leave, tblResult3.Leave
    PcaFunctionCommandLotSlipStart.Enabled = False
  End Sub

#End Region

  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then
      connector.LogOffSystem()
    End If
  End Sub

  Private Sub TabControl1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
    Select Case TabControl1.SelectedIndex

      Case DATA_REPORT_TAB
        If Not SLCmnFunction.checkObjectNothingEmpty(tblResult1.GetCellValue(0, ReportTableColumn.slipNo)) Then
          If CInt(tblResult1.GetCellValue(0, ReportTableColumn.slipNo)) <> 0 Then
            PcaFunctionCommandExport.Enabled = True
          Else
            PcaFunctionCommandExport.Enabled = False
          End If
        Else
          PcaFunctionCommandExport.Enabled = False
        End If
      Case UNALLOCATED_TAB
        If Not SLCmnFunction.checkObjectNothingEmpty(tblResult2.GetCellValue(0, UnAllocatedTableColumn.slipNo)) Then
          If CInt(tblResult2.GetCellValue(0, ReportTableColumn.slipNo)) <> 0 Then
            PcaFunctionCommandExport.Enabled = True
          Else
            PcaFunctionCommandExport.Enabled = False
          End If
        Else
          PcaFunctionCommandExport.Enabled = False
        End If
      Case UNINPUTTED_TAB
        If Not SLCmnFunction.checkObjectNothingEmpty(tblResult3.GetCellValue(0, UnInputtedTableColumn.slipNo)) Then
          If CInt(tblResult3.GetCellValue(0, ReportTableColumn.slipNo)) <> 0 Then
            PcaFunctionCommandExport.Enabled = True
          Else
            PcaFunctionCommandExport.Enabled = False
          End If
        Else
          PcaFunctionCommandExport.Enabled = False
        End If

    End Select
  End Sub

#Region "Validate Range Date"
  Private Sub rdt2SalesDate_Validated(sender As System.Object, e As System.EventArgs) Handles rdt2SalesDate.Validated
    DoSearch(tblResult2, m_ReportResult)
  End Sub

  Private Sub rdt1Date_Validated(sender As System.Object, e As System.EventArgs) Handles rdt1Date.Validated
    DoSearch(tblResult1, m_ReportResult)
  End Sub

  Private Sub rdt3PurchaseDate_Validated(sender As System.Object, e As System.EventArgs) Handles rdt3PurchaseDate.Validated
    DoSearch(tblResult3, m_ReportResult)
  End Sub
#End Region
  
End Class

Public Class ReportTableColumn
  Public Const type As String = "区分"
  Public Const slipNo As String = "伝票No"
  Public Const slipDate As String = "日付"
  Public Const partnerCode As String = "仕入先コード" 'Display　コード only in Table Header
  Public Const partnerName As String = "仕入先名"
  Public Const productCode As String = "商品コード" 'Display　コード only in Table Header
  Public Const productName As String = "商品名"
  Public Const warningMessage As String = "警告メッセージ"

  Public Const Code As String = "コード"

  Public Const LotSlipHeaderID As String = "LotSlipID" 'Invisible column
End Class

Public Class UnAllocatedTableColumn
  Public Const slipNo As String = "伝票No"
  Public Const slipDate As String = "売上日"
  Public Const partnerCode As String = "取引先コード" 'Display　コード only in Table Header
  Public Const partnerName As String = "取引先名"
  Public Const productCode As String = "商品コード" 'Display　コード only in Table Header
  Public Const productName As String = "商品名"
  Public Const quantity As String = "数量  "

  Public Const Code As String = "コード"

  Public Const LotSlipHeaderID As String = "LotSlipID" 'Invisible column
End Class

Public Class UnInputtedTableColumn
  Public Const slipNo As String = "伝票No"
  Public Const slipDate As String = "仕入日"
  Public Const partnerCode As String = "取引先コード" 'Display　コード only in Table Header
  Public Const partnerName As String = "取引先名"
  Public Const productCode As String = "商品コード" 'Display　コード only in Table Header
  Public Const productName As String = "商品名"
  Public Const quantity As String = "数量  "

  Public Const Code As String = "コード"

  Public Const LotSlipHeaderID As String = "LotSlipID" 'Invisible column
End Class

Public Class SearchResult
  Public Property type As String 'Only in ReportTableResult (1)
  Public Property slipNo As Integer
  Public Property slipDate As String
  Public Property partnerCode As String
  Public Property partnerName As String
  Public Property productCode As String
  Public Property productName As String
  Public Property warningMessage As String 'Only in ReportTableResult (1)
  Public Property quantity As Decimal 'Only in Unaalocated and Uninputted Result (2,3)

  Public Property LotSlipHeaderID As Integer
End Class

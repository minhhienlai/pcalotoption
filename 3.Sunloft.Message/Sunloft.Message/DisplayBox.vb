﻿Imports Sunloft.PCAForms
Imports PCA.UITools
Imports System.Windows.Forms

Public Class DisplayBox
  Public Shared Sub ShowError(ByVal Msg As String)
    MessageBox.Show(Msg, "警告")
    '2016/1/13 TNOGUCHI
    SaveLog(Msg)
  End Sub

  Public Shared Function ShowWarning(ByVal Msg As String) As DialogResult
    Return MessageBox.Show(Msg, "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
  End Function

  Public Shared Sub ShowNotify(ByVal Msg As String)
    MessageBox.Show(Msg, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
  End Sub

  Public Shared Sub ShowCritical(ByVal ex As Exception)
    MessageBox.Show(ex.Message, "エラーが発生しました")
    '2016/1/13 TNOGUCHI
    SaveErrorLog(ex)
    'TODO log output
  End Sub

  Public Shared Sub ShowCritical(ByVal ex As Exception, ByVal extraMessage As String, Optional ByVal Title As String = "エラーが発生しました")
    Dim sb As New System.Text.StringBuilder(256)
    If extraMessage.Length > 0 Then sb.AppendLine(extraMessage).AppendLine()
    sb.AppendLine("【システムエラー】")
    sb.AppendLine(ex.Message).AppendLine()
    MessageBox.Show(sb.ToString(), Title, MessageBoxButtons.OK, MessageBoxIcon.Error)
    SaveErrorLog(ex)
  End Sub

  Public Shared Function ShowContact(ByVal Msg As String) As DialogResult
    Dim result As DialogResult = MessageBox.Show("編集中の内容は未登録です。登録してもよろしいですか？", "登録確認", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
    Return result
  End Function

  Public Shared Sub ShowNoTitle(ByVal Msg As String)
    MessageBox.Show(Msg, String.Empty)
  End Sub

  Public Shared Sub SaveErrorLog(ByVal ex As Exception)

    Dim sbMsg As New System.Text.StringBuilder(256)

    Try
      sbMsg.AppendLine(DateTime.Now.ToString())
      sbMsg.AppendLine(ex.Source)
      sbMsg.AppendLine(ex.GetType().FullName)
      sbMsg.AppendLine(ex.Message)
      sbMsg.AppendLine(ex.StackTrace).AppendLine()
      SaveLog(sbMsg.ToString())
    Catch ex2 As Exception

    End Try
  End Sub

  Public Shared Function SaveLog(ByVal strErr As String) As Boolean
    Dim strPath As String = "..\errlog\" & DateTime.Now.ToString("yyyyMM") & "\"
    Dim strFileName As String = String.Format("{0}.txt", DateTime.Now.ToString("yyyyMMdd"))
    Dim isSuccess As Boolean
    Try
      'カレントディレクトリをアプリケーションのパスにセット
      System.IO.Directory.SetCurrentDirectory(Application.StartupPath)

      'ディレクトリが無かったら作成
      If Not System.IO.Directory.Exists(strPath) Then System.IO.Directory.CreateDirectory(strPath)

      'ファイルが無かったら作成
      Using fs As New System.IO.FileStream(strPath & strFileName, System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.Write)
        Using sw As New IO.StreamWriter(fs, System.Text.Encoding.GetEncoding(932))
          '書き込み
          sw.Write(strErr)
          sw.Flush()
          sw.Close()
        End Using
        fs.Close()
      End Using
      isSuccess = True

    Catch ex As Exception
      isSuccess = False
    End Try
    Return isSuccess
  End Function
End Class

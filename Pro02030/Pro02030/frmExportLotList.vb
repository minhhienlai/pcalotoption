﻿Imports Sunloft.Message
Imports PCA.ApplicationIntegration
Imports Sunloft.PCAIF
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports PCA.Controls
Imports System.Globalization
Imports System.IO
Imports System.Text

Public Class frmExportLotList
  Private m_columnName As ColumnName = New ColumnName
  Private m_result As columnResult() = {New columnResult}
  Private m_printResult As printColumnResult() = Nothing
  Private m_SlipNoColumn As Integer

  Property connector As IIntegratedApplication = Nothing
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private m_conf As Sunloft.PcaConfig
  Private m_loginString As String
  Private MasterDialog As SLMasterSearchDialog
  Private m_AMS1Class As AMS1
  Private m_DoSearch As Boolean = False
  Private m_currentItem As Integer
  Private m_ReferEnable As Boolean = True 'Enable F8 flag
  Private m_noOfChecked As Integer = 0
  Private m_currentRowIndex As Integer = -1
  Private m_currentColumnIndex As Integer = -1
  Private m_currentCellValue As String = ""
  Private m_isKeyDowned As Boolean = False
  Private m_SL_LMBClass As SL_LMB
  Private arrLotDetails(10) As String
  Private Const TOTAL_COLUMN As Integer = 5 'Total column standing in front of column 備考

  Private Const WIDTH_ISSUE As Integer = 50
  Private Const WIDTH_SALES_DATE As Integer = 100
  Private Const WIDTH_BILLING_DATE As Integer = 100
  Private Const WIDTH_CUSTOMER_CODE As Integer = 70
  Private Const WIDTH_CUSTOMER_NAME As Integer = 560
  Private Const WIDTH_SALES_SLIPNO As Integer = 80
  Private Const WIDTH_SLIPNO_ID As Integer = 100
  Private Const WIDTH_POSTAL_CODE As Integer = 100
  Private Const WIDTH_ADDRESS_1 As Integer = 100
  Private Const WIDTH_ADDRESS_2 As Integer = 100
  Private Const WIDTH_PHONE_NUMBER As Integer = 100
  Private Const WIDTH_FAX As Integer = 100
  Private Const WIDTH_SHIPPING_SLIPNO As Integer = 100

  Private Const REPORT_PRODUCTNAME_SIZE = 25

  Private Const HEIGHT_HEADER As Integer = 25

  Private Const ISSUE_COLUMN As Integer = 0
  Private Const SALES_DATE_COLUMN As Integer = 1
  Private Const BILLING_DATE_COLUMN As Integer = 2
  Private Const CUSTOMER_CODE_COLUMN As Integer = 3
  Private Const CUSTOMER_NAME_COLUMN As Integer = 4
  Private Const SALES_SLIPNO_COLUMN As Integer = 5
  Private Const SLIPNO_ID_COLUMN As Integer = 6
  Private Const POSTAL_CODE_COLUMN As Integer = 7
  Private Const ADDRESS_1_COLUMN As Integer = 8
  Private Const ADDRESS_2_COLUMN As Integer = 9
  Private Const PHONE_NUMBER_COLUMN As Integer = 10
  Private Const FAX_COLUMN As Integer = 11
  Private Const SHIPPING_SLIPNO_COLUMN As Integer = 12

  Private Const FROM_CUSTOMER_CODE_TEXT As Integer = 0
  Private Const TO_CUSTOMER_CODE_TEXT As Integer = 1
  Private Const FROM_SLIP_NO_CODE_TEXT As Integer = 2
  Private Const TO_SLIP_NO_CODE_TEXT As Integer = 3

  Private Const NUMBER_TYPE As String = "Number"
  Private Const TEXT_TYPE As String = "String"
  Private Const DATE_TYPE As String = "Date"
  Private Const CHECKBOX_TYPE As String = "CHECKBOX_TYPE"
  Private m_currentString As String = ""

  Private Const RPT_FILENAME As String = "ExportLotList02030.rpt" ' Form to Preview/Print
  Private Const RPT_TITLE As String = "出荷案内書"
  Private Const CSV_FILE_NAME As String = "出荷案内書"
  Private Const EXPORT_FUNCTION_BAR As Integer = 10
  Private m_enter As Integer = 0 'Resolve problem after select slipNo field can't select differant field
  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    m_appClass.ConnectToPCA()
    connector = m_appClass.connector

    m_conf = New Sunloft.PcaConfig
    m_loginString = m_appClass.EncryptedLoginString

    m_AMS1Class = New AMS1(connector)
    m_AMS1Class.ReadAll()
    m_SL_LMBClass = New SL_LMB(connector)
    m_SL_LMBClass.ReadOnlyRow()

    arrLotDetails(0) = m_SL_LMBClass.sl_lmb_label1
    arrLotDetails(1) = m_SL_LMBClass.sl_lmb_label2
    arrLotDetails(2) = m_SL_LMBClass.sl_lmb_label3
    arrLotDetails(3) = m_SL_LMBClass.sl_lmb_label4
    arrLotDetails(4) = m_SL_LMBClass.sl_lmb_label5
    arrLotDetails(5) = m_SL_LMBClass.sl_lmb_label6
    arrLotDetails(6) = m_SL_LMBClass.sl_lmb_label7
    arrLotDetails(7) = m_SL_LMBClass.sl_lmb_label8
    arrLotDetails(8) = m_SL_LMBClass.sl_lmb_label9
    arrLotDetails(9) = m_SL_LMBClass.sl_lmb_label10

    m_SlipNoColumn = m_SL_LMBClass.sl_lmb_maxlotop + TOTAL_COLUMN

    ' Add any initialization after the InitializeComponent() call.
    InitTable()

    MasterDialog = New SLMasterSearchDialog(connector)

    'Disable refer command (F8)
    PcaCommandItemUnselectAll.Enabled = False
    PcaCommandItemSelectAll.Enabled = False
    PcaCommandItemRefer.Enabled = False
    PcaCommandItemPrint.Enabled = False
    PcaCommandItemExport.Enabled = False
    PcaCommandItemPreview.Enabled = False
  End Sub
#Region "InitTable"
  ''' <summary>
  ''' set InitTable, header columns name
  ''' </summary>
  ''' <param name="isEmpty">Clear search result array</param>
  Private Sub InitTable(Optional ByVal isEmpty As Boolean = False)
    Try
      dgv.Columns.Clear()
      InitTableColumn(CHECKBOX_TYPE, m_columnName.issue, CInt(WIDTH_ISSUE), DataGridViewContentAlignment.MiddleCenter, , False, DataGridViewContentAlignment.MiddleCenter)
      InitTableColumn(TEXT_TYPE, m_columnName.salesdate, CInt(WIDTH_SALES_DATE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      InitTableColumn(TEXT_TYPE, m_columnName.billingdate, CInt(WIDTH_BILLING_DATE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      InitTableColumn(TEXT_TYPE, m_columnName.customercode, CInt(WIDTH_CUSTOMER_CODE), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, m_columnName.customername, CInt(WIDTH_CUSTOMER_NAME), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, m_columnName.slipNo, CInt(WIDTH_SALES_SLIPNO), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, m_columnName.slipNo_id, CInt(WIDTH_SLIPNO_ID), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft, False)
      InitTableColumn(TEXT_TYPE, m_columnName.postal, CInt(WIDTH_POSTAL_CODE), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft, False)
      InitTableColumn(TEXT_TYPE, m_columnName.address1, CInt(WIDTH_ADDRESS_1), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft, False)
      InitTableColumn(TEXT_TYPE, m_columnName.address2, CInt(WIDTH_ADDRESS_2), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft, False)
      InitTableColumn(TEXT_TYPE, m_columnName.phonenumber, CInt(WIDTH_PHONE_NUMBER), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft, False)
      InitTableColumn(TEXT_TYPE, m_columnName.fax, CInt(WIDTH_FAX), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft, False)
      InitTableColumn(TEXT_TYPE, m_columnName.shipping_slipNo, CInt(WIDTH_SHIPPING_SLIPNO), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      'Set header cell font color to white
      For intCount As Integer = 0 To m_SlipNoColumn - 1
        dgv.Columns(intCount).HeaderCell.Style.ForeColor = Color.White
      Next
      'Set header size (Default size is a bit small)
      dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
      dgv.ColumnHeadersHeight = HEIGHT_HEADER
      dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing

      If isEmpty Then
        m_result = Nothing 'Clear search result array
        dgv.Rows.Clear()
      End If

      'Set table not sortable
      For Each column As DataGridViewColumn In dgv.Columns
        column.SortMode = DataGridViewColumnSortMode.NotSortable
      Next
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  ''' <summary>
  ''' set alignment header
  ''' </summary>
  ''' <param name="headerType">Header Type</param>
  ''' <param name="headerText">Header Text</param>
  ''' <param name="columnwidth">Width Column</param>
  ''' <param name="alignment">Alignment Header Columns</param>
  ''' <param name="columnname">Column Name</param>
  ''' <param name="isReadonly">isReadonly</param>
  ''' <param name="headerAlignment">Header Alignment</param>
  ''' <param name="visible">Visible</param>
  ''' <remarks></remarks>
  Private Sub InitTableColumn(ByVal headerType As String, ByVal headerText As String, ByVal columnwidth As Integer, Optional ByVal alignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft, Optional ByVal columnname As String = "", Optional ByVal isReadonly As Boolean = True, Optional ByVal headerAlignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft, Optional ByVal visible As Boolean = True)
    Dim col As New DataGridViewColumn
    Select Case headerType
      Case CHECKBOX_TYPE
        col = New DataGridViewCheckBoxColumn
      Case NUMBER_TYPE
        col = New DataGridViewTextBoxColumn
      Case TEXT_TYPE
        col = New DataGridViewTextBoxColumn
      Case DATE_TYPE
        col = New DataGridViewTextBoxColumn
      Case Else
        col = New DataGridViewTextBoxColumn
    End Select
    col.ReadOnly = isReadonly
    If columnname = "" Then
      col.Name = headerText
    Else
      col.Name = columnname
    End If
    col.Width = columnwidth
    col.DefaultCellStyle.Alignment = alignment
    col.HeaderCell.Style.Alignment = headerAlignment
    col.DefaultCellStyle.FormatProvider = CultureInfo.CreateSpecificCulture("ja-JP")
    col.Visible = visible
    dgv.Columns.Add(col)
  End Sub
#End Region
#Region "DoSearch"
  Private Sub DoSearch()
    PcaRangeDate.Focus()
    'Get Search Result
    GetSearchResult()
    'Display Search Result into grid
    DisplaySearchResult()
    If Not m_result Is Nothing AndAlso Not m_result(0) Is Nothing Then
      PcaCommandItemSelectAll.Enabled = True
      PcaCommandItemUnselectAll.Enabled = True
      m_noOfChecked = m_result.Count
    Else
      PcaCommandItemSelectAll.Enabled = False
      PcaCommandItemUnselectAll.Enabled = False
      m_noOfChecked = 0
    End If
    dgv.Focus() 'Focus dgv after dosearch
  End Sub
#End Region
#Region "GetSearchResult"
  ''' <summary>
  ''' Event called when the user clicks search button
  ''' make a query into database to get the result
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetSearchResult()
    Try
      ToolStrip1.Focus() 'Before Search focus ToolStrip1
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim fromDate As Integer = PcaRangeDate.FromIntDate
      Dim toDate As Integer = PcaRangeDate.ToIntDate
      Dim fromCustomerCode As String = PcaRangeCodeSetCustomerCode.FromCodeText
      Dim toCustomerCode As String = PcaRangeCodeSetCustomerCode.ToCodeText
      Dim fromSlipNo As Integer
      Dim toSlipNo As Integer
      If PcaRangeCodeSetSlipNo.FromCodeText.ToString = "" Then
        fromSlipNo = 0
      Else
        fromSlipNo = CInt(PcaRangeCodeSetSlipNo.FromCodeText.ToString)
      End If
      If PcaRangeCodeSetSlipNo.ToCodeText.ToString = "" Then
        toSlipNo = 0
      Else
        toSlipNo = CInt(PcaRangeCodeSetSlipNo.ToCodeText.ToString)
      End If

      Dim intCount As Integer = -1

      Array.Clear(m_result, 0, m_result.Length)

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SYKH_Pro02030")
      selectCommand.Parameters("@fromDate").SetValue(fromDate)
      selectCommand.Parameters("@toDate").SetValue(toDate)
      selectCommand.Parameters("@fromCustomerCode").SetValue(fromCustomerCode)
      selectCommand.Parameters("@toCustomerCode").SetValue(toCustomerCode)
      selectCommand.Parameters("@fromSlipNo").SetValue(fromSlipNo)
      selectCommand.Parameters("@toSlipNo").SetValue(toSlipNo)
      reader = selectCommand.ExecuteReader

      While reader.Read() = True
        intCount = intCount + 1
        ReDim Preserve m_result(intCount)
        m_result(intCount) = New columnResult
        If reader.GetValue("sykh_uribi").ToString <> "" AndAlso CInt(reader.GetValue("sykh_uribi").ToString) > 0 Then
          m_result(intCount).salesdate = Date.ParseExact(reader.GetValue("sykh_uribi").ToString(), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        Else
          m_result(intCount).salesdate = ""
        End If
        If reader.GetValue("sykh_seibi").ToString <> "" AndAlso CInt(reader.GetValue("sykh_seibi").ToString) > 0 Then
          m_result(intCount).billingdate = Date.ParseExact(reader.GetValue("sykh_seibi").ToString(), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        Else
          m_result(intCount).billingdate = ""
        End If
        m_result(intCount).customercode = reader.GetValue("sykh_tcd").ToString.TrimEnd()
        m_result(intCount).customername = reader.GetValue("cms_mei1").ToString.TrimEnd() & " " & reader.GetValue("cms_mei2").ToString.TrimEnd()
        m_result(intCount).slipNo = reader.GetValue("sykh_denno").ToString.TrimEnd()

        'Creat more Column but visible to call for Print/Preview
        m_result(intCount).slipNo_id = CInt(reader.GetValue("sykh_yi1").ToString())
        m_result(intCount).postal = reader.GetValue("cms_mail").ToString()
        m_result(intCount).address1 = reader.GetValue("cms_ad1").ToString()
        m_result(intCount).address2 = reader.GetValue("cms_ad2").ToString()
        m_result(intCount).phonenumber = reader.GetValue("cms_tel").ToString()
        m_result(intCount).fax = reader.GetValue("cms_fax").ToString()
        m_result(intCount).shipping_slipNo = reader.GetValue("sl_sykh_denno").ToString()

      End While

      reader.Close()
      reader.Dispose()
      If Not m_result Is Nothing AndAlso Not m_result(0) Is Nothing Then
        PcaCommandItemPrint.Enabled = True
        PcaCommandItemExport.Enabled = True
        PcaCommandItemPreview.Enabled = True
        PcaCommandItemSelectAll.Enabled = True
        PcaCommandItemUnselectAll.Enabled = True
      Else
        PcaCommandItemPrint.Enabled = False
        PcaCommandItemExport.Enabled = False
        PcaCommandItemPreview.Enabled = False
        PcaCommandItemSelectAll.Enabled = False
        PcaCommandItemUnselectAll.Enabled = False
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSearch.CommandId)
        PcaRangeDate.FocusFrom()
      End If

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try

  End Sub
#End Region
#Region "DisplaySearchResult"
  ''' <summary>
  ''' Display search result into table
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub DisplaySearchResult()
    Try
      dgv.Columns.Clear()
      InitTable()

      For intCount As Integer = 0 To m_result.Length - 1
        If Not SLCmnFunction.checkObjectNothingEmpty(m_result(intCount)) Then
          dgv.Rows.Add(New DataGridViewRow)

          dgv.Rows.Item(intCount).Cells.Item(ISSUE_COLUMN).Value = True
          dgv.Rows.Item(intCount).Cells.Item(SALES_DATE_COLUMN).Value = m_result(intCount).salesdate
          dgv.Rows.Item(intCount).Cells.Item(BILLING_DATE_COLUMN).Value = m_result(intCount).billingdate
          dgv.Rows.Item(intCount).Cells.Item(CUSTOMER_CODE_COLUMN).Value = m_result(intCount).customercode
          dgv.Rows.Item(intCount).Cells.Item(CUSTOMER_NAME_COLUMN).Value = m_result(intCount).customername
          dgv.Rows.Item(intCount).Cells.Item(SALES_SLIPNO_COLUMN).Value = m_result(intCount).slipNo
          dgv.Rows.Item(intCount).Cells.Item(SLIPNO_ID_COLUMN).Value = m_result(intCount).slipNo_id
          dgv.Rows.Item(intCount).Cells.Item(POSTAL_CODE_COLUMN).Value = m_result(intCount).postal
          dgv.Rows.Item(intCount).Cells.Item(ADDRESS_1_COLUMN).Value = m_result(intCount).address1
          dgv.Rows.Item(intCount).Cells.Item(ADDRESS_2_COLUMN).Value = m_result(intCount).address2
          dgv.Rows.Item(intCount).Cells.Item(PHONE_NUMBER_COLUMN).Value = m_result(intCount).phonenumber
          dgv.Rows.Item(intCount).Cells.Item(FAX_COLUMN).Value = m_result(intCount).fax
          dgv.Rows.Item(intCount).Cells.Item(SHIPPING_SLIPNO_COLUMN).Value = m_result(intCount).shipping_slipNo
        End If
      Next

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub
#End Region


  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
    '
    Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
    Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子

    If (keyCode = Keys.Tab OrElse keyCode = Keys.Enter) Then
      'Validate if user press Enter/Tab +  Anykey except Shift, →, ↓
      m_DoSearch = True
    Else
      m_DoSearch = False
    End If

    Return MyBase.ProcessDialogKey(keyData)

  End Function

#Region "SearchItem"
  ' <summary>
  ' Display search screen (Search items like press F8)
  ' </summary>
  ' <param name="sender"></param>
  '<param name="e"></param>
  ' <remarks></remarks>
  Private Sub SearchItem(ByVal searchItem As Integer, Optional ByVal e As System.ComponentModel.CancelEventArgs = Nothing)
    Dim itemName As String = String.Empty
    Dim newCode As String = String.Empty
    ' Dim location As Point = Tools.ControlTool.GetDialogLocation(Me.tblResult.RefButton)
    Dim strCurrentCode As String = String.Empty

    Try
      Dim denpyoDialog As SLDenSearchDialog = New SLDenSearchDialog(connector)
      denpyoDialog.Location = New Point(dgv.Location.X, dgv.Location.Y)
      Dim MasterDialog As SLMasterSearchDialog = New SLMasterSearchDialog(connector)

      denpyoDialog.StartPosition = FormStartPosition.CenterParent

      newCode = denpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.PCASalesSlip)

      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
      End If
      If searchItem = FROM_SLIP_NO_CODE_TEXT Then
        PcaRangeCodeSetSlipNo.FromCodeText = newCode
      Else
        PcaRangeCodeSetSlipNo.ToCodeText = newCode
      End If

      validateManagement(Nothing, If(searchItem = FROM_SLIP_NO_CODE_TEXT, PcaRangeCodeSetSlipNo.FromCodeText, PcaRangeCodeSetSlipNo.ToCodeText))


    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
#End Region
#Region "validatePcaRangeCodeSetCustomerCode"

  Private Sub PcaRangeCodeSetCustomerCode_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaRangeCodeSetCustomerCode.ToCodeTextValidating2
    validateManagement(e, PcaRangeCodeSetCustomerCode.ToCodeText)
  End Sub
  Private Sub PcaRangeCodeSetCustomerCode_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaRangeCodeSetCustomerCode.FromCodeTextValidating2
    validateManagement(e, PcaRangeCodeSetCustomerCode.FromCodeText)
  End Sub

  Private Sub PcaRangeCodeSetCustomerCode_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaRangeCodeSetCustomerCode.Leave
    PcaCommandItemRefer.Enabled = False
  End Sub

  Private Sub PcaRangeCodeSetCustomerCode_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaRangeCodeSetCustomerCode.ToCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = PcaRangeCodeSetCustomerCode.ToCodeText
    m_currentItem = TO_CUSTOMER_CODE_TEXT
  End Sub

  Private Sub PcaRangeCodeSetCustomerCode_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaRangeCodeSetCustomerCode.FromCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = PcaRangeCodeSetCustomerCode.FromCodeText
    m_currentItem = FROM_CUSTOMER_CODE_TEXT
  End Sub
  Private Sub PcaRangeCodeSetCustomerCode_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaRangeCodeSetCustomerCode.ClickFromReferButton2
    m_currentItem = FROM_CUSTOMER_CODE_TEXT
    DisplayReferScreen(e, FROM_CUSTOMER_CODE_TEXT)
  End Sub

  Private Sub PcaRangeCodeSetCustomerCode_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaRangeCodeSetCustomerCode.ClickToReferButton2
    m_currentItem = TO_CUSTOMER_CODE_TEXT
    DisplayReferScreen(e, TO_CUSTOMER_CODE_TEXT)
  End Sub
#End Region

#Region "validateManagement"
  Private Sub validateManagement(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByRef CodeText As String = "")
    If m_currentItem = FROM_CUSTOMER_CODE_TEXT Or m_currentItem = TO_CUSTOMER_CODE_TEXT Then
      Try
        Dim PcaRangeCodeSetCustomerCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_CustomerLength)
        If Not String.IsNullOrEmpty(PcaRangeCodeSetCustomerCode) AndAlso m_currentString = PcaRangeCodeSetCustomerCode Then
          CodeText = PcaRangeCodeSetCustomerCode
          'No changes
          Return
        End If

        'tblResult.ClearCellValues()
        If String.IsNullOrEmpty(PcaRangeCodeSetCustomerCode) Then
          Return
        End If
        'validate supplier code
        Dim beMasterTms As PCA.TSC.Kon.BusinessEntity.BEMasterTms = MasterDialog.FindBEMasterTms(PcaRangeCodeSetCustomerCode)

        If Not beMasterTms.TokuisakiCode.Length > 0 Then
          'If there's error
          If Not e Is Nothing Then e.Cancel = True
          CodeText = m_currentString
          Return
        Else
          CodeText = PcaRangeCodeSetCustomerCode
        End If

      Catch ex As Exception
        DisplayBox.ShowCritical(ex)
      End Try
    ElseIf m_currentItem = FROM_SLIP_NO_CODE_TEXT Or m_currentItem = TO_SLIP_NO_CODE_TEXT Then

      Try
        If m_currentString = CodeText Then
          'No changes
          Return
        End If
        Dim intSlipNo As Integer
        If CodeText = "" AndAlso Not Integer.TryParse(CodeText, intSlipNo) Then
          Return
        End If
        Dim selectCommand As ICustomCommand
        Dim reader As ICustomDataReader
        selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SYKH")
        selectCommand.Parameters("@sykh_denno").SetValue(intSlipNo)
        reader = selectCommand.ExecuteReader
        If reader.Read() Then
        Else
          If Not e Is Nothing Then e.Cancel = True
          CodeText = ""
        End If
        reader.Close()
        reader.Dispose()

        If m_currentItem = FROM_SLIP_NO_CODE_TEXT Then
          PcaRangeCodeSetSlipNo.FromCodeText = CodeText
        Else
          PcaRangeCodeSetSlipNo.ToCodeText = CodeText
        End If

        m_enter = 1

      Catch ex As Exception
        DisplayBox.ShowCritical(ex)
      End Try
    End If
  End Sub
#End Region
#Region "DisplayReferScreen"
  ''' <summary>
  ''' Display refer screen when the user selects from CodeSet field
  ''' </summary>
  ''' <param name="e"></param>
  ''' <param name="Item"></param>
  ''' <remarks></remarks>
  Private Sub DisplayReferScreen(ByVal e As System.ComponentModel.CancelEventArgs, ByRef Item As Integer)
    If m_currentItem = FROM_CUSTOMER_CODE_TEXT Or m_currentItem = TO_CUSTOMER_CODE_TEXT Then
      Dim newCode As String = String.Empty
      Dim location As Point = PCA.TSC.Kon.Tools.ControlTool.GetDialogLocation(PcaRangeCodeSetCustomerCode.FromReferButton)
      newCode = MasterDialog.ShowReferTmsDialog(If(Item = FROM_CUSTOMER_CODE_TEXT, PcaRangeCodeSetCustomerCode.FromCodeText, PcaRangeCodeSetCustomerCode.ToCodeText), location)
      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
        If m_currentString = newCode Then
          'No changes
          Return
        End If
        Dim beMasterTms As PCA.TSC.Kon.BusinessEntity.BEMasterTms = MasterDialog.FindBEMasterTms(newCode)
        If Not beMasterTms.TokuisakiCode.Length > 0 Then
          'Validate failed
          If Not e Is Nothing Then e.Cancel = True
          If Item = FROM_CUSTOMER_CODE_TEXT Then
            PcaRangeCodeSetCustomerCode.FromCodeText = m_currentString
          Else
            PcaRangeCodeSetCustomerCode.ToCodeText = m_currentString
          End If
          Return
        End If
        If Item = FROM_CUSTOMER_CODE_TEXT Then
          PcaRangeCodeSetCustomerCode.FromCodeText = newCode
        Else
          PcaRangeCodeSetCustomerCode.ToCodeText = newCode
        End If
      End If
    ElseIf m_currentItem = FROM_SLIP_NO_CODE_TEXT Or m_currentItem = TO_SLIP_NO_CODE_TEXT Then
      SearchItem(m_currentItem, e)
    End If
  End Sub
#End Region
#Region "validatePcaRangeCodeSetSlipNo"
  Private Sub PcaRangeCodeSetSlipNo_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaRangeCodeSetSlipNo.ToCodeTextValidating2
    validateManagement(e, PcaRangeCodeSetSlipNo.ToCodeText)
  End Sub

  Private Sub PcaRangeCodeSetSlipNo_ToCodeTextValidated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaRangeCodeSetSlipNo.ToCodeTextValidated
    If m_DoSearch Then
      m_DoSearch = False
      DoSearch()
    End If
  End Sub

  Private Sub PcaRangeCodeSetSlipNo_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaRangeCodeSetSlipNo.ToCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_DoSearch = False
    m_currentString = PcaRangeCodeSetSlipNo.ToCodeText
    m_currentItem = TO_SLIP_NO_CODE_TEXT
  End Sub

  Private Sub PcaRangeCodeSetSlipNo_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaRangeCodeSetSlipNo.FromCodeTextValidating2
    validateManagement(e, PcaRangeCodeSetSlipNo.FromCodeText)
  End Sub

  Private Sub PcaRangeCodeSetSlipNo_FromCodeTextLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaRangeCodeSetSlipNo.Leave
    PcaCommandItemRefer.Enabled = False
    If m_enter = 1 Then
      PcaRangeCodeSetSlipNo.FocusTo()
      m_enter = 0
    End If
  End Sub

  Private Sub PcaRangeCodeSetSlipNo_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaRangeCodeSetSlipNo.FromCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentItem = FROM_SLIP_NO_CODE_TEXT
    m_currentString = PcaRangeCodeSetSlipNo.FromCodeText
  End Sub
  Private Sub PcaRangeCodeSetSlipNo_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaRangeCodeSetSlipNo.ClickToReferButton2
    m_currentItem = TO_SLIP_NO_CODE_TEXT
    SearchItem(TO_SLIP_NO_CODE_TEXT, e)
  End Sub

  Private Sub PcaRangeCodeSetSlipNo_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaRangeCodeSetSlipNo.ClickFromReferButton2
    m_currentItem = FROM_SLIP_NO_CODE_TEXT
    SearchItem(FROM_SLIP_NO_CODE_TEXT, e)
  End Sub
#End Region

#Region "PCA command Manager"
  ''' <summary>
  ''' Event called when PCACommandManager is called (PCA Function Bar button is pressed/Select from Menu, press F～ key)
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub PcaCommandManager1_Command(ByVal sender As System.Object, ByVal e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try
      Dim commandItem As PcaCommandItem = e.CommandItem

      Select Case True
        'Case commandItem Is PcaCommandItemHelp 'F1
        'do nothing
        Case commandItem Is PcaCommandItemSearch 'F5
          DoSearch()
        Case commandItem Is PcaCommandItemPrint 'F9
          'print data
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.salesdate, 0)) Then
            If MsgBox(RPT_TITLE & "を" & SLConstants.ConfirmMessage.PRINT_CONFIRM_MESSAGE, MsgBoxStyle.OkCancel, SLConstants.NotifyMessage.TITLE_MESSAGE) = MsgBoxResult.Ok Then
              PrintData()
            End If
          End If
        Case commandItem Is PcaCommandItemPreview 'Shift + F9
          'preview
          PrintData(True)
        Case commandItem Is PcaCommandItemExport 'F10
          'do export
          If OutPutCSV() Then
            SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, _
                                      SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, SLConstants.DURATION, PcaFunctionBar1, ToolTip1, Me, EXPORT_FUNCTION_BAR)
          End If
          'Case commandItem Is PcaCommandItemRefer
        Case commandItem Is PcaCommandItemRefer
          If m_isKeyDowned Then m_isKeyDowned = False : Exit Sub
          DisplayReferScreen(Nothing, m_currentItem)

        Case commandItem Is PcaCommandItemClose 'F12
          Me.Close()
          'do SelectAll
        Case commandItem Is PcaCommandItemSelectAll 'F3
          PcaRangeDate.Focus()
          For intCount As Integer = 0 To m_result.Count - 1
            dgv.Rows.Item(intCount).Cells.Item(ISSUE_COLUMN).Value = True
          Next
          m_noOfChecked = m_result.Count
          'do UnselectAll
        Case commandItem Is PcaCommandItemUnselectAll 'F4
          PcaRangeDate.Focus()
          For intCount As Integer = 0 To m_result.Count - 1
            dgv.Rows.Item(intCount).Cells.Item(ISSUE_COLUMN).Value = False
          Next
          m_noOfChecked = 0
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
#End Region
  Private Sub frmExportLotList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
    If e.KeyCode = Keys.F8 Then m_isKeyDowned = True
  End Sub
#Region "Print/Preview"
  ''' <summary>
  ''' Export detailed information output of goods sold
  ''' </summary>
  Private Function PrintData(Optional ByVal isPreview As Boolean = False) As Boolean
    Try
      Dim ReportName As String = RPT_FILENAME
      Dim ReportTitle As String = RPT_TITLE
      Dim rpt As New Sunloft.Windows.Forms.SLCrystalReport
      Dim RpParam As New Dictionary(Of String, Object)
      Dim editedDataTable As DataTable = Nothing

      rpt.ReportFile = Path.GetDirectoryName(Application.ExecutablePath) & "\" & ReportName
      If Not EditPrintData(editedDataTable) Then
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.PRINT_DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, 9)
        Return False
        Exit Function
      End If

      rpt.DataSource = editedDataTable
      ' 出力開始
      If isPreview Then
        rpt.Preview(ReportTitle, True)
      Else
        rpt.PrintToPrinter()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

    Return True
  End Function
  Private Sub GetPrintData()
    Try
      m_printResult = Nothing
      For intCount As Integer = 0 To dgv.Rows.Count - 1
        If dgv(m_columnName.issue, intCount).Value.ToString = "True" Then
          Dim selectCommand As ICustomCommand
          Dim reader As ICustomDataReader = Nothing

          selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SYKH_Pro02030_PRINT")
          selectCommand.Parameters("@sykh_id").SetValue(dgv(m_columnName.slipNo_id, intCount).Value)
          reader = selectCommand.ExecuteReader

          While reader.Read() = True
            ' add header infor
            If m_printResult Is Nothing Then
              m_printResult = {New printColumnResult}
            Else
              ReDim Preserve m_printResult(m_printResult.Length)
            End If

            m_printResult(m_printResult.Length - 1) = New printColumnResult
            m_printResult(m_printResult.Length - 1).rowIndex = intCount
            m_printResult(m_printResult.Length - 1).shipping_code = reader.GetValue("sl_sykh_tcd").ToString()
            m_printResult(m_printResult.Length - 1).memo = reader.GetValue("sl_sykh_tekmei").ToString()
            m_printResult(m_printResult.Length - 1).product_code = reader.GetValue("sl_sykd_scd").ToString()
            m_printResult(m_printResult.Length - 1).product_name = reader.GetValue("sl_sykd_mei").ToString()
            m_printResult(m_printResult.Length - 1).lotNo = reader.GetValue("sl_zdn_ulotno").ToString()
            m_printResult(m_printResult.Length - 1).quantity = CDec(reader.GetValue("sl_sykd_suryo").ToString())
            m_printResult(m_printResult.Length - 1).number_of_fish = CInt(reader.GetValue("sl_sykd_honsu").ToString())
            If reader.GetValue("sl_zdn_bbdate").ToString() <> "0" Then
              m_printResult(m_printResult.Length - 1).Bbdate = Date.ParseExact(reader.GetValue("sl_zdn_bbdate").ToString(), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
            Else
              m_printResult(m_printResult.Length - 1).Bbdate = ""
            End If
            For intCountLotDetails As Integer = 1 To m_SL_LMBClass.sl_lmb_maxlotop
              m_printResult(m_printResult.Length - 1).Lotdetails(intCountLotDetails) = reader.GetValue("sl_zdn_ldmei" & intCountLotDetails.ToString).ToString()
            Next
            m_printResult(m_printResult.Length - 1).quantityNoOfDecDigit = CInt(reader.GetValue("sms_sketa"))
          End While

          'MsgBox(reader.GetValue("sms_scd").ToString)
          If Not reader Is Nothing Then
            reader.Close()
            reader.Dispose()
          End If

        End If
      Next


    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub
  Private Function EditPrintData(ByRef argEditedData As DataTable) As Boolean
    Dim editData As New DataSet1.DataTable1DataTable
    Dim editRow As DataRow
    Dim intLpc As Integer = 0
    Dim editLotDetails As String = ""
    Dim result As Boolean = False
    Try
      GetPrintData()
      While Not m_printResult Is Nothing AndAlso intLpc < m_printResult.Length AndAlso Not m_printResult(intLpc) Is Nothing
        If m_printResult(intLpc).rowIndex > -1 Then
          With dgv
            editRow = editData.NewRow
            editRow.Item("SlipNo") = "No: " & dgv(m_columnName.slipNo, m_printResult(intLpc).rowIndex).Value.ToString
            editRow.Item("Shipping_code") = m_printResult(intLpc).shipping_code
            editRow.Item("Customername") = dgv(m_columnName.customername, m_printResult(intLpc).rowIndex).Value.ToString
            editRow.Item("Postal") = "〒" & dgv(m_columnName.postal, m_printResult(intLpc).rowIndex).Value.ToString
            editRow.Item("Address1") = dgv(m_columnName.address1, m_printResult(intLpc).rowIndex).Value.ToString.Trim
            editRow.Item("Address2") = dgv(m_columnName.address2, m_printResult(intLpc).rowIndex).Value.ToString.Trim
            editRow.Item("Phonenumber") = "TEL: " & dgv(m_columnName.phonenumber, m_printResult(intLpc).rowIndex).Value.ToString
            editRow.Item("Fax") = "FAX: " & dgv(m_columnName.fax, m_printResult(intLpc).rowIndex).Value.ToString
            editRow.Item("Memo") = "摘要: " & m_printResult(intLpc).memo
            editRow.Item("Product_code") = m_printResult(intLpc).product_code
            editRow.Item("Product_name") = Sunloft.Common.SLCmnFunction.NewLineManager(m_printResult(intLpc).product_name, REPORT_PRODUCTNAME_SIZE)
            editRow.Item("LotNo") = m_printResult(intLpc).lotNo
            editRow.Item("Quantity") = SLCmnFunction.formatNumberOfDigit(CDec(m_printResult(intLpc).quantity), m_printResult(intLpc).quantityNoOfDecDigit)
            If m_printResult(intLpc).number_of_fish = 0 Then
              editRow.Item("Number_of_fish") = ""
            Else
              editRow.Item("Number_of_fish") = SLCmnFunction.formatNumberOfDigit(m_printResult(intLpc).number_of_fish)
            End If

            editLotDetails = ""
            If m_SL_LMBClass.sl_lmb_bbdflg > 0 Then editLotDetails = m_printResult(intLpc).Bbdate

            For intCountLotDetails As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
              If Not m_printResult(intLpc).Lotdetails(intCountLotDetails) Is Nothing AndAlso m_printResult(intLpc).Lotdetails(intCountLotDetails).Trim <> "" Then
                If editLotDetails = "" Then
                  editLotDetails = m_printResult(intLpc).Lotdetails(intCountLotDetails)
                Else
                  editLotDetails = editLotDetails & Environment.NewLine & m_printResult(intLpc).Lotdetails(intCountLotDetails)
                End If
              End If

            Next

            editRow.Item("Lotdetails1") = editLotDetails
            editRow.Item("SlipNo_id") = dgv(m_columnName.slipNo_id, m_printResult(intLpc).rowIndex).Value.ToString

            editRow.Item("CompanyName") = m_AMS1Class.companyName
            editRow.Item("CompanyPostalCode") = "〒" & m_AMS1Class.companyPostalCode
            editRow.Item("CompanyAddress1") = m_AMS1Class.companyAddress1
            editRow.Item("CompanyAddress2") = m_AMS1Class.companyAddress2
            editRow.Item("CompanyTel") = "TEL: " & m_AMS1Class.companyTel
            editRow.Item("CompanyFAX") = "FAX: " & m_AMS1Class.companyFAX

            editData.Rows.Add(editRow)
            result = True
          End With

        End If
        intLpc = intLpc + 1
      End While

      argEditedData = editData
      Return result
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

#End Region
#Region "OutPutCSV"
  Private Function OutPutCSV() As Boolean
    Dim strSaveFileName As String = String.Empty
    Dim strColumns() As String
    Dim sbCsv As New StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      ToolStrip1.Focus()
      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .FileName = CSV_FILE_NAME
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        .OverwritePrompt = True

        Dim res As System.Windows.Forms.DialogResult = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False

        strSaveFileName = .FileName

      End With

      With m_columnName
        strColumns = {.issue, .salesdate, .billingdate, .customercode, .customername, .slipNo}
      End With

      SLCmnFunction.OutPutCSVFromDataGridView(strColumns, dgv, strSaveFileName, dgv.RowCount, 0)
      'OutPutCSVFromDataGridView(strColumns, dgv, strSaveFileName, dgv.RowCount, 0)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function
  Private Sub dgv_CurrentCellDirtyStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgv.CurrentCellDirtyStateChanged
    If dgv.IsCurrentCellDirty Then
      dgv.CommitEdit(DataGridViewDataErrorContexts.Commit)
    End If
  End Sub
#End Region
End Class
''' <summary>
''' This class defines header properties for the table that will be used do display result search
''' </summary>
''' <remarks></remarks>
Public Class ColumnName
  Public Property issue As String = "発行"
  Public Property salesdate As String = "売上日"
  Public Property billingdate As String = "請求日"
  Public Property customercode As String = "得意先"
  Public Property customername As String = "得意先名"
  Public Property slipNo As String = "売上No"
  Public Property slipNo_id As String = "売上Noid"
  Public Property postal As String = "1"
  Public Property address1 As String = "2"
  Public Property address2 As String = "3"
  Public Property phonenumber As String = "4"
  Public Property fax As String = "5"
  Public Property shipping_slipNo As String = "出荷No"
End Class

Public Class PrintColumnResultName
  Public Property shipping_code As String = "出荷先コード"
  Public Property memo As String = "摘要"
  Public Property product_code As String = "商品コード"
  Public Property product_name As String = "商品名"
  Public Property lotNo As String = "ロットNo"
  Public Property quantity As String = "数量"
  Public Property number_of_fish As String = "本数"

  Public Property Bbdate As String = "賞味期限"
  Public Property Lotdetails As String = "ロット詳細情報"
  Public Property Lotdetails1 As String = "ロット詳細情報1"
  Public Property Lotdetails2 As String = "ロット詳細情報2"
  Public Property Lotdetails3 As String = "ロット詳細情報3"
  Public Property Lotdetails4 As String = "ロット詳細情報4"
  Public Property Lotdetails5 As String = "ロット詳細情報5"
  Public Property Lotdetails6 As String = "ロット詳細情報6"
  Public Property Lotdetails7 As String = "ロット詳細情報7"
  Public Property Lotdetails8 As String = "ロット詳細情報8"
  Public Property Lotdetails9 As String = "ロット詳細情報9"
  Public Property Lotdetails10 As String = "ロット詳細情報10"
End Class

Public Class printColumnResult
  Public Property shipping_code As String = ""
  Public Property memo As String = ""
  Public Property product_code As String = ""
  Public Property product_name As String = ""
  Public Property lotNo As String = ""
  Public Property quantity As Decimal = 0
  Public Property number_of_fish As Integer
  Public Property quantityNoOfDecDigit As Integer
  Public Property Bbdate As String = ""
  Public Property Lotdetails As String() = New String(10) {}
  Public Property rowIndex As Integer = -1
End Class

Public Class columnResult
  Public Property issue As String = ""
  Public Property salesdate As String = ""
  Public Property billingdate As String = ""
  Public Property customercode As String = ""
  Public Property customername As String = ""
  Public Property slipNo As String = ""
  Public Property slipNo_id As Integer
  Public Property postal As String = ""
  Public Property address1 As String = ""
  Public Property address2 As String = ""
  Public Property phonenumber As String = ""
  Public Property fax As String = ""
  Public Property shipping_slipNo As String = ""
End Class
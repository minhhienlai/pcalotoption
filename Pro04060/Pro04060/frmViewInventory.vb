﻿Imports PCA.ApplicationIntegration
Imports Sunloft.PCAIF
Imports Sunloft.PCAForms
Imports Sunloft.Message
Imports System.Globalization
Imports Sunloft.Common

Public Class frmViewInventory

  Private Const WIDTH_ID As Single = 80
  Private Const WIDTH_WAREHOUSING_DATE As Single = 80
  Private Const WIDTH_INVENTORY As Single = 80
  Private Const WIDTH_PRODUCT_CODE As Single = 100
  Private Const WIDTH_PRODUCT_NAME As Single = 200
  Private Const WIDTH_LOT_NO As Single = 80
  Private Const WIDTH_CURRENT_QUANTITY As Single = 120
  Private Const WIDTH_NUMBER_OF_FISH As Single = 120
  Private Const WIDTH_UNIT_PRICE As Single = 120
  Private Const WIDTH_AMOUNT_OF_MONEY As Single = 120
  Private Const WIDTH_SHUKKA_DATE As Single = 80
  Private Const WIDTH_SHIYOU_DATE As Single = 80
  Private Const WIDTH_SHOUMI_DATE As Single = 80
  Private Const WIDTH_SHOUHI_DATE As Single = 80
  Private Const WIDTH_LOT_DETAILS As Single = 80
  Private Const WIDTH_SLIP_NO As Integer = 150

  Private Const ID_DATE_COLUMN As Integer = 0
  Private Const WAREHOUSING_DATE_COLUMN As Integer = 1
  Private Const INVENTORY_COLUMN As Integer = 2
  Private Const PRODUCT_CODE_COLUMN As Integer = 3
  Private Const PRODUCT_NAME_COLUMN As Integer = 4
  Private Const LOT_NO_COLUMN As Integer = 5
  Private Const CURRENT_QUANTITY_COLUMN As Integer = 6
  Private Const NUMBER_OF_FISH_COLUMN As Integer = 7
  Private Const UNIT_PRICE_COLUMN As Integer = 8
  Private Const AMOUNT_OF_MONEY_COLUMN As Integer = 9
  Private Const SHUKKA_DATE_COLUMN As Integer = 10
  Private Const SHIYOU_DATE_COLUMN As Integer = 11
  Private Const SHOUMI_DATE_COLUMN As Integer = 12
  Private Const SHOUHI_DATE_COLUMN As Integer = 13
  Private Const LOT_DETAILS_COLUMN As Integer = 14

  Private Const REPORT_PRODUCTNAME_LENGTH = 46

  Private Const ROW_COLUMN As Integer = 14
  Private Const HEIGHT_HEADER As Integer = 25
  Private Const NUMBER_TYPE As String = "Number"
  Private Const TEXT_TYPE As String = "String"
  Private Const DATE_TYPE As String = "Date"
  Private Const PROGRAM_TO_CALL As String = "\Pro04070.exe" 'Click table cell -> Display slip
  Private Const RPT_FILENAME As String = "ViewInventory04060.rpt" ' Form to preview/Print
  Private Const RPT_TITLE As String = "ロット別在庫一覧表"
  Private Const CSV_FILE_NAME As String = "ロット別在庫一覧表"
  Private Const NOT_USE As Integer = 0
  Private Const USE As Integer = 1
  Private Const FROM_PRODUCT_CODE_TEXT As Integer = 0
  Private Const TO_PRODUCT_CODE_TEXT As Integer = 1
  Private Const FROM_INVENTORY_CODE_TEXT As Integer = 2
  Private Const TO_INVENTORY_CODE_TEXT As Integer = 3

  Property connector As IIntegratedApplication = Nothing
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private m_SL_LMBClass As SL_LMB
  Private m_AMS1Class As AMS1
  Private m_loginString As String
  Private arrLotDetails(10) As String
  Private m_result As columnResult() = {New columnResult}
  Private m_SlipNoColumn As Integer
  Private m_DoSearch As Boolean = False
  Private m_currentString As String = ""
  Private m_currentItem As Integer
  Private m_currentRow As Integer = -1
  Private MasterDialog As SLMasterSearchDialog
  Private m_conf As Sunloft.PcaConfig
  Private location_date(6) As Point
  Private location_dgv(2) As Point
  Private count_date As Integer = 0
  Private dgv_size(2) As Size
  Private m_isKeyDowned As Boolean = False
  'set value condition in report
  Private m_PcaSelectedDate As String = ""
  Private f_PcaWarehousingDate As String = ""
  Private t_PcaWarehousingDate As String = ""
  Private f_PcaProduct As String = ""
  Private t_PcaProduct As String = ""
  Private m_PcaLotNo As String = ""
  Private f_PcaInventory As String = ""
  Private t_PcaInventory As String = ""
  Private f_PcaShukkaDate As String = ""
  Private t_PcaShukkaDate As String = ""
  Private f_PcaShiyouDate As String = ""
  Private t_PcaShiyouDate As String = ""
  Private f_PcaShoumiDate As String = ""
  Private t_PcaShoumiDate As String = ""
  Private f_PcaShouhiDate As String = ""
  Private t_PcaShouhiDate As String = ""
  Private m_PcaLotDetails As String = ""
  Private m_PcaLotDetailsContent As String = ""
  Public Sub New()
    Try
      InitializeComponent()

      m_appClass.ConnectToPCA()
      connector = m_appClass.connector

      If connector Is Nothing Then
        Dispose()
        Me.Close()
      End If


      m_conf = New Sunloft.PcaConfig
      m_loginString = m_appClass.EncryptedLoginString
      MasterDialog = New SLMasterSearchDialog(connector)

      m_SL_LMBClass = New SL_LMB(connector)
      m_SL_LMBClass.ReadOnlyRow()
      m_AMS1Class = New AMS1(connector)
      m_AMS1Class.ReadAll()

      arrLotDetails(0) = m_SL_LMBClass.sl_lmb_label1
      arrLotDetails(1) = m_SL_LMBClass.sl_lmb_label2
      arrLotDetails(2) = m_SL_LMBClass.sl_lmb_label3
      arrLotDetails(3) = m_SL_LMBClass.sl_lmb_label4
      arrLotDetails(4) = m_SL_LMBClass.sl_lmb_label5
      arrLotDetails(5) = m_SL_LMBClass.sl_lmb_label6
      arrLotDetails(6) = m_SL_LMBClass.sl_lmb_label7
      arrLotDetails(7) = m_SL_LMBClass.sl_lmb_label8
      arrLotDetails(8) = m_SL_LMBClass.sl_lmb_label9
      arrLotDetails(9) = m_SL_LMBClass.sl_lmb_label10

      location_date(0).X = 816
      location_date(0).Y = 71
      location_date(1).X = 816
      location_date(1).Y = 97
      location_date(2).X = 816
      location_date(2).Y = 123
      location_date(3).X = 816
      location_date(3).Y = 150
      location_date(4).X = 816
      location_date(4).Y = 176

      location_dgv(0).X = 10
      location_dgv(0).Y = 188
      location_dgv(1).X = 10
      location_dgv(1).Y = 214

      dgv_size(0).Width = 1203
      dgv_size(0).Height = 324
      dgv_size(1).Width = 1203
      dgv_size(1).Height = 298

      PcaLotDetails.Items.Add(" ")
      For intCount As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        PcaLotDetails.Items.Add(arrLotDetails(intCount))
      Next
      PcaLotDetails.SelectedIndex = 1
      'check 出荷期限	使用期限 賞味期限 消費期限
      If m_SL_LMBClass.sl_lmb_sdlflg = USE Then
        PcaShukkaDate.Visible = True
        PcaShukkaDate.Location = location_date(count_date)
        count_date = count_date + 1

      ElseIf m_SL_LMBClass.sl_lmb_sdlflg = NOT_USE Then
        count_date = 0
        PcaShukkaDate.Visible = False
      End If
      If m_SL_LMBClass.sl_lmb_ubdflg = USE Then
        PcaShiyouDate.Visible = True
        PcaShiyouDate.Location = location_date(count_date)
        count_date = count_date + 1

      ElseIf m_SL_LMBClass.sl_lmb_ubdflg = NOT_USE Then
        PcaShiyouDate.Visible = False
      End If
      If m_SL_LMBClass.sl_lmb_bbdflg = USE Then
        PcaShoumiDate.Visible = True
        PcaShoumiDate.Location = location_date(count_date)
        count_date = count_date + 1

      ElseIf m_SL_LMBClass.sl_lmb_bbdflg = NOT_USE Then
        PcaShoumiDate.Visible = False
      End If
      If m_SL_LMBClass.sl_lmb_expflg = USE Then
        PcaShouhiDate.Visible = True
        PcaShouhiDate.Location = location_date(count_date)
        count_date = count_date + 1

      ElseIf m_SL_LMBClass.sl_lmb_expflg = NOT_USE Then
        PcaShouhiDate.Visible = False
      End If
      If PcaLotDetails.Visible = False Then
        PcaLotDetails.Location = location_date(count_date)
        location_date(count_date).X = 900
        PcaLotDetailsContent.Location = location_date(count_date)
      End If
      If count_date = 3 Or count_date = 2 Or count_date = 1 Or count_date = 0 Then
        dgv.Location = location_dgv(0)
        dgv.Size = dgv_size(0)
      Else
        dgv.Location = location_dgv(1)
        dgv.Size = dgv_size(1)
      End If
      'Disable  command 
      PcaCommandItemPrint.Enabled = False
      PcaCommandItemExport.Enabled = False
      PcaCommandItemPreview.Enabled = False
      PcaCommandItemWarehousing.Enabled = False
      PcaCommandItemRefer.Enabled = False
      If dgv.Focused = False Then
        PcaCommandItemWarehousing.Enabled = False
      Else
        PcaCommandItemWarehousing.Enabled = True
      End If
      'check 入荷日 empty

      PcaWarehousingDate.FromIntDate = vbEmpty
      PcaWarehousingDate.ToIntDate = vbEmpty
      PcaShukkaDate.FromIntDate = vbEmpty
      PcaShukkaDate.ToIntDate = vbEmpty
      PcaShiyouDate.FromIntDate = vbEmpty
      PcaShiyouDate.ToIntDate = vbEmpty
      PcaShoumiDate.FromIntDate = vbEmpty
      PcaShoumiDate.ToIntDate = vbEmpty
      PcaShouhiDate.FromIntDate = vbEmpty
      PcaShouhiDate.ToIntDate = vbEmpty

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)

    InitTable(True)

    MyBase.OnLoad(e)

    'DoSearch()
  End Sub
#Region "InitTable"
  ''' <summary>
  '''  Set table and Alignment table 
  ''' </summary>
  ''' <param name="isEmpty">Clear search result array</param>
  ''' <remarks></remarks>
  Private Sub InitTable(Optional ByVal isEmpty As Boolean = False)
    Try
      dgv.Columns.Clear()

      initTableColumn(TEXT_TYPE, ColumnName.id, CInt(WIDTH_ID), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.warehousing, CInt(WIDTH_WAREHOUSING_DATE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.inventory, CInt(WIDTH_INVENTORY), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, ColumnName.productCode, CInt(WIDTH_PRODUCT_CODE), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, ColumnName.productName, CInt(WIDTH_PRODUCT_NAME), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, ColumnName.lotNo, CInt(WIDTH_LOT_NO), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, ColumnName.currentquantify, CInt(WIDTH_CURRENT_QUANTITY), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      initTableColumn(TEXT_TYPE, ColumnName.numberoffish, CInt(WIDTH_NUMBER_OF_FISH), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      initTableColumn(TEXT_TYPE, ColumnName.unitPrice, CInt(WIDTH_UNIT_PRICE), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      initTableColumn(TEXT_TYPE, ColumnName.amountofmoney, CInt(WIDTH_AMOUNT_OF_MONEY), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      initTableColumn(TEXT_TYPE, ColumnName.shukka, CInt(WIDTH_SHUKKA_DATE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.shiyou, CInt(WIDTH_SHIYOU_DATE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.shoumi, CInt(WIDTH_SHOUMI_DATE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.shouhi, CInt(WIDTH_SHOUHI_DATE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      'check 出荷期限	使用期限 賞味期限 消費期限
      If m_SL_LMBClass.sl_lmb_sdlflg = NOT_USE Then
        Me.dgv.Columns(ColumnName.shukka).Visible = False
      End If
      If m_SL_LMBClass.sl_lmb_ubdflg = NOT_USE Then
        Me.dgv.Columns(ColumnName.shiyou).Visible = False
      End If
      If m_SL_LMBClass.sl_lmb_bbdflg = NOT_USE Then
        Me.dgv.Columns(ColumnName.shoumi).Visible = False
      End If
      If m_SL_LMBClass.sl_lmb_expflg = NOT_USE Then
        Me.dgv.Columns(ColumnName.shouhi).Visible = False
      End If
      Me.dgv.Columns(ColumnName.id).Visible = False
      For intCount As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        initTableColumn(TEXT_TYPE, arrLotDetails(intCount), CInt(WIDTH_LOT_DETAILS), DataGridViewContentAlignment.MiddleLeft)
      Next

      'Set header cell font color to white
      For intCount As Integer = 0 To m_SlipNoColumn - 1
        dgv.Columns(intCount).HeaderCell.Style.ForeColor = Color.White
      Next
      'Set header size (Default size is a bit small)
      dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
      dgv.ColumnHeadersHeight = HEIGHT_HEADER

      If isEmpty Then
        m_result = Nothing 'Clear search result array
        dgv.Rows.Clear()
      End If

      'Set table not sortable
      For Each column As DataGridViewColumn In dgv.Columns
        column.SortMode = DataGridViewColumnSortMode.NotSortable
      Next

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  ''' <summary>
  ''' set alignment header
  ''' </summary>
  ''' <param name="headerType">Header Type</param>
  ''' <param name="headerText"> Header Text</param>
  ''' <param name="columnwidth"> Width of column</param>
  ''' <param name="columnname">Name of column</param>
  ''' <param name="isReadonly">Readonly</param>
  ''' <remarks></remarks>
  Private Sub initTableColumn(ByVal headerType As String, ByVal headerText As String, ByVal columnwidth As Integer, Optional ByVal alignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft, Optional ByVal columnname As String = "", Optional ByVal isReadonly As Boolean = True, Optional ByVal headerAlignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft)
    Dim col As New DataGridViewColumn
    Select Case headerType
      Case NUMBER_TYPE
        col = New DataGridViewTextBoxColumn
      Case TEXT_TYPE
        col = New DataGridViewTextBoxColumn
      Case Else
        col = New DataGridViewTextBoxColumn
    End Select
    col.ReadOnly = isReadonly
    If columnname = "" Then
      col.Name = headerText
    Else
      col.Name = columnname
    End If
    col.Width = columnwidth
    col.DefaultCellStyle.Alignment = alignment
    col.HeaderCell.Style.Alignment = headerAlignment
    col.DefaultCellStyle.FormatProvider = CultureInfo.CreateSpecificCulture("ja-JP")
    dgv.Columns.Add(col)
  End Sub
#End Region
#Region "Validate "
#Region "Event ProductCode"
  Private Sub PcaProduct_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaProduct.Leave
    PcaCommandItemRefer.Enabled = False
  End Sub
  Private Sub PcaProduct_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaProduct.ToCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = PcaProduct.FromCodeText
    m_currentItem = TO_PRODUCT_CODE_TEXT
  End Sub

  Private Sub PcaProduct_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaProduct.FromCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_DoSearch = False
    m_currentString = PcaProduct.ToCodeText
    m_currentItem = FROM_PRODUCT_CODE_TEXT
  End Sub
  Private Sub frmLotInput_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then
      connector.LogOffSystem()
    End If
  End Sub

  Private Sub PcaProduct_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaProduct.ClickFromReferButton2
    m_currentItem = FROM_PRODUCT_CODE_TEXT
    m_currentString = PcaProduct.FromCodeText
    DisplayReferScreen(e, FROM_PRODUCT_CODE_TEXT)
  End Sub

  Private Sub PcaProduct_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaProduct.ClickToReferButton2
    m_currentItem = TO_PRODUCT_CODE_TEXT
    m_currentString = PcaProduct.ToCodeText
    DisplayReferScreen(e, TO_PRODUCT_CODE_TEXT)
  End Sub

  Private Sub PcaProduct_ToCodeTextValidated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaProduct.ToCodeTextValidated
  End Sub

  Private Sub PcaProduct_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaProduct.ToCodeTextValidating2
    m_currentItem = TO_PRODUCT_CODE_TEXT
    validateManagement(e, PcaProduct.ToCodeText)
  End Sub

  Private Sub PcaProduct_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaProduct.FromCodeTextValidating2
    m_currentItem = FROM_PRODUCT_CODE_TEXT
    validateManagement(e, PcaProduct.FromCodeText)
  End Sub

#End Region
#Region "Event Inventory"
  Private Sub PcaInventory_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaInventory.ToCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = PcaInventory.FromCodeText
    m_currentItem = TO_INVENTORY_CODE_TEXT
  End Sub

  Private Sub PcaInventory_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaInventory.FromCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = PcaInventory.FromCodeText
    m_currentItem = FROM_INVENTORY_CODE_TEXT
  End Sub

  Private Sub PcaInventory_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaInventory.Leave
    PcaCommandItemRefer.Enabled = False
  End Sub
  Private Sub PcaInventory_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaInventory.ToCodeTextValidating2
    validateManagement(e, PcaInventory.ToCodeText)
  End Sub

  Private Sub PcaInventory_ToCodeTextValidated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaInventory.ToCodeTextValidated
    'If m_DoSearch Then
    '    m_DoSearch = False
    '    DoSearch()
    'End If
  End Sub

  Private Sub PcaInventory_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaInventory.ClickFromReferButton2
    m_currentItem = FROM_INVENTORY_CODE_TEXT
    m_currentString = PcaInventory.FromCodeText
    DisplayReferScreen(e, FROM_INVENTORY_CODE_TEXT)
  End Sub

  Private Sub PcaInventory_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaInventory.ClickToReferButton2
    m_currentItem = TO_INVENTORY_CODE_TEXT
    m_currentString = PcaInventory.ToCodeText
    DisplayReferScreen(e, TO_INVENTORY_CODE_TEXT)
  End Sub

  Private Sub PcaInventory_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaInventory.FromCodeTextValidating2
    validateManagement(e, PcaInventory.FromCodeText)
  End Sub

#End Region
  '***********************************************************																														
  'Name          : validateManagement
  'Content       : Event called when Product Code inventory (From, To Leave event)
  'Return Value  : 
  'Argument      : e As PCA.Controls.ValidatingEventArgs, CodeText As String
  'Created date  : 2016/03/12  by DuyTN
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub validateManagement(ByVal e As PCA.Controls.ValidatingEventArgs, ByRef CodeText As String)
    If m_currentItem = FROM_PRODUCT_CODE_TEXT Or m_currentItem = TO_PRODUCT_CODE_TEXT Then
      Try
        If CodeText.TrimEnd().Length > m_AMS1Class.ams1_ProductLength Then
          e.Cancel = True
          CodeText = m_currentString 'MasterDialog.ModifiedEntity.Header.TorihikisakiCode
          Return
        End If
        Dim curProductCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_ProductLength)
        If Not String.IsNullOrEmpty(curProductCode) AndAlso m_currentString = curProductCode Then
          CodeText = curProductCode
          'No changes
          Return
        End If
        If String.IsNullOrEmpty(curProductCode) Then
          Return
        End If

        'validate supplier code
        Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms = MasterDialog.FindBEMasterSms(curProductCode)

        If Not beMasterSms.SyohinCode.Length > 0 Then
          'If there's error
          e.Cancel = True
          CodeText = m_currentString 'MasterDialog.ModifiedEntity.Header.TorihikisakiCode
          Return
        Else
          CodeText = curProductCode
        End If

      Catch ex As Exception
        DisplayBox.ShowCritical(ex)
      End Try
    ElseIf m_currentItem = FROM_INVENTORY_CODE_TEXT Or m_currentItem = TO_INVENTORY_CODE_TEXT Then
      Try
        If CodeText.TrimEnd().Length > m_AMS1Class.ams1_WarehouseLength Then
          e.Cancel = True
          CodeText = m_currentString 'MasterDialog.ModifiedEntity.Header.TorihikisakiCode
          Return
        End If
        Dim curWarehouseCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_WarehouseLength)
        If Not String.IsNullOrEmpty(curWarehouseCode) AndAlso m_currentString = curWarehouseCode Then
          CodeText = curWarehouseCode
          'No changes
          Return
        End If
        If String.IsNullOrEmpty(curWarehouseCode) Then
          Return
        End If

        'validate supplier code
        Dim beMasterEms As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(curWarehouseCode)
        If Not beMasterEms.SokoCode.Length > 0 Then
          'If there's error
          e.Cancel = True
          CodeText = m_currentString 'MasterDialog.ModifiedEntity.Header.TorihikisakiCode
          Return
        Else
          CodeText = curWarehouseCode
        End If

      Catch ex As Exception
        DisplayBox.ShowCritical(ex)
      End Try
    End If
  End Sub

  '***********************************************************																														
  'Name          : DisplayReferScreen
  'Content       : Display refer screen when the user selects from CodeSet field
  'Return Value  : 
  'Argument      : 
  'Created date  : 2016/03/21  DuyTN
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub DisplayReferScreen(ByVal e As System.ComponentModel.CancelEventArgs, ByRef Item As Integer)
    If m_currentItem = FROM_PRODUCT_CODE_TEXT Or m_currentItem = TO_PRODUCT_CODE_TEXT Then
      Dim newCode As String = String.Empty
      Dim location As Point = PCA.TSC.Kon.Tools.ControlTool.GetDialogLocation(PcaProduct.FromReferButton)
      newCode = MasterDialog.ShowReferSmsDialog(If(Item = FROM_PRODUCT_CODE_TEXT, PcaProduct.FromCodeText, PcaProduct.ToCodeText), location)
      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
        If m_currentString = newCode Then
          'No changes
          Return
        End If
        Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms = MasterDialog.FindBEMasterSms(newCode)
        If Not beMasterSms.SyohinCode.Length > 0 Then
          'Validate failed
          If Not e Is Nothing Then e.Cancel = True
          If Item = FROM_PRODUCT_CODE_TEXT Then
            PcaProduct.FromCodeText = m_currentString
          Else
            PcaProduct.ToCodeText = m_currentString
          End If
          Return
        End If
        If Item = FROM_PRODUCT_CODE_TEXT Then
          PcaProduct.FromCodeText = newCode
        Else
          PcaProduct.ToCodeText = newCode
        End If
      End If
    ElseIf m_currentItem = FROM_INVENTORY_CODE_TEXT Or m_currentItem = TO_INVENTORY_CODE_TEXT Then
      Dim newCode As String = String.Empty
      Dim location As Point = PCA.TSC.Kon.Tools.ControlTool.GetDialogLocation(PcaInventory.FromReferButton)
      newCode = MasterDialog.ShowReferSokoDialog(If(Item = FROM_INVENTORY_CODE_TEXT, PcaInventory.FromCodeText, PcaInventory.ToCodeText), location)
      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
        If m_currentString = newCode Then
          'No changes
          Return
        End If
        Dim beMasterEms As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(newCode)
        If Not beMasterEms.SokoCode.Length > 0 Then
          'Validate failed
          If Not e Is Nothing Then e.Cancel = True
          If Item = FROM_INVENTORY_CODE_TEXT Then
            PcaInventory.FromCodeText = m_currentString
          Else
            PcaInventory.ToCodeText = m_currentString
          End If
          Return
        End If
        If Item = FROM_INVENTORY_CODE_TEXT Then
          PcaInventory.FromCodeText = newCode
        Else
          PcaInventory.ToCodeText = newCode
        End If
      End If
    End If
  End Sub
#End Region
#Region " Search"
  Private Sub Search()

    GetSearchResult()
    If Not m_result Is Nothing Then DisplaySearchResult()

  End Sub

  '***********************************************************																														
  'Name          : GetSearchResult
  'Content       : Event called when the user clicks search button
  '                make a query into database to get the result
  'Return Value  : 
  'Argument      : 
  'Created date  : 2016/04/02  by DuyTN
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub GetSearchResult()
    Try
      PcaSelectedDate.Focus()
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim SelectDate As Integer = PcaSelectedDate.IntDate
      Dim fromWarehousing As Integer = PcaWarehousingDate.FromIntDate
      Dim toWarehousing As Integer = PcaWarehousingDate.ToIntDate
      Dim fromProduct As String = PcaProduct.FromCodeText
      Dim toProduct As String = PcaProduct.ToCodeText
      Dim LotNo As String = PcaLotNo.Text
      Dim fromInventory As String = PcaInventory.FromCodeText
      Dim toInventory As String = PcaInventory.ToCodeText
      Dim fromShukka As Integer = PcaShukkaDate.FromIntDate
      Dim toShukka As Integer = PcaShukkaDate.ToIntDate
      Dim fromShiyou As Integer = PcaShiyouDate.FromIntDate
      Dim toShiyou As Integer = PcaShiyouDate.ToIntDate
      Dim fromShoumi As Integer = PcaShoumiDate.FromIntDate
      Dim toShoumi As Integer = PcaShoumiDate.ToIntDate
      Dim fromShouhi As Integer = PcaShouhiDate.FromIntDate
      Dim toShouhi As Integer = PcaShouhiDate.ToIntDate
      Dim LostDetails As String = CStr(CDbl(PcaLotDetails.SelectedIndex.ToString) - 1)
      Dim LotDetailsContent As String = PcaLotDetailsContent.Text
      Dim intCount As Integer = -1

      If m_result IsNot Nothing Then Array.Clear(m_result, 0, m_result.Length)

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKD_Pro04060")
      selectCommand.Parameters("@fromWarehousing").SetValue(fromWarehousing)
      selectCommand.Parameters("@toWarehousing").SetValue(toWarehousing)
      selectCommand.Parameters("@fromProduct").SetValue(fromProduct)
      selectCommand.Parameters("@toProduct").SetValue(toProduct)
      selectCommand.Parameters("@fromShukka").SetValue(fromShukka)
      selectCommand.Parameters("@toShukka").SetValue(toShukka)
      selectCommand.Parameters("@fromShiyou").SetValue(fromShiyou)
      selectCommand.Parameters("@toShiyou").SetValue(toShiyou)
      selectCommand.Parameters("@fromShoumi").SetValue(fromShoumi)
      selectCommand.Parameters("@toShoumi").SetValue(toShoumi)
      selectCommand.Parameters("@fromShouhi").SetValue(fromShouhi)
      selectCommand.Parameters("@toShouhi").SetValue(toShouhi)
      selectCommand.Parameters("@SelectDate").SetValue(SelectDate)
      selectCommand.Parameters("@LotNo").SetValue(LotNo)
      selectCommand.Parameters("@fromInventory").SetValue(fromInventory)
      selectCommand.Parameters("@toInventory").SetValue(toInventory)
      selectCommand.Parameters("@LostDetails").SetValue(LostDetails)
      selectCommand.Parameters("@LotDetailsContent").SetValue(LotDetailsContent)

      reader = selectCommand.ExecuteReader

      While reader.Read() = True
        intCount = intCount + 1
        ReDim Preserve m_result(intCount)
        m_result(intCount) = New columnResult
        m_result(intCount).id = reader.GetValue("sl_zdn_id").ToString()
        m_result(intCount).warehousing = reader.GetValue("sl_zdn_uribi").ToString()
        If CDbl(reader.GetValue("sl_zdn_uribi").ToString()) > 0 Then
          m_result(intCount).warehousing = Date.ParseExact(CStr(reader.GetValue("sl_zdn_uribi").ToString()), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If

        m_result(intCount).inventory = reader.GetValue("ems_str").ToString.Trim
        m_result(intCount).productCode = reader.GetValue("sl_zdn_scd").ToString()
        m_result(intCount).productName = reader.GetValue("sms_mei").ToString.TrimEnd() 'reader.GetValue("sl_sykd_mei").ToString.TrimEnd()
        m_result(intCount).lotNo = reader.GetValue("sl_zdn_ulotno").ToString()
        m_result(intCount).currentquantity = CDec(reader.GetValue("sl_zdn_suryo").ToString())
        m_result(intCount).numberoffish = CInt(CStr(CInt(reader.GetValue("sl_zdn_honsu").ToString())))
        m_result(intCount).unitPrice = CDec(reader.GetValue("sl_zdn_tanka").ToString())
        m_result(intCount).amountofmoney = CDec((m_result(intCount).currentquantity) * CDbl(m_result(intCount).unitPrice))

        m_result(intCount).shukka = reader.GetValue("sl_zdn_sdldate").ToString()
        m_result(intCount).shiyou = reader.GetValue("sl_zdn_ubdate").ToString()
        m_result(intCount).shoumi = reader.GetValue("sl_zdn_bbdate").ToString()
        m_result(intCount).shouhi = reader.GetValue("sl_zdn_expdate").ToString()

        ConvertDate(m_result(intCount).shukka)
        ConvertDate(m_result(intCount).shiyou)
        ConvertDate(m_result(intCount).shoumi)
        ConvertDate(m_result(intCount).shouhi)
        m_result(intCount).currentquantityNoOfDigit = CInt(reader.GetValue("sms_sketa"))
        m_result(intCount).unitPriceNoOfDigit = CInt(reader.GetValue("sms_tketa"))
        For intCountLotDetails As Integer = 1 To m_SL_LMBClass.sl_lmb_maxlotop
          m_result(intCount).Lotdetails(intCountLotDetails - 1) = reader.GetValue("sl_zdn_ldmei" & intCountLotDetails.ToString).ToString()
        Next
        'If reader.GetValue("sl_zdn_id").ToString = "291" Then
        '  MsgBox(reader.GetValue("sl_zdn_ldid1").ToString & " ID: " & reader.GetValue("sl_zdn_id").ToString)
        'End If
      End While
      reader.Close()
      reader.Dispose()

      'set Pca Command when search
      If Not m_result Is Nothing AndAlso Not m_result(0) Is Nothing Then
        PcaCommandItemPrint.Enabled = True
        PcaCommandItemExport.Enabled = True
        PcaCommandItemPreview.Enabled = True
        PcaCommandItemWarehousing.Enabled = True
        ' PcaCommandItemRefer.Enabled = True

        'set value condition when condition change
        m_PcaSelectedDate = Date.ParseExact(CStr(SelectDate), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        If fromWarehousing > 0 Then
          f_PcaWarehousingDate = Date.ParseExact(CStr(fromWarehousing), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        If toWarehousing > 0 Then
          t_PcaWarehousingDate = Date.ParseExact(CStr(toWarehousing), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        If fromShukka > 0 Then
          f_PcaShukkaDate = Date.ParseExact(CStr(fromShukka), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        If toShukka > 0 Then
          t_PcaShukkaDate = Date.ParseExact(CStr(toShukka), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        If fromShiyou > 0 Then
          f_PcaShiyouDate = Date.ParseExact(CStr(fromShiyou), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        If toShiyou > 0 Then
          t_PcaShiyouDate = Date.ParseExact(CStr(toShiyou), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        If fromShoumi > 0 Then
          f_PcaShoumiDate = Date.ParseExact(CStr(fromShoumi), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        If toShoumi > 0 Then
          t_PcaShoumiDate = Date.ParseExact(CStr(toShoumi), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        If fromShouhi > 0 Then
          f_PcaShouhiDate = Date.ParseExact(CStr(fromShouhi), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        If toShouhi > 0 Then
          t_PcaShouhiDate = Date.ParseExact(CStr(toShouhi), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If
        f_PcaProduct = fromProduct
        t_PcaProduct = toProduct
        m_PcaLotNo = LotNo
        f_PcaInventory = fromInventory
        t_PcaInventory = toInventory
        If CDbl(LostDetails) > -1 Then
          m_PcaLotDetails = PcaLotDetails.SelectedItem.ToString
        End If
        m_PcaLotDetailsContent = LotDetailsContent
        If LotDetailsContent = "" Then
          m_PcaLotDetails = ""
        End If
      Else
        PcaCommandItemPrint.Enabled = False
        PcaCommandItemExport.Enabled = False
        PcaCommandItemPreview.Enabled = False
        PcaCommandItemWarehousing.Enabled = False
        PcaCommandItemRefer.Enabled = False
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSearch.CommandId)
      End If
      m_currentRow = -1
      dgv.Focus()
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub
  Private Function ConvertDate(ByRef varString As String) As Boolean
    Dim result As Integer
    If Not varString Is Nothing And varString.Length = 8 And Integer.TryParse(varString, result) Then
      varString = Date.ParseExact(varString, "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
      Return True
    End If
    If varString.ToString = "" Then
      varString = ""
      Return True
    End If
    If CDbl(varString) = 0 Then
      varString = ""
      Return True
    End If
    Return False
  End Function
  '***********************************************************																														
  'Name          : DisplaySearchResult
  'Content       : Display search result into table
  'Return Value  : 
  'Argument      : 
  'Created date  : 2016/04/02  by DuyTN
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub DisplaySearchResult()
    Try
      dgv.Columns.Clear()
      InitTable()

      For intCount As Integer = 0 To m_result.Length - 1
        If Not SLCmnFunction.checkObjectNothingEmpty(m_result(intCount)) Then
          dgv.Rows.Add(New DataGridViewRow)

          dgv.Rows.Item(intCount).Cells.Item(ID_DATE_COLUMN).Value = m_result(intCount).id
          dgv.Rows.Item(intCount).Cells.Item(WAREHOUSING_DATE_COLUMN).Value = m_result(intCount).warehousing
          dgv.Rows.Item(intCount).Cells.Item(INVENTORY_COLUMN).Value = m_result(intCount).inventory
          dgv.Rows.Item(intCount).Cells.Item(PRODUCT_CODE_COLUMN).Value = m_result(intCount).productCode
          dgv.Rows.Item(intCount).Cells.Item(PRODUCT_NAME_COLUMN).Value = m_result(intCount).productName
          dgv.Rows.Item(intCount).Cells.Item(LOT_NO_COLUMN).Value = m_result(intCount).lotNo
          dgv.Rows.Item(intCount).Cells.Item(CURRENT_QUANTITY_COLUMN).Value = SLCmnFunction.formatNumberOfDigit(CDec(m_result(intCount).currentquantity), m_result(intCount).currentquantityNoOfDigit)
          dgv.Rows.Item(intCount).Cells.Item(NUMBER_OF_FISH_COLUMN).Value = SLCmnFunction.formatNumberOfDigit(CDec(m_result(intCount).numberoffish))
          dgv.Rows.Item(intCount).Cells.Item(UNIT_PRICE_COLUMN).Value = SLCmnFunction.formatNumberOfDigit(CDec(m_result(intCount).unitPrice), m_result(intCount).unitPriceNoOfDigit)
          dgv.Rows.Item(intCount).Cells.Item(AMOUNT_OF_MONEY_COLUMN).Value = SLCmnFunction.formatNumberOfDigit(m_result(intCount).amountofmoney)
          dgv.Rows.Item(intCount).Cells.Item(SHUKKA_DATE_COLUMN).Value = m_result(intCount).shukka
          dgv.Rows.Item(intCount).Cells.Item(SHIYOU_DATE_COLUMN).Value = m_result(intCount).shiyou
          dgv.Rows.Item(intCount).Cells.Item(SHOUMI_DATE_COLUMN).Value = m_result(intCount).shoumi
          dgv.Rows.Item(intCount).Cells.Item(SHOUHI_DATE_COLUMN).Value = m_result(intCount).shouhi

          For intCountLotDetails As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
            dgv.Rows.Item(intCount).Cells.Item(ROW_COLUMN + intCountLotDetails).Value = m_result(intCount).Lotdetails(intCountLotDetails)
          Next
        End If
      Next

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub
#End Region
#Region "PCA Command Manager"
  Private Sub PcaCommandManager1_Command(ByVal sender As System.Object, ByVal e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Dim commandItem As PCA.Controls.PcaCommandItem = e.CommandItem
    Select Case True
      Case commandItem Is PcaCommandItemHelp 'F1
        'do nothing
      Case commandItem Is PcaCommandItemSearch 'F5
        Search()
      Case commandItem Is PcaCommandItemRefer 'F8
        If m_isKeyDowned Then m_isKeyDowned = False : Exit Sub
        DisplayReferScreen(Nothing, m_currentItem)
      Case commandItem Is PcaCommandItemWarehousing 'F7
        If m_currentRow = -1 OrElse dgv.Rows(m_currentRow).Cells(ID_DATE_COLUMN).Value Is Nothing Then
          Return
        End If
        SLCmnFunction.StartUpInputExe(IO.Path.GetDirectoryName(Application.ExecutablePath) & PROGRAM_TO_CALL, m_loginString, dgv.Rows(m_currentRow).Cells(ID_DATE_COLUMN).Value.ToString)
      Case commandItem Is PcaCommandItemPrint 'F9
        'print data
        If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.productCode, 0)) Then
          If MsgBox(RPT_TITLE & "を" & SLConstants.ConfirmMessage.PRINT_CONFIRM_MESSAGE, MsgBoxStyle.OkCancel, SLConstants.NotifyMessage.TITLE_MESSAGE) = MsgBoxResult.Ok Then
            PrintData()
          End If
        End If
      Case commandItem Is PcaCommandItemPreview 'Shift + F9
        PrintData(True)
      Case commandItem Is PcaCommandItemExport 'F10
        'do export
        If OutPutCSV() Then
          SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemExport.CommandId)
        End If
      Case commandItem Is PcaCommandItemClose 'F12
        Me.Close()
    End Select
  End Sub
  Private Sub frmTransLotList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
    If e.KeyCode = Keys.F8 Then m_isKeyDowned = True
  End Sub
#End Region
#Region "Print/Preview"
  ''' <summary>
  ''' print data 
  ''' </summary>
  ''' <param name="isPreview">is print or is priview</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function PrintData(Optional ByVal isPreview As Boolean = False) As Boolean
    Try
      Dim ReportName As String = RPT_FILENAME
      Dim ReportTitle As String = RPT_TITLE
      Dim rpt As New Sunloft.Windows.Forms.SLCrystalReport
      Dim RpParam As New Dictionary(Of String, Object)
      Dim editedDataTable As DataTable = Nothing

      rpt.ReportFile = IO.Path.GetDirectoryName(Application.ExecutablePath) & "\" & ReportName
      EditPrintData(editedDataTable)
      rpt.DataSource = editedDataTable
      ' 出力開始
      If isPreview Then
        rpt.Preview(ReportTitle, True)
      Else
        rpt.PrintToPrinter()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

    Return True
  End Function
  ''' <summary>
  ''' prepare data for print and priview
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EditPrintData(ByRef argEditedData As DataTable) As Boolean
    Dim editData As New Pro04060.DataSet1.pro04060DataTable
    Dim editRow As DataRow
    Dim intLpc As Integer = 0

    Try
      While intLpc < dgv.Rows.Count AndAlso Not dgv(ColumnName.productCode, intLpc) Is Nothing 'SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.productCode, intLpc))
        With dgv
          editRow = editData.NewRow
          editRow.Item("Condition1") = "在庫日付: " & m_PcaSelectedDate.ToString
          editRow.Item("Condition2") = "抽出条件: " & Condition(f_PcaWarehousingDate.ToString, t_PcaWarehousingDate.ToString, PcaWarehousingDate.RangeGroupBoxText) _
                                      & Condition(f_PcaProduct.ToString, t_PcaProduct.ToString, PcaProduct.RangeGroupBoxText) _
                                      & ConditionText(m_PcaLotNo, PcaLotNo.LabelText) _
                                      & Condition(f_PcaInventory.ToString, t_PcaInventory.ToString, PcaInventory.RangeGroupBoxText) _
                                      & Condition(f_PcaShukkaDate.ToString, t_PcaShukkaDate.ToString, PcaShukkaDate.RangeGroupBoxText) _
                                      & Condition(f_PcaShiyouDate.ToString, t_PcaShiyouDate.ToString, PcaShiyouDate.RangeGroupBoxText) _
                                      & Condition(f_PcaShoumiDate.ToString, t_PcaShoumiDate.ToString, PcaShoumiDate.RangeGroupBoxText) _
                                      & Condition(f_PcaShouhiDate.ToString, t_PcaShouhiDate.ToString, PcaShouhiDate.RangeGroupBoxText) _
                                      & ConditionText(m_PcaLotDetails, PcaLotDetailsContent.Text)

          editRow.Item("warehousing") = dgv(ColumnName.warehousing, intLpc).Value
          editRow.Item("inventory") = dgv(ColumnName.inventory, intLpc).Value
          'editRow.Item("productCode") = dgv(ColumnName.productCode, intLpc).Value
          'editRow.Item("productName") = dgv(ColumnName.productName, intLpc).Value.ToString
          editRow.Item("lotNo") = dgv(ColumnName.lotNo, intLpc).Value
          editRow.Item("currentquantify") = dgv(ColumnName.currentquantify, intLpc).Value
          editRow.Item("numberoffish") = dgv(ColumnName.numberoffish, intLpc).Value
          editRow.Item("unitPrice") = dgv(ColumnName.unitPrice, intLpc).Value
          editRow.Item("amountofmoney") = dgv(ColumnName.amountofmoney, intLpc).Value
          editRow.Item("shukka") = dgv(ColumnName.shukka, intLpc).Value
          editRow.Item("shiyou") = dgv(ColumnName.shiyou, intLpc).Value
          editRow.Item("shoumi") = dgv(ColumnName.shoumi, intLpc).Value
          editRow.Item("shouhi") = dgv(ColumnName.shouhi, intLpc).Value
          editRow.Item("Product") = Sunloft.Common.SLCmnFunction.NewLineManager(dgv(ColumnName.productCode, intLpc).Value.ToString & " " & dgv(ColumnName.productName, intLpc).Value.ToString, REPORT_PRODUCTNAME_LENGTH)

          editData.Rows.Add(editRow)
        End With
        intLpc = intLpc + 1
      End While

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
    argEditedData = editData
    Return True
  End Function
  Protected Function Condition(ByRef fromString As String, ByRef toString As String, ByRef nameString As String) As String
    Dim strString As String = ""
    If Not (fromString = "" AndAlso toString = "") Then
      strString = nameString & " : " & fromString & " ～ " & toString & " "
    ElseIf fromString <> "" Then

      strString = nameString & " : " & fromString & " ～ " & " "
    ElseIf toString <> "" Then

      strString = nameString & " : " & " ～ " & toString & " "
    End If
    Return strString
  End Function
  Protected Function ConditionText(ByRef varString As String, ByRef nameString As String) As String
    Dim strString As String = ""
    If Not varString = "" Then
      strString = nameString & " : " & varString & " "
    End If
    Return strString
  End Function
  ''' <summary>
  ''' export file CSV
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function OutPutCSV() As Boolean
    Dim strSaveFileName As String = String.Empty
    Dim strColumns() As String
    Dim sbCsv As New System.Text.StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .FileName = CSV_FILE_NAME
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        .OverwritePrompt = True

        Dim res As System.Windows.Forms.DialogResult = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False

        strSaveFileName = .FileName

      End With

      strColumns = {ColumnName.warehousing, ColumnName.inventory, ColumnName.productCode, ColumnName.productName, ColumnName.lotNo, _
                    ColumnName.currentquantify, ColumnName.numberoffish, ColumnName.unitPrice, ColumnName.amountofmoney, ColumnName.shukka, ColumnName.shiyou, ColumnName.shoumi, ColumnName.shouhi}
      For intCountLotDetails As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        ReDim Preserve strColumns(strColumns.Length)
        strColumns(strColumns.Length - 1) = arrLotDetails(intCountLotDetails)
      Next

      SLCmnFunction.OutPutCSVFromDataGridView(strColumns, dgv, strSaveFileName, dgv.RowCount)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function
#End Region
  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
    '
    Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
    Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子

    If (keyCode = Keys.Tab OrElse keyCode = Keys.Enter) Then
      ' If (keyCode = Keys.Enter) Then
      'Validate if user press Enter/Tab +  Anykey except Shift, →, ↓
      m_DoSearch = True
    Else
      m_DoSearch = False
    End If

    Return MyBase.ProcessDialogKey(keyData)

  End Function
  '***********************************************************																														
  'Name          : dgv_CellDoubleClick
  'Content       : Display lot detail when the user double clicks on a row in the table
  'Return Value  : 
  'Argument      : sender As System.Object, args As System.Windows.Forms.DataGridViewCellEventArgs
  'Created date  : 2016/04/03  by Duytn
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub dgv_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellDoubleClick
    If e.RowIndex = -1 OrElse dgv.Rows(e.RowIndex).Cells(ID_DATE_COLUMN).Value Is Nothing Then
      Return
    End If
    SLCmnFunction.StartUpInputExe(IO.Path.GetDirectoryName(Application.ExecutablePath) & PROGRAM_TO_CALL, m_loginString, dgv.Rows(e.RowIndex).Cells(ID_DATE_COLUMN).Value.ToString)
  End Sub

  Private Sub dgv_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
    m_currentRow = e.RowIndex
  End Sub

  Private Sub dgv_CellEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEnter
    m_currentRow = e.RowIndex
  End Sub

  Private Sub PcaLotDetailsContent_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaLotDetailsContent.Validated
    If m_DoSearch Then
      m_DoSearch = False
      Search()
    End If
  End Sub

  Private Sub dgv_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgv.Leave
    PcaCommandItemWarehousing.Enabled = False
  End Sub

  Private Sub dgv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgv.Click
    PcaCommandItemWarehousing.Enabled = True
  End Sub

End Class
'***********************************************************																														
'Name          : columnName
'Content       : This class defines header properties for the table that will be used do display result search
'Return Value  : 
'Created date  : 2016/03/26  Duytn
'Modified date :        by       Content: 																											
'***********************************************************
Public Class ColumnName
  Public Const id As String = "ID"
  Public Const warehousing As String = "入荷日"
  Public Const inventory As String = "倉庫"
  Public Const productCode As String = "商品コード"
  Public Const productName As String = "商品名"
  Public Const lotNo As String = "ロットNo"
  Public Const currentquantify As String = "在庫数量"
  Public Const numberoffish As String = "本数"
  Public Const unitPrice As String = "単価"
  Public Const amountofmoney As String = "金額"
  Public Const shukka As String = "出荷期限"
  Public Const shiyou As String = "使用期限"
  Public Const shoumi As String = "賞味期限"
  Public Const shouhi As String = "消費期限"
  Public Const lotdetails As String = "ロット詳細情報"
End Class
Public Class columnResult
  Public Property id As String = ""
  Public Property warehousing As String = ""
  Public Property inventory As String = ""
  Public Property productCode As String = ""
  Public Property productName As String = ""
  Public Property lotNo As String = ""
  Public Property currentquantity As Decimal
  Public Property numberoffish As Integer
  Public Property unitPrice As Decimal
  Public Property amountofmoney As Decimal
  Public Property shukka As String = ""
  Public Property shiyou As String = ""
  Public Property shoumi As String = ""
  Public Property shouhi As String = ""
  Public Property currentquantityNoOfDigit As Integer
  Public Property unitPriceNoOfDigit As Integer
  Public Property amountofmoneyNoOfDigit As Integer
  Public Property Lotdetails() As String() = New String(10) {}
End Class
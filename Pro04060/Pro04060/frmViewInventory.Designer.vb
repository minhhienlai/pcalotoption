﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewInventory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewInventory))
    Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.FToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPreview = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemExport = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemReference = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPrint = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPreview = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonExport = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSearch = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandWarehousing = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrint = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPreview = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandExport = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemPrint = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPreview = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemExport = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemWarehousing = New PCA.Controls.PcaCommandItem()
    Me.PcaLotNo = New PCA.Controls.PcaLabeledTextBox()
    Me.PcaProduct = New PCA.Controls.PcaRangeCodeSet()
    Me.PcaWarehousingDate = New PCA.Controls.PcaRangeDate()
    Me.PcaSelectedDate = New PCA.Controls.PcaLabeledDate()
    Me.PcaInventory = New PCA.Controls.PcaRangeCodeSet()
    Me.PcaShukkaDate = New PCA.Controls.PcaRangeDate()
    Me.PcaShiyouDate = New PCA.Controls.PcaRangeDate()
    Me.PcaShoumiDate = New PCA.Controls.PcaRangeDate()
    Me.dgv = New System.Windows.Forms.DataGridView()
    Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.PcaLotDetailsContent = New PCA.Controls.PcaTextBox()
    Me.PcaLotDetails = New PCA.Controls.PcaComboBox()
    Me.PcaShouhiDate = New PCA.Controls.PcaRangeDate()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(1225, 24)
    Me.MenuStrip1.TabIndex = 4
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'FToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.FToolStripMenuItemFile, Nothing)
    Me.FToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemPrint, Me.ToolStripMenuItemPreview, Me.ToolStripMenuItemExport, Me.ToolStripMenuItemClose})
    Me.FToolStripMenuItemFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.FToolStripMenuItemFile.Name = "FToolStripMenuItemFile"
    Me.FToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.FToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPrint, Me.PcaCommandItemPrint)
    Me.ToolStripMenuItemPrint.Name = "ToolStripMenuItemPrint"
    Me.ToolStripMenuItemPrint.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPrint.Text = "印刷(&P)"
    '
    'ToolStripMenuItemPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPreview, Me.PcaCommandItemPreview)
    Me.ToolStripMenuItemPreview.Name = "ToolStripMenuItemPreview"
    Me.ToolStripMenuItemPreview.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPreview.Text = "印刷プレビュー(&V)"
    '
    'ToolStripMenuItemExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemExport, Me.PcaCommandItemExport)
    Me.ToolStripMenuItemExport.Name = "ToolStripMenuItemExport"
    Me.ToolStripMenuItemExport.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemExport.Text = "出力(&O)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSearch, Me.ToolStripMenuItemReference})
    Me.ToolStripMenuItemEdit.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSearch.Text = "再表示(&R)"
    '
    'ToolStripMenuItemReference
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemReference, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemReference.Name = "ToolStripMenuItemReference"
    Me.ToolStripMenuItemReference.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemReference.Text = "参照(&U)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Nothing)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContent.Text = "目次(&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonPrint, Me.ToolStripButtonPreview, Me.ToolStripButtonExport, Me.ToolStripButtonSearch, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1225, 37)
    Me.ToolStrip1.TabIndex = 5
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPrint, Me.PcaCommandItemPrint)
    Me.ToolStripButtonPrint.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonPrint.Image = CType(resources.GetObject("ToolStripButtonPrint.Image"), System.Drawing.Image)
    Me.ToolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPrint.Name = "ToolStripButtonPrint"
    Me.ToolStripButtonPrint.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonPrint.Text = "印刷"
    Me.ToolStripButtonPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPreview, Me.PcaCommandItemPreview)
    Me.ToolStripButtonPreview.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonPreview.Image = CType(resources.GetObject("ToolStripButtonPreview.Image"), System.Drawing.Image)
    Me.ToolStripButtonPreview.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPreview.Name = "ToolStripButtonPreview"
    Me.ToolStripButtonPreview.Size = New System.Drawing.Size(81, 34)
    Me.ToolStripButtonPreview.Text = "プレビュー"
    Me.ToolStripButtonPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonExport, Me.PcaCommandItemExport)
    Me.ToolStripButtonExport.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonExport.Image = CType(resources.GetObject("ToolStripButtonExport.Image"), System.Drawing.Image)
    Me.ToolStripButtonExport.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonExport.Name = "ToolStripButtonExport"
    Me.ToolStripButtonExport.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonExport.Text = "出力"
    Me.ToolStripButtonExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSearch, Me.PcaCommandItemSearch)
    Me.ToolStripButtonSearch.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonSearch.Image = CType(resources.GetObject("ToolStripButtonSearch.Image"), System.Drawing.Image)
    Me.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSearch.Name = "ToolStripButtonSearch"
    Me.ToolStripButtonSearch.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonSearch.Text = "再表示"
    Me.ToolStripButtonSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"), System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandSearch, Me.PcaFunctionCommandWarehousing, Me.PcaFunctionCommandPrint, Me.PcaFunctionCommandPreview, Me.PcaFunctionCommandExport, Me.PcaFunctionCommandClose, Me.PcaFunctionCommandRefer})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 519)
    Me.PcaFunctionBar1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1225, 28)
    Me.PcaFunctionBar1.TabIndex = 6
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    '
    'PcaFunctionCommandSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSearch, Me.PcaCommandItemSearch)
    Me.PcaFunctionCommandSearch.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandSearch.Tag = Nothing
    Me.PcaFunctionCommandSearch.Text = "再集計"
    '
    'PcaFunctionCommandWarehousing
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandWarehousing, Me.PcaCommandItemWarehousing)
    Me.PcaFunctionCommandWarehousing.FunctionKey = PCA.Controls.FunctionKey.F7
    Me.PcaFunctionCommandWarehousing.Tag = Nothing
    Me.PcaFunctionCommandWarehousing.Text = "受払"
    '
    'PcaFunctionCommandPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrint, Me.PcaCommandItemPrint)
    Me.PcaFunctionCommandPrint.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPrint.Tag = Nothing
    Me.PcaFunctionCommandPrint.Text = "印刷"
    '
    'PcaFunctionCommandPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPreview, Me.PcaCommandItemPreview)
    Me.PcaFunctionCommandPreview.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPreview.ModifierKeys = PCA.Controls.FunctionModifierKeys.Shift
    Me.PcaFunctionCommandPreview.Tag = Nothing
    Me.PcaFunctionCommandPreview.Text = "プレビュー"
    '
    'PcaFunctionCommandExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandExport, Me.PcaCommandItemExport)
    Me.PcaFunctionCommandExport.FunctionKey = PCA.Controls.FunctionKey.F10
    Me.PcaFunctionCommandExport.Tag = Nothing
    Me.PcaFunctionCommandExport.Text = "出力"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemSearch, Me.PcaCommandItemWarehousing, Me.PcaCommandItemPrint, Me.PcaCommandItemPreview, Me.PcaCommandItemExport, Me.PcaCommandItemClose, Me.PcaCommandItemRefer})
    '
    'PcaCommandItemPrint
    '
    Me.PcaCommandItemPrint.CommandId = 9
    '
    'PcaCommandItemPreview
    '
    Me.PcaCommandItemPreview.CommandId = 9
    '
    'PcaCommandItemExport
    '
    Me.PcaCommandItemExport.CommandId = 10
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    '
    'PcaCommandItemSearch
    '
    Me.PcaCommandItemSearch.CommandId = 5
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandId = 8
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    '
    'PcaCommandItemWarehousing
    '
    Me.PcaCommandItemWarehousing.CommandId = 7
    '
    'PcaLotNo
    '
    Me.PcaLotNo.ClientProcess = Nothing
    Me.PcaLotNo.LabelText = "ロットNo"
    Me.PcaLotNo.LimitLength = 26
    Me.PcaLotNo.Location = New System.Drawing.Point(9, 150)
    Me.PcaLotNo.MaxLabelLength = 10
    Me.PcaLotNo.MaxLength = 26
    Me.PcaLotNo.MaxTextLength = 19
    Me.PcaLotNo.Name = "PcaLotNo"
    Me.PcaLotNo.Size = New System.Drawing.Size(203, 22)
    Me.PcaLotNo.TabIndex = 4
    '
    'PcaProduct
    '
    Me.PcaProduct.AutoTopMargin = False
    Me.PcaProduct.ClientProcess = Nothing
    Me.PcaProduct.LimitLength = 12
    Me.PcaProduct.Location = New System.Drawing.Point(10, 123)
    Me.PcaProduct.MaxCodeLength = 14
    Me.PcaProduct.MaxLabelLength = 10
    Me.PcaProduct.MaxNameLength = 0
    Me.PcaProduct.Name = "PcaProduct"
    Me.PcaProduct.RangeGroupBoxText = "商品"
    Me.PcaProduct.RangeGroupBoxVisible = False
    Me.PcaProduct.Size = New System.Drawing.Size(343, 22)
    Me.PcaProduct.TabIndex = 3
    '
    'PcaWarehousingDate
    '
    Me.PcaWarehousingDate.AllowEmpty = True
    Me.PcaWarehousingDate.AutoTextSize = False
    Me.PcaWarehousingDate.AutoTopMargin = False
    Me.PcaWarehousingDate.ClientProcess = Nothing
    Me.PcaWarehousingDate.DayLabel = ""
    Me.PcaWarehousingDate.DisplaySlash = True
    Me.PcaWarehousingDate.Holidays = Nothing
    Me.PcaWarehousingDate.Location = New System.Drawing.Point(10, 97)
    Me.PcaWarehousingDate.MaxLabelLength = 10
    Me.PcaWarehousingDate.MonthLabel = "/"
    Me.PcaWarehousingDate.Name = "PcaWarehousingDate"
    Me.PcaWarehousingDate.RangeGroupBoxText = "入荷日"
    Me.PcaWarehousingDate.RangeGroupBoxVisible = False
    Me.PcaWarehousingDate.Size = New System.Drawing.Size(373, 22)
    Me.PcaWarehousingDate.TabIndex = 2
    Me.PcaWarehousingDate.YearLabel = "/"
    '
    'PcaSelectedDate
    '
    Me.PcaSelectedDate.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.PcaSelectedDate.ClientProcess = Nothing
    Me.PcaSelectedDate.DayLabel = ""
    Me.PcaSelectedDate.DisplaySlash = True
    Me.PcaSelectedDate.Holidays = Nothing
    Me.PcaSelectedDate.LabelText = "在庫日付"
    Me.PcaSelectedDate.Location = New System.Drawing.Point(9, 71)
    Me.PcaSelectedDate.MaxLabelLength = 10
    Me.PcaSelectedDate.MaxTextLength = 16
    Me.PcaSelectedDate.MonthLabel = "/"
    Me.PcaSelectedDate.Name = "PcaSelectedDate"
    Me.PcaSelectedDate.Size = New System.Drawing.Size(182, 22)
    Me.PcaSelectedDate.TabIndex = 1
    Me.PcaSelectedDate.YearLabel = "/"
    '
    'PcaInventory
    '
    Me.PcaInventory.AutoTopMargin = False
    Me.PcaInventory.ClientProcess = Nothing
    Me.PcaInventory.LimitLength = 6
    Me.PcaInventory.Location = New System.Drawing.Point(430, 71)
    Me.PcaInventory.MaxCodeLength = 12
    Me.PcaInventory.MaxLabelLength = 10
    Me.PcaInventory.MaxNameLength = 0
    Me.PcaInventory.Name = "PcaInventory"
    Me.PcaInventory.RangeGroupBoxText = "倉庫"
    Me.PcaInventory.RangeGroupBoxVisible = False
    Me.PcaInventory.Size = New System.Drawing.Size(315, 22)
    Me.PcaInventory.TabIndex = 5
    '
    'PcaShukkaDate
    '
    Me.PcaShukkaDate.AllowEmpty = True
    Me.PcaShukkaDate.AutoTopMargin = False
    Me.PcaShukkaDate.ClientProcess = Nothing
    Me.PcaShukkaDate.DayLabel = ""
    Me.PcaShukkaDate.DisplaySlash = True
    Me.PcaShukkaDate.Holidays = Nothing
    Me.PcaShukkaDate.Location = New System.Drawing.Point(816, 71)
    Me.PcaShukkaDate.MaxLabelLength = 10
    Me.PcaShukkaDate.MonthLabel = "/"
    Me.PcaShukkaDate.Name = "PcaShukkaDate"
    Me.PcaShukkaDate.RangeGroupBoxText = "出荷期限"
    Me.PcaShukkaDate.RangeGroupBoxVisible = False
    Me.PcaShukkaDate.Size = New System.Drawing.Size(343, 22)
    Me.PcaShukkaDate.TabIndex = 6
    Me.PcaShukkaDate.YearLabel = "/"
    '
    'PcaShiyouDate
    '
    Me.PcaShiyouDate.AllowEmpty = True
    Me.PcaShiyouDate.AutoTopMargin = False
    Me.PcaShiyouDate.ClientProcess = Nothing
    Me.PcaShiyouDate.DayLabel = ""
    Me.PcaShiyouDate.DisplaySlash = True
    Me.PcaShiyouDate.Holidays = Nothing
    Me.PcaShiyouDate.Location = New System.Drawing.Point(816, 97)
    Me.PcaShiyouDate.MaxLabelLength = 10
    Me.PcaShiyouDate.MonthLabel = "/"
    Me.PcaShiyouDate.Name = "PcaShiyouDate"
    Me.PcaShiyouDate.RangeGroupBoxText = "使用期限"
    Me.PcaShiyouDate.RangeGroupBoxVisible = False
    Me.PcaShiyouDate.Size = New System.Drawing.Size(343, 22)
    Me.PcaShiyouDate.TabIndex = 7
    Me.PcaShiyouDate.YearLabel = "/"
    '
    'PcaShoumiDate
    '
    Me.PcaShoumiDate.AllowEmpty = True
    Me.PcaShoumiDate.AutoTopMargin = False
    Me.PcaShoumiDate.ClientProcess = Nothing
    Me.PcaShoumiDate.DayLabel = ""
    Me.PcaShoumiDate.DisplaySlash = True
    Me.PcaShoumiDate.Holidays = Nothing
    Me.PcaShoumiDate.Location = New System.Drawing.Point(816, 123)
    Me.PcaShoumiDate.MaxLabelLength = 10
    Me.PcaShoumiDate.MonthLabel = "/"
    Me.PcaShoumiDate.Name = "PcaShoumiDate"
    Me.PcaShoumiDate.RangeGroupBoxText = "賞味期限"
    Me.PcaShoumiDate.RangeGroupBoxVisible = False
    Me.PcaShoumiDate.Size = New System.Drawing.Size(343, 22)
    Me.PcaShoumiDate.TabIndex = 8
    Me.PcaShoumiDate.YearLabel = "/"
    '
    'dgv
    '
    Me.dgv.AllowUserToAddRows = False
    Me.dgv.AllowUserToDeleteRows = False
    Me.dgv.AllowUserToResizeRows = False
    Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dgv.BackgroundColor = System.Drawing.SystemColors.Control
    DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
    DataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue
    DataGridViewCellStyle1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Window
    DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
    DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
    DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
    Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
    Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
    Me.dgv.EnableHeadersVisualStyles = False
    Me.dgv.GridColor = System.Drawing.SystemColors.Control
    Me.dgv.Location = New System.Drawing.Point(10, 214)
    Me.dgv.MultiSelect = False
    Me.dgv.Name = "dgv"
    Me.dgv.RowHeadersVisible = False
    Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
    Me.dgv.Size = New System.Drawing.Size(1203, 298)
    Me.dgv.TabIndex = 27
    '
    'PcaLotDetailsContent
    '
    Me.PcaLotDetailsContent.AutoTextSize = False
    Me.PcaLotDetailsContent.CellValue = ""
    Me.PcaLotDetailsContent.LimitLength = 26
    Me.PcaLotDetailsContent.Location = New System.Drawing.Point(900, 178)
    Me.PcaLotDetailsContent.MaxLength = 26
    Me.PcaLotDetailsContent.Name = "PcaLotDetailsContent"
    Me.PcaLotDetailsContent.Size = New System.Drawing.Size(120, 22)
    Me.PcaLotDetailsContent.TabIndex = 12
    '
    'PcaLotDetails
    '
    Me.PcaLotDetails.DropDownWidth = 84
    Me.PcaLotDetails.FormattingEnabled = True
    Me.PcaLotDetails.Location = New System.Drawing.Point(816, 178)
    Me.PcaLotDetails.MaxTextLength = 12
    Me.PcaLotDetails.Name = "PcaLotDetails"
    Me.PcaLotDetails.Size = New System.Drawing.Size(84, 22)
    Me.PcaLotDetails.TabIndex = 10
    '
    'PcaShouhiDate
    '
    Me.PcaShouhiDate.AllowEmpty = True
    Me.PcaShouhiDate.AutoTopMargin = False
    Me.PcaShouhiDate.ClientProcess = Nothing
    Me.PcaShouhiDate.DayLabel = ""
    Me.PcaShouhiDate.DisplaySlash = True
    Me.PcaShouhiDate.Holidays = Nothing
    Me.PcaShouhiDate.Location = New System.Drawing.Point(816, 150)
    Me.PcaShouhiDate.MaxLabelLength = 10
    Me.PcaShouhiDate.MonthLabel = "/"
    Me.PcaShouhiDate.Name = "PcaShouhiDate"
    Me.PcaShouhiDate.RangeGroupBoxText = "消費期限"
    Me.PcaShouhiDate.RangeGroupBoxVisible = False
    Me.PcaShouhiDate.Size = New System.Drawing.Size(343, 22)
    Me.PcaShouhiDate.TabIndex = 9
    Me.PcaShouhiDate.YearLabel = "/"
    '
    'frmViewInventory
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1225, 547)
    Me.Controls.Add(Me.PcaShouhiDate)
    Me.Controls.Add(Me.PcaLotDetails)
    Me.Controls.Add(Me.PcaLotDetailsContent)
    Me.Controls.Add(Me.dgv)
    Me.Controls.Add(Me.PcaInventory)
    Me.Controls.Add(Me.PcaShukkaDate)
    Me.Controls.Add(Me.PcaShiyouDate)
    Me.Controls.Add(Me.PcaShoumiDate)
    Me.Controls.Add(Me.PcaLotNo)
    Me.Controls.Add(Me.PcaProduct)
    Me.Controls.Add(Me.PcaWarehousingDate)
    Me.Controls.Add(Me.PcaSelectedDate)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
    Me.MinimumSize = New System.Drawing.Size(950, 500)
    Me.Name = "frmViewInventory"
    Me.Text = "ロット別在庫一覧表"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemReference As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonPrint As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonPreview As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonExport As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonSearch As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
    Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
    Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemSearch As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemWarehousing As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemPrint As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemPreview As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemExport As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
    Friend WithEvents PcaLotNo As PCA.Controls.PcaLabeledTextBox
    Friend WithEvents PcaProduct As PCA.Controls.PcaRangeCodeSet
    Friend WithEvents PcaWarehousingDate As PCA.Controls.PcaRangeDate
    Friend WithEvents PcaSelectedDate As PCA.Controls.PcaLabeledDate
    Friend WithEvents PcaInventory As PCA.Controls.PcaRangeCodeSet
    Friend WithEvents PcaShukkaDate As PCA.Controls.PcaRangeDate
    Friend WithEvents PcaShiyouDate As PCA.Controls.PcaRangeDate
    Friend WithEvents PcaShoumiDate As PCA.Controls.PcaRangeDate
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandSearch As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandWarehousing As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandPrint As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandPreview As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandExport As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PcaLotDetailsContent As PCA.Controls.PcaTextBox
    Friend WithEvents PcaLotDetails As PCA.Controls.PcaComboBox
    Friend WithEvents PcaShouhiDate As PCA.Controls.PcaRangeDate
    Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
    Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand

End Class

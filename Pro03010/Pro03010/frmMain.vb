﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports PCA.UITools
Imports PCA.Controls
Imports PCA.TSC.Kon.Tools
Imports PCA.TSC.Kon.Tools.PEKonInput
Imports System.Xml
Imports System.IO
Imports System.ComponentModel
Imports Sunloft.PCAIF
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports PCA.TSC.Kon
Imports PCA
Imports System.Globalization
Imports System

Public Class frmMain

#Region " Declare"
  Private connector As IIntegratedApplication = Nothing
  'Table Column's width Constants
  Private Const NOTE_LENGTH As Integer = 40
  Private Const QUANTITY_LENGTH As Integer = 10
  Private Const NUMBER_LENGTH As Integer = 6

  Private Const BRANCH_WIDTH As Double = 1.0F
  Private Const TYPE_WIDTH As Double = 3.0F
  Private Const PRODUCT_CODE_WIDTH As Double = 3.0F
  Private Const COLOR_WIDTH As Double = 2.0F
  Private Const SIZE_WIDTH As Double = 2.0F
  Private Const WAREHOUSE_WIDTH As Double = 3.0F
  Private Const PRODUCT_NAME_WIDTH As Double = PRODUCT_CODE_WIDTH + COLOR_WIDTH + SIZE_WIDTH + WAREHOUSE_WIDTH
  Private Const TAX_WIDTH As Double = 3.0F
  Private Const NUMBER_WIDTH As Double = 2.0F
  Private Const QTT_PER_CASE_WIDTH As Double = 2.0F
  Private Const NO_OF_CASE_WIDTH As Double = 2.0F
  Private Const QUANTITY_WIDTH As Double = 2.5F
  Private Const UNIT_WITDH As Double = 1.5F
  Private Const UNIT_PRICE_WIDTH As Double = 3.0F
  Private Const AMOUNT_WIDTH As Double = 3.0F
  Private Const LOT_NO_WIDTH As Double = 3.0F
  Private Const SUP_LOT_NO_WIDTH As Double = 3.0F
  Private Const NOTE_WIDTH As Double = LOT_NO_WIDTH + SUP_LOT_NO_WIDTH
  Private Const WAREHOUSING_TYPE_CODE As Integer = 1

  'Pop up message location
  Private Const SUM_TABLE_WIDTH As Double = BRANCH_WIDTH + TYPE_WIDTH + PRODUCT_NAME_WIDTH + TAX_WIDTH + QUANTITY_WIDTH + UNIT_WITDH + _
                                            NUMBER_WIDTH + UNIT_PRICE_WIDTH + AMOUNT_WIDTH + LOT_NO_WIDTH + SUP_LOT_NO_WIDTH
  Private Const TYPE_ERRMESSAGE_LOCATION As Double = (BRANCH_WIDTH + TYPE_WIDTH) / SUM_TABLE_WIDTH
  Private Const WAREHOUSE_ERRMESSAGE_LOCATION = (BRANCH_WIDTH + TYPE_WIDTH + PRODUCT_NAME_WIDTH) / SUM_TABLE_WIDTH
  Private Const QUANTITY_ERRMESSAGE_LOCATION = (BRANCH_WIDTH + TYPE_WIDTH + PRODUCT_NAME_WIDTH + TAX_WIDTH + QUANTITY_WIDTH) / SUM_TABLE_WIDTH
  Private Const LOT_ERRMESSAGE_LOCATION = (BRANCH_WIDTH + TYPE_WIDTH + PRODUCT_NAME_WIDTH + TAX_WIDTH + _
                                           QUANTITY_WIDTH + UNIT_WITDH + NUMBER_WIDTH + UNIT_PRICE_WIDTH + AMOUNT_WIDTH + LOT_NO_WIDTH) / SUM_TABLE_WIDTH

  Private Const LOT_NO_COLUMN = 8
  Private Const BRANCH_COLUMN = 0
  Private Const LOT_NO_ULOT_CELL = 0
  Private Const LOT_NO_ALOT_CELL = 1
  Private Const PRODUCT_COLUMN = 2
  Private Const PRODUCT_CODE_CELL = 0
  Private Const WAREHOUSE_CODE_CELL = 3

  'Save to common lib?
  Private Const NORMAL_SHIPPING As String = "通常入荷"
  Private Const RETURN_GOODS As String = "返品"
  Private Const WAREHOUSING_TYPE_SL_ZDN As Integer = 1
  Private Const TAX_CAL_NOT_REQUIRED As Integer = 0 '
  Private Const CONVERT_FLAG_NONE As Integer = 0    '2015/12/22 TNOGUCHI
  Private Const CONVERT_FLAG_DONE As Integer = 1    '2015/12/22 TNOGUCHI

  Private m_conf As Sunloft.PcaConfig
  Private m_appClass As ConnectToPCA = New ConnectToPCA
  Private m_columnName As ColumnName

  Private m_strColName As String = String.Empty
  Private m_IntSlipNo As Integer
  Private m_strCurrentUser As String = String.Empty
  Private m_strSearchItem As String = String.Empty
  Private m_IsF12CloseFlag As Boolean = True
  Private m_IntRowIndex As Integer = -1
  Private m_IntRowCount As Integer = 0
  Private m_IntTax_rate As Double() = {0}
  Private m_intOrderSlipID As Integer
  Private m_intPurSlipID As Integer

  Private AMS1Class As AMS1
  Private SL_NYKHClass As SL_NYKH
  Private SL_NYKDClass As SL_NYKD
  Private NYKD_lists As SL_NYKD()
  Private SL_ZDNClass As SL_ZDN
  Private SL_LMBClass As SL_LMB

  Private m_strCurCode As String = String.Empty
  Private m_SlipType As SlipType
  Private m_isShowDialog As Boolean = False

  Private newDetailsList As Collection = New Collection()
  Private oldDetailsList As Collection = New Collection()
  Private DBDetailsList As SL_NYKD() = {New SL_NYKD(connector)}

  'setType : 伝票区分(通常/返品)
  Private Enum enumSlipType
    NormalShipping
    ReturnGoods
  End Enum
  Private Enum SlipType
    Self
    Order
    Sup
  End Enum
  '<Browsable(False)> _
  Public Property IsValidate() As Boolean = False
#End Region

#Region "Initialize"

  Public Sub New()
    Try
      InitializeComponent()
      'Connect to PCA
      Dim strParam As String = Command()
      Dim parts As String() = strParam.Split(New Char() {" "c})
      Dim intParam As Integer = 0
      Dim args As String = m_appClass.argumentContent

      If parts.Length > 1 Then
        m_appClass.ConnectToPCA(strParam)
        connector = m_appClass.connector
      ElseIf Integer.TryParse(strParam, intParam) Then
        connector = Sunloft.PCAIF.ConnectToPCA.ConnectToPCACalledPG()
      Else
        m_appClass.ConnectToPCA() 'start normally (from code or menu)
        connector = m_appClass.connector
      End If

      m_conf = New Sunloft.PcaConfig

      AMS1Class = New AMS1(connector)
      AMS1Class.ReadAll() 'Get number of digits for fields
      setProject.Text = AMS1Class.ProjectName.Trim

      m_columnName = New ColumnName(AMS1Class)

      SL_NYKHClass = New SL_NYKH(connector)
      SL_NYKDClass = New SL_NYKD(connector)
      SL_ZDNClass = New SL_ZDN(connector)
      SL_LMBClass = New SL_LMB(connector)
      InitTable()

      setSupCode.LimitLength = AMS1Class.ams1_SupplierLength
      setPerson.LimitLength = AMS1Class.ams1_PersonLength
      setDepartment.LimitLength = AMS1Class.ams1_DepartmentLength
      setMemoCode.LimitLength = AMS1Class.ams1_MemoLength

      m_SlipType = SlipType.Self

      setType.Focus()

      tblTable.Enabled = False
      PcaCommandItemLotSearch.Enabled = False
      PcaCommandItemRefer.Enabled = False
      PcaCommandItemLotDetails.Enabled = False
      PcaCommandItemLotSearch.Enabled = False
      PcaCommandItemCopy.Enabled = False
      PcaCommandItemNext.Enabled = False
      PcaCommandItemCopy.Enabled = False
      PcaCommandItemDelete.Enabled = False
      PcaCommandItemDeleteRow.Enabled = False

      If intParam <> 0 OrElse (args <> String.Empty AndAlso Integer.TryParse(args, intParam)) Then
        DisplaySlipByNo(intParam)
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitTable()
    Try
      With tblTable
        .DataRowCount = 99

        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader()
        InitTableBody()

        .UpdateTable()

        tblTable.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
        tblTable.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
        tblTable.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
        tblTable.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

        SLCmnFunction.SetBranchNumber(tblTable, m_columnName.strNo)
      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    Try
      MyBase.OnLoad(e)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  '***********************************************************																														
  'Name          : InitTableHeader
  'Content       : Init Table Header when create Table
  'Return Value  : None
  'Argument      : None
  'Created date  : 2015/11/15  by Lai Minh Hien
  'Modified date :         by        Content																														
  '***********************************************************
  Private Sub InitTableHeader()
    Try
      tblTable.HeadTextHeight = 3
      tblTable.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
      tblTable.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell

      'Big column 1
      column = New PcaColumn("Header1", BRANCH_WIDTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(String.empty)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 2
      column = New PcaColumn("Header2", TYPE_WIDTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(m_columnName.strType)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 3
      column = New PcaColumn("Header3", CSng(PRODUCT_NAME_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(m_columnName.strProductCode2, CSng(PRODUCT_CODE_WIDTH), 1, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strColor, CSng(COLOR_WIDTH), 1, PRODUCT_CODE_WIDTH, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strSize, CSng(SIZE_WIDTH), 1, PRODUCT_CODE_WIDTH + COLOR_WIDTH, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strWarehouse2, CSng(WAREHOUSE_WIDTH), 1, PRODUCT_CODE_WIDTH + COLOR_WIDTH + SIZE_WIDTH, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strProductName, CSng(PRODUCT_NAME_WIDTH), 1, 0.0F, 1.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strSpec, CSng(PRODUCT_NAME_WIDTH), 1, 0.0F, 2.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 4
      column = New PcaColumn("Header4", CSng(TAX_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(m_columnName.strTaxIncluded, CSng(TAX_WIDTH), 2, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strRate, CSng(TAX_WIDTH), 1, 0.0F, 2.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 5
      column = New PcaColumn("Header5", CSng(NO_OF_CASE_WIDTH + QTT_PER_CASE_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(m_columnName.strQttPerCase, CSng(QTT_PER_CASE_WIDTH), 1, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strNoOfCase, CSng(NO_OF_CASE_WIDTH), 1, QTT_PER_CASE_WIDTH, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strQuantity, CSng(QUANTITY_WIDTH), 2, 0.0F, 1.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strUnit, CSng(UNIT_WITDH), 2, QUANTITY_WIDTH, 1.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 6
      column = New PcaColumn("Header6", CSng(NUMBER_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(m_columnName.strNumber, CSng(NUMBER_WIDTH), 3, 0.0F, 0.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 7
      column = New PcaColumn("Header7", CSng(UNIT_PRICE_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(m_columnName.strUnitPrice, CSng(UNIT_PRICE_WIDTH), 3, 0.0F, 0.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 8
      column = New PcaColumn("Header8", CSng(AMOUNT_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(m_columnName.strAmount, CSng(AMOUNT_WIDTH), 3, 0.0F, 0.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 9
      column = New PcaColumn("Header9", CSng(NOTE_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(m_columnName.strUserLotNo, CSng(LOT_NO_WIDTH), 1, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strSupLotNo, CSng(SUP_LOT_NO_WIDTH), 1, LOT_NO_WIDTH, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(m_columnName.strNote, CSng(NOTE_WIDTH), 2, 0.0F, 1.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Invisible column (From Lot Details screen)
      InitHeaderColumn(column, cell, 0, m_columnName.strManageNo, "Header10")
      InitHeaderColumn(column, cell, 0, m_columnName.strShukka, "Header11")
      InitHeaderColumn(column, cell, 0, m_columnName.strShiyou, "Header12")
      InitHeaderColumn(column, cell, 0, m_columnName.strShoumi, "Header13")
      InitHeaderColumn(column, cell, 0, m_columnName.strShouhi, "Header14")

      InitHeaderColumn(column, cell, 0, m_columnName.strLotID1, "Header15")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotID2, "Header16")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotID3, "Header17")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotID4, "Header18")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotID5, "Header19")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotID6, "Header20")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotID7, "Header21")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotID8, "Header22")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotID9, "Header23")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotID10, "Header24")

      InitHeaderColumn(column, cell, 0, m_columnName.strLotName1, "Header25")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotName2, "Header26")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotName3, "Header27")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotName4, "Header28")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotName5, "Header29")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotName6, "Header30")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotName7, "Header31")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotName8, "Header32")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotName9, "Header33")
      InitHeaderColumn(column, cell, 0, m_columnName.strLotName10, "Header34")


    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  '***********************************************************																														
  'Name          : InitTableHeaderColumn
  'Content       : Init a Header column
  'Return Value  : None
  'Argument      : None
  'Created date  : 2015/11/15  by Lai Minh Hien
  'Modified date :         by        Content																														
  '***********************************************************
  Private Sub InitHeaderColumn(ByVal column As PcaColumn, ByVal cell As PcaCell, ByVal columnWidth As Integer, ByVal cellName As String, ByVal columnName As String)
    column = New PcaColumn(columnName, columnWidth)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(cellName, columnWidth, 3, 0.0F, 0.0F)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)
  End Sub
  '***********************************************************																														
  'Name          : InitTableBody
  'Content       : InitTableBody when create Table
  'Return Value  : None
  'Argument      : None
  'Created date  : 2015/11/15  by Lai Minh Hien
  'Modified date :  2015/12/01       by Hien  Content: Add invisible columns																														
  '***********************************************************
  Private Sub InitTableBody()
    Try
      tblTable.BodyTextHeight = 3

      tblTable.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
      tblTable.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
      tblTable.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
      tblTable.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell

      'Big column 1
      column = New PcaColumn("Body1", BRANCH_WIDTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(m_columnName.strNo, 1.0F, 3.0F, 0.0F, 0.0F, m_columnName.strNo)
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 2
      column = New PcaColumn("Body2", TYPE_WIDTH)
      column.CanResize = True
      cell = New PcaCell(m_columnName.strType, CellType.ItemSelect, 0.0F, 0.0F)
      'cell.SelectItems = {m_ConstantClass.WAREHOUSE_ALL, m_ConstantClass.WAREHOUSE_APART}
      SLCmnFunction.SetPcaCellCombo(cell, SLConstants.ComboType.WareHouse)
      'cell.SelectItems = {SLConstants.WarehouseKbn.WAREHOUSE_ALL, SLConstants.WarehouseKbn.WAREHOUSE_APART}
      cell.EditMode = True
      cell.Enabled = True
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)
      '
      'Big column 3
      column = New PcaColumn("Body3", CSng(PRODUCT_NAME_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareSyohinCode(CSng(PRODUCT_CODE_WIDTH), 1, 0.0F, 0.0F, m_columnName.strProductCode2)
      cell.LimitLength = CShort(AMS1Class.ams1_ProductLength)  '.ams1_intg3)
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(m_columnName.strColor, CSng(COLOR_WIDTH), 1, PRODUCT_CODE_WIDTH, 0.0F, m_columnName.strColor)
      cell.LimitLength = 7
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(m_columnName.strSize, CSng(SIZE_WIDTH), 1, PRODUCT_CODE_WIDTH + COLOR_WIDTH, 0.0F, m_columnName.strSize)
      cell.LimitLength = 5
      column.Cells.Add(cell)
      cell = tblTable.PrepareSokoCode(CSng(WAREHOUSE_WIDTH), 1, PRODUCT_CODE_WIDTH + COLOR_WIDTH + SIZE_WIDTH, 0.0F, m_columnName.strWarehouse2)
      'cell.LimitLength = CShort(m_AMS1Class.ams1_intg59)
      cell.LimitLength = CShort(AMS1Class.ams1_WarehouseLength)
      cell.CellStyle.CorrectBorderSide = CellBorderSide.All
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(m_columnName.strProductName, CSng(PRODUCT_NAME_WIDTH), 1, 0.0F, 1.0F, m_columnName.strProductName)
      cell.LimitLength = 18
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(m_columnName.strSpec, CSng(PRODUCT_NAME_WIDTH), 1, 0.0F, 2.0F, m_columnName.strSpec)
      cell.LimitLength = 18
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 4
      column = New PcaColumn("Body4", CSng(TAX_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareTextCell(m_columnName.strTaxIncluded, CSng(TAX_WIDTH), 2, 0.0F, 0.0F, m_columnName.strTaxIncluded)
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(m_columnName.strRate, CSng(TAX_WIDTH), 1, 0.0F, 2.0F, m_columnName.strRate)
      cell.CellStyle.Alignment = StringAlignment.Far
      cell.EditMode = False

      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 5
      column = New PcaColumn("Body5", CSng(NO_OF_CASE_WIDTH + QTT_PER_CASE_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(m_columnName.strQttPerCase, CSng(QTT_PER_CASE_WIDTH), 1, 0.0F, 0.0F, m_columnName.strQttPerCase)
      cell.LimitLength = NUMBER_LENGTH
      column.Cells.Add(cell)

      cell = tblTable.PrepareNumberCell(m_columnName.strNoOfCase, CSng(NO_OF_CASE_WIDTH), 1, QTT_PER_CASE_WIDTH, 0.0F, m_columnName.strNoOfCase)
      cell.LimitLength = NUMBER_LENGTH
      column.Cells.Add(cell)

      cell = tblTable.PrepareTextCell("Empty", CSng(QUANTITY_WIDTH), 1, 0.0F, 1.0F, m_columnName.strQuantity)
      cell.EditMode = False
      cell.CellStyle.BorderSide = CellBorderSide.Right
      column.Cells.Add(cell)

      cell = New PcaCell(m_columnName.strQuantity, CellType.Number, 0.0, 2.0)
      cell = tblTable.PrepareNumberCell(m_columnName.strQuantity, CSng(QUANTITY_WIDTH), 1, 0.0F, 2.0F, m_columnName.strQuantity)
      cell.CanResize = True
      cell.AllowMinus = True
      column.Cells.Add(cell)

      cell = tblTable.PrepareTextCell(m_columnName.strUnit, CSng(UNIT_WITDH), 1, QUANTITY_WIDTH, 2.0F, m_columnName.strUnit)
      cell.EditMode = False
      cell.CellStyle.BorderSide = CellBorderSide.None
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 6
      column = New PcaColumn("Body6", CSng(NUMBER_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(m_columnName.strNumber, CSng(NUMBER_WIDTH), 1, 0.0F, 2.0F, m_columnName.strNumber)
      cell.LimitLength = NUMBER_LENGTH
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 7
      column = New PcaColumn("Body7", CSng(UNIT_PRICE_WIDTH))

      column.CanResize = True

      cell = tblTable.PrepareNumberCell(m_columnName.strUnitPrice, CSng(UNIT_PRICE_WIDTH), 1, 0.0F, 2.0F, m_columnName.strUnitPrice)
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 8
      column = New PcaColumn("Body8", CSng(AMOUNT_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(m_columnName.strAmount, CSng(AMOUNT_WIDTH), 1, 0.0F, 2.0, m_columnName.strAmount)
      cell.AllowMinus = True

      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 9
      column = New PcaColumn("Body9", CSng(NOTE_WIDTH))
      column.CanResize = True

      cell = tblTable.PrepareTextCell(m_columnName.strUserLotNo, CSng(LOT_NO_WIDTH), 1, 0.0F, 0.0F, m_columnName.strUserLotNo)
      column.Cells.Add(cell)

      cell = tblTable.PrepareTextCell(m_columnName.strSupLotNo, CSng(SUP_LOT_NO_WIDTH), 1, LOT_NO_WIDTH, 0.0F, m_columnName.strSupLotNo)
      column.Cells.Add(cell)

      cell = tblTable.PrepareTextCell(m_columnName.strNote, CSng(NOTE_WIDTH), 1, 0.0F, 2.0F, m_columnName.strNote)
      cell.LimitLength = NOTE_LENGTH
      column.Cells.Add(cell)

      tblTable.BodyColumns.Add(column)

      'Invisible column from Lot Details screen

      InitBodyColumnText(column, cell, "Body10", m_columnName.strManageNo, 0, 3, m_columnName.strManageNo, False)
      InitBodyColumnNumber(column, cell, "Body11", m_columnName.strShukka, 0, 3, m_columnName.strShukka, False)
      InitBodyColumnNumber(column, cell, "Body12", m_columnName.strShiyou, 0, 3, m_columnName.strShiyou, False)
      InitBodyColumnNumber(column, cell, "Body13", m_columnName.strShoumi, 0, 3, m_columnName.strShoumi, False)
      InitBodyColumnNumber(column, cell, "Body14", m_columnName.strShouhi, 0, 3, m_columnName.strShouhi, False)

      InitBodyColumnText(column, cell, "Body15", m_columnName.strLotID1, 0, 3, m_columnName.strLotID1, False)
      InitBodyColumnText(column, cell, "Body16", m_columnName.strLotID2, 0, 3, m_columnName.strLotID2, False)
      InitBodyColumnText(column, cell, "Body17", m_columnName.strLotID3, 0, 3, m_columnName.strLotID3, False)
      InitBodyColumnText(column, cell, "Body18", m_columnName.strLotID4, 0, 3, m_columnName.strLotID4, False)
      InitBodyColumnText(column, cell, "Body19", m_columnName.strLotID5, 0, 3, m_columnName.strLotID5, False)
      InitBodyColumnText(column, cell, "Body20", m_columnName.strLotID6, 0, 3, m_columnName.strLotID6, False)
      InitBodyColumnText(column, cell, "Body21", m_columnName.strLotID7, 0, 3, m_columnName.strLotID7, False)
      InitBodyColumnText(column, cell, "Body22", m_columnName.strLotID8, 0, 3, m_columnName.strLotID8, False)
      InitBodyColumnText(column, cell, "Body23", m_columnName.strLotID9, 0, 3, m_columnName.strLotID9, False)
      InitBodyColumnText(column, cell, "Body24", m_columnName.strLotID10, 0, 3, m_columnName.strLotID10, False)

      InitBodyColumnText(column, cell, "Body25", m_columnName.strLotName1, 0, 3, m_columnName.strLotName1, False)
      InitBodyColumnText(column, cell, "Body26", m_columnName.strLotName2, 0, 3, m_columnName.strLotName2, False)
      InitBodyColumnText(column, cell, "Body27", m_columnName.strLotName3, 0, 3, m_columnName.strLotName3, False)
      InitBodyColumnText(column, cell, "Body28", m_columnName.strLotName4, 0, 3, m_columnName.strLotName4, False)
      InitBodyColumnText(column, cell, "Body29", m_columnName.strLotName5, 0, 3, m_columnName.strLotName5, False)
      InitBodyColumnText(column, cell, "Body30", m_columnName.strLotName6, 0, 3, m_columnName.strLotName6, False)
      InitBodyColumnText(column, cell, "Body31", m_columnName.strLotName7, 0, 3, m_columnName.strLotName7, False)
      InitBodyColumnText(column, cell, "Body32", m_columnName.strLotName8, 0, 3, m_columnName.strLotName8, False)
      InitBodyColumnText(column, cell, "Body33", m_columnName.strLotName9, 0, 3, m_columnName.strLotName9, False)
      InitBodyColumnText(column, cell, "Body34", m_columnName.strLotName10, 0, 3, m_columnName.strLotName10, False)

      InitBodyColumnNumber(column, cell, "Body35", m_columnName.strQuantityNoOfDecimal, 0, 3, m_columnName.strQuantityNoOfDecimal, False)
      InitBodyColumnNumber(column, cell, "Body36", m_columnName.strUnitPriceNoOfDecimal, 0, 3, m_columnName.strUnitPriceNoOfDecimal, False)
      InitBodyColumnNumber(column, cell, "Body37", m_columnName.strCurrentNumber, 0, 3, m_columnName.strCurrentNumber, False)
      InitBodyColumnNumber(column, cell, "Body38", m_columnName.strCurrentQuantity, 0, 3, m_columnName.strCurrentQuantity, True)

      InitBodyColumnText(column, cell, "Body39", m_columnName.strDetailsID, 0, 3, m_columnName.strDetailsID, False)
      InitBodyColumnText(column, cell, "Body40", m_columnName.strInvID, 0, 3, m_columnName.strInvID, False)
      InitBodyColumnText(column, cell, "Body41", m_columnName.strSysLotNo, 0, 3, m_columnName.strSysLotNo, False)

      InitBodyColumnText(column, cell, "Body42", m_columnName.strRetInvID, 0, 3, m_columnName.strRetInvID, False)
      InitBodyColumnText(column, cell, "Body43", m_columnName.strRetAllocateID, 0, 3, m_columnName.strRetAllocateID, False)
      InitBodyColumnText(column, cell, "Body44", m_columnName.strConvertFlag, 0, 3, m_columnName.strConvertFlag, False)
      InitBodyColumnText(column, cell, "Body45", m_columnName.strRetAllocateNum, 0, 3, m_columnName.strRetAllocateNum, False)
      InitBodyColumnText(column, cell, "Body46", m_columnName.strRetAllocateQty, 0, 3, m_columnName.strRetAllocateQty, False)
      
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  '***********************************************************																														
  'Name          : InitBodyColumnText
  'Content       : Init Text column when create Table 
  'Return Value  : None
  'Argument      : None
  'Created date  : 2015/12/01  by Lai Minh Hien
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub InitBodyColumnText(column As PcaColumn, cell As PcaCell, columnName As String, cellname As String, cellwidth As Short, cellHeight As Short, headerName As String, Optional ByVal editable As Boolean = True)
    'Create column text
    column = New PcaColumn(columnName, cellwidth)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(cellname, cellwidth, cellHeight, 0, 0, headerName)
    cell.EditMode = editable
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)
  End Sub

  '***********************************************************																														
  'Name          : InitBodyColumnNumber
  'Content       : Init Number column when create Table 
  'Return Value  : None
  'Argument      : None
  'Created date  : 2015/12/01  by Lai Minh Hien
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub InitBodyColumnNumber(column As PcaColumn, cell As PcaCell, columnName As String, cellname As String, cellwidth As Short, cellHeight As Short, headerName As String, Optional ByVal editable As Boolean = True)
    'Create column number. Allow decimal (For Current Quantity)
    column = New PcaColumn(columnName, cellwidth)
    column.CanResize = True
    cell = tblTable.PrepareNumberCell(cellname, cellwidth, cellHeight, 0, 0, headerName)
    cell.FormatDecimal = True
    cell.EditMode = editable
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)
  End Sub

  ''' <summary>
  ''' Set to initial mode
  ''' </summary>
  ''' <param name="isClearData">Clear all Hearer fields. False when Copying slip</param>
  ''' <remarks></remarks>
  Public Sub InitCreateMode(Optional isClearData As Boolean = True)
    Try
      HeaderLabel.State = LabelStateType.New
      If isClearData Then
        ClearData()
      Else
        ClearDataForCopy()
      End If
      SL_NYKHClass = New SL_NYKH(connector)
      SL_NYKDClass = New SL_NYKD(connector)
      NYKD_lists = Nothing

      setSupCode.Enabled = True
      setOrderNo.Enabled = True
      setPurNo.Enabled = True
      PcaCommandItemNext.Enabled = False
      PcaCommandItemDelete.Enabled = False
      PcaCommandItemCopy.Enabled = False
      PcaCommandItemNext.Enabled = False
      PcaCommandItemCopy.Enabled = False
      SLCmnFunction.SetBranchNumber(tblTable, m_columnName.strNo)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub InitCreateMode_ClearParameters()
    m_IntSlipNo = 0
    m_strCurrentUser = String.empty
    m_strSearchItem = String.empty
    m_IsF12CloseFlag = True
    m_IntRowIndex = 0
    m_IntRowCount = 0
    tblTable.Enabled = False
  End Sub

  ''' <summary>
  ''' Set to edit mode.
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitEditMode()
    HeaderLabel.State = LabelStateType.Modify
    m_SlipType = SlipType.Self
    setSupCode.Enabled = False
    setOrderNo.Enabled = False
    setPurNo.Enabled = False
    PcaCommandItemCopy.Enabled = True
    PcaCommandItemNext.Enabled = True
    PcaCommandItemDelete.Enabled = True
    PcaCommandItemNext.Enabled = True
    PcaCommandItemCopy.Enabled = True
    SLCmnFunction.SetBranchNumber(tblTable, m_columnName.strNo)
  End Sub
#End Region

#Region "Private method"

  ''' <summary>
  ''' Display number of allocated product in message
  ''' </summary>
  ''' <param name="isQuantity"></param>
  ''' <param name="number"></param>
  ''' <remarks>2015/11/30 by   Hien    Content: Check allocated quantity </remarks>
  Private Sub AllocatedErrorMessage(isQuantity As Boolean, number As Double, m_IntRowIndex As Integer)
    Try
      Dim PosForm As System.Drawing.Point
      PosForm.Y = tblTable.Location.Y + tblTable.HeadColumns.Height * (m_IntRowIndex + 2)
      PosForm.X = CInt(WAREHOUSE_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
      If isQuantity Then
        SLCmnFunction.ShowToolTip("引当済のため" & number.ToString & "より少ない数量に変更できません", String.empty, ToolTipIcon.Warning, PosForm, ToolTip1, Me)
      Else
        SLCmnFunction.ShowToolTip("引当済のため" & number.ToString & "より少ない本数に変更できません", String.empty, ToolTipIcon.Warning, PosForm, ToolTip1, Me)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Set Lot column editable/uneditable depends on product code column
  ''' </summary>
  ''' <param name="strStandardProductCode"></param>
  ''' <remarks></remarks>
  Private Sub SetLotNoEditable(strStandardProductCode As String)
    Try
      strStandardProductCode = strStandardProductCode.Trim
      'Disable Lot No if product's lot flag = not use
      Dim SL_SMSClass As New SL_SMS(connector)
      SL_SMSClass.ReadByProductCode(strStandardProductCode)
      If SL_SMSClass.sl_sms_lkbn = 0 Then 'Set Lotno Not editable if product's lot flag = not use
        tblTable.BodyColumns(LOT_NO_COLUMN).Cells(LOT_NO_ALOT_CELL).EditMode = False
        tblTable.BodyColumns(LOT_NO_COLUMN).Cells(LOT_NO_ULOT_CELL).EditMode = False
        PcaCommandItemLotDetails.Enabled = False
      Else
        tblTable.BodyColumns(LOT_NO_COLUMN).Cells(LOT_NO_ALOT_CELL).EditMode = True
        tblTable.BodyColumns(LOT_NO_COLUMN).Cells(LOT_NO_ULOT_CELL).EditMode = True
        PcaCommandItemLotDetails.Enabled = True
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' In edit mode, if a product is already allocated, it's product code and warehouse are set uneditable
  ''' </summary>
  ''' <param name="intRowIndex"></param>
  ''' <remarks></remarks>
  Private Sub SetProductCodeEditable(intRowIndex As Integer)
    Try
      Dim intDetail As Integer = 0
      'compare current quantity with table details
      If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intRowIndex, m_columnName.strCurrentQuantity)) Then
        Dim m_SL_NYKDClass = New SL_NYKD(connector)
        If Integer.TryParse(tblTable.GetCellValue(intRowIndex, m_columnName.strDetailsID).ToString, intDetail) Then
          m_SL_NYKDClass.ReadByID(CInt(tblTable.GetCellValue(intRowIndex, m_columnName.strDetailsID)))
          If CDec(m_SL_NYKDClass.sl_nykd_suryo) <> CDec(tblTable.GetCellValue(intRowIndex, m_columnName.strCurrentQuantity)) Then
            tblTable.BodyColumns(PRODUCT_COLUMN).Cells(PRODUCT_CODE_CELL).EditMode = False
            tblTable.BodyColumns(PRODUCT_COLUMN).Cells(WAREHOUSE_CODE_CELL).EditMode = False
          Else
            tblTable.BodyColumns(PRODUCT_COLUMN).Cells(PRODUCT_CODE_CELL).EditMode = True
            tblTable.BodyColumns(PRODUCT_COLUMN).Cells(WAREHOUSE_CODE_CELL).EditMode = True
          End If
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Reset input data
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ClearData() As Boolean
    'Clear data 
    tblTable.ClearCellValues()
    m_IntRowCount = 0
    setType.CodeText = SLConstants.WarehouseKbn.NORMAL_WAREHOUSING_CODE.ToString
    ltxtWhNo.Text = String.empty
    setSupCode.CodeText = String.empty
    setOrderNo.CodeText = String.empty
    setPurNo.CodeText = String.empty
    ltxtWhNo2.Text = String.empty
    setPerson.CodeText = String.empty
    setDepartment.CodeText = String.empty
    setProject.CodeText = String.empty
    setMemoCode.CodeText = String.empty
    setMemoContent.CodeText = String.empty
    lblAddr.Text = String.empty
    lblSupFax.Text = "FAX: "
    lblSupName.Text = String.empty
    lblSupTax.Text = "消費税: "
    lblSupTel.Text = "TEL: "
    setPerson.NameText = " "
    setDepartment.NameText = String.empty
    setProject.NameText = String.empty
    ltxtCreatedAt.Text = String.empty
    ltxtCreatedBy.Text = String.empty
    ltxtModifiedAt.Text = String.empty
    ltxtModifiedBy.Text = String.empty
    Return True
  End Function

  Private Function ClearDataForCopy() As Boolean
    Try
      Dim intLpc As Integer = 0
      'tblTable.
      For intLpc = 0 To m_IntRowCount - 1
        If tblTable.GetCellValue(intLpc, m_columnName.strDetailsID) IsNot Nothing AndAlso tblTable.GetCellValue(intLpc, m_columnName.strDetailsID).ToString.Trim.Length > 0 Then
          tblTable.SetCellValue(intLpc, m_columnName.strDetailsID, String.Empty)
        End If

      Next

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  ''' <summary>
  ''' Set number of decimal digit depends on product code
  ''' </summary>
  ''' <param name="rowIndex"></param>
  ''' <param name="strProductcode"></param>
  ''' <remarks></remarks>
  Private Sub SetNoOfDecimalDigitByProduct(ByVal rowIndex As Integer, ByVal strProductcode As String)
    Try
      Dim strStandardCode As String = SLCmnFunction.standardlizeCode(strProductcode, CShort(AMS1Class.ams1_ProductLength))
      Dim m_SMSClass As SMS = New SMS(connector)
      If m_SMSClass.ReadByID(strStandardCode) = True Then
        'Product found
        'Set quantity value
        Dim cell As PcaCell = tblTable.BodyColumns.GetCell(m_columnName.strQuantity)
        cell.DecimalDigit = CShort(m_SMSClass.sms_sketa)
        cell.FormatDecimal = True
        cell.Enabled = True

        'Set unit price value
        cell = tblTable.BodyColumns.GetCell(m_columnName.strUnitPrice)
        cell.DecimalDigit = CShort(m_SMSClass.sms_tketa)
        cell.FormatDecimal = True
        cell.Enabled = True

        'Set current quantity value
        cell = tblTable.BodyColumns.GetCell(m_columnName.strCurrentQuantity)
        cell.DecimalDigit = CShort(m_SMSClass.sms_sketa)
        cell.FormatDecimal = True
        cell.Enabled = True

      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Show Return Lot Inventory Dialog And Set Select Lot Key
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SearchReturnLot()
    Try
      Dim strProductCode As String = tblTable.GetCellValue(tblTable.InputRow, m_columnName.strProductCode).ToString
      Dim strWarehouseCode As String = tblTable.GetCellValue(tblTable.InputRow, m_columnName.strWarehouse).ToString
      Dim intInvID As Integer = 0
      Dim intNum As Integer = 0
      Dim decQty As Decimal = 0D
      Dim intQtyPerCase As Integer = 0
      Dim decCase As Decimal = 0D
      Dim invSlipState As PCA.Controls.LabelStateType

      If tblTable.GetCellValue(tblTable.InputRow, m_columnName.strInvID) Is Nothing Then
        invSlipState = LabelStateType.New
      Else
        invSlipState = HeaderLabel.State
      End If

      Dim zaikoDialog As SLZaikoDialog = _
       New SLZaikoDialog(connector, strProductCode, ldtWhDate.Date, SLZaikoDialog.enmSlipType.typReturn, strWarehouseCode, intInvID, intNum, Math.Abs(decQty), invSlipState)

      If tblTable.GetCellValue(tblTable.InputRow, m_columnName.strInvID) IsNot Nothing Then
        Integer.TryParse(tblTable.GetCellValue(tblTable.InputRow, m_columnName.strInvID).ToString, intInvID)
        Integer.TryParse(tblTable.GetCellValue(tblTable.InputRow, m_columnName.strNumber).ToString, intNum)
        Decimal.TryParse(tblTable.GetCellValue(tblTable.InputRow, m_columnName.strQuantity).ToString, decQty)
      End If

      Dim intCnt As Integer = 0
      If CType(zaikoDialog.ShowDialog, System.Windows.Forms.DialogResult) = System.Windows.Forms.DialogResult.OK Then
        For Each value In zaikoDialog.returnValue()
          Select Case intCnt
            Case 0
              'Return Lot ID
              tblTable.SetCellValue(tblTable.InputRow, m_columnName.strRetInvID, value.ToString())
              Integer.TryParse(value.ToString, intInvID)
            Case 1
              tblTable.SetCellValue(tblTable.InputRow, m_columnName.strNumber, value.ToString())
              Integer.TryParse(value.ToString, intNum)
            Case 2
              tblTable.SetCellValue(tblTable.InputRow, m_columnName.strQuantity, -1 * CDec(value.ToString()))
              Decimal.TryParse(value.ToString, decQty)
          End Select
          intCnt += 1
        Next
        tblTable.SetCellValue(tblTable.InputRow, m_columnName.strUserLotNo, zaikoDialog.RetStrLotNo)
        'calc
        intQtyPerCase = Integer.Parse(tblTable.GetCellValue(tblTable.InputRow, m_columnName.strQttPerCase).ToString)
        If intQtyPerCase = 0 Then intQtyPerCase = 1
        If decQty Mod intQtyPerCase = 0 Then
          decCase = decQty / CDec(intQtyPerCase)
        Else
          decCase = decQty
          intQtyPerCase = 1
        End If

        'setting
        tblTable.SetCellValue(tblTable.InputRow, m_columnName.strQttPerCase, intQtyPerCase)
        tblTable.SetCellValue(tblTable.InputRow, m_columnName.strNoOfCase, decCase)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SetLotDetails()
    Dim frmLotDetail As New frmLotDetails(connector)
    With frmLotDetail
      .lngDetailRow = m_IntRowIndex
      SetDetailFormData(.strLotNo, m_columnName.strUserLotNo, m_IntRowIndex)
      SetDetailFormData(.strSupLotNo, m_columnName.strSupLotNo, m_IntRowIndex)
      SetDetailFormData(.strItemName, m_columnName.strProductName, m_IntRowIndex)

      'Send current Data to frmLotDetail screen (In Edit mode, or in Create Mode when user set data in LotDetails scree, -> OK -> get back to Lot Details screen)      
      SetDetailFormData(.strManagementNum, m_columnName.strManageNo, m_IntRowIndex)

      SetDetailFormData(.dteShukka, m_columnName.strShukka, m_IntRowIndex)
      SetDetailFormData(.dteShiyou, m_columnName.strShiyou, m_IntRowIndex)
      SetDetailFormData(.dteShoumi, m_columnName.strShoumi, m_IntRowIndex)
      SetDetailFormData(.dteShouhi, m_columnName.strShouhi, m_IntRowIndex)
      
      SL_LMBClass.ReadOnlyRow()

      .strLotDetailLabel1 = SL_LMBClass.sl_lmb_label1
      .strLotDetailLabel2 = SL_LMBClass.sl_lmb_label2
      .strLotDetailLabel3 = SL_LMBClass.sl_lmb_label3
      .strLotDetailLabel4 = SL_LMBClass.sl_lmb_label4
      .strLotDetailLabel5 = SL_LMBClass.sl_lmb_label5
      .strLotDetailLabel6 = SL_LMBClass.sl_lmb_label6
      .strLotDetailLabel7 = SL_LMBClass.sl_lmb_label7
      .strLotDetailLabel8 = SL_LMBClass.sl_lmb_label8
      .strLotDetailLabel9 = SL_LMBClass.sl_lmb_label9
      .strLotDetailLabel10 = SL_LMBClass.sl_lmb_label10

      SetDetailFormData(.strLotDetailCode1, m_columnName.strLotID1, m_IntRowIndex)
      SetDetailFormData(.strLotDetailCode2, m_columnName.strLotID2, m_IntRowIndex)
      SetDetailFormData(.strLotDetailCode3, m_columnName.strLotID3, m_IntRowIndex)
      SetDetailFormData(.strLotDetailCode4, m_columnName.strLotID4, m_IntRowIndex)
      SetDetailFormData(.strLotDetailCode5, m_columnName.strLotID5, m_IntRowIndex)
      SetDetailFormData(.strLotDetailCode6, m_columnName.strLotID6, m_IntRowIndex)
      SetDetailFormData(.strLotDetailCode7, m_columnName.strLotID7, m_IntRowIndex)
      SetDetailFormData(.strLotDetailCode8, m_columnName.strLotID8, m_IntRowIndex)
      SetDetailFormData(.strLotDetailCode9, m_columnName.strLotID9, m_IntRowIndex)
      SetDetailFormData(.strLotDetailCode10, m_columnName.strLotID10, m_IntRowIndex)

      SetDetailFormData(.strLotDetailName1, m_columnName.strLotName1, m_IntRowIndex)
      SetDetailFormData(.strLotDetailName2, m_columnName.strLotName2, m_IntRowIndex)
      SetDetailFormData(.strLotDetailName3, m_columnName.strLotName3, m_IntRowIndex)
      SetDetailFormData(.strLotDetailName4, m_columnName.strLotName4, m_IntRowIndex)
      SetDetailFormData(.strLotDetailName5, m_columnName.strLotName5, m_IntRowIndex)
      SetDetailFormData(.strLotDetailName6, m_columnName.strLotName6, m_IntRowIndex)
      SetDetailFormData(.strLotDetailName7, m_columnName.strLotName7, m_IntRowIndex)
      SetDetailFormData(.strLotDetailName8, m_columnName.strLotName8, m_IntRowIndex)
      SetDetailFormData(.strLotDetailName9, m_columnName.strLotName9, m_IntRowIndex)
      SetDetailFormData(.strLotDetailName10, m_columnName.strLotName10, m_IntRowIndex)

      .ShowDialog()

      tblTable.SetCellValue(.lngDetailRow, m_columnName.strUserLotNo, .strLotNo)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strSupLotNo, .strSupLotNo)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strManageNo, .strManagementNum)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strShukka, .dteShukka)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strShiyou, .dteShiyou)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strShoumi, .dteShoumi)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strShouhi, .dteShouhi)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID1, .strLotDetailCode1)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID2, .strLotDetailCode2)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID3, .strLotDetailCode3)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID4, .strLotDetailCode4)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID5, .strLotDetailCode5)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID6, .strLotDetailCode6)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID7, .strLotDetailCode7)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID8, .strLotDetailCode8)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID9, .strLotDetailCode9)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotID10, .strLotDetailCode10)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName1, .strLotDetailName1)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName2, .strLotDetailName2)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName3, .strLotDetailName3)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName4, .strLotDetailName4)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName5, .strLotDetailName5)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName6, .strLotDetailName6)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName7, .strLotDetailName7)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName8, .strLotDetailName8)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName9, .strLotDetailName9)
      tblTable.SetCellValue(.lngDetailRow, m_columnName.strLotName10, .strLotDetailName10)
    End With
  End Sub

  Private Sub GetPreviousSlip()
    If HeaderLabel.State = LabelStateType.New Then
      If SL_NYKHClass.GetNewSlipID() > 0 Then
        'Display slip with max header
        If DisplaySlipByNo(SL_NYKHClass.sl_nykh_denno) Then 'Hien 01/25 id-> denno, <> 0 -> >0
          InitEditMode()
        End If
      Else
        Sunloft.Message.DisplayBox.ShowError(SLConstants.NotifyMessage.PREVIOUS_MESSAGE_NOT_FOUND)
      End If

    ElseIf HeaderLabel.State = LabelStateType.Modify Then
      If SL_NYKHClass.GetPrevSlipNo(SL_NYKHClass.sl_nykh_denno) > 0 Then 'Hien 01/25 id-> denno, <> 0 -> >0
        DisplaySlipByNo(SL_NYKHClass.sl_nykh_denno)
      Else
        Sunloft.Message.DisplayBox.ShowError(SLConstants.NotifyMessage.PREVIOUS_MESSAGE_NOT_FOUND)
      End If

    End If
  End Sub

  Private Sub GetNextSlip()
    'Only enable in Edit mode
    If HeaderLabel.State = LabelStateType.Modify Then

      If SL_NYKHClass.GetNextSlipNo(SL_NYKHClass.sl_nykh_denno) > 0 Then
        DisplaySlipByNo(SL_NYKHClass.sl_nykh_denno)
      Else
        Sunloft.Message.DisplayBox.ShowError(SLConstants.NotifyMessage.NEXT_MESSAGE_NOT_FOUND)
      End If

    End If
  End Sub

  Private Sub SlipSearch(sender As Object)
    Dim denpyoDialog = New SLDenSearchDialog(connector)
    Dim strSlipNo As String = denpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.WarehousingSlip)
    If String.IsNullOrEmpty(strSlipNo) = False Then
      ltxtWhNo.Text = denpyoDialog.returnValue()
      ltxtWhNo_Validating(sender, New CancelEventArgs(False))
    End If
  End Sub

  Private Sub Copy()
    InitCreateMode(False)
    ltxtWhNo.Text = String.Empty
    For intLpc As Integer = 0 To m_IntRowCount - 1
      tblTable.SetCellValue(intLpc, m_columnName.strConvertFlag, 0)
    Next
  End Sub

  Private Sub Save()
    If ValidateInput() Then
      Dim isRegister As Boolean = False
      If HeaderLabel.State = LabelStateType.New Then isRegister = True
      If SaveData(isRegister) Then
        InitCreateMode()
        InitCreateMode_ClearParameters()
        m_IsF12CloseFlag = True

      End If
    End If
  End Sub

  Private Sub Delete()
    Try
      Dim intConvertSlipNo As Integer = SL_NYKHClass.CheckConvertedSlip
      If intConvertSlipNo > 0 Then
        SLCmnFunction.ShowToolTip("仕入伝票へ" & SLConstants.NotifyMessage.CANNOT_DELETE_BY_CONVERTED & vbNewLine & "仕入伝票番号:" & intConvertSlipNo _
                                  , SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
        Exit Sub
      End If
      Dim result As DialogResult = Sunloft.Message.DisplayBox.ShowWarning(SLConstants.ConfirmMessage.DELETE_CONFIRM_MESSAGE)
      If result = Windows.Forms.DialogResult.Yes Then
        If SL_NYKHClass.Delete() Then
          InitCreateMode()
          InitCreateMode_ClearParameters()
        End If

      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub CreateNew()
    InitCreateMode()
    InitCreateMode_ClearParameters()
  End Sub

  Private Sub Reset()
    If HeaderLabel.State = LabelStateType.New Then

      InitCreateMode()
      InitCreateMode_ClearParameters()
    ElseIf HeaderLabel.State = LabelStateType.Modify Then

      'Clear table, display current slip
      tblTable.ClearCellValues()
      m_IntRowCount = 0
      DisplaySlipByNo(SL_NYKHClass.sl_nykh_denno, False)

    End If
  End Sub

  Private Sub DeleteRow()
    Dim result As Integer = MsgBox(SLConstants.ConfirmMessage.DELETE_ROW_CONFIRM_MESSAGE, MsgBoxStyle.YesNo, "確認")
    If result = DialogResult.Yes Then SLCmnFunction.DeleteRow(tblTable, m_IntRowIndex, m_IntRowCount, m_columnName.strNo)

  End Sub

  Private Sub InsertRow()
    SLCmnFunction.InsertRow(tblTable, m_IntRowIndex, m_IntRowCount, m_columnName.strNo, m_columnName.strProductCode, PRODUCT_COLUMN, PRODUCT_CODE_CELL)
    PcaCommandItemRefer.Enabled = True
    m_strSearchItem = m_columnName.strProductCode
  End Sub
#End Region

#Region "PCA command Manager"
  ''' <summary>
  ''' Event called when PCACommandManager is called (PCA Function Bar button is pressed/Select from Menu, press F～ key)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try
      Dim commandItem As PcaCommandItem = e.CommandItem
      Select Case commandItem.CommandName

        Case PcaCommandItemPrevious.CommandName 'F2

          GetPreviousSlip()

        Case PcaCommandItemNext.CommandName 'F3

          GetNextSlip()

        Case PcaCommandItemSlipSearch.CommandName 'F6

          SlipSearch(sender)

        Case PcaCommandItemLotSearch.CommandName 'F7

          SearchReturnLot()

        Case PcaCommandItemRefer.CommandName 'F8
          SearchItem(sender)

        Case PcaCommandItemLotDetails.CommandName 'F9

          lblSupName.Focus() 'To validate all other text boxes
          SetLotDetails()

        Case PcaCommandItemCopy.CommandName 'F11

          Copy()

        Case PcaCommandItemSave.CommandName 'F12

          lblSupName.Focus() 'Focus on a label to validate all other text boxes.
          Save()

        Case PcaCommandItemClose.CommandName 'F12

          Me.Close()

        Case PcaCommandItemDelete.CommandName 'Delete

          Delete()

        Case PcaCommandItemNew.CommandName

          CreateNew()

        Case PcaCommandItemReset.CommandName

          Reset()

        Case PcaCommandItemDeleteRow.CommandName

          DeleteRow()

        Case PcaCommandItemInsertRow.CommandName
          InsertRow()
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  '***********************************************************																														
  'Name          : PcaCommandManager1_UpdateCommandUI
  'Content       :  F12 = Close / Register
  'Return Value  : 
  'Argument      : slipNo, isClearDataFirst = False in case of Reset (Remain data in labels)
  'Created date  :  (PCA Sample)
  'Modified date :   by       Content: 
  '***********************************************************	
  Private Sub PcaCommandManager1_UpdateCommandUI(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.UpdateCommandUI
    Dim commandItem As PcaCommandItem = e.CommandItem
    'Change F12 caption+function between Save and Close
    If commandItem Is PcaCommandItemSave Then
      commandItem.Enabled = Not m_IsF12CloseFlag

    ElseIf commandItem Is PcaCommandItemClose Then
      commandItem.Enabled = m_IsF12CloseFlag
      If m_IsF12CloseFlag Then
        ToolStripMenuItemSave.Enabled = False
        ToolStripButtonSave.Enabled = False
      Else
        ToolStripButtonSave.Enabled = True
      End If
    End If

    PcaCommandItemInsertRow.Enabled = PcaCommandItemDeleteRow.Enabled
    PcaFunctionCommandLotDetails.Enabled = PcaCommandItemLotDetails.Enabled
    PcaFunctionCommandLotSearch.Enabled = PcaCommandItemLotSearch.Enabled

  End Sub
#End Region

#Region "Display"
  '***********************************************************																														
  'Name          : ToTokuisakiItems
  'Content       :  Display corresponding supplier information
  'Return Value  : 
  'Argument      : 
  'Created date  : 2015/11/17  by Lai Minh Hien (PCA Sample)
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub ToTokuisakiItems(ByVal CMS As CMS, ByVal RMS As RMS)
    Try
      If String.IsNullOrEmpty(CMS.cms_mei2) Then
        '名2が未設定時は名1を下段に
        Me.lblSupName.Text = Environment.NewLine & CMS.cms_mei1
      Else
        Me.lblSupName.Text = CMS.cms_mei1 & Environment.NewLine & CMS.cms_mei2
      End If

      If String.IsNullOrEmpty(CMS.cms_ad2) Then
        '住所2が未設定時は住所名1を下段に
        Me.lblAddr.Text = Environment.NewLine & CMS.cms_mail.PadRight(9) & CMS.cms_ad1
      Else
        Me.lblAddr.Text = CMS.cms_mail.PadRight(9) & CMS.cms_ad1 & Environment.NewLine & "　　　 　".PadRight(9) & CMS.cms_ad2
      End If

      Me.lblSupTel.Text = "TEL： " & CMS.cms_tel
      Me.lblSupFax.Text = "FAX： " & CMS.cms_fax

      If RMS.rms_tax.ToString.Length = 0 Then
        Me.lblSupTax.Text = "消費税： "
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  '***********************************************************																														
  'Name          : DisplaySlipByNo
  'Content       :  Display warehousing slip to screen (By slip no)
  'Return Value  : 
  'Argument      : slipNo, isClearDataFirst = False in case of Reset (Remain data in labels)
  'Created date  :  (PCA Sample)
  'Modified date :   by       Content: 
  '***********************************************************	
  Private Function DisplaySlipByNo(ByVal slipNo As Integer, Optional isClearDataFirst As Boolean = True) As Boolean
    Try

      If isClearDataFirst Then ClearData()

      If SL_NYKHClass.ReadByID(slipNo) Then

        HeaderLabel.State = LabelStateType.Modify
        PcaCommandItemNext.Enabled = True
        PcaCommandItemDelete.Enabled = True
        PcaCommandItemCopy.Enabled = True
        tblTable.Enabled = True

        DisplayHeaderToScreen()

        'Get details data to class
        SL_NYKDClass.sl_nykd_hid = SL_NYKHClass.sl_nykh_id
        If SL_NYKHClass.sl_nykh_return = enumSlipType.ReturnGoods Then
          NYKD_lists = SL_NYKDClass.ReadByHeaderIDRet()
        Else
          NYKD_lists = SL_NYKDClass.ReadByHeaderID()
        End If

        'Display to screen
        m_IntRowCount = DisplayDetailsToScreen(NYKD_lists)

        Return True
      End If
      Return False 'Slip number is not existed
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function DisplayDetailsToScreen(myTableDetails As SL_NYKD()) As Integer
    'Set from class to table
    Dim intCount As Integer = 0
    Dim decQty As Decimal = 0D

    tblTable.ClearCellValues()

    While intCount < myTableDetails.Length AndAlso Not myTableDetails(intCount) Is Nothing AndAlso Not myTableDetails(intCount).sl_nykd_scd Is Nothing
      Dim SMSClass As New SMS(connector)
      SMSClass.ReadByID(myTableDetails(intCount).sl_nykd_scd)
      Dim SL_SMSClass As New SL_SMS(connector)
      SL_SMSClass.ReadByProductCode(myTableDetails(intCount).sl_nykd_scd)

      tblTable.SetCellValue(intCount, m_columnName.strNo, myTableDetails(intCount).sl_nykd_eda)
      tblTable.SetCellValue(intCount, m_columnName.strProductCode, myTableDetails(intCount).sl_nykd_scd)
      tblTable.SetCellValue(intCount, m_columnName.strType, myTableDetails(intCount).sl_nykd_nkbn)
      tblTable.SetCellValue(intCount, m_columnName.strColor, myTableDetails(intCount).sl_nykd_color)
      tblTable.SetCellValue(intCount, m_columnName.strSize, myTableDetails(intCount).sl_nykd_size)
      tblTable.SetCellValue(intCount, m_columnName.strWarehouse, myTableDetails(intCount).sl_nykd_souko)
      tblTable.SetCellValue(intCount, m_columnName.strProductName, myTableDetails(intCount).sl_nykd_mei)
      tblTable.SetCellValue(intCount, m_columnName.strSpec, myTableDetails(intCount).sl_nykd_kikaku)

      Select Case myTableDetails(intCount).sl_nykd_tax
        Case 0 '非課税
          tblTable.SetCellValue(intCount, m_columnName.strRate, String.Empty)
          tblTable.SetCellValue(intCount, m_columnName.strTaxIncluded, SLConstants.TAX.NO_TAX)
        Case Else
          tblTable.SetCellValue(intCount, m_columnName.strRate, myTableDetails(intCount).sl_nykd_rate.ToString & "%")
          Select Case myTableDetails(intCount).sl_nykd_komi.ToString
            Case SLConstants.TAX.TAX_INCLUDED_CODE.ToString()
              tblTable.SetCellValue(intCount, m_columnName.strTaxIncluded, SLConstants.TAX.TAX_INCLUDED)
            Case Else
              tblTable.SetCellValue(intCount, m_columnName.strTaxIncluded, SLConstants.TAX.TAX_EXCLUDED)
          End Select
      End Select

      tblTable.SetCellValue(intCount, m_columnName.strQttPerCase, myTableDetails(intCount).sl_nykd_iri)
      tblTable.SetCellValue(intCount, m_columnName.strNoOfCase, myTableDetails(intCount).sl_nykd_hako)
      decQty = Math.Round(CDec(myTableDetails(intCount).sl_nykd_suryo), SMSClass.sms_sketa)
      If SL_NYKHClass.sl_nykh_return = enumSlipType.ReturnGoods Then decQty = Math.Abs(decQty) * -1
      tblTable.SetCellValue(intCount, m_columnName.strQuantity, Format(decQty, "#,##0." & New String("0"c, SMSClass.sms_sketa)))

      tblTable.SetCellValue(intCount, m_columnName.strUnit, myTableDetails(intCount).sl_nykd_tani)
      tblTable.SetCellValue(intCount, m_columnName.strNumber, myTableDetails(intCount).sl_nykd_honsu)
      tblTable.SetCellValue(intCount, m_columnName.strUnitPrice, Format(Math.Round(CDec(myTableDetails(intCount).sl_nykd_tanka), SMSClass.sms_tketa), "#,##0." & New String("0"c, SMSClass.sms_tketa)))
      tblTable.SetCellValue(intCount, m_columnName.strAmount, Format(CDec(CInt(myTableDetails(intCount).sl_nykd_kingaku)), "#,##0"))
      tblTable.SetCellValue(intCount, m_columnName.strSysLotNo, myTableDetails(intCount).sl_nykd_lotno)
      tblTable.SetCellValue(intCount, m_columnName.strUserLotNo, myTableDetails(intCount).sl_nykd_ulotno)
      tblTable.SetCellValue(intCount, m_columnName.strSupLotNo, myTableDetails(intCount).sl_nykd_alotno)
      tblTable.SetCellValue(intCount, m_columnName.strNote, myTableDetails(intCount).sl_nykd_biko)

      tblTable.SetCellValue(intCount, m_columnName.strCurrentNumber, myTableDetails(intCount).intCurrentNumber)
      tblTable.SetCellValue(intCount, m_columnName.strCurrentQuantity, CDec(myTableDetails(intCount).dblCurrentQuantity))

      tblTable.SetCellValue(intCount, m_columnName.strManageNo, myTableDetails(intCount).sl_nykd_kanrino)
      tblTable.SetCellValue(intCount, m_columnName.strShukka, myTableDetails(intCount).sl_nykd_sdldate)
      tblTable.SetCellValue(intCount, m_columnName.strShiyou, myTableDetails(intCount).sl_nykd_ubdate)
      tblTable.SetCellValue(intCount, m_columnName.strShoumi, myTableDetails(intCount).sl_nykd_bbdate)
      tblTable.SetCellValue(intCount, m_columnName.strShouhi, myTableDetails(intCount).sl_nykd_expdate)

      tblTable.SetCellValue(intCount, m_columnName.strLotID1, myTableDetails(intCount).sl_nykd_ldid1)
      tblTable.SetCellValue(intCount, m_columnName.strLotID2, myTableDetails(intCount).sl_nykd_ldid2)
      tblTable.SetCellValue(intCount, m_columnName.strLotID3, myTableDetails(intCount).sl_nykd_ldid3)
      tblTable.SetCellValue(intCount, m_columnName.strLotID4, myTableDetails(intCount).sl_nykd_ldid4)
      tblTable.SetCellValue(intCount, m_columnName.strLotID5, myTableDetails(intCount).sl_nykd_ldid5)
      tblTable.SetCellValue(intCount, m_columnName.strLotID6, myTableDetails(intCount).sl_nykd_ldid6)
      tblTable.SetCellValue(intCount, m_columnName.strLotID7, myTableDetails(intCount).sl_nykd_ldid7)
      tblTable.SetCellValue(intCount, m_columnName.strLotID8, myTableDetails(intCount).sl_nykd_ldid8)
      tblTable.SetCellValue(intCount, m_columnName.strLotID9, myTableDetails(intCount).sl_nykd_ldid9)
      tblTable.SetCellValue(intCount, m_columnName.strLotID10, myTableDetails(intCount).sl_nykd_ldid10)
      tblTable.SetCellValue(intCount, m_columnName.strLotName1, myTableDetails(intCount).sl_nykd_ldmei1)
      tblTable.SetCellValue(intCount, m_columnName.strLotName2, myTableDetails(intCount).sl_nykd_ldmei2)
      tblTable.SetCellValue(intCount, m_columnName.strLotName3, myTableDetails(intCount).sl_nykd_ldmei3)
      tblTable.SetCellValue(intCount, m_columnName.strLotName4, myTableDetails(intCount).sl_nykd_ldmei4)
      tblTable.SetCellValue(intCount, m_columnName.strLotName5, myTableDetails(intCount).sl_nykd_ldmei5)
      tblTable.SetCellValue(intCount, m_columnName.strLotName6, myTableDetails(intCount).sl_nykd_ldmei6)
      tblTable.SetCellValue(intCount, m_columnName.strLotName7, myTableDetails(intCount).sl_nykd_ldmei7)
      tblTable.SetCellValue(intCount, m_columnName.strLotName8, myTableDetails(intCount).sl_nykd_ldmei8)
      tblTable.SetCellValue(intCount, m_columnName.strLotName9, myTableDetails(intCount).sl_nykd_ldmei9)
      tblTable.SetCellValue(intCount, m_columnName.strLotName10, myTableDetails(intCount).sl_nykd_ldmei10)

      tblTable.SetCellValue(intCount, m_columnName.strDetailsID, myTableDetails(intCount).sl_nykd_id)
      tblTable.SetCellValue(intCount, m_columnName.strInvID, myTableDetails(intCount).intInventoryID)
      tblTable.SetCellValue(intCount, m_columnName.strRetInvID, myTableDetails(intCount).intInventoryID)
      tblTable.SetCellValue(intCount, m_columnName.strRetAllocateNum, myTableDetails(intCount).sl_nykd_honsu)
      tblTable.SetCellValue(intCount, m_columnName.strRetAllocateQty, myTableDetails(intCount).sl_nykd_suryo)
      tblTable.SetCellValue(intCount, m_columnName.strConvertFlag, myTableDetails(intCount).sl_nykd_convert)

      SetNoOfDecimalDigitByProduct(intCount, myTableDetails(intCount).sl_nykd_scd)

      intCount = intCount + 1
    End While
    Return intCount
  End Function


  Public Function DisplayHeaderToScreen() As Boolean
    Dim provider As CultureInfo = CultureInfo.CurrentCulture
    Try
      'Set Header from Class
      With SL_NYKHClass
        setType.CodeText = .sl_nykh_return.ToString
        ldtWhDate.Date = Date.ParseExact(.sl_nykh_uribi.ToString, "yyyyMMdd", provider)

        setSupCode.CodeText = .sl_nykh_tcd
        CodeTextValidating(, False) 'Previous slip -> previous slip (Same supCode)

        If .sl_nykh_hno <> 0 Then setOrderNo.CodeText = .sl_nykh_hno.ToString
        If .sl_nykh_sno <> 0 Then setPurNo.CodeText = .sl_nykh_sno.ToString

        ltxtWhNo2.Text = .sl_nykh_denno2
        setPerson.CodeText = .sl_nykh_jtan
        CodeTextPersonValidating(, False)

        setDepartment.CodeText = .sl_nykh_jbmn
        CodeTextDepartmentValidating(, False)

        setMemoCode.CodeText = .sl_nykh_tekcd
        CodeTextMemoCodeValidating(, False)

        setMemoContent.CodeText = .sl_nykh_tekmei

        setProject.CodeText = .sl_nykh_pjcode
        CodeTextProjectValidating(, False)

        ltxtCreatedBy.Text = .sl_nykh_insuser
        ltxtCreatedAt.Text = CStr(.sl_nykh_insdate)
        ltxtModifiedBy.Text = .sl_nykh_upduser
        If CStr(.sl_nykh_upddate) <> SLConstants.NOTHING_DATE Then ltxtModifiedAt.Text = CStr(.sl_nykh_upddate)

        ltxtWhNo.Text = .sl_nykh_denno.ToString()
      End With
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  ''' <summary>
  ''' Display warehousing slip to screen (By Order slip no)
  ''' </summary>
  ''' <param name="intOrderNo">slip no</param>
  ''' <param name="isClearDataFirst">False in case of Reset (Remain data in labels)</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DisplaySlipByOrderNo(ByVal intOrderNo As Integer, Optional ByVal isClearDataFirst As Boolean = True) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim provider As CultureInfo = CultureInfo.CurrentCulture

      If isClearDataFirst Then ClearData()

      'Get order slip data
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_HACH")
      If Not m_isShowDialog Then
        selectCommand.Parameters("@hach_jno").SetValue(intOrderNo)
      Else
        selectCommand.Parameters("@hach_id").SetValue(intOrderNo)
      End If
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        'Get header data
        HeaderLabel.State = LabelStateType.New
        PcaFunctionCommandNext.Enabled = False
        ToolStripButtonDelete.Enabled = False
        PcaFunctionCommandCopy.Enabled = False

        tblTable.Enabled = True
        setType.CodeText = SLConstants.WarehouseKbn.NORMAL_WAREHOUSING_CODE.ToString
        If reader.GetValue("hach_noki").ToString.Length > 0 Then ldtWhDate.Date = Date.ParseExact(reader.GetValue("hach_noki").ToString, "yyyyMMdd", provider)
        setSupCode.CodeText = reader.GetValue("hach_tcd").ToString
        CodeTextValidating(, False)
        setOrderNo.CodeText = reader.GetValue("hach_jno").ToString
        setPurNo.CodeText = String.Empty
        ltxtWhNo2.Text = String.Empty

        If reader.GetValue("hach_jtan").ToString.Trim <> String.Empty AndAlso reader.GetValue("hach_jtan").ToString.Trim <> "0000" Then
          setPerson.CodeText = reader.GetValue("hach_jtan").ToString
          CodeTextPersonValidating(, False)
        End If

        If reader.GetValue("hach_jbmn").ToString.Trim <> String.Empty AndAlso reader.GetValue("hach_jbmn").ToString.Trim <> "0000" Then
          setDepartment.CodeText = reader.GetValue("hach_jbmn").ToString
          CodeTextDepartmentValidating(, False)
        End If

        If reader.GetValue("hach_tekcd").ToString.Trim <> String.Empty AndAlso reader.GetValue("hach_tekcd").ToString.Trim <> "0000" Then
          setMemoCode.CodeText = reader.GetValue("hach_tekcd").ToString
          CodeTextMemoCodeValidating(, False)
        End If

        setMemoContent.CodeText = reader.GetValue("hach_tekmei").ToString

        If reader.GetValue("hach_pjcode").ToString.Trim <> String.Empty Then
          setProject.CodeText = reader.GetValue("hach_pjcode").ToString
          CodeTextProjectValidating(, False)
        End If

        m_intOrderSlipID = CInt(reader.GetValue("hach_id"))

        'Get details data
        selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_HACD")
        selectCommand.Parameters("@hacd_hid").SetValue(reader.GetValue("hach_id"))
        reader = selectCommand.ExecuteReader
        Dim intBranchCount As Integer = 0
        While reader.Read() = True
          tblTable.SetCellValue(intBranchCount, m_columnName.strNo, intBranchCount + 1)
          tblTable.SetCellValue(intBranchCount, m_columnName.strProductCode, reader.GetValue("hacd_scd"))

          tblTable.SetCellValue(intBranchCount, m_columnName.strColor, reader.GetValue("hacd_color"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strSize, reader.GetValue("hacd_size"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strWarehouse, reader.GetValue("hacd_souko"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strProductName, reader.GetValue("hacd_mei"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strSpec, reader.GetValue("hacd_kikaku"))

          Select Case reader.GetValue("hacd_tax").ToString.Trim
            Case "0" '非課税
              tblTable.SetCellValue(intBranchCount, m_columnName.strRate, String.Empty)
              tblTable.SetCellValue(intBranchCount, m_columnName.strTaxIncluded, SLConstants.TAX.NO_TAX)
            Case Else
              tblTable.SetCellValue(intBranchCount, m_columnName.strRate, reader.GetValue("hacd_rate").ToString & "%")
              Select Case reader.GetValue("hacd_komi").ToString
                Case SLConstants.TAX.TAX_INCLUDED_CODE.ToString()
                  tblTable.SetCellValue(intBranchCount, m_columnName.strTaxIncluded, SLConstants.TAX.TAX_INCLUDED)
                Case Else
                  tblTable.SetCellValue(intBranchCount, m_columnName.strTaxIncluded, SLConstants.TAX.TAX_EXCLUDED)
              End Select
          End Select

          tblTable.SetCellValue(intBranchCount, m_columnName.strQttPerCase, CInt(reader.GetValue("hacd_iri")))
          tblTable.SetCellValue(intBranchCount, m_columnName.strNoOfCase, CInt(reader.GetValue("hacd_hako")))
          tblTable.SetCellValue(intBranchCount, m_columnName.strQuantity, reader.GetValue("hacd_suryo"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strUnit, reader.GetValue("hacd_tani"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strUnitPrice, CInt(reader.GetValue("hacd_tanka")))
          tblTable.SetCellValue(intBranchCount, m_columnName.strAmount, CInt(reader.GetValue("hacd_kingaku")) - CInt(reader.GetValue("hacd_uchi")))
          tblTable.SetCellValue(intBranchCount, m_columnName.strNote, reader.GetValue("hacd_biko"))
          '2015/12/22 TNOGUCHI check purchase convert
          tblTable.SetCellValue(intBranchCount, m_columnName.strConvertFlag, CONVERT_FLAG_NONE)
          tblTable.SetCellValue(intBranchCount, m_columnName.strRetInvID, 0)
          tblTable.SetCellValue(intBranchCount, m_columnName.strRetAllocateID, 0)
          tblTable.SetCellValue(intBranchCount, m_columnName.strRetAllocateNum, 0)
          tblTable.SetCellValue(intBranchCount, m_columnName.strRetAllocateQty, 0)

          SetNoOfDecimalDigitByProduct(intBranchCount, reader.GetValue("hacd_scd").ToString)
          intBranchCount = intBranchCount + 1
        End While
        If intBranchCount > 0 Then m_IntRowCount = intBranchCount - 1
        Return True
      End If
      reader.Close()
      reader.Dispose()
      Return False 'Slip number is not existed
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  ''' DisplaySlipByPurNo
  ''' </summary>
  ''' <param name="intPurNo">パラメータ1</param>
  ''' <param name="isClearDataFirst">パラメータ2</param>
  ''' <returns>返り値</returns>
  ''' <remarks>Display warehousing slip to screen (By Purchase slip no)</remarks>
  Private Function DisplaySlipByPurNo(ByVal intPurNo As Integer, Optional ByVal isClearDataFirst As Boolean = True) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim provider As CultureInfo = CultureInfo.CurrentCulture

      If isClearDataFirst Then ClearData()

      'Get order slip data
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_NYKH")
      selectCommand.Parameters("@nykh_denno").SetValue(intPurNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() = True Then
        'Get header data
        HeaderLabel.State = LabelStateType.New
        PcaFunctionCommandNext.Enabled = False
        ToolStripButtonDelete.Enabled = False
        PcaFunctionCommandCopy.Enabled = False

        tblTable.Enabled = True
        setType.CodeText = SLConstants.WarehouseKbn.NORMAL_WAREHOUSING_CODE.ToString
        If reader.GetValue("nykh_uribi").ToString.Length > 0 Then ldtWhDate.Date = Date.ParseExact(reader.GetValue("nykh_uribi").ToString, "yyyyMMdd", provider)
        setSupCode.CodeText = reader.GetValue("nykh_tcd").ToString
        CodeTextValidating(, False)
        setOrderNo.CodeText = String.Empty
        setPurNo.CodeText = CStr(intPurNo)
        ltxtWhNo2.Text = String.Empty

        If reader.GetValue("nykh_jtan").ToString.Trim <> String.Empty AndAlso reader.GetValue("nykh_jtan").ToString.Trim <> "0000" Then
          setPerson.CodeText = reader.GetValue("nykh_jtan").ToString
          CodeTextPersonValidating(, False)
        End If

        If reader.GetValue("nykh_jbmn").ToString.Trim <> String.Empty AndAlso reader.GetValue("nykh_jbmn").ToString.Trim <> "0000" Then
          setDepartment.CodeText = reader.GetValue("nykh_jbmn").ToString
          CodeTextDepartmentValidating(, False)
        End If

        If reader.GetValue("nykh_tekcd").ToString.Trim <> String.Empty AndAlso reader.GetValue("nykh_tekcd").ToString.Trim <> "0000" Then
          setMemoCode.CodeText = reader.GetValue("nykh_tekcd").ToString
          CodeTextMemoCodeValidating(, False)
        End If

        setMemoContent.CodeText = reader.GetValue("nykh_tekmei").ToString

        If reader.GetValue("nykh_pjcode").ToString.Trim <> String.Empty Then
          setProject.CodeText = reader.GetValue("nykh_pjcode").ToString
          CodeTextProjectValidating(, False)
        End If

        m_intPurSlipID = CInt(reader.GetValue("nykh_id"))
        'Get details data
        selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_NYKD")
        selectCommand.Parameters("@nykd_hid").SetValue(reader.GetValue("nykh_id"))
        reader = selectCommand.ExecuteReader
        Dim intBranchCount As Integer = 0
        While reader.Read() = True
          tblTable.SetCellValue(intBranchCount, m_columnName.strNo, intBranchCount + 1)
          tblTable.SetCellValue(intBranchCount, m_columnName.strProductCode, reader.GetValue("nykd_scd"))

          tblTable.SetCellValue(intBranchCount, m_columnName.strColor, reader.GetValue("nykd_color"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strSize, reader.GetValue("nykd_size"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strWarehouse, reader.GetValue("nykd_souko"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strProductName, reader.GetValue("nykd_mei"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strSpec, reader.GetValue("nykd_kikaku"))

          'If reader.GetValue("nykd_tax").ToString = SLConstants.TAX.TAX_EXCLUDED_CODE.ToString Then
          '  tblTable.SetCellValue(intBranchCount, m_columnName.strTaxIncluded, SLConstants.TAX.TAX_EXCLUDED)
          'Else
          '  tblTable.SetCellValue(intBranchCount, m_columnName.strTaxIncluded, SLConstants.TAX.TAX_INCLUDED)
          'End If

          Select Case reader.GetValue("nykd_tax").ToString.Trim
            Case "0" '非課税
              tblTable.SetCellValue(intBranchCount, m_columnName.strRate, String.Empty)
              tblTable.SetCellValue(intBranchCount, m_columnName.strTaxIncluded, SLConstants.TAX.NO_TAX)
            Case Else
              tblTable.SetCellValue(intBranchCount, m_columnName.strRate, reader.GetValue("hacd_rate").ToString & "%")
              Select Case reader.GetValue("nykd_komi").ToString
                Case SLConstants.TAX.TAX_INCLUDED_CODE.ToString()
                  tblTable.SetCellValue(intBranchCount, m_columnName.strTaxIncluded, SLConstants.TAX.TAX_INCLUDED)
                Case Else
                  tblTable.SetCellValue(intBranchCount, m_columnName.strTaxIncluded, SLConstants.TAX.TAX_EXCLUDED)
              End Select
          End Select

          'tblTable.SetCellValue(intBranchCount, m_columnName.strRate, reader.GetValue("nykd_rate").ToString & "%")
          tblTable.SetCellValue(intBranchCount, m_columnName.strQttPerCase, CInt(reader.GetValue("nykd_iri")))
          tblTable.SetCellValue(intBranchCount, m_columnName.strNoOfCase, CInt(reader.GetValue("nykd_hako")))
          tblTable.SetCellValue(intBranchCount, m_columnName.strQuantity, reader.GetValue("nykd_suryo"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strUnit, reader.GetValue("nykd_tani"))
          tblTable.SetCellValue(intBranchCount, m_columnName.strUnitPrice, CInt(reader.GetValue("nykd_tanka")))
          tblTable.SetCellValue(intBranchCount, m_columnName.strAmount, CInt(reader.GetValue("nykd_kingaku")) - CInt(reader.GetValue("nykd_uchi")))
          tblTable.SetCellValue(intBranchCount, m_columnName.strNote, reader.GetValue("nykd_biko"))
          '2015/12/22 TNOGUCHI check purchase convert
          tblTable.SetCellValue(intBranchCount, m_columnName.strConvertFlag, CONVERT_FLAG_NONE)
          tblTable.SetCellValue(intBranchCount, m_columnName.strRetInvID, 0)
          tblTable.SetCellValue(intBranchCount, m_columnName.strRetAllocateID, 0)
          tblTable.SetCellValue(intBranchCount, m_columnName.strRetAllocateNum, 0)
          tblTable.SetCellValue(intBranchCount, m_columnName.strRetAllocateQty, 0)

          SetNoOfDecimalDigitByProduct(intBranchCount, reader.GetValue("nykd_scd").ToString)

          intBranchCount = intBranchCount + 1
        End While
        If intBranchCount > 0 Then m_IntRowCount = intBranchCount - 1
        Return True
      End If
      reader.Close()
      reader.Dispose()
      Return False 'Slip number is not existed
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function
#End Region

#Region "Validate"
  '★please standardize of validate events

  ''' <summary>
  ''' After user input slip number, if there's corresponding data, display it, move to edit mode. If not, delete slip number.
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub ltxtWhNo_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles ltxtWhNo.Validating
    Try
      Dim intSlipNo As Integer = 0

      If ltxtWhNo.Text = String.Empty Then Return
      If Not Integer.TryParse(ltxtWhNo.Text, intSlipNo) OrElse Not DisplaySlipByNo(intSlipNo) Then
        e.Cancel = True
        ltxtWhNo.Text = String.Empty
      Else
        InitEditMode()
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Order No Validating
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub setOrderNo_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setOrderNo.Validating

    Try
      Dim intSlipNo As Integer = 0

      If setOrderNo.CodeText = String.Empty Then Return
      If Not Integer.TryParse(setOrderNo.CodeText, intSlipNo) OrElse Not DisplaySlipByOrderNo(intSlipNo) Then
        e.Cancel = True
        setOrderNo.CodeText = String.Empty
      Else
        setOrderNo.Enabled = True
        setSupCode.Enabled = False
        setPurNo.Enabled = False
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
    
  End Sub

  ''' <summary>
  ''' Purchase No Validating
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub setPurNo_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setPurNo.Validating

    Try
      Dim intSlipNo As Integer = 0

      If setPurNo.CodeText = String.Empty Then Return
      If Not Integer.TryParse(setPurNo.CodeText, intSlipNo) OrElse Not DisplaySlipByPurNo(intSlipNo) Then
        e.Cancel = True
        setPurNo.CodeText = String.Empty
      Else
        setSupCode.Enabled = False
        setOrderNo.Enabled = False
        setPurNo.Enabled = True
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Validate Warehouse
  ''' </summary>
  ''' <param name="rowIndex"></param>
  ''' <param name="newValue"></param>
  ''' <param name="needMessageBox"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ValidateSoko(ByVal rowIndex As Integer, ByVal newValue As String, ByVal needMessageBox As Boolean) As Boolean
    Try
      Dim headerLabelText As String = String.Empty
      Dim EMS As New EMS(connector)
      Dim strWHName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.Warehouse, newValue)

      If strWHName.Trim.Length = 0 Then
        Return False
      End If
      Me.HeaderLabel.Text = headerLabelText

      If String.IsNullOrEmpty(newValue) Then
        Me.tblTable.SetCellValue(rowIndex, MeisaiTableDefines.SokoCode, String.Empty)
      Else
        Me.tblTable.SetCellValue(rowIndex, MeisaiTableDefines.SokoCode, newValue)
      End If

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  ''' <summary>
  ''' Validating cell
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks>Content: Check allocated quantity </remarks>
  Private Sub tblTable_ValidatingCell(sender As System.Object, args As PCA.Controls.CancelCellEventArgs) Handles tblTable.ValidatingCell
    Dim myTable As TSC.Kon.Tools.TscMeisaiTable = DirectCast(sender, TSC.Kon.Tools.TscMeisaiTable)
    Try
      Dim strProductCode = tblTable.GetCellValue(m_IntRowIndex, m_columnName.strProductCode)
      If strProductCode IsNot Nothing AndAlso strProductCode.ToString.Trim.Length > 0 Then PcaCommandItemLotSearch.Enabled = setType.CodeText = CStr(enumSlipType.ReturnGoods)

      Select Case args.CellName
        Case m_columnName.strProductCode
          If (args.NewValue Is Nothing OrElse args.NewValue.ToString = String.Empty) Then
            If Not (args.CurValue Is Nothing OrElse args.CurValue.ToString = String.Empty) Then args.Cancel = True
            Return
          End If
          If args.CurValue Is Nothing Then 'If input a new row
            If Not ValidateSyohin(m_IntRowIndex, args.NewValue.ToString, False) Then
              'product not found: return old value, keep focusing
              tblTable.SetCellValue(m_IntRowIndex, m_columnName.strProductCode, args.CurValue)
              tblTable.SetCellFocus(m_IntRowIndex, m_columnName.strProductCode)
            Else
              PcaCommandItemLotDetails.Enabled = True
              SetNoOfDecimalDigitByProduct(m_IntRowIndex, args.NewValue.ToString)
              PcaCommandItemDeleteRow.Enabled = True
            End If

          ElseIf Not args.NewValue.ToString = args.CurValue.ToString Then 'input a different value
            If Not ValidateSyohin(m_IntRowIndex, args.NewValue.ToString, False) Then
              'product not found: return old value, keep focusing
              tblTable.SetCellValue(m_IntRowIndex, m_columnName.strProductCode, args.CurValue)
              tblTable.SetCellFocus(m_IntRowIndex, m_columnName.strProductCode)
            Else
              PcaCommandItemLotDetails.Enabled = True
              SetNoOfDecimalDigitByProduct(m_IntRowIndex, args.NewValue.ToString)
            End If
          End If

        Case m_columnName.strQttPerCase, m_columnName.strNoOfCase 'If both are set, quantity = Number of cases * quantity per case

          Dim decQty As Decimal = 0D
          tblTable.SetCellValue(m_IntRowIndex, args.CellName, args.NewValue)

          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strQttPerCase)) Or _
           SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strNoOfCase)) Then

          ElseIf CInt(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strQttPerCase)) >= 0 AndAlso CInt(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strNoOfCase)) >= 0 Then

            decQty = CInt(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strQttPerCase)) * CInt(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strNoOfCase))
            If SL_NYKHClass.sl_nykh_return = enumSlipType.ReturnGoods Then decQty *= -1

            tblTable.SetCellValue(m_IntRowIndex, m_columnName.strQuantity, decQty)

          End If

        Case m_columnName.strNumber
          tblTable.SetCellValue(m_IntRowIndex, args.CellName, args.NewValue)

          Dim intNewValue As Integer
          Dim intCurrentNumber As Integer
          If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strCurrentNumber)) Then
            If Integer.TryParse(args.NewValue.ToString, intNewValue) AndAlso _
             Integer.TryParse(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strCurrentNumber).ToString, intCurrentNumber) Then

              Dim intAllocatedNumber = CInt(args.CurValue) - CInt(intCurrentNumber)

              If CInt(args.NewValue) < intAllocatedNumber Then 'Cannot edit to a number less than allocated quantity

                AllocatedErrorMessage(False, intAllocatedNumber, m_IntRowIndex)
                args.Cancel = True
              End If
            End If
          End If


        Case m_columnName.strQuantity
          'return goods
          If setType.CodeText = CStr(enumSlipType.ReturnGoods) AndAlso IsNumeric(args.NewValue.ToString) Then
            If CDbl(args.NewValue.ToString) > 0 Then tblTable.SetCellValue(args.Point.Y, m_columnName.strQuantity, CDbl(args.NewValue.ToString) * -1)

            If tblTable.GetCellValue(args.Point.Y, m_columnName.strUserLotNo) Is Nothing OrElse tblTable.GetCellValue(args.Point.Y, m_columnName.strUserLotNo).ToString.Trim.Length = 0 Then SearchReturnLot()
          End If
          'Check allocated quantity
          If HeaderLabel.State = LabelStateType.Modify Then
            tblTable.SetCellValue(m_IntRowIndex, args.CellName, args.NewValue)
            Dim decNewValue As Decimal
            If Decimal.TryParse(args.NewValue.ToString, decNewValue) Then
              Dim decAllocatedQuantity As Decimal = CDec(args.CurValue) - CDec(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strCurrentQuantity))
              If CDec(args.NewValue) < decAllocatedQuantity Then 'Cannot edit to a number less than allocated quantity
                AllocatedErrorMessage(True, decAllocatedQuantity, m_IntRowIndex)
                args.Cancel = True
                Return
              End If
            End If
          End If
          'If both are set, Amount = quantity * unit price
          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strUnitPrice)) OrElse SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strQuantity)) Then
          Else 'Set amount = quantity * unit price
            tblTable.SetCellValue(m_IntRowIndex, m_columnName.strAmount, CInt(CDec(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strUnitPrice)) * CDec(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strQuantity))))
          End If

        Case m_columnName.strUnitPrice
          'If both are set, Amount = quantity * unit price
          tblTable.SetCellValue(m_IntRowIndex, args.CellName, args.NewValue)
          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strUnitPrice)) Or SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strQuantity)) Then
          Else 'Set amount = quantity * unit price
            tblTable.SetCellValue(m_IntRowIndex, m_columnName.strAmount, CInt(CDec(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strUnitPrice)) * CDec(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strQuantity))))
          End If
        Case m_columnName.strWarehouse 'Set number of warehouse digits
          Dim EMSClass As New EMS(connector)
          Dim strStandardCode = SLCmnFunction.standardlizeCode(args.NewValue.ToString, AMS1Class.ams1_WarehouseLength)
          If strStandardCode.Trim.Length > 0 AndAlso EMSClass.ReadKbnMasterByIDAndKbn(SLConstants.EMS.Warehouse, strStandardCode).ToString.Trim = String.Empty Then
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.IMPROPRIETY_INPUTDATA, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, 8)
            tblTable.SetCellValue(m_IntRowIndex, args.CellName, args.CurValue)
          Else
            tblTable.SetCellValue(m_IntRowIndex, args.CellName, strStandardCode)
          End If
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' CodeTextPersonValidating
  ''' </summary>
  ''' <param name="e"></param>
  ''' <param name="NotCheckIfNoChange"></param>
  ''' <remarks></remarks>
  Private Sub CodeTextPersonValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True)
    Try
      HeaderLabel.Text = String.Empty
      Dim EMST = New EMST(connector)
      Dim EMSB = New EMSB(connector)
      Dim strPersonCode As String = SLCmnFunction.standardlizeCode(setPerson.CodeText.TrimEnd(), AMS1Class.ams1_PersonLength)
      EMST.ReadByCode(strPersonCode)

      If NotCheckIfNoChange Then
        If m_strCurCode = strPersonCode Then
          'no changes
          If Not String.IsNullOrEmpty(strPersonCode) AndAlso String.IsNullOrEmpty(Me.setPerson.NameText) Then
            Me.setPerson.NameText = EMST.emst_str
          End If
          Return
        End If
      End If

      If EMST.ReadByCode(strPersonCode) Then
        If EMSB.ReadByCode(EMST.emst_bmn) Then
          ValidatedTantosya(EMST, EMSB)
        Else
          ValidatedTantosya(EMST)
        End If

      Else
        If strPersonCode.Trim.Length > 0 Then DisplayBox.ShowNotify("担当者" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        If Me.IsValidate Then
          e.Cancel = True
        Else
          Me.setPerson.CodeText = m_strCurCode
        End If
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' CodeTextDepartmentValidating
  ''' </summary>
  ''' <param name="e"></param>
  ''' <param name="NotCheckIfNoChange"></param>
  ''' <remarks></remarks>
  Private Sub CodeTextDepartmentValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True)
    Try
      HeaderLabel.Text = String.Empty
      Dim EMSB = New EMSB(connector)
      Dim strBumoncode As String = SLCmnFunction.standardlizeCode(setDepartment.CodeText.TrimEnd(), AMS1Class.ams1_DepartmentLength)
      EMSB.ReadByCode(strBumoncode)

      If NotCheckIfNoChange Then
        If m_strCurCode = strBumoncode Then
          'No changes
          If Not String.IsNullOrEmpty(strBumoncode) AndAlso String.IsNullOrEmpty(Me.setDepartment.NameText) Then
            'Set department name
            Me.setDepartment.NameText = EMSB.emsb_str
          End If
          Return
        End If
      End If

      If EMSB.ReadByCode(strBumoncode) Then
        ValidatedBumon(EMSB)
      Else
        If strBumoncode.Trim.Length > 0 Then DisplayBox.ShowNotify("部門" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        If Me.IsValidate Then
          e.Cancel = True
        Else
          Me.setDepartment.CodeText = m_strCurCode
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' MemoCode_CodeTextValidating2
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub setMemoCode_CodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setMemoCode.CodeTextValidating2
    CodeTextMemoCodeValidating(e)
  End Sub

  ''' <summary>
  ''' CodeTextMemoCodeValidating
  ''' </summary>
  ''' <param name="e"></param>
  ''' <param name="NotCheckIfNoChange"></param>
  ''' <remarks></remarks>
  Private Sub CodeTextMemoCodeValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True)
    Try
      HeaderLabel.Text = String.Empty
      Dim EMS As New EMS(connector)
      Dim strMemoCode As String = SLCmnFunction.standardlizeCode(setMemoCode.CodeText.TrimEnd(), AMS1Class.ams1_MemoLength)
      Dim strName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, strMemoCode)
      If NotCheckIfNoChange Then
        If m_strCurCode = strMemoCode Then Return 'No changes
      End If

      If String.IsNullOrEmpty(strMemoCode) Then Me.setMemoContent.CodeText = String.Empty : Return

      If EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, strMemoCode).Length = 0 Then
        If strMemoCode.Trim.Length > 0 Then DisplayBox.ShowNotify("摘要" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        If Me.IsValidate Then e.Cancel = True
      Else
        ValidatedTekiyo(strMemoCode, strName)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub setProject_CodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setProject.CodeTextValidating2
    CodeTextProjectValidating(e)
  End Sub

  Private Sub CodeTextProjectValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True)
    Try
      HeaderLabel.Text = String.Empty
      Dim PJMS As New PJMS(connector)
      Dim strPJCode As String = Me.setProject.CodeText.TrimEnd()
      PJMS.ReadByCode(strPJCode)
      If NotCheckIfNoChange Then
        If m_strCurCode = strPJCode Then
          'No changes
          If Not String.IsNullOrEmpty(strPJCode) AndAlso String.IsNullOrEmpty(Me.setProject.NameText) Then Me.setProject.NameText = PJMS.pjms_name

          Return
        End If
      End If

      If PJMS.ReadByCode(strPJCode) Then
        ValidatedProject(PJMS)
      Else
        If strPJCode.Trim.Length > 0 Then DisplayBox.ShowNotify("案件" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        If Me.IsValidate Then
          e.Cancel = True
        Else
          setProject.CodeText = strPJCode
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Validate input before register
  ''' </summary>
  ''' <returns>True/False: Is input valid or not</returns>
  ''' <remarks></remarks>
  Private Function ValidateInput() As Boolean
    Try
      Dim SL_SMS As New SL_SMS(connector)
      Dim decQtyPerCase As Decimal = 0D '入数
      Dim intNoOfCase As Integer = 0 'ケース数
      Dim strErrMsg As String = String.Empty
      SL_LMBClass.ReadOnlyRow()

      If String.IsNullOrEmpty(setSupCode.CodeText) Then
        SLCmnFunction.ShowToolTip("仕入先コード" & SLConstants.NotifyMessage.INPUT_REQUIRED, String.Empty, ToolTipIcon.Warning, setSupCode, ToolTip1, Me)
        Return False
      End If

      Dim isEmptyTable As Boolean = True
      For intCount As Integer = 0 To m_IntRowCount - 1
        If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strProductCode)) Then isEmptyTable = False
      Next
      If isEmptyTable Then
        'Check if there's any product in details table
        SLCmnFunction.ShowToolTip("明細" & SLConstants.NotifyMessage.INPUT_REQUIRED, String.Empty, ToolTipIcon.Warning, setSupCode, ToolTip1, Me)
        Return False
      End If

      For intCount = 0 To m_IntRowCount
        If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strProductCode)) Then
          SL_SMS = New SL_SMS(connector)
          SL_SMS.ReadByProductCode(tblTable.GetCellValue(intCount, m_columnName.strProductCode).ToString)
          Dim PosForm As System.Drawing.Point
          PosForm.Y = tblTable.Location.Y + tblTable.HeadColumns.Height * (intCount + 2)

          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strWarehouse)) Then
            PosForm.X = CInt(WAREHOUSE_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
            SLCmnFunction.ShowToolTip((intCount + 1).ToString & "行目の倉庫" & SLConstants.NotifyMessage.INPUT_REQUIRED, String.Empty, ToolTipIcon.Warning, PosForm, ToolTip1, Me)
            Return False
          End If

          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strType)) Then
            PosForm.X = CInt(TYPE_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
            SLCmnFunction.ShowToolTip((intCount + 1).ToString & "行目の区分" & SLConstants.NotifyMessage.SELECT_REQUIRED, String.Empty, ToolTipIcon.Warning, PosForm, ToolTip1, Me)
            Return False
          End If

          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strQuantity)) OrElse CDec(tblTable.GetCellValue(intCount, m_columnName.strQuantity)) = 0D Then
            PosForm.X = CInt(QUANTITY_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
            SLCmnFunction.ShowToolTip((intCount + 1).ToString & "行目の数量" & SLConstants.NotifyMessage.INPUT_REQUIRED, String.Empty, ToolTipIcon.Warning, PosForm, ToolTip1, Me)
            Return False
          End If

          If Decimal.TryParse(tblTable.GetCellValue(intCount, m_columnName.strQttPerCase).ToString, decQtyPerCase) And _
             Integer.TryParse(tblTable.GetCellValue(intCount, m_columnName.strNoOfCase).ToString, intNoOfCase) Then
            If Decimal.Parse(tblTable.GetCellValue(intCount, m_columnName.strQuantity).ToString) <> decQtyPerCase * intNoOfCase Then
              PosForm.X = CInt(QUANTITY_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
              strErrMsg = "{0}行目の数量が{1}と{2}から求められた数量と異なります。"
              strErrMsg = String.Format(strErrMsg, (intCount + 1).ToString, m_columnName.strQttPerCase, m_columnName.strNoOfCase)
              SLCmnFunction.ShowToolTip(strErrMsg, String.Empty, ToolTipIcon.Warning, PosForm, ToolTip1, Me)
              Return False
            End If
          End If
          

          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strQttPerCase)) OrElse CInt(tblTable.GetCellValue(intCount, m_columnName.strQttPerCase)) = 0 Then
            tblTable.SetCellValue(intCount, m_columnName.strQttPerCase, 1)
          End If

          If SL_SMS.sl_sms_lkbn = 1 Then
            If SL_LMBClass.sl_lmb_autolot = SLConstants.SetLotType.Manual And tblTable.GetCellValue(intCount, m_columnName.strUserLotNo).ToString = String.Empty Then
              PosForm.X = CInt(LOT_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
              SLCmnFunction.ShowToolTip((intCount + 1).ToString & "行目のロットNo" & SLConstants.NotifyMessage.INPUT_REQUIRED, String.Empty, ToolTipIcon.Warning, PosForm, ToolTip1, Me)
              Return False
            End If
          End If
          If setType.CodeText = CStr(enumSlipType.ReturnGoods) Then

            If IsNothing(tblTable.GetCellValue(intCount, m_columnName.strRetInvID)) Then
              SLCmnFunction.ShowToolTip((intCount + 1).ToString & "行目の返品ロット" & SLConstants.NotifyMessage.SELECT_REQUIRED & "(F7でロット検索を行えます)", String.Empty _
                                        , ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemLotSearch.CommandId)
              Return False

            End If
          End If

        End If
      Next
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Sub ValidatedTantosya(ByVal EMST As EMST, Optional ByVal EMSB As EMSB = Nothing)
    Try
      Me.setPerson.CodeText = EMST.emst_kbn
      Me.setPerson.NameText = EMST.emst_str
      If Not EMSB Is Nothing Then
        Me.setDepartment.CodeText = EMSB.emsb_kbn
        Me.setDepartment.NameText = EMSB.emsb_str
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub ValidatedBumon(ByVal EMSB As EMSB)
    Try
      Me.setDepartment.CodeText = EMSB.emsb_kbn
      Me.setDepartment.NameText = EMSB.emsb_str
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub ValidatedProject(ByVal PJMS As PJMS)
    Try
      Me.setProject.CodeText = PJMS.pjms_code
      Me.setProject.NameText = PJMS.pjms_name
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub ValidatedTekiyo(ByVal strCode As String, ByVal strName As String)
    Try
      Me.setMemoCode.CodeText = strCode
      Me.setMemoContent.CodeText = strName.Trim
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub CodeTextValidating(Optional e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True)
    Try
      HeaderLabel.Text = String.Empty

      Dim RMS As New RMS(connector)
      Dim CMS As New CMS(connector)
      Dim strSupCode As String = SLCmnFunction.standardlizeCode(setSupCode.CodeText.TrimEnd(), AMS1Class.ams1_CustomerLength)
      If NotCheckIfNoChange Then
        If Not String.IsNullOrEmpty(strSupCode) AndAlso m_strCurCode = strSupCode Then
          setSupCode.CodeText = strSupCode
          Return
        End If
      End If

      If strSupCode.Length = 0 Then Exit Sub

      m_IntRowCount = 0
      If IsValidate Then
        IsValidate = False
        If String.IsNullOrEmpty(strSupCode) Then
          e.Cancel = True
          setSupCode.CodeText = m_strCurCode
          Return
        End If
        'validate supplier code
        If Not RMS.ReadByCode(strSupCode) Then
          'If there's error
          e.Cancel = True
          setSupCode.CodeText = m_strCurCode
          Return
        Else
          CMS.ReadByID(RMS.rms_cmsid)
          ToTokuisakiItems(CMS, RMS)
        End If
      Else
        'validate supplier code
        If Not RMS.ReadByCode(strSupCode) Then
          setSupCode.CodeText = m_strCurCode
          setSupCode.Focus()
        Else
          CMS.ReadByID(RMS.rms_cmsid)
          ToTokuisakiItems(CMS, RMS)
        End If
      End If

      If setSupCode.CodeText <> String.Empty Then
        setOrderNo.Enabled = False
        setPurNo.Enabled = False
        tblTable.Enabled = True
        m_IsF12CloseFlag = False
        m_SlipType = SlipType.Self
        setPerson.Focus()
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Function ValidateSyohin(ByVal rowIndex As Integer, ByVal newValue As String, ByVal needMessageBox As Boolean) As Boolean
    Try
      Dim strStandardCode As String = SLCmnFunction.standardlizeCode(newValue, AMS1Class.ams1_ProductLength) 'Standardlize no of product code digit
      'Load data Product data from Product code
      Dim SMSClass As SMS = New SMS(connector)
      Dim TaxClass As TAX = New TAX(connector)
      Dim SMSPClass As SMSP = New SMSP(connector)

      If SMSClass.ReadByID(strStandardCode) Then
        SMSPClass.ReadByID(strStandardCode)
        'Product found
        tblTable.SetCellValue(rowIndex, m_columnName.strType, SLConstants.WarehouseKbn.WAREHOUSE_ALL_CODE)
        tblTable.SetCellValue(rowIndex, m_columnName.strProductCode, strStandardCode)
        tblTable.SetCellValue(rowIndex, m_columnName.strProductName, SMSClass.sms_mei)
        tblTable.SetCellValue(rowIndex, m_columnName.strSpec, SMSClass.sms_kikaku)
        tblTable.SetCellValue(rowIndex, m_columnName.strColor, SMSClass.sms_color)
        tblTable.SetCellValue(rowIndex, m_columnName.strSize, SMSClass.sms_size)
        tblTable.SetCellValue(rowIndex, m_columnName.strWarehouse, SMSClass.sms_souko)
        If SMSClass.smsp_tax = 0 Then
          tblTable.SetCellValue(rowIndex, m_columnName.strTaxIncluded, SLConstants.TAX.NO_TAX)
        Else
          tblTable.SetCellValue(rowIndex, m_columnName.strTaxIncluded, SMSClass.smsp_komi)
        End If
        
        tblTable.SetCellValue(rowIndex, m_columnName.strQttPerCase, SMSClass.sms_iri)
        tblTable.SetCellValue(rowIndex, m_columnName.strUnit, SMSClass.sms_tani)
        tblTable.SetCellValue(rowIndex, m_columnName.strUnitPrice, Format(CDec(SMSClass.smsp_sitan), "#,##0." & New String("0"c, SMSClass.sms_tketa)))

        Dim taxRate As Decimal = SLCmnFunction.GetTaxRate(strStandardCode, Integer.Parse(ldtWhDate.Date.ToString("yyyyMMdd")), SMSPClass, TaxClass)

        tblTable.SetCellValue(rowIndex, m_columnName.strRate, taxRate.ToString & "%")

        If m_IntRowIndex = m_IntRowCount Then m_IntRowCount = m_IntRowCount + 1
        tblTable.SetCellValue(rowIndex, m_columnName.strQuantityNoOfDecimal, SMSClass.sms_sketa)
        tblTable.SetCellValue(rowIndex, m_columnName.strUnitPriceNoOfDecimal, SMSClass.sms_tketa)
        tblTable.SetCellValue(rowIndex, m_columnName.strUnitPriceNoOfDecimal, SMSClass.sms_tketa)
        tblTable.SetCellValue(rowIndex, m_columnName.strNoOfCase, 0)
        tblTable.SetCellValue(rowIndex, m_columnName.strNumber, 0)
        tblTable.SetCellValue(rowIndex, m_columnName.strQuantity, 0)
        tblTable.SetCellValue(rowIndex, m_columnName.strAmount, 0)

        SetLotNoEditable(strStandardCode)
        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function
#End Region

#Region "Search"
  ''' <summary>
  ''' Display search screen (Search items like press F8)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub SearchItem(ByVal sender As System.Object, Optional e As System.ComponentModel.CancelEventArgs = Nothing)
    Dim _CodeSet As PcaCodeSet = Nothing
    Dim itemName As String = String.Empty
    Dim newCode As String = String.Empty
    Dim location As Point = Tools.ControlTool.GetDialogLocation(Me.tblTable.RefButton)
    Dim strCurrentCode As String = String.Empty

    Dim EMST As New EMST(connector)
    Dim RMS As New RMS(connector)
    Dim EMSB As New EMSB(connector)
    Dim PJMS As New PJMS(connector)
    Dim EMS As New EMS(connector)

    Try
      Dim denpyoDialog As SLDenSearchDialog = New SLDenSearchDialog(connector)
      Dim masterDialog As SLMasterSearchDialog = New SLMasterSearchDialog(connector)

      Select Case sender.GetType.ToString
        Case GetType(PCA.Controls.PcaCodeSet).ToString, GetType(PCA.Controls.PcaCommandManager).ToString '
          If sender.GetType.ToString = GetType(PCA.Controls.PcaCodeSet).ToString Then
            _CodeSet = DirectCast(sender, PcaCodeSet)
            location = Tools.ControlTool.GetDialogLocation(_CodeSet.ReferButton)
          Else
            'location
          End If

          Select Case If(sender.GetType.ToString = GetType(PCA.Controls.PcaCodeSet).ToString, _CodeSet.Name, m_strSearchItem)
            Case setPerson.Name
              strCurrentCode = setPerson.CodeText
              newCode = masterDialog.ShowReferTantosyaDialog(strCurrentCode, location)
            Case setProject.Name
              strCurrentCode = setProject.CodeText
              newCode = masterDialog.ShowReferProjectDialog(strCurrentCode, location)
            Case setDepartment.Name
              strCurrentCode = setDepartment.CodeText
              newCode = masterDialog.ShowReferBumonDialog(strCurrentCode, location)
            Case setMemoCode.Name
              strCurrentCode = setMemoCode.CodeText
              newCode = masterDialog.ShowReferTekiyoDialog(strCurrentCode, location)
            Case setSupCode.Name
              strCurrentCode = setSupCode.CodeText
              newCode = masterDialog.ShowReferRmsDialog(strCurrentCode, location)
            Case setOrderNo.Name
              newCode = denpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.PCAPurchaseOrderSlip)
            Case setPurNo.Name
              newCode = denpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.PCAPurchaseSlip)
            Case Else
              SearchItemTable(sender, itemName, strCurrentCode, newCode, masterDialog, True)
              Return
          End Select

          If String.IsNullOrEmpty(newCode) Then
            If Not e Is Nothing Then e.Cancel = True
          Else
            If strCurrentCode = newCode Then
              'No changes
              Return
            End If

            Select Case If(sender.GetType.ToString = GetType(PCA.Controls.PcaCodeSet).ToString, _CodeSet.Name, m_strSearchItem)
              Case setPurNo.Name
                setPurNo.CodeText = newCode
                setPurNo_Validating(sender, e)
              Case setOrderNo.Name
                setOrderNo.CodeText = newCode
                setOrderNo_Validating(sender, e)
              Case setPerson.Name
                If EMST.ReadByCode(newCode) Then
                  If EMSB.ReadByCode(EMST.emst_bmn) Then
                    ValidatedTantosya(EMST, EMSB)
                  Else
                    ValidatedTantosya(EMST)
                  End If
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If

              Case setProject.Name
                If PJMS.ReadByCode(newCode) Then
                  ValidatedProject(PJMS)
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If

              Case setDepartment.Name
                If EMSB.ReadByCode(newCode) Then
                  ValidatedBumon(EMSB)
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If

              Case setMemoCode.Name
                Dim strName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, newCode)
                If strName.Trim.Length > 0 Then
                  ValidatedTekiyo(newCode, strName)
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If
              Case setSupCode.Name
                setSupCode.CodeText = newCode
                CodeTextValidating()
            End Select
          End If

        Case GetType(PCA.TSC.Kon.Tools.TscMeisaiTable).ToString
          SearchItemTable(sender, itemName, strCurrentCode, newCode, masterDialog)
      End Select

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SearchItemTable(ByVal sender As System.Object, strItemName As String, strCurrentCode As String, strNewCode As String, masterDialog As SLMasterSearchDialog, _
                              Optional ByVal isCommandManager As Boolean = False)
    Dim isGetData As Boolean = False
    Dim strNextCellName As String = String.Empty
    
    If isCommandManager Then
      strItemName = m_strSearchItem
    Else
      Dim _table = DirectCast(sender, PCA.TSC.Kon.Tools.TscMeisaiTable)
      strItemName = _table.InputName
    End If

    strCurrentCode = Me.tblTable.GetCellValue(m_IntRowIndex, strItemName).ToString.Trim
    Select Case strItemName
      Case MeisaiTableDefines.SyohinCode
        'Display sub screen
        strNewCode = masterDialog.ShowReferSmsDialog(strCurrentCode, location)
      Case MeisaiTableDefines.SokoCode

        strNewCode = masterDialog.ShowReferSokoDialog(strCurrentCode, location)
    End Select

    If String.IsNullOrEmpty(strNewCode) = False Then
      If strNewCode <> m_strCurCode Then
        Select Case strItemName
          Case MeisaiTableDefines.SyohinCode
            'Validate if there's any changes
            strNextCellName = m_columnName.strColor
            If ValidateSyohin(m_IntRowIndex, strNewCode, True) Then isGetData = True
          Case MeisaiTableDefines.SokoCode
            strNextCellName = m_columnName.strProductName
            If Me.ValidateSoko(m_IntRowIndex, strNewCode, True) Then isGetData = True
        End Select

        If Not isGetData Then
          'Validate error
          Me.tblTable.SetCellValue(m_IntRowIndex, strItemName, strCurrentCode)
          Me.tblTable.SetCellFocus(m_IntRowIndex, strItemName)
          Return
        End If
      End If
      Me.tblTable.SetCellValue(m_IntRowIndex, strItemName, strNewCode)
      If String.IsNullOrEmpty(strNextCellName) = False Then Me.tblTable.SetCellFocus(m_IntRowIndex, strNextCellName)
    Else
      Me.tblTable.SetCellFocus(m_IntRowIndex, strItemName)
      Return
    End If
  End Sub
#End Region

#Region "Item events"

  Private Sub ToolStripButtonClose_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButtonClose.Click, ToolStripMenuItemClose.Click
    Me.Close()
  End Sub

  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then connector.LogOffSystem()
  End Sub

  ''' <summary>
  ''' CodeTextEnter/Leave  Get Controller name, enable/disable F8 (Search)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks> (PCA Sample)</remarks>
  Private Sub tblTable_LostCellFocus(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.LostCellFocus
    If m_strSearchItem = m_columnName.strProductCode Or m_strSearchItem = m_columnName.strWarehouse Then
      m_strSearchItem = String.Empty
      PcaFunctionCommandRefer.Enabled = False
    End If
    PcaFunctionCommandLotDetails.Enabled = False
    PcaFunctionCommandLotSearch.Enabled = False
  End Sub

  Private Sub CodeSet_CodeTextEnter(sender As System.Object, e As System.EventArgs) Handles setSupCode.CodeTextEnter, setPerson.CodeTextEnter, setDepartment.CodeTextEnter, setMemoCode.CodeTextEnter, setProject.CodeTextEnter, setOrderNo.CodeTextEnter, setPurNo.CodeTextEnter
    Dim myCodeSet As PcaCodeSet
    Try
      Select Case sender.GetType.ToString
        Case "PCA.Controls.PcaCodeSet"
          myCodeSet = DirectCast(sender, PcaCodeSet)
          m_strSearchItem = myCodeSet.Name
      End Select
      PcaFunctionCommandRefer.Enabled = True
      IsValidate = False 'hien 12/04
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  Private Sub CodeSet_CodeTextLeave(sender As System.Object, e As System.EventArgs) Handles setSupCode.CodeTextLeave, setPerson.CodeTextLeave, setDepartment.CodeTextLeave, setMemoCode.CodeTextLeave, setProject.CodeTextLeave, setOrderNo.CodeTextLeave, setPurNo.CodeTextLeave
    Dim myCodeSet As PcaCodeSet
    Try
      Select Case sender.GetType.ToString
        Case "PCA.Controls.PcaCodeSet"
          myCodeSet = DirectCast(sender, PcaCodeSet)
          m_strSearchItem = String.Empty
          PcaFunctionCommandRefer.Enabled = False
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  Private Sub CodeSet_ClickReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setSupCode.ClickReferButton2, setPerson.ClickReferButton2, _
                                                                                                                     setDepartment.ClickReferButton2, setProject.ClickReferButton2, _
                                                                                                                     setMemoCode.ClickReferButton2, setOrderNo.ClickReferButton2, _
                                                                                                                     setPurNo.ClickReferButton2
    'Display Refer screen
    SearchItem(sender, e)
  End Sub

  Private Sub setSupCode_CodeTextValidating(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setSupCode.CodeTextValidating2
    CodeTextValidating(e)
  End Sub

  Private Sub setPerson_CodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setPerson.CodeTextValidating2
    CodeTextPersonValidating(e)
  End Sub

  Private Sub setDepartment_CodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setDepartment.CodeTextValidating2
    CodeTextDepartmentValidating(e)
  End Sub

#End Region

#Region "Table event"
  Private Sub tblTable_LeaveInputRow(sender As System.Object, args As PCA.Controls.InputRowEventArgs) Handles tblTable.LeaveInputRow
    PcaCommandItemDeleteRow.Enabled = False
  End Sub
  ''' <summary>
  ''' Get row index, set Lot no cell editable/uneditable based on Product code
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks>2015/11/30 by   Hien    Content: Check allocated quantity </remarks>
  Private Sub TscMeisaiTable1_EnterInputRow(sender As Object, args As PCA.Controls.InputRowEventArgs) Handles tblTable.EnterInputRow
    Try

      Dim strProductCode As String = Nothing
      If tblTable.GetCellValue(args.RowIndex, m_columnName.strProductCode) IsNot Nothing Then strProductCode = tblTable.GetCellValue(args.RowIndex, m_columnName.strProductCode).ToString.Trim

      m_IntRowIndex = args.RowIndex
      If Not SLCmnFunction.checkObjectNothingEmpty(strProductCode) Then
        PcaCommandItemLotDetails.Enabled = True
        SetLotNoEditable(strProductCode)
        SetNoOfDecimalDigitByProduct(m_IntRowIndex, strProductCode)
        SetProductCodeEditable(m_IntRowIndex)
      End If
      PcaCommandItemDeleteRow.Enabled = True

      If m_IntRowCount = m_IntRowIndex Then
        tblTable.BodyColumns(PRODUCT_COLUMN).Cells(PRODUCT_CODE_CELL).EditMode = True
        tblTable.BodyColumns(PRODUCT_COLUMN).Cells(WAREHOUSE_CODE_CELL).EditMode = True
      End If

      If strProductCode IsNot Nothing AndAlso strProductCode.ToString.Trim.Length > 0 Then PcaCommandItemLotSearch.Enabled = setType.CodeText = CStr(enumSlipType.ReturnGoods)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Get row index, set Lot no cell editable/uneditable based on Product code
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks>Content: Check allocated quantity </remarks>
  Private Sub tblTable_GotCellFocus(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.GotCellFocus
    Dim isLotNoEditable As Boolean = False
    Try
      tblTable.SelectHeaderCell(args)
      If m_IntRowIndex < m_IntRowCount Then ' Choose an exsisting row
        If Not tblTable.GetCellValue(m_IntRowIndex, m_columnName.strProductCode) Is Nothing AndAlso tblTable.GetCellValue(m_IntRowIndex, m_columnName.strProductCode).ToString <> String.Empty Then
          isLotNoEditable = True
        Else 'Add new row in the middle
          tblTable.SetCellFocus(m_IntRowIndex, m_columnName.strProductCode)
          tblTable.BodyColumns(PRODUCT_COLUMN).Cells(PRODUCT_CODE_CELL).EditMode = True
          PcaFunctionCommandRefer.Enabled = True
          m_strSearchItem = args.CellName
          Return
        End If
      ElseIf m_IntRowIndex = m_IntRowCount Then 'Choose a new row, focus on product code, or in edit mode, select last row?
        If Not tblTable.GetCellValue(m_IntRowIndex, m_columnName.strProductCode) Is Nothing Then
          If tblTable.GetCellValue(m_IntRowIndex, m_columnName.strProductCode).ToString = String.Empty Then
            isLotNoEditable = False
          Else
            isLotNoEditable = True
          End If
        End If

      End If
      'Set search item, enable Refer F8
      Select Case args.CellName
        Case m_columnName.strProductCode, m_columnName.strWarehouse
          PcaFunctionCommandRefer.Enabled = True
          m_strSearchItem = args.CellName
        Case Else
          PcaFunctionCommandRefer.Enabled = False
          m_strSearchItem = String.Empty
      End Select

      If isLotNoEditable Then
        SetLotNoEditable(tblTable.GetCellValue(m_IntRowIndex, m_columnName.strProductCode).ToString)
        SetProductCodeEditable(m_IntRowIndex)
      Else
        'Jump in the next empty row, focus on Product code
        If tblTable.GetCellValue(m_IntRowCount, m_columnName.strProductCode) Is Nothing Then
          setPerson.Focus()
          tblTable.SetCellValue(m_IntRowCount, m_columnName.strProductCode, String.Empty)
        End If
        tblTable.SetCellFocus(m_IntRowCount, m_columnName.strProductCode)

      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Search Table Details Items Master(Item, Warehouse ...)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks></remarks>
  Private Sub tblTable_ClickRefButton(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.ClickRefButton
    Try
      SearchItem(sender, New CancelEventArgs(False))
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
#End Region

#Region "Save Data"
  ''' <summary>
  ''' Save Data when press F12
  ''' </summary>
  ''' <param name="isCreate">Save new data in Create mode. = False in Edit mode</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SaveData(isCreate As Boolean) As Boolean
    Dim isSuccess As Boolean = True
    Dim session As ICustomSession
    Try
      session = connector.CreateTransactionalSession
      isSuccess = SaveHeader(isCreate, session)
      If isSuccess Then isSuccess = SaveDetails(isCreate, session)
      If isSuccess Then
        session.Commit()
        session.Dispose()
      Else
        session.Rollback()
        session.Dispose()
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function

  ''' <summary>
  ''' If cell is not empty, save cell text value to table
  ''' </summary>
  ''' <param name="isCreate">Save new data in Create mode. = False in Edit mode</param>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SaveHeader(isCreate As Boolean, ByRef session As ICustomSession) As Boolean 'Is Register or Edit
    Try
      With SL_NYKHClass
        If isCreate Then
          .sl_nykh_id = SL_NYKHClass.GetNewID
          .sl_nykh_denno = SL_NYKHClass.GetNewSlipID
        End If
        .sl_nykh_return = Integer.Parse(setType.CodeText)
        .sl_nykh_uribi = ldtWhDate.IntDate
        .sl_nykh_tcd = setSupCode.CodeText
        .sl_nykh_jtan = setPerson.CodeText
        .sl_nykh_tekcd = setMemoCode.CodeText
        .sl_nykh_tekmei = setMemoContent.CodeText
        If Not SLCmnFunction.checkObjectNothingEmpty(setOrderNo.CodeText) Then
          .sl_nykh_hno = Integer.Parse(setOrderNo.CodeText)
          .sl_nykh_datakbn = 1
          .sl_nykh_dataid = m_intOrderSlipID
        End If
        If Not SLCmnFunction.checkObjectNothingEmpty(setPurNo.CodeText) Then
          .sl_nykh_sno = Integer.Parse(setPurNo.CodeText)
          .sl_nykh_datakbn = 2
          .sl_nykh_dataid = m_intPurSlipID
        End If
        .sl_nykh_denno2 = ltxtWhNo2.Text
        .sl_nykh_jbmn = setDepartment.CodeText
        .sl_nykh_pjcode = setProject.CodeText

        If isCreate Then
          .sl_nykh_insdate = Now
          .sl_nykh_insuser = connector.UserId
        Else
          .sl_nykh_upddate = Now
          .sl_nykh_upduser = connector.UserId
        End If
      End With
      'Save class data to database
      If isCreate Then
        Return SL_NYKHClass.Insert(session)
      Else
        Return SL_NYKHClass.Update(session)
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
    End Try
  End Function

  Private Function CompareDeleteUnusedCell(isCreate As Boolean, ByRef isSuccess As Boolean, session As ICustomSession) As Boolean
    Try
      newDetailsList = New Collection
      oldDetailsList = New Collection
      If Not isCreate Then
        'Get all Details ID from Table (1)
        For intCount As Integer = 0 To m_IntRowCount - 1
          If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strDetailsID)) Then
            newDetailsList.Add(CInt(tblTable.GetCellValue(intCount, m_columnName.strDetailsID)), tblTable.GetCellValue(intCount, m_columnName.strDetailsID).ToString) 'Add to collection Value = Int, key = string -> to search .Contains()
          End If
        Next
        'Get all Details ID from HeaderID from Data base (2)
        SL_NYKDClass.sl_nykd_hid = SL_NYKHClass.sl_nykh_id
        If SL_NYKHClass.sl_nykh_return = enumSlipType.ReturnGoods Then
          DBDetailsList = SL_NYKDClass.ReadByHeaderIDRet()
        Else
          DBDetailsList = SL_NYKDClass.ReadByHeaderID()
        End If

        'Delete all Details ID in (2) but not in (1)
        For Each value In DBDetailsList
          oldDetailsList.Add(value.sl_nykd_id, value.sl_nykd_id.ToString)
          If Not value Is Nothing Then
            If isSuccess AndAlso Not newDetailsList.Contains(value.sl_nykd_id.ToString) Then
              SL_NYKDClass.sl_nykd_id = value.sl_nykd_id
              isSuccess = SL_NYKDClass.Delete(session)
              SL_ZDNClass.sl_zdn_datakbn = SLConstants.SL_ZDNSlipType.Warehousing
              SL_ZDNClass.sl_zdn_dataid = value.sl_nykd_id
              If isSuccess Then isSuccess = SL_ZDNClass.Delete(session)
            End If
          End If
        Next
      End If
      Return isSuccess
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function SaveDataToNYKDClass(ByRef intCount As Integer, ByVal edaCount As Integer, ByRef nykd_lists As SL_NYKD()) As Boolean
    Try
      Dim SMSClass As SMS = New SMS(connector)
      nykd_lists(intCount) = New SL_NYKD(connector)

      'Save all data to m_tableDetails class
      With nykd_lists(intCount)

        If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strDetailsID)) Then
          .isExisted = False
          Dim intMaxSL_NYKD_ID As Integer = .GetNewID() + intCount
          .sl_nykd_id = intMaxSL_NYKD_ID
          .sl_nykd_lotno = (SL_ZDNClass.GetNewSysLotID + intCount).ToString.Trim
        Else
          .isExisted = True
          .sl_nykd_id = CInt(tblTable.GetCellValue(intCount, m_columnName.strDetailsID))
          .sl_nykd_lotno = tblTable.GetCellValue(intCount, m_columnName.strSysLotNo).ToString.Trim
        End If
        .sl_nykd_hid = SL_NYKHClass.sl_nykh_id
        .sl_nykd_eda = edaCount
        SaveToClass(m_columnName.strType, intCount, .sl_nykd_nkbn)

        .sl_nykd_scd = tblTable.GetCellValue(intCount, m_columnName.strProductCode).ToString.Trim
        SaveToClass(m_columnName.strProductName, intCount, .sl_nykd_mei)
        SaveToClass(m_columnName.strSpec, intCount, .sl_nykd_kikaku)
        SaveToClass(m_columnName.strColor, intCount, .sl_nykd_color)
        SaveToClass(m_columnName.strSize, intCount, .sl_nykd_size)
        SaveToClass(m_columnName.strWarehouse, intCount, .sl_nykd_souko)

        SMSClass.ReadByID(tblTable.GetCellValue(intCount, m_columnName.strProductCode).ToString)
        If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strTaxIncluded)) Then
          .sl_nykd_tax = SMSClass.smsp_tax
        End If

        Select Case tblTable.GetCellValue(intCount, m_columnName.strTaxIncluded).ToString.Trim
          Case SLConstants.TAX.TAX_EXCLUDED
            .sl_nykd_komi = SLConstants.TAX.TAX_EXCLUDED_CODE
          Case SLConstants.TAX.TAX_INCLUDED
            .sl_nykd_komi = SLConstants.TAX.TAX_INCLUDED_CODE
          Case Else
            .sl_nykd_komi = SLConstants.TAX.TAX_EXCLUDED_CODE
        End Select

        SaveToClass(m_columnName.strQttPerCase, intCount, .sl_nykd_iri)
        SaveToClass(m_columnName.strNoOfCase, intCount, .sl_nykd_hako)
        SaveToClass(m_columnName.strQuantity, intCount, .sl_nykd_suryo)
        SaveToClass(m_columnName.strNumber, intCount, .sl_nykd_honsu)

        SaveToClass(m_columnName.strUnit, intCount, .sl_nykd_tani)

        SaveToClass(m_columnName.strUnitPrice, intCount, .sl_nykd_tanka)
        SaveToClass(m_columnName.strAmount, intCount, .sl_nykd_kingaku)

        SaveToClass(m_columnName.strNote, intCount, .sl_nykd_biko)

        If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strRate)) Then
          Dim strRateTemp As String = tblTable.GetCellValue(intCount, m_columnName.strRate).ToString.Trim
          .sl_nykd_rate = CInt(strRateTemp.Substring(0, strRateTemp.Length - 1)) 'Get Tax rate, remove "%" 
        End If

        'SaveToClass(m_columnName.strUserLotNo, intCount, SetUserLotNumberToSaveToClass(intCount)) 'Hien fix 01/25
        Dim SL_SMSClass As New SL_SMS(connector)
        SL_SMSClass.ReadByProductCode(.sl_nykd_scd)
        If SL_SMSClass.sl_sms_lkbn = 1 Then 'Set Lotno Not editable if product's lot flag = not use
          .sl_nykd_ulotno = SetUserLotNumberToSaveToClass(intCount)
        End If

        SaveToClass(m_columnName.strSupLotNo, intCount, .sl_nykd_alotno)
        SaveToClass(m_columnName.strManageNo, intCount, .sl_nykd_kanrino)

        SaveToClass(m_columnName.strShukka, intCount, .sl_nykd_sdldate)
        SaveToClass(m_columnName.strShiyou, intCount, .sl_nykd_ubdate)
        SaveToClass(m_columnName.strShoumi, intCount, .sl_nykd_bbdate)
        SaveToClass(m_columnName.strShouhi, intCount, .sl_nykd_expdate)

        SaveToClass(m_columnName.strLotID1, intCount, .sl_nykd_ldid1)
        SaveToClass(m_columnName.strLotID2, intCount, .sl_nykd_ldid2)
        SaveToClass(m_columnName.strLotID3, intCount, .sl_nykd_ldid3)
        SaveToClass(m_columnName.strLotID4, intCount, .sl_nykd_ldid4)
        SaveToClass(m_columnName.strLotID5, intCount, .sl_nykd_ldid5)
        SaveToClass(m_columnName.strLotID6, intCount, .sl_nykd_ldid6)
        SaveToClass(m_columnName.strLotID7, intCount, .sl_nykd_ldid7)
        SaveToClass(m_columnName.strLotID8, intCount, .sl_nykd_ldid8)
        SaveToClass(m_columnName.strLotID9, intCount, .sl_nykd_ldid9)
        SaveToClass(m_columnName.strLotID10, intCount, .sl_nykd_ldid10)

        SaveToClass(m_columnName.strLotName1, intCount, .sl_nykd_ldmei1)
        SaveToClass(m_columnName.strLotName2, intCount, .sl_nykd_ldmei2)
        SaveToClass(m_columnName.strLotName3, intCount, .sl_nykd_ldmei3)
        SaveToClass(m_columnName.strLotName4, intCount, .sl_nykd_ldmei4)
        SaveToClass(m_columnName.strLotName5, intCount, .sl_nykd_ldmei5)
        SaveToClass(m_columnName.strLotName6, intCount, .sl_nykd_ldmei6)
        SaveToClass(m_columnName.strLotName7, intCount, .sl_nykd_ldmei7)
        SaveToClass(m_columnName.strLotName8, intCount, .sl_nykd_ldmei8)
        SaveToClass(m_columnName.strLotName9, intCount, .sl_nykd_ldmei9)
        SaveToClass(m_columnName.strLotName10, intCount, .sl_nykd_ldmei10)
        '2015/12/22 TNOGUCHI
        SaveToClass(m_columnName.strConvertFlag, intCount, .sl_nykd_convert)
      End With
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  ''' Save Details Data (Table)
  ''' </summary>
  ''' <param name="isCreate">Save new data in Create mode. = False in Edit mode</param>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SaveDetails(isCreate As Boolean, ByRef session As ICustomSession) As Boolean
    Dim isSuccess As Boolean = True
    Dim intRetInvID As Integer = 0
    Dim intRetAllocateID As Integer = 0
    Try
      CompareDeleteUnusedCell(isCreate, isSuccess, session)

      Dim nykd_lists() As SL_NYKD
      ReDim nykd_lists(m_IntRowCount)
      Dim intedaCount As Integer = 0
      Dim intStockID As Integer = 0
      For intCount = 0 To m_IntRowCount - 1 'Create in DB if new row, Update if existing row. Count number of new rows (to set system lot number)

        If isSuccess Then
          If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strProductCode)) Then

            SaveDataToNYKDClass(intCount, intedaCount, nykd_lists)
            intedaCount += 1

            'Register details table in SL_NYKD
            If isCreate OrElse Not oldDetailsList.Contains(nykd_lists(intCount).sl_nykd_id.ToString) Then
              isSuccess = nykd_lists(intCount).Insert(session)
            Else
              isSuccess = nykd_lists(intCount).Update(session)
              intStockID = CInt(tblTable.GetCellValue(intCount, m_columnName.strInvID))
            End If

            If isSuccess Then
              If SL_NYKHClass.sl_nykh_return = enumSlipType.ReturnGoods Then
                nykd_lists(intCount).isExisted = True
                intStockID = CInt(tblTable.GetCellValue(intCount, m_columnName.strRetInvID))
              End If

              SetToZDN(nykd_lists, SL_NYKHClass, intCount, intStockID, session)
              If SL_NYKHClass.sl_nykh_return = enumSlipType.ReturnGoods Then

                '在庫IDがある場合(登録対象のIDがある場合)
                If Not IsNothing(tblTable.GetCellValue(intCount, m_columnName.strRetInvID)) AndAlso _
                 Integer.TryParse(tblTable.GetCellValue(intCount, m_columnName.strRetInvID).ToString, intRetInvID) Then
                  Dim SL_ZHKClass As SL_ZHK = New SL_ZHK(connector)
                  SetToZHK(SL_ZHKClass, SL_NYKHClass, nykd_lists(intCount), intCount, intRetInvID)

                  If SL_ZDNClass.UpdateNumberQuantity(False, session) Then
                    If Not isCreate Then
                      '修正前で戻します。
                      SL_ZDNClass.sl_zdn_nyukohonsu = CInt(tblTable.GetCellValue(intCount, m_columnName.strRetAllocateNum)) * -1
                      SL_ZDNClass.sl_zdn_nyukosuryo = Math.Abs(CInt(tblTable.GetCellValue(intCount, m_columnName.strRetAllocateQty))) * -1
                      If Not SL_ZDNClass.UpdateNumberQuantity(False, session) Then

                      End If
                      'Return true
                    End If
                    'Add Inventory Allocate Table (SL_ZHK)
                    If Not isCreate Then
                      SL_ZHKClass.DeleteInventory(session, True, True)
                    End If
                    If SL_ZHKClass.Insert(session) Then isSuccess = True

                  Else
                    isSuccess = False
                  End If
                End If
              Else
                If isCreate OrElse Not oldDetailsList.Contains(nykd_lists(intCount).sl_nykd_id.ToString) Then
                  isSuccess = SL_ZDNClass.Insert(session)
                Else
                  isSuccess = SL_ZDNClass.Update(session)
                End If
              End If
            Else
              isSuccess = False
            End If
          End If
        End If
      Next

      Return isSuccess
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function SetToZDN(ByVal m_tableDetails As SL_NYKD(), ByVal m_tableHeader As SL_NYKH, ByVal intCount As Integer, ByVal intInventoryID As Integer, ByRef session As ICustomSession) As Boolean
    Dim sum_sl_zhk_honsu As Integer
    Dim sum_sl_zhk_suryo As Decimal
    Try
      SL_ZDNClass.GetZHKAllocated(sum_sl_zhk_honsu, sum_sl_zhk_suryo, m_tableDetails(intCount).sl_nykd_id)
      With SL_ZDNClass
        If m_tableDetails(intCount).isExisted Then
          SL_ZDNClass.sl_zdn_id = intInventoryID
        Else
          SL_ZDNClass.sl_zdn_id = SL_ZDNClass.GetNewID + intCount
        End If

        .sl_zdn_dataid = m_tableDetails(intCount).sl_nykd_id
        .sl_zdn_datakbn = SLConstants.SL_ZDNSlipType.Warehousing
        .sl_zdn_denno = m_tableHeader.sl_nykh_denno
        .sl_zdn_lotno = m_tableDetails(intCount).sl_nykd_lotno
        .sl_zdn_ulotno = m_tableDetails(intCount).sl_nykd_ulotno
        .sl_zdn_alotno = m_tableDetails(intCount).sl_nykd_alotno
        .sl_zdn_scd = m_tableDetails(intCount).sl_nykd_scd
        .sl_zdn_souko = m_tableDetails(intCount).sl_nykd_souko
        .sl_zdn_uribi = m_tableHeader.sl_nykh_uribi
        .sl_zdn_honsu = m_tableDetails(intCount).sl_nykd_honsu - sum_sl_zhk_honsu
        .sl_zdn_suryo = CDec(m_tableDetails(intCount).sl_nykd_suryo - sum_sl_zhk_suryo)
        .sl_zdn_nyukohonsu = Math.Abs(m_tableDetails(intCount).sl_nykd_honsu)
        .sl_zdn_nyukosuryo = Math.Abs(CDec(m_tableDetails(intCount).sl_nykd_suryo))
        .sl_zdn_tanka = m_tableDetails(intCount).sl_nykd_tanka
        .sl_zdn_tax = m_tableDetails(intCount).sl_nykd_rate
        .sl_zdn_kanrino = m_tableDetails(intCount).sl_nykd_kanrino
        .sl_zdn_sdldate = m_tableDetails(intCount).sl_nykd_sdldate
        .sl_zdn_ubdate = m_tableDetails(intCount).sl_nykd_ubdate
        .sl_zdn_bbdate = m_tableDetails(intCount).sl_nykd_bbdate
        .sl_zdn_expdate = m_tableDetails(intCount).sl_nykd_expdate
        .sl_zdn_ldid1 = m_tableDetails(intCount).sl_nykd_ldid1
        .sl_zdn_ldid2 = m_tableDetails(intCount).sl_nykd_ldid2
        .sl_zdn_ldid3 = m_tableDetails(intCount).sl_nykd_ldid3
        .sl_zdn_ldid4 = m_tableDetails(intCount).sl_nykd_ldid4
        .sl_zdn_ldid5 = m_tableDetails(intCount).sl_nykd_ldid5
        .sl_zdn_ldid6 = m_tableDetails(intCount).sl_nykd_ldid6
        .sl_zdn_ldid7 = m_tableDetails(intCount).sl_nykd_ldid7
        .sl_zdn_ldid8 = m_tableDetails(intCount).sl_nykd_ldid8
        .sl_zdn_ldid9 = m_tableDetails(intCount).sl_nykd_ldid9
        .sl_zdn_ldid10 = m_tableDetails(intCount).sl_nykd_ldid10
        .sl_zdn_ldmei1 = m_tableDetails(intCount).sl_nykd_ldmei1
        .sl_zdn_ldmei2 = m_tableDetails(intCount).sl_nykd_ldmei2
        .sl_zdn_ldmei3 = m_tableDetails(intCount).sl_nykd_ldmei3
        .sl_zdn_ldmei4 = m_tableDetails(intCount).sl_nykd_ldmei4
        .sl_zdn_ldmei5 = m_tableDetails(intCount).sl_nykd_ldmei5
        .sl_zdn_ldmei6 = m_tableDetails(intCount).sl_nykd_ldmei6
        .sl_zdn_ldmei7 = m_tableDetails(intCount).sl_nykd_ldmei7
        .sl_zdn_ldmei8 = m_tableDetails(intCount).sl_nykd_ldmei8
        .sl_zdn_ldmei9 = m_tableDetails(intCount).sl_nykd_ldmei9
        .sl_zdn_ldmei10 = m_tableDetails(intCount).sl_nykd_ldmei10
        .sl_zdn_relay_zdnid = .sl_zdn_relay_zdnid '???
        .sl_zdn_iri = .sl_zdn_iri

      End With
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  ''' temporary function -> when PCAIF code change, rewrite this function
  ''' </summary>
  ''' <param name="_sl_zhk"></param>
  ''' <param name="intRow"></param>
  ''' <param name="intAllocateID"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SetToZHK(ByRef _sl_zhk As SL_ZHK, ByVal _header As SL_NYKH, ByVal _detail As SL_NYKD, ByVal intRow As Integer, ByVal intAllocateID As Integer) As Boolean
    Dim is_success As Boolean = True
    Try
      With _sl_zhk
        .sl_zhk_dataid = _detail.sl_nykd_id
        .sl_zhk_zdn_id = intAllocateID
        .sl_zhk_datakbn = SLConstants.SL_ZHKSlipType.ReturnWarehousing
        .sl_zhk_date = _header.sl_nykh_uribi

        .sl_zhk_honsu = _detail.sl_nykd_honsu
        .sl_zhk_suryo = Math.Abs(CDec(_detail.sl_nykd_suryo))
        .sl_zhk_seq = 1  'only 1 lot of each row
        .sl_zhk_insdate = Now
        .sl_zhk_insuser = connector.UserId
      End With
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      is_success = False
    End Try
    Return is_success
  End Function

  ''' <summary>
  ''' If cell is not empty, save cell text value to table
  ''' </summary>
  ''' <param name="strColumnName">cell column name</param>
  ''' <param name="intCount">row index</param>
  ''' <param name="strTableDetailsName">table name</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SaveToClass(strColumnName As String, intCount As Integer, ByRef strTableDetailsName As String) As Boolean
    If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, strColumnName)) Then
      strTableDetailsName = tblTable.GetCellValue(intCount, strColumnName).ToString.Trim
    End If
    Return True
  End Function

  ''' <summary>
  ''' If cell is not empty, save cell number value to table
  ''' </summary>
  ''' <param name="strColumnName">cell column name</param>
  ''' <param name="intCount">row index</param>
  ''' <param name="strTableDetailsName">table name</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SaveToClass(strColumnName As String, intCount As Integer, ByRef strTableDetailsName As Integer) As Boolean
    If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, strColumnName)) Then
      strTableDetailsName = CInt(tblTable.GetCellValue(intCount, strColumnName).ToString)
    End If
    Return True
  End Function

  ''' <summary>
  ''' If cell is not empty, save cell number value to table
  ''' </summary>
  ''' <param name="strColumnName">cell column name</param>
  ''' <param name="intCount">row index</param>
  ''' <param name="strTableDetailsName">table name</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SaveToClass(strColumnName As String, intCount As Integer, ByRef strTableDetailsName As Decimal) As Boolean
    If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, strColumnName)) Then
      strTableDetailsName = CDec(tblTable.GetCellValue(intCount, strColumnName).ToString)
    End If
    Return True
  End Function

  Private Function SaveToClass(strColumnName As String, intCount As Integer, ByRef strTableDetailsName As Double) As Boolean
    If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, strColumnName)) Then
      strTableDetailsName = CDbl(tblTable.GetCellValue(intCount, strColumnName).ToString)
    End If
    Return True
  End Function

  Private Function SetUserLotNumberToSaveToClass(ByVal intCount As Integer) As String
    Dim strUserLotNo As String = String.Empty
    Try
      If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, m_columnName.strUserLotNo)) Then
        'If lot no is not blank
        strUserLotNo = tblTable.GetCellValue(intCount, m_columnName.strUserLotNo).ToString.Trim
      Else
        'If it is blank, check format (in Basic settings screen). 
        SL_LMBClass.ReadOnlyRow()
        If SL_LMBClass.sl_lmb_lotfmt = String.Empty Then
          'If format is blank, set MAX Lot no  + 1
          Dim MaxLot As Integer = 0
          SL_ZDNClass.GetNewUserLotID()
          If Integer.TryParse(SL_ZDNClass.sl_zdn_ulotno, MaxLot) Then strUserLotNo = MaxLot.ToString.Trim
        End If
      End If

      Return strUserLotNo
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
    End Try
  End Function

#End Region

  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
    '
    Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
    Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子

    If (keyModifiers <> Keys.Shift AndAlso (keyCode = Keys.Tab OrElse keyCode = Keys.Enter)) _
    OrElse keyCode = Keys.Right _
    OrElse keyCode = Keys.Down Then
      'Validate if user press Enter/Tab +  Anykey except Shift, →, ↓
      Me.IsValidate = True
    ElseIf (keyModifiers = Keys.Shift AndAlso (keyCode = Keys.Tab OrElse keyCode = Keys.Enter)) _
    OrElse keyCode = Keys.Left _
    OrElse keyCode = Keys.Up Then
      'Not validate if user press Shift + Tab, Shift + Enter, ←, ↑
      Me.IsValidate = False
    End If

    Return MyBase.ProcessDialogKey(keyData)

  End Function

  Private Sub setType_CodeTextChanged(sender As System.Object, e As System.EventArgs) Handles setType.CodeTextChanged
    Try
      'Type changes: Normal shipping & Return goods
      Select Case setType.CodeText
        Case CStr(SLConstants.WarehouseKbn.NORMAL_WAREHOUSING_CODE)
          setType.NameText = SLConstants.WarehouseKbn.NORMAL_WAREHOUSING
        Case CStr(SLConstants.WarehouseKbn.RETURN_GOODS_CODE)
          setType.NameText = SLConstants.WarehouseKbn.RETURN_GOODS
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SetDetailFormData(ByRef objTarget As String, ByVal strColName As String, ByVal intRowIndex As Integer)
    Try
      If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intRowIndex, strColName)) Then objTarget = tblTable.GetCellValue(intRowIndex, strColName).ToString
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SetDetailFormData(ByRef objTarget As Integer, ByVal strColName As String, ByVal intRowIndex As Integer)
    Try
      Dim intValue As Integer = 0
      If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intRowIndex, strColName)) Then
        Integer.TryParse(tblTable.GetCellValue(intRowIndex, strColName).ToString, intValue)
        If intValue > 0 Then objTarget = intValue
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
End Class

Public Class ColumnName
  Public Property projectName As String = "プロジェクト"
  Public Property strNo As String = String.empty
  Public Property strType As String = "区分"
  Public Property strProductCode As String = MeisaiTableDefines.SyohinCode 'Set the same as table column name in library
  Public Property strProductCode2 As String = "商品コード" 'For display header column
  Public Property strColor As String = "色"
  Public Property strSize As String = "サイズ"
  Public Property strWarehouse As String = MeisaiTableDefines.SokoCode
  Public Property strWarehouse2 As String = "倉庫"
  Public Property strProductName As String = "商品名"
  Public Property strSpec As String = "規格・型番"
  Public Property strTaxIncluded As String = "税区分"
  Public Property strRate As String = "税率"
  Public Property strNumber As String = "本数"
  Public Property strQttPerCase As String = "入数"
  Public Property strNoOfCase As String = "箱数"
  Public Property strQuantity As String = "数量"
  Public Property strQuantityNoOfDecimal As String = "数量小数桁数"
  Public Property strUnit As String = "単位"
  Public Property strUnitPrice As String = "単価"
  Public Property strUnitPriceNoOfDecimal As String = "単価小数桁数"
  Public Property strAmount As String = "金額"
  Public Property strSysLotNo As String = "システムロットNo"
  Public Property strUserLotNo As String = "ロットNo"
  Public Property strSupLotNo As String = "相手先ロットNo"
  Public Property strNote As String = "備考"
  Public Property strManageNo As String = "管理番号"
  Public Property strShukka As String = "出荷期限"
  Public Property strShiyou As String = "使用期限"
  Public Property strShoumi As String = "賞味期限"
  Public Property strShouhi As String = "消費期限"
  Public Property strLotID1 As String = "ロット詳細ID1"
  Public Property strLotID2 As String = "ロット詳細ID2"
  Public Property strLotID3 As String = "ロット詳細ID3"
  Public Property strLotID4 As String = "ロット詳細ID4"
  Public Property strLotID5 As String = "ロット詳細ID5"
  Public Property strLotID6 As String = "ロット詳細ID6"
  Public Property strLotID7 As String = "ロット詳細ID7"
  Public Property strLotID8 As String = "ロット詳細ID8"
  Public Property strLotID9 As String = "ロット詳細ID9"
  Public Property strLotID10 As String = "ロット詳細ID10"
  Public Property strLotName1 As String = "ロット詳細名1"
  Public Property strLotName2 As String = "ロット詳細名2"
  Public Property strLotName3 As String = "ロット詳細名3"
  Public Property strLotName4 As String = "ロット詳細名4"
  Public Property strLotName5 As String = "ロット詳細名5"
  Public Property strLotName6 As String = "ロット詳細名6"
  Public Property strLotName7 As String = "ロット詳細名7"
  Public Property strLotName8 As String = "ロット詳細名8"
  Public Property strLotName9 As String = "ロット詳細名9"
  Public Property strLotName10 As String = "ロット詳細名10"
  Public Property strCurrentNumber As String = "現在庫本数"
  Public Property strCurrentQuantity As String = "現在庫数量"
  Public Property strDetailsID As String = "詳細ID"
  Public Property strInvID As String = "在庫ID" '返品IDもとい修正前ID
  Public Property strRetInvID As String = "返品在庫ID" '新規登録(上書き)する在庫ID
  Public Property strRetAllocateID As String = "返品在庫引当ID"
  Public Property strConvertFlag As String = "入荷変換区分"
  Public Property strRetAllocateNum As String = "修正前本数"
  Public Property strRetAllocateQty As String = "修正前数量"
  Public Property strPrevRetID As String = "修正前在庫ID"

  Public Sub New(ByVal myAMSClass As AMS1)
    strColor = myAMSClass.ColorName
    strSize = myAMSClass.SizeName
    strSpec = myAMSClass.SpecName
    strQttPerCase = myAMSClass.QttPerCaseName
    strNoOfCase = myAMSClass.NoOfCaseName
    projectName = myAMSClass.ProjectName
  End Sub

End Class
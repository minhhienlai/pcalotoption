﻿Imports PCA.ApplicationIntegration
Imports System.Xml
Imports Sunloft.Message
Imports Sunloft.PCAForms

Public Class frmLotDetails

#Region "Properties"

  Public Property lngDetailRow As Integer = 0

  ''' <summary>
  ''' ProductName
  ''' </summary>
  Public Property strItemName As String = String.Empty

  ''' <summary>
  ''' LotNumber
  ''' </summary>
  Public Property strLotNo As String = String.Empty

  ''' <summary>
  ''' Supplier LotNumber
  ''' </summary>
  Public Property strSupLotNo As String = String.Empty

  ''' <summary>
  ''' ManagementNumber
  ''' </summary>
  Public Property strManagementNum As String = String.Empty

  ''' <summary>
  ''' 賞味期限
  ''' </summary>
  Public Property dteShoumi As Integer = 0

  ''' <summary>
  ''' 消費期限
  ''' </summary>
  Public Property dteShouhi As Integer = 0

  ''' <summary>
  ''' 出荷期限
  ''' </summary>
  Public Property dteShukka As Integer = 0

  ''' <summary>
  ''' 使用期限
  ''' </summary>
  Public Property dteShiyou As Integer = 0

  ''' <summary>
  ''' ロット詳細ラベル1
  ''' </summary>
  Public Property strLotDetailLabel1 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ラベル2
  ''' </summary>
  Public Property strLotDetailLabel2 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ラベル3
  ''' </summary>
  Public Property strLotDetailLabel3 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ラベル4
  ''' </summary>
  Public Property strLotDetailLabel4 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ラベル5
  ''' </summary>
  Public Property strLotDetailLabel5 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ラベル6
  ''' </summary>
  Public Property strLotDetailLabel6 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ラベル7
  ''' </summary>
  Public Property strLotDetailLabel7 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ラベル8
  ''' </summary>
  Public Property strLotDetailLabel8 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ラベル9
  ''' </summary>
  Public Property strLotDetailLabel9 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ラベル10
  ''' </summary>
  Public Property strLotDetailLabel10 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID1
  ''' </summary>
  Public Property strLotDetailCode1 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID2
  ''' </summary>
  Public Property strLotDetailCode2 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID3
  ''' </summary>
  Public Property strLotDetailCode3 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID4
  ''' </summary>
  Public Property strLotDetailCode4 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID5
  ''' </summary>
  Public Property strLotDetailCode5 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID6
  ''' </summary>
  Public Property strLotDetailCode6 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID7
  ''' </summary>
  Public Property strLotDetailCode7 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID8
  ''' </summary>
  Public Property strLotDetailCode8 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID9
  ''' </summary>
  Public Property strLotDetailCode9 As String = String.Empty

  ''' <summary>
  ''' ロット詳細ID10
  ''' </summary>
  Public Property strLotDetailCode10 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名1
  ''' </summary>
  Public Property strLotDetailName1 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名2
  ''' </summary>
  Public Property strLotDetailName2 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名3
  ''' </summary>
  Public Property strLotDetailName3 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名4
  ''' </summary>
  Public Property strLotDetailName4 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名5
  ''' </summary>
  Public Property strLotDetailName5 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名6
  ''' </summary>
  Public Property strLotDetailName6 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名7
  ''' </summary>
  Public Property strLotDetailName7 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名8
  ''' </summary>
  Public Property strLotDetailName8 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名9
  ''' </summary>
  Public Property strLotDetailName9 As String = String.Empty

  ''' <summary>
  ''' ロット詳細名10
  ''' </summary>
  Public Property strLotDetailName10 As String = String.Empty

#End Region

  Private connector As IIntegratedApplication

  Private m_conf As New Sunloft.PcaConfig
  Private arPcaCodeSetC(10) As PCA.Controls.PcaCodeSet
  Private arPcaCodeSetN(10) As PCA.Controls.PcaCodeSet
  Private arPcaLabeledDate(4) As PCA.Controls.PcaLabeledDate
  Private arPcaLabeledDateLeft(4) As Integer
  Private strLMDCode As String = String.Empty
  Private strLMDName As String = String.Empty
  Private m_isClosing As Boolean = False
  Private m_isSearching As Boolean = False

#Region "Initialize Methods"

  Public Sub New(ByVal argConnector As IIntegratedApplication)

    ' この呼び出しはデザイナーで必要です。
    InitializeComponent()

    ' InitializeComponent() 呼び出しの後で初期化を追加します。
    connector = argConnector
  End Sub

#End Region

#Region "Button Methods"

  Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
    m_isClosing = True
    Me.Close()
    m_isClosing = False
  End Sub

  ''' <summary>"OK" button Push Events</summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks>Setting Return Values</remarks>
  Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click

    If ltxtLotNo.Text.Trim = String.Empty Then MsgBox("ロットNoを入力してください。") : ltxtLotNo.Focus() : Exit Sub

    _strLotNo = ltxtLotNo.Text
    _strSupLotNo = ltxtSupLotNo.Text
    _strManagementNum = ltxtManageNo.Text
    _dteShoumi = ldtShoumi.IntDate
    _dteShouhi = ldtShouhi.IntDate
    _dteShukka = ldtShukka.IntDate
    _dteShiyou = ldtShiyou.IntDate

    _strLotDetailCode1 = setDetails1.CodeText
    _strLotDetailCode2 = setDetails2.CodeText
    _strLotDetailCode3 = setDetails3.CodeText
    _strLotDetailCode4 = setDetails4.CodeText
    _strLotDetailCode5 = setDetails5.CodeText
    _strLotDetailCode6 = setDetails6.CodeText
    _strLotDetailCode7 = setDetails7.CodeText
    _strLotDetailCode8 = setDetails8.CodeText
    _strLotDetailCode9 = setDetails9.CodeText
    _strLotDetailCode10 = setDetails10.CodeText

    _strLotDetailName1 = setDetailsName1.CodeText
    _strLotDetailName2 = setDetailsName2.CodeText
    _strLotDetailName3 = setDetailsName3.CodeText
    _strLotDetailName4 = setDetailsName4.CodeText
    _strLotDetailName5 = setDetailsName5.CodeText
    _strLotDetailName6 = setDetailsName6.CodeText
    _strLotDetailName7 = setDetailsName7.CodeText
    _strLotDetailName8 = setDetailsName8.CodeText
    _strLotDetailName9 = setDetailsName9.CodeText
    _strLotDetailName10 = setDetailsName10.CodeText
    Me.Close()
  End Sub

#End Region

  Private Sub setDetails_ClickReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setDetails10.ClickReferButton2, _
                                                                                                                        setDetails9.ClickReferButton2, _
                                                                                                                        setDetails8.ClickReferButton2, _
                                                                                                                        setDetails7.ClickReferButton2, _
                                                                                                                        setDetails6.ClickReferButton2, _
                                                                                                                        setDetails5.ClickReferButton2, _
                                                                                                                        setDetails4.ClickReferButton2, _
                                                                                                                        setDetails3.ClickReferButton2, _
                                                                                                                        setDetails2.ClickReferButton2, _
                                                                                                                        setDetails1.ClickReferButton2

    '参照画面を表示

    Dim newCode As String = String.Empty
    Dim objPcaCodeSet As PCA.Controls.PcaCodeSet = DirectCast(sender, PCA.Controls.PcaCodeSet)
    Dim location As Point = PCA.TSC.Kon.Tools.ControlTool.GetDialogLocation(objPcaCodeSet.ReferButton)

    Dim MasterDialog As SLMasterSearchDialog = New SLMasterSearchDialog(connector)
    MasterDialog.ShowReferLotDetailDialog(objPcaCodeSet.CodeText, newCode, location, connector, CInt(objPcaCodeSet.Tag))

    If dialogResult <> Windows.Forms.DialogResult.Cancel Then
      If String.IsNullOrEmpty(newCode) Then
        e.Cancel = True
      Else

        If Not ValidatePcaCodeset(connector, objPcaCodeSet, CInt(objPcaCodeSet.Tag), CInt(newCode)) Then
          e.Cancel = True
          objPcaCodeSet.CodeText = newCode
          Return
        Else
          objPcaCodeSet.CodeText = strLMDCode
          SetDetailName(objPcaCodeSet, strLMDName)
        End If
        m_isSearching = False
      End If
    Else
      e.Cancel = True
    End If
  End Sub

  Private Sub frmLotDetails_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    If Trim(_strItemName) <> String.Empty Then ltxtProduct.Text = _strItemName
    If Trim(_strLotNo) <> String.Empty Then ltxtLotNo.Text = _strLotNo
    If Trim(_strSupLotNo) <> String.Empty Then ltxtSupLotNo.Text = _strSupLotNo
    If Trim(_strManagementNum) <> String.Empty Then ltxtManageNo.Text = _strManagementNum

    ldtShoumi.IntDate = _dteShoumi
    ldtShouhi.IntDate = _dteShouhi
    ldtShukka.IntDate = _dteShukka
    ldtShiyou.IntDate = _dteShiyou

    If Trim(_strLotDetailLabel1) <> String.Empty Then setDetails1.Text = _strLotDetailLabel1
    If Trim(_strLotDetailLabel2) <> String.Empty Then setDetails2.Text = _strLotDetailLabel2
    If Trim(_strLotDetailLabel3) <> String.Empty Then setDetails3.Text = _strLotDetailLabel3
    If Trim(_strLotDetailLabel4) <> String.Empty Then setDetails4.Text = _strLotDetailLabel4
    If Trim(_strLotDetailLabel5) <> String.Empty Then setDetails5.Text = _strLotDetailLabel5
    If Trim(_strLotDetailLabel6) <> String.Empty Then setDetails6.Text = _strLotDetailLabel6
    If Trim(_strLotDetailLabel7) <> String.Empty Then setDetails7.Text = _strLotDetailLabel7
    If Trim(_strLotDetailLabel8) <> String.Empty Then setDetails8.Text = _strLotDetailLabel8
    If Trim(_strLotDetailLabel9) <> String.Empty Then setDetails9.Text = _strLotDetailLabel9
    If Trim(_strLotDetailLabel10) <> String.Empty Then setDetails10.Text = _strLotDetailLabel10
    If Trim(_strLotDetailCode1) <> String.Empty Then setDetails1.CodeText = _strLotDetailCode1
    If Trim(_strLotDetailCode2) <> String.Empty Then setDetails2.CodeText = _strLotDetailCode2
    If Trim(_strLotDetailCode3) <> String.Empty Then setDetails3.CodeText = _strLotDetailCode3
    If Trim(_strLotDetailCode4) <> String.Empty Then setDetails4.CodeText = _strLotDetailCode4
    If Trim(_strLotDetailCode5) <> String.Empty Then setDetails5.CodeText = _strLotDetailCode5
    If Trim(_strLotDetailCode6) <> String.Empty Then setDetails6.CodeText = _strLotDetailCode6
    If Trim(_strLotDetailCode7) <> String.Empty Then setDetails7.CodeText = _strLotDetailCode7
    If Trim(_strLotDetailCode8) <> String.Empty Then setDetails8.CodeText = _strLotDetailCode8
    If Trim(_strLotDetailCode9) <> String.Empty Then setDetails9.CodeText = _strLotDetailCode9
    If Trim(_strLotDetailCode10) <> String.Empty Then setDetails10.CodeText = _strLotDetailCode10
    If Trim(_strLotDetailName1) <> String.Empty Then setDetailsName1.CodeText = _strLotDetailName1
    If Trim(_strLotDetailName2) <> String.Empty Then setDetailsName2.CodeText = _strLotDetailName2
    If Trim(_strLotDetailName3) <> String.Empty Then setDetailsName3.CodeText = _strLotDetailName3
    If Trim(_strLotDetailName4) <> String.Empty Then setDetailsName4.CodeText = _strLotDetailName4
    If Trim(_strLotDetailName5) <> String.Empty Then setDetailsName5.CodeText = _strLotDetailName5
    If Trim(_strLotDetailName6) <> String.Empty Then setDetailsName6.CodeText = _strLotDetailName6
    If Trim(_strLotDetailName7) <> String.Empty Then setDetailsName7.CodeText = _strLotDetailName7
    If Trim(_strLotDetailName8) <> String.Empty Then setDetailsName8.CodeText = _strLotDetailName8
    If Trim(_strLotDetailName9) <> String.Empty Then setDetailsName9.CodeText = _strLotDetailName9
    If Trim(_strLotDetailName10) <> String.Empty Then setDetailsName10.CodeText = _strLotDetailName10

    arPcaCodeSetC = _
      {setDetails1, setDetails2, setDetails3, setDetails4, setDetails5, _
       setDetails6, setDetails7, setDetails8, setDetails9, setDetails10}
    arPcaCodeSetN = _
      {setDetailsName1, setDetailsName2, setDetailsName3, setDetailsName4, setDetailsName5, _
       setDetailsName6, setDetailsName7, setDetailsName8, setDetailsName9, setDetailsName10}
    arPcaLabeledDate = {ldtShukka, ldtShiyou, ldtShoumi, ldtShouhi}
    arPcaLabeledDateLeft = {ldtShukka.Left, ldtShiyou.Left, ldtShoumi.Left, ldtShouhi.Left}

    ShowDetails(connector)
  End Sub

#Region "PcaCodeSet Events"

  Private Sub setDetails_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setDetails10.Validating, _
                                                                                                                 setDetails9.Validating, _
                                                                                                                 setDetails8.Validating, _
                                                                                                                 setDetails7.Validating, _
                                                                                                                 setDetails6.Validating, _
                                                                                                                 setDetails5.Validating, _
                                                                                                                 setDetails4.Validating, _
                                                                                                                 setDetails3.Validating, _
                                                                                                                 setDetails2.Validating, _
                                                                                                                 setDetails1.Validating
    Dim objPcaCodeset As PCA.Controls.PcaCodeSet = DirectCast(sender, PCA.Controls.PcaCodeSet)

    If objPcaCodeset.CodeText.ToString.Trim = String.Empty Then Exit Sub
    If m_isClosing Then Exit Sub

    objPcaCodeset.CodeText = objPcaCodeset.CodeText.PadLeft(6, CChar("0"))

    If ValidatePcaCodeset(connector, objPcaCodeset, CInt(objPcaCodeset.Tag), objPcaCodeset.CodeText, False) Then
      objPcaCodeset.CodeText = strLMDCode
      SetDetailName(objPcaCodeset, strLMDName)
    Else
      DisplayBox.ShowError(Sunloft.Common.SLConstants.NotifyMessage.DATA_NOT_FOUND)
      objPcaCodeset.NameText = String.Empty
      e.Cancel = True
    End If
    m_isSearching = False

  End Sub

#End Region

#Region "Private Method"

  ''' <summary>ロット詳細名に名称をセットします。</summary>
  ''' <param name="argPcaCodeset"></param>
  ''' <param name="strLotDetailName"></param>
  ''' <remarks></remarks>
  Private Sub SetDetailName(ByVal argPcaCodeset As PCA.Controls.PcaCodeSet, ByVal strLotDetailName As String)

    Dim PCALotDetailsName() As PCA.Controls.PcaCodeSet = _
      {setDetailsName1, setDetailsName2, setDetailsName3, setDetailsName4, setDetailsName5, setDetailsName6, setDetailsName7, setDetailsName8, setDetailsName9, setDetailsName10}

    Try
      PCALotDetailsName(CInt(argPcaCodeset.Tag) - 1).CodeText = strLotDetailName
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>
  ''' コード決定時のValidationイベント
  ''' </summary>
  ''' <param name="PcaCodeset">CodeSet</param>
  ''' <param name="intLMDNo">ロット詳細番号</param>
  ''' <remarks></remarks>
  Private Function ValidatePcaCodeset(ByRef app As IIntegratedApplication, ByRef PcaCodeset As PCA.Controls.PcaCodeSet, _
                                      ByVal intLMDNo As Integer, ByVal code As Object, Optional ByVal isCalledDialog As Boolean = True) As Boolean

    Dim reader As ICustomDataReader = Nothing
    Try

      Dim cmdselectCommand As ICustomCommand
      Dim isExists As Boolean = False
      If m_isSearching Then m_isSearching = False : Return True

      cmdselectCommand = app.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_LMD" & intLMDNo)
      If isCalledDialog Then
        cmdselectCommand.Parameters("@sl_lmd_id").SetValue(code)
        cmdselectCommand.Parameters("@sl_lmd_ldcd").SetValue(String.Empty)
      Else
        cmdselectCommand.Parameters("@sl_lmd_id").SetValue(-1)
        cmdselectCommand.Parameters("@sl_lmd_ldcd").SetValue(code)
      End If

      reader = cmdselectCommand.ExecuteReader

      While reader.Read()

        strLMDCode = reader.GetValue("sl_lmd_ldcd").ToString
        strLMDName = reader.GetValue("sl_lmd_mei").ToString
        isExists = True
      End While

      Return isExists
    Catch ex As Exception
      strLMDCode = String.Empty
      strLMDName = String.Empty
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try

  End Function
  ''' <summary>Get and Hidden Contents </summary>
  ''' <param name="app"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ShowDetails(ByRef app As IIntegratedApplication) As Boolean
    Dim isSuccess As Boolean = True
    Dim intMaxLotOp As Integer = 0
    Dim intLpc As Integer = 0
    Dim intLpcOtherLabel As Integer = 0
    Dim intLabelDeleted As Integer = 0
    Dim SL_LMB As New Sunloft.PCAIF.SL_LMB(app)

    Try
      Call SL_LMB.ReadOnlyRow()

      If SL_LMB.sl_lmb_sdlflg = 1 Then ldtShukka.Visible = True Else ldtShukka.Visible = False
      If SL_LMB.sl_lmb_ubdflg = 1 Then ldtShiyou.Visible = True Else ldtShiyou.Visible = False
      If SL_LMB.sl_lmb_bbdflg = 1 Then ldtShoumi.Visible = True Else ldtShoumi.Visible = False
      If SL_LMB.sl_lmb_expflg = 1 Then ldtShouhi.Visible = True Else ldtShouhi.Visible = False

      For intLpc = 0 To arPcaLabeledDate.Count - 1
        If Not arPcaLabeledDate(intLpc).Visible Then
          For intLpcOtherLabel = intLpc To arPcaLabeledDate.Count - 2
            arPcaLabeledDate(intLpcOtherLabel + 1).Left = arPcaLabeledDateLeft(intLpcOtherLabel)

            intLabelDeleted = intLabelDeleted + 1
          Next

        End If
      Next

      intMaxLotOp = SL_LMB.sl_lmb_maxlotop
      For intLpc = 0 To arPcaCodeSetC.Count - 1
        If intLpc < intMaxLotOp Then
          arPcaCodeSetC(intLpc).Visible = True
          arPcaCodeSetN(intLpc).Visible = True
        Else
          arPcaCodeSetC(intLpc).Visible = False
          arPcaCodeSetN(intLpc).Visible = False

          Me.Height = Me.Height - arPcaCodeSetC(intLpc).Height
          'Me.Size.Height =
          btnOK.Top = btnOK.Top - arPcaCodeSetC(intLpc).Height
          btnCancel.Top = btnCancel.Top - arPcaCodeSetC(intLpc).Height
        End If

      Next

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function

  'Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
  '  '
  '  Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
  '  Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子

  '  If (keyModifiers <> Keys.Shift AndAlso (keyCode = Keys.Tab OrElse keyCode = Keys.Enter)) _
  '  OrElse keyCode = Keys.Right _
  '  OrElse keyCode = Keys.Down Then
  '    'Validate if user press Enter/Tab +  Anykey except Shift, →, ↓
  '    Me.IsValidate = True
  '  ElseIf (keyModifiers = Keys.Shift AndAlso (keyCode = Keys.Tab OrElse keyCode = Keys.Enter)) _
  '  OrElse keyCode = Keys.Left _
  '  OrElse keyCode = Keys.Up Then
  '    'Not validate if user press Shift + Tab, Shift + Enter, ←, ↑
  '    Me.IsValidate = False
  '  End If

  '  Return MyBase.ProcessDialogKey(keyData)

  'End Function

#End Region

  Private Sub setDetails1_Leave(sender As System.Object, e As System.EventArgs) Handles setDetails1.Leave
    Try
      If ActiveControl.Name = btnCancel.Name Then m_isClosing = True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
End Class
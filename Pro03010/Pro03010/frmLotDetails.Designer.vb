﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLotDetails
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ldtShoumi = New PCA.Controls.PcaLabeledDate()
    Me.ldtShouhi = New PCA.Controls.PcaLabeledDate()
    Me.ldtShukka = New PCA.Controls.PcaLabeledDate()
    Me.ldtShiyou = New PCA.Controls.PcaLabeledDate()
    Me.setDetails1 = New PCA.Controls.PcaCodeSet()
    Me.setDetails6 = New PCA.Controls.PcaCodeSet()
    Me.setDetails2 = New PCA.Controls.PcaCodeSet()
    Me.setDetails7 = New PCA.Controls.PcaCodeSet()
    Me.setDetails3 = New PCA.Controls.PcaCodeSet()
    Me.setDetails8 = New PCA.Controls.PcaCodeSet()
    Me.setDetails4 = New PCA.Controls.PcaCodeSet()
    Me.setDetails9 = New PCA.Controls.PcaCodeSet()
    Me.setDetails5 = New PCA.Controls.PcaCodeSet()
    Me.setDetails10 = New PCA.Controls.PcaCodeSet()
    Me.btnOK = New PCA.Controls.PcaButton()
    Me.btnCancel = New PCA.Controls.PcaButton()
    Me.PcaGroupBox1 = New PCA.Controls.PcaGroupBox()
    Me.setDetailsName10 = New PCA.Controls.PcaCodeSet()
    Me.setDetailsName9 = New PCA.Controls.PcaCodeSet()
    Me.setDetailsName8 = New PCA.Controls.PcaCodeSet()
    Me.setDetailsName7 = New PCA.Controls.PcaCodeSet()
    Me.setDetailsName6 = New PCA.Controls.PcaCodeSet()
    Me.setDetailsName5 = New PCA.Controls.PcaCodeSet()
    Me.setDetailsName4 = New PCA.Controls.PcaCodeSet()
    Me.setDetailsName3 = New PCA.Controls.PcaCodeSet()
    Me.setDetailsName2 = New PCA.Controls.PcaCodeSet()
    Me.setDetailsName1 = New PCA.Controls.PcaCodeSet()
    Me.ltxtProduct = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtLotNo = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtSupLotNo = New PCA.Controls.PcaLabeledTextBox()
    Me.ltxtManageNo = New PCA.Controls.PcaLabeledTextBox()
    Me.PcaLabeledTextBox1 = New PCA.Controls.PcaLabeledTextBox()
    Me.PcaLabeledTextBox2 = New PCA.Controls.PcaLabeledTextBox()
    Me.PcaLabeledTextBox3 = New PCA.Controls.PcaLabeledTextBox()
    Me.PcaLabeledTextBox4 = New PCA.Controls.PcaLabeledTextBox()
    Me.PcaGroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'ldtShoumi
    '
    Me.ldtShoumi.AllowEmpty = True
    Me.ldtShoumi.ClientProcess = Nothing
    Me.ldtShoumi.EmptyIfDisabled = True
    Me.ldtShoumi.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtShoumi.Holidays = Nothing
    Me.ldtShoumi.LabelText = "賞味期限"
    Me.ldtShoumi.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtShoumi.LabelTopAlign = True
    Me.ldtShoumi.Location = New System.Drawing.Point(372, 125)
    Me.ldtShoumi.Name = "ldtShoumi"
    Me.ldtShoumi.Size = New System.Drawing.Size(164, 44)
    Me.ldtShoumi.TabIndex = 6
    '
    'ldtShouhi
    '
    Me.ldtShouhi.AllowEmpty = True
    Me.ldtShouhi.ClientProcess = Nothing
    Me.ldtShouhi.EmptyIfDisabled = True
    Me.ldtShouhi.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtShouhi.Holidays = Nothing
    Me.ldtShouhi.LabelText = "消費期限"
    Me.ldtShouhi.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtShouhi.LabelTopAlign = True
    Me.ldtShouhi.Location = New System.Drawing.Point(544, 125)
    Me.ldtShouhi.Name = "ldtShouhi"
    Me.ldtShouhi.Size = New System.Drawing.Size(164, 44)
    Me.ldtShouhi.TabIndex = 7
    '
    'ldtShukka
    '
    Me.ldtShukka.AllowEmpty = True
    Me.ldtShukka.ClientProcess = Nothing
    Me.ldtShukka.EmptyIfDisabled = True
    Me.ldtShukka.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtShukka.Holidays = Nothing
    Me.ldtShukka.LabelText = "出荷期限"
    Me.ldtShukka.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtShukka.LabelTopAlign = True
    Me.ldtShukka.Location = New System.Drawing.Point(22, 125)
    Me.ldtShukka.Name = "ldtShukka"
    Me.ldtShukka.Size = New System.Drawing.Size(164, 44)
    Me.ldtShukka.TabIndex = 4
    '
    'ldtShiyou
    '
    Me.ldtShiyou.AllowEmpty = True
    Me.ldtShiyou.ClientProcess = Nothing
    Me.ldtShiyou.EmptyIfDisabled = True
    Me.ldtShiyou.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtShiyou.Holidays = Nothing
    Me.ldtShiyou.LabelText = "使用期限"
    Me.ldtShiyou.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtShiyou.LabelTopAlign = True
    Me.ldtShiyou.Location = New System.Drawing.Point(197, 125)
    Me.ldtShiyou.Name = "ldtShiyou"
    Me.ldtShiyou.Size = New System.Drawing.Size(164, 44)
    Me.ldtShiyou.TabIndex = 5
    '
    'setDetails1
    '
    Me.setDetails1.AutoTopMargin = False
    Me.setDetails1.ClientProcess = Nothing
    Me.setDetails1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails1.GroupBoxVisible = False
    Me.setDetails1.LimitLength = 6
    Me.setDetails1.Location = New System.Drawing.Point(22, 185)
    Me.setDetails1.MaxCodeLength = 7
    Me.setDetails1.MaxLabelLength = 15
    Me.setDetails1.MaxNameLength = 0
    Me.setDetails1.Name = "setDetails1"
    Me.setDetails1.Size = New System.Drawing.Size(175, 22)
    Me.setDetails1.TabIndex = 8
    Me.setDetails1.Tag = "1"
    Me.setDetails1.Text = "ロット詳細1"
    Me.setDetails1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setDetails6
    '
    Me.setDetails6.AutoTopMargin = False
    Me.setDetails6.ClientProcess = Nothing
    Me.setDetails6.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails6.GroupBoxVisible = False
    Me.setDetails6.LimitLength = 6
    Me.setDetails6.Location = New System.Drawing.Point(22, 290)
    Me.setDetails6.MaxCodeLength = 7
    Me.setDetails6.MaxLabelLength = 15
    Me.setDetails6.MaxNameLength = 0
    Me.setDetails6.Name = "setDetails6"
    Me.setDetails6.Size = New System.Drawing.Size(175, 22)
    Me.setDetails6.TabIndex = 13
    Me.setDetails6.Tag = "6"
    Me.setDetails6.Text = "ロット詳細6"
    Me.setDetails6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setDetails2
    '
    Me.setDetails2.AutoTopMargin = False
    Me.setDetails2.ClientProcess = Nothing
    Me.setDetails2.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails2.GroupBoxVisible = False
    Me.setDetails2.LimitLength = 6
    Me.setDetails2.Location = New System.Drawing.Point(22, 206)
    Me.setDetails2.MaxCodeLength = 7
    Me.setDetails2.MaxLabelLength = 15
    Me.setDetails2.MaxNameLength = 0
    Me.setDetails2.Name = "setDetails2"
    Me.setDetails2.Size = New System.Drawing.Size(175, 22)
    Me.setDetails2.TabIndex = 9
    Me.setDetails2.Tag = "2"
    Me.setDetails2.Text = "ロット詳細2"
    Me.setDetails2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setDetails7
    '
    Me.setDetails7.AutoTopMargin = False
    Me.setDetails7.ClientProcess = Nothing
    Me.setDetails7.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails7.GroupBoxVisible = False
    Me.setDetails7.LimitLength = 6
    Me.setDetails7.Location = New System.Drawing.Point(22, 311)
    Me.setDetails7.MaxCodeLength = 7
    Me.setDetails7.MaxLabelLength = 15
    Me.setDetails7.MaxNameLength = 0
    Me.setDetails7.Name = "setDetails7"
    Me.setDetails7.Size = New System.Drawing.Size(175, 22)
    Me.setDetails7.TabIndex = 14
    Me.setDetails7.Tag = "7"
    Me.setDetails7.Text = "ロット詳細7"
    Me.setDetails7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setDetails3
    '
    Me.setDetails3.AutoTopMargin = False
    Me.setDetails3.ClientProcess = Nothing
    Me.setDetails3.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails3.GroupBoxVisible = False
    Me.setDetails3.LimitLength = 6
    Me.setDetails3.Location = New System.Drawing.Point(22, 227)
    Me.setDetails3.MaxCodeLength = 7
    Me.setDetails3.MaxLabelLength = 15
    Me.setDetails3.MaxNameLength = 0
    Me.setDetails3.Name = "setDetails3"
    Me.setDetails3.Size = New System.Drawing.Size(175, 22)
    Me.setDetails3.TabIndex = 10
    Me.setDetails3.Tag = "3"
    Me.setDetails3.Text = "ロット詳細3"
    Me.setDetails3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setDetails8
    '
    Me.setDetails8.AutoTopMargin = False
    Me.setDetails8.ClientProcess = Nothing
    Me.setDetails8.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails8.GroupBoxVisible = False
    Me.setDetails8.LimitLength = 6
    Me.setDetails8.Location = New System.Drawing.Point(22, 332)
    Me.setDetails8.MaxCodeLength = 7
    Me.setDetails8.MaxLabelLength = 15
    Me.setDetails8.MaxNameLength = 0
    Me.setDetails8.Name = "setDetails8"
    Me.setDetails8.Size = New System.Drawing.Size(175, 22)
    Me.setDetails8.TabIndex = 15
    Me.setDetails8.Tag = "8"
    Me.setDetails8.Text = "ロット詳細8"
    Me.setDetails8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setDetails4
    '
    Me.setDetails4.AutoTopMargin = False
    Me.setDetails4.ClientProcess = Nothing
    Me.setDetails4.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails4.GroupBoxVisible = False
    Me.setDetails4.LimitLength = 6
    Me.setDetails4.Location = New System.Drawing.Point(22, 248)
    Me.setDetails4.MaxCodeLength = 7
    Me.setDetails4.MaxLabelLength = 15
    Me.setDetails4.MaxNameLength = 0
    Me.setDetails4.Name = "setDetails4"
    Me.setDetails4.Size = New System.Drawing.Size(175, 22)
    Me.setDetails4.TabIndex = 11
    Me.setDetails4.Tag = "4"
    Me.setDetails4.Text = "ロット詳細4"
    Me.setDetails4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setDetails9
    '
    Me.setDetails9.AutoTopMargin = False
    Me.setDetails9.ClientProcess = Nothing
    Me.setDetails9.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails9.GroupBoxVisible = False
    Me.setDetails9.LimitLength = 6
    Me.setDetails9.Location = New System.Drawing.Point(22, 353)
    Me.setDetails9.MaxCodeLength = 7
    Me.setDetails9.MaxLabelLength = 15
    Me.setDetails9.MaxNameLength = 0
    Me.setDetails9.Name = "setDetails9"
    Me.setDetails9.Size = New System.Drawing.Size(175, 22)
    Me.setDetails9.TabIndex = 16
    Me.setDetails9.Tag = "9"
    Me.setDetails9.Text = "ロット詳細9"
    Me.setDetails9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setDetails5
    '
    Me.setDetails5.AutoTopMargin = False
    Me.setDetails5.ClientProcess = Nothing
    Me.setDetails5.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails5.GroupBoxVisible = False
    Me.setDetails5.LimitLength = 6
    Me.setDetails5.Location = New System.Drawing.Point(22, 269)
    Me.setDetails5.MaxCodeLength = 7
    Me.setDetails5.MaxLabelLength = 15
    Me.setDetails5.MaxNameLength = 0
    Me.setDetails5.Name = "setDetails5"
    Me.setDetails5.Size = New System.Drawing.Size(175, 22)
    Me.setDetails5.TabIndex = 12
    Me.setDetails5.Tag = "5"
    Me.setDetails5.Text = "ロット詳細5"
    Me.setDetails5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setDetails10
    '
    Me.setDetails10.AutoTopMargin = False
    Me.setDetails10.ClientProcess = Nothing
    Me.setDetails10.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.setDetails10.GroupBoxVisible = False
    Me.setDetails10.LimitLength = 6
    Me.setDetails10.Location = New System.Drawing.Point(22, 374)
    Me.setDetails10.MaxCodeLength = 7
    Me.setDetails10.MaxLabelLength = 15
    Me.setDetails10.MaxNameLength = 0
    Me.setDetails10.Name = "setDetails10"
    Me.setDetails10.Size = New System.Drawing.Size(175, 22)
    Me.setDetails10.TabIndex = 17
    Me.setDetails10.Tag = "10"
    Me.setDetails10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btnOK
    '
    Me.btnOK.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.btnOK.Location = New System.Drawing.Point(277, 415)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(84, 24)
    Me.btnOK.TabIndex = 18
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'btnCancel
    '
    Me.btnCancel.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.btnCancel.Location = New System.Drawing.Point(372, 415)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(84, 24)
    Me.btnCancel.TabIndex = 19
    Me.btnCancel.Text = "キャンセル"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'PcaGroupBox1
    '
    Me.PcaGroupBox1.AutoTopMargin = False
    Me.PcaGroupBox1.BottomControl = Nothing
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName10)
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName9)
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName8)
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName7)
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName6)
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName5)
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName4)
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName3)
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName2)
    Me.PcaGroupBox1.Controls.Add(Me.setDetailsName1)
    Me.PcaGroupBox1.Controls.Add(Me.ltxtProduct)
    Me.PcaGroupBox1.Controls.Add(Me.ltxtLotNo)
    Me.PcaGroupBox1.Controls.Add(Me.ltxtSupLotNo)
    Me.PcaGroupBox1.Controls.Add(Me.ltxtManageNo)
    Me.PcaGroupBox1.Controls.Add(Me.btnCancel)
    Me.PcaGroupBox1.Controls.Add(Me.btnOK)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails10)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails5)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails9)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails4)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails8)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails3)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails7)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails2)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails6)
    Me.PcaGroupBox1.Controls.Add(Me.setDetails1)
    Me.PcaGroupBox1.Controls.Add(Me.ldtShiyou)
    Me.PcaGroupBox1.Controls.Add(Me.ldtShukka)
    Me.PcaGroupBox1.Controls.Add(Me.ldtShouhi)
    Me.PcaGroupBox1.Controls.Add(Me.ldtShoumi)
    Me.PcaGroupBox1.Location = New System.Drawing.Point(9, 10)
    Me.PcaGroupBox1.Name = "PcaGroupBox1"
    Me.PcaGroupBox1.Padding = New System.Windows.Forms.Padding(14, 7, 14, 13)
    Me.PcaGroupBox1.Size = New System.Drawing.Size(722, 454)
    Me.PcaGroupBox1.TabIndex = 2
    Me.PcaGroupBox1.TabStop = False
    '
    'setDetailsName10
    '
    Me.setDetailsName10.AutoTextSize = False
    Me.setDetailsName10.AutoTopMargin = False
    Me.setDetailsName10.ClientProcess = Nothing
    Me.setDetailsName10.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName10.GroupBoxVisible = False
    Me.setDetailsName10.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName10.LimitLength = 40
    Me.setDetailsName10.Location = New System.Drawing.Point(198, 374)
    Me.setDetailsName10.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName10.MaxCodeLength = 73
    Me.setDetailsName10.MaxLabelLength = 0
    Me.setDetailsName10.MaxNameLength = 0
    Me.setDetailsName10.Name = "setDetailsName10"
    Me.setDetailsName10.ReferButtonVisible = False
    Me.setDetailsName10.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName10.TabIndex = 29
    '
    'setDetailsName9
    '
    Me.setDetailsName9.AutoTextSize = False
    Me.setDetailsName9.AutoTopMargin = False
    Me.setDetailsName9.ClientProcess = Nothing
    Me.setDetailsName9.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName9.GroupBoxVisible = False
    Me.setDetailsName9.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName9.LimitLength = 40
    Me.setDetailsName9.Location = New System.Drawing.Point(198, 353)
    Me.setDetailsName9.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName9.MaxCodeLength = 73
    Me.setDetailsName9.MaxLabelLength = 0
    Me.setDetailsName9.MaxNameLength = 0
    Me.setDetailsName9.Name = "setDetailsName9"
    Me.setDetailsName9.ReferButtonVisible = False
    Me.setDetailsName9.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName9.TabIndex = 28
    '
    'setDetailsName8
    '
    Me.setDetailsName8.AutoTextSize = False
    Me.setDetailsName8.AutoTopMargin = False
    Me.setDetailsName8.ClientProcess = Nothing
    Me.setDetailsName8.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName8.GroupBoxVisible = False
    Me.setDetailsName8.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName8.LimitLength = 40
    Me.setDetailsName8.Location = New System.Drawing.Point(198, 332)
    Me.setDetailsName8.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName8.MaxCodeLength = 73
    Me.setDetailsName8.MaxLabelLength = 0
    Me.setDetailsName8.MaxNameLength = 0
    Me.setDetailsName8.Name = "setDetailsName8"
    Me.setDetailsName8.ReferButtonVisible = False
    Me.setDetailsName8.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName8.TabIndex = 27
    '
    'setDetailsName7
    '
    Me.setDetailsName7.AutoTextSize = False
    Me.setDetailsName7.AutoTopMargin = False
    Me.setDetailsName7.ClientProcess = Nothing
    Me.setDetailsName7.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName7.GroupBoxVisible = False
    Me.setDetailsName7.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName7.LimitLength = 40
    Me.setDetailsName7.Location = New System.Drawing.Point(198, 311)
    Me.setDetailsName7.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName7.MaxCodeLength = 73
    Me.setDetailsName7.MaxLabelLength = 0
    Me.setDetailsName7.MaxNameLength = 0
    Me.setDetailsName7.Name = "setDetailsName7"
    Me.setDetailsName7.ReferButtonVisible = False
    Me.setDetailsName7.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName7.TabIndex = 26
    '
    'setDetailsName6
    '
    Me.setDetailsName6.AutoTextSize = False
    Me.setDetailsName6.AutoTopMargin = False
    Me.setDetailsName6.ClientProcess = Nothing
    Me.setDetailsName6.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName6.GroupBoxVisible = False
    Me.setDetailsName6.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName6.LimitLength = 40
    Me.setDetailsName6.Location = New System.Drawing.Point(198, 290)
    Me.setDetailsName6.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName6.MaxCodeLength = 73
    Me.setDetailsName6.MaxLabelLength = 0
    Me.setDetailsName6.MaxNameLength = 0
    Me.setDetailsName6.Name = "setDetailsName6"
    Me.setDetailsName6.ReferButtonVisible = False
    Me.setDetailsName6.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName6.TabIndex = 25
    '
    'setDetailsName5
    '
    Me.setDetailsName5.AutoTextSize = False
    Me.setDetailsName5.AutoTopMargin = False
    Me.setDetailsName5.ClientProcess = Nothing
    Me.setDetailsName5.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName5.GroupBoxVisible = False
    Me.setDetailsName5.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName5.LimitLength = 40
    Me.setDetailsName5.Location = New System.Drawing.Point(198, 269)
    Me.setDetailsName5.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName5.MaxCodeLength = 73
    Me.setDetailsName5.MaxLabelLength = 0
    Me.setDetailsName5.MaxNameLength = 0
    Me.setDetailsName5.Name = "setDetailsName5"
    Me.setDetailsName5.ReferButtonVisible = False
    Me.setDetailsName5.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName5.TabIndex = 24
    '
    'setDetailsName4
    '
    Me.setDetailsName4.AutoTextSize = False
    Me.setDetailsName4.AutoTopMargin = False
    Me.setDetailsName4.ClientProcess = Nothing
    Me.setDetailsName4.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName4.GroupBoxVisible = False
    Me.setDetailsName4.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName4.LimitLength = 40
    Me.setDetailsName4.Location = New System.Drawing.Point(198, 248)
    Me.setDetailsName4.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName4.MaxCodeLength = 73
    Me.setDetailsName4.MaxLabelLength = 0
    Me.setDetailsName4.MaxNameLength = 0
    Me.setDetailsName4.Name = "setDetailsName4"
    Me.setDetailsName4.ReferButtonVisible = False
    Me.setDetailsName4.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName4.TabIndex = 23
    '
    'setDetailsName3
    '
    Me.setDetailsName3.AutoTextSize = False
    Me.setDetailsName3.AutoTopMargin = False
    Me.setDetailsName3.ClientProcess = Nothing
    Me.setDetailsName3.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName3.GroupBoxVisible = False
    Me.setDetailsName3.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName3.LimitLength = 40
    Me.setDetailsName3.Location = New System.Drawing.Point(198, 227)
    Me.setDetailsName3.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName3.MaxCodeLength = 73
    Me.setDetailsName3.MaxLabelLength = 0
    Me.setDetailsName3.MaxNameLength = 0
    Me.setDetailsName3.Name = "setDetailsName3"
    Me.setDetailsName3.ReferButtonVisible = False
    Me.setDetailsName3.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName3.TabIndex = 22
    '
    'setDetailsName2
    '
    Me.setDetailsName2.AutoTextSize = False
    Me.setDetailsName2.AutoTopMargin = False
    Me.setDetailsName2.ClientProcess = Nothing
    Me.setDetailsName2.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName2.GroupBoxVisible = False
    Me.setDetailsName2.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName2.LimitLength = 40
    Me.setDetailsName2.Location = New System.Drawing.Point(198, 206)
    Me.setDetailsName2.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName2.MaxCodeLength = 73
    Me.setDetailsName2.MaxLabelLength = 0
    Me.setDetailsName2.MaxNameLength = 0
    Me.setDetailsName2.Name = "setDetailsName2"
    Me.setDetailsName2.ReferButtonVisible = False
    Me.setDetailsName2.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName2.TabIndex = 21
    '
    'setDetailsName1
    '
    Me.setDetailsName1.AutoTextSize = False
    Me.setDetailsName1.AutoTopMargin = False
    Me.setDetailsName1.ClientProcess = Nothing
    Me.setDetailsName1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDetailsName1.GroupBoxVisible = False
    Me.setDetailsName1.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setDetailsName1.LimitLength = 40
    Me.setDetailsName1.Location = New System.Drawing.Point(198, 185)
    Me.setDetailsName1.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setDetailsName1.MaxCodeLength = 73
    Me.setDetailsName1.MaxLabelLength = 0
    Me.setDetailsName1.MaxNameLength = 0
    Me.setDetailsName1.Name = "setDetailsName1"
    Me.setDetailsName1.ReferButtonVisible = False
    Me.setDetailsName1.Size = New System.Drawing.Size(516, 22)
    Me.setDetailsName1.TabIndex = 20
    '
    'ltxtProduct
    '
    Me.ltxtProduct.AutoTextSize = False
    Me.ltxtProduct.ClientProcess = Nothing
    Me.ltxtProduct.Enabled = False
    Me.ltxtProduct.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtProduct.LabelText = "商品名"
    Me.ltxtProduct.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtProduct.Location = New System.Drawing.Point(22, 20)
    Me.ltxtProduct.MaxLabelLength = 15
    Me.ltxtProduct.MaxTextLength = 18
    Me.ltxtProduct.Name = "ltxtProduct"
    Me.ltxtProduct.ReadOnly = True
    Me.ltxtProduct.Size = New System.Drawing.Size(496, 22)
    Me.ltxtProduct.TabIndex = 0
    Me.ltxtProduct.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtLotNo
    '
    Me.ltxtLotNo.AutoTextSize = False
    Me.ltxtLotNo.ClientProcess = Nothing
    Me.ltxtLotNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtLotNo.LabelText = "自社ロットNo"
    Me.ltxtLotNo.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtLotNo.Location = New System.Drawing.Point(22, 41)
    Me.ltxtLotNo.MaxLabelLength = 15
    Me.ltxtLotNo.Name = "ltxtLotNo"
    Me.ltxtLotNo.Size = New System.Drawing.Size(280, 22)
    Me.ltxtLotNo.TabIndex = 1
    '
    'ltxtSupLotNo
    '
    Me.ltxtSupLotNo.AutoTextSize = False
    Me.ltxtSupLotNo.ClientProcess = Nothing
    Me.ltxtSupLotNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtSupLotNo.LabelText = "相手先ロットNo"
    Me.ltxtSupLotNo.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtSupLotNo.Location = New System.Drawing.Point(22, 62)
    Me.ltxtSupLotNo.MaxLabelLength = 15
    Me.ltxtSupLotNo.Name = "ltxtSupLotNo"
    Me.ltxtSupLotNo.Size = New System.Drawing.Size(280, 22)
    Me.ltxtSupLotNo.TabIndex = 2
    '
    'ltxtManageNo
    '
    Me.ltxtManageNo.AutoTextSize = False
    Me.ltxtManageNo.ClientProcess = Nothing
    Me.ltxtManageNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtManageNo.LabelText = "管理番号"
    Me.ltxtManageNo.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtManageNo.Location = New System.Drawing.Point(22, 83)
    Me.ltxtManageNo.MaxLabelLength = 15
    Me.ltxtManageNo.MaxTextLength = 26
    Me.ltxtManageNo.Name = "ltxtManageNo"
    Me.ltxtManageNo.Size = New System.Drawing.Size(280, 22)
    Me.ltxtManageNo.TabIndex = 3
    '
    'PcaLabeledTextBox1
    '
    Me.PcaLabeledTextBox1.ClientProcess = Nothing
    Me.PcaLabeledTextBox1.LabelText = "TextBox"
    Me.PcaLabeledTextBox1.Location = New System.Drawing.Point(0, 0)
    Me.PcaLabeledTextBox1.Name = "PcaLabeledTextBox1"
    Me.PcaLabeledTextBox1.Size = New System.Drawing.Size(240, 150)
    Me.PcaLabeledTextBox1.TabIndex = 0
    '
    'PcaLabeledTextBox2
    '
    Me.PcaLabeledTextBox2.ClientProcess = Nothing
    Me.PcaLabeledTextBox2.LabelText = "TextBox"
    Me.PcaLabeledTextBox2.Location = New System.Drawing.Point(0, 0)
    Me.PcaLabeledTextBox2.Name = "PcaLabeledTextBox2"
    Me.PcaLabeledTextBox2.Size = New System.Drawing.Size(251, 150)
    Me.PcaLabeledTextBox2.TabIndex = 0
    '
    'PcaLabeledTextBox3
    '
    Me.PcaLabeledTextBox3.ClientProcess = Nothing
    Me.PcaLabeledTextBox3.LabelText = "TextBox"
    Me.PcaLabeledTextBox3.Location = New System.Drawing.Point(0, 0)
    Me.PcaLabeledTextBox3.Name = "PcaLabeledTextBox3"
    Me.PcaLabeledTextBox3.Size = New System.Drawing.Size(251, 150)
    Me.PcaLabeledTextBox3.TabIndex = 0
    '
    'PcaLabeledTextBox4
    '
    Me.PcaLabeledTextBox4.ClientProcess = Nothing
    Me.PcaLabeledTextBox4.LabelText = "TextBox"
    Me.PcaLabeledTextBox4.Location = New System.Drawing.Point(0, 0)
    Me.PcaLabeledTextBox4.Name = "PcaLabeledTextBox4"
    Me.PcaLabeledTextBox4.Size = New System.Drawing.Size(251, 150)
    Me.PcaLabeledTextBox4.TabIndex = 0
    '
    'frmLotDetails
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CausesValidation = False
    Me.ClientSize = New System.Drawing.Size(744, 479)
    Me.Controls.Add(Me.PcaGroupBox1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MinimumSize = New System.Drawing.Size(760, 250)
    Me.Name = "frmLotDetails"
    Me.Text = "ロット詳細入力"
    Me.PcaGroupBox1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ldtShoumi As PCA.Controls.PcaLabeledDate
  Friend WithEvents ldtShouhi As PCA.Controls.PcaLabeledDate
  Friend WithEvents ldtShukka As PCA.Controls.PcaLabeledDate
  Friend WithEvents ldtShiyou As PCA.Controls.PcaLabeledDate
  Friend WithEvents setDetails1 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetails6 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetails2 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetails7 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetails3 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetails8 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetails4 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetails9 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetails5 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetails10 As PCA.Controls.PcaCodeSet
  Friend WithEvents btnOK As PCA.Controls.PcaButton
  Friend WithEvents btnCancel As PCA.Controls.PcaButton
  Friend WithEvents PcaGroupBox1 As PCA.Controls.PcaGroupBox
  Friend WithEvents ltxtSupLotNo As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtManageNo As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtLotNo As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents ltxtProduct As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents PcaLabeledTextBox1 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents PcaLabeledTextBox2 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents PcaLabeledTextBox3 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents PcaLabeledTextBox4 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents setDetailsName1 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetailsName10 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetailsName9 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetailsName8 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetailsName7 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetailsName6 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetailsName5 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetailsName4 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetailsName3 As PCA.Controls.PcaCodeSet
  Friend WithEvents setDetailsName2 As PCA.Controls.PcaCodeSet
End Class

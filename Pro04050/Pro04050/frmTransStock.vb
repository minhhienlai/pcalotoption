﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.PCAIF
Imports Sunloft.Common
Imports PCA.Controls
Imports System.Xml
Imports PCA.ApplicationIntegration

Public Class frmTransStock
  Property connector As IIntegratedApplication = Nothing
  
  'Table Column's width Constants
  Private Const CHECK_LENGTH As Short = 5
  Private Const SLIPNO_LENGTH As Short = 10
  Private Const TYPE_LENGTH As Integer = 10
  Private Const DATE_LENGTH As Short = 13
  Private Const CODE_LENGTH As Short = 6
  Private Const NAME_LENGTH As Short = 40
  Private Const LOTNO_LENGTH As Short = 20
  Private Const QUANTITY_LENGTH As Integer = 10

  Private Const STOCK_PG_NAME As String = "\Pro04010.exe"
  Private Const TRANS_PG_NAME As String = "\Pro04030.exe"

  'Save to common lib?

  Private m_IntHeaderID As Integer
  Private m_IsClosingFlag As Boolean = False
  Private m_IntRowIndex As Integer = -1
  Private m_IntRowCount As Integer = 0
  Private m_IntTransferCount As Integer = 0
  Private m_SL_LMBClass As SL_LMB
  Private m_AMS1Class As AMS1

  Private m_conf As Sunloft.PcaConfig
  Private MasterDialog As SLMasterSearchDialog
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private m_loginString As String
  Private m_isAllowKeyDown As Boolean

#Region "Initialize Methods"

  Public Sub New()
    Try
      InitializeComponent()

      'Connect to PCA
      m_appClass.ConnectToPCA()
      connector = m_appClass.connector

      m_loginString = m_appClass.EncryptedLoginString

      m_SL_LMBClass = New SL_LMB(connector)
      m_SL_LMBClass.ReadOnlyRow()
      m_AMS1Class = New AMS1(connector)
      m_AMS1Class.ReadAll()
      MasterDialog = New SLMasterSearchDialog(connector)

      m_conf = New Sunloft.PcaConfig

      PcaFunctionCommandExecute.Enabled = False

      InitTable()
      DisplayTrabsferableSlip()
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitTable()
    Try
      With tblTable
        .DataRowCount = 99
        .BodyRowCount = 99

        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader()
        InitTableBody()

        .UpdateTable()

        .BodyColumns.DefaultStyle.BackColor = Color.White
        .BodyColumns.DefaultAltStyle.BackColor = Color.White
        .DataRowCount = 0
      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    Try
      Dim configXMLDoc As New XmlDocument

      MyBase.OnLoad(e)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>Init Table Header when create Table</summary>
  ''' <remarks></remarks>
  ''' 
  Private Sub InitTableHeader()
    Try
      tblTable.HeadTextHeight = 1
      tblTable.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
      tblTable.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell
      Dim strHeaderName As String = String.Empty

      column = New PcaColumn(ColumnName.Check, CHECK_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Check)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.SlipNo, SLIPNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.SlipNo)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.Kbn, TYPE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Kbn)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.TargetDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.TargetDate)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      strHeaderName = Replace(ColumnName.ProductCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_ProductLength + 2 + NAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(strHeaderName)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      strHeaderName = Replace(ColumnName.WareHouseCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_WarehouseLength + 2 + NAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(strHeaderName)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.Quantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Quantity)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitName, CODE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.UnitName)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      column = New PcaColumn(ColumnName.LotNo, LOTNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.LotNo)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Invisible column (From Lot Details screen

      InitHeaderColumn(column, cell, 0, ColumnName.StockID, ColumnName.StockID)
      InitHeaderColumn(column, cell, 0, ColumnName.WarehouseID, ColumnName.WarehouseID)
      InitHeaderColumn(column, cell, 0, ColumnName.GRDate, ColumnName.GRDate)
      InitHeaderColumn(column, cell, 0, ColumnName.GRWHCode, ColumnName.GRWHCode)
      InitHeaderColumn(column, cell, 0, ColumnName.ProductName, ColumnName.ProductName)
      InitHeaderColumn(column, cell, 0, ColumnName.ProductKikaku, ColumnName.ProductKikaku)
      InitHeaderColumn(column, cell, 0, ColumnName.ProductColor, ColumnName.ProductColor)
      InitHeaderColumn(column, cell, 0, ColumnName.ProductSize, ColumnName.ProductSize)
      InitHeaderColumn(column, cell, 0, ColumnName.UnitDecimalDigits, ColumnName.UnitDecimalDigits)
      InitHeaderColumn(column, cell, 0, ColumnName.QuantityDecimalDigits, ColumnName.QuantityDecimalDigits)
      InitHeaderColumn(column, cell, 0, ColumnName.Dummy, ColumnName.Dummy)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>Init a Header column</summary>
  ''' <param name="column">PcaColumnControl</param>
  ''' <param name="cell">PcaCellControl</param>
  ''' <param name="columnWidth">columnWidth</param>
  ''' <param name="cellName">cellName</param>
  ''' <param name="columnName">columnName</param>
  ''' <remarks></remarks>
  ''' 
  Private Sub InitHeaderColumn(ByVal column As PcaColumn, ByVal cell As PcaCell, ByVal columnWidth As Integer, ByVal cellName As String, ByVal columnName As String)
    Try
      column = New PcaColumn(columnName, columnWidth)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(cellName, columnWidth, 1, 0.0F, 0.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>InitTableBody when create Table</summary>
  ''' <remarks></remarks>
  Private Sub InitTableBody()
    Try
      tblTable.BodyTextHeight = 1

      tblTable.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
      tblTable.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
      tblTable.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
      tblTable.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell
      Dim strHeaderName As String = String.Empty

      column = New PcaColumn(ColumnName.Check, CHECK_LENGTH)
      column.CanResize = True
      cell = New PcaCell(ColumnName.Check, CellType.Boolean)
      cell.EditMode = True
      cell.CellStyle.Alignment = StringAlignment.Center
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.SlipNo, SLIPNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.SlipNo, SLIPNO_LENGTH, 1, 0.0F, 0.0F, ColumnName.SlipNo)
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.Kbn, TYPE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.Kbn, TYPE_LENGTH, 1, 0.0F, 0.0F, ColumnName.Kbn)
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.TargetDate, DATE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.TargetDate, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.TargetDate)
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      strHeaderName = Replace(ColumnName.ProductCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_ProductLength + 2 + NAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.ProductCode, m_AMS1Class.ams1_ProductLength + 2, 1, 0.0F, 0.0F, strHeaderName)
      cell.EditMode = False
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.AllProductName, NAME_LENGTH, 1, m_AMS1Class.ams1_ProductLength + 2, 0.0F, strHeaderName)
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      strHeaderName = Replace(ColumnName.WareHouseCode, "コード", String.Empty)
      column = New PcaColumn(strHeaderName, m_AMS1Class.ams1_WarehouseLength + 2 + NAME_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.WareHouseCode, m_AMS1Class.ams1_WarehouseLength + 2, 1, 0.0F, 0.0F, strHeaderName)
      cell.EditMode = False
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.WareHouseName, NAME_LENGTH, 1, m_AMS1Class.ams1_WarehouseLength + 2, 0.0F, strHeaderName)
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.Quantity, QUANTITY_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.Quantity, QUANTITY_LENGTH, 1, 0.0F, 0.0F, ColumnName.Quantity)
      cell.Type = CellType.Number
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.UnitName, CODE_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.UnitName, DATE_LENGTH, 1, 0.0F, 0.0F, ColumnName.UnitName)
      cell.Type = CellType.Text
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      column = New PcaColumn(ColumnName.LotNo, LOTNO_LENGTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.LotNo, LOTNO_LENGTH, 1, 0.0F, 0.0F, ColumnName.LotNo)
      cell.Type = CellType.Text
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Invisible column from Lot Details screen

      InitBodyColumnText(column, cell, ColumnName.StockID, ColumnName.StockID, 0, 1, ColumnName.StockID, False)
      InitBodyColumnText(column, cell, ColumnName.WarehouseID, ColumnName.WarehouseID, 0, 1, ColumnName.WarehouseID, False)
      InitBodyColumnText(column, cell, ColumnName.GRDate, ColumnName.GRDate, 0, 1, ColumnName.GRDate, False)
      InitBodyColumnText(column, cell, ColumnName.GRWHCode, ColumnName.GRWHCode, 0, 1, ColumnName.GRWHCode, False)
      InitBodyColumnText(column, cell, ColumnName.ProductName, ColumnName.ProductName, 0, 1, ColumnName.ProductName, False)
      InitBodyColumnText(column, cell, ColumnName.ProductKikaku, ColumnName.ProductKikaku, 0, 1, ColumnName.ProductKikaku, False)
      InitBodyColumnText(column, cell, ColumnName.ProductColor, ColumnName.ProductColor, 0, 1, ColumnName.ProductColor, False)
      InitBodyColumnText(column, cell, ColumnName.ProductSize, ColumnName.ProductSize, 0, 1, ColumnName.ProductSize, False)
      InitBodyColumnText(column, cell, ColumnName.UnitDecimalDigits, ColumnName.UnitDecimalDigits, 0, 1, ColumnName.UnitDecimalDigits, False)
      InitBodyColumnText(column, cell, ColumnName.QuantityDecimalDigits, ColumnName.QuantityDecimalDigits, 0, 1, ColumnName.QuantityDecimalDigits, False)
      InitBodyColumnText(column, cell, ColumnName.Dummy, ColumnName.Dummy, 0, 1, ColumnName.Dummy, False)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>Init Text column when create Table</summary>
  ''' <param name="column"></param>
  ''' <param name="cell"></param>
  ''' <param name="columnName"></param>
  ''' <param name="cellname"></param>
  ''' <param name="cellwidth"></param>
  ''' <param name="cellHeight"></param>
  ''' <param name="headerName"></param>
  ''' <param name="editable"></param>
  ''' <remarks></remarks>
  Private Sub InitBodyColumnText(column As PcaColumn, cell As PcaCell, columnName As String, cellname As String, cellwidth As Short, cellHeight As Short, headerName As String, Optional ByVal editable As Boolean = True)
    'Create column text
    column = New PcaColumn(columnName, cellwidth)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(cellname, cellwidth, cellHeight, 0, 0, headerName)
    cell.EditMode = editable
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)
  End Sub

#End Region


#Region "PcaTable Methods"

  Private Sub tblTable_EnterInputRow(sender As System.Object, args As PCA.Controls.InputRowEventArgs) Handles tblTable.EnterInputRow
    Try
      m_IntRowIndex = args.RowIndex
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return
    End Try

  End Sub

  Private Sub tblTable_DoubleClickCell(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.DoubleClickCell

    Try
      Dim strCallPGName As String
      Dim strKeySlipNo As String
      If tblTable.GetCellValue(args.Point.Y, ColumnName.StockID) Is Nothing Then Return
      If tblTable.GetCellValue(args.Point.Y, ColumnName.StockID).ToString.Trim = String.Empty Then
        strCallPGName = TRANS_PG_NAME
      Else
        strCallPGName = STOCK_PG_NAME
      End If
      strKeySlipNo = tblTable.GetCellValue(args.Point.Y, ColumnName.SlipNo).ToString

      If strKeySlipNo.Trim <> String.Empty Then
        SLCmnFunction.StartUpInputExe(IO.Path.GetDirectoryName(Application.ExecutablePath) & strCallPGName, m_loginString, strKeySlipNo)
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return
    End Try

  End Sub

#End Region


#Region "Private Methods"

  ''' <summary>Display Transfealbe SlipList</summary>
  ''' <returns></returns>
  ''' <remarks>日付、伝票種類をもとに振替可能な伝票を取得します</remarks>
  Private Function DisplayTrabsferableSlip() As Boolean

    Dim intRowCount As Integer = 0
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader

    Try

      If chkTransLot.Checked = False And chkStockLot.Checked = False Then
        Sunloft.Message.DisplayBox.ShowError("いずれかの伝票種類" & SLConstants.NotifyMessage.SELECT_REQUIRED)
        chkTransLot.Focus()
        Return False
      End If

      tblTable.DataRowCount = 99
      tblTable.BodyRowCount = 99
      tblTable.ClearCellValues()
      chkIsInclude.Focus()
      ldtDate.Focus()
      'Get details data

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_TRABSFERABLE_SLIP")
      selectCommand.Parameters("@InputDate").SetValue(ldtDate.IntDate.SerializeTarget)
      selectCommand.Parameters("@IncludeFlag").SetValue(chkIsInclude.CheckState)
      reader = selectCommand.ExecuteReader
      While reader.Read()

        If chkStockLot.CheckState = CheckState.Unchecked And CInt(reader.GetValue("StockID").ToString) >= 0 Then Continue While
        If chkTransLot.CheckState = CheckState.Unchecked And CInt(reader.GetValue("WarehouseID").ToString) >= 0 Then Continue While

        tblTable.SetCellValue(intRowCount, ColumnName.Check, True)
        tblTable.SetCellValue(intRowCount, ColumnName.SlipNo, reader.GetValue("SlipNo"))
        tblTable.SetCellValue(intRowCount, ColumnName.Kbn, reader.GetValue("Kbn"))
        tblTable.SetCellValue(intRowCount, ColumnName.TargetDate, Format(reader.GetValue("TargetDate"), "0000/00/00"))
        tblTable.SetCellValue(intRowCount, ColumnName.ProductCode, reader.GetValue("ProductCode"))
        tblTable.SetCellValue(intRowCount, ColumnName.AllProductName, reader.GetValue("AllProductName"))
        tblTable.SetCellValue(intRowCount, ColumnName.WareHouseCode, reader.GetValue("FromWarehouseCode"))
        tblTable.SetCellValue(intRowCount, ColumnName.WareHouseName, reader.GetValue("FromWarehouseName"))
        tblTable.SetCellValue(intRowCount, ColumnName.Quantity, CDbl(reader.GetValue("Quantity")).ToString("#,0.####"))
        tblTable.SetCellValue(intRowCount, ColumnName.UnitName, reader.GetValue("sms_tani").ToString.Trim)
        tblTable.SetCellValue(intRowCount, ColumnName.LotNo, reader.GetValue("sl_zdn_ulotno"))
        tblTable.SetCellValue(intRowCount, ColumnName.StockID, IIf(CInt(reader.GetValue("StockID").ToString) >= 0, reader.GetValue("StockID"), String.Empty))
        tblTable.SetCellValue(intRowCount, ColumnName.WarehouseID, IIf(CInt(reader.GetValue("WarehouseID").ToString) >= 0, reader.GetValue("WarehouseID"), String.Empty))
        tblTable.SetCellValue(intRowCount, ColumnName.GRDate, reader.GetValue("GRDate"))
        tblTable.SetCellValue(intRowCount, ColumnName.GRWHCode, reader.GetValue("GRWarehouseCode"))
        tblTable.SetCellValue(intRowCount, ColumnName.ProductName, reader.GetValue("sms_mei"))
        tblTable.SetCellValue(intRowCount, ColumnName.ProductKikaku, reader.GetValue("sms_kikaku"))
        tblTable.SetCellValue(intRowCount, ColumnName.ProductColor, reader.GetValue("sms_color"))
        tblTable.SetCellValue(intRowCount, ColumnName.ProductSize, reader.GetValue("sms_size"))
        tblTable.SetCellValue(intRowCount, ColumnName.QuantityDecimalDigits, reader.GetValue("sms_sketa"))
        tblTable.SetCellValue(intRowCount, ColumnName.UnitDecimalDigits, reader.GetValue("sms_tketa").ToString)


        intRowCount += 1
      End While
      If intRowCount > 0 Then m_IntRowCount = intRowCount - 1 Else m_IntRowCount = 0

      If intRowCount = 0 Then
        PcaFunctionCommandExecute.Enabled = False
        ToolStripMenuItemExecute.Enabled = False
        ToolStripButtonExecute.Enabled = False
        Return False
      Else
        PcaFunctionCommandExecute.Enabled = True
        ToolStripMenuItemExecute.Enabled = True
        ToolStripButtonExecute.Enabled = True
        Return True
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
      tblTable.DataRowCount = intRowCount
      tblTable.BodyRowCount = intRowCount
    End Try

    Return True
  End Function

  ''' <summary>
  ''' Transfer Slip
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>仕入/出荷伝票に変換します。</remarks>
  Private Function TransferSlip() As Boolean

    Try
      Dim intLpc As Integer = 0
      Dim isSelected As Boolean = False
      Dim result As IIntegratedResult
      Dim isGL As Boolean
      Dim isTransferSlip As Boolean
      Dim dlgResult As DialogResult

      Const INPUT_URI_STRING As String = "InputURI?CalcTotal=True&CalcTax=True"

      ldtDate.Focus()

      For intLpc = 0 To m_IntRowCount
        If CBool(tblTable.GetCellValue(intLpc, ColumnName.Check)) Then isSelected = True
      Next

      If Not isSelected Then
        DisplayBox.ShowError(SLConstants.NotifyMessage.TRANSFER_SLIP_UNSELECTED)
        Return False
      End If

      dlgResult = MessageBox.Show(SLConstants.ConfirmMessage.TRANSFER_CONFIRM, "登録確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

      If dlgResult = Windows.Forms.DialogResult.No Then Return False

      For intLpc = 0 To m_IntRowCount
        If CBool(tblTable.GetCellValue(intLpc, ColumnName.Check).ToString()) Then
          Select Case tblTable.GetCellValue(intLpc, ColumnName.Kbn).ToString
            Case SlipType.Transfer
              isTransferSlip = True
            Case SlipType.GLAjust, SlipType.GLInventry
              isTransferSlip = False
              isGL = True
            Case Else
              isTransferSlip = False
              isGL = False
          End Select

          result = connector.Create(INPUT_URI_STRING, CreateXMLString(isGL, intLpc, isTransferSlip))


          If Not result.Status = IntegratedStatus.Success Then
            DisplayBox.ShowError(result.ErrorMessage + result.BusinessValue)
            Return False
          End If

          If isTransferSlip Then
            result = connector.Create(INPUT_URI_STRING, CreateXMLString(Not isGL, intLpc, isTransferSlip))
            If Not result.Status = IntegratedStatus.Success Then
              DisplayBox.ShowError(result.ErrorMessage + result.BusinessValue)
              Return False
            End If
          End If

          m_IntTransferCount += 1
        End If
      Next

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function

  ''' <summary>
  ''' Make XMLString
  ''' </summary>
  ''' <param name="isGL">出荷フラグ</param>
  ''' <param name="intRowIndex">行番号</param>
  ''' <param name="isTransferSlip">振替伝票フラグ</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateXMLString(ByVal isGL As Boolean, ByVal intRowIndex As Integer, ByVal isTransferSlip As Boolean) As String
    Dim strReturnString As String = String.Empty
    Try
      Dim xEncode As System.Text.Encoding = System.Text.Encoding.GetEncoding(65001I) '65001がUTF8
      Dim xDocument As XmlDataDocument = New XmlDataDocument  'XMLドキュメント作成
      Dim xDeclaration As XmlDeclaration = xDocument.CreateXmlDeclaration("1.0", xEncode.BodyName, Nothing) 'ヘッダ部作成
      Dim xRoot As XmlElement = xDocument.CreateElement("ArrayOfBEInputURI")   'ルート部
      Dim xNodeURI As XmlElement = xDocument.CreateElement("BEInputURI")
      Dim xNodeURIH As XmlElement = xDocument.CreateElement("InputURIH")
      Dim xNodeURIDList As XmlElement = xDocument.CreateElement("InputURIDList")
      Dim xNodeURID As XmlElement = xDocument.CreateElement("BEInputURID")
      Dim xNode As XmlElement = Nothing

      xNode = xDocument.CreateElement("BEVersion")
      xNode.InnerText = CStr(3)
      xNodeURI.AppendChild(xNode)

      xNode = xDocument.CreateElement("Zeikeisan")
      xNode.InnerText = CStr(0)
      xNodeURIH.AppendChild(xNode)

      xNode = xDocument.CreateElement("Syukabi")
      If isGL Then
        xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.GRDate).ToString
      Else
        xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.TargetDate).ToString
      End If
      xNode.InnerText = Replace(xNode.InnerText, "/", String.Empty)
      xNodeURIH.AppendChild(xNode)

      xNode = xDocument.CreateElement("DenpyoNo")
      xNode.InnerText = CStr(0)
      xNodeURIH.AppendChild(xNode)

      xNode = xDocument.CreateElement("SyukasakiCode")
      xNode.InnerText = m_SL_LMBClass.sl_lmb_tcd
      xNodeURIH.AppendChild(xNode)

      xNode = xDocument.CreateElement("YobiInt1")
      If isTransferSlip Then
        xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.WarehouseID).ToString
      Else
        xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.StockID).ToString
      End If
      xNodeURIH.AppendChild(xNode)

      xNode = xDocument.CreateElement("YobiShort1")
      If isTransferSlip Then
        xNode.InnerText = CStr(2)
      Else
        xNode.InnerText = CStr(1)
      End If
      xNodeURIH.AppendChild(xNode)

      xNodeURI.AppendChild(xNodeURIH)

      xNode = xDocument.CreateElement("SyohinCode")
      xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.ProductCode).ToString.Trim
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("ZeiKubun")
      xNode.InnerText = CStr(0)
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("ZeikomiKubun")
      xNode.InnerText = CStr(0)
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("TankaKeta")
      xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.UnitDecimalDigits).ToString.Trim
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("SuryoKeta")
      xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.QuantityDecimalDigits).ToString.Trim
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("SyohinMei")
      xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.ProductName).ToString.Trim
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("KikakuKataban")
      xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.ProductKikaku).ToString.Trim
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("Color")
      xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.ProductColor).ToString.Trim
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("Size")
      xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.ProductSize).ToString.Trim
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("SokoCode")
      If isGL Then
        xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.GRWHCode).ToString.Trim
      Else
        xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.WareHouseCode).ToString.Trim
      End If
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("Irisu")
      xNode.InnerText = CStr(1)
      xNodeURID.AppendChild(xNode)

      Dim decQuantity As Decimal = 0D
      decQuantity = CDec(tblTable.GetCellValue(intRowIndex, ColumnName.Quantity))
      If isGL Then decQuantity *= -1

      xNode = xDocument.CreateElement("Hakosu")
      xNode.InnerText = decQuantity.ToString
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("Suryo")
      xNode.InnerText = decQuantity.ToString
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("Tani")
      xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.UnitName).ToString.Trim
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("Tanka")
      xNode.InnerText = CStr(0)
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("Kingaku")
      xNode.InnerText = CStr(0)
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("Biko")
      xNode.InnerText = tblTable.GetCellValue(intRowIndex, ColumnName.LotNo).ToString.Trim
      xNodeURID.AppendChild(xNode)

      xNode = xDocument.CreateElement("ZeiRitsu")
      xNode.InnerText = CStr(0)
      xNodeURID.AppendChild(xNode)

      xNodeURIDList.AppendChild(xNodeURID)
      xNodeURI.AppendChild(xNodeURIDList)
      xRoot.AppendChild(xNodeURI)
      xDocument.AppendChild(xRoot)

      strReturnString = xDocument.OuterXml

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return String.Empty
    End Try
    Return strReturnString
  End Function

  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean

    If (keyData And Keys.KeyCode) = Keys.Tab And m_isAllowKeyDown Then
      If DisplayTrabsferableSlip() Then
        tblTable.Focus()
      Else
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSlipSearch.CommandId)
        ldtDate.Focus()
      End If
    End If
    Return MyBase.ProcessDialogKey(keyData)
  End Function
#End Region

#Region "Finalize Methods"

  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then
      connector.LogOffSystem()
    End If
  End Sub

#End Region

#Region "PcaCommandManager Methods"

  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try
      Dim commandItem As PcaCommandItem = e.CommandItem
      Select Case commandItem.CommandName

        Case PcaCommandItemHelp.CommandName

        Case PcaCommandItemSlipSearch.CommandName 'F5

          If DisplayTrabsferableSlip() Then
            tblTable.Focus()
          Else
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, _
                                      SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSlipSearch.CommandId)
            ldtDate.Focus()
          End If

        Case PcaCommandItemExecute.CommandName 'F11

          If TransferSlip() Then

            Sunloft.Common.SLCmnFunction.ShowToolTip("振替処理が完了しました。件数：" & m_IntTransferCount & " 件", _
                                                     SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, commandItem.CommandId)
            If DisplayTrabsferableSlip() Then
              tblTable.Focus()
            Else
              ldtDate.Focus()
            End If
          End If

        Case PcaCommandItemClose.CommandName 'F12

          m_IsClosingFlag = True

          Me.Close()

      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region "PcaLabeledDate Methods"

  Private Sub ldtDate_Enter(sender As System.Object, e As System.EventArgs) Handles ldtDate.Enter
    m_isAllowKeyDown = True
  End Sub

  Private Sub ldtDate_Leave(sender As System.Object, e As System.EventArgs) Handles ldtDate.Leave
    m_isAllowKeyDown = False
  End Sub
#End Region


End Class

''' <summary>ColumnName</summary>
''' <remarks></remarks>
Friend Class ColumnName
  Public Const Check As String = "選択"
  Public Const SlipNo As String = "伝票番号"
  Public Const Kbn As String = "区分"
  Public Const TargetDate As String = "日付"
  Public Const ProductCode As String = "商品コード"
  Public Const AllProductName As String = "商品名"
  Public Const WareHouseCode As String = "倉庫コード"
  Public Const WareHouseName As String = "倉庫名"
  Public Const Quantity As String = "数量"
  Public Const UnitName As String = "単位"
  Public Const LotNo As String = "ロットNo"
  'Hidden
  Public Const StockID As String = "在庫調整ID"
  Public Const WarehouseID As String = "倉庫移動ID"
  Public Const GRDate As String = "入庫日付"
  Public Const GRWHCode As String = "入荷倉庫"
  Public Const ProductName As String = "商品名"
  Public Const ProductKikaku As String = "規格"
  Public Const ProductColor As String = "カラー"
  Public Const ProductSize As String = "サイズ"
  Public Const UnitDecimalDigits As String = "単価小数桁"
  Public Const QuantityDecimalDigits As String = "数量小数桁"
  Public Const Dummy As String = "Dummy"

End Class

''' <summary>Kind of Slip</summary>
Friend Class SlipType

  Public Const Transfer As String = "振替"
  Public Const GRAjust As String = "入庫調整"
  Public Const GLAjust As String = "出庫調整"
  Public Const GRInventry As String = "棚卸(入庫)"
  Public Const GLInventry As String = "棚卸(出庫)"
    
End Class
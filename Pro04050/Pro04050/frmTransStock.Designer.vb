﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransStock
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransStock))
    Me.ServiceController1 = New System.ServiceProcess.ServiceController()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.FToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemExecute = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemShow = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonExecute = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonShow = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.ldtDate = New Sunloft.PCAControls.SLPcaLabeledDate()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSlipSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandExecute = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemExecute = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSlipSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.tblTable = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.setLblType = New PCA.Controls.PcaCodeSet()
    Me.chkTransLot = New PCA.Controls.PcaCheckBox()
    Me.chkStockLot = New PCA.Controls.PcaCheckBox()
    Me.chkIsInclude = New PCA.Controls.PcaCheckBox()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'FToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.FToolStripMenuItemFile, Nothing)
    Me.FToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemExecute, Me.ToolStripMenuItemClose})
    Me.FToolStripMenuItemFile.Name = "FToolStripMenuItemFile"
    Me.FToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.FToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemExecute
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemExecute, Me.PcaCommandItemExecute)
    Me.ToolStripMenuItemExecute.Name = "ToolStripMenuItemExecute"
    Me.ToolStripMenuItemExecute.Size = New System.Drawing.Size(151, 22)
    Me.ToolStripMenuItemExecute.Text = "振替実行(&S)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(151, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemShow})
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemShow
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemShow, Me.PcaCommandItemSlipSearch)
    Me.ToolStripMenuItemShow.Name = "ToolStripMenuItemShow"
    Me.ToolStripMenuItemShow.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemShow.Text = "再表示(&R)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContent.Text = "目次(&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonExecute, Me.ToolStripButtonShow, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1008, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Image = Global.Pro04050.My.Resources.Resources._Exit
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonExecute
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonExecute, Me.PcaCommandItemExecute)
    Me.ToolStripButtonExecute.Image = Global.Pro04050.My.Resources.Resources.trans
    Me.ToolStripButtonExecute.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonExecute.Name = "ToolStripButtonExecute"
    Me.ToolStripButtonExecute.Size = New System.Drawing.Size(67, 34)
    Me.ToolStripButtonExecute.Text = "振替実行"
    Me.ToolStripButtonExecute.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonShow
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonShow, Me.PcaCommandItemSlipSearch)
    Me.ToolStripButtonShow.Image = Global.Pro04050.My.Resources.Resources.Search
    Me.ToolStripButtonShow.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonShow.Name = "ToolStripButtonShow"
    Me.ToolStripButtonShow.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonShow.Text = "再表示"
    Me.ToolStripButtonShow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Image = Global.Pro04050.My.Resources.Resources.help
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ldtDate
    '
    Me.ldtDate.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.ldtDate.ClientProcess = Nothing
    Me.ldtDate.DisplaySlash = True
    Me.ldtDate.EmptyIfDisabled = True
    Me.ldtDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtDate.Holidays = Nothing
    Me.ldtDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.ldtDate.LabelText = "日付"
    Me.ldtDate.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtDate.Location = New System.Drawing.Point(12, 102)
    Me.ldtDate.MaxLabelLength = 15
    Me.ldtDate.MaxTextLength = 15
    Me.ldtDate.MustInput = False
    Me.ldtDate.Name = "ldtDate"
    Me.ldtDate.Size = New System.Drawing.Size(210, 22)
    Me.ldtDate.TabIndex = 1
    Me.ldtDate.Tag = "検索日付を入力してください"
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 708)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1008, 22)
    Me.StatusStrip1.TabIndex = 25
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandSlipSearch, Me.PcaFunctionCommandExecute, Me.PcaFunctionCommandClose})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 682)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1008, 26)
    Me.PcaFunctionBar1.TabIndex = 44
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    '
    'PcaFunctionCommandSlipSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSlipSearch, Me.PcaCommandItemSlipSearch)
    Me.PcaFunctionCommandSlipSearch.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandSlipSearch.Tag = Nothing
    Me.PcaFunctionCommandSlipSearch.Text = "再表示"
    Me.PcaFunctionCommandSlipSearch.ToolTipText = "在庫振替可能な伝票を表示します。"
    '
    'PcaFunctionCommandExecute
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandExecute, Me.PcaCommandItemExecute)
    Me.PcaFunctionCommandExecute.FunctionKey = PCA.Controls.FunctionKey.F11
    Me.PcaFunctionCommandExecute.Tag = Nothing
    Me.PcaFunctionCommandExecute.Text = "振替実行"
    Me.PcaFunctionCommandExecute.ToolTipText = "在庫振替を実行します。"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemSlipSearch, Me.PcaCommandItemExecute, Me.PcaCommandItemClose})
    Me.PcaCommandManager1.Parent = Me
    '
    'PcaCommandItemExecute
    '
    Me.PcaCommandItemExecute.CommandId = 11
    Me.PcaCommandItemExecute.CommandName = "Execute"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    Me.PcaCommandItemClose.CommandName = "Close"
    '
    'PcaCommandItemSlipSearch
    '
    Me.PcaCommandItemSlipSearch.CommandId = 5
    Me.PcaCommandItemSlipSearch.CommandName = "Search"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    '
    'tblTable
    '
    Me.tblTable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tblTable.BEKihonJoho = CType(resources.GetObject("tblTable.BEKihonJoho"), PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblTable.ContextMenu = Nothing
    Me.tblTable.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.tblTable.HeadContextMenu = Nothing
    Me.tblTable.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblTable.Location = New System.Drawing.Point(12, 127)
    Me.tblTable.Name = "tblTable"
    Me.tblTable.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblTable.Size = New System.Drawing.Size(984, 529)
    Me.tblTable.TabIndex = 45
    Me.tblTable.UpDownKeyAsTab = True
    '
    'setLblType
    '
    Me.setLblType.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setLblType.AutoRestore = True
    Me.setLblType.AutoTextSize = False
    Me.setLblType.AutoTopMargin = False
    Me.setLblType.ClientProcess = Nothing
    Me.setLblType.CodeEnabled = False
    Me.setLblType.FillZeroFlag = True
    Me.setLblType.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setLblType.GroupBoxVisible = False
    Me.setLblType.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setLblType.LabelTopAlign = True
    Me.setLblType.LimitLength = 4
    Me.setLblType.Location = New System.Drawing.Point(12, 80)
    Me.setLblType.MaxCodeLength = 1
    Me.setLblType.MaxLabelLength = 15
    Me.setLblType.MaxNameLength = 0
    Me.setLblType.Name = "setLblType"
    Me.setLblType.ReferButtonVisible = False
    Me.setLblType.Size = New System.Drawing.Size(106, 22)
    Me.setLblType.TabIndex = 47
    Me.setLblType.TabStop = False
    Me.setLblType.Text = "伝票種類"
    Me.setLblType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'chkTransLot
    '
    Me.chkTransLot.Checked = True
    Me.chkTransLot.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chkTransLot.Location = New System.Drawing.Point(125, 80)
    Me.chkTransLot.Name = "chkTransLot"
    Me.chkTransLot.Size = New System.Drawing.Size(140, 22)
    Me.chkTransLot.TabIndex = 1
    Me.chkTransLot.TabStop = False
    Me.chkTransLot.Text = "振替ロット伝票"
    Me.chkTransLot.UseVisualStyleBackColor = True
    '
    'chkStockLot
    '
    Me.chkStockLot.AutoTextSize = False
    Me.chkStockLot.Checked = True
    Me.chkStockLot.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chkStockLot.Location = New System.Drawing.Point(265, 80)
    Me.chkStockLot.Name = "chkStockLot"
    Me.chkStockLot.Size = New System.Drawing.Size(166, 22)
    Me.chkStockLot.TabIndex = 2
    Me.chkStockLot.TabStop = False
    Me.chkStockLot.Text = "在庫調整ロット伝票"
    Me.chkStockLot.UseVisualStyleBackColor = True
    '
    'chkIsInclude
    '
    Me.chkIsInclude.AutoTextSize = False
    Me.chkIsInclude.Checked = True
    Me.chkIsInclude.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chkIsInclude.Location = New System.Drawing.Point(265, 102)
    Me.chkIsInclude.Name = "chkIsInclude"
    Me.chkIsInclude.Size = New System.Drawing.Size(260, 22)
    Me.chkIsInclude.TabIndex = 4
    Me.chkIsInclude.TabStop = False
    Me.chkIsInclude.Text = "指定日以前で未振替のデータを含む"
    Me.chkIsInclude.UseVisualStyleBackColor = True
    '
    'frmTransStock
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1008, 730)
    Me.Controls.Add(Me.chkIsInclude)
    Me.Controls.Add(Me.chkStockLot)
    Me.Controls.Add(Me.chkTransLot)
    Me.Controls.Add(Me.setLblType)
    Me.Controls.Add(Me.tblTable)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.ldtDate)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1024, 726)
    Me.Name = "frmTransStock"
    Me.Text = "在庫振替処理"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents ServiceController1 As System.ServiceProcess.ServiceController
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents FToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemExecute As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonExecute As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonShow As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemSlipSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemExecute As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandSlipSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandExecute As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents tblTable As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents ldtDate As Sunloft.PCAControls.SLPcaLabeledDate
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents ToolStripMenuItemShow As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents setLblType As PCA.Controls.PcaCodeSet
  Friend WithEvents chkIsInclude As PCA.Controls.PcaCheckBox
  Friend WithEvents chkStockLot As PCA.Controls.PcaCheckBox
  Friend WithEvents chkTransLot As PCA.Controls.PcaCheckBox

End Class

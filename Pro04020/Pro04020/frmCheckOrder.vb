﻿Imports PCA.Controls
Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports Sunloft.PCAIF
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports PCA.TSC.Kon
Imports System.Globalization
Imports System.Text

Public Class frmCheckOrder
    Private m_columnName As columnName = New columnName

    Private Const WIDTH_TYPE As Single = 90
    Private Const WIDTH_ADJUSTMENT_DATE As Single = 100
    Private Const WIDTH_SLIP_NO As Single = 70
    Private Const WIDTH_SLIP_NO2 As Single = 70
    Private Const WIDTH_PRODUCT_CODE As Single = 60
    Private Const WIDTH_PRODUCT_NAME As Single = 180
    Private Const WIDTH_WAREHOUSE_CODE As Single = 60
    Private Const WIDTH_WAREHOUSE_NAME As Single = 120
    Private Const WIDTH_NUMBER_NO As Single = 50
    Private Const WIDTH_QUANTITY As Single = 100
    Private Const WIDTH_UNIT As Single = 50
    Private Const WIDTH_LOT_NO As Single = 150
    Private Const WIDTH_ADJUSTMENT_REASON_CODE As Single = 120
    Private Const WIDTH_ADJUSTMENT_REASON_NAME As Single = 150
    Private Const WIDTH_SHIPMENT_DEADLINE As Single = 120
    Private Const WIDTH_USE_BY_DATE As Single = 100
    Private Const WIDTH_BEST_BEFORE_DATE As Single = 100
    Private Const WIDTH_EXPIRATION_DATE As Single = 100
    Private Const WIDTH_LOT_NAME_DETAILS As Single = 180
    Private Const WIDTH_INVENTORY_ADJUSTMENT_ID As Single = 150

    Private Const TYPE_COLUMN As Integer = 0
    Private Const ADJUSTMENT_DATE_COLUMN As Integer = 1
    Private Const SLIP_NO_COLUMN As Integer = 2
    Private Const SLIP_NO2_COLUMN As Integer = 3
    Private Const PRODUCT_CODE_COLUMN As Integer = 4
    Private Const PRODUCT_NAME_COLUMN As Integer = 5
    Private Const WAREHOUSE_CODE_COLUMN As Integer = 6
    Private Const WAREHOUSE_NAME_COLUMN As Integer = 7
    Private Const NUMBER_NO_COLUMN As Integer = 8
    Private Const QUANTITY_COLUMN As Integer = 9
    Private Const UNIT_COLUMN As Integer = 10
    Private Const LOT_NO_COLUMN As Integer = 11
    Private Const ADJUSTMENT_REASON_CODE_COLUMN As Integer = 12
    Private Const ADJUSTMENT_REASON_NAME_COLUMN As Integer = 13
    Private Const SHIPMENT_DEADLINE_COLUMN As Integer = 14
    Private Const USE_BY_DATE_COLUMN As Integer = 15
    Private Const BEST_BEFORE_DATE_COLUMN As Integer = 16
    Private Const EXPIRATION_DATE_COLUMN As Integer = 17
    Private Const INVENTORY_ADJUSTMENT_ID_COLUMN As Integer = 18
    Private Const LOT_NAME_DETAILS_COLUMN As Integer = 19

    Private Const ROW_COLUMN As Integer = 19
    Private Const HEIGHT_HEADER As Integer = 25
    Private Const NUMBER_TYPE As String = "Number"
    Private Const TEXT_TYPE As String = "String"
    Private Const DATE_TYPE As String = "Date"
    Private Const NOT_USE As Integer = 0
    Private Const USE As Integer = 1
    Private Const PROGRAM_TO_CALL As String = "\Pro04010.exe" 'Click table cell -> Display slip
  Private Const RPT_FILENAME As String = "InventoryAdjustmentSlips04020.rpt" ' Form to preview/Print
    Private Const RPT_TITLE As String = "在庫調整ロット一覧表"

    Private Const FROM_PRODUCT_CODE_TEXT As Integer = 0
    Private Const TO_PRODUCT_CODE_TEXT As Integer = 1
    Private Const FROM_WAREHOUSE_CODE_TEXT As Integer = 2
    Private Const TO_WAREHOUSE_CODE_TEXT As Integer = 3
    Private Const FROM_ADJUSTMENT_CODE_TEXT As Integer = 4
    Private Const TO_ADJUSTMENT_CODE_TEXT As Integer = 5
    Private Const CSV_FILE_NAME As String = "在庫調整ロット一覧表"

    Property connector As IIntegratedApplication = Nothing
    Private m_appClass As ConnectToPCA = New ConnectToPCA()
    Private m_SL_LMBClass As SL_LMB
    Private m_AMS1Class As AMS1
    Private m_loginString As String
    Private m_DoSearch As Boolean = False
    Private m_currentString As String = ""
    Private m_currentItem As Integer
    Private MasterDialog As SLMasterSearchDialog
    Private m_result As columnResult() = {New columnResult}
    Private m_conf As Sunloft.PcaConfig
    Private arrLotDetails(10) As String
    Private m_isKeyDowned As Boolean = False
    Private m_SlipNoColumn As Integer
    Private f_adjustmentDate As String = ""
    Private t_adjustmentDate As String = ""
    Private m_lShippingType As String = ""
    Private m_enter As Integer = 0


    Public Sub New()
        Try
            InitializeComponent()

            m_appClass.ConnectToPCA()
            connector = m_appClass.connector
            m_conf = New Sunloft.PcaConfig
            m_loginString = m_appClass.EncryptedLoginString
            m_SL_LMBClass = New SL_LMB(connector)
            m_SL_LMBClass.ReadOnlyRow()
            m_AMS1Class = New AMS1(connector)
            m_AMS1Class.ReadAll()
            MasterDialog = New SLMasterSearchDialog(connector)

            arrLotDetails(0) = m_SL_LMBClass.sl_lmb_label1
            arrLotDetails(1) = m_SL_LMBClass.sl_lmb_label2
            arrLotDetails(2) = m_SL_LMBClass.sl_lmb_label3
            arrLotDetails(3) = m_SL_LMBClass.sl_lmb_label4
            arrLotDetails(4) = m_SL_LMBClass.sl_lmb_label5
            arrLotDetails(5) = m_SL_LMBClass.sl_lmb_label6
            arrLotDetails(6) = m_SL_LMBClass.sl_lmb_label7
            arrLotDetails(7) = m_SL_LMBClass.sl_lmb_label8
            arrLotDetails(8) = m_SL_LMBClass.sl_lmb_label9
            arrLotDetails(9) = m_SL_LMBClass.sl_lmb_label10

            'Disable refer command (F8)
            PcaCommandItemRefer.Enabled = False
            PcaCommandItemPrint.Enabled = False
            PcaCommandItemExport.Enabled = False
            PcaCommandItemPreview.Enabled = False
      'set condition LimitLength
      productCode.LimitLength = m_AMS1Class.ams1_ProductLength
      codeWarehouse.LimitLength = m_AMS1Class.ams1_WarehouseLength
      adjustmentManagerCodes.LimitLength = m_AMS1Class.ams1_MemoLength

        Catch ex As Exception
            DisplayBox.ShowCritical(ex)
        End Try
    End Sub
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        InitTable(True)
        MyBase.OnLoad(e)
    End Sub
#Region "InitTable"
    ''' <summary>
    '''  Set table and Alignment table 
    ''' </summary>
    ''' <param name="isEmpty">Clear search result array</param>
    ''' <remarks></remarks>
    Private Sub InitTable(Optional ByVal isEmpty As Boolean = False)
        Try
            dgv.Columns.Clear()


            Dim dgvColumnHeaderStyle As New DataGridViewCellStyle()
            dgvColumnHeaderStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

      initTableColumn(TEXT_TYPE, m_columnName.type, CInt(WIDTH_TYPE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
            initTableColumn(TEXT_TYPE, m_columnName.adjustmentDate, CInt(WIDTH_ADJUSTMENT_DATE), DataGridViewContentAlignment.MiddleCenter, , False, DataGridViewContentAlignment.MiddleCenter)
            initTableColumn(TEXT_TYPE, m_columnName.slipNo, CInt(WIDTH_SLIP_NO), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
            initTableColumn(TEXT_TYPE, m_columnName.slipNo2, CInt(WIDTH_SLIP_NO2), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, m_columnName.productCode, CInt(WIDTH_PRODUCT_CODE), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, m_columnName.productName, CInt(WIDTH_PRODUCT_NAME), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
            initTableColumn(TEXT_TYPE, m_columnName.warehouseCode, CInt(WIDTH_WAREHOUSE_CODE), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, m_columnName.warehouseName, CInt(WIDTH_WAREHOUSE_NAME), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
            initTableColumn(TEXT_TYPE, m_columnName.number, CInt(WIDTH_NUMBER_NO), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
            initTableColumn(TEXT_TYPE, m_columnName.quantity, CInt(WIDTH_QUANTITY), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
            initTableColumn(TEXT_TYPE, m_columnName.unit, CInt(WIDTH_UNIT), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
            initTableColumn(TEXT_TYPE, m_columnName.lotNo, CInt(WIDTH_LOT_NO), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
            initTableColumn(TEXT_TYPE, m_columnName.adjustmentReasonCode, CInt(WIDTH_ADJUSTMENT_REASON_CODE), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, m_columnName.adjustmentReasonName, CInt(WIDTH_ADJUSTMENT_REASON_NAME), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
            initTableColumn(TEXT_TYPE, m_columnName.shipmentDeadline, CInt(WIDTH_SHIPMENT_DEADLINE), DataGridViewContentAlignment.MiddleCenter, , False, DataGridViewContentAlignment.MiddleCenter)
            initTableColumn(TEXT_TYPE, m_columnName.useByDate, CInt(WIDTH_USE_BY_DATE), DataGridViewContentAlignment.MiddleCenter, , False, DataGridViewContentAlignment.MiddleCenter)
            initTableColumn(TEXT_TYPE, m_columnName.bestBeforeDate, CInt(WIDTH_BEST_BEFORE_DATE), DataGridViewContentAlignment.MiddleCenter, , False, DataGridViewContentAlignment.MiddleCenter)
            initTableColumn(TEXT_TYPE, m_columnName.expirationDate, CInt(WIDTH_EXPIRATION_DATE), DataGridViewContentAlignment.MiddleCenter, , False, DataGridViewContentAlignment.MiddleCenter)
            If m_SL_LMBClass.sl_lmb_sdlflg = NOT_USE Then
                Me.dgv.Columns(m_columnName.shipmentDeadline).Visible = False
            End If
            If m_SL_LMBClass.sl_lmb_ubdflg = NOT_USE Then
                Me.dgv.Columns(m_columnName.useByDate).Visible = False
            End If
            If m_SL_LMBClass.sl_lmb_bbdflg = NOT_USE Then
                Me.dgv.Columns(m_columnName.bestBeforeDate).Visible = False
            End If
            If m_SL_LMBClass.sl_lmb_expflg = NOT_USE Then
                Me.dgv.Columns(m_columnName.expirationDate).Visible = False
            End If

            initTableColumn(TEXT_TYPE, m_columnName.inventoryAdjustmentID, CInt(WIDTH_INVENTORY_ADJUSTMENT_ID), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
            Me.dgv.Columns(m_columnName.slipNo2).Visible = False
            Me.dgv.Columns(m_columnName.inventoryAdjustmentID).Visible = False

            For intCount As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
                initTableColumn(TEXT_TYPE, arrLotDetails(intCount), CInt(WIDTH_LOT_NAME_DETAILS), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
            Next

            'Set header cell font color to white
            For intCount As Integer = 0 To m_SlipNoColumn - 1
                dgv.Columns(intCount).HeaderCell.Style.ForeColor = Color.White
            Next
            'Set header size (Default size is a bit small)
            dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
            dgv.ColumnHeadersHeight = HEIGHT_HEADER

            'Set table not sortable
            For Each column As DataGridViewColumn In dgv.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            If isEmpty Then
                m_result = Nothing 'Clear search result array
                dgv.Rows.Clear()
            End If

            'Set table not sortable
            For Each column As DataGridViewColumn In dgv.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

        Catch ex As Exception
            DisplayBox.ShowCritical(ex)
        End Try
    End Sub
     ''' <summary>
    ''' set alignment header
    ''' </summary>
    ''' <param name="headerType">Header Type</param>
    ''' <param name="headerText"> Header Text</param>
    ''' <param name="columnwidth"> Width of column</param>
    ''' <param name="columnname">Name of column</param>
    ''' <param name="isReadonly">Readonly</param>
    ''' <remarks></remarks>
  Private Sub initTableColumn(ByVal headerType As String, ByVal columnname As String, ByVal columnwidth As Integer, Optional ByVal alignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft, Optional ByVal headerText As String = "", Optional ByVal isReadonly As Boolean = True, Optional ByVal headerAlignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft)
    Dim col As New DataGridViewColumn
    Select Case headerType
      Case NUMBER_TYPE
        col = New DataGridViewTextBoxColumn
      Case TEXT_TYPE
        col = New DataGridViewTextBoxColumn
      Case Else
        col = New DataGridViewTextBoxColumn
    End Select
    col.ReadOnly = isReadonly

    col.Name = columnname
    If col.Name = m_columnName.productName Or col.Name = m_columnName.adjustmentReasonName Or col.Name = m_columnName.warehouseName Then
      col.HeaderText = headerText
    End If
    col.Width = columnwidth
    col.DefaultCellStyle.Alignment = alignment
    col.HeaderCell.Style.Alignment = headerAlignment
    col.DefaultCellStyle.FormatProvider = CultureInfo.CreateSpecificCulture("ja-JP")
    dgv.Columns.Add(col)
  End Sub
#End Region
#Region "Private Method"
    ''' <summary>
    ''' Search and desplay data 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DoSearch()
        dgv.Focus()
        'dgv.ClearCellValues()
        GetSearchResult()
        If Not m_result Is Nothing Then
            DisplaySearchResult()
        End If

    End Sub

    '***********************************************************																														
    'Name          : GetSearchResult
    'Content       : Event called when the user clicks search button
    '                make a query into database to get the result
    'Return Value  : 
    'Argument      : 
    'Created date  : 2016/03/14  by DuyTN
    'Modified date :        by       Content: 																											
    '***********************************************************
    Private Sub GetSearchResult()
        Try
            Dim selectCommand As ICustomCommand
            Dim reader As ICustomDataReader
            Dim fromDate As Integer = adjustmentDate.FromIntDate
            Dim toDate As Integer = adjustmentDate.ToIntDate
            Dim lShippingType As String = CStr(shippingType.SelectedItem)
            Dim fromProductCode As String = productCode.FromCodeText
            Dim toProductCode As String = productCode.ToCodeText
            Dim fromCodeWarehouse As String = codeWarehouse.FromCodeText
            Dim toCodeWarehouse As String = codeWarehouse.ToCodeText
            Dim fromAdjustmentManagerCodes As String = adjustmentManagerCodes.FromCodeText
            Dim toAdjustmentManagerCodes As String = adjustmentManagerCodes.ToCodeText
            Dim intCount As Integer = -1

            If Not m_result Is Nothing Then
                Array.Clear(m_result, 0, m_result.Length)
            End If

            selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKD_Pro04020")
            selectCommand.Parameters("@fromAdjustmentDate").SetValue(fromDate)
            selectCommand.Parameters("@toAdjustmentDate").SetValue(toDate)
            selectCommand.Parameters("@shippingType").SetValue(lShippingType)
            selectCommand.Parameters("@fromProductCode").SetValue(fromProductCode)
            selectCommand.Parameters("@toProductCode").SetValue(toProductCode)
            selectCommand.Parameters("@fromCodeWarehouse").SetValue(fromCodeWarehouse)
            selectCommand.Parameters("@toCodeWarehouse").SetValue(toCodeWarehouse)
            selectCommand.Parameters("@fromAdjustmentManagerCodes").SetValue(fromAdjustmentManagerCodes)
            selectCommand.Parameters("@toAdjustmentManagerCodes").SetValue(toAdjustmentManagerCodes)
            reader = selectCommand.ExecuteReader
            While reader.Read() = True
                intCount = intCount + 1
                ReDim Preserve m_result(intCount)
                m_result(intCount) = New columnResult
                'value 区分
                m_result(intCount).type = reader.GetValue("sl_zadj_iokbn").ToString()
                If CDbl(m_result(intCount).type) = 1 Then
                    m_result(intCount).type = "出庫"
                ElseIf CDbl(m_result(intCount).type) = 2 Then
                    m_result(intCount).type = "入庫"
                ElseIf CDbl(m_result(intCount).type) = 11 Then
                    m_result(intCount).type = "棚卸(出庫)"
                ElseIf CDbl(m_result(intCount).type) = 12 Then
                    m_result(intCount).type = "棚卸(入庫)"
                End If

                m_result(intCount).adjustmentDate = Date.ParseExact(reader.GetValue("sl_zadj_date").ToString(), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_zadj_denno")) Then
          m_result(intCount).slipNo = CStr(CInt(reader.GetValue("sl_zadj_denno")))
        End If
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_zadj_denno2")) Then
          m_result(intCount).slipNo2 = CStr((reader.GetValue("sl_zadj_denno2")))
        End If

        m_result(intCount).productCode = reader.GetValue("sl_zadj_scd").ToString()
        m_result(intCount).productName = reader.GetValue("sms_mei").ToString.TrimEnd() & reader.GetValue("sms_kikaku").ToString.TrimEnd() & reader.GetValue("sms_color").ToString.TrimEnd() & reader.GetValue("sms_size").ToString.TrimEnd()

        m_result(intCount).warehouseCode = reader.GetValue("sl_zadj_souko").ToString()
        m_result(intCount).warehouseName = reader.GetValue("ems_str").ToString.TrimEnd()

        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_zadj_honsu")) Then
          m_result(intCount).number = CInt(reader.GetValue("sl_zadj_honsu"))
        End If
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_zadj_suryo")) Then
          m_result(intCount).quantity = CDec(reader.GetValue("sl_zadj_suryo"))
        End If

        m_result(intCount).unit = reader.GetValue("sms_tani").ToString()

        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_zdn_ulotno")) Then
          m_result(intCount).lotNo = reader.GetValue("sl_zdn_ulotno").ToString
        End If
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_zadj_tekcd")) Then
          m_result(intCount).adjustmentReasonCode = reader.GetValue("sl_zadj_tekcd").ToString()
        End If
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_zadj_tekmei")) Then
          m_result(intCount).adjustmentReasonName = reader.GetValue("sl_zadj_tekmei").ToString()
        End If
        m_result(intCount).shipmentDeadline = reader.GetValue("sl_zdn_sdldate").ToString()
        m_result(intCount).useByDate = reader.GetValue("sl_zdn_ubdate").ToString()
        m_result(intCount).bestBeforeDate = reader.GetValue("sl_zdn_bbdate").ToString()
        m_result(intCount).expirationDate = reader.GetValue("sl_zdn_expdate").ToString()
        convertDate(m_result(intCount).shipmentDeadline)
        convertDate(m_result(intCount).useByDate)
        convertDate(m_result(intCount).bestBeforeDate)
        convertDate(m_result(intCount).expirationDate)

        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_zadj_id")) Then
          m_result(intCount).inventoryAdjustmentID = reader.GetValue("sl_zadj_id").ToString()
        End If

        For intCountLotDetails As Integer = 1 To m_SL_LMBClass.sl_lmb_maxlotop
          m_result(intCount).Lotdetails(intCountLotDetails - 1) = reader.GetValue("sl_zdn_ldmei" & intCountLotDetails.ToString).ToString()
        Next
        m_result(intCount).quantityNoOfDigit = CInt(reader.GetValue("sms_sketa"))

        ' MsgBox("Slip no : " & m_result(intCount).slipNo & " ID: " & reader.GetValue("sl_zdn_id").ToString)

      End While
      reader.Close()
      reader.Dispose()

      If Not m_result Is Nothing AndAlso Not m_result(0) Is Nothing Then
        PcaCommandItemPrint.Enabled = True
        PcaCommandItemExport.Enabled = True
        PcaCommandItemPreview.Enabled = True

        f_adjustmentDate = Date.ParseExact(CStr(fromDate), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        t_adjustmentDate = Date.ParseExact(CStr(toDate), "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        m_lShippingType = lShippingType
      Else
        PcaCommandItemPrint.Enabled = False
        PcaCommandItemRefer.Enabled = False
        PcaCommandItemExport.Enabled = False
        PcaCommandItemPreview.Enabled = False
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSearch.CommandId)
        adjustmentDate.FocusFrom()
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub
  Private Function convertDate(ByRef varString As String) As Boolean
    Dim result As Integer
    If Not varString Is Nothing And varString.Length = 8 And Integer.TryParse(varString, result) Then
      varString = Date.ParseExact(varString, "yyyyMMdd", provider:=CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
      Return True
    End If
    If varString.ToString = "" Then
      varString = ""
      Return True
    End If
    If CDbl(varString) = 0 Then
      varString = ""
      Return True
    End If
    Return False
  End Function

  '***********************************************************																														
  'Name          : DisplaySearchResult
  'Content       : Display search result into table
  'Return Value  : 
  'Argument      : 
  'Created date  : 2016/03/14  by DuyTN
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub DisplaySearchResult()
    Try
      dgv.Columns.Clear()
      InitTable()

      For intCount As Integer = 0 To m_result.Length - 1
        If Not SLCmnFunction.checkObjectNothingEmpty(m_result(intCount)) Then
          dgv.Rows.Add(New DataGridViewRow)

          dgv.Rows.Item(intCount).Cells.Item(TYPE_COLUMN).Value = m_result(intCount).type
          dgv.Rows.Item(intCount).Cells.Item(ADJUSTMENT_DATE_COLUMN).Value = m_result(intCount).adjustmentDate.Trim
          dgv.Rows.Item(intCount).Cells.Item(SLIP_NO_COLUMN).Value = m_result(intCount).slipNo
          dgv.Rows.Item(intCount).Cells.Item(SLIP_NO2_COLUMN).Value = m_result(intCount).slipNo2

          dgv.Rows.Item(intCount).Cells.Item(PRODUCT_CODE_COLUMN).Value = m_result(intCount).productCode
          dgv.Rows.Item(intCount).Cells.Item(PRODUCT_NAME_COLUMN).Value = m_result(intCount).productName
          dgv.Rows.Item(intCount).Cells.Item(WAREHOUSE_CODE_COLUMN).Value = m_result(intCount).warehouseCode
          dgv.Rows.Item(intCount).Cells.Item(WAREHOUSE_NAME_COLUMN).Value = m_result(intCount).warehouseName
          dgv.Rows.Item(intCount).Cells.Item(QUANTITY_COLUMN).Value = SLCmnFunction.formatNumberOfDigit(CDec(m_result(intCount).quantity), m_result(intCount).quantityNoOfDigit)
          dgv.Rows.Item(intCount).Cells.Item(NUMBER_NO_COLUMN).Value = SLCmnFunction.formatNumberOfDigit(m_result(intCount).number)
          dgv.Rows.Item(intCount).Cells.Item(UNIT_COLUMN).Value = m_result(intCount).unit
          dgv.Rows.Item(intCount).Cells.Item(LOT_NO_COLUMN).Value = m_result(intCount).lotNo
          dgv.Rows.Item(intCount).Cells.Item(ADJUSTMENT_REASON_CODE_COLUMN).Value = m_result(intCount).adjustmentReasonCode
          dgv.Rows.Item(intCount).Cells.Item(ADJUSTMENT_REASON_NAME_COLUMN).Value = m_result(intCount).adjustmentReasonName
          dgv.Rows.Item(intCount).Cells.Item(SHIPMENT_DEADLINE_COLUMN).Value = m_result(intCount).shipmentDeadline
          dgv.Rows.Item(intCount).Cells.Item(USE_BY_DATE_COLUMN).Value = m_result(intCount).useByDate
          dgv.Rows.Item(intCount).Cells.Item(BEST_BEFORE_DATE_COLUMN).Value = m_result(intCount).bestBeforeDate
          dgv.Rows.Item(intCount).Cells.Item(EXPIRATION_DATE_COLUMN).Value = m_result(intCount).expirationDate
          dgv.Rows.Item(intCount).Cells.Item(INVENTORY_ADJUSTMENT_ID_COLUMN).Value = m_result(intCount).inventoryAdjustmentID
          For intCountLotDetails As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
            dgv.Rows.Item(intCount).Cells.Item(ROW_COLUMN + intCountLotDetails).Value = m_result(intCount).Lotdetails(intCountLotDetails)
          Next
          ' dgv.Rows.Item(intCount).Cells.Item(m_SlipNoColumn).Value = m_result(intCount).slipNo.ToString.Trim
        End If
      Next

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

#End Region
#Region "validation"
#Region "Event Product Code Button"

  Private Sub productCode_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles productCode.ToCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = productCode.FromCodeText
    m_currentItem = TO_PRODUCT_CODE_TEXT
  End Sub

  Private Sub productCode_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles productCode.FromCodeTextEnter
    m_DoSearch = False
    PcaCommandItemRefer.Enabled = True
    m_currentString = productCode.ToCodeText
    m_currentItem = FROM_PRODUCT_CODE_TEXT
  End Sub

  Private Sub productCode_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles productCode.Leave
    PcaCommandItemRefer.Enabled = False
  End Sub
  Private Sub productCode_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles productCode.FromCodeTextValidating2
    validateMangement(e, productCode.FromCodeText)
  End Sub
  Private Sub productCode_ToCodeTextValidated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles productCode.ToCodeTextValidated
    PcaCommandItemRefer.Enabled = False
  End Sub

  Private Sub productCode_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles productCode.ToCodeTextValidating2
    validateMangement(e, productCode.ToCodeText)
  End Sub
  Private Sub productCode_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles productCode.ClickFromReferButton2
    m_currentItem = FROM_PRODUCT_CODE_TEXT
    DisplayReferScreen(e, FROM_PRODUCT_CODE_TEXT)
  End Sub

  Private Sub productCode_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles productCode.ClickToReferButton2
    m_currentItem = TO_PRODUCT_CODE_TEXT
    DisplayReferScreen(e, TO_PRODUCT_CODE_TEXT)
  End Sub
#End Region
#Region "Event Adjustment reason Button"

  Private Sub adjustmentManagerCodes_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles adjustmentManagerCodes.FromCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = adjustmentManagerCodes.FromCodeText
    m_currentItem = FROM_ADJUSTMENT_CODE_TEXT
  End Sub

  Private Sub adjustmentManagerCodes_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles adjustmentManagerCodes.Leave
    PcaCommandItemRefer.Enabled = False
    'm_currentItem = NO_SELECTED_CODETEXT
    If m_enter = 1 Then
      adjustmentManagerCodes.FocusTo()
      m_enter = 0
    End If

  End Sub

  Private Sub adjustmentManagerCodes_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles adjustmentManagerCodes.ToCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = adjustmentManagerCodes.FromCodeText
    m_currentItem = TO_ADJUSTMENT_CODE_TEXT
  End Sub
  Private Sub adjustmentManagerCodes_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles adjustmentManagerCodes.ClickFromReferButton2
    m_currentItem = FROM_ADJUSTMENT_CODE_TEXT
    DisplayReferScreen(e, FROM_ADJUSTMENT_CODE_TEXT)
  End Sub

  Private Sub adjustmentManagerCodes_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles adjustmentManagerCodes.ClickToReferButton2
    m_currentItem = TO_ADJUSTMENT_CODE_TEXT
    DisplayReferScreen(e, TO_ADJUSTMENT_CODE_TEXT)
    adjustmentManagerCodes.FocusTo()

  End Sub

  Private Sub adjustmentManagerCodes_ToCodeTextValidated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles adjustmentManagerCodes.ToCodeTextValidated
    PcaCommandItemRefer.Enabled = False
    If m_DoSearch Then
      m_DoSearch = False
      DoSearch()
    End If
  End Sub

  Private Sub adjustmentManagerCodes_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles adjustmentManagerCodes.ToCodeTextValidating2
    validateMangement(e, adjustmentManagerCodes.ToCodeText)
  End Sub

  Private Sub adjustmentManagerCodes_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles adjustmentManagerCodes.FromCodeTextValidating2
    validateMangement(e, adjustmentManagerCodes.FromCodeText)
  End Sub
#End Region
#Region "Event Warehouse code Button"
  Private Sub codeWarehouse_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles codeWarehouse.FromCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = codeWarehouse.FromCodeText
    m_currentItem = FROM_WAREHOUSE_CODE_TEXT
  End Sub

  Private Sub codeWarehouse_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles codeWarehouse.Leave
    PcaCommandItemRefer.Enabled = False
    ' m_currentItem = NO_SELECTED_CODETEXT
  End Sub

  Private Sub codeWarehouse_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles codeWarehouse.ToCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_currentString = codeWarehouse.FromCodeText
    m_currentItem = TO_WAREHOUSE_CODE_TEXT
  End Sub

  Private Sub codeWarehouse_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles codeWarehouse.ClickFromReferButton2
    m_currentItem = FROM_WAREHOUSE_CODE_TEXT
    DisplayReferScreen(e, FROM_WAREHOUSE_CODE_TEXT)
  End Sub

  Private Sub codeWarehouse_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles codeWarehouse.ClickToReferButton2
    m_currentItem = TO_WAREHOUSE_CODE_TEXT
    DisplayReferScreen(e, TO_WAREHOUSE_CODE_TEXT)
  End Sub

  Private Sub codeWarehouse_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles codeWarehouse.FromCodeTextValidating2
    validateMangement(e, codeWarehouse.FromCodeText)
  End Sub

  Private Sub codeWarehouse_ToCodeTextValidated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles codeWarehouse.ToCodeTextValidated
    PcaCommandItemRefer.Enabled = False
  End Sub

  Private Sub codeWarehouse_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles codeWarehouse.ToCodeTextValidating2
    validateMangement(e, codeWarehouse.ToCodeText)
  End Sub
#End Region
  '***********************************************************																														
  'Name          : validateMangement
  'Content       : Event called when Warehouse code , Product code , adjustment Manager Codes (From, To Leave event)
  'Return Value  : 
  'Argument      : e As PCA.Controls.ValidatingEventArgs, CodeText As String
  'Created date  : 2016/03/12  by DuyTN
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub validateMangement(ByVal e As PCA.Controls.ValidatingEventArgs, ByRef CodeText As String)
    ' validate productCode
    If m_currentItem = FROM_PRODUCT_CODE_TEXT Or m_currentItem = TO_PRODUCT_CODE_TEXT Then
      Try
        Dim curProductCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_ProductLength)
        If Not String.IsNullOrEmpty(curProductCode) AndAlso m_currentString = curProductCode Then
          CodeText = curProductCode
          'No changes
          Return
        End If
        If String.IsNullOrEmpty(curProductCode) Then
          Return
        End If

        'validate supplier code
        Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms = MasterDialog.FindBEMasterSms(curProductCode)
        If Not beMasterSms.SyohinCode.Length > 0 Then
          'If there's error
          'e.Cancel = True
          'CodeText = m_currentString 'MasterDialog.ModifiedEntity.Header.TorihikisakiCode
          Return
        Else
          CodeText = curProductCode
        End If

      Catch ex As Exception
        DisplayBox.ShowCritical(ex)
      End Try

      ' validate adjustmentManagerCodes
    ElseIf m_currentItem = FROM_ADJUSTMENT_CODE_TEXT Or m_currentItem = TO_ADJUSTMENT_CODE_TEXT Then
      Try
        Dim curAdjustmentReasonCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_MemoLength)
        If Not String.IsNullOrEmpty(curAdjustmentReasonCode) AndAlso m_currentString = curAdjustmentReasonCode Then
          CodeText = curAdjustmentReasonCode
          'No changes
          Return
        End If
        If String.IsNullOrEmpty(curAdjustmentReasonCode) Then
          Return
        End If
        'validate supplier code
        Dim beMasterTekiyo As PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo = MasterDialog.FindBEMasterTekiyo(KubunIdType.TekiyoKubun7, curAdjustmentReasonCode)
        'Dim strName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, newCode)

        If Not beMasterTekiyo.TekiyoCode.Length > 0 Then
          'If there's error
          'e.Cancel = True
          'CodeText = m_currentString 'MasterDialog.ModifiedEntity.Header.TorihikisakiCode
          Return
        Else
          CodeText = curAdjustmentReasonCode
        End If
        'adjustmentManagerCodes.FocusTo()
      Catch ex As Exception
        DisplayBox.ShowCritical(ex)
      End Try
      ' validate codeWarehouse
    ElseIf m_currentItem = FROM_WAREHOUSE_CODE_TEXT Or m_currentItem = TO_WAREHOUSE_CODE_TEXT Then
      Try
        Dim curWarehouseCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_WarehouseLength)
        If Not String.IsNullOrEmpty(curWarehouseCode) AndAlso m_currentString = curWarehouseCode Then
          CodeText = curWarehouseCode
          'No changes
          Return
        End If
        If String.IsNullOrEmpty(curWarehouseCode) Then
          Return
        End If
        'validate supplier code
        Dim beMasterEms As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(curWarehouseCode)
        If Not beMasterEms.SokoCode.Length > 0 Then
          'If there's error
          'e.Cancel = True
          'CodeText = m_currentString 'MasterDialog.ModifiedEntity.Header.TorihikisakiCode
          Return
        Else
          CodeText = curWarehouseCode
        End If

      Catch ex As Exception
        DisplayBox.ShowCritical(ex)
      End Try
    End If
  End Sub
  '***********************************************************																														
  'Name          : DisplayReferScreen
  'Content       : Display refer screen when the user selects from CodeSet field
  'Return Value  : 
  'Argument      : 
  'Created date  : 2016/03/10  DuyTN
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub DisplayReferScreen(ByVal e As System.ComponentModel.CancelEventArgs, ByRef Item As Integer)
    'DisplayReferScreen adjustmentManagerCodes
    If m_currentItem = FROM_ADJUSTMENT_CODE_TEXT Or m_currentItem = TO_ADJUSTMENT_CODE_TEXT Then
      Dim EMS As New EMS(connector)
      Dim newCode As String = String.Empty
      Dim location As Point = Tools.ControlTool.GetDialogLocation(adjustmentManagerCodes.FromReferButton)
      newCode = MasterDialog.ShowReferTekiyoDialog(If(Item = FROM_ADJUSTMENT_CODE_TEXT, adjustmentManagerCodes.FromCodeText, adjustmentManagerCodes.ToCodeText), location)
      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
        If m_currentString = newCode Then
          'No changes
          Return
        End If
        Dim strName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, newCode)
        If strName.Trim.Length > 0 Then
          If Item = FROM_ADJUSTMENT_CODE_TEXT Then
            adjustmentManagerCodes.FromCodeText = newCode
          Else
            adjustmentManagerCodes.ToCodeText = newCode
          End If
        Else
          If Not e Is Nothing Then e.Cancel = True
        End If
      End If
      m_enter = 1

      'DisplayReferScreen productCode
    ElseIf m_currentItem = FROM_PRODUCT_CODE_TEXT Or m_currentItem = TO_PRODUCT_CODE_TEXT Then
      Dim newCode As String = String.Empty
      Dim location As Point = Tools.ControlTool.GetDialogLocation(productCode.FromReferButton)
      newCode = MasterDialog.ShowReferSmsDialog(If(Item = FROM_PRODUCT_CODE_TEXT, productCode.FromCodeText, productCode.ToCodeText), location)
      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
        If m_currentString = newCode Then
          'No changes
          Return
        End If
        Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms = MasterDialog.FindBEMasterSms(newCode)
        If Not beMasterSms.SyohinCode.Length > 0 Then
          'Validate failed
          If Not e Is Nothing Then e.Cancel = True
          If Item = FROM_PRODUCT_CODE_TEXT Then
            productCode.FromCodeText = m_currentString
          Else
            productCode.ToCodeText = m_currentString
          End If
          Return
        End If
        If Item = FROM_PRODUCT_CODE_TEXT Then
          productCode.FromCodeText = newCode
        Else
          productCode.ToCodeText = newCode
        End If
      End If
      'DisplayReferScreen codeWarehouse
    ElseIf m_currentItem = FROM_WAREHOUSE_CODE_TEXT Or m_currentItem = TO_WAREHOUSE_CODE_TEXT Then
      Dim newCode As String = String.Empty
      Dim location As Point = Tools.ControlTool.GetDialogLocation(codeWarehouse.FromReferButton)
      newCode = MasterDialog.ShowReferSokoDialog(If(Item = FROM_WAREHOUSE_CODE_TEXT, codeWarehouse.FromCodeText, codeWarehouse.ToCodeText), location)
      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
        If m_currentString = newCode Then
          'No changes
          Return
        End If
        Dim beMasterEms As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(newCode)
        If Not beMasterEms.SokoCode.Length > 0 Then
          'Validate failed
          If Not e Is Nothing Then e.Cancel = True
          If Item = FROM_WAREHOUSE_CODE_TEXT Then
            codeWarehouse.FromCodeText = m_currentString
          Else
            codeWarehouse.ToCodeText = m_currentString
          End If
          Return
        End If
        If Item = FROM_WAREHOUSE_CODE_TEXT Then
          codeWarehouse.FromCodeText = newCode
        Else
          codeWarehouse.ToCodeText = newCode
        End If
      End If
    End If
  End Sub
#End Region
#Region "PCA Command Manager"
  Private Sub PcaCommandManager1_Command(ByVal sender As System.Object, ByVal e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Dim commandItem As PcaCommandItem = e.CommandItem
    Select Case True
      'Case commandItem Is PcaCommandItemHelp 'F1
      'do nothing
      Case commandItem Is PcaCommandItemSearch 'F5
        DoSearch()
      Case commandItem Is PcaCommandItemRefer 'F8
        '  DisplayReferScreen
        If m_isKeyDowned Then m_isKeyDowned = False : Exit Sub
        DisplayReferScreen(Nothing, m_currentItem)
      Case commandItem Is PcaCommandItemPrint 'F9
        'print data
        If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.productCode, 0)) Then
          If MsgBox(RPT_TITLE & "を" & SLConstants.ConfirmMessage.PRINT_CONFIRM_MESSAGE, MsgBoxStyle.OkCancel, SLConstants.NotifyMessage.TITLE_MESSAGE) = MsgBoxResult.Ok Then
            PrintData()
          End If
        End If

      Case commandItem Is PcaCommandItemPreview 'Shift + F9
        'preview
        PrintData(True)
      Case commandItem Is PcaCommandItemExport 'F10
        'do export
        If OutPutCSV() Then
          SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemExport.CommandId)
        End If
      Case commandItem Is PcaCommandItemClose 'F12
        Me.Close()
    End Select
  End Sub
#End Region
#Region "Print/Preview"
  ''' <summary>
  ''' print data 
  ''' </summary>
  ''' <param name="isPreview">is print or is priview</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function PrintData(Optional ByVal isPreview As Boolean = False) As Boolean
    Try
      Dim ReportName As String = RPT_FILENAME
      Dim ReportTitle As String = RPT_TITLE
      Dim rpt As New Sunloft.Windows.Forms.SLCrystalReport
      Dim RpParam As New Dictionary(Of String, Object)
      Dim editedDataTable As DataTable = Nothing


      rpt.ReportFile = IO.Path.GetDirectoryName(Application.ExecutablePath) & "\" & ReportName
      EditPrintData(editedDataTable)
      rpt.DataSource = editedDataTable
      ' 出力開始
      If isPreview Then
        rpt.Preview(ReportTitle, True)
      Else
        rpt.PrintToPrinter()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

    Return True
  End Function
  ''' <summary>
  ''' prepare data for print and priview
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EditPrintData(ByRef argEditedData As DataTable) As Boolean
    Dim editData As New DataSet1.DataTable1DataTable
    Dim editRow As DataRow
    Dim intLpc As Integer = 0

    Try
      While intLpc < dgv.Rows.Count AndAlso Not dgv(m_columnName.productCode, intLpc) Is Nothing 'SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.productCode, intLpc))
        With dgv
          editRow = editData.NewRow
          editRow.Item("Condition1") = "入出庫区分: " & m_lShippingType
          editRow.Item("Condition2") = "調整日: " & f_adjustmentDate.ToString & " ～ " & t_adjustmentDate.ToString

          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.type, intLpc)) Then
            editRow.Item("Type") = dgv(m_columnName.type, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.adjustmentDate, intLpc)) Then
            editRow.Item("AdjustmentDate") = dgv(m_columnName.adjustmentDate, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.slipNo, intLpc)) Then
            editRow.Item("SlipNo") = dgv(m_columnName.slipNo, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.slipNo2, intLpc)) Then
            editRow.Item("SlipNo2") = dgv(m_columnName.slipNo2, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.productCode, intLpc)) Then
            editRow.Item("ProductCore") = dgv(m_columnName.productCode, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.unit, intLpc)) Then
            editRow.Item("ProductName") = dgv(m_columnName.productName, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.warehouseCode, intLpc)) Then
            editRow.Item("WarehouseCode") = dgv(m_columnName.warehouseCode, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.warehouseName, intLpc)) Then
            editRow.Item("WarehouseName") = dgv(m_columnName.warehouseName, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.number, intLpc)) Then
            editRow.Item("Number") = dgv(m_columnName.number, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.quantity, intLpc)) Then
            editRow.Item("Quantity") = dgv(m_columnName.quantity, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.lotNo, intLpc)) Then
            editRow.Item("LotNo") = dgv(m_columnName.lotNo, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.unit, intLpc)) Then
            editRow.Item("Unit") = dgv(m_columnName.unit, intLpc).Value
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(m_columnName.adjustmentReasonName, intLpc)) Then
            editRow.Item("AdjustmentReasonName") = dgv(m_columnName.adjustmentReasonName, intLpc).Value
          End If
          editRow.Item("product") = dgv(m_columnName.productCode, intLpc).Value.ToString & " " & dgv(m_columnName.productName, intLpc).Value.ToString
          editRow.Item("warehouse") = dgv(m_columnName.warehouseCode, intLpc).Value.ToString & " " & dgv(m_columnName.warehouseName, intLpc).Value.ToString
          editData.Rows.Add(editRow)
        End With
        intLpc = intLpc + 1
      End While

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
    argEditedData = editData
    Return True
  End Function
  ''' <summary>
  ''' export file CSV
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function OutPutCSV() As Boolean
    Dim strSaveFileName As String = CSV_FILE_NAME
    Dim strColumns() As String
    Dim sbCsv As New StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .FileName = CSV_FILE_NAME
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        .OverwritePrompt = True

        Dim res As System.Windows.Forms.DialogResult = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False

        strSaveFileName = .FileName

      End With

      With m_columnName

        strColumns = {.type, .adjustmentDate, .slipNo, .productCode, .productName, .warehouseCode, .warehouseName, .number, .quantity, .unit, .lotNo, .adjustmentReasonCode, .adjustmentReasonName, .shipmentDeadline, .useByDate, .bestBeforeDate, .expirationDate} '.Lotdetails,.inventoryAdjustmentID
        For intCountLotDetails As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
          ReDim Preserve strColumns(strColumns.Length)
          strColumns(strColumns.Length - 1) = arrLotDetails(intCountLotDetails)
        Next
      End With

      SLCmnFunction.OutPutCSVFromDataGridView(strColumns, dgv, strSaveFileName, dgv.RowCount - 1)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function

  '***********************************************************																														
  'Name          : dgv_CellDoubleClick
  'Content       : Display lot detail when the user double clicks on a row in the table
  'Return Value  : 
  'Argument      : sender As System.Object, args As System.Windows.Forms.DataGridViewCellEventArgs
  'Created date  : 2016/03/15  by Duytn
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub dgv_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellDoubleClick
    If e.RowIndex = -1 OrElse dgv.Rows(e.RowIndex).Cells(INVENTORY_ADJUSTMENT_ID_COLUMN).Value Is Nothing Then
      Return
    End If
    SLCmnFunction.StartUpInputExe(IO.Path.GetDirectoryName(Application.ExecutablePath) & PROGRAM_TO_CALL, m_loginString, dgv.Rows(e.RowIndex).Cells(INVENTORY_ADJUSTMENT_ID_COLUMN).Value.ToString)
  End Sub
#End Region

    Private Sub frmLotInput_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If Not m_appClass.isAttach Then
            connector.LogOffSystem()
        End If
    End Sub
    Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
        '
        Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
        Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子
        If (keyCode = Keys.Tab OrElse keyCode = Keys.Enter) Then
            'If (keyCode = Keys.Enter) Then
            'Validate if user press Enter/Tab +  Anykey except Shift, →, ↓
            m_DoSearch = True
        Else
            m_DoSearch = False
        End If

        Return MyBase.ProcessDialogKey(keyData)

    End Function
    Private Sub frmTransLotList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F8 Then m_isKeyDowned = True
    End Sub
#Region "Paint ColumnHeaders"
    ''' <summary>
    ''' join two  header 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub dgv_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles dgv.Paint
        Dim monthes1 As String() = {m_columnName.productCode, m_columnName.warehouseCode}
        Dim monthes2 As String() = {m_columnName.adjustmentReasonCode}
        Dim j As Integer = PRODUCT_CODE_COLUMN 'Merge 2-3 and 4-5
        Dim i As Integer = ADJUSTMENT_REASON_CODE_COLUMN 'Merge 12-13
        While j <= WAREHOUSE_NAME_COLUMN
            Dim r1 As Rectangle = dgv.GetCellDisplayRectangle(j, -1, True)
            Dim w2 As Integer = dgv.GetCellDisplayRectangle(j + 1, -1, True).Width
            r1.X += 1
            r1.Y += 1
            r1.Width = r1.Width + w2 - 2
            r1.Height = CInt(r1.Height - 1)
            e.Graphics.FillRectangle(New SolidBrush(dgv.ColumnHeadersDefaultCellStyle.BackColor), r1)
            Dim format As New StringFormat()
            format.Alignment = StringAlignment.Near
            format.LineAlignment = StringAlignment.Center
            e.Graphics.DrawString(" " & monthes1(j \ 2 - 2), dgv.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Color.White), r1, format)
            j += 2
        End While
        While i <= ADJUSTMENT_REASON_NAME_COLUMN
            Dim r1 As Rectangle = dgv.GetCellDisplayRectangle(i, -1, True)
            Dim w2 As Integer = dgv.GetCellDisplayRectangle(i + 1, -1, True).Width
            r1.X += 1
            r1.Y += 1
            r1.Width = r1.Width + w2 - 2
            r1.Height = CInt(r1.Height - 1)
            e.Graphics.FillRectangle(New SolidBrush(dgv.ColumnHeadersDefaultCellStyle.BackColor), r1)
            Dim format As New StringFormat()
            format.Alignment = StringAlignment.Near
            format.LineAlignment = StringAlignment.Center
            e.Graphics.DrawString(" " & monthes2(i \ 2 - 6), dgv.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Color.White), r1, format)
            i += 2
        End While
    End Sub

  Private Sub dgv_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgv.Scroll
    Dim rtHeader As Rectangle = dgv.DisplayRectangle
    rtHeader.Height = CInt(dgv.ColumnHeadersHeight)
    dgv.Invalidate(rtHeader)
  End Sub
#End Region
End Class

'***********************************************************																														
'Name          : columnName
'Content       : This class defines header properties for the table that will be used do display result search
'Return Value  : 
'Created date  : 2016/03/09  Duytn
'Modified date :        by       Content: 																											
'***********************************************************
Public Class columnName
    Public Property type As String = "区分"
    Public Property adjustmentDate As String = "調整日"
    Public Property slipNo As String = "伝票番号"
    Public Property slipNo2 As String = "伝票番号2"
    Public Property productCode As String = "商品"
    Public Property productName As String = "商品(名称)"
    Public Property warehouseCode As String = "倉庫"
    Public Property warehouseName As String = "倉庫(名称)"
    Public Property number As String = "本数"
    Public Property quantity As String = "数量"
    Public Property unit As String = "単位"
    Public Property lotNo As String = "ロットNo"
    Public Property adjustmentReasonCode As String = "調整理由"
    Public Property adjustmentReasonName As String = "調整理由(名称)"
    Public Property shipmentDeadline As String = "出荷期限"
    Public Property useByDate As String = "使用期限"
    Public Property bestBeforeDate As String = "賞味期限"
    Public Property expirationDate As String = "消費期限"
    Public Property lotNameDetails As String = "ロット詳細名1～10"
    Public Property inventoryAdjustmentID As String = "在庫調整ID(非表示)"

    Public Property Lotdetails As String = "ロット詳細情報"
    Public Property Lotdetails1 As String = "ロット詳細情報1"
    Public Property Lotdetails2 As String = "ロット詳細情報2"
    Public Property Lotdetails3 As String = "ロット詳細情報3"
    Public Property Lotdetails4 As String = "ロット詳細情報4"
    Public Property Lotdetails5 As String = "ロット詳細情報5"
    Public Property Lotdetails6 As String = "ロット詳細情報6"
    Public Property Lotdetails7 As String = "ロット詳細情報7"
    Public Property Lotdetails8 As String = "ロット詳細情報8"
    Public Property Lotdetails9 As String = "ロット詳細情報9"
    Public Property Lotdetails10 As String = "ロット詳細情報10"
End Class
Public Class columnResult
    Public Property type As String = ""
    Public Property adjustmentDate As String = ""
    Public Property slipNo As String = ""
    Public Property slipNo2 As String = ""
    Public Property productCode As String = ""
    Public Property productName As String = ""
    Public Property warehouseCode As String = ""
    Public Property warehouseName As String = ""
    Public Property number As Integer
    Public Property quantity As Decimal
    Public Property unit As String = ""
    Public Property lotNo As String = ""
    Public Property adjustmentReasonCode As String = ""
    Public Property adjustmentReasonName As String = ""
    Public Property shipmentDeadline As String = ""
    Public Property useByDate As String = ""
    Public Property bestBeforeDate As String = ""
    Public Property expirationDate As String = ""
    Public Property Lotdetails() As String() = New String(10) {}
    Public Property inventoryAdjustmentID As String = ""
    Public Property quantityNoOfDigit As Integer
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCheckOrder))
    Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.FToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPreview = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemExport = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemReference = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPrint = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPreview = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonExport = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSearch = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.dgv = New System.Windows.Forms.DataGridView()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrint = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPreview = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandExport = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.adjustmentManagerCodes = New PCA.Controls.PcaRangeCodeSet()
    Me.codeWarehouse = New PCA.Controls.PcaRangeCodeSet()
    Me.productCode = New PCA.Controls.PcaRangeCodeSet()
    Me.adjustmentDate = New PCA.Controls.PcaRangeDate()
    Me.shippingType = New PCA.Controls.PcaLabeledComboBox()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemPrint = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPreview = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemExport = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(1248, 24)
    Me.MenuStrip1.TabIndex = 2
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'FToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.FToolStripMenuItemFile, Nothing)
    Me.FToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemPrint, Me.ToolStripMenuItemPreview, Me.ToolStripMenuItemExport, Me.ToolStripMenuItemClose})
    Me.FToolStripMenuItemFile.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.FToolStripMenuItemFile.Name = "FToolStripMenuItemFile"
    Me.FToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.FToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPrint, Me.PcaCommandItemPrint)
    Me.ToolStripMenuItemPrint.Name = "ToolStripMenuItemPrint"
    Me.ToolStripMenuItemPrint.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPrint.Text = "印刷(&P)"
    '
    'ToolStripMenuItemPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPreview, Me.PcaCommandItemPreview)
    Me.ToolStripMenuItemPreview.Name = "ToolStripMenuItemPreview"
    Me.ToolStripMenuItemPreview.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPreview.Text = "印刷プレビュー(&V)"
    '
    'ToolStripMenuItemExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemExport, Me.PcaCommandItemExport)
    Me.ToolStripMenuItemExport.Name = "ToolStripMenuItemExport"
    Me.ToolStripMenuItemExport.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemExport.Text = "出力(&O)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSearch, Me.ToolStripMenuItemReference})
    Me.ToolStripMenuItemEdit.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSearch.Text = "再表示(&R)"
    '
    'ToolStripMenuItemReference
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemReference, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemReference.Name = "ToolStripMenuItemReference"
    Me.ToolStripMenuItemReference.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemReference.Text = "参照(&U)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Nothing)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContent.Text = "目次(&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonPrint, Me.ToolStripButtonPreview, Me.ToolStripButtonExport, Me.ToolStripButtonSearch, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1248, 37)
    Me.ToolStrip1.TabIndex = 3
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPrint, Me.PcaCommandItemPrint)
    Me.ToolStripButtonPrint.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.ToolStripButtonPrint.Image = CType(resources.GetObject("ToolStripButtonPrint.Image"), System.Drawing.Image)
    Me.ToolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPrint.Name = "ToolStripButtonPrint"
    Me.ToolStripButtonPrint.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonPrint.Text = "印刷"
    Me.ToolStripButtonPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPreview, Me.PcaCommandItemPreview)
    Me.ToolStripButtonPreview.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.ToolStripButtonPreview.Image = CType(resources.GetObject("ToolStripButtonPreview.Image"), System.Drawing.Image)
    Me.ToolStripButtonPreview.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPreview.Name = "ToolStripButtonPreview"
    Me.ToolStripButtonPreview.Size = New System.Drawing.Size(81, 34)
    Me.ToolStripButtonPreview.Text = "プレビュー"
    Me.ToolStripButtonPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonExport, Me.PcaCommandItemExport)
    Me.ToolStripButtonExport.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.ToolStripButtonExport.Image = CType(resources.GetObject("ToolStripButtonExport.Image"), System.Drawing.Image)
    Me.ToolStripButtonExport.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonExport.Name = "ToolStripButtonExport"
    Me.ToolStripButtonExport.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonExport.Text = "出力"
    Me.ToolStripButtonExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSearch, Me.PcaCommandItemSearch)
    Me.ToolStripButtonSearch.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.ToolStripButtonSearch.Image = CType(resources.GetObject("ToolStripButtonSearch.Image"), System.Drawing.Image)
    Me.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSearch.Name = "ToolStripButtonSearch"
    Me.ToolStripButtonSearch.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonSearch.Text = "再表示"
    Me.ToolStripButtonSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"), System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'dgv
    '
    Me.dgv.AllowUserToAddRows = False
    Me.dgv.AllowUserToDeleteRows = False
    Me.dgv.AllowUserToResizeRows = False
    Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dgv.BackgroundColor = System.Drawing.SystemColors.Control
    Me.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
    DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
    DataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue
    DataGridViewCellStyle1.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Window
    DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.WindowText
    DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
    DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
    Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
    Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgv.EnableHeadersVisualStyles = False
    Me.dgv.Location = New System.Drawing.Point(10, 154)
    Me.dgv.Name = "dgv"
    Me.dgv.ReadOnly = True
    Me.dgv.RowHeadersVisible = False
    Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
    Me.dgv.Size = New System.Drawing.Size(1226, 291)
    Me.dgv.TabIndex = 115
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandSearch, Me.PcaFunctionCommandPrint, Me.PcaFunctionCommandPreview, Me.PcaFunctionCommandExport, Me.PcaFunctionCommandClose})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("MS Gothic", 10.0!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 451)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1248, 28)
    Me.PcaFunctionBar1.TabIndex = 114
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    '
    'PcaFunctionCommandSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSearch, Me.PcaCommandItemSearch)
    Me.PcaFunctionCommandSearch.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandSearch.Tag = Nothing
    Me.PcaFunctionCommandSearch.Text = "再表示"
    '
    'PcaFunctionCommandPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrint, Me.PcaCommandItemPrint)
    Me.PcaFunctionCommandPrint.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPrint.Tag = Nothing
    Me.PcaFunctionCommandPrint.Text = "印刷"
    '
    'PcaFunctionCommandPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPreview, Me.PcaCommandItemPreview)
    Me.PcaFunctionCommandPreview.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPreview.ModifierKeys = PCA.Controls.FunctionModifierKeys.Shift
    Me.PcaFunctionCommandPreview.Tag = Nothing
    Me.PcaFunctionCommandPreview.Text = "プレビュー"
    '
    'PcaFunctionCommandExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandExport, Me.PcaCommandItemExport)
    Me.PcaFunctionCommandExport.FunctionKey = PCA.Controls.FunctionKey.F10
    Me.PcaFunctionCommandExport.Tag = Nothing
    Me.PcaFunctionCommandExport.Text = "出力"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'adjustmentManagerCodes
    '
    Me.adjustmentManagerCodes.AutoTopMargin = False
    Me.adjustmentManagerCodes.ClientProcess = Nothing
    Me.adjustmentManagerCodes.LimitLength = 6
    Me.adjustmentManagerCodes.Location = New System.Drawing.Point(842, 120)
    Me.adjustmentManagerCodes.MaxCodeLength = 14
    Me.adjustmentManagerCodes.MaxLabelLength = 15
    Me.adjustmentManagerCodes.MaxNameLength = 0
    Me.adjustmentManagerCodes.Name = "adjustmentManagerCodes"
    Me.adjustmentManagerCodes.RangeGroupBoxText = "調整理由コード"
    Me.adjustmentManagerCodes.RangeGroupBoxVisible = False
    Me.adjustmentManagerCodes.Size = New System.Drawing.Size(378, 22)
    Me.adjustmentManagerCodes.TabIndex = 5
    '
    'codeWarehouse
    '
    Me.codeWarehouse.AutoTopMargin = False
    Me.codeWarehouse.ClientProcess = Nothing
    Me.codeWarehouse.LimitLength = 6
    Me.codeWarehouse.Location = New System.Drawing.Point(399, 120)
    Me.codeWarehouse.MaxCodeLength = 14
    Me.codeWarehouse.MaxLabelLength = 15
    Me.codeWarehouse.MaxNameLength = 0
    Me.codeWarehouse.Name = "codeWarehouse"
    Me.codeWarehouse.RangeGroupBoxText = "倉庫コード"
    Me.codeWarehouse.RangeGroupBoxVisible = False
    Me.codeWarehouse.Size = New System.Drawing.Size(378, 22)
    Me.codeWarehouse.TabIndex = 4
    '
    'productCode
    '
    Me.productCode.AutoTopMargin = False
    Me.productCode.ClientProcess = Nothing
    Me.productCode.Location = New System.Drawing.Point(10, 120)
    Me.productCode.MaxCodeLength = 11
    Me.productCode.MaxLabelLength = 15
    Me.productCode.MaxNameLength = 0
    Me.productCode.Name = "productCode"
    Me.productCode.RangeGroupBoxText = "商品コード"
    Me.productCode.RangeGroupBoxVisible = False
    Me.productCode.Size = New System.Drawing.Size(336, 22)
    Me.productCode.TabIndex = 3
    '
    'adjustmentDate
    '
    Me.adjustmentDate.AutoTopMargin = False
    Me.adjustmentDate.ClientProcess = Nothing
    Me.adjustmentDate.DayLabel = ""
    Me.adjustmentDate.DisplaySlash = True
    Me.adjustmentDate.Holidays = Nothing
    Me.adjustmentDate.Location = New System.Drawing.Point(10, 95)
    Me.adjustmentDate.MaxLabelLength = 15
    Me.adjustmentDate.MaxTextLength = 14
    Me.adjustmentDate.MonthLabel = " /"
    Me.adjustmentDate.Name = "adjustmentDate"
    Me.adjustmentDate.RangeGroupBoxText = "調整日付"
    Me.adjustmentDate.RangeGroupBoxVisible = False
    Me.adjustmentDate.Size = New System.Drawing.Size(336, 22)
    Me.adjustmentDate.TabIndex = 2
    Me.adjustmentDate.YearLabel = " /"
    '
    'shippingType
    '
    Me.shippingType.AllowDrop = True
    Me.shippingType.ClientProcess = Nothing
    Me.shippingType.DropDownWidth = 175
    Me.shippingType.Items.AddRange(New Object() {"すべて表示", "出庫調整", "入庫調整", "棚卸調整"})
    Me.shippingType.LabelText = "入出庫区分"
    Me.shippingType.Location = New System.Drawing.Point(10, 70)
    Me.shippingType.MaxLabelLength = 15
    Me.shippingType.MaxTextLength = 25
    Me.shippingType.Name = "shippingType"
    Me.shippingType.SelectedIndex = 0
    Me.shippingType.SelectedItem = "すべて表示"
    Me.shippingType.SelectedText = ""
    Me.shippingType.SelectedValue = Nothing
    Me.shippingType.Size = New System.Drawing.Size(280, 22)
    Me.shippingType.TabIndex = 1
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemSearch, Me.PcaCommandItemRefer, Me.PcaCommandItemPrint, Me.PcaCommandItemPreview, Me.PcaCommandItemExport, Me.PcaCommandItemClose})
    '
    'PcaCommandItemPrint
    '
    Me.PcaCommandItemPrint.CommandId = 9
    '
    'PcaCommandItemPreview
    '
    Me.PcaCommandItemPreview.CommandId = 9
    '
    'PcaCommandItemExport
    '
    Me.PcaCommandItemExport.CommandId = 10
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    '
    'PcaCommandItemSearch
    '
    Me.PcaCommandItemSearch.CommandId = 5
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandId = 8
    '
    'frmCheckOrder
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1248, 479)
    Me.Controls.Add(Me.dgv)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.adjustmentManagerCodes)
    Me.Controls.Add(Me.codeWarehouse)
    Me.Controls.Add(Me.productCode)
    Me.Controls.Add(Me.adjustmentDate)
    Me.Controls.Add(Me.shippingType)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.KeyPreview = True
    Me.MinimumSize = New System.Drawing.Size(1264, 517)
    Me.Name = "frmCheckOrder"
    Me.Text = "在庫調整ロット一覧表"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents FToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPrint As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPreview As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemExport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemReference As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPrint As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPreview As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonExport As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonSearch As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents shippingType As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents adjustmentDate As PCA.Controls.PcaRangeDate
  Friend WithEvents productCode As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents codeWarehouse As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents adjustmentManagerCodes As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPrint As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPreview As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemExport As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSearch As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandPrint As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandPreview As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandExport As PCA.Controls.PcaFunctionCommand
    Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog

End Class

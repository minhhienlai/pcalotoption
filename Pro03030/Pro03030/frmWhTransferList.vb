﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports PCA.TSC.Kon.Tools
Imports Sunloft.PCAIF
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports PCA.TSC.Kon
Imports System.Globalization

Public Class frmList

  Private connector As IIntegratedApplication = Nothing
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private m_SL_LMBClass As SL_LMB
  Private m_AMS1Class As AMS1
  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private m_SMSCLass As SMS
  Private m_SMSPClass As SMSP

  Private arrLotDetails(10) As String

  Private Const WIDTH_IS_CONVERT As Integer = 50
  Private Const WIDTH_WAREHOUSE_DATE As Integer = 100
  Private Const WIDTH_PURCHASE_DATE As Integer = 100
  Private Const WIDTH_SUP_CODE As Integer = 70
  Private Const WIDTH_SUP_NAME As Integer = 200
  Private Const WIDTH_PRODUCT_CODE As Integer = 70
  Private Const WIDTH_PRODUCT_NAME As Integer = 200

  Private Const WIDTH_QUANTITY As Integer = 80
  Private Const WIDTH_UNIT As Integer = 50
  Private Const WIDTH_UNIT_PRICE As Integer = 80
  Private Const WIDTH_LOT_NO As Integer = 150
  Private Const WIDTH_LOT_DETAILS As Integer = 100
  Private Const WIDTH_SLIP_NO As Integer = 150

  Private Const HEIGHT_HEADER As Integer = 25

  Private Const TEXT_TYPE As String = "TEXT_TYPE"
  Private Const CHECKBOX_TYPE As String = "CHECKBOX_TYPE"
  Private Const CALENDAR_TYPE As String = "CALENDAR_TYPE"

  Private Const MAX_UNIT_PRICE_LENGTH As Integer = 15

  Private Results As ColumnResult() = {New ColumnResult}
  Private duplicatedCell(,) As Boolean 'If a cell content is the same as the one above (not display)
  Private m_noOfChecked As Integer = 0
  Private m_noOfCheckBox As Integer = 0

  Private m_Controller As InputNYKController()
  Private m_SlipNoColumn As Integer
  Private m_isDataSet As Boolean = False

  Private m_CurRowIndex As Integer = -1
  Private m_CurColumnIndex As Integer = -1
  Private m_CurCellValue As String = String.Empty

  Private m_isClosing As Boolean = False

  Private Enum ColumnIndex
    ConvertFlag
    WHDate
    PurDate
    SupCode
    SupName
    ProductCode
    ProductName
    Qty
    UnitName
    UnitPrice
    LotNo
    ToTal
  End Enum

#Region "Initialize"

  Public Sub New()
    Try
      InitializeComponent()

      m_appClass.ConnectToPCA()
      connector = m_appClass.connector

      m_SL_LMBClass = New SL_LMB(connector) : m_SL_LMBClass.ReadOnlyRow()
      m_AMS1Class = New AMS1(connector) : m_AMS1Class.ReadAll()
      m_SMSCLass = New SMS(connector)
      m_SMSPClass = New SMSP(connector)

      arrLotDetails = {m_SL_LMBClass.sl_lmb_label1, m_SL_LMBClass.sl_lmb_label2, m_SL_LMBClass.sl_lmb_label3, m_SL_LMBClass.sl_lmb_label4, m_SL_LMBClass.sl_lmb_label5 _
                      , m_SL_LMBClass.sl_lmb_label6, m_SL_LMBClass.sl_lmb_label7, m_SL_LMBClass.sl_lmb_label8, m_SL_LMBClass.sl_lmb_label9, m_SL_LMBClass.sl_lmb_label10}

      m_SlipNoColumn = m_SL_LMBClass.sl_lmb_maxlotop + ColumnIndex.ToTal
      ldtWhDate.Date = Today

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)

    InitTable(True)

    MyBase.OnLoad(e)

    Search()
  End Sub

  Private Sub InitTable(Optional isEmpty As Boolean = False)
    Try
      dgv.Columns.Clear()
      If isEmpty Then
        InitTableColumn(TEXT_TYPE, ColumnName.isConvert, WIDTH_IS_CONVERT, DataGridViewContentAlignment.MiddleCenter, , isEmpty) 'Do not display checkbox in Empty table
      Else
        InitTableColumn(CHECKBOX_TYPE, ColumnName.isConvert, WIDTH_IS_CONVERT, DataGridViewContentAlignment.MiddleCenter, , isEmpty)
      End If

      InitTableColumn(TEXT_TYPE, ColumnName.WhDate, WIDTH_WAREHOUSE_DATE, DataGridViewContentAlignment.MiddleCenter)
      InitTableColumn(CALENDAR_TYPE, ColumnName.PurDate, WIDTH_PURCHASE_DATE, DataGridViewContentAlignment.MiddleCenter, , False)
      InitTableColumn(TEXT_TYPE, ColumnName.SupCode, WIDTH_SUP_CODE, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.SupName, WIDTH_SUP_NAME, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.ProductCode, WIDTH_PRODUCT_CODE, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.ProductName, WIDTH_PRODUCT_NAME, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.Quantity, WIDTH_QUANTITY, DataGridViewContentAlignment.MiddleRight)
      InitTableColumn(TEXT_TYPE, ColumnName.UnitName, WIDTH_UNIT, DataGridViewContentAlignment.MiddleLeft)
      InitTableColumn(TEXT_TYPE, ColumnName.UnitPrice, WIDTH_UNIT_PRICE, DataGridViewContentAlignment.MiddleRight, , isEmpty)
      If m_SL_LMBClass.sl_lmb_autolot = SLConstants.SetLotType.Manual Then
        InitTableColumn(TEXT_TYPE, ColumnName.LotNo, WIDTH_LOT_NO, DataGridViewContentAlignment.MiddleLeft, , isEmpty)
      Else
        InitTableColumn(TEXT_TYPE, ColumnName.LotNo, WIDTH_LOT_NO, DataGridViewContentAlignment.MiddleLeft)
      End If

      For intCount As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        InitTableColumn(TEXT_TYPE, arrLotDetails(intCount), WIDTH_LOT_DETAILS, DataGridViewContentAlignment.MiddleLeft)
      Next

      InitTableColumn(TEXT_TYPE, ColumnName.SlipNo, WIDTH_SLIP_NO)
      dgv.Columns(ColumnName.SlipNo).Visible = False

      'Set editable column color
      SLCmnFunction.SetEditableColumnColor(dgv, ColumnIndex.PurDate, Nothing)
      SLCmnFunction.SetEditableColumnColor(dgv, ColumnIndex.UnitPrice, Nothing)
      If m_SL_LMBClass.sl_lmb_autolot = SLConstants.SetLotType.Manual Then
        SLCmnFunction.SetEditableColumnColor(dgv, ColumnIndex.LotNo, Nothing)
      End If
      'Set header cell font color to white
      For intCount As Integer = 0 To m_SlipNoColumn - 1
        dgv.Columns(intCount).HeaderCell.Style.ForeColor = Color.White
      Next
      'Set header size (Default size is a bit small)
      dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
      dgv.ColumnHeadersHeight = HEIGHT_HEADER

      If isEmpty Then
        Results = Nothing 'Clear search result array
        dgv.Rows.Clear()
      End If
      'Set comma for quantity and unit price column
      dgv.Columns(ColumnIndex.Qty).DefaultCellStyle.Format = "N4"
      dgv.Columns(ColumnIndex.UnitPrice).DefaultCellStyle.Format = "N4"

      'Set table not sortable
      For Each column As DataGridViewColumn In dgv.Columns
        column.SortMode = DataGridViewColumnSortMode.NotSortable
      Next

      PcaFunctionCommandExecute.Enabled = False
      PcaFunctionCommandSelectAll.Enabled = False
      PcaFunctionCommandUnselectAll.Enabled = False

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  Private Sub InitTableColumn(ByVal strHeaderType As String, ByVal strHeaderText As String, ByVal intColumnWidth As Integer, _
                              Optional alignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft, Optional columnname As String = "", _
                              Optional isReadonly As Boolean = True)
    Dim col As New DataGridViewColumn
    Select Case strHeaderType
      Case CALENDAR_TYPE
        col = New CalendarColumn()
      Case TEXT_TYPE
        col = New DataGridViewTextBoxColumn
      Case CHECKBOX_TYPE
        col = New DataGridViewCheckBoxColumn
      Case Else
        col = New DataGridViewTextBoxColumn
    End Select
    col.ReadOnly = isReadonly
    If columnname = String.Empty Then
      col.Name = strHeaderText
    Else
      col.Name = columnname
    End If
    col.Width = intColumnWidth
    col.DefaultCellStyle.Alignment = alignment
    dgv.Columns.Add(col)
  End Sub

#End Region

#Region "PCA Command Manager"

  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command

    Dim commandItem As PCA.Controls.PcaCommandItem = e.CommandItem
    Try
      Select Case True
        Case commandItem Is PcaCommandItemClose
          m_isClosing = True
          Me.Close()
          m_isClosing = False
        Case commandItem Is PcaCommandItemSearch
          PcaFunctionBar1.Focus()
          Search()
        Case commandItem Is PcaCommandItemExecute
          If Not ValidateConvert() Then Return

          PcaFunctionBar1.Focus() 'To validate all other controllers
          Dim result As Integer = MsgBox(SLConstants.ConfirmMessage.CONVERT_CONFIRM_MESSAGE, MsgBoxStyle.YesNo, SLConstants.NotifyMessage.TITLE_MESSAGE)
          If result = DialogResult.Yes Then
            If ExecuteConvert() Then
              SLCmnFunction.ShowToolTip(SLConstants.CommonMessage.TRANSFER_SUCCESS_MESSAGE, _
                                        SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemExecute.CommandId)
              InitTable(True)
            End If
          End If

        Case commandItem Is PcaCommandItemSelectAll
          ldtWhDate.Focus()
          For intCount As Integer = 0 To Results.Count - 1
            dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ConvertFlag).Value = True
          Next
          If Results.Length > 0 Then PcaFunctionCommandExecute.Enabled = True
          m_noOfChecked = m_noOfCheckBox
        Case commandItem Is PcaCommandItemUnselectAll
          ldtWhDate.Focus()
          For intCount As Integer = 0 To Results.Count - 1
            dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ConvertFlag).Value = False
          Next
          PcaFunctionCommandExecute.Enabled = False
          m_noOfChecked = 0
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub PcaCommandManager1_UpdateCommandUI(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.UpdateCommandUI
    Try
      PcaCommandItemExecute.Enabled = dgv.RowCount = 0
      PcaCommandItemSelectAll.Enabled = dgv.RowCount = 0
      PcaCommandItemUnselectAll.Enabled = dgv.RowCount = 0
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Exit Sub
    End Try
  End Sub

#End Region

#Region " Private Method"

#Region "  Search"

  Private Sub Search(Optional isSelected As Boolean = True)
    GetSearchResult(isSelected)
    If Results Is Nothing Then
      SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSearch.CommandId)
      dgv.Columns.Clear()
      InitTable(True)
    Else
      DisplaySearchResult()

    End If

    m_isDataSet = True
    If Not Results Is Nothing Then
      If isSelected Then
        PcaFunctionCommandExecute.Enabled = True
      Else
        PcaFunctionCommandExecute.Enabled = False
      End If

      PcaFunctionCommandSelectAll.Enabled = True
      PcaFunctionCommandUnselectAll.Enabled = True

    Else
      PcaFunctionCommandExecute.Enabled = False
      PcaFunctionCommandSelectAll.Enabled = False
      PcaFunctionCommandUnselectAll.Enabled = False

    End If
  End Sub

#End Region

#Region "  Convert"

  Private Function ExecuteConvert() As Boolean

    Try
      Dim SL_NYKHClass As SL_NYKH = New SL_NYKH(connector)
      Dim SL_NYKDArray As SL_NYKD() = {New SL_NYKD(connector)}
      Dim SL_NYKDArrayAllSlip As SL_NYKD() = Nothing
      Dim detailsCount As Integer = 0
      Dim headerCount As Integer = -1
      Dim issuccess As Boolean = True

      Dim dblPrice As Double = 0D

      Dim session As ICustomSession = connector.CreateTransactionalSession
      
      m_Controller = {New InputNYKController(Me)}

      For intCount As Integer = 0 To Results.Length - 1

        If Results(intCount) IsNot Nothing AndAlso _
         (intCount = 0 OrElse dgv.Rows.Item(intCount - 1).Cells.Item(m_SlipNoColumn).Value.ToString <> dgv.Rows.Item(intCount).Cells.Item(m_SlipNoColumn).Value.ToString) AndAlso _
         dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ConvertFlag).Value.ToString = "True" Then

          'For each different slip that is selected

          Dim slipNo As Integer = CInt(dgv.Rows.Item(intCount).Cells.Item(m_SlipNoColumn).Value)
          headerCount += 1
          'Get Header to class
          SL_NYKHClass.sl_nykh_denno = slipNo
          SL_NYKHClass.ReadByID(slipNo)

          'Get uribi from screen
          SL_NYKHClass.sl_nykh_uribi = CInt(CDate(dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.PurDate).Value).ToString("yyyyMMdd"))

          If headerCount > 0 Then ReDim Preserve m_Controller(m_Controller.Count)
          m_Controller(m_Controller.Count - 1) = New InputNYKController(Me)
          m_Controller(headerCount).CommonVariable.IApplication = connector
          Dim entity As PEKonInput = m_Controller(headerCount).CreateInitialEntity()
          m_Controller(headerCount).UpdateActiveEntity(entity)

          'Save Header to Business Entity
          m_Controller(headerCount).ModifiedEntity.Header.ShireKamoku = 0 '固定　(仕入）
          m_Controller(headerCount).ModifiedEntity.Header.Denku = 0 '固定　(掛買）
          m_Controller(headerCount).ModifiedEntity.Header.DenpyoHizuke = SL_NYKHClass.sl_nykh_uribi
          m_Controller(headerCount).ModifiedEntity.Header.Hizuke = SL_NYKHClass.sl_nykh_uribi
          m_Controller(headerCount).ModifiedEntity.Header.TorihikisakiCode = SL_NYKHClass.sl_nykh_tcd
          If SL_NYKHClass.sl_nykh_datakbn = 1 Then m_Controller(headerCount).ModifiedEntity.Header.RendoDenpyoNo = SL_NYKHClass.sl_nykh_dataid
          m_Controller(headerCount).ModifiedEntity.Header.DenpyoNo2 = SL_NYKHClass.sl_nykh_denno2.Trim

          m_Controller(headerCount).ModifiedEntity.Header.TantosyaCode = SL_NYKHClass.sl_nykh_jtan.Trim
          m_Controller(headerCount).ModifiedEntity.Header.BumonCode = SL_NYKHClass.sl_nykh_jbmn.Trim
          m_Controller(headerCount).ModifiedEntity.Header.TekiyoCode = SL_NYKHClass.sl_nykh_tekcd.Trim
          m_Controller(headerCount).ModifiedEntity.Header.Tekiyo = SL_NYKHClass.sl_nykh_tekmei.Trim
          m_Controller(headerCount).ModifiedEntity.Header.ProCode = SL_NYKHClass.sl_nykh_pjcode.Trim
          m_Controller(headerCount).ModifiedEntity.Header.YobiInt1 = SL_NYKHClass.sl_nykh_id

          'Get Details to class
          Dim SL_NYKDClass = New SL_NYKD(connector)
          SL_NYKDClass.sl_nykd_hid = SL_NYKHClass.sl_nykh_id
          SL_NYKDArray = SL_NYKDClass.ReadAllFromHeaderIDToConvert()
          SL_NYKDArray = DuplicateNYKD(SL_NYKDArray)

          For Each SL_NYKDItem In SL_NYKDArray
            If SL_NYKDArrayAllSlip Is Nothing Then
              SL_NYKDArrayAllSlip = {SL_NYKDItem}
            Else
              ReDim Preserve SL_NYKDArrayAllSlip(SL_NYKDArrayAllSlip.Count)
            End If

            SL_NYKDArrayAllSlip(SL_NYKDArrayAllSlip.Count - 1) = SL_NYKDItem
          Next

          For intCountDetailsUnitPrice As Integer = 0 To SL_NYKDArray.Count - 1
            If Not SL_NYKDArray(intCountDetailsUnitPrice) Is Nothing Then
              m_SMSCLass.ReadByID(SL_NYKDArray(intCountDetailsUnitPrice).sl_nykd_scd)
              SL_NYKDArray(intCountDetailsUnitPrice).sl_nykd_tanka = Math.Round(CDec(dgv.Rows.Item(intCount + intCountDetailsUnitPrice).Cells.Item(ColumnIndex.UnitPrice).Value), m_SMSCLass.sms_tketa)
              SL_NYKDArray(intCountDetailsUnitPrice).sl_nykd_kingaku = Math.Round(SL_NYKDArray(intCountDetailsUnitPrice).sl_nykd_tanka * SL_NYKDArray(intCountDetailsUnitPrice).sl_nykd_suryo)
              SL_NYKDArray(intCountDetailsUnitPrice).sl_nykd_ulotno = dgv.Rows.Item(intCount + intCountDetailsUnitPrice).Cells.Item(ColumnIndex.LotNo).Value.ToString
            End If
          Next

          'Summary Details that has same 入荷区分、商品コード、商品名、規格・型番、色、サイズ、倉庫、税区分、税込区分、入数、単位、単価
          'Using Data Table
          Dim Total_SL_NYKDArray As SL_NYKD() = SummaryDetails(SL_NYKDArray)

          'Save Details to Business Entity
          For intCountDetails As Integer = 0 To Total_SL_NYKDArray.Count - 1
            If Not Total_SL_NYKDArray(intCountDetails) Is Nothing Then
              m_SMSCLass.sms_scd = Total_SL_NYKDArray(intCountDetails).sl_nykd_scd.Trim
              m_SMSCLass.ReadByID(Total_SL_NYKDArray(intCountDetails).sl_nykd_scd.Trim)
              m_SMSPClass.ReadByID(Total_SL_NYKDArray(intCountDetails).sl_nykd_scd.Trim)

              With m_Controller(headerCount).ModifiedEntity
                .DetailList(intCountDetails).SyohinCode = Total_SL_NYKDArray(intCountDetails).sl_nykd_scd.Trim
                .DetailList(intCountDetails).MasterKubun = MasterKubunType.Ippan
                .DetailList(intCountDetails).ZeiKubun = CType(Total_SL_NYKDArray(intCountDetails).sl_nykd_tax, ZeiKubunType)
                .DetailList(intCountDetails).ZeikomiKubun = CType(Total_SL_NYKDArray(intCountDetails).sl_nykd_komi, ZeikomiKubnType)
                .DetailList(intCountDetails).TankaKeta = CShort(m_SMSCLass.sms_tketa)
                .DetailList(intCountDetails).SuryoKeta = CShort(m_SMSCLass.sms_sketa)
                .DetailList(intCountDetails).SyohinMei = Total_SL_NYKDArray(intCountDetails).sl_nykd_mei.Trim
                .DetailList(intCountDetails).KikakuKataban = Total_SL_NYKDArray(intCountDetails).sl_nykd_kikaku.Trim
                .DetailList(intCountDetails).Color = Total_SL_NYKDArray(intCountDetails).sl_nykd_color.Trim
                .DetailList(intCountDetails).Size = Total_SL_NYKDArray(intCountDetails).sl_nykd_size.Trim
                .DetailList(intCountDetails).SokoCode = Total_SL_NYKDArray(intCountDetails).sl_nykd_souko
                .DetailList(intCountDetails).Ku = CType(SL_NYKHClass.sl_nykh_return, KuType)
                .DetailList(intCountDetails).Irisu = Total_SL_NYKDArray(intCountDetails).sl_nykd_iri
                .DetailList(intCountDetails).Hakosu = Total_SL_NYKDArray(intCountDetails).sl_nykd_hako
                .DetailList(intCountDetails).Suryo = Math.Round(CDec(Total_SL_NYKDArray(intCountDetails).sl_nykd_suryo), m_SMSCLass.sms_sketa)
                .DetailList(intCountDetails).Tani = Total_SL_NYKDArray(intCountDetails).sl_nykd_tani

                .DetailList(intCountDetails).Tanka = CDec(Total_SL_NYKDArray(intCountDetails).sl_nykd_tanka)

                .DetailList(intCountDetails).Kingaku = CDec(Total_SL_NYKDArray(intCountDetails).sl_nykd_kingaku)
                .DetailList(intCountDetails).Biko = Total_SL_NYKDArray(intCountDetails).sl_nykd_biko
                .DetailList(intCountDetails).HyojunShireTanka = m_SMSPClass.smsp_sitan
                .DetailList(intCountDetails).ZeiRitsu = Total_SL_NYKDArray(intCountDetails).sl_nykd_rate

                dblPrice = Total_SL_NYKDArray(intCountDetails).sl_nykd_kingaku
                If .DetailList(intCountDetails).ZeikomiKubun = ZeikomiKubnType.Zeinuki Then
                  .DetailList(intCountDetails).SotoZeigaku = CDec(dblPrice * Total_SL_NYKDArray(intCountDetails).sl_nykd_rate / 100)
                Else
                  .DetailList(intCountDetails).UchiZeigaku = CDec(dblPrice / (100 + Total_SL_NYKDArray(intCountDetails).sl_nykd_rate) * Total_SL_NYKDArray(intCountDetails).sl_nykd_rate)
                End If

                .DetailList(intCountDetails).RendoShiji = Total_SL_NYKDArray(intCountDetails).sl_nykd_nkbn.ToString

                If SL_NYKHClass.sl_nykh_datakbn = 1 Then .DetailList(intCountDetails).RendoHeaderId = SL_NYKHClass.sl_nykh_dataid

                If issuccess Then issuccess = SL_NYKHClass.UpdateDetailsAfterConvert(session)
              End With
            End If
          Next
        End If
      Next

      For intcount As Integer = 0 To headerCount
        If Not ValidateFormItems(m_Controller(headerCount)) Then Return False
      Next

      For iLpc As Integer = 0 To headerCount
        If issuccess AndAlso Not Create(m_Controller(iLpc)) Then issuccess = False
      Next

      If issuccess Then
        session.Commit()
        session.Dispose()
      Else
        session.Rollback()
        session.Dispose()
      End If

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function DuplicateNYKD(ByVal originalNYKD As SL_NYKD()) As SL_NYKD()
    Try
      Dim duplidatedNYKD As SL_NYKD() = {New SL_NYKD(connector)}
      Dim intLpc As Integer = 0
      Dim intInsertRow As Integer = 0
      Dim decQty As Double = 0D
      Dim intCase As Integer = 0
      Dim intNum As Integer = 0

      ReDim duplidatedNYKD(0)
      For intLpc = 1 To originalNYKD.Length - 1
        If originalNYKD(intLpc - 1).sl_nykd_hid = originalNYKD(intLpc).sl_nykd_hid And originalNYKD(intLpc - 1).sl_nykd_tanka = originalNYKD(intLpc).sl_nykd_tanka And _
           originalNYKD(intLpc - 1).sl_nykd_mei = originalNYKD(intLpc).sl_nykd_mei And originalNYKD(intLpc - 1).sl_nykd_scd = originalNYKD(intLpc).sl_nykd_scd And _
           originalNYKD(intLpc - 1).sl_nykd_ulotno = originalNYKD(intLpc).sl_nykd_ulotno Then

          decQty += originalNYKD(intLpc - 1).dblCurrentQuantity
          intCase += originalNYKD(intLpc - 1).sl_nykd_hako
          intNum += originalNYKD(intLpc - 1).intCurrentNumber

        Else
          ReDim Preserve duplidatedNYKD(intInsertRow)
          duplidatedNYKD(intInsertRow) = originalNYKD(intLpc - 1)
          If decQty > 0 Then
            decQty += originalNYKD(intLpc - 1).dblCurrentQuantity
            intCase += originalNYKD(intLpc - 1).sl_nykd_hako
            intNum += originalNYKD(intLpc - 1).intCurrentNumber

            duplidatedNYKD(intInsertRow).sl_nykd_suryo = decQty
            duplidatedNYKD(intInsertRow).sl_nykd_hako = intCase
            duplidatedNYKD(intInsertRow).sl_nykd_honsu = intNum
          End If

          intInsertRow += 1
          decQty = 0D
        End If
      Next

      'last row
      ReDim Preserve duplidatedNYKD(intInsertRow)
      duplidatedNYKD(intInsertRow) = originalNYKD(intLpc - 1)
      If decQty > 0 Then
        decQty += originalNYKD(intLpc - 1).dblCurrentQuantity
        intCase += originalNYKD(intLpc - 1).sl_nykd_hako
        intNum += originalNYKD(intLpc - 1).intCurrentNumber

        duplidatedNYKD(intInsertRow).sl_nykd_suryo = decQty
        duplidatedNYKD(intInsertRow).sl_nykd_hako = intCase
        duplidatedNYKD(intInsertRow).sl_nykd_honsu = intNum

      End If
      
      Return duplidatedNYKD
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return originalNYKD
    End Try

  End Function

#End Region

  Private Function SummaryDetails(ByVal oldArray As SL_NYKD()) As SL_NYKD()
    Try
      'Summary Details that has same denno, 入荷区分、商品コード、商品名、規格・型番、色、サイズ、倉庫、税区分、税込区分、入数、単位、単価
      'Using Data Table
      Dim newArray As SL_NYKD() = {}
      Dim dataTable As DataTable = New DataTable
      Dim DiffPriceCount As Integer = -1
      Dim intDiffPriceCountTemp As Integer

      'Get ID and calculate Quantity (suryo)
      dataTable.Columns.Add("sl_nykd_id")
      dataTable.Columns.Add("sl_nykd_suryo")
      dataTable.Columns.Add("sl_nykd_kingaku")
      dataTable.Columns.Add("sl_nykd_hako")
      dataTable.Columns.Add("sl_nykd_honsu")
      'Item to compare to decide summarization
      dataTable.Columns.Add("sl_nykd_hid")
      dataTable.Columns.Add("sl_nykd_nkbn")
      dataTable.Columns.Add("sl_nykd_scd")
      dataTable.Columns.Add("sl_nykd_mei")
      dataTable.Columns.Add("sl_nykd_kikaku")
      dataTable.Columns.Add("sl_nykd_color")
      dataTable.Columns.Add("sl_nykd_size")
      dataTable.Columns.Add("sl_nykd_souko")
      dataTable.Columns.Add("sl_nykd_tax")
      dataTable.Columns.Add("sl_nykd_komi")
      dataTable.Columns.Add("sl_nykd_iri")
      dataTable.Columns.Add("sl_nykd_tani")
      dataTable.Columns.Add("sl_nykd_tanka")
      dataTable.Columns.Add("DiffPriceNo")

      'Mark data to sort by Unit price and branch number
      For intCount As Integer = 0 To oldArray.Length - 1

        If intCount = 0 OrElse oldArray(intCount).sl_nykd_hid <> oldArray(intCount - 1).sl_nykd_hid OrElse oldArray(intCount).sl_nykd_scd.Trim <> oldArray(intCount - 1).sl_nykd_scd.Trim Then

          DiffPriceCount += 1
          CreateNewRow(dataTable, oldArray(intCount), DiffPriceCount)

        Else
          intDiffPriceCountTemp = IsContained(oldArray(intCount), dataTable)
          If intDiffPriceCountTemp = -1 Then 'Not contain
            DiffPriceCount += 1
            CreateNewRow(dataTable, oldArray(intCount), DiffPriceCount)

          Else 'Contain -> Item to summarize
            CreateNewRow(dataTable, oldArray(intCount), intDiffPriceCountTemp)

          End If

        End If

      Next

      'Sort marked data
      Dim sortDataRow As DataRow()
      sortDataRow = dataTable.Select(String.Empty, "DiffPriceNo")

      'Summarize duplicate data
      For intDataRowCount As Integer = 0 To sortDataRow.Length - 1
        If intDataRowCount = 0 OrElse sortDataRow(intDataRowCount).Item("DiffPriceNo").ToString.Trim <> sortDataRow(intDataRowCount - 1).Item("DiffPriceNo").ToString.Trim Then

          Dim SL_NYKDItem As SL_NYKD = New SL_NYKD(connector)
          SL_NYKDItem.ReadByID(CInt(sortDataRow(intDataRowCount).Item("sl_nykd_id")))
          SL_NYKDItem.sl_nykd_tanka = CDec(sortDataRow(intDataRowCount).Item("sl_nykd_tanka"))
          SL_NYKDItem.sl_nykd_suryo = CDec(sortDataRow(intDataRowCount).Item("sl_nykd_suryo"))
          SL_NYKDItem.sl_nykd_hako = CInt(sortDataRow(intDataRowCount).Item("sl_nykd_hako"))
          SL_NYKDItem.sl_nykd_honsu = CInt(sortDataRow(intDataRowCount).Item("sl_nykd_honsu"))
          SL_NYKDItem.sl_nykd_kingaku = CInt(sortDataRow(intDataRowCount).Item("sl_nykd_kingaku"))
          ReDim Preserve newArray(newArray.Length)
          newArray(newArray.Length - 1) = SL_NYKDItem

        Else
          newArray(newArray.Length - 1).sl_nykd_suryo += CDec(sortDataRow(intDataRowCount).Item("sl_nykd_suryo"))
          newArray(newArray.Length - 1).sl_nykd_kingaku += CDec(sortDataRow(intDataRowCount).Item("sl_nykd_kingaku"))
          newArray(newArray.Length - 1).sl_nykd_hako = CInt(sortDataRow(intDataRowCount).Item("sl_nykd_hako"))
          newArray(newArray.Length - 1).sl_nykd_honsu = CInt(sortDataRow(intDataRowCount).Item("sl_nykd_honsu "))
        End If

      Next

      Return newArray
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return Nothing
    End Try

  End Function

  Public Sub CreateNewRow(Datatable As DataTable, SL_NYKDItem As SL_NYKD, DiffPriceCount As Integer)
    Dim dr As DataRow = Datatable.NewRow
    dr("sl_nykd_id") = SL_NYKDItem.sl_nykd_id
    dr("sl_nykd_suryo") = SL_NYKDItem.sl_nykd_suryo
    dr("sl_nykd_kingaku") = SL_NYKDItem.sl_nykd_kingaku
    dr("sl_nykd_hid") = SL_NYKDItem.sl_nykd_hid
    dr("sl_nykd_scd") = SL_NYKDItem.sl_nykd_scd
    dr("sl_nykd_nkbn") = SL_NYKDItem.sl_nykd_nkbn
    dr("sl_nykd_mei") = SL_NYKDItem.sl_nykd_mei
    dr("sl_nykd_kikaku") = SL_NYKDItem.sl_nykd_kikaku
    dr("sl_nykd_color") = SL_NYKDItem.sl_nykd_color
    dr("sl_nykd_size") = SL_NYKDItem.sl_nykd_size
    dr("sl_nykd_souko") = SL_NYKDItem.sl_nykd_souko
    dr("sl_nykd_tax") = SL_NYKDItem.sl_nykd_tax
    dr("sl_nykd_komi") = SL_NYKDItem.sl_nykd_komi
    dr("sl_nykd_iri") = SL_NYKDItem.sl_nykd_iri
    dr("sl_nykd_tani") = SL_NYKDItem.sl_nykd_tani
    dr("sl_nykd_tanka") = SL_NYKDItem.sl_nykd_tanka
    dr("sl_nykd_hako") = SL_NYKDItem.sl_nykd_hako
    dr("sl_nykd_honsu") = SL_NYKDItem.sl_nykd_honsu
    dr("DiffPriceNo") = DiffPriceCount
    Datatable.Rows.Add(dr)
  End Sub

  Public Function IsContained(SL_NYKDItem As SL_NYKD, dt As System.Data.DataTable) As Integer

    For Each dr As DataRow In dt.Rows
      If CInt(dr("sl_nykd_hid")) = SL_NYKDItem.sl_nykd_hid AndAlso _
      dr("sl_nykd_scd").ToString = SL_NYKDItem.sl_nykd_scd AndAlso _
      CInt(dr("sl_nykd_nkbn")) = SL_NYKDItem.sl_nykd_nkbn AndAlso _
      dr("sl_nykd_mei").ToString = SL_NYKDItem.sl_nykd_mei AndAlso _
      dr("sl_nykd_kikaku").ToString = SL_NYKDItem.sl_nykd_kikaku AndAlso _
      dr("sl_nykd_color").ToString = SL_NYKDItem.sl_nykd_color AndAlso _
      dr("sl_nykd_size").ToString = SL_NYKDItem.sl_nykd_size AndAlso _
      dr("sl_nykd_souko").ToString = SL_NYKDItem.sl_nykd_souko AndAlso _
      CInt(dr("sl_nykd_tax")) = SL_NYKDItem.sl_nykd_tax AndAlso _
      CInt(dr("sl_nykd_komi")) = SL_NYKDItem.sl_nykd_komi AndAlso _
      CDec(dr("sl_nykd_iri")) = SL_NYKDItem.sl_nykd_iri AndAlso _
      dr("sl_nykd_tani").ToString = SL_NYKDItem.sl_nykd_tani AndAlso _
      CDec(dr("sl_nykd_tanka")) = SL_NYKDItem.sl_nykd_tanka Then
        Return CInt(dr("DiffPriceNo"))
      End If
    Next

    Return -1
  End Function

  Private Function Create(myController As InputNYKController) As Boolean

    Dim oldCursor As Cursor = Me.Cursor

    Try
      Me.Cursor = Cursors.WaitCursor

      '伝票入力エンティティから仕入伝票を設定
      Dim beInputNYK As BusinessEntity.BEInputNYK = ConvertKonInput.InputToNYK(myController.ModifiedEntity)

      If Not (beInputNYK.InputNYKDList.BEInputNYKD.Count > 0) Then Return False

      myController.AdjustDenpyo(beInputNYK)

      '登録
      Dim denpyoNo As Integer = 0
      Dim result As ResultSaveInputNYK = myController.CreateBEInputNYK(beInputNYK, IntegratedApplicationMethodType.Create, connector)

      If result.Status = PCA.ApplicationIntegration.IntegratedStatus.Success Then
        denpyoNo = result.ArrayOfBEKonIntegrationResultOfBEInputNYK.BEKonIntegrationResultOfBEInputNYK(0).Target.InputNYKH.DenpyoNo
      End If


      ' 初期化()
      Dim newKonInput As PEKonInput = myController.CreateInitialEntity()
      myController.UpdateActiveEntity(newKonInput)
      Me.ldtWhDate.Focus()

      Return True

    Finally
      'カーソルを元に戻す
      Me.Cursor = oldCursor

    End Try
  End Function

  Private Sub GetSearchResult(Optional isSelected As Boolean = True)
    Try
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim whDate As Integer = ldtWhDate.IntDate
      Dim isInclude As Integer
      Dim intCount As Integer = -1

      duplicatedCell = Nothing
      m_noOfChecked = 0
      Results = Nothing
      If chkIsInclude.Checked Then isInclude = 1 Else isInclude = 0

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKH_NYKD_Pro03030")
      selectCommand.Parameters("@whDate").SetValue(whDate)
      selectCommand.Parameters("@isInclude").SetValue(isInclude)

      reader = selectCommand.ExecuteReader
      While reader.Read() = True

        intCount = intCount + 1
        ReDim Preserve Results(intCount)

        Results(intCount) = New ColumnResult

        Results(intCount).IsConvert = isSelected

        Dim tempDate As Date
        If Date.TryParseExact(reader.GetValue("sl_nykh_uribi").ToString(), "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, tempDate) Then
          Results(intCount).WhDate = tempDate.ToShortDateString.Trim
          Results(intCount).PurDate = tempDate
        End If

        Results(intCount).SupCode = reader.GetValue("sl_nykh_tcd").ToString().Trim
        Results(intCount).SupName = reader.GetValue("cms_mei1").ToString().Trim & " " & reader.GetValue("cms_mei2").ToString().Trim
        Results(intCount).ProductCode = reader.GetValue("sl_nykd_scd").ToString().Trim
        Results(intCount).ProductName = reader.GetValue("sl_nykd_mei").ToString().Trim

        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_nykd_suryo")) Then
          Results(intCount).Quantity = CDec(reader.GetValue("sl_nykd_suryo"))
        End If

        Results(intCount).UnitName = reader.GetValue("sl_nykd_tani").ToString().Trim

        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_nykd_tanka")) Then
          Results(intCount).UnitPrice = CDec(reader.GetValue("sl_nykd_tanka"))
        End If

        Results(intCount).LotNo = reader.GetValue("sl_nykd_ulotno").ToString().Trim

        For intCountLotDetails As Integer = 1 To m_SL_LMBClass.sl_lmb_maxlotop
          Results(intCount).Lotdetails(intCountLotDetails - 1) = reader.GetValue("sl_nykd_ldmei" & intCountLotDetails.ToString).ToString()
        Next

        Results(intCount).SlipNo = CInt(reader.GetValue("sl_nykh_denno"))

        Results(intCount).QuantityNoOfDecDigit = CInt(reader.GetValue("sms_sketa"))
        Results(intCount).UnitPriceNoOfDecDigit = CInt(reader.GetValue("sms_tketa"))
        Results(intCount).DetailsID = CInt(reader.GetValue("sl_nykd_id"))

      End While

      'create duplicatedCell Array
      If Not Results Is Nothing Then duplicatedCell = New Boolean(Results.Length - 1, m_SlipNoColumn) {}

      reader.Close()
      reader.Dispose()

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

  Private Sub DisplaySearchResult()
    Try
      dgv.Columns.Clear()
      InitTable()

      DuplicateRow()

      m_noOfCheckBox = If(Results.Count > 0, 1, 0)
      For intCount As Integer = 0 To Results.Count - 1
        If intCount > dgv.Rows.Count - 1 AndAlso Results(intCount) IsNot Nothing Then dgv.Rows.Add()
        If Not Results(intCount) Is Nothing Then
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ConvertFlag).Value = Results(intCount).IsConvert
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.WHDate).Value = Results(intCount).WhDate.Trim
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.PurDate).Value = Results(intCount).PurDate
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.SupCode).Value = Results(intCount).SupCode
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.SupName).Value = Results(intCount).SupName
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ProductCode).Value = Results(intCount).ProductCode
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.ProductName).Value = Results(intCount).ProductName
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.Qty).Value = Results(intCount).Quantity.ToString(SLCmnFunction.FormatComma(Results(intCount).QuantityNoOfDecDigit))
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitName).Value = Results(intCount).UnitName
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.UnitPrice).Value = Results(intCount).UnitPrice.ToString(SLCmnFunction.FormatComma(Results(intCount).UnitPriceNoOfDecDigit))
          dgv.Rows.Item(intCount).Cells.Item(ColumnIndex.LotNo).Value = Results(intCount).LotNo

          Dim SL_SMSClass As SL_SMS = New SL_SMS(connector)
          SL_SMSClass.ReadByProductCode(Results(intCount).ProductCode)
          If m_SL_LMBClass.sl_lmb_autolot = SLConstants.SetLotType.Manual Then
            If SL_SMSClass.sl_sms_lkbn = 0 Then
              dgv(ColumnIndex.LotNo, intCount).ReadOnly = True
              SLCmnFunction.SetEditableCellColor(dgv, ColumnIndex.LotNo, intCount, Color.White)
            Else
              dgv(ColumnIndex.LotNo, intCount).ReadOnly = False
              SLCmnFunction.SetEditableCellColor(dgv, ColumnIndex.LotNo, intCount, Nothing)
            End If

          Else
            dgv(ColumnIndex.LotNo, intCount).ReadOnly = True
          End If

          For intCountLotDetails As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
            dgv.Rows.Item(intCount).Cells.Item(11 + intCountLotDetails).Value = Results(intCount).Lotdetails(intCountLotDetails)
          Next
          dgv.Rows.Item(intCount).Cells.Item(m_SlipNoColumn).Value = Results(intCount).SlipNo.ToString.Trim
        End If
      Next

      CheckDuplicateCell()

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

  Private Sub CheckDuplicateCell()
    Try
      Dim QtyAdded As Boolean = False
      Dim decQty As Decimal = 0D
      Dim intAddBeginRow As Integer = 0
      Dim SMS_Class As SMS = New SMS(connector)

      'In the same slip, only cells in first row  = true, others = false
      For intRow As Integer = 1 To Results.Length - 1
        For intColumn As Integer = ColumnIndex.ConvertFlag To ColumnIndex.SupName
          If Not Results(intRow) Is Nothing Then

            If dgv.Rows(intRow - 1).Cells(m_SlipNoColumn).Value.ToString().Trim = dgv.Rows(intRow).Cells(m_SlipNoColumn).Value.ToString().Trim Then 'If the slip number is the same
              duplicatedCell(intRow, intColumn) = True 'duplicated.
            Else
              duplicatedCell(intRow, intColumn) = False
              If intColumn = ColumnIndex.ConvertFlag Then m_noOfCheckBox = m_noOfCheckBox + 1
            End If
          End If
        Next
      Next
      'In the same slip and same product, only cells in first row  = true, others = false
      For intRow As Integer = 1 To Results.Length - 1
        If Not Results(intRow) Is Nothing Then
          'If the slip number and productcode/productname is the same
          If dgv.Rows(intRow - 1).Cells(m_SlipNoColumn).Value.ToString().Trim = dgv.Rows(intRow).Cells(m_SlipNoColumn).Value.ToString().Trim AndAlso _
                          (dgv.Rows(intRow - 1).Cells(ColumnIndex.ProductCode).Value.ToString().Trim = dgv.Rows(intRow).Cells(ColumnIndex.ProductCode).Value.ToString().Trim And _
                          dgv.Rows(intRow - 1).Cells(ColumnIndex.ProductName).Value.ToString().Trim = dgv.Rows(intRow).Cells(ColumnIndex.ProductName).Value.ToString().Trim) Then
            'duplicated.
            duplicatedCell(intRow, ColumnIndex.ProductCode) = True
            duplicatedCell(intRow, ColumnIndex.ProductName) = True
          Else
            duplicatedCell(intRow, ColumnIndex.ProductCode) = False
            duplicatedCell(intRow, ColumnIndex.ProductName) = False
          End If
        End If
      Next
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub DuplicateRow()
    Try
      Dim duplicatedResult() As ColumnResult = {New ColumnResult}
      Dim intInsertRow As Integer = 0
      Dim decQty As Decimal = 0D
      Dim intLpc As Integer = 0

      ReDim duplicatedResult(0)
      For intLpc = 1 To Results.Length - 1
        If Results(intLpc - 1).SlipNo = Results(intLpc).SlipNo And Results(intLpc - 1).ProductCode = Results(intLpc).ProductCode And Results(intLpc - 1).ProductName = Results(intLpc).ProductName And _
             Results(intLpc - 1).UnitPrice = Results(intLpc).UnitPrice And Results(intLpc - 1).LotNo = Results(intLpc).LotNo Then

          decQty += Results(intLpc - 1).Quantity

        Else
          ReDim Preserve duplicatedResult(intInsertRow)
          duplicatedResult(intInsertRow) = Results(intLpc - 1)
          If decQty > 0 Then decQty += Results(intLpc - 1).Quantity : duplicatedResult(intInsertRow).Quantity = decQty
          intInsertRow += 1
          decQty = 0D
        End If
      Next

      'last row
      ReDim Preserve duplicatedResult(intInsertRow)
      duplicatedResult(intInsertRow) = Results(intLpc - 1)
      If decQty > 0 Then decQty += Results(intLpc - 1).Quantity : duplicatedResult(intInsertRow).Quantity = decQty

      Results = duplicatedResult
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region " DataGridView"

  Private Sub dgv_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick
    If e.RowIndex > -1 AndAlso Not dgv.Rows(e.RowIndex).Cells(ColumnIndex.ConvertFlag).Value Is Nothing AndAlso _
     e.ColumnIndex = ColumnIndex.ConvertFlag AndAlso Not duplicatedCell(e.RowIndex, e.ColumnIndex) Then
      If dgv.Rows(e.RowIndex).Cells(ColumnIndex.ConvertFlag).Value.ToString = "False" Then
        m_noOfChecked = m_noOfChecked - 1
      Else
        m_noOfChecked = m_noOfChecked + 1
      End If

      If m_noOfChecked > 0 Then
        PcaFunctionCommandExecute.Enabled = True
      Else
        PcaFunctionCommandExecute.Enabled = False
      End If
    End If

  End Sub

  Private Sub dgv_CurrentCellDirtyStateChanged(sender As System.Object, e As System.EventArgs) Handles dgv.CurrentCellDirtyStateChanged
    If dgv.IsCurrentCellDirty Then
      dgv.CommitEdit(DataGridViewDataErrorContexts.Commit)
    End If
  End Sub

  Private Sub DataGridView1_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgv.CellPainting
    If m_isDataSet AndAlso Not duplicatedCell Is Nothing Then

      If e.ColumnIndex <= ColumnIndex.ProductName AndAlso e.RowIndex > -1 Then
        Using gridBrush As Brush = New SolidBrush(Me.dgv.GridColor), backColorBrush As Brush = New SolidBrush(e.CellStyle.BackColor)

          Using gridLinePen As Pen = New Pen(gridBrush)

            ' Clear cell  
            e.Graphics.FillRectangle(backColorBrush, e.CellBounds)

            ' Draw line (bottom border and right border of current cell)  
            'If next row cell has different content, only draw bottom border line of current cell  
            If e.RowIndex < Results.Length - 1 Then
              If duplicatedCell Is Nothing OrElse Not duplicatedCell(e.RowIndex + 1, e.ColumnIndex) Then
                e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1, e.CellBounds.Right - 1, e.CellBounds.Bottom - 1)
              End If
            Else
              e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1, e.CellBounds.Right - 1, e.CellBounds.Bottom - 1)
            End If

            ' Draw right border line of current cell  
            e.Graphics.DrawLine(gridLinePen, e.CellBounds.Right - 1, e.CellBounds.Top, e.CellBounds.Right - 1, e.CellBounds.Bottom)

            ' draw/fill content in current cell, and fill only one cell of multiple same cells  
            If Not e.Value Is Nothing Then
              If duplicatedCell(e.RowIndex, e.ColumnIndex) Then
              Else
                e.PaintContent(e.CellBounds) ' Fill in checkbox column
              End If
            End If
            e.Handled = True
          End Using
        End Using
      End If
    End If
  End Sub

  Private Sub DataGridView1_CellEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEnter
    If (e.RowIndex = 0 AndAlso e.ColumnIndex = 0) OrElse duplicatedCell Is Nothing OrElse e.RowIndex > Results.Length - 1 Then
      Return
    End If
    If duplicatedCell(e.RowIndex, e.ColumnIndex) Then
      dgv.Rows(e.RowIndex).ReadOnly = True
    Else
      m_CurColumnIndex = e.ColumnIndex
      m_CurRowIndex = e.RowIndex
      If Not dgv.CurrentCell.Value Is Nothing Then m_CurCellValue = dgv.CurrentCell.Value.ToString
    End If

  End Sub

  ''Format value for Unit Price column
  Private Sub dgv_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgv.EditingControlShowing
    If m_CurColumnIndex = ColumnIndex.UnitPrice Then
      AddHandler CType(e.Control, TextBox).KeyPress, AddressOf Cell_KeyPress
      AddHandler CType(e.Control, TextBox).KeyUp, AddressOf Cell_keyUp
    End If
  End Sub

  Private Sub Cell_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
    If m_CurColumnIndex = ColumnIndex.UnitPrice Then
      'If user input a character that is not numberic or not backspace
      If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = "." Or e.KeyChar = System.Convert.ToChar(Keys.Back)) Then
        e.Handled = True
        'If current value is already contains "." or Unit price's no of decimal digit = 0 (Must be integer)
      ElseIf (m_CurCellValue.Contains(".") OrElse Results(m_CurRowIndex).UnitPriceNoOfDecDigit = 0) AndAlso e.KeyChar = "." Then
        e.Handled = True
      End If
    End If
  End Sub

  Private Sub Cell_keyUp(ByVal sender As Object, ByVal e As KeyEventArgs)
    If m_CurColumnIndex = ColumnIndex.UnitPrice Then
      'Get current cell value
      Dim txtbox As TextBox = DirectCast(sender, TextBox)
      m_CurCellValue = txtbox.Text
    End If
  End Sub

  Private Sub dgv_CellValidated(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellValidated
    If m_CurColumnIndex = ColumnIndex.UnitPrice Then
      If Not IsNumeric(m_CurCellValue) OrElse m_CurCellValue.Length > MAX_UNIT_PRICE_LENGTH Then
        dgv.CurrentCell.Value = 0
      Else
        dgv.CurrentCell.Value = Decimal.Parse(m_CurCellValue).ToString(SLCmnFunction.FormatComma(Results(e.RowIndex).UnitPriceNoOfDecDigit))
      End If
      m_CurColumnIndex = -1
      m_CurRowIndex = -1
      m_CurCellValue = String.Empty

    End If

  End Sub

  'Merge header cell 
  Private Sub DataGridView1_Paint(sender As Object, e As System.Windows.Forms.PaintEventArgs) Handles dgv.Paint
    Dim monthes As String() = {ColumnName.SupCode, ColumnName.ProductCode}
    Dim j As Integer = ColumnIndex.SupCode 'Merge 2-3 and 4-5
    While j <= ColumnIndex.ProductName
      Dim r1 As Rectangle = dgv.GetCellDisplayRectangle(j, -1, True)
      Dim w2 As Integer = dgv.GetCellDisplayRectangle(j + 1, -1, True).Width
      r1.X += 1
      r1.Y += 1
      r1.Width = r1.Width + w2 - 2
      r1.Height = CInt(r1.Height - 1)
      e.Graphics.FillRectangle(New SolidBrush(dgv.ColumnHeadersDefaultCellStyle.BackColor), r1)
      Dim format As New StringFormat()
      format.Alignment = StringAlignment.Near
      format.LineAlignment = StringAlignment.Center
      e.Graphics.DrawString(monthes(j \ 2 - 1), dgv.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Color.White), r1, format)
      j += 2
    End While
  End Sub

  Private Sub DataGridView1_Scroll(sender As Object, e As System.Windows.Forms.ScrollEventArgs) Handles dgv.Scroll
    Dim rtHeader As Rectangle = dgv.DisplayRectangle
    rtHeader.Height = CInt(dgv.ColumnHeadersHeight / 2)
    dgv.Invalidate(rtHeader)
  End Sub

  Private Sub dgv_CellValidating(sender As Object, e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgv.CellValidating
    Try
      Dim dtSupDate As Date

      If m_isClosing Then Return
      If e.ColumnIndex = ColumnIndex.LotNo And Not dgv(e.ColumnIndex, e.RowIndex).ReadOnly Then
        If dgv(ColumnIndex.LotNo, e.RowIndex).Value Is Nothing OrElse dgv(ColumnIndex.LotNo, e.RowIndex).Value.ToString.Trim.Length = 0 Then
          SLCmnFunction.ShowToolTip("ロットNo" & SLConstants.NotifyMessage.INPUT_REQUIRED, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PcaFunctionBar1, ToolTip1, Me, _
                                    PcaCommandItemExecute.CommandId)
          e.Cancel = True
          Return
        End If
      End If


      If e.ColumnIndex = ColumnIndex.PurDate Then 'WHDate
        If Not Date.TryParse(dgv(ColumnIndex.PurDate, e.RowIndex).Value.ToString, dtSupDate) Then
          SLCmnFunction.ShowToolTip("正しい日付" & SLConstants.NotifyMessage.INPUT_REQUIRED, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PcaFunctionBar1, ToolTip1, Me, _
                                    PcaCommandItemExecute.CommandId)
          e.Cancel = True
          Return
        End If

        If Date.Parse(dgv(ColumnIndex.WHDate, e.RowIndex).Value.ToString) > Date.Parse(dgv(ColumnIndex.PurDate, e.RowIndex).Value.ToString) Then
          SLCmnFunction.ShowToolTip("入荷日以降の日付" & SLConstants.NotifyMessage.INPUT_REQUIRED, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PcaFunctionBar1, ToolTip1, Me, _
                                    PcaCommandItemExecute.CommandId)
          e.Cancel = True
          Return
        End If

      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region "Validate"

  Public Function ValidateFormItems(ByVal m_Controller As InputNYKController) As Boolean

    If Not ValidateHeader(m_Controller) Then Return False

    If Not ValidateMeisai(m_Controller) Then Return False

    Return True

  End Function

  Private Function ValidateHeader(ByVal m_Controller As InputNYKController) As Boolean

    If String.IsNullOrEmpty(m_Controller.ModifiedEntity.Header.TorihikisakiCode) Then
      ShowMessage.OnExclamation(Me, String.Format(MessageDefines.Exclamation.NotInput, BusinessEntity.ItemText.Tokuisaki), "入荷振替")
      Me.ldtWhDate.Focus()
      Return False
    End If

    Return True

  End Function

  Private Function ValidateMeisai(ByVal m_Controller As InputNYKController) As Boolean
    '
    Dim edaban As Integer = 0
    'Edit Slip Input entity 
    For Each detail As PEKonInputDetail In m_Controller.ModifiedEntity.DetailList
      'Set branch no.
      edaban += 1
      detail.Edaban = CShort(edaban)
      'Set table state
      detail.MeisaiState = KonCalc.JudgeSykdState(detail.SyohinCode, detail.RendoHeaderId, detail.RendoShiji, detail.MasterKubun, detail.Suryo)
    Next

    'In Create mode
    If m_Controller.ModifiedEntity.Header.MitsumoriJuchuKubun = MitsumoriJuchuKubunType.Juchu _
    AndAlso m_Controller.ModifiedEntity.Header.RendoDenpyoNo <> 0 Then
      Return True
    End If

    If m_Controller.ModifiedEntity.DetailList.Exists(Function(match) match.MeisaiState = PEKonInput.MeisaiStateType.Valid) = False Then
      'If there is no details slip
      Me.ldtWhDate.Focus()
      Return False
    End If

    If m_Controller.ModifiedEntity IsNot Nothing _
    AndAlso m_Controller.ModifiedEntity.DetailList IsNot Nothing AndAlso m_Controller.ModifiedEntity.DetailList.Count > 0 Then
      Dim errorMessage As String = String.Empty
      For i As Integer = 0 To m_Controller.ModifiedEntity.DetailList.Count - 1
        If (m_Controller.ModifiedEntity.DetailList(i).MasterKubun = MasterKubunType.Ippan OrElse m_Controller.ModifiedEntity.DetailList(i).MasterKubun = MasterKubunType.Zatsuhin) _
        AndAlso (m_Controller.CheckFugoKingakuAndGenka(m_Controller.ModifiedEntity.DetailList(i).Kingaku, m_Controller.ModifiedEntity.DetailList(i).Genka, False) = False) Then

          If String.IsNullOrEmpty(errorMessage) Then
            errorMessage = "枝番 '" & m_Controller.ModifiedEntity.DetailList(i).Edaban.ToString() & "'"
          Else
            errorMessage += ", '" & m_Controller.ModifiedEntity.DetailList(i).Edaban.ToString() & "'"
          End If
        End If
      Next

      If errorMessage.Length <> 0 Then
        errorMessage += " の"
        ShowMessage.OnExclamation(Me, errorMessage & String.Format(MessageDefines.Exclamation.NotEqualSign, BusinessEntity.ItemText.Kingaku, BusinessEntity.ItemText.Genka), "入荷振替")
        Return False
      End If

    End If

    'Check number of digit in Unit price
    If Not m_Controller.CheckTankaKeta(m_Controller.ModifiedEntity, PEKonInput.DenpyoType.Uriage) Then Return False

    Return True

  End Function

  Private Function ValidateConvert() As Boolean

    Try
      Dim dtSupDate As Date
      For lLpc As Integer = 0 To dgv.RowCount - 1
        If Not dgv(ColumnIndex.LotNo, lLpc).ReadOnly Then
          If dgv(ColumnIndex.LotNo, lLpc).Value Is Nothing OrElse dgv(ColumnIndex.LotNo, lLpc).Value.ToString.Trim.Length = 0 And _
           dgv.Rows.Item(lLpc).Cells.Item(ColumnIndex.ConvertFlag).Value.ToString = "True" Then
            dgv.CurrentCell = dgv(ColumnIndex.LotNo, lLpc)
            SLCmnFunction.ShowToolTip("ロットNoを入力して下さい。", SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemExecute.CommandId)
            Return False
          End If
        End If

        If Not Date.TryParse(dgv(ColumnIndex.PurDate, lLpc).Value.ToString, dtSupDate) Then
          SLCmnFunction.ShowToolTip("正しい日付" & SLConstants.NotifyMessage.INPUT_REQUIRED, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PcaFunctionBar1, ToolTip1, Me, _
                                    PcaCommandItemExecute.CommandId)
          Return False
        End If

        If Date.Parse(dgv(ColumnIndex.WHDate, lLpc).Value.ToString) > Date.Parse(dgv(ColumnIndex.PurDate, lLpc).Value.ToString) Then
          SLCmnFunction.ShowToolTip("入荷日以降の日付" & SLConstants.NotifyMessage.INPUT_REQUIRED, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PcaFunctionBar1, ToolTip1, Me, _
                                    PcaCommandItemExecute.CommandId)

          Return False
        End If
      Next
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

#End Region


  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    Try
      If Not m_appClass.isAttach Then connector.LogOffSystem()
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

End Class

Public Class ColumnResult
  Public Property IsConvert As Boolean
  Public Property WhDate As String
  Public Property PurDate As Date

  Public Property SupCode As String = String.Empty
  Public Property SupName As String = String.Empty
  Public Property ProductCode As String = String.Empty
  Public Property ProductName As String = String.Empty

  Public Property Quantity As Decimal
  Public Property UnitName As String = String.Empty
  Public Property UnitPrice As Decimal
  Public Property LotNo As String = String.Empty

  Public Property Lotdetails() As String() = New String(10) {}

  Public Property SlipNo As Integer
  Public Property QuantityNoOfDecDigit As Integer
  Public Property UnitPriceNoOfDecDigit As Integer
  Public Property DetailsID As Integer
End Class

Public Class ColumnName
  Public Const IsConvert As String = "仕入"
  Public Const WhDate As String = "入荷日"
  Public Const PurDate As String = "仕入日"
  Public Const SupCode As String = " 仕入先"
  Public Const SupName As String = "仕入先名"
  Public Const ProductCode As String = " 商品"
  Public Const ProductName As String = "商品名"
  Public Const Quantity As String = "数量"
  Public Const UnitName As String = "単位"
  Public Const UnitPrice As String = "単価"
  Public Const LotNo As String = "ロットNo"
  Public Const Lotdetails As String = "ロット詳細"
  Public Const Lotdetails1 As String = "ロット詳細1"
  Public Const Lotdetails2 As String = "ロット詳細2"
  Public Const Lotdetails3 As String = "ロット詳細3"
  Public Const Lotdetails4 As String = "ロット詳細4"
  Public Const Lotdetails5 As String = "ロット詳細5"
  Public Const Lotdetails6 As String = "ロット詳細6"
  Public Const Lotdetails7 As String = "ロット詳細7"
  Public Const Lotdetails8 As String = "ロット詳細8"
  Public Const Lotdetails9 As String = "ロット詳細9"
  Public Const Lotdetails10 As String = "ロット詳細10"
  Public Const SlipNo As String = "伝票番号"
End Class

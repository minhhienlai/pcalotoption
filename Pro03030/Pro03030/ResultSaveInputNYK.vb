﻿Imports PCA.TSC.Kon
Imports PCA.TSC.Kon.Tools
#Region "【更新履歴】コメント"
'15.07.30 @TSC  <AppId>Kon10</AppId><Version>1.0</Version><Revision>4.00</Revision><BEVersion>1</BEVersion>
'

#End Region

''' <summary>売上伝票(InputSYK)のCreate、Modifyメソッドの戻り値
''' （補足）複数の業務エンティティを指定することが前提。
''' </summary>
Public Class ResultSaveInputNYK

  'Status、ErrorCode、ErrorMessageの各プロパティ
  Inherits Tools.ResultBase

  '複数の業務エンティティ
  Public Property ArrayOfBEKonIntegrationResultOfBEInputNYK As ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK _
      = New ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK

  Public Class ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK

    Public Property BEKonIntegrationResultOfBEInputNYK As List(Of ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK) _
        = New List(Of ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK)

    ''' <summary>売上伝票(InputSYK)のCreate、Modifyメソッドの伝票一件ごとの結果
    ''' </summary>
    Public Class ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK

      'Status、ErrorCode、ErrorMessage、InnerStatusの各項目
      Inherits BEKonIntegratedStatusResult

      '売上伝票(InputSYK)の単一の業務エンティティ
      Public Target As BusinessEntity.BEInputNYK = New BusinessEntity.BEInputNYK

    End Class

  End Class

End Class


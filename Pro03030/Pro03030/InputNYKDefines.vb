﻿
''' <summary>伝票テーブルのカラム名の定義クラス
''' </summary>
Public Class ColumnNameDefines

    ''' <summary>枝番ヘッダーカラム名
    ''' </summary>
    Public Const HeaderEdaban As String = "HeaderEdaban"

    ''' <summary>連動指示ヘッダーカラム名
    ''' </summary>
    Public Const HeaderRendoShiji As String = "RendoShiji"

    ''' <summary>商品コード等ヘッダーカラム名
    ''' </summary>
    Public Const HeaderSyohinCodeEtc As String = "HeaderSyohinEtc"

    ''' <summary>数量等ヘッダーカラム名
    ''' </summary>
    Public Const HeaderSuryoEtc As String = "HeaderSuryoEtc"

    ''' <summary>売単価等ヘッダーカラム名
    ''' </summary>
    Public Const HeaderTankaEtc As String = "HeaderTankaEtc"

    ''' <summary>売価金額等ヘッダーカラム名
    ''' </summary>
    Public Const HeaderKingakuEtc As String = "HeaderKingakuEtc"

    ''' <summary>利益率等ヘッダーカラム名
    ''' </summary>
    Public Const HeaderRiekiRitsuEtc As String = "HeaderRiekiRitsuEtc"

    ''' <summary>標準価格等ヘッダーカラム名
    ''' </summary>
    Public Const HeaderHyojyunKakakuEtc As String = "HeaderHyojyunKakakuEtc"

    ''' <summary>*(商管マーク)等ヘッダーカラム名
    ''' </summary>
    Public Const HeaderKanMark As String = "HeaderKanMark"

    ''' <summary>数値1等ヘッダーカラム名
    ''' </summary>
    Public Const HeaderYobiInt1Etc As String = "HeaderYobiInt1Etc"

    ''' <summary>枝番ボディーカラム名
    ''' </summary>
    Public Const BodyEdaban As String = "BodyEdaban"

    ''' <summary>出(連動指示)ボディーカラム名
    ''' </summary>
    Public Const BodyRendoShiji As String = "BodyRendoShiji"

    ''' <summary>商品コード等ボディーカラム名
    ''' </summary>
    Public Const BodySyohinCodeEtc As String = "BodySyohinCodeEtc"

    ''' <summary>数量等ボディーカラム名
    ''' </summary>
    Public Const BodySuryoEtc As String = "BodySuryoEtc"

    ''' <summary>売単価等ボディーカラム名
    ''' </summary>
    Public Const BodyTankaEtc As String = "BodyTankaEtc"

    ''' <summary>売価金額等ボディーカラム名
    ''' </summary>
    Public Const BodyKingakuEtc As String = "BodyKingakuEtc"

    ''' <summary>利益率等ボディーカラム名
    ''' </summary>
    Public Const BodyRiekiRitsuEtc As String = "BodyRiekiRitsuEtc"

    ''' <summary>標準価格等ボディーカラム名
    ''' </summary>
    Public Const BodyHyojyunKakakuEtc As String = "BodyHyojyunKakakuEtc"

    ''' <summary>*(商管マーク)等ボディーカラム名
    ''' </summary>
    Public Const BodyNyuka As String = "BodyNyuka"

    ''' <summary>数値1等ボディーカラム名
    ''' </summary>
    Public Const BodyYobiInt1Etc As String = "BodyYobiInt1Etc"

End Class

''' <summary>明細テーブルのヘッダーセル名の定義クラス
''' </summary>
Public Class HeaderCellNameDefines

    ''' <summary>枝番（表示上は空文字）
    ''' </summary>
    Public Shared Edaban As String = String.Empty

    ''' <summary>出荷指示
    ''' </summary>
  Public Shared RendoShiji As String = PCA.TSC.Kon.BusinessEntity.ItemText.Syu

    ''' <summary>商品コード
    ''' </summary>
  Public Shared SyohinCode As String = PCA.TSC.Kon.BusinessEntity.ItemText.SyohinCode

    '色、サイズは会社基本情報より設定

    ''' <summary>倉庫
    ''' </summary>
  Public Shared SokoCode As String = PCA.TSC.Kon.BusinessEntity.ItemText.Soko

    ''' <summary>区
    ''' </summary>
  Public Shared Ku As String = PCA.TSC.Kon.BusinessEntity.ItemText.Ku

    ''' <summary>品名
    ''' </summary>
    ''' '***upd***top 15.11.30
    ''' 'Public Shared SyohinMei As String = BusinessEntity.ItemText.HinMei
  Public Shared SyohinMei As String = PCA.TSC.Kon.BusinessEntity.ItemText.SyohinMei
    '***upd***btm 15.11.30

    '規格・型番は会社基本情報より設定

    ''' <summary>数量
    ''' </summary>
  Public Shared Suryo As String = PCA.TSC.Kon.BusinessEntity.ItemText.Suryo

    ''' <summary>売単価
    ''' </summary>
  Public Shared BaiTanka As String = PCA.TSC.Kon.BusinessEntity.ItemText.BaiTanka

    ''' <summary>原単価
    ''' </summary>
  Public Shared GenTanka As String = PCA.TSC.Kon.BusinessEntity.ItemText.GenTanka

    ''' <summary>単価
    ''' </summary>
  Public Shared Tanka As String = PCA.TSC.Kon.BusinessEntity.ItemText.Tanka

    ''' <summary>売価金額
    ''' </summary>
  Public Shared BaikaKingaku As String = PCA.TSC.Kon.BusinessEntity.ItemText.BaikaKingaku

    ''' <summary>原価
    ''' </summary>
  Public Shared Genka As String = PCA.TSC.Kon.BusinessEntity.ItemText.Genka

    ''' <summary>金額
    ''' </summary>
  Public Shared Kingaku As String = PCA.TSC.Kon.BusinessEntity.ItemText.Kingaku

    ''' <summary>利益率
    ''' </summary>
  Public Shared RiekiRitsu As String = PCA.TSC.Kon.BusinessEntity.ItemText.RiekiRitsu

    ''' <summary>粗利益
    ''' </summary>
  Public Shared Ararieki As String = PCA.TSC.Kon.BusinessEntity.ItemText.Ararieki

    ''' <summary>税率
    ''' </summary>
  Public Shared ZeiRitsu As String = PCA.TSC.Kon.BusinessEntity.ItemText.ZeiRitsu

    ''' <summary>備考
    ''' </summary>
  Public Shared Biko As String = PCA.TSC.Kon.BusinessEntity.ItemText.Biko

    ''' <summary>標準価格
    ''' </summary>
  Public Shared HyojunKakaku As String = PCA.TSC.Kon.BusinessEntity.ItemText.HyojunKakaku

    ''' <summary>入荷マーク（アスタリスク）
    ''' </summary>
  Public Shared KanMark As String = PCA.TSC.Kon.BusinessEntity.ItemText.Asterisk

    ''' <summary>予備数値1
    ''' </summary>
  Public Shared YobiInt1 As String = PCA.TSC.Kon.BusinessEntity.ItemText.YobiSuchi1

    ''' <summary>予備金額1
    ''' </summary>
  Public Shared YobiDecimal1 As String = PCA.TSC.Kon.BusinessEntity.ItemText.YobiKingaku1

    ''' <summary>予備文字列1
    ''' </summary>
  Public Shared YobiString1 As String = PCA.TSC.Kon.BusinessEntity.ItemText.YobiMojiretsu1

End Class

''' <summary>伝票テーブルのカラム/セル幅の定義クラス
''' </summary>
Public Class TableDefines

    ''' <summary>枝番カラム/セルの幅
    ''' </summary>
    Public Const EdabanWidth As Single = 3.5F

    ''' <summary>出(連動指示)カラム/セルの幅
    ''' </summary>
    Public Const RendoShijiWidth As Single = 3.5F

    ''' <summary>商品コードセルの幅
    ''' </summary>
    Public Const SyohinCodeWidth As Single = 14.0F

    ''' <summary>色セルの幅
    ''' </summary>
    Public Const ColorWidth As Single = 8.0F

    ''' <summary>サイズセルの幅
    ''' </summary>
    Public Const SizeWidth As Single = 6.0F

    ''' <summary>倉庫セルの幅
    ''' </summary>
    Public Const SokoWidth As Single = 7.0F

    ''' <summary>区セルの幅
    ''' </summary>
    Public Const KuWidth As Single = 3.0F

    ''' <summary>商品コード等のカラムの幅
    ''' </summary>
    Public Const SyohinCodeEtcWidth As Single = SyohinCodeWidth + ColorWidth + SizeWidth + SokoWidth + KuWidth

    ''' <summary>入数セルの幅
    ''' </summary>
    Public Const IriSuWidth As Single = 12.0F

    ''' <summary>箱数セルの幅
    ''' </summary>
    Public Const HakoSuWidth As Single = 12.0F

    ''' <summary>数量等のカラムの幅
    ''' </summary>
    Public Const SuryoEtcWidth As Single = IriSuWidth + HakoSuWidth

    ''' <summary>数量セルの幅
    ''' </summary>
    Public Const SuryoWidth As Single = 19.0F

    ''' <summary>単位セルの幅
    ''' </summary>
    Public Const TaniWidth As Single = SuryoEtcWidth - SuryoWidth

    ''' <summary>単価等のカラムの幅
    ''' </summary>
    Public Const TankaEtcWidth As Single = 15.0F

    ''' <summary>金額等のカラムの幅
    ''' </summary>
    Public Const KingakuEtcWidth As Single = 15.0F

    ''' <summary>利益率等のカラムの幅
    ''' </summary>
    Public Const RiekiRitsuEtcWidth As Single = 15.0F

    ''' <summary>利益率等のカラムの幅
    ''' </summary>
    Public Const HyojunKakakuEtcWidth As Single = 15.0F

    ''' <summary>*(商管マークカラムの幅
    ''' </summary>
    Public Const KanMarkWidth As Single = 4.0F

    ''' <summary>数値1等のカラムの幅
    ''' </summary>
    ''' <remarks></remarks>
    Public Const YobiInt1EtcWidth As Single = 20.0F

End Class
﻿Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity
Imports System.ComponentModel
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports System.Xml
Imports PCA.TSC.Kon
Imports PCA.TSC.Kon.Tools

Public Class InputNYKController

  Inherits PCA.TSC.Kon.Tools.TscInputControllerBase

#Region "プロパティ"

  ''' <summary>伝区の初期値値を取得、設定。
  ''' </summary>
  ''' 
  Public Property DefaultDenku() As PCA.TSC.Kon.BusinessEntity.Defines.DenkuType = PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Kake

#End Region

  ''' <summary>コンストラクタ
  ''' </summary>
  Public Sub New(ByVal owner As IWin32Window)
    '
    '売上伝票入力用に基底クラスを実体化
    MyBase.New(owner, PEKonInput.DenpyoType.Uriage)

  End Sub

#Region "初期化"

  ''' <summary>コントローラクラスを初期化
  ''' </summary>
  Public Overrides Function Initialize(ByRef configXMLDoc As XmlDocument) As Boolean
    '
    'PCAクラウドAPIの準備、会社基本情報を読込み
    If MyBase.Initialize(configXMLDoc) = False Then
      Return False
    End If

    MyBase.MasterRms.NeedLoad = True 'Hien 12/22
    'マスターの先読込み（メモリ化）
    MyBase.MasterSms.NeedLoad = True        '商品
    MyBase.MasterSoko.NeedLoad = True       '倉庫
    MyBase.MasterBumon.NeedLoad = True      '部門
    MyBase.MasterTantosya.NeedLoad = True   '担当者
    MyBase.MasterProject.NeedLoad = True    'プロジェクト
    MyBase.MasterTekiyo.NeedLoad = True     '摘要
    MyBase.MasterTms.NeedLoad = True        '得意先
    MyBase.MasterYms.NeedLoad = True        '直送先
    MyBase.AreaUser.NeedLoad = True         '領域ユーザー
    If Not MyBase.LoadMaster() Then
      Return False
    End If

    '参照画面の表示項目の先読込み（メモリ化）
    MyBase.ReferSms.NeedLoad = True        '商品
    MyBase.ReferSoko.NeedLoad = True       '倉庫
    MyBase.ReferBumon.NeedLoad = True      '部門
    MyBase.ReferTantosya.NeedLoad = True   '担当者
    MyBase.ReferProject.NeedLoad = True    'プロジェクト
    MyBase.ReferTekiyo.NeedLoad = True     '摘要
    MyBase.ReferTms.NeedLoad = True        '得意先
    MyBase.ReferYms.NeedLoad = True        '直送先
    If Not MyBase.LoadReferItemsList() Then
      Return False
    End If

    '売上伝票のIDリストを初期化([検索]、[前移動]/[次移動]用)
    Me.ReferDenpyoList = New List(Of PCA.TSC.Kon.Tools.ReferItems)

    'デフォルト日付の設定
    Me.InitializeDefaultHizuke()

    '商魂のみの導入
    MyBase.IsOnlyKon = False

    Return True

  End Function


#End Region

#Region "売上伝票関連メソッド"

  ''' <summary>
  ''' 売上伝票のロック情報
  ''' </summary>
  ''' <remarks></remarks>
  Private m_BELockKonDenpyoNYK As BusinessEntity.BELockKon = Nothing


  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="beInputNYK"></param>
  ''' <param name="methodType"></param>
  ''' <param name="iApp"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CreateBEInputNYK(ByVal beInputNYK As BEInputNYK, methodType As PCA.TSC.Kon.BusinessEntity.Defines.IntegratedApplicationMethodType, iApp As IIntegratedApplication) As ResultSaveInputNYK

    Dim arrayOfBEInputNYK As ArrayOfBEInputNYK = New ArrayOfBEInputNYK
    arrayOfBEInputNYK.BEInputNYK.Add(beInputNYK)
    Dim saveParam As SaveParameter =
     New SaveParameter With {.TransactionScopeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem, .TransactionScope = Tools.Defines.TransactionScopeType.Whole, .CalcTax = False}
    Return SaveInputNYK(iApp, methodType, arrayOfBEInputNYK, saveParam)

  End Function

  Public Shared Function SaveInputNYK(ByRef iApplication As IIntegratedApplication, _
                                       ByVal methodType As BusinessEntity.Defines.IntegratedApplicationMethodType, _
                                       ByVal arrayOfBEInputNYK As BusinessEntity.ArrayOfBEInputNYK, _
                                       ByVal saveParameter As Tools.SaveParameter) As ResultSaveInputNYK
    '
    Dim entityString As String = String.Empty
    Dim inputNYKTool As New BusinessEntity.InputNYKTool
    Dim uriParameterString As String = String.Empty

    Dim iResult As IIntegratedResult
    '
    Dim resultSaveInputNYK As New ResultSaveInputNYK

    Try

      'URIパラメータを編集
      uriParameterString = EditSaveParameter(saveParameter)

      'ArrayOfBEToXmlString Hien add 

      Dim xmlDoc As New XmlDocument
      Dim xmlNodeArrayOfBEInputNYK As XmlNode
      Dim xmlNodeBEInputNYK As XmlNode

      If Not (arrayOfBEInputNYK Is Nothing) Then
        'ArrayOfBEInputSYK
        xmlNodeArrayOfBEInputNYK = xmlDoc.CreateElement(BusinessEntity.Defines.BusinessEntityNodePrefix & BusinessEntity.Defines.BusinessEntityPrefix & "InputNYK")

        For i As Integer = 0 To arrayOfBEInputNYK.BEInputNYK.Count - 1
          xmlNodeBEInputNYK = inputNYKTool.BEToXmlNode(xmlDoc, BusinessEntity.Defines.BusinessEntityPrefix & "InputNYK", arrayOfBEInputNYK.BEInputNYK(i), methodType)
          xmlNodeArrayOfBEInputNYK.AppendChild(xmlNodeBEInputNYK)
        Next

        xmlDoc.AppendChild(xmlNodeArrayOfBEInputNYK)
      End If

      entityString = xmlDoc.InnerXml

      If methodType = BusinessEntity.Defines.IntegratedApplicationMethodType.Create Then
        '登録
        iResult = iApplication.Create(inputNYKTool.URI & uriParameterString, entityString)
      Else
        '更新
        iResult = iApplication.Modify(inputNYKTool.URI & uriParameterString, entityString)
      End If

      'Hien add display error message
      If iResult.Status = IntegratedStatus.Failure Then
        MsgBox(iResult.ErrorMessage)
      End If

      '戻り値を編集 
      resultSaveInputNYK = ResultSaveToolNYK.IResultSaveInputNYKToClass(iResult)

    Catch ex As Exception
      ' そのまま例外をスロー
      Throw

    End Try

    Return resultSaveInputNYK

  End Function


  Public Shared Function EditSaveParameter(ByVal saveParameter As SaveParameter) As String
    '
    Dim questionUsed As Boolean = False
    Dim saveParameterString As String = String.Empty

    '業務エンティティタグ構造
    saveParameter.ElementUsing = BusinessEntity.Defines.UsingType.UsingOneItem
    saveParameter.CalcTotalUsing = BusinessEntity.Defines.UsingType.UsingOneItem
    saveParameter.CalcTaxUsing = BusinessEntity.Defines.UsingType.UsingOneItem
    saveParameter.CalcTotal = True
    saveParameter.CalcTax = True
    saveParameterString = AddURIParameter(saveParameterString, "Element", Tools.Defines.ElementType.ArrayElement, questionUsed)

    If saveParameter.TransactionScopeUsing = BusinessEntity.Defines.UsingType.UsingOneItem Then
      'トランザクション範囲
      saveParameterString = AddURIParameter(saveParameterString, "TransactionScope", saveParameter.TransactionScope, questionUsed)
    End If

    If saveParameter.CalcTotalUsing = BusinessEntity.Defines.UsingType.UsingOneItem Then
      '伝票合計自動計算
      saveParameterString = AddURIParameter(saveParameterString, "CalcTotal", saveParameter.CalcTotal.ToString, questionUsed)
    End If

    If saveParameter.CalcTaxUsing = BusinessEntity.Defines.UsingType.UsingOneItem Then
      '伝票消費税自動計算
      saveParameterString = AddURIParameter(saveParameterString, "CalcTax", saveParameter.CalcTax.ToString, questionUsed)
    End If

    If saveParameter.BaseTotalEntityUsing = BusinessEntity.Defines.UsingType.UsingOneItem Then
      '登録する合計金額と消費税の基となるエンティティ
      saveParameterString = AddURIParameter(saveParameterString, "BaseTotalEntity", saveParameter.BaseTotalEntity, questionUsed)
    End If
    saveParameterString = AddURIParameter(saveParameterString, "CalcDetailTax", True, questionUsed)

    Return saveParameterString

  End Function



  ''' <summary>uriパラメータ文字列に各パラメータクラスの値を追加。
  ''' </summary>
  Public Shared Function AddURIParameter(ByVal uriParameterString As String, ByVal valueName As String, ByVal value As Object, ByRef questionUsed As Boolean) As String
    '
    Dim questionChar As String = "?"
    Dim ampersandChar As String = "&"
    Dim equalChar As String = "="

    Dim returnURIParameterString As String = Trim(uriParameterString)

    '接続文字を編集
    If questionUsed = False Then
      '?が未設定の時、?を付与
      returnURIParameterString = returnURIParameterString & questionChar
      questionUsed = True
    ElseIf returnURIParameterString.Length <> 0 Then
      'URIパラメータが設定済の時、&を付与
      returnURIParameterString = returnURIParameterString & ampersandChar
    End If

    'URIパラメータに値を付与
    If TypeOf value Is String Then
      '文字列
      returnURIParameterString = returnURIParameterString & Trim(valueName) & equalChar & Trim(value.ToString)
    Else
      'Boolean、Short、Integerの整数型 ※enum型も有り得る為にToStringは使用しない
      returnURIParameterString = returnURIParameterString & Trim(valueName) & equalChar & CStr(value)
    End If

    Return returnURIParameterString

  End Function

#End Region


#Region "エンティティ操作"

  ''' <summary>
  ''' エンティティを初期化します。
  ''' </summary>
  ''' <returns>初期化されたエンティティ</returns>
  ''' <remarks></remarks>
  Public Function CreateInitialEntity() As PEKonInput
    '
    Dim beInputNYK As New BEInputNYK()
    Dim peKonInput As PEKonInput = ConvertKonInput.NYKToInput(beInputNYK, Nothing, Nothing)

    ' 条件により初期値が変わる項目はここでセットする
    peKonInput.Header.DenpyoHizuke = Me.DefaultHizuke
    peKonInput.Header.Hizuke = Me.DefaultHizuke
    peKonInput.Header.Denku = DefaultDenku      ' 伝区

    Return peKonInput

  End Function

  ''' <summary>
  ''' データの調整を行います。
  ''' </summary>
  ''' <param name="beInputNYK">調整対象の伝票データ</param>
  ''' <remarks></remarks>
  Public Sub AdjustDenpyo(ByVal beInputNYK As BEInputNYK)

    If beInputNYK.InputNYKH.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Kake Then
      '請求日をクリア
      beInputNYK.InputNYKH.Shirebi = 0 'hien 12/24
    End If

    Dim shiresaki As BEMasterRms = MyBase.FindBEMasterRms(beInputNYK.InputNYKH.ShiresakiCode)
    Dim shiharaisaki As BEMasterRms = MyBase.FindBEMasterRms(shiresaki.ShiharaisakiCode) 'Hien test 12/24

    Dim needCalcZei As Boolean = False
    Dim needCalcTotal As Boolean = False
    Dim addEntries As New List(Of BEInputNYKD)

    For Each beInputNYKD As BEInputNYKD In beInputNYK.InputNYKDList.BEInputNYKD
      needCalcZei = False
      '金額、原価、売価金額の小数桁以下を丸める
      If KonCalc.CountSyosuKeta(beInputNYKD.Kingaku) > 0 Then
        beInputNYKD.Kingaku = KonCalc.Round(beInputNYKD.Kingaku, 0, beInputNYK.InputNYKH.KingakuHasu)
        needCalcZei = True
      End If
      If KonCalc.CountSyosuKeta(beInputNYKD.UchiZeigaku) > 0 Then
        beInputNYKD.UchiZeigaku = KonCalc.Round(beInputNYKD.UchiZeigaku, 0, beInputNYK.InputNYKH.SyohizeiHasu)
        needCalcZei = True
      End If
      If KonCalc.CountSyosuKeta(beInputNYKD.SotoZeigaku) > 0 Then
        beInputNYKD.SotoZeigaku = KonCalc.Round(beInputNYKD.SotoZeigaku, 0, beInputNYK.InputNYKH.SyohizeiHasu)
        needCalcZei = True
      End If

      If needCalcZei Then
        '各金額に変更があったら明細を再計算
        Dim sotoZeigaku As Decimal = beInputNYKD.SotoZeigaku
        Dim uchiZeigaku As Decimal = beInputNYKD.UchiZeigaku
        '消費税額
        KonCalc.CalcMeisaiZei(beInputNYK.InputNYKH.SyohizeiHasu, beInputNYK.InputNYKH.SyohizeiTsuchi, beInputNYKD.ZeiRitsu, beInputNYKD.ZeikomiKubun, beInputNYKD.Kingaku, _
                              sotoZeigaku, uchiZeigaku, _BEKihonJoho.ZeiHasuKurai)
        beInputNYKD.SotoZeigaku = sotoZeigaku
        beInputNYKD.UchiZeigaku = uchiZeigaku

        '合計も再計算
        needCalcTotal = True
      End If


      Dim meisaiState As PEKonInput.MeisaiStateType _
          = KonCalc.JudgeNykdState(beInputNYKD.SyohinCode, beInputNYKD.HachuHeaderId, beInputNYKD.NyukaShiji, beInputNYKD.MasterKubun, beInputNYKD.Suryo)
      If meisaiState = PEKonInput.MeisaiStateType.Valid _
      OrElse meisaiState = PEKonInput.MeisaiStateType.RendoInvalid Then
        '有効明細と受注からの未出荷明細のみ追加
        addEntries.Add(beInputNYKD)
      End If

    Next

    beInputNYK.InputNYKDList.BEInputNYKD.Clear()
    beInputNYK.InputNYKDList.BEInputNYKD.AddRange(CType(addEntries, Global.System.Collections.Generic.IEnumerable(Of Global.PCA.TSC.Kon.BusinessEntity.BEInputNYKD)))

    If needCalcTotal Then
      '必要がある時、合計を再計算

      Dim curTotal As New PEKonInput()

      '再計算した明細のある伝票エンティティをクローン化
      curTotal = ConvertKonInput.NYKToInput(CType(beInputNYK.Clone(), BusinessEntity.BEInputNYK), shiresaki, shiharaisaki)

      Me.CalcTotal(curTotal, False, Tools.PEKonInput.DenpyoType.Shire)

      beInputNYK.InputNYKT.KingakuGokei = curTotal.Total.KingakuGokei
      beInputNYK.InputNYKT.SotoZeiGokei = curTotal.Total.SotoZeiGokei
      beInputNYK.InputNYKT.UchiZeiGokei = curTotal.Total.UchiZeiGokei
      beInputNYK.InputNYKT.Hasu = curTotal.Total.Hasu

      '税率別合計
      Dim oldInputNYKZList As New List(Of BEInputSykNykZ)
      oldInputNYKZList.AddRange(CType(beInputNYK.InputNYKZList.BEInputNYKZ, System.Collections.Generic.IEnumerable(Of PCA.TSC.Kon.BusinessEntity.BEInputSykNykZ)))

      Dim addInputNYK As New BEInputNYK()
      addInputNYK = ConvertKonInput.InputToNYK(curTotal)

      beInputNYK.InputNYKZList.BEInputNYKZ.Clear()
      beInputNYK.InputNYKZList.BEInputNYKZ.AddRange(CType(addInputNYK.InputNYKZList.BEInputNYKZ, System.Collections.Generic.IEnumerable(Of BEInputSykNykZ)))

      For Each old As BEInputSykNykZ In oldInputNYKZList
        Dim oldInputSYKZ As BEInputSykNykZ = old    '警告回避

        If addInputNYK.InputNYKZList.BEInputNYKZ.Exists(Function(match) _
            match.ZeiRitsu = oldInputSYKZ.ZeiRitsu) = False AndAlso (old.Hosei <> 0 OrElse old.HoseiHizuke <> 0) Then
          '税率の適用が無くなっても、補正済は旧伝票から移す
          old.KingakuGokei = 0
          old.SotoZeiGokei = 0
          old.UchiZeiGokei = 0
          old.Hasu = 0

          beInputNYK.InputNYKZList.BEInputNYKZ.Add(old)

        End If
      Next
    End If

  End Sub

#End Region

#Region "ツール"
  ''' <summary>原価割れ、利益率割れ、与信限度額、在庫割れの各チェックと続行確認を実施。
  ''' （補足）
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="checkType">True=登録禁止、False=続行確認</param>
  Public Function CheckUriageAndPrompt(ByVal peKonInput As PEKonInput, ByVal checkType As Boolean) As Boolean
    '
    '原価割れ、利益率割れのチェック、通知は常に行う
    '(チェックをする/しない/登録更新できないは次バージョン以降対応予定)

    '原価割れをチェック
    If MyBase.CheckGenkawareAndPrompt(peKonInput.DetailList) = False Then
      Return False
    End If

    '利益率割れをチェック
    If MyBase.CheckSaiteiRiekiritsuAndPrompt(peKonInput, peKonInput.DenpyoType.Uriage) = False Then
      Return False
    End If

    If peKonInput.Header.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Keiyaku _
    AndAlso ((MyBase.BEKihonJoho.ZaikowareCheckUriage = BusinessEntity.Defines.ZaikowareCheckType.Cant AndAlso checkType = True) _
      OrElse (MyBase.BEKihonJoho.ZaikowareCheckUriage = BusinessEntity.Defines.ZaikowareCheckType.Check AndAlso checkType = False)) Then
      '在庫割れをチェック
      If MyBase.CheckZaikowareAndPrompt(peKonInput.DetailList, peKonInput.DenpyoType.Uriage) = False Then
        Return False
      End If
    End If

    Return True

  End Function

#End Region

End Class


﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmList
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmList))
    Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
    Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.ToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSearch = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandExecute = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSelectAll = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandUnselectAll = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemExecute = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSelectAll = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemUnselectAll = New PCA.Controls.PcaCommandItem()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.ldtWhDate = New PCA.Controls.PcaLabeledDate()
    Me.chkIsInclude = New PCA.Controls.PcaCheckBox()
    Me.dgv = New System.Windows.Forms.DataGridView()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("メイリオ", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(1176, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'ToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemFile, Nothing)
    Me.ToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemClose})
    Me.ToolStripMenuItemFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemFile.Name = "ToolStripMenuItemFile"
    Me.ToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.ToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSearch})
    Me.ToolStripMenuItemEdit.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSearch.Text = "再表示(&R)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemExecute)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(130, 22)
    Me.ToolStripMenuItemContent.Text = "目次（&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("メイリオ", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonSearch, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1176, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSearch, Me.PcaCommandItemSearch)
    Me.ToolStripButtonSearch.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonSearch.Image = CType(resources.GetObject("ToolStripButtonSearch.Image"), System.Drawing.Image)
    Me.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSearch.Name = "ToolStripButtonSearch"
    Me.ToolStripButtonSearch.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonSearch.Text = "再表示"
    Me.ToolStripButtonSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Nothing)
    Me.ToolStripButtonHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"), System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 832)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1176, 22)
    Me.StatusStrip1.TabIndex = 4
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandSearch, Me.PcaFunctionCommandClose, Me.PcaFunctionCommandExecute, Me.PcaFunctionCommandSelectAll, Me.PcaFunctionCommandUnselectAll})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 804)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1176, 28)
    Me.PcaFunctionBar1.TabIndex = 5
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    '
    'PcaFunctionCommandSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSearch, Me.PcaCommandItemSearch)
    Me.PcaFunctionCommandSearch.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandSearch.Tag = Nothing
    Me.PcaFunctionCommandSearch.Text = "再表示"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'PcaFunctionCommandExecute
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandExecute, Me.PcaCommandItemExecute)
    Me.PcaFunctionCommandExecute.FunctionKey = PCA.Controls.FunctionKey.F11
    Me.PcaFunctionCommandExecute.Tag = Nothing
    Me.PcaFunctionCommandExecute.Text = "振替実行"
    '
    'PcaFunctionCommandSelectAll
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSelectAll, Me.PcaCommandItemSelectAll)
    Me.PcaFunctionCommandSelectAll.FunctionKey = PCA.Controls.FunctionKey.F3
    Me.PcaFunctionCommandSelectAll.Tag = Nothing
    Me.PcaFunctionCommandSelectAll.Text = "全選択"
    '
    'PcaFunctionCommandUnselectAll
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandUnselectAll, Me.PcaCommandItemUnselectAll)
    Me.PcaFunctionCommandUnselectAll.FunctionKey = PCA.Controls.FunctionKey.F4
    Me.PcaFunctionCommandUnselectAll.Tag = Nothing
    Me.PcaFunctionCommandUnselectAll.Text = "全解除"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemExecute, Me.PcaCommandItemSearch, Me.PcaCommandItemClose, Me.PcaCommandItemHelp, Me.PcaCommandItemSelectAll, Me.PcaCommandItemUnselectAll})
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    Me.PcaCommandItemClose.CommandName = "CommandClose"
    '
    'PcaCommandItemSearch
    '
    Me.PcaCommandItemSearch.CommandId = 5
    Me.PcaCommandItemSearch.CommandName = "CommandSearch"
    '
    'PcaCommandItemExecute
    '
    Me.PcaCommandItemExecute.CommandId = 11
    Me.PcaCommandItemExecute.CommandName = "CommandHelp"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    '
    'PcaCommandItemSelectAll
    '
    Me.PcaCommandItemSelectAll.CommandId = 3
    '
    'PcaCommandItemUnselectAll
    '
    Me.PcaCommandItemUnselectAll.CommandId = 4
    '
    'ldtWhDate
    '
    Me.ldtWhDate.ClientProcess = Nothing
    Me.ldtWhDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtWhDate.Holidays = Nothing
    Me.ldtWhDate.LabelText = "入荷日"
    Me.ldtWhDate.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtWhDate.Location = New System.Drawing.Point(15, 85)
    Me.ldtWhDate.MaxLabelLength = 10
    Me.ldtWhDate.Name = "ldtWhDate"
    Me.ldtWhDate.Size = New System.Drawing.Size(211, 22)
    Me.ldtWhDate.TabIndex = 7
    '
    'chkIsInclude
    '
    Me.chkIsInclude.AutoTextSize = False
    Me.chkIsInclude.Checked = True
    Me.chkIsInclude.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chkIsInclude.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.chkIsInclude.Location = New System.Drawing.Point(240, 85)
    Me.chkIsInclude.Name = "chkIsInclude"
    Me.chkIsInclude.Size = New System.Drawing.Size(280, 22)
    Me.chkIsInclude.TabIndex = 8
    Me.chkIsInclude.Text = "入荷日以前で未振替のデータを含む"
    Me.chkIsInclude.UseVisualStyleBackColor = True
    '
    'dgv
    '
    Me.dgv.AllowUserToAddRows = False
    Me.dgv.AllowUserToDeleteRows = False
    Me.dgv.AllowUserToResizeRows = False
    Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dgv.BackgroundColor = System.Drawing.SystemColors.Control
    Me.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
    DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
    DataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue
    DataGridViewCellStyle1.Font = New System.Drawing.Font("MS UI Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
    DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control
    DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
    DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
    Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
    Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
    DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
    DataGridViewCellStyle2.Font = New System.Drawing.Font("MS UI Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
    DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window
    DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
    DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
    Me.dgv.DefaultCellStyle = DataGridViewCellStyle2
    Me.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
    Me.dgv.EnableHeadersVisualStyles = False
    Me.dgv.Location = New System.Drawing.Point(15, 133)
    Me.dgv.Name = "dgv"
    Me.dgv.RowHeadersVisible = False
    Me.dgv.RowTemplate.Height = 21
    Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
    Me.dgv.Size = New System.Drawing.Size(1131, 645)
    Me.dgv.TabIndex = 10
    '
    'frmList
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1176, 854)
    Me.Controls.Add(Me.dgv)
    Me.Controls.Add(Me.chkIsInclude)
    Me.Controls.Add(Me.ldtWhDate)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("MS UI Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1192, 892)
    Me.Name = "frmList"
    Me.Text = " 入荷振替"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents ToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonSearch As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemExecute As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandExecute As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ldtWhDate As PCA.Controls.PcaLabeledDate
  Friend WithEvents chkIsInclude As PCA.Controls.PcaCheckBox
  Friend WithEvents dgv As System.Windows.Forms.DataGridView
  Friend WithEvents PcaCommandItemSelectAll As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemUnselectAll As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandSelectAll As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandUnselectAll As PCA.Controls.PcaFunctionCommand

End Class

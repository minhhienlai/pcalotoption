﻿Imports PCA.TSC.Kon
Imports PCA.TSC.Kon.Tools
Public Class ResultSaveToolNYK

  '''<summary>XMLから項目定義クラスへ変換</summary>
  Public Shared Function IResultSaveInputNYKToClass(ByVal iResult As PCA.ApplicationIntegration.IIntegratedResult) As ResultSaveInputNYK
    '
    Dim xmlDoc As New System.Xml.XmlDocument
    Dim xmlNodeList As System.Xml.XmlNodeList
    Dim xmlNode As System.Xml.XmlNode

    Dim inputNYKTool As New BusinessEntity.InputNYKTool

    Dim beKonIntegratedStatusResult As New BEKonIntegratedStatusResult
    'Dim beKonIntegrationResultOfBEInputSYK As New List(Of ResultSaveInputSYK.ArrayOfBEKonIntegrationResultOfBEInputSYKBEKonIntegrationResultOfBEInputSYK.ArrayOfBEKonIntegrationResultOfBEInputSYKBEKonIntegrationResultOfBEInputSYKBEKonIntegrationResultOfBEInputSYK)
    Dim beKonIntegrationResultOfBEInputNYK As New ResultSaveInputNYK.ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK.ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK
    'Dim arrayOfBEKonIntegrationResultOfBEInputSYK As New List(Of BEResultOfInputSYK.BEKonIntegrationResultOfBEInputSYK)
    Dim resultSaveInputNYK As New ResultSaveInputNYK

    resultSaveInputNYK.Status = iResult.Status
    resultSaveInputNYK.ErrorCode = iResult.ErrorCode
    resultSaveInputNYK.ErrorMessage = iResult.ErrorMessage

    xmlDoc.LoadXml(iResult.BusinessValue)
    xmlNodeList = xmlDoc.SelectNodes("ArrayOfBEKonIntegrationResultOfBEInputNYK/BEKonIntegrationResultOfBEInputNYK")

    For i As Integer = 0 To xmlNodeList.Count - 1
      xmlNode = xmlNodeList(i)

      beKonIntegratedStatusResult = New BEKonIntegratedStatusResult
      beKonIntegratedStatusResult = KonIntegratedStatusResultTool.XmlNodeToBE(xmlNode)

      beKonIntegrationResultOfBEInputNYK = New ResultSaveInputNYK.ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK.ArrayOfBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYKBEKonIntegrationResultOfBEInputNYK
      beKonIntegrationResultOfBEInputNYK.ErrorCode = beKonIntegratedStatusResult.ErrorCode
      beKonIntegrationResultOfBEInputNYK.ErrorMessage = beKonIntegratedStatusResult.ErrorMessage
      beKonIntegrationResultOfBEInputNYK.ErrorLevel = beKonIntegratedStatusResult.ErrorLevel
      beKonIntegrationResultOfBEInputNYK.InnerStatus = beKonIntegratedStatusResult.InnerStatus

      beKonIntegrationResultOfBEInputNYK.Target = New BusinessEntity.BEInputNYK
      beKonIntegrationResultOfBEInputNYK.Target = BusinessEntity.InputNYKTool.XmlNodeToBE(xmlNode.SelectSingleNode("Target"))
      '
      resultSaveInputNYK.ArrayOfBEKonIntegrationResultOfBEInputNYK.BEKonIntegrationResultOfBEInputNYK.Add(beKonIntegrationResultOfBEInputNYK)
    Next

    Return resultSaveInputNYK

  End Function

End Class
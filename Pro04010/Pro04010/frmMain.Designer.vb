﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
    Me.ServiceController1 = New System.ServiceProcess.ServiceController()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.FToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSave = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemNew = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemReset = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPrevious = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemNext = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRefer = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemDelete = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemCopy = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSave = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonCreate = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonReset = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPrevious = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonNext = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSearch = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonDelete = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonCopy = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.HeaderLabel = New PCA.TSC.Kon.Tools.TscInputHeaderLabel()
    Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
    Me.ldtAdjustmentDate = New Sunloft.PCAControls.SLPcaLabeledDate()
    Me.ltxtSlipNo2 = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.setProduct = New PCA.Controls.PcaCodeSet()
    Me.setWarehouse = New PCA.Controls.PcaCodeSet()
    Me.setReasonCode = New PCA.Controls.PcaCodeSet()
    Me.setReasonContent = New PCA.Controls.PcaCodeSet()
    Me.ltxtCreatedBy = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtCreatedAt = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtModifiedBy = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtModifiedAt = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrevious = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandNext = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSlipSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandCopy = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSave = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandStock = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemSave = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemNew = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemReset = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPrevious = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemNext = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSlipSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemDelete = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemCopy = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemStock = New PCA.Controls.PcaCommandItem()
    Me.ltxtSlipNo = New PCA.Controls.PcaLabeledNumberBox()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.rdoKbn = New PCA.Controls.PcaMultiRadioButton()
    Me.ltxtLotNo = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtStockNumber = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtStockQuantity = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.lblStock = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.lblAdjustment = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.numAdjustNumber = New PCA.Controls.PcaNumberBox()
    Me.numAdjustQuantity = New PCA.Controls.PcaNumberBox()
    Me.lblUnitName = New PCA.Controls.PcaLabel()
    Me.rdoKbnNotSelect = New PCA.Controls.PcaMultiRadioButton()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'FToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.FToolStripMenuItemFile, Nothing)
    Me.FToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSave, Me.ToolStripMenuItemClose})
    Me.FToolStripMenuItemFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.FToolStripMenuItemFile.Name = "FToolStripMenuItemFile"
    Me.FToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.FToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSave, Me.PcaCommandItemSave)
    Me.ToolStripMenuItemSave.Name = "ToolStripMenuItemSave"
    Me.ToolStripMenuItemSave.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSave.Text = "登録(&S)"
    Me.ToolStripMenuItemSave.ToolTipText = "伝票を登録します。"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    Me.ToolStripMenuItemClose.ToolTipText = "プログラムを終了します。"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemNew, Me.ToolStripMenuItemReset, Me.ToolStripMenuItemPrevious, Me.ToolStripMenuItemNext, Me.ToolStripMenuItemSearch, Me.ToolStripMenuItemRefer, Me.ToolStripMenuItemDelete, Me.ToolStripMenuItemCopy})
    Me.ToolStripMenuItemEdit.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemNew
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemNew, Me.PcaCommandItemNew)
    Me.ToolStripMenuItemNew.Name = "ToolStripMenuItemNew"
    Me.ToolStripMenuItemNew.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemNew.Text = "新規入力(&N)"
    Me.ToolStripMenuItemNew.ToolTipText = "伝票の新規作成を行います。"
    '
    'ToolStripMenuItemReset
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemReset, Me.PcaCommandItemReset)
    Me.ToolStripMenuItemReset.Name = "ToolStripMenuItemReset"
    Me.ToolStripMenuItemReset.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemReset.Text = "入力前に戻る(&O)"
    Me.ToolStripMenuItemReset.ToolTipText = "伝票を入力前の状態に戻します。"
    '
    'ToolStripMenuItemPrevious
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPrevious, Me.PcaCommandItemPrevious)
    Me.ToolStripMenuItemPrevious.Name = "ToolStripMenuItemPrevious"
    Me.ToolStripMenuItemPrevious.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemPrevious.Text = "前伝票(&B)"
    Me.ToolStripMenuItemPrevious.ToolTipText = "前の伝票を表示します。"
    '
    'ToolStripMenuItemNext
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemNext, Me.PcaCommandItemNext)
    Me.ToolStripMenuItemNext.Name = "ToolStripMenuItemNext"
    Me.ToolStripMenuItemNext.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemNext.Text = "次伝票(&F)"
    Me.ToolStripMenuItemNext.ToolTipText = "次の伝票を表示します。"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSlipSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemSearch.Text = "検索(&S)"
    Me.ToolStripMenuItemSearch.ToolTipText = "伝票を検索します。"
    '
    'ToolStripMenuItemRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRefer, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemRefer.Name = "ToolStripMenuItemRefer"
    Me.ToolStripMenuItemRefer.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemRefer.Text = "参照(&U)"
    Me.ToolStripMenuItemRefer.ToolTipText = "マスタの検索を行います。"
    '
    'ToolStripMenuItemDelete
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemDelete, Me.PcaCommandItemDelete)
    Me.ToolStripMenuItemDelete.Name = "ToolStripMenuItemDelete"
    Me.ToolStripMenuItemDelete.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemDelete.Text = "伝票削除(&X)"
    Me.ToolStripMenuItemDelete.ToolTipText = "伝票の削除を行います。"
    '
    'ToolStripMenuItemCopy
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemCopy, Me.PcaCommandItemCopy)
    Me.ToolStripMenuItemCopy.Name = "ToolStripMenuItemCopy"
    Me.ToolStripMenuItemCopy.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemCopy.Text = "伝票複写(&Y)"
    Me.ToolStripMenuItemCopy.ToolTipText = "伝票の複写を行います。"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContent.Text = "目次(&C)"
    Me.ToolStripMenuItemContent.ToolTipText = "ヘルプを表示します。"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonSave, Me.ToolStripButtonCreate, Me.ToolStripButtonReset, Me.ToolStripButtonPrevious, Me.ToolStripButtonNext, Me.ToolStripButtonSearch, Me.ToolStripButtonDelete, Me.ToolStripButtonCopy, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1008, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonClose.ToolTipText = "プログラムを終了します。"
    '
    'ToolStripButtonSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSave, Me.PcaCommandItemSave)
    Me.ToolStripButtonSave.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonSave.Image = CType(resources.GetObject("ToolStripButtonSave.Image"), System.Drawing.Image)
    Me.ToolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSave.Name = "ToolStripButtonSave"
    Me.ToolStripButtonSave.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonSave.Text = "登録"
    Me.ToolStripButtonSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonSave.ToolTipText = "伝票を登録します。"
    '
    'ToolStripButtonCreate
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonCreate, Me.PcaCommandItemNew)
    Me.ToolStripButtonCreate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonCreate.Image = CType(resources.GetObject("ToolStripButtonCreate.Image"), System.Drawing.Image)
    Me.ToolStripButtonCreate.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonCreate.Name = "ToolStripButtonCreate"
    Me.ToolStripButtonCreate.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonCreate.Text = "新規"
    Me.ToolStripButtonCreate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonCreate.ToolTipText = "伝票を新規で作成します。"
    '
    'ToolStripButtonReset
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonReset, Me.PcaCommandItemReset)
    Me.ToolStripButtonReset.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonReset.Image = CType(resources.GetObject("ToolStripButtonReset.Image"), System.Drawing.Image)
    Me.ToolStripButtonReset.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonReset.Name = "ToolStripButtonReset"
    Me.ToolStripButtonReset.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonReset.Text = "入力前"
    Me.ToolStripButtonReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonReset.ToolTipText = "入力前の状態に戻します。"
    '
    'ToolStripButtonPrevious
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPrevious, Me.PcaCommandItemPrevious)
    Me.ToolStripButtonPrevious.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonPrevious.Image = CType(resources.GetObject("ToolStripButtonPrevious.Image"), System.Drawing.Image)
    Me.ToolStripButtonPrevious.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPrevious.Name = "ToolStripButtonPrevious"
    Me.ToolStripButtonPrevious.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonPrevious.Text = "前伝票"
    Me.ToolStripButtonPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonPrevious.ToolTipText = "前伝票を表示します。"
    '
    'ToolStripButtonNext
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonNext, Me.PcaCommandItemNext)
    Me.ToolStripButtonNext.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonNext.Image = CType(resources.GetObject("ToolStripButtonNext.Image"), System.Drawing.Image)
    Me.ToolStripButtonNext.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonNext.Name = "ToolStripButtonNext"
    Me.ToolStripButtonNext.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonNext.Text = "次伝票"
    Me.ToolStripButtonNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonNext.ToolTipText = "次伝票を表示します。"
    '
    'ToolStripButtonSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSearch, Me.PcaCommandItemSlipSearch)
    Me.ToolStripButtonSearch.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonSearch.Image = Global.Pro04010.My.Resources.Resources.Search
    Me.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSearch.Name = "ToolStripButtonSearch"
    Me.ToolStripButtonSearch.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonSearch.Text = "検索"
    Me.ToolStripButtonSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonDelete
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonDelete, Me.PcaCommandItemDelete)
    Me.ToolStripButtonDelete.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonDelete.Image = CType(resources.GetObject("ToolStripButtonDelete.Image"), System.Drawing.Image)
    Me.ToolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonDelete.Name = "ToolStripButtonDelete"
    Me.ToolStripButtonDelete.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonDelete.Text = "削除"
    Me.ToolStripButtonDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonDelete.ToolTipText = "伝票を削除します。"
    '
    'ToolStripButtonCopy
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonCopy, Me.PcaCommandItemCopy)
    Me.ToolStripButtonCopy.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonCopy.Image = CType(resources.GetObject("ToolStripButtonCopy.Image"), System.Drawing.Image)
    Me.ToolStripButtonCopy.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonCopy.Name = "ToolStripButtonCopy"
    Me.ToolStripButtonCopy.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonCopy.Text = "複写"
    Me.ToolStripButtonCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonCopy.ToolTipText = "伝票を複写します。"
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"), System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonHelp.ToolTipText = "ヘルプを表示します。"
    '
    'HeaderLabel
    '
    Me.HeaderLabel.BackColor = System.Drawing.SystemColors.Control
    Me.HeaderLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.HeaderLabel.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Bold)
    Me.HeaderLabel.FusenFlag = PCA.TSC.Kon.BusinessEntity.Defines.DenpyoFusenCommentType.Nashi
    Me.HeaderLabel.FusenText = ""
    Me.HeaderLabel.Location = New System.Drawing.Point(0, 61)
    Me.HeaderLabel.Name = "HeaderLabel"
    Me.HeaderLabel.ShowFusenIcon = True
    Me.HeaderLabel.ShowSyoninIcon = True
    Me.HeaderLabel.Size = New System.Drawing.Size(1008, 25)
    Me.HeaderLabel.State = PCA.Controls.LabelStateType.[New]
    Me.HeaderLabel.SyoninFlag = PCA.TSC.Kon.BusinessEntity.Defines.DenpyoSyoninFlagType.Misyonin
    Me.HeaderLabel.SyoninText = ""
    Me.HeaderLabel.TabIndex = 2
    Me.HeaderLabel.TabStop = False
    '
    'ldtAdjustmentDate
    '
    Me.ldtAdjustmentDate.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.ldtAdjustmentDate.ClientProcess = Nothing
    Me.ldtAdjustmentDate.DisplaySlash = True
    Me.ldtAdjustmentDate.EmptyIfDisabled = True
    Me.ldtAdjustmentDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ldtAdjustmentDate.Holidays = Nothing
    Me.ldtAdjustmentDate.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.ldtAdjustmentDate.LabelText = "調整日付"
    Me.ldtAdjustmentDate.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtAdjustmentDate.LabelTopAlign = True
    Me.ldtAdjustmentDate.Location = New System.Drawing.Point(12, 86)
    Me.ldtAdjustmentDate.MaxLabelLength = 15
    Me.ldtAdjustmentDate.MaxTextLength = 15
    Me.ldtAdjustmentDate.MustInput = False
    Me.ldtAdjustmentDate.Name = "ldtAdjustmentDate"
    Me.ldtAdjustmentDate.Size = New System.Drawing.Size(105, 44)
    Me.ldtAdjustmentDate.TabIndex = 0
    '
    'ltxtSlipNo2
    '
    Me.ltxtSlipNo2.AllowTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.ltxtSlipNo2.AutoTextSize = False
    Me.ltxtSlipNo2.ClientProcess = Nothing
    Me.ltxtSlipNo2.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtSlipNo2.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.ltxtSlipNo2.LabelText = "伝票No2"
    Me.ltxtSlipNo2.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtSlipNo2.LabelTopAlign = True
    Me.ltxtSlipNo2.Location = New System.Drawing.Point(243, 86)
    Me.ltxtSlipNo2.Margin = New System.Windows.Forms.Padding(21, 0, 0, 0)
    Me.ltxtSlipNo2.MaxLabelLength = 32
    Me.ltxtSlipNo2.MaxLength = 32
    Me.ltxtSlipNo2.MaxTextLength = 32
    Me.ltxtSlipNo2.MustInput = False
    Me.ltxtSlipNo2.Name = "ltxtSlipNo2"
    Me.ltxtSlipNo2.Size = New System.Drawing.Size(120, 44)
    Me.ltxtSlipNo2.TabIndex = 2
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 708)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1008, 22)
    Me.StatusStrip1.TabIndex = 25
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'setProduct
    '
    Me.setProduct.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setProduct.AutoRestore = True
    Me.setProduct.AutoTextSize = False
    Me.setProduct.AutoTopMargin = False
    Me.setProduct.ClientProcess = Nothing
    Me.setProduct.F8EventSupport = False
    Me.setProduct.FillZeroFlag = True
    Me.setProduct.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setProduct.GroupBoxVisible = False
    Me.setProduct.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setProduct.Location = New System.Drawing.Point(12, 172)
    Me.setProduct.MaxCodeLength = 18
    Me.setProduct.MaxLabelLength = 15
    Me.setProduct.MaxNameLength = 33
    Me.setProduct.Name = "setProduct"
    Me.setProduct.Size = New System.Drawing.Size(515, 21)
    Me.setProduct.TabIndex = 4
    Me.setProduct.Text = "商品"
    Me.setProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setWarehouse
    '
    Me.setWarehouse.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setWarehouse.AutoRestore = True
    Me.setWarehouse.AutoTextSize = False
    Me.setWarehouse.AutoTopMargin = False
    Me.setWarehouse.ClientProcess = Nothing
    Me.setWarehouse.F8EventSupport = False
    Me.setWarehouse.FillZeroFlag = True
    Me.setWarehouse.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setWarehouse.GroupBoxVisible = False
    Me.setWarehouse.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setWarehouse.Location = New System.Drawing.Point(12, 194)
    Me.setWarehouse.MaxCodeLength = 18
    Me.setWarehouse.MaxLabelLength = 15
    Me.setWarehouse.MaxNameLength = 33
    Me.setWarehouse.Name = "setWarehouse"
    Me.setWarehouse.Size = New System.Drawing.Size(528, 22)
    Me.setWarehouse.TabIndex = 5
    Me.setWarehouse.Text = "倉庫"
    Me.setWarehouse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setReasonCode
    '
    Me.setReasonCode.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setReasonCode.AutoRestore = True
    Me.setReasonCode.AutoTextSize = False
    Me.setReasonCode.AutoTopMargin = False
    Me.setReasonCode.ClientProcess = Nothing
    Me.setReasonCode.F8EventSupport = False
    Me.setReasonCode.FillZeroFlag = True
    Me.setReasonCode.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setReasonCode.GroupBoxVisible = False
    Me.setReasonCode.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setReasonCode.Location = New System.Drawing.Point(12, 395)
    Me.setReasonCode.MaxCodeLength = 18
    Me.setReasonCode.MaxLabelLength = 15
    Me.setReasonCode.MaxNameLength = 0
    Me.setReasonCode.Name = "setReasonCode"
    Me.setReasonCode.Size = New System.Drawing.Size(252, 22)
    Me.setReasonCode.TabIndex = 8
    Me.setReasonCode.Text = "調整理由"
    Me.setReasonCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'setReasonContent
    '
    Me.setReasonContent.AutoTextSize = False
    Me.setReasonContent.AutoTopMargin = False
    Me.setReasonContent.BackColor = System.Drawing.SystemColors.Control
    Me.setReasonContent.ClientProcess = Nothing
    Me.setReasonContent.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setReasonContent.GroupBoxVisible = False
    Me.setReasonContent.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setReasonContent.LimitLength = 30
    Me.setReasonContent.Location = New System.Drawing.Point(261, 395)
    Me.setReasonContent.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setReasonContent.MaxCodeLength = 33
    Me.setReasonContent.MaxLabelLength = 0
    Me.setReasonContent.MaxNameLength = 0
    Me.setReasonContent.Name = "setReasonContent"
    Me.setReasonContent.ReferButtonVisible = False
    Me.setReasonContent.Size = New System.Drawing.Size(266, 22)
    Me.setReasonContent.TabIndex = 9
    Me.setReasonContent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'ltxtCreatedBy
    '
    Me.ltxtCreatedBy.AutoTextSize = False
    Me.ltxtCreatedBy.ClientProcess = Nothing
    Me.ltxtCreatedBy.Enabled = False
    Me.ltxtCreatedBy.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtCreatedBy.LabelText = "登録者"
    Me.ltxtCreatedBy.Location = New System.Drawing.Point(555, 150)
    Me.ltxtCreatedBy.MaxLabelLength = 15
    Me.ltxtCreatedBy.MaxTextLength = 25
    Me.ltxtCreatedBy.MustInput = False
    Me.ltxtCreatedBy.Name = "ltxtCreatedBy"
    Me.ltxtCreatedBy.ReadOnly = True
    Me.ltxtCreatedBy.Size = New System.Drawing.Size(280, 21)
    Me.ltxtCreatedBy.TabIndex = 42
    Me.ltxtCreatedBy.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtCreatedAt
    '
    Me.ltxtCreatedAt.AutoTextSize = False
    Me.ltxtCreatedAt.ClientProcess = Nothing
    Me.ltxtCreatedAt.Enabled = False
    Me.ltxtCreatedAt.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtCreatedAt.LabelText = "登録日時"
    Me.ltxtCreatedAt.Location = New System.Drawing.Point(555, 170)
    Me.ltxtCreatedAt.MaxLabelLength = 15
    Me.ltxtCreatedAt.MaxTextLength = 25
    Me.ltxtCreatedAt.MustInput = False
    Me.ltxtCreatedAt.Name = "ltxtCreatedAt"
    Me.ltxtCreatedAt.ReadOnly = True
    Me.ltxtCreatedAt.Size = New System.Drawing.Size(280, 21)
    Me.ltxtCreatedAt.TabIndex = 42
    Me.ltxtCreatedAt.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtModifiedBy
    '
    Me.ltxtModifiedBy.AutoTextSize = False
    Me.ltxtModifiedBy.ClientProcess = Nothing
    Me.ltxtModifiedBy.Enabled = False
    Me.ltxtModifiedBy.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtModifiedBy.LabelText = "更新者"
    Me.ltxtModifiedBy.Location = New System.Drawing.Point(555, 190)
    Me.ltxtModifiedBy.MaxLabelLength = 15
    Me.ltxtModifiedBy.MaxTextLength = 25
    Me.ltxtModifiedBy.MustInput = False
    Me.ltxtModifiedBy.Name = "ltxtModifiedBy"
    Me.ltxtModifiedBy.ReadOnly = True
    Me.ltxtModifiedBy.Size = New System.Drawing.Size(280, 21)
    Me.ltxtModifiedBy.TabIndex = 42
    Me.ltxtModifiedBy.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtModifiedAt
    '
    Me.ltxtModifiedAt.AutoTextSize = False
    Me.ltxtModifiedAt.ClientProcess = Nothing
    Me.ltxtModifiedAt.Enabled = False
    Me.ltxtModifiedAt.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtModifiedAt.LabelText = "更新日時"
    Me.ltxtModifiedAt.Location = New System.Drawing.Point(555, 210)
    Me.ltxtModifiedAt.MaxLabelLength = 15
    Me.ltxtModifiedAt.MaxTextLength = 25
    Me.ltxtModifiedAt.MustInput = False
    Me.ltxtModifiedAt.Name = "ltxtModifiedAt"
    Me.ltxtModifiedAt.ReadOnly = True
    Me.ltxtModifiedAt.Size = New System.Drawing.Size(280, 25)
    Me.ltxtModifiedAt.TabIndex = 42
    Me.ltxtModifiedAt.TextBackColor = System.Drawing.SystemColors.Control
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandPrevious, Me.PcaFunctionCommandNext, Me.PcaFunctionCommandSlipSearch, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandCopy, Me.PcaFunctionCommandClose, Me.PcaFunctionCommandSave, Me.PcaFunctionCommandStock})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 680)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1008, 28)
    Me.PcaFunctionBar1.TabIndex = 44
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    Me.PcaFunctionCommandHelp.ToolTipText = "ヘルプを参照します。"
    '
    'PcaFunctionCommandPrevious
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrevious, Me.PcaCommandItemPrevious)
    Me.PcaFunctionCommandPrevious.FunctionKey = PCA.Controls.FunctionKey.F2
    Me.PcaFunctionCommandPrevious.Tag = Nothing
    Me.PcaFunctionCommandPrevious.Text = "前伝票"
    Me.PcaFunctionCommandPrevious.ToolTipText = "前伝票を表示します。"
    '
    'PcaFunctionCommandNext
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandNext, Me.PcaCommandItemNext)
    Me.PcaFunctionCommandNext.FunctionKey = PCA.Controls.FunctionKey.F3
    Me.PcaFunctionCommandNext.Tag = Nothing
    Me.PcaFunctionCommandNext.Text = "次伝票"
    Me.PcaFunctionCommandNext.ToolTipText = "次伝票を表示します。"
    '
    'PcaFunctionCommandSlipSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSlipSearch, Me.PcaCommandItemSlipSearch)
    Me.PcaFunctionCommandSlipSearch.FunctionKey = PCA.Controls.FunctionKey.F6
    Me.PcaFunctionCommandSlipSearch.Tag = Nothing
    Me.PcaFunctionCommandSlipSearch.Text = "検索"
    Me.PcaFunctionCommandSlipSearch.ToolTipText = "伝票を検索します。"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    Me.PcaFunctionCommandRefer.ToolTipText = "マスタの検索を行います。"
    '
    'PcaFunctionCommandCopy
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandCopy, Me.PcaCommandItemCopy)
    Me.PcaFunctionCommandCopy.FunctionKey = PCA.Controls.FunctionKey.F11
    Me.PcaFunctionCommandCopy.Tag = Nothing
    Me.PcaFunctionCommandCopy.Text = "伝票複写"
    Me.PcaFunctionCommandCopy.ToolTipText = "伝票を複写します。"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    Me.PcaFunctionCommandClose.ToolTipText = "プログラムを終了します。"
    '
    'PcaFunctionCommandSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSave, Me.PcaCommandItemSave)
    Me.PcaFunctionCommandSave.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandSave.Tag = Nothing
    Me.PcaFunctionCommandSave.Text = "登録"
    Me.PcaFunctionCommandSave.ToolTipText = "伝票を登録します。"
    '
    'PcaFunctionCommandStock
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandStock, Me.PcaCommandItemStock)
    Me.PcaFunctionCommandStock.FunctionKey = PCA.Controls.FunctionKey.F7
    Me.PcaFunctionCommandStock.Tag = Nothing
    Me.PcaFunctionCommandStock.Text = "在庫検索"
    Me.PcaFunctionCommandStock.ToolTipText = "在庫検索画面を開きます。"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemPrevious, Me.PcaCommandItemNext, Me.PcaCommandItemSlipSearch, Me.PcaCommandItemRefer, Me.PcaCommandItemCopy, Me.PcaCommandItemSave, Me.PcaCommandItemClose, Me.PcaCommandItemNew, Me.PcaCommandItemDelete, Me.PcaCommandItemReset, Me.PcaCommandItemStock})
    Me.PcaCommandManager1.Parent = Me
    '
    'PcaCommandItemSave
    '
    Me.PcaCommandItemSave.CommandId = 12
    Me.PcaCommandItemSave.CommandName = "Save"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    Me.PcaCommandItemClose.CommandName = "Close"
    '
    'PcaCommandItemNew
    '
    Me.PcaCommandItemNew.CommandName = "New"
    '
    'PcaCommandItemPrevious
    '
    Me.PcaCommandItemPrevious.CommandId = 2
    Me.PcaCommandItemPrevious.CommandName = "Previous"
    '
    'PcaCommandItemNext
    '
    Me.PcaCommandItemNext.CommandId = 3
    Me.PcaCommandItemNext.CommandName = "Next"
    '
    'PcaCommandItemSlipSearch
    '
    Me.PcaCommandItemSlipSearch.CommandId = 6
    Me.PcaCommandItemSlipSearch.CommandName = "Search"
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandId = 7
    Me.PcaCommandItemRefer.CommandName = "Refer"
    '
    'PcaCommandItemDelete
    '
    Me.PcaCommandItemDelete.CommandName = "Delete"
    '
    'PcaCommandItemCopy
    '
    Me.PcaCommandItemCopy.CommandId = 11
    Me.PcaCommandItemCopy.CommandName = "Copy"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    Me.PcaCommandItemHelp.CommandName = "Help"
    '
    'PcaCommandItemStock
    '
    Me.PcaCommandItemStock.CommandId = 7
    '
    'ltxtSlipNo
    '
    Me.ltxtSlipNo.ClientProcess = Nothing
    Me.ltxtSlipNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtSlipNo.LabelText = "伝票No"
    Me.ltxtSlipNo.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtSlipNo.LabelTopAlign = True
    Me.ltxtSlipNo.LimitLength = 16
    Me.ltxtSlipNo.Location = New System.Drawing.Point(152, 86)
    Me.ltxtSlipNo.MaxLabelLength = 10
    Me.ltxtSlipNo.MaxTextLength = 10
    Me.ltxtSlipNo.Name = "ltxtSlipNo"
    Me.ltxtSlipNo.ShowCalculator = False
    Me.ltxtSlipNo.Size = New System.Drawing.Size(70, 44)
    Me.ltxtSlipNo.TabIndex = 1
    Me.ltxtSlipNo.TabStop = False
    Me.ltxtSlipNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'rdoKbn
    '
    Me.rdoKbn.AutoTopMargin = False
    Me.rdoKbn.BindingEntity = Nothing
    Me.rdoKbn.ClientProcess = Nothing
    Me.rdoKbn.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "入庫調整", 0, "入庫調整"))
    Me.rdoKbn.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "出庫調整", 0, "出庫調整"))
    Me.rdoKbn.Location = New System.Drawing.Point(12, 151)
    Me.rdoKbn.MaxLabelLength = 15
    Me.rdoKbn.Name = "rdoKbn"
    Me.rdoKbn.Size = New System.Drawing.Size(281, 22)
    Me.rdoKbn.TabIndex = 3
    Me.rdoKbn.TargetProperty = Nothing
    Me.rdoKbn.Text = "入出庫区分"
    Me.rdoKbn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdoKbn.ToolTip = "入出庫区分を選択してください。"
    '
    'ltxtLotNo
    '
    Me.ltxtLotNo.AutoTextSize = False
    Me.ltxtLotNo.ClientProcess = Nothing
    Me.ltxtLotNo.Enabled = False
    Me.ltxtLotNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtLotNo.LabelText = "ロットNo"
    Me.ltxtLotNo.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtLotNo.Location = New System.Drawing.Point(12, 237)
    Me.ltxtLotNo.MaxLabelLength = 15
    Me.ltxtLotNo.MaxTextLength = 50
    Me.ltxtLotNo.MustInput = False
    Me.ltxtLotNo.Name = "ltxtLotNo"
    Me.ltxtLotNo.ReadOnly = True
    Me.ltxtLotNo.Size = New System.Drawing.Size(483, 22)
    Me.ltxtLotNo.TabIndex = 46
    Me.ltxtLotNo.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtStockNumber
    '
    Me.ltxtStockNumber.AutoTextSize = False
    Me.ltxtStockNumber.ClientProcess = Nothing
    Me.ltxtStockNumber.Enabled = False
    Me.ltxtStockNumber.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtStockNumber.LabelText = "本数"
    Me.ltxtStockNumber.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtStockNumber.Location = New System.Drawing.Point(9, 310)
    Me.ltxtStockNumber.MaxLabelLength = 15
    Me.ltxtStockNumber.MaxTextLength = 36
    Me.ltxtStockNumber.MustInput = False
    Me.ltxtStockNumber.Name = "ltxtStockNumber"
    Me.ltxtStockNumber.ReadOnly = True
    Me.ltxtStockNumber.Size = New System.Drawing.Size(225, 22)
    Me.ltxtStockNumber.TabIndex = 47
    Me.ltxtStockNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ltxtStockNumber.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtStockQuantity
    '
    Me.ltxtStockQuantity.AutoTextSize = False
    Me.ltxtStockQuantity.ClientProcess = Nothing
    Me.ltxtStockQuantity.Enabled = False
    Me.ltxtStockQuantity.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtStockQuantity.LabelText = "数量"
    Me.ltxtStockQuantity.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtStockQuantity.Location = New System.Drawing.Point(9, 331)
    Me.ltxtStockQuantity.MaxLabelLength = 15
    Me.ltxtStockQuantity.MaxTextLength = 36
    Me.ltxtStockQuantity.MustInput = False
    Me.ltxtStockQuantity.Name = "ltxtStockQuantity"
    Me.ltxtStockQuantity.ReadOnly = True
    Me.ltxtStockQuantity.Size = New System.Drawing.Size(225, 22)
    Me.ltxtStockQuantity.TabIndex = 48
    Me.ltxtStockQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ltxtStockQuantity.TextBackColor = System.Drawing.SystemColors.Control
    '
    'lblStock
    '
    Me.lblStock.AllowTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.lblStock.AutoTextSize = False
    Me.lblStock.ClientProcess = Nothing
    Me.lblStock.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.lblStock.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.lblStock.LabelText = "在庫数"
    Me.lblStock.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblStock.LabelTopAlign = True
    Me.lblStock.Location = New System.Drawing.Point(114, 288)
    Me.lblStock.Margin = New System.Windows.Forms.Padding(21, 0, 0, 0)
    Me.lblStock.MaxLabelLength = 32
    Me.lblStock.MaxLength = 32
    Me.lblStock.MaxTextLength = 32
    Me.lblStock.MustInput = False
    Me.lblStock.Name = "lblStock"
    Me.lblStock.Size = New System.Drawing.Size(120, 22)
    Me.lblStock.TabIndex = 50
    Me.lblStock.TabStop = False
    '
    'lblAdjustment
    '
    Me.lblAdjustment.AllowTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.lblAdjustment.AutoTextSize = False
    Me.lblAdjustment.ClientProcess = Nothing
    Me.lblAdjustment.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.lblAdjustment.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.lblAdjustment.LabelText = "調整数"
    Me.lblAdjustment.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblAdjustment.LabelTopAlign = True
    Me.lblAdjustment.Location = New System.Drawing.Point(233, 288)
    Me.lblAdjustment.Margin = New System.Windows.Forms.Padding(21, 0, 0, 0)
    Me.lblAdjustment.MaxLabelLength = 32
    Me.lblAdjustment.MaxLength = 32
    Me.lblAdjustment.MaxTextLength = 32
    Me.lblAdjustment.MustInput = False
    Me.lblAdjustment.Name = "lblAdjustment"
    Me.lblAdjustment.Size = New System.Drawing.Size(119, 22)
    Me.lblAdjustment.TabIndex = 51
    Me.lblAdjustment.TabStop = False
    '
    'numAdjustNumber
    '
    Me.numAdjustNumber.CellValue = ""
    Me.numAdjustNumber.FormatComma = True
    Me.numAdjustNumber.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.numAdjustNumber.Location = New System.Drawing.Point(233, 310)
    Me.numAdjustNumber.MaxTextLength = 17
    Me.numAdjustNumber.Name = "numAdjustNumber"
    Me.numAdjustNumber.Size = New System.Drawing.Size(119, 22)
    Me.numAdjustNumber.TabIndex = 6
    Me.numAdjustNumber.TableType = GetType(String)
    Me.numAdjustNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'numAdjustQuantity
    '
    Me.numAdjustQuantity.CellValue = ""
    Me.numAdjustQuantity.FormatComma = True
    Me.numAdjustQuantity.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.numAdjustQuantity.Location = New System.Drawing.Point(233, 331)
    Me.numAdjustQuantity.MaxTextLength = 17
    Me.numAdjustQuantity.Name = "numAdjustQuantity"
    Me.numAdjustQuantity.Size = New System.Drawing.Size(119, 22)
    Me.numAdjustQuantity.TabIndex = 7
    Me.numAdjustQuantity.TableType = GetType(String)
    Me.numAdjustQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'lblUnitName
    '
    Me.lblUnitName.Location = New System.Drawing.Point(352, 331)
    Me.lblUnitName.Name = "lblUnitName"
    Me.lblUnitName.Size = New System.Drawing.Size(140, 22)
    Me.lblUnitName.TabIndex = 52
    Me.lblUnitName.Text = "kg"
    '
    'rdoKbnNotSelect
    '
    Me.rdoKbnNotSelect.AutoTopMargin = False
    Me.rdoKbnNotSelect.BindingEntity = Nothing
    Me.rdoKbnNotSelect.ClientProcess = Nothing
    Me.rdoKbnNotSelect.Enabled = False
    Me.rdoKbnNotSelect.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "棚卸(入庫)", 0, "棚卸(入庫)"))
    Me.rdoKbnNotSelect.ItemList.Add(New PCA.Controls.PcaMultiRadioButton.Item("0", "棚卸(出庫)", 0, "棚卸(出庫)"))
    Me.rdoKbnNotSelect.Location = New System.Drawing.Point(291, 151)
    Me.rdoKbnNotSelect.MaxLabelLength = 0
    Me.rdoKbnNotSelect.Name = "rdoKbnNotSelect"
    Me.rdoKbnNotSelect.Size = New System.Drawing.Size(204, 22)
    Me.rdoKbnNotSelect.TabIndex = 53
    Me.rdoKbnNotSelect.TargetProperty = Nothing
    Me.rdoKbnNotSelect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'frmMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackColor = System.Drawing.SystemColors.Control
    Me.ClientSize = New System.Drawing.Size(1008, 730)
    Me.Controls.Add(Me.rdoKbnNotSelect)
    Me.Controls.Add(Me.lblUnitName)
    Me.Controls.Add(Me.numAdjustQuantity)
    Me.Controls.Add(Me.numAdjustNumber)
    Me.Controls.Add(Me.ltxtStockQuantity)
    Me.Controls.Add(Me.ltxtStockNumber)
    Me.Controls.Add(Me.ltxtLotNo)
    Me.Controls.Add(Me.rdoKbn)
    Me.Controls.Add(Me.ltxtSlipNo)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.ltxtModifiedAt)
    Me.Controls.Add(Me.ltxtModifiedBy)
    Me.Controls.Add(Me.ltxtCreatedAt)
    Me.Controls.Add(Me.ltxtCreatedBy)
    Me.Controls.Add(Me.setProduct)
    Me.Controls.Add(Me.setWarehouse)
    Me.Controls.Add(Me.setReasonCode)
    Me.Controls.Add(Me.setReasonContent)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.ldtAdjustmentDate)
    Me.Controls.Add(Me.ltxtSlipNo2)
    Me.Controls.Add(Me.HeaderLabel)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Controls.Add(Me.lblStock)
    Me.Controls.Add(Me.lblAdjustment)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1024, 768)
    Me.Name = "frmMain"
    Me.Text = "在庫調整ロット入力"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents ServiceController1 As System.ServiceProcess.ServiceController
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents FToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSave As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemNew As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemReset As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPrevious As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemNext As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemRefer As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemDelete As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemCopy As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPrevious As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonNext As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonCreate As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonReset As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonDelete As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonCopy As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonSave As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents HeaderLabel As PCA.TSC.Kon.Tools.TscInputHeaderLabel
  Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents setProduct As PCA.Controls.PcaCodeSet
  Friend WithEvents setWarehouse As PCA.Controls.PcaCodeSet
  Friend WithEvents setReasonCode As PCA.Controls.PcaCodeSet
  Friend WithEvents setReasonContent As PCA.Controls.PcaCodeSet
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents ltxtCreatedBy As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemPrevious As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemNext As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSlipSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemCopy As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSave As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandPrevious As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandNext As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSlipSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandCopy As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSave As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ldtAdjustmentDate As Sunloft.PCAControls.SLPcaLabeledDate
  Friend WithEvents ltxtSlipNo2 As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtCreatedAt As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtModifiedBy As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtModifiedAt As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtSlipNo As PCA.Controls.PcaLabeledNumberBox
  Friend WithEvents PcaCommandItemDelete As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemNew As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemReset As PCA.Controls.PcaCommandItem
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaCommandItemStock As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandStock As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolStripButtonSearch As System.Windows.Forms.ToolStripButton
  Friend WithEvents rdoKbn As PCA.Controls.PcaMultiRadioButton
  Friend WithEvents ltxtLotNo As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtStockQuantity As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtStockNumber As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents lblStock As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents lblAdjustment As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents numAdjustQuantity As PCA.Controls.PcaNumberBox
  Friend WithEvents numAdjustNumber As PCA.Controls.PcaNumberBox
  Friend WithEvents lblUnitName As PCA.Controls.PcaLabel
  Friend WithEvents rdoKbnNotSelect As PCA.Controls.PcaMultiRadioButton

End Class

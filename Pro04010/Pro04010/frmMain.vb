﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports PCA.UITools
Imports PCA.Controls
Imports Sunloft.PCAIF
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon
Imports PCA.TSC.Kon.BusinessEntity.Defines

Public Class frmMain

#Region "Declare - 変数宣言"
  Private connector As IIntegratedApplication = Nothing

  Private m_conf As Sunloft.PcaConfig
  Private m_appClass As ConnectToPCA = New ConnectToPCA

  Private m_IntSlipNo As Integer = 0

  Private m_strCurrentUser As String = String.Empty
  Private m_strSearchItem As String = String.Empty
  Private m_IsF12CloseFlag As Boolean = True

  Private AMS1Class As AMS1

  Private SL_ZADJClass As SL_ZADJ

  Private m_strCurCode As String = String.Empty
  Private m_isShowDialog As Boolean = False
  Private m_enmCurrentRdo As RadioType

  Private m_intStockID As Integer = 0
  Private m_intWarehousingNum As Integer = 0
  Private m_decWarehousingQty As Decimal = 0D

  Private m_intStockIDOld As Integer = 0
  Private m_intAdjustNumOld As Integer = 0
  Private m_decAdjustQtyOld As Decimal = 0D

  Private m_isNotValidate As Boolean = True

  Private m_isDisallowRegByDate As Boolean = False
  Private m_isDisallowRegByST As Boolean = False
  Private m_intCutoffDate As Integer = 0

  Private m_HeaderStateBefore As PCA.Controls.LabelStateType
  Private m_isDataChanged As Boolean = False

  Private Enum RadioType
    Ajustment_WH
    Ajustment_Shipping
    Inventry_WH
    Inventry_Shipping
  End Enum

#End Region

#Region "Initialize"

  Public Sub New()
    Try

      InitializeComponent()
      m_conf = New Sunloft.PcaConfig
      Dim strParam As String = Command()
      Dim parts As String() = strParam.Split(New Char() {" "c})
      Dim intParam As Integer = 0


      If parts.Length > 1 Then 'call from 03020 from menu
        m_appClass.ConnectToPCA(strParam)
        connector = m_appClass.connector
      ElseIf Integer.TryParse(strParam, intParam) Then

        connector = Sunloft.PCAIF.ConnectToPCA.ConnectToPCACalledPG() ' call from 03020 from code
      Else
        m_appClass.ConnectToPCA() 'start normally (from code or menu)
        connector = m_appClass.connector
      End If

      InitControl()
      InitCreateMode()

      If intParam <> 0 OrElse (m_appClass.argumentContent <> "" AndAlso Integer.TryParse(m_appClass.argumentContent, intParam)) Then
        'search by parameter
        DisplaySlipByNo(intParam)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  Private Sub InitControl()
    Try

      Dim SL_LMBClass = New SL_LMB(connector)

      AMS1Class = New AMS1(connector)
      AMS1Class.ReadAll() 'Get number of digits for fields
      SL_ZADJClass = New SL_ZADJ(connector)
      Dim MasterDialog As SLMasterSearchDialog = New SLMasterSearchDialog(connector)

      SL_LMBClass.ReadOnlyRow()
      m_intCutoffDate = SL_LMBClass.sl_lmb_zcutoff_date

      SLCmnFunction.SetCodesetInitialize(setProduct, AMS1Class.ams1_ProductLength, 52)
      SLCmnFunction.SetCodesetInitialize(setWarehouse, AMS1Class.ams1_WarehouseLength, 52)
      setReasonContent.Left = setReasonCode.Left + setReasonCode.Width
      setReasonCode.LimitLength = AMS1Class.ams1_MemoLength

      '### Function Enabled
      PcaFunctionCommandRefer.Enabled = False
      PcaFunctionCommandCopy.Enabled = False
      PcaFunctionCommandNext.Enabled = False
      PcaFunctionCommandStock.Enabled = False
      ToolStripButtonNext.Enabled = False
      ToolStripButtonCopy.Enabled = False
      ToolStripButtonDelete.Enabled = False

      ldtAdjustmentDate.Focus()
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    Try

      MyBase.OnLoad(e)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  ''' <summary>
  ''' Set to initial mode
  ''' </summary>
  ''' <param name="isClearData">Clear all Hearer fields. False when Copying slip</param>
  ''' <remarks></remarks>
  Public Sub InitCreateMode(Optional isClearData As Boolean = True)
    Try
      HeaderLabel.State = LabelStateType.New
      m_HeaderStateBefore = LabelStateType.New
      If isClearData Then
        ClearData()
        m_IntSlipNo = 0
        m_strCurrentUser = String.Empty
        m_strSearchItem = String.Empty
        m_IsF12CloseFlag = True
        numAdjustNumber.Enabled = False
        numAdjustQuantity.Enabled = False
      Else
        numAdjustNumber.Enabled = True
        numAdjustQuantity.Enabled = True
        m_IsF12CloseFlag = False
      End If
      m_isDisallowRegByDate = False
      m_isDisallowRegByST = False

      rdoKbn.SelectedIndex = RadioType.Ajustment_Shipping
      m_enmCurrentRdo = RadioType.Ajustment_Shipping

      lblUnitName.Text = String.Empty
      ldtAdjustmentDate.Date = Now

      PcaFunctionCommandNext.Enabled = False
      ToolStripButtonDelete.Enabled = False
      PcaFunctionCommandCopy.Enabled = False
      ToolStripButtonNext.Enabled = False
      ToolStripButtonCopy.Enabled = False
      ltxtSlipNo.Enabled = True
      m_isDataChanged = False

      ldtAdjustmentDate.Focus()
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Set to edit mode.
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitEditMode()
    HeaderLabel.State = LabelStateType.Modify
    m_HeaderStateBefore = LabelStateType.Modify
    PcaFunctionCommandCopy.Enabled = True
    PcaFunctionCommandNext.Enabled = True
    ltxtSlipNo.Enabled = False
    ToolStripButtonDelete.Enabled = True
    ToolStripButtonNext.Enabled = True
    ToolStripButtonCopy.Enabled = True
    PcaFunctionCommandStock.Enabled = True
    m_IsF12CloseFlag = False
    m_isDataChanged = False
  End Sub
#End Region

#Region "Private method"

  Private Sub SetRetZaikoDlg(ByVal ZaikoDialog As Sunloft.PCAForms.SLZaikoDialog)
    Try
      Dim decDigit As Short = numAdjustQuantity.DecimalDigit

      m_intStockID = ZaikoDialog.RetIntInvID
      ltxtLotNo.Text = ZaikoDialog.RetStrLotNo
      ltxtStockNumber.Text = Format(ZaikoDialog.RetDecInvNum, "#,##0")
      ltxtStockQuantity.Text = Format(ZaikoDialog.RetDecInvQty, "#,##0." & New String("0"c, decDigit))
      numAdjustNumber.Text = If(ZaikoDialog.RetDecShippingNumber = 0, String.Empty, ZaikoDialog.RetDecShippingNumber.ToString)
      numAdjustQuantity.Text = If(ZaikoDialog.RetDecShippingQty = 0, String.Empty, ZaikoDialog.RetDecShippingQty.ToString)
      setWarehouse.CodeText = ZaikoDialog.RetStrWarehouseCode.ToString
      CodeTextWarehouseValidating()

      numAdjustNumber.Enabled = True
      numAdjustQuantity.Enabled = True

      numAdjustNumber.Focus()

      m_IsF12CloseFlag = False

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>
  ''' Reset Data
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ClearData(Optional ByVal isStockReset As Boolean = False) As Boolean
    'Clear data 
    If Not isStockReset Then
      ltxtSlipNo.Text = String.Empty
      ltxtSlipNo2.Text = String.Empty

      setProduct.CodeText = String.Empty
      setWarehouse.CodeText = String.Empty
      setReasonCode.CodeText = String.Empty
      setReasonContent.CodeText = String.Empty
      setProduct.NameText = " "
      setWarehouse.NameText = String.Empty
      lblUnitName.Text = String.Empty
      ltxtCreatedAt.Text = String.Empty
      ltxtCreatedBy.Text = String.Empty
      ltxtModifiedAt.Text = String.Empty
      ltxtModifiedBy.Text = String.Empty
    End If

    ltxtLotNo.Text = String.Empty
    numAdjustNumber.Text = String.Empty
    numAdjustQuantity.Text = String.Empty
    ltxtStockNumber.Text = String.Empty
    ltxtStockQuantity.Text = String.Empty

    PcaFunctionCommandStock.Enabled = False
    numAdjustNumber.Enabled = False
    numAdjustQuantity.Enabled = False

    m_IsF12CloseFlag = True

    Return True
  End Function

  Private Sub SetProductInfo(ByVal SMS As SMS)
    Try
      setProduct.CodeText = SMS.sms_scd
      setProduct.NameText = SMS.sms_AllItemName
      lblUnitName.Text = SMS.sms_tani
      numAdjustQuantity.DecimalDigit = CShort(SMS.sms_sketa)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SetWarehouseInfo(ByVal strCode As String, ByVal strName As String)
    Try
      setWarehouse.CodeText = strCode
      setWarehouse.NameText = strName
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SetReasonInfo(ByVal strCode As String, ByVal strName As String)
    Try
      setReasonCode.CodeText = strCode
      setReasonContent.CodeText = strName.Trim
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Function ValidateInput(Optional ByVal isRegister As Boolean = False) As Boolean
    Try
      Dim PosForm As System.Drawing.Point
      Dim intTargetNum As Integer = 0
      Dim decTargetQty As Decimal = 0D
      Integer.TryParse(numAdjustNumber.Text, intTargetNum)
      Decimal.TryParse(numAdjustQuantity.Text, decTargetQty)
      Dim SMS As SMS = New SMS(connector)

      If isRegister Then

        If Not CodeTextProductValidating(, False) Then
          setProduct.Focus()
          Return False
        End If
        If Not CodeTextWarehouseValidating(, False) Then
          setWarehouse.Focus()
          Return False
        End If
        If Not CodeTextReasonValidating(, False, True) Then
          setReasonCode.Focus()
          Return False
        End If

        If m_intStockID = 0 And ltxtLotNo.Text.Trim.Length = 0 Then
          DisplayBox.ShowNotify("在庫調整を行うロット" & SLConstants.NotifyMessage.SELECT_REQUIRED)
          setProduct.Focus()
          Return False
        End If

        If numAdjustQuantity.Text.Trim.Length = 0 Then
          DisplayBox.ShowNotify("在庫調整を行うロット" & SLConstants.NotifyMessage.SELECT_REQUIRED)
          If numAdjustQuantity.Enabled Then
            numAdjustQuantity.Focus()
          Else
            setProduct.Focus()
          End If
          Return False
        End If

      End If

      If ltxtLotNo.Text.Trim.Length > 0 Then
        Select Case m_enmCurrentRdo
          Case RadioType.Ajustment_Shipping
            If intTargetNum > CInt(ltxtStockNumber.Text) Then
              PosForm.X = numAdjustNumber.Location.X + numAdjustNumber.Width
              PosForm.Y = numAdjustNumber.Location.Y
              SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.ADJUST_QUANTITY_OVER_ST, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PosForm, ToolTip1, Me)
              numAdjustNumber.Focus()
              Return False
            ElseIf decTargetQty > CDec(ltxtStockQuantity.Text) Then
              PosForm.X = numAdjustQuantity.Location.X + numAdjustQuantity.Width
              PosForm.Y = numAdjustQuantity.Location.Y
              SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.ADJUST_QUANTITY_OVER_ST, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PosForm, ToolTip1, Me)
              numAdjustQuantity.Focus()
              Return False
            End If

          Case RadioType.Ajustment_WH
            If intTargetNum > m_intWarehousingNum Then
              PosForm.X = numAdjustNumber.Location.X + numAdjustNumber.Width
              PosForm.Y = numAdjustNumber.Location.Y
              SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.ADJUST_NUMBER_OVER_WH, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PosForm, ToolTip1, Me)
              numAdjustNumber.Focus()
              Return False
            ElseIf decTargetQty > m_decWarehousingQty Then
              PosForm.X = numAdjustQuantity.Location.X + numAdjustQuantity.Width
              PosForm.Y = numAdjustQuantity.Location.Y
              SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.ADJUST_QUANTITY_OVER_WH, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PosForm, ToolTip1, Me)
              numAdjustQuantity.Focus()
              Return False
            End If
        End Select
      End If

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

  ''' <summary>Get Warehousing Number ,Quantity By SL_NYKD</summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetWHNumQty() As Boolean
    Try
      Dim SL_NYKDClass As SL_NYKD = New SL_NYKD(connector)

      SL_NYKDClass.GetNumberQuantity(setProduct.CodeText, setWarehouse.CodeText, m_intWarehousingNum, m_decWarehousingQty)

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function


  Private Function ShowStockDialog() As Boolean
    Try
      Dim zaikoDialog As Sunloft.PCAForms.SLZaikoDialog
      Dim intStockIDTemp As Integer = m_intStockID
      Dim intNumber As Integer
      Dim decQty As Decimal
      Dim zaikoSlipState As PCA.Controls.LabelStateType

      Integer.TryParse(numAdjustNumber.Text, intNumber)
      Decimal.TryParse(numAdjustQuantity.Text, decQty)

      If m_intStockID > 0 Then
        zaikoSlipState = HeaderLabel.State
      Else
        zaikoSlipState = LabelStateType.New
      End If

      zaikoDialog = New Sunloft.PCAForms.SLZaikoDialog(connector, setProduct.CodeText, ldtAdjustmentDate.Date, SLZaikoDialog.enmSlipType.typNormal, setWarehouse.CodeText, _
                                                       m_intStockID, intNumber, decQty, zaikoSlipState)
      
      If zaikoDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
        SetRetZaikoDlg(zaikoDialog)
        GetWHNumQty()
        Return True
      Else
        m_intStockID = intStockIDTemp
        Return False
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

#End Region

#Region "PCA command Manager"
  ''' <summary>
  ''' Event called when PCACommandManager is called (PCA Function Bar button is pressed/Select from Menu, press F～ key)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try
      Dim commandItem As PcaCommandItem = e.CommandItem
      Dim isSuccess As Boolean = True

      Select Case True

        Case commandItem Is PcaCommandItemPrevious 'F2
          Dim intPrevSlipNo As Integer = SL_ZADJClass.sl_zadj_denno
          If (HeaderLabel.State = LabelStateType.New And Not m_IsF12CloseFlag) Or (HeaderLabel.State = LabelStateType.Modify And m_isDataChanged) Then
            Dim dlgResult = DisplayBox.ShowContact(String.Empty)
            If dlgResult = Windows.Forms.DialogResult.Yes Then

              If ValidateInput() Then
                If HeaderLabel.State = LabelStateType.New Then
                  isSuccess = InsertData()
                Else
                  isSuccess = UpdateData()
                End If
              Else
                isSuccess = False
              End If

              If isSuccess Then
                intPrevSlipNo = SL_ZADJClass.sl_zadj_denno
                InitControl()
                HeaderLabel.State = LabelStateType.Modify
                m_IsF12CloseFlag = True
              Else
                Return
              End If
            ElseIf dlgResult = Windows.Forms.DialogResult.Cancel Then
              Return
            End If
          End If

          If HeaderLabel.State = LabelStateType.New Then
            SL_ZADJClass.sl_zadj_denno = SL_ZADJClass.GetMaxSlipNo()
            If SL_ZADJClass.sl_zadj_denno > 0 Then
              'Display slip with max header
              If DisplaySlipByNo(SL_ZADJClass.sl_zadj_denno) Then
                InitEditMode()
              End If
            Else
              Sunloft.Message.DisplayBox.ShowError(SLConstants.NotifyMessage.PREVIOUS_MESSAGE_NOT_FOUND)
            End If

          ElseIf HeaderLabel.State = LabelStateType.Modify Or HeaderLabel.State = LabelStateType.Refer Then
            SL_ZADJClass.sl_zadj_denno = SL_ZADJClass.GetPrevSlipNo(SL_ZADJClass.sl_zadj_denno)
            If SL_ZADJClass.sl_zadj_denno > 0 Then
              DisplaySlipByNo(SL_ZADJClass.sl_zadj_denno)
            Else
              Sunloft.Message.DisplayBox.ShowError(SLConstants.NotifyMessage.PREVIOUS_MESSAGE_NOT_FOUND)
            End If

          End If

        Case commandItem Is PcaCommandItemNext 'F3
          Dim intNextSlipNo As Integer = SL_ZADJClass.sl_zadj_denno
          If (HeaderLabel.State = LabelStateType.New And Not m_IsF12CloseFlag) Or (HeaderLabel.State = LabelStateType.Modify And m_isDataChanged) Then
            Dim dlgResult = DisplayBox.ShowContact(String.Empty)
            If dlgResult = Windows.Forms.DialogResult.Yes Then

              If ValidateInput() Then
                If HeaderLabel.State = LabelStateType.New Then
                  isSuccess = InsertData()
                Else
                  isSuccess = UpdateData()
                End If
              Else
                isSuccess = False
              End If

              If isSuccess Then
                intNextSlipNo = SL_ZADJClass.sl_zadj_denno
                InitControl()
                HeaderLabel.State = LabelStateType.Modify
                m_IsF12CloseFlag = True
              Else
                Return
              End If
            ElseIf dlgResult = Windows.Forms.DialogResult.Cancel Then
              Return
            End If
          End If

          'Only enable in Edit mode
          If HeaderLabel.State = LabelStateType.Modify Or HeaderLabel.State = LabelStateType.Refer Then
            intNextSlipNo = SL_ZADJClass.GetNextSlipNo(intNextSlipNo)
            If intNextSlipNo > 0 Then
              DisplaySlipByNo(intNextSlipNo)
            Else
              Sunloft.Message.DisplayBox.ShowError(SLConstants.NotifyMessage.NEXT_MESSAGE_NOT_FOUND)
            End If

          End If

        Case commandItem Is PcaCommandItemSlipSearch 'F6
          Dim denpyoDialog = New SLDenSearchDialog(connector)
          Dim strSlipNo As String = denpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.InventorySlip)
          If String.IsNullOrEmpty(strSlipNo) = False Then
            ltxtSlipNo.Text = denpyoDialog.returnValue()
            ltxtWhNo_Validating(sender, New System.ComponentModel.CancelEventArgs(False))
          End If

        Case commandItem Is PcaCommandItemStock 'F7

          ShowStockDialog()

        Case commandItem Is PcaCommandItemRefer 'F8

          SearchItem(sender)

        Case commandItem Is PcaCommandItemCopy 'F11
          Dim intStockIDTemp As Integer = m_intStockID
          InitCreateMode(False)
          ltxtSlipNo.Text = String.Empty
          m_intStockID = intStockIDTemp

        Case commandItem Is PcaCommandItemSave 'F12


          If ValidateInput(True) Then
            If HeaderLabel.State = LabelStateType.New Then
              isSuccess = InsertData()
            Else
              isSuccess = UpdateData()
            End If

            If isSuccess Then
              InitCreateMode()
              m_IsF12CloseFlag = True

            End If
          End If

        Case commandItem Is PcaCommandItemClose 'F12

          Me.Close()

        Case commandItem Is PcaCommandItemDelete 'Delete
          Dim result As DialogResult = Sunloft.Message.DisplayBox.ShowWarning(SLConstants.ConfirmMessage.DELETE_CONFIRM_MESSAGE)
          If result = Windows.Forms.DialogResult.Yes Then

            Dim session As ICustomSession
            session = connector.CreateTransactionalSession

            isSuccess = Delete(session)

            If isSuccess Then
              session.Commit()
              session.Dispose()
              InitCreateMode()
            Else
              session.Rollback()
              session.Dispose()
            End If

          End If

        Case commandItem Is PcaCommandItemNew

          InitCreateMode()

        Case commandItem Is PcaCommandItemReset

          Select Case HeaderLabel.State
            Case LabelStateType.New
              InitCreateMode()
            Case LabelStateType.Modify, LabelStateType.Refer
              'Clear table, display current slip
              DisplaySlipByNo(SL_ZADJClass.sl_zadj_denno, False)
          End Select

      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  '***********************************************************																														
  'Name          : PcaCommandManager1_UpdateCommandUI
  'Content       :  F12 = Close / Register
  'Return Value  : 
  'Argument      : slipNo, isClearDataFirst = False in case of Reset (Remain data in labels)
  'Created date  : 2015/11/23  by Lai Minh Hien (PCA Sample)
  'Modified date :   by       Content: 
  '***********************************************************	
  Private Sub PcaCommandManager1_UpdateCommandUI(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.UpdateCommandUI
    Dim commandItem As PcaCommandItem = e.CommandItem
    'Change F12 caption+function between 
    If commandItem Is PcaCommandItemSave Then
      commandItem.Enabled = Not m_IsF12CloseFlag

    ElseIf commandItem Is PcaCommandItemClose Then
      commandItem.Enabled = m_IsF12CloseFlag
      If m_IsF12CloseFlag Then
        ToolStripMenuItemSave.Enabled = False
        ToolStripButtonSave.Enabled = False
      Else
        ToolStripButtonSave.Enabled = True
      End If
      ToolStripButtonClose.Enabled = True
      ToolStripMenuItemClose.Enabled = True
    End If

    If Not m_isNotValidate Then rdoKbn.SelectedIndex = m_enmCurrentRdo
    If m_isDisallowRegByDate Then
      m_IsF12CloseFlag = True
      HeaderLabel.State = LabelStateType.Refer
      HeaderLabel.Text = SLConstants.NotifyMessage.CANNOT_REG_BY_CLOSEDATE
    ElseIf m_isDisallowRegByST Then
      m_IsF12CloseFlag = True
      HeaderLabel.State = LabelStateType.Refer
      HeaderLabel.Text = SLConstants.NotifyMessage.CANNOT_REG_BY_STOCKTAKING
    Else
      HeaderLabel.State = m_HeaderStateBefore
      If m_HeaderStateBefore = LabelStateType.Modify Then m_IsF12CloseFlag = False
      HeaderLabel.Text = String.Empty
    End If
  End Sub
#End Region

#Region "Display"

  Private Function DisplaySlipByNo(ByVal intSlipNo As Integer, Optional isClearDataFirst As Boolean = True) As Boolean
    Try

      If isClearDataFirst Then ClearData()
      If SL_ZADJClass.ReadByID(intSlipNo) Then

        HeaderLabel.State = LabelStateType.Modify
        PcaFunctionCommandNext.Enabled = True
        ToolStripButtonDelete.Enabled = True
        PcaFunctionCommandCopy.Enabled = True

        numAdjustNumber.Enabled = True
        numAdjustQuantity.Enabled = True

        DisplayToScreen()
        InitEditMode()
        GetWHNumQty()

        Return True
      End If
      Return False 'Slip number is not existed
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function DisplayToScreen() As Boolean

    Try
      Dim provider As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CurrentCulture
      Dim SMSClass As SMS = New SMS(connector)
      Dim SL_ZDNClass As SL_ZDN = New SL_ZDN(connector)

      'Set Header from Class
      With SL_ZADJClass
        ldtAdjustmentDate.Date = Date.ParseExact(.sl_zadj_date.ToString, "yyyyMMdd", provider)

        ltxtSlipNo2.Text = .sl_zadj_denno2
        setProduct.CodeText = .sl_zadj_scd
        CodeTextProductValidating(, False)
        SMSClass.ReadByID(.sl_zadj_scd)

        setWarehouse.CodeText = .sl_zadj_souko
        CodeTextWarehouseValidating(, False)

        setReasonCode.CodeText = .sl_zadj_tekcd
        setReasonContent.CodeText = .sl_zadj_tekmei

        SL_ZDNClass.ReadByID(.sl_zadj_zdn_id)

        ltxtStockNumber.Text = Format(.sl_zadj_honsu + SL_ZDNClass.sl_zdn_honsu, "#,##0")
        ltxtStockQuantity.Text = Format(.sl_zadj_suryo + SL_ZDNClass.sl_zdn_suryo, "#,##0." & New String("0"c, SMSClass.sms_sketa))

        numAdjustNumber.Text = .sl_zadj_honsu.ToString
        numAdjustQuantity.Text = Format(.sl_zadj_suryo, "#,##0." & New String("0"c, SMSClass.sms_sketa))
        m_intAdjustNumOld = .sl_zadj_honsu
        m_decAdjustQtyOld = .sl_zadj_suryo

        ltxtLotNo.Text = SL_ZDNClass.sl_zdn_ulotno

        ltxtCreatedBy.Text = .sl_zadj_insuser
        ltxtCreatedAt.Text = CStr(.sl_zadj_insdate)
        ltxtModifiedBy.Text = .sl_zadj_upduser
        If CStr(.sl_zadj_upddate) <> SLConstants.NOTHING_DATE Then ltxtModifiedAt.Text = CStr(.sl_zadj_upddate)

        m_intStockID = .sl_zadj_zdn_id
        m_intStockIDOld = .sl_zadj_zdn_id

        ltxtSlipNo.Text = .sl_zadj_denno.ToString()

        Select Case .sl_zadj_iokbn
          Case SLConstants.SL_ZADJType.StocktakingMinus, SLConstants.SL_ZADJType.StocktakingPlus
            m_isDisallowRegByST = True
          Case Else
            m_isDisallowRegByST = False
        End Select

        Select Case SL_ZADJClass.sl_zadj_iokbn
          Case SLConstants.SL_ZADJType.StocktakingMinus
            m_enmCurrentRdo = RadioType.Inventry_Shipping
            rdoKbn.SelectedIndex = -1
            rdoKbnNotSelect.SelectedIndex = m_enmCurrentRdo - 2
          Case SLConstants.SL_ZADJType.StocktakingPlus
            m_enmCurrentRdo = RadioType.Inventry_WH
            rdoKbn.SelectedIndex = -1
            rdoKbnNotSelect.SelectedIndex = m_enmCurrentRdo - 2
          Case SLConstants.SL_ZADJType.Shipping
            m_enmCurrentRdo = RadioType.Ajustment_Shipping
            rdoKbnNotSelect.SelectedIndex = -1
            rdoKbn.SelectedIndex = m_enmCurrentRdo
          Case SLConstants.SL_ZADJType.Warehousing
            m_enmCurrentRdo = RadioType.Ajustment_WH
            rdoKbnNotSelect.SelectedIndex = -1
            rdoKbn.SelectedIndex = m_enmCurrentRdo

        End Select
      End With
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

#End Region

#Region "Validate"
  Private Sub PcaNumberBox_Validating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles numAdjustNumber.Validating2, numAdjustQuantity.Validating2
    Try
      m_isNotValidate = ValidateInput()
      If Not m_isNotValidate Then e.Cancel = True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  '★please standardize of validate events

  ''' <summary>
  ''' After user input slip number, if there's corresponding data, display it, move to edit mode. If not, delete slip number.
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub ltxtWhNo_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles ltxtSlipNo.Validating
    Try
      Dim intSlipNo As Integer = 0
      If ltxtSlipNo.Text = String.Empty Then Return

      If Not Integer.TryParse(ltxtSlipNo.Text, intSlipNo) OrElse Not DisplaySlipByNo(intSlipNo) Then
        e.Cancel = True
        ltxtSlipNo.Text = String.Empty
      Else
        InitEditMode()
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub PcaCodeSet_CodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setReasonCode.CodeTextValidating2, _
                                                                                                                      setProduct.CodeTextValidating2, _
                                                                                                                     setWarehouse.CodeTextValidating2
    Try
      Dim objPcaCodeSet As PcaCodeSet = DirectCast(sender, PcaCodeSet)

      Select Case objPcaCodeSet.Name
        Case setWarehouse.Name, setProduct.Name
          If objPcaCodeSet.CodeText.Trim.Length = 0 Then
            ClearData(True) : Exit Sub
          End If

        Case setReasonCode.Name
          If objPcaCodeSet.CodeText.Trim.Length = 0 Then Exit Sub
      End Select

      Select Case objPcaCodeSet.Name
        Case setReasonCode.Name
          CodeTextReasonValidating(e)
        Case setProduct.Name
          CodeTextProductValidating(e)
        Case setWarehouse.Name
          CodeTextWarehouseValidating(e)
      End Select

      Select Case objPcaCodeSet.Name
        Case setProduct.Name, setWarehouse.Name
          If setProduct.NameText.Trim.Length > 0 And setWarehouse.NameText.Trim.Length > 0 Then
            If m_isShowDialog Then
              If Not ShowStockDialog() Then e.Cancel = True
            Else
              e.Cancel = False
            End If
            PcaFunctionCommandStock.Enabled = True
          Else
            ClearData(True)
          End If

      End Select

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try


  End Sub

  ''' <summary>
  ''' Validate Warehouse
  ''' </summary>
  ''' <param name="rowIndex"></param>
  ''' <param name="newValue"></param>
  ''' <param name="needMessageBox"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ''' 
  Private Function ValidateSoko(ByVal rowIndex As Integer, ByVal newValue As String, ByVal needMessageBox As Boolean) As Boolean
    Try
      Dim headerLabelText As String = String.Empty
      Dim EMS As New EMS(connector)
      Dim strWHName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.Warehouse, newValue)

      If strWHName.Trim.Length = 0 Then
        Return False
      End If
      Me.HeaderLabel.Text = headerLabelText

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function CodeTextProductValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True) As Boolean
    Try
      HeaderLabel.Text = String.Empty
      Dim SMS = New SMS(connector)
      Dim strProductCode As String = SLCmnFunction.standardlizeCode(setProduct.CodeText.TrimEnd(), AMS1Class.ams1_PersonLength)
      SMS.ReadByID(strProductCode)

      If NotCheckIfNoChange Then
        If m_strCurCode = strProductCode Then
          'no changes
          If Not String.IsNullOrEmpty(strProductCode) AndAlso String.IsNullOrEmpty(Me.setProduct.NameText) Then
            Me.setProduct.NameText = SMS.sms_mei
          End If
          m_isShowDialog = False
          Return True
        End If
      End If

      m_isShowDialog = True
      If SMS.ReadByID(strProductCode) Then
        SetProductInfo(SMS)
        Return True
      Else
        If strProductCode.Trim.Length > 0 Then DisplayBox.ShowNotify("商品" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        m_isShowDialog = False
        If Not e Is Nothing Then e.Cancel = True
        setProduct.CodeText = m_strCurCode
        setProduct.Focus()
        Return False
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function CodeTextWarehouseValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True) As Boolean
    Try
      HeaderLabel.Text = String.Empty
      Dim EMS = New EMS(connector)
      Dim strWHCode As String = SLCmnFunction.standardlizeCode(setWarehouse.CodeText.TrimEnd(), AMS1Class.ams1_DepartmentLength)
      Dim strWHName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.Warehouse, strWHCode)

      If NotCheckIfNoChange Then
        If m_strCurCode = strWHCode Then
          'No changes
          If Not String.IsNullOrEmpty(strWHCode) AndAlso String.IsNullOrEmpty(Me.setWarehouse.NameText) Then
            Me.setWarehouse.NameText = strWHName
          End If
          m_isShowDialog = False
          Return True
        End If
      End If

      m_isShowDialog = True
      If strWHName.Trim.Length > 0 Then
        SetWarehouseInfo(strWHCode, strWHName)
        Return True
      Else
        DisplayBox.ShowNotify("倉庫" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        m_isShowDialog = False
        If Not e Is Nothing Then e.Cancel = True
        setWarehouse.CodeText = m_strCurCode
        setWarehouse.Focus()
        Return False
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function CodeTextReasonValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True, Optional ByVal isRegister As Boolean = False) As Boolean
    Try
      HeaderLabel.Text = String.Empty
      Dim EMS As New EMS(connector)
      Dim strMemoCode As String = SLCmnFunction.standardlizeCode(setReasonCode.CodeText.TrimEnd(), AMS1Class.ams1_MemoLength)
      Dim strName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, strMemoCode)
      If NotCheckIfNoChange Then
        If m_strCurCode = strMemoCode Then
          'No changes
          Return True
        End If
      End If

      If String.IsNullOrEmpty(strMemoCode) Then Return True

      If EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, strMemoCode).Length = 0 Then
        If strMemoCode.Trim.Length > 0 Then DisplayBox.ShowNotify("調整理由" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        If Not e Is Nothing Then e.Cancel = True
        setReasonCode.CodeText = m_strCurCode
        setReasonCode.Focus()
        Return False
      Else
        If Not isRegister Then SetReasonInfo(strMemoCode, strName)
        Return True
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function


#End Region

#Region "Search"
  ''' <summary>
  ''' Display search screen (Search items like press F8)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub SearchItem(ByVal sender As System.Object, Optional e As System.ComponentModel.CancelEventArgs = Nothing)
    Dim _CodeSet As PcaCodeSet = Nothing
    Dim itemName As String = String.Empty
    Dim newCode As String = String.Empty
    Dim location As Point
    Dim strCurrentCode As String = String.Empty
    Dim SMS As New SMS(connector)
    Dim EMS As New EMS(connector)

    Try
      Dim denpyoDialog As SLDenSearchDialog = New SLDenSearchDialog(connector)
      Dim MasterDialog As SLMasterSearchDialog = New SLMasterSearchDialog(connector)

      Select Case sender.GetType.ToString
        Case GetType(PCA.Controls.PcaCodeSet).ToString, GetType(PCA.Controls.PcaCommandManager).ToString ' "PCA.Controls.PcaCommandManager"
          If sender.GetType.ToString = GetType(PCA.Controls.PcaCodeSet).ToString Then
            _CodeSet = DirectCast(sender, PcaCodeSet)
            location = Tools.ControlTool.GetDialogLocation(_CodeSet.ReferButton)
          Else
            'location
            Select Case m_strSearchItem
              Case setProduct.Name
                location = Tools.ControlTool.GetDialogLocation(setProduct.ReferButton)
              Case setWarehouse.Name
                location = Tools.ControlTool.GetDialogLocation(setWarehouse.ReferButton)
              Case setReasonCode.Name
                location = Tools.ControlTool.GetDialogLocation(setReasonCode.ReferButton)

            End Select

          End If

          Select Case If(sender.GetType.ToString = GetType(PCA.Controls.PcaCodeSet).ToString, _CodeSet.Name, m_strSearchItem)
            Case setProduct.Name
              strCurrentCode = setProduct.CodeText
              newCode = MasterDialog.ShowReferSmsDialog(strCurrentCode, location)
            Case setWarehouse.Name
              strCurrentCode = setWarehouse.CodeText
              newCode = MasterDialog.ShowReferSokoDialog(strCurrentCode, location)
            Case setReasonCode.Name
              strCurrentCode = setReasonCode.CodeText
              newCode = MasterDialog.ShowReferTekiyoDialog(strCurrentCode, location)
          End Select

          If String.IsNullOrEmpty(newCode) Then
            If Not e Is Nothing Then e.Cancel = True
          Else
            If strCurrentCode = newCode Then
              'No changes
              Return
            End If

            Select Case If(sender.GetType.ToString = GetType(PCA.Controls.PcaCodeSet).ToString, _CodeSet.Name, m_strSearchItem)
              Case setProduct.Name
                If SMS.ReadByID(newCode) Then
                  SetProductInfo(SMS)
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If

              Case setWarehouse.Name
                Dim strName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.Warehouse, newCode)
                If strName.Trim.Length > 0 Then
                  SetWarehouseInfo(newCode, strName)
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If

              Case setReasonCode.Name
                Dim strName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, newCode)
                If strName.Trim.Length > 0 Then
                  SetReasonInfo(newCode, strName)
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If


            End Select
          End If

      End Select

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub



#End Region

#Region "Sub Form Method"
  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then
      connector.LogOffSystem()
    End If
  End Sub
#End Region

#Region " PcaCodeSet Methods"

  Private Sub CodeSet_CodeTextEnter(sender As System.Object, e As System.EventArgs) Handles setProduct.CodeTextEnter, setWarehouse.CodeTextEnter, setReasonCode.CodeTextEnter
    Dim myCodeSet As PcaCodeSet
    Try
      Select Case sender.GetType.ToString
        Case GetType(PCA.Controls.PcaCodeSet).ToString
          myCodeSet = DirectCast(sender, PcaCodeSet)
          m_strSearchItem = myCodeSet.Name
      End Select
      PcaFunctionCommandRefer.Enabled = True

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub CodeSet_CodeTextLeave(sender As System.Object, e As System.EventArgs) Handles setProduct.CodeTextLeave, setWarehouse.CodeTextLeave, setReasonCode.CodeTextLeave
    Dim myCodeSet As PcaCodeSet
    Try
      Select Case sender.GetType.ToString
        Case GetType(PCA.Controls.PcaCodeSet).ToString
          myCodeSet = DirectCast(sender, PcaCodeSet)
          m_strSearchItem = String.Empty
          PcaFunctionCommandRefer.Enabled = False
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub CodeSet_Enter(sender As System.Object, e As System.EventArgs) Handles setProduct.Enter, setWarehouse.Enter, setReasonCode.Enter
    Try
      Dim objCodeSet As PcaCodeSet = DirectCast(sender, PcaCodeSet)
      m_strCurCode = objCodeSet.CodeText
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Search Master 
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub CodeSet_ClickReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setProduct.ClickReferButton2, _
                                                                                                                     setWarehouse.ClickReferButton2, _
                                                                                                                     setReasonCode.ClickReferButton2
    'Display Refer screen
    SearchItem(sender, e)

  End Sub

#End Region


  Private Sub rdoKbn_EnterItem(sender As System.Object, e As PCA.Controls.MultiRadioItemEventArgs) Handles rdoKbn.EnterItem
    Try
      If m_enmCurrentRdo <> e.ItemIndex Then
        numAdjustNumber.Text = String.Empty
        numAdjustQuantity.Text = String.Empty
        m_enmCurrentRdo = DirectCast(e.ItemIndex, RadioType)
      End If

      m_isDataChanged = True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  Private Sub HeaderItems_Changed(sender As System.Object, e As System.EventArgs) Handles ldtAdjustmentDate.DateChanged, setProduct.CodeTextChanged, _
                                                                                           setWarehouse.CodeTextChanged, ltxtSlipNo2.TextChanged, _
                                                                                           setReasonCode.CodeTextChanged, numAdjustNumber.TextChanged, _
                                                                                           numAdjustQuantity.TextChanged
    Try

      m_isDataChanged = True

      If ldtAdjustmentDate.IntDate <= m_intCutoffDate Then
        m_isDisallowRegByDate = True
      Else
        m_isDisallowRegByDate = False
      End If

    Catch ex As Exception

    End Try
  End Sub

#Region " Insert"

  ''' <summary>
  ''' Save Data when press F12
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Insert new data in Create mode</remarks>
  Private Function InsertData() As Boolean
    Dim isSuccess As Boolean = True
    Dim session As ICustomSession
    Dim SL_ZHKClass As SL_ZHK = New SL_ZHK(connector)
    Dim SL_ZDNClass As SL_ZDN = New SL_ZDN(connector)

    Try
      session = connector.CreateTransactionalSession
      isSuccess = InsertZADJ(session)
      m_IntSlipNo = SL_ZADJClass.sl_zadj_denno

      isSuccess = If(isSuccess, SetToZHK(SL_ZHKClass), False)
      isSuccess = If(isSuccess, SL_ZHKClass.Insert(session), False)

      isSuccess = If(isSuccess, SetToZDN(SL_ZDNClass), False)
      isSuccess = If(isSuccess, SL_ZDNClass.UpdateNumberQuantity(False, session), False)

      If isSuccess Then
        session.Commit()
        session.Dispose()
        SLCmnFunction.ShowToolTip("伝票番号：" & SL_ZADJClass.sl_zadj_denno & " で登録されました。", _
                                  SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, SLConstants.DURATION, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
      Else
        session.Rollback()
        session.Dispose()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function

  ''' <summary>
  ''' If cell is not empty, save cell text value to table
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertZADJ(ByRef session As ICustomSession) As Boolean 'Is Register or Edit
    Try

      Dim intAdjustNum As Integer = 0
      Dim decAdjustQty As Decimal = 0D

      Integer.TryParse(numAdjustNumber.Text, intAdjustNum)
      Decimal.TryParse(numAdjustQuantity.Text, decAdjustQty)
      With SL_ZADJClass

        .sl_zadj_denno2 = ltxtSlipNo2.Text
        .sl_zadj_date = ldtAdjustmentDate.IntDate
        .sl_zadj_scd = setProduct.CodeText
        .sl_zadj_souko = setWarehouse.CodeText

        Select Case m_enmCurrentRdo
          Case RadioType.Ajustment_WH
            .sl_zadj_iokbn = SLConstants.SL_ZADJType.Warehousing
          Case RadioType.Ajustment_Shipping
            .sl_zadj_iokbn = SLConstants.SL_ZADJType.Shipping
        End Select

        .sl_zadj_suryo = decAdjustQty
        .sl_zadj_honsu = intAdjustNum

        .sl_zadj_honsu = intAdjustNum

        .sl_zadj_tekcd = setReasonCode.CodeText
        .sl_zadj_tekmei = setReasonContent.CodeText

        .sl_zadj_zdn_id = m_intStockID

        .sl_zadj_insdate = Now
        .sl_zadj_insuser = connector.UserId

      End With
      Return SL_ZADJClass.Insert(session)

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
    End Try
  End Function

#End Region

#Region " Update"

  ''' <summary>
  ''' Save Data when press F12
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Update new data in Modyfy mode</remarks>
  Private Function UpdateData() As Boolean
    Dim isSuccess As Boolean = True
    Dim session As ICustomSession
    Dim SL_ZHKClass As SL_ZHK = New SL_ZHK(connector)
    Dim SL_ZDNClass As SL_ZDN = New SL_ZDN(connector)

    Try
      session = connector.CreateTransactionalSession

      isSuccess = DeleteStockData(session)

      isSuccess = If(isSuccess, UpdateZADJ(session), False)

      isSuccess = If(isSuccess, SetToZHK(SL_ZHKClass), False)
      isSuccess = If(isSuccess, SL_ZHKClass.Insert(session), False)

      isSuccess = If(isSuccess, SetToZDN(SL_ZDNClass), False)
      isSuccess = If(isSuccess, SL_ZDNClass.UpdateNumberQuantity(False, session), False)


      If isSuccess Then
        session.Commit()
        session.Dispose()
        SLCmnFunction.ShowToolTip(SLConstants.CommonMessage.SUCCESS_CARRYOVER, _
                                  SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, SLConstants.DURATION, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
      Else
        session.Rollback()
        session.Dispose()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess

  End Function

  ''' <summary>
  ''' If cell is not empty, save cell text value to table
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateZADJ(ByRef session As ICustomSession) As Boolean 'Is Register or Edit
    Try
      Dim intAdjustNum As Integer = 0
      Dim decAdjustQty As Decimal = 0D

      Integer.TryParse(numAdjustNumber.Text, intAdjustNum)
      Decimal.TryParse(numAdjustQuantity.Text, decAdjustQty)

      With SL_ZADJClass

        .sl_zadj_denno2 = ltxtSlipNo2.Text
        .sl_zadj_date = ldtAdjustmentDate.IntDate
        .sl_zadj_scd = setProduct.CodeText
        .sl_zadj_souko = setWarehouse.CodeText

        Select Case m_enmCurrentRdo
          Case RadioType.Ajustment_WH
            .sl_zadj_iokbn = SLConstants.SL_ZADJType.Warehousing
          Case RadioType.Ajustment_Shipping
            .sl_zadj_iokbn = SLConstants.SL_ZADJType.Shipping
        End Select

        .sl_zadj_suryo = decAdjustQty
        .sl_zadj_honsu = intAdjustNum


        .sl_zadj_tekcd = setReasonCode.CodeText
        .sl_zadj_tekmei = setReasonContent.CodeText

        .sl_zadj_zdn_id = m_intStockID

        .sl_zadj_upddate = Now
        .sl_zadj_upduser = connector.UserId

      End With

      Return SL_ZADJClass.Update(session)

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
    End Try
  End Function


#End Region

#Region " Delete"

  Private Function DeleteStockData(ByRef session As ICustomSession) As Boolean

    Try
      Dim isSuccess As Boolean = True
      Dim SL_ZHKClass As SL_ZHK = New SL_ZHK(connector)
      Dim SL_ZDNClass As SL_ZDN = New SL_ZDN(connector)

      If isSuccess Then
        If SetToZHK(SL_ZHKClass, True) Then isSuccess = SL_ZHKClass.DeleteInventoryWithSEQ(session) Else isSuccess = False

        If isSuccess Then
          If SetToZDN(SL_ZDNClass, True) Then isSuccess = SL_ZDNClass.UpdateNumberQuantity(True, session) Else isSuccess = False
        End If

      End If
      Return isSuccess
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

  Private Function Delete(ByRef session As ICustomSession) As Boolean
    Try
      Dim isSuccess As Boolean = True

      isSuccess = DeleteStockData(session)

      isSuccess = If(isSuccess, SL_ZADJClass.Delete(session), False)


      Return isSuccess
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

#End Region

#Region " Save/Set Data"

  Private Function SetToZDN(ByVal SL_ZDNClass As SL_ZDN, Optional ByVal isRelay As Boolean = False) As Boolean
    Dim intCoefficient As Integer
    Dim intAdjustNum As Integer = 0
    Dim decAdjustQty As Decimal = 0D

    Integer.TryParse(numAdjustNumber.Text, intAdjustNum)
    Decimal.TryParse(numAdjustQuantity.Text, decAdjustQty)

    Select Case m_enmCurrentRdo
      Case RadioType.Ajustment_Shipping, RadioType.Inventry_Shipping
        intCoefficient = 1
      Case Else
        intCoefficient = -1
    End Select

    Try
      With SL_ZDNClass
        .sl_zdn_id = m_intStockID
        .sl_zdn_relay_zdnid = m_intStockIDOld
        If isRelay Then
          .sl_zdn_nyukohonsu = m_intAdjustNumOld * intCoefficient * -1
          .sl_zdn_nyukosuryo = m_decAdjustQtyOld * intCoefficient * -1
        Else
          .sl_zdn_nyukohonsu = intAdjustNum * intCoefficient
          .sl_zdn_nyukosuryo = decAdjustQty * intCoefficient
        End If

      End With
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function SetToZHK(ByRef SL_ZHK_Class As SL_ZHK, Optional ByVal isRelay As Boolean = False) As Boolean
    Dim is_success As Boolean = True
    Dim intCoefficient As Integer

    Dim intAdjustNum As Integer = 0
    Dim decAdjustQty As Decimal = 0D

    Integer.TryParse(numAdjustNumber.Text, intAdjustNum)
    Decimal.TryParse(numAdjustQuantity.Text, decAdjustQty)

    Select Case m_enmCurrentRdo
      Case RadioType.Ajustment_Shipping, RadioType.Inventry_Shipping
        intCoefficient = -1
      Case Else
        intCoefficient = 1
    End Select

    Try
      With SL_ZHK_Class
        If isRelay Then
          .sl_zhk_zdn_id = m_intStockIDOld
        Else
          .sl_zhk_zdn_id = m_intStockID
        End If

        .sl_zhk_datakbn = SLConstants.SL_ZHKSlipType.WarehouseAdjustment
        .sl_zhk_dataid = SL_ZADJClass.sl_zadj_id
        .sl_zhk_seq = 1  'only 1 lot of each row
        .sl_zhk_date = ldtAdjustmentDate.IntDate

        .sl_zhk_honsu = intAdjustNum * intCoefficient
        .sl_zhk_suryo = decAdjustQty * intCoefficient

        .sl_zhk_insdate = Now
        .sl_zhk_insuser = connector.UserId
        .sl_zhk_upddate = Now
        .sl_zhk_upduser = connector.UserId
      End With
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      is_success = False
    End Try
    Return is_success
  End Function

#End Region

End Class

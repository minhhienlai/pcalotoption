﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  Public Class DataGridViewEx
    Inherits DataGridView
    'Inherits System.Windows.Forms.DataGridView

    <System.Security.Permissions.UIPermission( _
        System.Security.Permissions.SecurityAction.Demand, _
        Window:=System.Security.Permissions.UIPermissionWindow.AllWindows)> _
    Protected Overrides Function ProcessDialogKey( _
                ByVal keyData As Keys) As Boolean
      'Enterキーが押された時は、Tabキーが押されたようにする
      If (keyData And Keys.KeyCode) = Keys.Enter Then
        Return Me.ProcessTabKey(keyData)
      End If
      Return MyBase.ProcessDialogKey(keyData)
    End Function

    <System.Security.Permissions.SecurityPermission( _
        System.Security.Permissions.SecurityAction.Demand, _
        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)> _
    Protected Overrides Function ProcessDataGridViewKey( _
                ByVal e As KeyEventArgs) As Boolean
      'Enterキーが押された時は、Tabキーが押されたようにする
      If e.KeyCode = Keys.Enter Then
        Return Me.ProcessTabKey(e.KeyCode)
      ElseIf e.KeyCode = Keys.Delete Then
        Return Me.ProcessDeleteKey(e.KeyCode)
      End If
      Return MyBase.ProcessDataGridViewKey(e)
    End Function
  End Class

  'Windows フォーム デザイナで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
  'Windows フォーム デザイナを使用して変更できます。  
  'コード エディタを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.ファイルToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.閉じるToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.編集ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.削除ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.toolSripSeperator = New System.Windows.Forms.ToolStripSeparator()
    Me.ToolStripButtonRefresh = New System.Windows.Forms.ToolStripButton()
    Me.toolSripSeperator2 = New System.Windows.Forms.ToolStripSeparator()
    Me.ToolStripButtonStop = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
    Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
    Me.statusStrip = New System.Windows.Forms.StatusStrip()
    Me.statusLabel = New System.Windows.Forms.ToolStripStatusLabel()
    Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
    Me.EventLog1 = New System.Diagnostics.EventLog()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefresh = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandStop = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager = New PCA.Controls.PcaCommandManager(Me.components)
    Me.CommandClose = New PCA.Controls.PcaCommandItem()
    Me.CommandDelete = New PCA.Controls.PcaCommandItem()
    Me.CommandRefresh = New PCA.Controls.PcaCommandItem()
    Me.CommandStop = New PCA.Controls.PcaCommandItem()
    Me.FileMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ContextMenuStripTable = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.TscMeisaiTable1 = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.cbLotNew = New PCA.Controls.PcaLabeledComboBox()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.statusStrip.SuspendLayout()
    CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ファイルToolStripMenuItem, Me.編集ToolStripMenuItem})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(542, 24)
    Me.MenuStrip1.TabIndex = 5
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'ファイルToolStripMenuItem
    '
    Me.PcaCommandManager.SetCommandItem(Me.ファイルToolStripMenuItem, Nothing)
    Me.ファイルToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.閉じるToolStripMenuItem})
    Me.ファイルToolStripMenuItem.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ファイルToolStripMenuItem.Name = "ファイルToolStripMenuItem"
    Me.ファイルToolStripMenuItem.Size = New System.Drawing.Size(96, 20)
    Me.ファイルToolStripMenuItem.Text = "ファイル(&F)"
    '
    '閉じるToolStripMenuItem
    '
    Me.PcaCommandManager.SetCommandItem(Me.閉じるToolStripMenuItem, Me.CommandClose)
    Me.閉じるToolStripMenuItem.Name = "閉じるToolStripMenuItem"
    Me.閉じるToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
    Me.閉じるToolStripMenuItem.Text = "閉じる(&X)　F12"
    '
    '編集ToolStripMenuItem
    '
    Me.PcaCommandManager.SetCommandItem(Me.編集ToolStripMenuItem, Nothing)
    Me.編集ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.削除ToolStripMenuItem})
    Me.編集ToolStripMenuItem.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.編集ToolStripMenuItem.Name = "編集ToolStripMenuItem"
    Me.編集ToolStripMenuItem.Size = New System.Drawing.Size(68, 20)
    Me.編集ToolStripMenuItem.Text = "編集(&E)"
    '
    '削除ToolStripMenuItem
    '
    Me.PcaCommandManager.SetCommandItem(Me.削除ToolStripMenuItem, Me.CommandDelete)
    Me.削除ToolStripMenuItem.Name = "削除ToolStripMenuItem"
    Me.削除ToolStripMenuItem.Size = New System.Drawing.Size(123, 22)
    Me.削除ToolStripMenuItem.Text = "削除(&D)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.AutoSize = False
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.toolSripSeperator, Me.ToolStripButtonRefresh, Me.toolSripSeperator2, Me.ToolStripButtonStop, Me.ToolStripSeparator1, Me.ToolStripButton1})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(542, 33)
    Me.ToolStrip1.TabIndex = 9
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.ToolStripButtonClose.AutoSize = False
    Me.PcaCommandManager.SetCommandItem(Me.ToolStripButtonClose, Me.CommandClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(48, 38)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'toolSripSeperator
    '
    Me.PcaCommandManager.SetCommandItem(Me.toolSripSeperator, Nothing)
    Me.toolSripSeperator.Name = "toolSripSeperator"
    Me.toolSripSeperator.Size = New System.Drawing.Size(6, 33)
    '
    'ToolStripButtonRefresh
    '
    Me.ToolStripButtonRefresh.AutoSize = False
    Me.PcaCommandManager.SetCommandItem(Me.ToolStripButtonRefresh, Me.CommandRefresh)
    Me.ToolStripButtonRefresh.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonRefresh.Image = CType(resources.GetObject("ToolStripButtonRefresh.Image"), System.Drawing.Image)
    Me.ToolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonRefresh.Name = "ToolStripButtonRefresh"
    Me.ToolStripButtonRefresh.Size = New System.Drawing.Size(48, 38)
    Me.ToolStripButtonRefresh.Text = "最新"
    Me.ToolStripButtonRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'toolSripSeperator2
    '
    Me.PcaCommandManager.SetCommandItem(Me.toolSripSeperator2, Nothing)
    Me.toolSripSeperator2.Name = "toolSripSeperator2"
    Me.toolSripSeperator2.Size = New System.Drawing.Size(6, 33)
    '
    'ToolStripButtonStop
    '
    Me.ToolStripButtonStop.AutoSize = False
    Me.PcaCommandManager.SetCommandItem(Me.ToolStripButtonStop, Me.CommandStop)
    Me.ToolStripButtonStop.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonStop.Image = CType(resources.GetObject("ToolStripButtonStop.Image"), System.Drawing.Image)
    Me.ToolStripButtonStop.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonStop.Name = "ToolStripButtonStop"
    Me.ToolStripButtonStop.Size = New System.Drawing.Size(48, 38)
    Me.ToolStripButtonStop.Text = "中止"
    Me.ToolStripButtonStop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripSeparator1
    '
    Me.PcaCommandManager.SetCommandItem(Me.ToolStripSeparator1, Nothing)
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 33)
    '
    'ToolStripButton1
    '
    Me.PcaCommandManager.SetCommandItem(Me.ToolStripButton1, Me.CommandDelete)
    Me.ToolStripButton1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
    Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButton1.Name = "ToolStripButton1"
    Me.ToolStripButton1.Size = New System.Drawing.Size(39, 30)
    Me.ToolStripButton1.Text = "削除"
    Me.ToolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'statusStrip
    '
    Me.statusStrip.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.statusStrip.AutoSize = False
    Me.statusStrip.Dock = System.Windows.Forms.DockStyle.None
    Me.statusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.statusLabel, Me.ToolStripStatusLabel2})
    Me.statusStrip.Location = New System.Drawing.Point(4, 479)
    Me.statusStrip.Name = "statusStrip"
    Me.statusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 11, 0)
    Me.statusStrip.Size = New System.Drawing.Size(534, 19)
    Me.statusStrip.TabIndex = 8
    Me.statusStrip.Text = "StatusStrip1"
    '
    'statusLabel
    '
    Me.PcaCommandManager.SetCommandItem(Me.statusLabel, Nothing)
    Me.statusLabel.Name = "statusLabel"
    Me.statusLabel.Size = New System.Drawing.Size(0, 14)
    '
    'ToolStripStatusLabel2
    '
    Me.PcaCommandManager.SetCommandItem(Me.ToolStripStatusLabel2, Nothing)
    Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
    Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(0, 14)
    '
    'EventLog1
    '
    Me.EventLog1.SynchronizingObject = Me
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.PcaFunctionBar1.BackColor = System.Drawing.SystemColors.Control
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandClose, Me.PcaFunctionCommandRefresh, Me.PcaFunctionCommandStop})
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 455)
    Me.PcaFunctionBar1.Margin = New System.Windows.Forms.Padding(2)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(542, 28)
    Me.PcaFunctionBar1.TabIndex = 7
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager.SetCommandItem(Me.PcaFunctionCommandClose, Me.CommandClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'PcaFunctionCommandRefresh
    '
    Me.PcaCommandManager.SetCommandItem(Me.PcaFunctionCommandRefresh, Me.CommandRefresh)
    Me.PcaFunctionCommandRefresh.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandRefresh.Tag = Nothing
    Me.PcaFunctionCommandRefresh.Text = "最新"
    '
    'PcaFunctionCommandStop
    '
    Me.PcaCommandManager.SetCommandItem(Me.PcaFunctionCommandStop, Me.CommandStop)
    Me.PcaFunctionCommandStop.FunctionKey = PCA.Controls.FunctionKey.F6
    Me.PcaFunctionCommandStop.Tag = Nothing
    Me.PcaFunctionCommandStop.Text = "中止"
    '
    'PcaCommandManager
    '
    Me.PcaCommandManager.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.CommandClose, Me.CommandRefresh, Me.CommandStop, Me.CommandDelete})
    Me.PcaCommandManager.Parent = Me
    '
    'CommandClose
    '
    Me.CommandClose.CommandName = "閉じる"
    '
    'CommandRefresh
    '
    Me.CommandRefresh.CommandId = 5
    Me.CommandRefresh.CommandName = "最新"
    '
    'CommandStop
    '
    Me.CommandStop.CommandName = "中止"
    '
    'FileMenuItemClose
    '
    Me.PcaCommandManager.SetCommandItem(Me.FileMenuItemClose, Me.CommandClose)
    Me.FileMenuItemClose.Name = "FileMenuItemClose"
    Me.FileMenuItemClose.ShortcutKeys = System.Windows.Forms.Keys.F12
    Me.FileMenuItemClose.Size = New System.Drawing.Size(32, 19)
    Me.FileMenuItemClose.Text = "登録(&S)"
    '
    'ContextMenuStripTable
    '
    Me.ContextMenuStripTable.Name = "ContextMenuStripTable"
    Me.ContextMenuStripTable.Size = New System.Drawing.Size(61, 4)
    '
    'TscMeisaiTable1
    '
    Me.TscMeisaiTable1.AccessibleRole = System.Windows.Forms.AccessibleRole.None
    Me.TscMeisaiTable1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TscMeisaiTable1.AutoSize = True
    Me.TscMeisaiTable1.BackColor = System.Drawing.Color.White
    Me.TscMeisaiTable1.BEKihonJoho = CType(resources.GetObject("TscMeisaiTable1.BEKihonJoho"), PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.TscMeisaiTable1.BufferRowCount = 0
    Me.TscMeisaiTable1.ContextMenu = Nothing
    Me.TscMeisaiTable1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.TscMeisaiTable1.HeadContextMenu = Nothing
    Me.TscMeisaiTable1.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.TscMeisaiTable1.LastRowInsertMode = True
    Me.TscMeisaiTable1.Location = New System.Drawing.Point(19, 99)
    Me.TscMeisaiTable1.Margin = New System.Windows.Forms.Padding(2)
    Me.TscMeisaiTable1.Name = "TscMeisaiTable1"
    Me.TscMeisaiTable1.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.TscMeisaiTable1.Size = New System.Drawing.Size(513, 338)
    Me.TscMeisaiTable1.TabIndex = 110
    '
    'cbLotNew
    '
    Me.cbLotNew.BackColor = System.Drawing.SystemColors.ButtonFace
    Me.cbLotNew.ClientProcess = Nothing
    Me.cbLotNew.DropDownWidth = 140
    Me.cbLotNew.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.cbLotNew.LabelText = "ロット詳細"
    Me.cbLotNew.Location = New System.Drawing.Point(19, 67)
    Me.cbLotNew.Name = "cbLotNew"
    Me.cbLotNew.SelectedIndex = -1
    Me.cbLotNew.SelectedItem = Nothing
    Me.cbLotNew.SelectedText = ""
    Me.cbLotNew.SelectedValue = Nothing
    Me.cbLotNew.Size = New System.Drawing.Size(280, 22)
    Me.cbLotNew.TabIndex = 106
    '
    'frmMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(542, 497)
    Me.Controls.Add(Me.TscMeisaiTable1)
    Me.Controls.Add(Me.cbLotNew)
    Me.Controls.Add(Me.statusStrip)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(548, 535)
    Me.Name = "frmMain"
    Me.Text = "ロット詳細マスタ"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    Me.statusStrip.ResumeLayout(False)
    Me.statusStrip.PerformLayout()
    CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents ファイルToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 閉じるToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 編集ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents 削除ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  'Friend WithEvents dgv As LottoShousai.Form1.DataGridViewEx
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonRefresh As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonStop As System.Windows.Forms.ToolStripButton
  Friend WithEvents statusStrip As System.Windows.Forms.StatusStrip
  Friend WithEvents EventLog1 As System.Diagnostics.EventLog
  'Hien added
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaCommandManager As PCA.Controls.PcaCommandManager
  Protected WithEvents CommandClose As PCA.Controls.PcaCommandItem
  Friend WithEvents FileMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Protected WithEvents CommandRefresh As PCA.Controls.PcaCommandItem
  Protected WithEvents CommandStop As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandRefresh As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandStop As PCA.Controls.PcaFunctionCommand
  Friend WithEvents statusLabel As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents ContextMenuStripTable As System.Windows.Forms.ContextMenuStrip
  Friend WithEvents TscMeisaiTable1 As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents cbLotNew As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents toolSripSeperator As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents toolSripSeperator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents CommandDelete As PCA.Controls.PcaCommandItem
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
End Class

﻿Imports PCA
Imports PCA.ApplicationIntegration
Imports PCA.Kon.Integration
Imports Sunloft.PcaConfig
Imports System.Xml
Imports System.IO
Imports System.Globalization  '週番号からのデータ取得用
Imports System.Text
Imports System.Net
Imports System.Collections.Specialized
Imports System.Environment
Imports System.ComponentModel

Imports PCA.Controls
Imports PCA.TSC.Kon.BusinessEntity
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports Sunloft.PCAIF
Imports Sunloft.Message
Imports Sunloft.Common

Public Class frmMain
  Private Const CODE_MAX_LENGTH As Integer = 6
  Private Const NAME_MAX_LENGTH As Integer = 40
  Private Const PG_NAME = "ロット詳細マスタ"
  Private Const ONLY_NUMBER_ERROR_MESSAGE = "コードには数字のみ指定することが可能です。"
  Private Const ALREADY_EXISTED_ERROR_MESSAGE = "指定したコードは既に使用されています。"
  Private Const IS_BEING_USED_ERROR = "既に伝票で使用されているため削除できません"
  Private Const FIRST_TABLE = 1

  Private connector As IIntegratedApplication = Nothing
  Private m_appClass As ConnectToPCA = New ConnectToPCA()

  Private m_SL_LMBClass As SL_LMB
  Private SL_LMDCLass As SL_LMD
  Private LotName(10) As String
  Private SearchResult As SL_LMD()

  Private CurrentTable As Integer = 0 'Table SL_LMD 1->10
  Private m_intRowCount As Integer = 0
  Private m_intRowIndex As Integer = -1
  Private objOldCellValue As Object

  Private codeInputMessage As String = "半角 " & CODE_MAX_LENGTH & " 桁で入力してください"
  Private nameInputMessage As String = "全角" & NAME_MAX_LENGTH / 2 & "(半角" & NAME_MAX_LENGTH & "文字)以内でで入力してください"

#Region " Init"
  Public Sub New()
    ' この呼び出しは、Windows フォーム デザイナで必要です。
    InitializeComponent()

    m_appClass.ConnectToPCA()
    connector = m_appClass.connector

    m_SL_LMBClass = New SL_LMB(connector)
    m_SL_LMBClass.ReadOnlyRow()

    SL_LMDCLass = New SL_LMD(connector)

    DisplayComboBox()
    InitTable()

    If m_SL_LMBClass.sl_lmb_maxlotop > 0 Then cbLotNew.SelectedIndex = 0
  End Sub

  Private Sub DisplayComboBox()
    Try
      LotName(0) = m_SL_LMBClass.sl_lmb_label1
      LotName(1) = m_SL_LMBClass.sl_lmb_label2
      LotName(2) = m_SL_LMBClass.sl_lmb_label3
      LotName(3) = m_SL_LMBClass.sl_lmb_label4
      LotName(4) = m_SL_LMBClass.sl_lmb_label5
      LotName(5) = m_SL_LMBClass.sl_lmb_label6
      LotName(6) = m_SL_LMBClass.sl_lmb_label7
      LotName(7) = m_SL_LMBClass.sl_lmb_label8
      LotName(8) = m_SL_LMBClass.sl_lmb_label9
      LotName(9) = m_SL_LMBClass.sl_lmb_label10

      cbLotNew.Items.Clear()
      For stepper = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
        cbLotNew.Items.Add(LotName(stepper))
      Next

    Catch ex As Exception
      MsgBox(ex.Message, vbExclamation)
    End Try
  End Sub

  Private Sub InitTable()

    TscMeisaiTable1.DataRowCount = 99

    TscMeisaiTable1.InputFrameMode = True
    TscMeisaiTable1.SelectRowMode = True
    InitTableHeader()
    InitTableBody()

    TscMeisaiTable1.UpdateTable()
    TscMeisaiTable1.BodyColumns(1).Cells(0).ImeMode = Windows.Forms.ImeMode.Hiragana

    TscMeisaiTable1.BodyColumns.DefaultStyle.BackColor = Color.White
    TscMeisaiTable1.BodyColumns.DefaultAltStyle.BackColor = Color.AliceBlue

  End Sub

  Private Sub InitTableHeader()
    TscMeisaiTable1.HeadTextHeight = 1
    TscMeisaiTable1.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
    TscMeisaiTable1.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

    InitHColumn(columnName.code, 10, False)
    InitHColumn(columnName.name, 2)
    InitHColumn(columnName.ID, 0)

  End Sub

  Private Sub InitHColumn(cellname As String, length As Integer, Optional resize As Boolean = True)
    Dim column As PcaColumn
    Dim cell As PcaCell

    column = New PcaColumn(cellname, length)
    column.CanResize = resize
    cell = TscMeisaiTable1.PrepareFixTextCell(cellname)
    column.Cells.Add(cell)
    TscMeisaiTable1.HeadColumns.Add(column)
  End Sub

  Private Sub InitTableBody()

    InitBColumn(columnName.code, CellType.Text, 10, CODE_MAX_LENGTH, True, False)
    InitBColumn(columnName.name, CellType.Text, 2, NAME_MAX_LENGTH, True, True, StringAlignment.Near)
    InitBColumn(columnName.ID, CellType.Number, 0, , False, True)

  End Sub

  Private Sub InitBColumn(strCellName As String, celltype As PCA.Controls.CellType, intLength As Integer, Optional intMaxLength As Integer = 0, _
                          Optional isEditable As Boolean = True, Optional isResize As Boolean = True, Optional align As StringAlignment = StringAlignment.Center)
    Dim column As PcaColumn
    Dim cell As PcaCell

    column = New PcaColumn(strCellName, intLength)
    column.CanResize = isResize
    cell = New PcaCell(strCellName, celltype)
    cell.EditMode = isEditable
    cell.Enabled = isEditable
    cell.CellStyle.Alignment = align
    If intMaxLength <> 0 Then cell.LimitLength = CShort(intMaxLength)

    column.Cells.Add(cell)
    TscMeisaiTable1.BodyColumns.Add(column)
  End Sub

#End Region

#Region " Private Method"
  ''' <summary>
  ''' 
  ''' </summary>
  Friend Function SearchAndDisplay(ByVal intSelectedIdx As Integer) As Boolean

    Dim is_success As Boolean = True

    Try

      If Not cbLotNew.SelectedIndex = intSelectedIdx - 1 Then cbLotNew.SelectedIndex = intSelectedIdx - 1

      SL_LMDCLass.number = intSelectedIdx
      SearchResult = SL_LMDCLass.ReadAll()

      TscMeisaiTable1.DataRowCount = SearchResult.Length + 1

      For intCount As Integer = 0 To SearchResult.Length - 1
        TscMeisaiTable1.SetCellValue(intCount, columnName.code, SearchResult(intCount).sl_lmd_ldcd)
        TscMeisaiTable1.SetCellValue(intCount, columnName.name, SearchResult(intCount).sl_lmd_mei)
        TscMeisaiTable1.SetCellValue(intCount, columnName.ID, SearchResult(intCount).sl_lmd_id)
      Next

      CurrentTable = intSelectedIdx
      m_intRowCount = SearchResult.Length
      If SearchResult.Length = 0 Then
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, PG_NAME, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, CommandRefresh.CommandId)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      is_success = False
    End Try
    Return is_success
  End Function

  Private Function ValidateCode(ByVal strCode As String, ByRef strErrMsg As String) As String
    Dim stringQuery = From c In strCode Where Char.IsDigit(c) Select c
    'Check if code contains only 0-9
    If stringQuery.Count <> strCode.Length Then
      strErrMsg = ONLY_NUMBER_ERROR_MESSAGE
      Return String.Empty
    Else
      strCode = SLCmnFunction.standardlizeCode(strCode, CODE_MAX_LENGTH) 'Add 000～
      'Check if code is duplicated
      For Each item In SearchResult
        If item.sl_lmd_ldcd = strCode Then
          strErrMsg = ALREADY_EXISTED_ERROR_MESSAGE
          Return String.Empty
        End If
      Next
    End If
    Return strCode
  End Function

  Private Sub DeleteRecord()
    Try
      If 0 <= m_intRowIndex And m_intRowIndex < m_intRowCount Then
        Dim result As Integer = DisplayBox.ShowWarning(SLConstants.ConfirmMessage.DELETE_CONFIRM_MESSAGE)
        If result = DialogResult.Yes Then
          SL_LMDCLass.sl_lmd_id = CInt(TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.ID))
          SL_LMDCLass.sl_lmd_ldcd = TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.code).ToString.Trim
          SL_LMDCLass.number = CurrentTable
          SL_LMDCLass.Delete() 'If code is being used in SL_NYKH or SL_ZDN, display error message (Do not delete)
          If SL_LMDCLass.isBeingUsed Then
            SLCmnFunction.ShowToolTip(IS_BEING_USED_ERROR, PG_NAME, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, CommandRefresh.CommandId)
          End If
        End If
        TscMeisaiTable1.ClearCellValues()
        SearchAndDisplay(CurrentTable)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region "Table Event"

  Private Sub TscMeisaiTable1_LostCellFocus(sender As Object, args As PCA.Controls.CellEventArgs) Handles TscMeisaiTable1.LostCellFocus
    statusLabel.Text = String.Empty
  End Sub

  ''' <summary>
  ''' Enter input row
  ''' </summary>
  Private Sub TscMeisaiTable1_EnterInputRow(sender As Object, args As PCA.Controls.InputRowEventArgs) Handles TscMeisaiTable1.EnterInputRow
    m_intRowIndex = args.RowIndex
    If m_intRowIndex < m_intRowCount Then ' Update -> Focus on Name column
      TscMeisaiTable1.BodyColumns(0).Cells(0).EditMode = False
      TscMeisaiTable1.BodyColumns(0).Cells(0).Enabled = False
      TscMeisaiTable1.BodyColumns(1).Cells(0).EditMode = True
      TscMeisaiTable1.BodyColumns(1).Cells(0).Enabled = True
      objOldCellValue = TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.name)
      statusLabel.Text = nameInputMessage
    ElseIf m_intRowIndex = m_intRowCount Then 'Insert -> Focus on Code column
      TscMeisaiTable1.BodyColumns(0).Cells(0).EditMode = True
      TscMeisaiTable1.BodyColumns(0).Cells(0).Enabled = True
    End If
  End Sub

  Private Sub TscMeisaiTable1_GotCellFocus(sender As Object, args As PCA.Controls.CellEventArgs) Handles TscMeisaiTable1.GotCellFocus
    Dim mt As TSC.Kon.Tools.TscMeisaiTable = DirectCast(sender, TSC.Kon.Tools.TscMeisaiTable)
    If m_intRowIndex > m_intRowCount Then
      mt.BodyColumns(0).Cells(0).EditMode = True
      mt.BodyColumns(0).Cells(0).Enabled = True
      mt.SetCellFocus(m_intRowCount, columnName.code)
      m_intRowIndex = m_intRowCount
    ElseIf m_intRowIndex = m_intRowCount Then
      'If code column is got focus, display code input message
      If args.CellName.ToString = columnName.code Then
        statusLabel.Text = codeInputMessage
        mt.SetCellFocus(m_intRowCount, columnName.code)
      ElseIf args.CellName.ToString = columnName.name Then
        If Not mt.GetCellValue(m_intRowCount, columnName.code) Is Nothing AndAlso mt.GetCellValue(m_intRowCount, columnName.code).ToString <> String.Empty Then
          'If code is inputed and name column is got focus, display name input message
          statusLabel.Text = nameInputMessage
          mt.SetCellFocus(m_intRowCount, columnName.name)
        Else
          'If name column is got focus but code is not inputed, focus back to code. (User can input name only when valid code is inputed
          statusLabel.Text = codeInputMessage
          mt.SetCellFocus(m_intRowCount, columnName.code)
        End If

      End If
    End If
  End Sub

  Private Sub TscMeisaiTable1_LeaveInputRow(sender As Object, args As PCA.Controls.InputRowEventArgs) Handles TscMeisaiTable1.LeaveInputRow
    Try
      If m_intRowIndex < m_intRowCount Then 'Update
        Dim newName As String = String.Empty
        If Not TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.name) Is Nothing Then
          newName = TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.name).ToString.Trim
        End If
        If Not objOldCellValue Is Nothing AndAlso newName <> objOldCellValue.ToString AndAlso newName.Length > 0 Then
          SL_LMDCLass.number = CurrentTable
          SL_LMDCLass.sl_lmd_id = CInt(TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.ID))
          SL_LMDCLass.sl_lmd_mei = newName
          SL_LMDCLass.Update()
        End If
        SearchAndDisplay(CurrentTable)
        'Insert
      ElseIf m_intRowIndex = m_intRowCount Then
        'If code and name are valid, insert
        If Not SLCmnFunction.checkObjectNothingEmpty(TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.code)) AndAlso Not _
               SLCmnFunction.checkObjectNothingEmpty(TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.name)) Then
          SL_LMDCLass.number = CurrentTable
          SL_LMDCLass.sl_lmd_ldcd = TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.code).ToString
          SL_LMDCLass.sl_lmd_mei = TscMeisaiTable1.GetCellValue(m_intRowIndex, columnName.name).ToString
          SL_LMDCLass.Insert()
        Else
          ' If user leaves new row after inputing code only, delete code.
          TscMeisaiTable1.SetCellValue(args.RowIndex, columnName.code, String.Empty)
        End If
        SearchAndDisplay(CurrentTable)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Validating Code.Valid code is 6-number-code. Code "000000" is considered valid
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks></remarks>
  Private Sub TscMeisaiTable1_ValidatingCell(sender As Object, args As PCA.Controls.CancelCellEventArgs) Handles TscMeisaiTable1.ValidatingCell
    If args.CellName = columnName.code Then
      If args.NewValue.ToString.Length > 0 Then
        Dim strErrMsg As String = String.Empty
        Dim strNewcode As String = ValidateCode(args.NewValue.ToString, strErrMsg)
        If strNewcode <> String.Empty Then
          TscMeisaiTable1.SetCellValue(m_intRowIndex, columnName.code, strNewcode)
        Else
          DisplayBox.ShowError(strErrMsg)
          args.Cancel = True
        End If
      End If
    End If
  End Sub
#End Region

#Region "PCA Function Bar Group"
  Private Sub PcaCommandManager_Command(sender As System.Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager.Command        '
    If e.CommandItem Is CommandClose Then
      Me.Close()
    ElseIf e.CommandItem Is CommandRefresh Then
      TscMeisaiTable1.ClearCellValues()
      SearchAndDisplay(cbLotNew.SelectedIndex + 1)
    ElseIf e.CommandItem Is CommandStop Then
      TscMeisaiTable1.ClearCellValues()
      SearchAndDisplay(FIRST_TABLE)
    ElseIf e.CommandItem Is CommandDelete Then
      DeleteRecord()
    End If
  End Sub
#End Region

  Private Sub cbLot_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbLotNew.SelectedIndexChanged
    TscMeisaiTable1.ClearCellValues()
    SearchAndDisplay(cbLotNew.SelectedIndex + 1) 'SelectIndec = 0 -> Table SL_LMD1
  End Sub

  Private Sub frmMain_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
    Try
      If Not m_appClass.isAttach Then connector.LogOffSystem()
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub Form1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
    If e.KeyCode = Keys.Delete Then DeleteRecord()
  End Sub
End Class

#Region "Define PCA Table Cell"
''' <summary>
''' 定義クラス
''' </summary>
''' <remarks></remarks>
Public Class columnName
  ''' <summary>
  ''' [数値]ボディセル名
  ''' </summary>
  ''' <remarks></remarks>
  Public Const code As String = "コード"
  ''' <summary>
  ''' [文字列]ボディセル名
  ''' </summary>
  ''' <remarks></remarks>
  Public Const name As String = "名称"
  ''' <summary>
  ''' [金額]ボディセル名
  ''' </summary>
  ''' <remarks></remarks>
  Public Const ID As String = "ID"
End Class
#End Region

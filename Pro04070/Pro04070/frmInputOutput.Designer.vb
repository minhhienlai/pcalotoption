﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StockQuantityZero
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StockQuantityZero))
    Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
    Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.FToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPreview = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemExport = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemReference = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPrint = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPreview = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonExport = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSearch = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.PcaProductCode = New PCA.Controls.PcaRangeCodeSet()
    Me.PcaLotNo = New PCA.Controls.PcaLabeledTextBox()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemPrint = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPreview = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemExport = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandReset = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrint = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPreView = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandExport = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.dgv = New System.Windows.Forms.DataGridView()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.rcsWareHouse = New PCA.Controls.PcaRangeCodeSet()
    Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
    Me.cmbLotDetail1 = New PCA.Controls.PcaComboBox()
    Me.cmbLotDetail2 = New PCA.Controls.PcaComboBox()
    Me.cmbLotDetail3 = New PCA.Controls.PcaComboBox()
    Me.cmbLotInfo1 = New PCA.Controls.PcaComboBox()
    Me.cmbLotInfo2 = New PCA.Controls.PcaComboBox()
    Me.cmbLotInfo3 = New PCA.Controls.PcaComboBox()
    Me.PcalableName1 = New PCA.Controls.PcaTextBox()
    Me.PcalableName2 = New PCA.Controls.PcaTextBox()
    Me.PcalableName3 = New PCA.Controls.PcaTextBox()
    Me.cbStockQuantity = New PCA.Controls.PcaCheckBox()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(1161, 24)
    Me.MenuStrip1.TabIndex = 2000
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'FToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.FToolStripMenuItemFile, Nothing)
    Me.FToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemPrint, Me.ToolStripMenuItemPreview, Me.ToolStripMenuItemExport, Me.ToolStripMenuItemClose})
    Me.FToolStripMenuItemFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.FToolStripMenuItemFile.Name = "FToolStripMenuItemFile"
    Me.FToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.FToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPrint, Me.PcaCommandItemPrint)
    Me.ToolStripMenuItemPrint.Name = "ToolStripMenuItemPrint"
    Me.ToolStripMenuItemPrint.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPrint.Text = "印刷(&P)"
    '
    'ToolStripMenuItemPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPreview, Me.PcaCommandItemPreview)
    Me.ToolStripMenuItemPreview.Name = "ToolStripMenuItemPreview"
    Me.ToolStripMenuItemPreview.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPreview.Text = "印刷プレビュー(&V)"
    '
    'ToolStripMenuItemExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemExport, Me.PcaCommandItemExport)
    Me.ToolStripMenuItemExport.Name = "ToolStripMenuItemExport"
    Me.ToolStripMenuItemExport.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemExport.Text = "出力(&O)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSearch, Me.ToolStripMenuItemReference})
    Me.ToolStripMenuItemEdit.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSearch.Text = "再表示(&R)"
    '
    'ToolStripMenuItemReference
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemReference, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemReference.Name = "ToolStripMenuItemReference"
    Me.ToolStripMenuItemReference.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemReference.Text = "参照(&U)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContent.Text = "目次(&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonPrint, Me.ToolStripButtonPreview, Me.ToolStripButtonExport, Me.ToolStripButtonSearch, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1161, 37)
    Me.ToolStrip1.TabIndex = 1000
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPrint, Me.PcaCommandItemPrint)
    Me.ToolStripButtonPrint.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonPrint.Image = CType(resources.GetObject("ToolStripButtonPrint.Image"), System.Drawing.Image)
    Me.ToolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPrint.Name = "ToolStripButtonPrint"
    Me.ToolStripButtonPrint.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonPrint.Text = "印刷"
    Me.ToolStripButtonPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPreview, Me.PcaCommandItemPreview)
    Me.ToolStripButtonPreview.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonPreview.Image = CType(resources.GetObject("ToolStripButtonPreview.Image"), System.Drawing.Image)
    Me.ToolStripButtonPreview.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPreview.Name = "ToolStripButtonPreview"
    Me.ToolStripButtonPreview.Size = New System.Drawing.Size(81, 34)
    Me.ToolStripButtonPreview.Text = "プレビュー"
    Me.ToolStripButtonPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonExport, Me.PcaCommandItemExport)
    Me.ToolStripButtonExport.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonExport.Image = CType(resources.GetObject("ToolStripButtonExport.Image"), System.Drawing.Image)
    Me.ToolStripButtonExport.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonExport.Name = "ToolStripButtonExport"
    Me.ToolStripButtonExport.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonExport.Text = "出力"
    Me.ToolStripButtonExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSearch, Me.PcaCommandItemSearch)
    Me.ToolStripButtonSearch.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonSearch.Image = CType(resources.GetObject("ToolStripButtonSearch.Image"), System.Drawing.Image)
    Me.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSearch.Name = "ToolStripButtonSearch"
    Me.ToolStripButtonSearch.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonSearch.Text = "再表示"
    Me.ToolStripButtonSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"), System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'PcaProductCode
    '
    Me.PcaProductCode.AutoTopMargin = False
    Me.PcaProductCode.BackColor = System.Drawing.SystemColors.Control
    Me.PcaProductCode.ClientProcess = Nothing
    Me.PcaProductCode.FromNameText = "productCodeFrom"
    Me.PcaProductCode.LimitLength = 12
    Me.PcaProductCode.Location = New System.Drawing.Point(10, 70)
    Me.PcaProductCode.MaxCodeLength = 15
    Me.PcaProductCode.MaxLabelLength = 12
    Me.PcaProductCode.MaxNameLength = 0
    Me.PcaProductCode.Name = "PcaProductCode"
    Me.PcaProductCode.RangeGroupBoxText = "商品"
    Me.PcaProductCode.RangeGroupBoxVisible = False
    Me.PcaProductCode.Size = New System.Drawing.Size(371, 22)
    Me.PcaProductCode.TabIndex = 1
    Me.PcaProductCode.ToNameText = "productCodeTo"
    '
    'PcaLotNo
    '
    Me.PcaLotNo.AutoTextSize = False
    Me.PcaLotNo.ClientProcess = Nothing
    Me.PcaLotNo.LabelText = "ロットNo"
    Me.PcaLotNo.LimitLength = 26
    Me.PcaLotNo.Location = New System.Drawing.Point(10, 94)
    Me.PcaLotNo.MaxLabelLength = 12
    Me.PcaLotNo.MaxTextLength = 26
    Me.PcaLotNo.Name = "PcaLotNo"
    Me.PcaLotNo.Size = New System.Drawing.Size(210, 22)
    Me.PcaLotNo.TabIndex = 2
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemSearch, Me.PcaCommandItemRefer, Me.PcaCommandItemPrint, Me.PcaCommandItemPreview, Me.PcaCommandItemExport, Me.PcaCommandItemClose})
    '
    'PcaCommandItemPrint
    '
    Me.PcaCommandItemPrint.CommandId = 9
    '
    'PcaCommandItemPreview
    '
    Me.PcaCommandItemPreview.CommandId = 9
    '
    'PcaCommandItemExport
    '
    Me.PcaCommandItemExport.CommandId = 10
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    '
    'PcaCommandItemSearch
    '
    Me.PcaCommandItemSearch.CommandId = 5
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandId = 8
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    '
    'PcaFunctionCommandReset
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandReset, Me.PcaCommandItemSearch)
    Me.PcaFunctionCommandReset.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandReset.Tag = Nothing
    Me.PcaFunctionCommandReset.Text = "再表示"
    '
    'PcaFunctionCommandPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrint, Me.PcaCommandItemPrint)
    Me.PcaFunctionCommandPrint.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPrint.Tag = Nothing
    Me.PcaFunctionCommandPrint.Text = "印刷"
    '
    'PcaFunctionCommandPreView
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPreView, Me.PcaCommandItemPreview)
    Me.PcaFunctionCommandPreView.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPreView.ModifierKeys = PCA.Controls.FunctionModifierKeys.Shift
    Me.PcaFunctionCommandPreView.Tag = Nothing
    Me.PcaFunctionCommandPreView.Text = "プレビュー"
    '
    'PcaFunctionCommandExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandExport, Me.PcaCommandItemExport)
    Me.PcaFunctionCommandExport.FunctionKey = PCA.Controls.FunctionKey.F10
    Me.PcaFunctionCommandExport.Tag = Nothing
    Me.PcaFunctionCommandExport.Text = "出力"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandReset, Me.PcaFunctionCommandPrint, Me.PcaFunctionCommandPreView, Me.PcaFunctionCommandExport, Me.PcaFunctionCommandClose})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 443)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1161, 28)
    Me.PcaFunctionBar1.TabIndex = 114
    Me.PcaFunctionBar1.TabStop = False
    '
    'dgv
    '
    Me.dgv.AllowUserToAddRows = False
    Me.dgv.AllowUserToDeleteRows = False
    Me.dgv.AllowUserToResizeRows = False
    Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dgv.BackgroundColor = System.Drawing.SystemColors.Control
    Me.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
    DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
    DataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue
    DataGridViewCellStyle1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Window
    DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.WindowText
    DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
    DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
    Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
    Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
    DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
    DataGridViewCellStyle2.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
    DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window
    DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText
    DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
    Me.dgv.DefaultCellStyle = DataGridViewCellStyle2
    Me.dgv.EnableHeadersVisualStyles = False
    Me.dgv.Location = New System.Drawing.Point(10, 159)
    Me.dgv.Name = "dgv"
    Me.dgv.ReadOnly = True
    Me.dgv.RowHeadersVisible = False
    Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
    Me.dgv.Size = New System.Drawing.Size(1139, 278)
    Me.dgv.TabIndex = 116
    '
    'rcsWareHouse
    '
    Me.rcsWareHouse.AutoTopMargin = False
    Me.rcsWareHouse.BackColor = System.Drawing.SystemColors.Control
    Me.rcsWareHouse.ClientProcess = Nothing
    Me.rcsWareHouse.FromNameText = "productCodeFrom"
    Me.rcsWareHouse.LimitLength = 6
    Me.rcsWareHouse.Location = New System.Drawing.Point(411, 70)
    Me.rcsWareHouse.MaxCodeLength = 15
    Me.rcsWareHouse.MaxLabelLength = 10
    Me.rcsWareHouse.MaxNameLength = 0
    Me.rcsWareHouse.Name = "rcsWareHouse"
    Me.rcsWareHouse.RangeGroupBoxText = "倉庫"
    Me.rcsWareHouse.RangeGroupBoxVisible = False
    Me.rcsWareHouse.Size = New System.Drawing.Size(357, 22)
    Me.rcsWareHouse.TabIndex = 4
    Me.rcsWareHouse.ToNameText = "productCodeTo"
    '
    'cmbLotDetail1
    '
    Me.cmbLotDetail1.DropDownWidth = 105
    Me.cmbLotDetail1.FormattingEnabled = True
    Me.cmbLotDetail1.Location = New System.Drawing.Point(810, 70)
    Me.cmbLotDetail1.MaxTextLength = 15
    Me.cmbLotDetail1.Name = "cmbLotDetail1"
    Me.cmbLotDetail1.Size = New System.Drawing.Size(105, 22)
    Me.cmbLotDetail1.TabIndex = 5
    '
    'cmbLotDetail2
    '
    Me.cmbLotDetail2.DropDownWidth = 105
    Me.cmbLotDetail2.FormattingEnabled = True
    Me.cmbLotDetail2.Location = New System.Drawing.Point(810, 94)
    Me.cmbLotDetail2.MaxTextLength = 15
    Me.cmbLotDetail2.Name = "cmbLotDetail2"
    Me.cmbLotDetail2.Size = New System.Drawing.Size(105, 22)
    Me.cmbLotDetail2.TabIndex = 7
    '
    'cmbLotDetail3
    '
    Me.cmbLotDetail3.DropDownWidth = 105
    Me.cmbLotDetail3.FormattingEnabled = True
    Me.cmbLotDetail3.Location = New System.Drawing.Point(810, 118)
    Me.cmbLotDetail3.MaxTextLength = 15
    Me.cmbLotDetail3.Name = "cmbLotDetail3"
    Me.cmbLotDetail3.Size = New System.Drawing.Size(105, 22)
    Me.cmbLotDetail3.TabIndex = 9
    '
    'cmbLotInfo1
    '
    Me.cmbLotInfo1.DropDownWidth = 105
    Me.cmbLotInfo1.FormattingEnabled = True
    Me.cmbLotInfo1.Location = New System.Drawing.Point(915, 70)
    Me.cmbLotInfo1.MaxTextLength = 15
    Me.cmbLotInfo1.Name = "cmbLotInfo1"
    Me.cmbLotInfo1.Size = New System.Drawing.Size(105, 22)
    Me.cmbLotInfo1.TabIndex = 6
    '
    'cmbLotInfo2
    '
    Me.cmbLotInfo2.DropDownWidth = 105
    Me.cmbLotInfo2.FormattingEnabled = True
    Me.cmbLotInfo2.Location = New System.Drawing.Point(915, 94)
    Me.cmbLotInfo2.MaxTextLength = 15
    Me.cmbLotInfo2.Name = "cmbLotInfo2"
    Me.cmbLotInfo2.Size = New System.Drawing.Size(105, 22)
    Me.cmbLotInfo2.TabIndex = 8
    '
    'cmbLotInfo3
    '
    Me.cmbLotInfo3.DropDownWidth = 105
    Me.cmbLotInfo3.FormattingEnabled = True
    Me.cmbLotInfo3.Location = New System.Drawing.Point(915, 118)
    Me.cmbLotInfo3.MaxTextLength = 15
    Me.cmbLotInfo3.Name = "cmbLotInfo3"
    Me.cmbLotInfo3.Size = New System.Drawing.Size(105, 22)
    Me.cmbLotInfo3.TabIndex = 10
    '
    'PcalableName1
    '
    Me.PcalableName1.CellValue = ""
    Me.PcalableName1.Location = New System.Drawing.Point(1020, 70)
    Me.PcalableName1.MaxTextLength = 15
    Me.PcalableName1.Name = "PcalableName1"
    Me.PcalableName1.ReadOnly = True
    Me.PcalableName1.Size = New System.Drawing.Size(105, 22)
    Me.PcalableName1.TabIndex = 124
    '
    'PcalableName2
    '
    Me.PcalableName2.CellValue = ""
    Me.PcalableName2.Location = New System.Drawing.Point(1020, 94)
    Me.PcalableName2.MaxTextLength = 15
    Me.PcalableName2.Name = "PcalableName2"
    Me.PcalableName2.ReadOnly = True
    Me.PcalableName2.Size = New System.Drawing.Size(105, 22)
    Me.PcalableName2.TabIndex = 125
    '
    'PcalableName3
    '
    Me.PcalableName3.CellValue = ""
    Me.PcalableName3.Location = New System.Drawing.Point(1020, 118)
    Me.PcalableName3.MaxTextLength = 15
    Me.PcalableName3.Name = "PcalableName3"
    Me.PcalableName3.ReadOnly = True
    Me.PcalableName3.Size = New System.Drawing.Size(105, 22)
    Me.PcalableName3.TabIndex = 126
    '
    'cbStockQuantity
    '
    Me.cbStockQuantity.Location = New System.Drawing.Point(10, 122)
    Me.cbStockQuantity.MaxTextLength = 30
    Me.cbStockQuantity.Name = "cbStockQuantity"
    Me.cbStockQuantity.Size = New System.Drawing.Size(210, 22)
    Me.cbStockQuantity.TabIndex = 2001
    Me.cbStockQuantity.Text = "在庫数0のデータを表示する"
    Me.cbStockQuantity.UseVisualStyleBackColor = True
    '
    'StockQuantityZero
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1161, 471)
    Me.Controls.Add(Me.cbStockQuantity)
    Me.Controls.Add(Me.PcalableName3)
    Me.Controls.Add(Me.PcalableName2)
    Me.Controls.Add(Me.PcalableName1)
    Me.Controls.Add(Me.cmbLotInfo3)
    Me.Controls.Add(Me.cmbLotInfo2)
    Me.Controls.Add(Me.cmbLotInfo1)
    Me.Controls.Add(Me.cmbLotDetail3)
    Me.Controls.Add(Me.cmbLotDetail2)
    Me.Controls.Add(Me.cmbLotDetail1)
    Me.Controls.Add(Me.rcsWareHouse)
    Me.Controls.Add(Me.dgv)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.PcaLotNo)
    Me.Controls.Add(Me.PcaProductCode)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MinimumSize = New System.Drawing.Size(1000, 450)
    Me.Name = "StockQuantityZero"
    Me.Text = "ロット別在庫受払表"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents FToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents ToolStripMenuItemPrint As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPreview As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemExport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemReference As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPrint As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPreview As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonExport As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonSearch As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents PcaProductCode As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents PcaLotNo As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPrint As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPreview As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemExport As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandReset As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandPrint As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandPreView As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandExport As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents dgv As System.Windows.Forms.DataGridView
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents rcsWareHouse As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
  Friend WithEvents cmbLotDetail1 As PCA.Controls.PcaComboBox
  Friend WithEvents cmbLotDetail2 As PCA.Controls.PcaComboBox
  Friend WithEvents cmbLotDetail3 As PCA.Controls.PcaComboBox
  Friend WithEvents cmbLotInfo1 As PCA.Controls.PcaComboBox
  Friend WithEvents cmbLotInfo2 As PCA.Controls.PcaComboBox
  Friend WithEvents cmbLotInfo3 As PCA.Controls.PcaComboBox
    Friend WithEvents PcalableName1 As PCA.Controls.PcaTextBox
    Friend WithEvents PcalableName2 As PCA.Controls.PcaTextBox
    Friend WithEvents PcalableName3 As PCA.Controls.PcaTextBox
    Friend WithEvents cbStockQuantity As PCA.Controls.PcaCheckBox

End Class

﻿Imports PCA.ApplicationIntegration
Imports Sunloft.Message
Imports Sunloft.PCAIF
Imports Sunloft.PCAForms
Imports Sunloft.Common

Public Class StockQuantityZero

#Region " Declare"

#Region " Enum"

  Private Enum COLUMNIDX
    ProductCode = 0
    ProductName
    Warehouse
    LotNo
    ShippingDate
    Type
    ShippingDest
    WHNum
    WHQty
    ShippingNum
    ShippingQty
    StockNum
    StockQty
    UnitName
    SlipNo
    Note
    ID
    UnitPrice
    RecType
  End Enum

  Private Enum CodeType
    ProductCode_F
    ProductCode_T
    WHCode_F
    WHCode_T
  End Enum


#End Region

#Region " Const"
  Private Const WIDTH_PRODUCT_CODE As Single = 80
  Private Const WIDTH_PRODUCT_NAME As Single = 150
  Private Const WIDTH_WH As Single = 100
  Private Const WIDTH_LOT_NO As Single = 100
  Private Const WIDTH_SHIPPING_DATE As Single = 80
  Private Const WIDTH_TYPE As Single = 80
  Private Const WIDTH_SHIPPING_DESTINATION As Single = 150
  Private Const WIDTH_WAREHOUSING_MEMBER As Single = 80
  Private Const WIDTH_WAREHOUSING_QUANTITY As Single = 80
  Private Const WIDTH_SHIPPING_MEMBER As Single = 80
  Private Const WIDTH_SHIPPING_QUANTITY As Single = 80
  Private Const WIDTH_STOCK_MEMBER As Single = 80
  Private Const WIDTH_STOCK_QUANTITY As Single = 80
  Private Const WIDTH_UNIT As Single = 80
  Private Const WIDTH_SLIP_NO As Single = 100
  Private Const WIDTH_NOT As Single = 100
  Private Const WIDTH_ID As Single = 100
  Private Const WIDTH_UNITPRICE As Single = 100
  Private Const WIDTH_REC_TYPE As Single = 100

  Private Const HEIGHT_HEADER As Integer = 25
  Private Const NUMBER_TYPE As String = "Number"
  Private Const TEXT_TYPE As String = "String"
  Private Const DATE_TYPE As String = "Date"
  Private Const RPT_FILENAME As String = "frmInputOutput04070.rpt" ' Form to preview/Print
  Private Const RPT_TITLE As String = "ロット別在庫受払表"
  Private Const CSV_FILE_NAME As String = "ロット別在庫受払表"
  Private Const WAREHOUSING_TYPE As String = "入庫"
#End Region

  Private ColumnResults As ColumnResult() = {New ColumnResult}
  Private connector As IIntegratedApplication = Nothing
  Private MasterDialog As SLMasterSearchDialog
  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private m_AMS1Class As AMS1

  Private arrLotDetails(10) As String
  Private m_isSearching As Boolean = False
  Private m_curString As String = String.Empty
  Private m_curItemIdx As Integer
  Private m_isKeyDowned As Boolean = False
  Private nameArray() As String = Nothing
  Private nameArray2() As String = Nothing
  Private nameArray3() As String = Nothing
  Private duplicatedCell(,) As Boolean 'If a cell content is the same as the one above (not display)
  Private m_noOfCheckBox As Integer = 0
  Private m_isDataSet As Boolean = False

#End Region

  Public Sub New()
    Try
      InitializeComponent()

      'Connect to PCA
      Dim strParam As String = Command()
      Dim parts As String() = strParam.Split(New Char() {" "c})
      Dim intParam As Integer = 0
      Dim SL_LMBClass As SL_LMB

      If parts.Length > 1 Then
        m_appClass.ConnectToPCA(strParam)
        connector = m_appClass.connector
      ElseIf Integer.TryParse(strParam, New Integer) Then
        connector = Sunloft.PCAIF.ConnectToPCA.ConnectToPCACalledPG()
      Else
        m_appClass.ConnectToPCA() 'start normally (from code or menu)
        connector = m_appClass.connector
      End If
      If connector Is Nothing Then Close() : Return
      MasterDialog = New SLMasterSearchDialog(connector)
      m_AMS1Class = New AMS1(connector)
      SL_LMBClass = New SL_LMB(connector)

      SL_LMBClass.ReadOnlyRow()
      arrLotDetails = {SL_LMBClass.sl_lmb_label1, SL_LMBClass.sl_lmb_label2, SL_LMBClass.sl_lmb_label3, SL_LMBClass.sl_lmb_label4, SL_LMBClass.sl_lmb_label5 _
                      , SL_LMBClass.sl_lmb_label6, SL_LMBClass.sl_lmb_label7, SL_LMBClass.sl_lmb_label8, SL_LMBClass.sl_lmb_label9, SL_LMBClass.sl_lmb_label10}

      m_AMS1Class.ReadAll()

      InitTable()

      'Disable refer command (F8)
      PcaCommandItemPreview.Enabled = False
      PcaCommandItemExport.Enabled = False
      PcaCommandItemPrint.Enabled = False
      PcaCommandItemRefer.Enabled = False

      'Init lot detail header
      cmbLotDetail1.Items.Add(" ")
      cmbLotDetail2.Items.Add(" ")
      cmbLotDetail3.Items.Add(" ")
      For intCount As Integer = 0 To SL_LMBClass.sl_lmb_maxlotop - 1
        cmbLotDetail1.Items.Add(arrLotDetails(intCount))
        cmbLotDetail2.Items.Add(arrLotDetails(intCount))
        cmbLotDetail3.Items.Add(arrLotDetails(intCount))
      Next

      If intParam <> 0 OrElse (m_appClass.argumentContent <> String.Empty AndAlso Integer.TryParse(m_appClass.argumentContent, intParam)) Then
        'search by parameter
        Search(intParam)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#Region " InitTable"

  ''' <summary>
  ''' Set table and Alignment table 
  ''' </summary>
  ''' <param name="isEmpty">Clear search result array</param>
  ''' <remarks></remarks>
  Private Sub InitTable(Optional ByVal isEmpty As Boolean = False)
    Try
      dgv.Columns.Clear()

      initTableColumn(TEXT_TYPE, ColumnName.Productcode, CInt(WIDTH_PRODUCT_CODE), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, ColumnName.ProductName, CInt(WIDTH_PRODUCT_NAME), DataGridViewContentAlignment.MiddleLeft, , False, DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, ColumnName.Warehouse, CInt(WIDTH_WH), DataGridViewContentAlignment.MiddleLeft, , True, DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, ColumnName.LotNo, CInt(WIDTH_LOT_NO), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(TEXT_TYPE, ColumnName.ShippingDate, CInt(WIDTH_SHIPPING_DATE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.Type, CInt(WIDTH_TYPE), DataGridViewContentAlignment.MiddleCenter, , , DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.ShippingDestination, CInt(WIDTH_SHIPPING_DESTINATION), DataGridViewContentAlignment.MiddleLeft, , , DataGridViewContentAlignment.MiddleLeft)
      initTableColumn(NUMBER_TYPE, ColumnName.WarehousingMember, CInt(WIDTH_WAREHOUSING_MEMBER), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      initTableColumn(NUMBER_TYPE, ColumnName.WarehousingQuantity, CInt(WIDTH_WAREHOUSING_QUANTITY), DataGridViewContentAlignment.MiddleRight, , True, DataGridViewContentAlignment.MiddleRight)
      initTableColumn(NUMBER_TYPE, ColumnName.ShippingMumber, CInt(WIDTH_SHIPPING_MEMBER), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      initTableColumn(NUMBER_TYPE, ColumnName.ShippingQuantity, CInt(WIDTH_SHIPPING_QUANTITY), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      initTableColumn(NUMBER_TYPE, ColumnName.StockNumber, CInt(WIDTH_STOCK_MEMBER), DataGridViewContentAlignment.MiddleRight, , True, DataGridViewContentAlignment.MiddleRight)
      initTableColumn(NUMBER_TYPE, ColumnName.StockQuantity, CInt(WIDTH_STOCK_QUANTITY), DataGridViewContentAlignment.MiddleRight, , , DataGridViewContentAlignment.MiddleRight)
      initTableColumn(TEXT_TYPE, ColumnName.Unit, CInt(WIDTH_UNIT), DataGridViewContentAlignment.MiddleLeft, , True, DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.SlipNo, CInt(WIDTH_SLIP_NO), DataGridViewContentAlignment.MiddleLeft, , True, DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.Note, CInt(WIDTH_NOT), DataGridViewContentAlignment.MiddleLeft, , True, DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.Id, CInt(WIDTH_ID), DataGridViewContentAlignment.MiddleLeft, , True, DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.UnitPrice, CInt(WIDTH_UNITPRICE), DataGridViewContentAlignment.MiddleLeft, , True, DataGridViewContentAlignment.MiddleCenter)
      initTableColumn(TEXT_TYPE, ColumnName.RecType, CInt(WIDTH_REC_TYPE), DataGridViewContentAlignment.MiddleLeft, , True, DataGridViewContentAlignment.MiddleCenter)

      dgv.Columns(ColumnName.UnitPrice).Visible = False
      'dgv.Columns(ColumnName.warehouse).Visible = False
      dgv.Columns(ColumnName.Note).Visible = False
      dgv.Columns(ColumnName.Id).Visible = False
      dgv.Columns(ColumnName.RecType).Visible = False
      'Set header cell font color to white
      For intCount As Integer = 0 To 13
        dgv.Columns(intCount).HeaderCell.Style.ForeColor = Color.White
      Next
      'Set header size (Default size is a bit small)
      dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
      dgv.ColumnHeadersHeight = HEIGHT_HEADER

      If isEmpty Then
        ColumnResults = Nothing 'Clear search result array
        dgv.Rows.Clear()
      End If

      'Set table not sortable
      For Each column As DataGridViewColumn In dgv.Columns
        column.SortMode = DataGridViewColumnSortMode.NotSortable
      Next

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>
  ''' set alignment header
  ''' </summary>
  ''' <param name="strHeaderType">Header Type</param>
  ''' <param name="strHeaderText"> Header Text</param>
  ''' <param name="intColWidth"> Width of column</param>
  ''' <param name="strColumnName">Name of column</param>
  ''' <param name="isReadonly">Readonly</param>
  ''' <remarks></remarks>
  Private Sub initTableColumn(ByVal strHeaderType As String, ByVal strColumnName As String, ByVal intColWidth As Integer, _
                              Optional ByVal alignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft, Optional ByVal strHeaderText As String = "", _
                              Optional ByVal isReadonly As Boolean = True, Optional ByVal headerAlignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft)
    Dim col As New DataGridViewColumn

    Select Case strHeaderType
      Case TEXT_TYPE
        col = New DataGridViewTextBoxColumn
      Case NUMBER_TYPE
        col = New DataGridViewTextBoxColumn
      Case Else
        col = New DataGridViewTextBoxColumn
    End Select

    Select Case col.Name
      Case ColumnName.ProductName, ColumnName.WarehousingMember, ColumnName.WarehousingQuantity, ColumnName.ShippingMumber, ColumnName.ShippingMumber, _
       ColumnName.ShippingQuantity, ColumnName.StockNumber, ColumnName.StockQuantity
        col.HeaderText = strHeaderText
    End Select

    col.ReadOnly = isReadonly
    col.Name = strColumnName
    col.Width = intColWidth
    col.DefaultCellStyle.Alignment = alignment
    col.HeaderCell.Style.Alignment = headerAlignment
    col.DefaultCellStyle.FormatProvider = Globalization.CultureInfo.CreateSpecificCulture("ja-JP")
    dgv.Columns.Add(col)
  End Sub

#End Region

#Region " Search "

  ''' <summary>
  ''' Search and desplay data 
  ''' </summary>
  ''' <param name="intParam">parameter of pro04060</param>
  ''' <remarks></remarks>
  Private Sub Search(Optional ByVal intParam As Integer = 0)
    dgv.Focus()
    dgv.Rows.Clear()
    If GetSearchResult(intParam) Then DisplaySearchResult()
    m_isDataSet = True
  End Sub

  ''' <summary>
  ''' search data 
  ''' </summary>
  ''' <param name="intParam">parameter of pro04060 </param>
  ''' <remarks></remarks>
  Private Function GetSearchResult(Optional ByVal intParam As Integer = 0) As Boolean

    Dim reader As ICustomDataReader = Nothing
    Try
      Dim selectCommand As ICustomCommand
      Dim lotNo As String = PcaLotNo.Text
      Dim strFromProductCode As String = PcaProductCode.FromCodeText
      Dim strToProductCode As String = PcaProductCode.ToCodeText
      Dim strFromWHCode As String = rcsWareHouse.FromCodeText
      Dim strToWHCode As String = rcsWareHouse.ToCodeText

      Dim lotInfo1 As String = " "
      Dim lotInfo2 As String = " "
      Dim lotInfo3 As String = " "

      Dim selectedLotDetail1 As String = String.Empty
      Dim selectedLotDetail2 As String = String.Empty
      Dim selectedLotDetail3 As String = String.Empty

      Dim isNewLotRow As Boolean = False
      Dim isShippingRowOfTheLot As Boolean = False
      Dim lStockQuantity As Integer = 0
      Dim lStockNumber As Integer = 0

      Dim intCount As Integer = -1
      Dim isDataExists As Boolean = False

      If cmbLotInfo1.SelectedItem IsNot Nothing Then lotInfo1 = cmbLotInfo1.SelectedItem.ToString

      If cmbLotInfo2.SelectedItem IsNot Nothing Then lotInfo2 = cmbLotInfo2.SelectedItem.ToString

      If cmbLotInfo3.SelectedItem IsNot Nothing Then lotInfo3 = cmbLotInfo3.SelectedItem.ToString

      'figure out which lot detail id is being selected
      If cmbLotDetail1.SelectedItem IsNot Nothing Then selectedLotDetail1 = CStr(Array.IndexOf(arrLotDetails, cmbLotDetail1.SelectedItem.ToString) + 1)

      If cmbLotDetail2.SelectedItem IsNot Nothing Then selectedLotDetail2 = CStr(Array.IndexOf(arrLotDetails, cmbLotDetail2.SelectedItem.ToString) + 1)

      If cmbLotDetail3.SelectedItem IsNot Nothing Then selectedLotDetail3 = CStr(Array.IndexOf(arrLotDetails, cmbLotDetail3.SelectedItem.ToString) + 1)

      duplicatedCell = Nothing


      Array.Clear(ColumnResults, 0, ColumnResults.Length)

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_ZDN_Pro04070")
      'set condition search SQL 
      selectCommand.Parameters("@fromCode").SetValue(strFromProductCode)
      selectCommand.Parameters("@toCode").SetValue(strToProductCode)
      selectCommand.Parameters("@fromWareHouseCode").SetValue(strFromWHCode)
      selectCommand.Parameters("@toWareHouseCode").SetValue(strToWHCode)
      selectCommand.Parameters("@lotNo").SetValue(lotNo)
      selectCommand.Parameters("@intParam").SetValue(intParam)

      If cbStockQuantity.Checked Then
        selectCommand.Parameters("@stockZero").SetValue("Checked") 'Set to any value, as long as the value is not empty
      Else
        selectCommand.Parameters("@stockZero").SetValue(String.Empty)
      End If

      If Not SLCmnFunction.checkObjectNothingEmpty(lotInfo1) AndAlso selectedLotDetail1 <> "0" Then
        selectCommand.Parameters("@lotInfoDetail" + selectedLotDetail1).SetValue(lotInfo1)
      End If

      If Not SLCmnFunction.checkObjectNothingEmpty(lotInfo2) AndAlso selectedLotDetail2 <> "0" Then
        selectCommand.Parameters("@lotInfoDetail" + selectedLotDetail2).SetValue(lotInfo2)
      End If

      If Not SLCmnFunction.checkObjectNothingEmpty(lotInfo3) AndAlso selectedLotDetail3 <> "0" Then
        selectCommand.Parameters("@lotInfoDetail" + selectedLotDetail3).SetValue(lotInfo3)
      End If

      'read data 
      reader = selectCommand.ExecuteReader

      While reader.Read()
        isDataExists = True
        intCount = intCount + 1
        ReDim Preserve ColumnResults(intCount)
        ColumnResults(intCount) = New ColumnResult

        ColumnResults(intCount).Productcode = reader.GetValue("sl_zdn_scd").ToString.Trim

        ColumnResults(intCount).ProductName = reader.GetValue("sms_mei").ToString.Trim
        ColumnResults(intCount).LotNo = reader.GetValue("sl_zdn_ulotno").ToString

        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("shippingDate")) AndAlso reader.GetValue("shippingDate").ToString.Length = 8 Then
          ColumnResults(intCount).ShippingDate = Date.ParseExact(reader.GetValue("shippingDate").ToString, "yyyyMMdd", Globalization.CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        End If

        ColumnResults(intCount).Type = reader.GetValue("Type").ToString
        ColumnResults(intCount).RecType = reader.GetValue("recType").ToString
        ColumnResults(intCount).ShippingDest = reader.GetValue("cms_mei").ToString

        If CDbl(ColumnResults(intCount).RecType) = 0 Then
          isNewLotRow = True
          isShippingRowOfTheLot = False
          lStockQuantity = 0
          lStockNumber = 0

          ColumnResults(intCount).WHNum = CInt(reader.GetValue("wareHousingNumber"))
          If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("wareHousingQuantity")) Then ColumnResults(intCount).WHQty = CDec(reader.GetValue("wareHousingQuantity"))

        End If

        If CDbl(ColumnResults(intCount).RecType) <> 0 Then
          If isNewLotRow Then isShippingRowOfTheLot = True

          ColumnResults(intCount).ShippingNum = CInt(reader.GetValue("shippingNumber").ToString)
          If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("shippingQuantity")) Then ColumnResults(intCount).ShippingQty = CDec(reader.GetValue("shippingQuantity"))

          isNewLotRow = False
        End If

        If Not isShippingRowOfTheLot Then
          If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("stockQuantity")) Then ColumnResults(intCount).StockQty = CDec(reader.GetValue("stockQuantity"))
          lStockQuantity = CInt(ColumnResults(intCount).StockQty)

          ColumnResults(intCount).StockNum = CInt(reader.GetValue("stockNumber").ToString)
          lStockNumber = CInt(ColumnResults(intCount).StockNum)
        End If

        If isShippingRowOfTheLot Then
          If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("shippingQuantity")) Then lStockQuantity = CInt(lStockQuantity - CDec(reader.GetValue("shippingQuantity").ToString))
          ColumnResults(intCount).StockQty = lStockQuantity

          lStockNumber = lStockNumber - CInt(reader.GetValue("shippingNumber").ToString)
          ColumnResults(intCount).StockNum = lStockNumber
        End If

        ColumnResults(intCount).UnitName = reader.GetValue("sl_nykd_tani").ToString
        ColumnResults(intCount).SlipNo = reader.GetValue("slipNo").ToString
        ColumnResults(intCount).Warehouse = reader.GetValue("ems_str").ToString.Trim
        ColumnResults(intCount).Note = reader.GetValue("note").ToString
        If intCount > 0 AndAlso ColumnResults(intCount).Productcode = ColumnResults(intCount - 1).Productcode AndAlso _
           ColumnResults(intCount - 1).LotNo = ColumnResults(intCount).LotNo AndAlso ColumnResults(intCount - 1).Warehouse.Trim <> String.Empty AndAlso _
           ColumnResults(intCount).Type <> WAREHOUSING_TYPE Then
          ColumnResults(intCount).Warehouse = ColumnResults(intCount - 1).Warehouse
        End If
        ColumnResults(intCount).ID = reader.GetValue("sl_zdn_id").ToString.Trim
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_nykd_tanka")) Then ColumnResults(intCount).UnitPrice = CDec(reader.GetValue("sl_nykd_tanka"))
        ColumnResults(intCount).UnitPriceNoOfDecDigit = CInt(reader.GetValue("sms_tketa"))
        ColumnResults(intCount).WHQtyDecDigit = CInt(reader.GetValue("sms_sketa"))
        ColumnResults(intCount).StockQtyDecDigit = CInt(reader.GetValue("sms_sketa"))
        ColumnResults(intCount).ShippingQtyDecDigit = CInt(reader.GetValue("sms_sketa"))

      End While

      'create duplicatedCell Array
      If Not ColumnResults Is Nothing Then duplicatedCell = New Boolean(ColumnResults.Length - 1, 3) {}

      'hide and show PcaCommand
      If ColumnResults(0) Is Nothing Then
        PcaCommandItemPreview.Enabled = False
        PcaCommandItemExport.Enabled = False
        PcaCommandItemPrint.Enabled = False
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSearch.CommandId)
      Else
        PcaCommandItemPreview.Enabled = True
        PcaCommandItemExport.Enabled = True
        PcaCommandItemPrint.Enabled = True
      End If
      Return isDataExists
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
      If reader IsNot Nothing Then
        reader.Close()
        reader.Dispose()
      End If
    End Try
  End Function

  ''' <summary>
  ''' Display data after get search 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub DisplaySearchResult()
    Try
      dgv.Columns.Clear()
      InitTable()
      Dim intInvNum As Integer = 0
      Dim decInvQty As Decimal = 0D
      Dim intOldInvID As Integer = Integer.Parse(ColumnResults(0).ID)

      For intCount As Integer = 0 To ColumnResults.Length - 1
        If Not SLCmnFunction.checkObjectNothingEmpty(ColumnResults(intCount)) Then
          dgv.Rows.Add(New DataGridViewRow)
          If intOldInvID <> Integer.Parse(ColumnResults(intCount).ID) Then intInvNum = 0 : decInvQty = 0D

          intOldInvID = Integer.Parse(ColumnResults(intCount).ID)

          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.RecType).Value = ColumnResults(intCount).RecType
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.ProductCode).Value = ColumnResults(intCount).Productcode
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.ProductName).Value = ColumnResults(intCount).ProductName
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.Warehouse).Value = ColumnResults(intCount).Warehouse
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.LotNo).Value = ColumnResults(intCount).LotNo
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.ShippingDate).Value = ColumnResults(intCount).ShippingDate.Trim
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.Type).Value = ColumnResults(intCount).Type
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.ShippingDest).Value = ColumnResults(intCount).ShippingDest.Trim

          Select Case Integer.Parse(ColumnResults(intCount).RecType)
            Case SLConstants.SL_ZHKSlipType.Warehousing
              dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.WHNum).Value = SLCmnFunction.formatNumberOfDigit(ColumnResults(intCount).WHNum)
              dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.WHQty).Value = SLCmnFunction.formatNumberOfDigit(CDec(ColumnResults(intCount).WHQty), ColumnResults(intCount).WHQtyDecDigit)
              intInvNum += ColumnResults(intCount).WHNum
              decInvQty += ColumnResults(intCount).WHQty
            Case SLConstants.SL_ZHKSlipType.ReturnWarehousing
              dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.WHNum).Value = SLCmnFunction.formatNumberOfDigit(ColumnResults(intCount).ShippingNum)
              dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.WHQty).Value = SLCmnFunction.formatNumberOfDigit(CDec(ColumnResults(intCount).ShippingQty), ColumnResults(intCount).ShippingQtyDecDigit)
              intInvNum += ColumnResults(intCount).ShippingNum
              decInvQty += ColumnResults(intCount).ShippingQty
            Case Else
              dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.ShippingNum).Value = SLCmnFunction.formatNumberOfDigit(ColumnResults(intCount).ShippingNum)
              dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.ShippingQty).Value = SLCmnFunction.formatNumberOfDigit(CDec(ColumnResults(intCount).ShippingQty), ColumnResults(intCount).ShippingQtyDecDigit)
              intInvNum -= ColumnResults(intCount).ShippingNum
              decInvQty -= ColumnResults(intCount).ShippingQty
          End Select

          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.StockNum).Value = SLCmnFunction.formatNumberOfDigit(intInvNum)
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.StockQty).Value = SLCmnFunction.formatNumberOfDigit(decInvQty, ColumnResults(intCount).StockQtyDecDigit)
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.UnitName).Value = ColumnResults(intCount).UnitName
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.SlipNo).Value = ColumnResults(intCount).SlipNo
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.Note).Value = ColumnResults(intCount).Note
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.ID).Value = ColumnResults(intCount).ID
          dgv.Rows.Item(intCount).Cells.Item(COLUMNIDX.UnitPrice).Value = SLCmnFunction.formatNumberOfDigit(CDec(ColumnResults(intCount).UnitPrice), ColumnResults(intCount).UnitPriceNoOfDecDigit)

        End If
      Next
      'In the same slip, only cells in first row  = true, others = false
      For intRow As Integer = 1 To ColumnResults.Length - 1
        For intColumn As Integer = COLUMNIDX.ProductCode To COLUMNIDX.LotNo
          If ColumnResults(intRow) IsNot Nothing Then

            If dgv.Rows(intRow - 1).Cells(COLUMNIDX.ProductCode).Value.ToString.Trim = dgv.Rows(intRow).Cells(COLUMNIDX.ProductCode).Value.ToString.Trim AndAlso _
               dgv.Rows(intRow - 1).Cells(COLUMNIDX.LotNo).Value.ToString.Trim = dgv.Rows(intRow).Cells(COLUMNIDX.LotNo).Value.ToString.Trim AndAlso _
               dgv.Rows(intRow - 1).Cells(COLUMNIDX.ID).Value.ToString.Trim = dgv.Rows(intRow).Cells(COLUMNIDX.ID).Value.ToString.Trim Then 'If the slip number is the same
              duplicatedCell(intRow, intColumn) = True 'duplicated.
            Else
              duplicatedCell(intRow, intColumn) = False
              If intColumn = COLUMNIDX.ProductCode Then m_noOfCheckBox = m_noOfCheckBox + 1
            End If
          End If
        Next
      Next
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub
#End Region
#Region "PCA Command Manager"
  ''' <summary>
  ''' PCA command (F1,F5,F8,F9,Shift F9, F10 , F12)
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub PcaCommandManager1_Command(ByVal sender As System.Object, ByVal e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try
      Dim commandItem As PCA.Controls.PcaCommandItem = e.CommandItem

      Select Case True
        Case commandItem Is PcaCommandItemHelp 'F1
          'do nothing
        Case commandItem Is PcaCommandItemSearch 'F5 (searh data)
          Search()
        Case commandItem Is PcaCommandItemRefer 'F8
          '  DisplayReferScreen
          If m_isKeyDowned Then m_isKeyDowned = False : Exit Sub
          DisplayReferScreen(Nothing, m_curItemIdx)
        Case commandItem Is PcaCommandItemPrint 'F9
          'print data
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.Productcode, 0)) Then
            If MsgBox(RPT_TITLE & "を" & SLConstants.ConfirmMessage.PRINT_CONFIRM_MESSAGE, MsgBoxStyle.OkCancel, SLConstants.NotifyMessage.TITLE_MESSAGE) = MsgBoxResult.Ok Then
              PrintData()
            End If
          End If
        Case commandItem Is PcaCommandItemPreview 'Shift + F9
          'preview
          PrintData(True)
        Case commandItem Is PcaCommandItemExport 'F10
          'do export
          If OutPutCSV() Then
            SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, _
                                      Me, PcaCommandItemExport.CommandId)
          End If
        Case commandItem Is PcaCommandItemClose 'F12 
          Close()
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

#End Region

#Region "Print/Preview"
  ''' <summary>
  ''' print data 
  ''' </summary>
  ''' <param name="isPreview">is print or is priview</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function PrintData(Optional ByVal isPreview As Boolean = False) As Boolean
    Try
      Dim ReportName As String = RPT_FILENAME
      Dim ReportTitle As String = RPT_TITLE
      Dim rpt As New Sunloft.Windows.Forms.SLCrystalReport
      Dim RpParam As New Dictionary(Of String, Object)
      Dim editedDataTable As DataTable = Nothing


      rpt.ReportFile = IO.Path.GetDirectoryName(Application.ExecutablePath) & "\" & ReportName
      EditPrintData(editedDataTable)
      rpt.DataSource = editedDataTable
      ' 出力開始
      If isPreview Then
        rpt.Preview(ReportTitle, True)
      Else
        rpt.PrintToPrinter()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

    Return True
  End Function
  ''' <summary>
  ''' prepare data for print and priview
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EditPrintData(ByRef argEditedData As DataTable) As Boolean
    Dim editData As New DataSet1.DataTable1DataTable
    Dim editRow As DataRow
    Dim intLpc As Integer = 0

    Try
      While intLpc < dgv.Rows.Count AndAlso Not dgv(ColumnName.Productcode, intLpc) Is Nothing 'SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.productCode, intLpc))
        With dgv
          'condition
          editRow = editData.NewRow
          editRow.Item("Warehouse") = "倉庫: " & dgv(ColumnName.Warehouse, intLpc).Value.ToString

          'data table
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.ShippingDate, intLpc).Value) Then
            editRow.Item("ShippingDate") = dgv(ColumnName.ShippingDate, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.LotNo, intLpc).Value) Then
            editRow.Item("LotNo") = dgv(ColumnName.LotNo, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.ProductName, intLpc).Value) Then
            editRow.Item("ProductName") = dgv(ColumnName.ProductName, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.Productcode, intLpc).Value) Then
            editRow.Item("Productcode") = dgv(ColumnName.Productcode, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.LotNo, intLpc).Value) Then
            editRow.Item("LotNo") = dgv(ColumnName.LotNo, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.ShippingDestination, intLpc).Value) Then
            editRow.Item("ShippingDestination") = dgv(ColumnName.ShippingDestination, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.Type, intLpc).Value) Then
            editRow.Item("Type") = dgv(ColumnName.Type, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.ShippingDestination, intLpc).Value) Then
            editRow.Item("ShippingDestination") = dgv(ColumnName.ShippingDestination, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.WarehousingQuantity, intLpc).Value) Then
            editRow.Item("WarehousingQuantity") = dgv(ColumnName.WarehousingQuantity, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.ShippingQuantity, intLpc).Value) Then
            editRow.Item("ShippingQuantity") = dgv(ColumnName.ShippingQuantity, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.StockQuantity, intLpc).Value) Then
            editRow.Item("StockQuantity") = dgv(ColumnName.StockQuantity, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.Unit, intLpc).Value) Then
            editRow.Item("Unit") = dgv(ColumnName.Unit, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.SlipNo, intLpc).Value) Then
            editRow.Item("SlipNo") = dgv(ColumnName.SlipNo, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.Note, intLpc).Value) Then
            editRow.Item("Note") = dgv(ColumnName.Note, intLpc).Value.ToString
          End If
          If Not SLCmnFunction.checkObjectNothingEmpty(dgv(ColumnName.UnitPrice, intLpc).Value) Then
            editRow.Item("UnitPrice") = dgv(ColumnName.UnitPrice, intLpc).Value.ToString
          End If
          editRow.Item("Id") = dgv(ColumnName.Id, intLpc).Value
          editRow.Item("key_PreProduct") = "■ "
          editRow.Item("Product") = dgv(ColumnName.Productcode, intLpc).Value.ToString & " " & dgv(ColumnName.ProductName, intLpc).Value.ToString

          editData.Rows.Add(editRow)
        End With
        intLpc = intLpc + 1
      End While

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
    argEditedData = editData
    Return True
  End Function
  ''' <summary>
  ''' export file CSV
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function OutPutCSV() As Boolean
    Dim strSaveFileName As String = String.Empty
    Dim strColumns() As String
    Dim sbCsv As New System.Text.StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .FileName = CSV_FILE_NAME
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        .OverwritePrompt = True

        Dim res As System.Windows.Forms.DialogResult = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False

        strSaveFileName = .FileName

      End With

      'column header and column value 
      strColumns = {ColumnName.Productcode, ColumnName.ProductName, ColumnName.LotNo, ColumnName.ShippingDate, ColumnName.Type, ColumnName.ShippingDestination,
                    ColumnName.WarehousingMember, ColumnName.WarehousingQuantity, ColumnName.ShippingMumber, ColumnName.ShippingQuantity,
                    ColumnName.StockNumber, ColumnName.StockQuantity, ColumnName.Unit, ColumnName.SlipNo}
      'export CSV 
      SLCmnFunction.OutPutCSVFromDataGridView(strColumns, dgv, strSaveFileName, dgv.RowCount)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function
#End Region

#Region "Validate "

#Region "Event Product Code"
  Private Sub PcaProductCode_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaProductCode.Leave
    PcaCommandItemRefer.Enabled = False
  End Sub
  Private Sub PcaProductCode_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaProductCode.ClickFromReferButton2
    m_curItemIdx = CodeType.ProductCode_F
    m_curString = PcaProductCode.FromCodeText
    DisplayReferScreen(e, CodeType.ProductCode_F)
  End Sub

  Private Sub PcaProductCode_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PcaProductCode.ClickToReferButton2
    m_curItemIdx = CodeType.ProductCode_T
    m_curString = PcaProductCode.ToCodeText
    DisplayReferScreen(e, CodeType.ProductCode_T)
  End Sub
  Private Sub PcaProductCode_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaProductCode.ToCodeTextValidating2
    validateManagement(e, PcaProductCode.ToCodeText)
  End Sub
  Private Sub PcaProductCode_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaProductCode.FromCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_curString = PcaProductCode.FromCodeText
    m_curItemIdx = CodeType.ProductCode_F
  End Sub

  Private Sub PcaProductCode_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles PcaProductCode.FromCodeTextValidating2
    validateManagement(e, PcaProductCode.FromCodeText)
  End Sub

  Private Sub PcaProductCode_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PcaProductCode.ToCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_curString = PcaProductCode.ToCodeText
    m_curItemIdx = CodeType.ProductCode_T
  End Sub
#End Region

#Region "Event WareHouse"
  Private Sub rcsWareHouse_ClickFromReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles rcsWareHouse.ClickFromReferButton2
    m_curItemIdx = CodeType.WHCode_F
    m_curString = rcsWareHouse.FromCodeText
    DisplayReferScreen(e, CodeType.WHCode_F)
  End Sub

  Private Sub rcsWareHouse_ClickToReferButton2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles rcsWareHouse.ClickToReferButton2
    m_curItemIdx = CodeType.WHCode_T
    m_curString = rcsWareHouse.ToCodeText
    DisplayReferScreen(e, CodeType.WHCode_T)
  End Sub

  Private Sub rcsWareHouse_FromCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rcsWareHouse.FromCodeTextEnter
    PcaCommandItemRefer.Enabled = True
    m_curString = rcsWareHouse.FromCodeText
    m_curItemIdx = CodeType.WHCode_F
  End Sub

  Private Sub rcsWareHouse_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rcsWareHouse.Leave
    PcaCommandItemRefer.Enabled = False
  End Sub

  Private Sub rcsWareHouse_FromCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles rcsWareHouse.FromCodeTextValidating2
    validateManagement(e, rcsWareHouse.FromCodeText)
  End Sub

  Private Sub rcsWareHouse_ToCodeTextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rcsWareHouse.ToCodeTextEnter

    PcaCommandItemRefer.Enabled = True
    m_curString = rcsWareHouse.ToCodeText
    m_curItemIdx = CodeType.WHCode_T
  End Sub

  Private Sub rcsWareHouse_ToCodeTextValidating2(ByVal sender As System.Object, ByVal e As PCA.Controls.ValidatingEventArgs) Handles rcsWareHouse.ToCodeTextValidating2
    validateManagement(e, rcsWareHouse.ToCodeText)
  End Sub
#End Region
  '***********************************************************																														
  'Name          : validateManagement
  'Content       : Event called when Product Code Warehouse (From, To Leave event)
  'Return Value  : 
  'Argument      : e As PCA.Controls.ValidatingEventArgs, CodeText As String
  'Created date  : 2016/03/20  by DuyTN
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub validateManagement(ByVal e As PCA.Controls.ValidatingEventArgs, ByRef CodeText As String)
    If m_curItemIdx = CodeType.ProductCode_F Or m_curItemIdx = CodeType.ProductCode_T Then
      Try
        If CodeText.TrimEnd().Length > m_AMS1Class.ams1_ProductLength Then
          e.Cancel = True
          CodeText = m_curString
          Return
        End If
        Dim curProductCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_ProductLength)
        If Not String.IsNullOrEmpty(curProductCode) AndAlso m_curString = curProductCode Then
          CodeText = curProductCode
          'No changes
          Return
        End If
        If String.IsNullOrEmpty(curProductCode) Then
          Return
        End If
        'validate supplier code
        Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms = MasterDialog.FindBEMasterSms(curProductCode)

        If Not beMasterSms.SyohinCode.Length > 0 Then
          'If there's error
          e.Cancel = True
          CodeText = m_curString
          Return
        Else
          CodeText = curProductCode
        End If

      Catch ex As Exception
        DisplayBox.ShowCritical(ex)
      End Try
    ElseIf m_curItemIdx = CodeType.WHCode_F Or m_curItemIdx = CodeType.WHCode_T Then
      Try
        If CodeText.TrimEnd().Length > m_AMS1Class.ams1_WarehouseLength Then
          e.Cancel = True
          CodeText = m_curString 'MasterDialog.ModifiedEntity.Header.TorihikisakiCode
          Return
        End If
        Dim curWarehouseCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_WarehouseLength)
        If Not String.IsNullOrEmpty(curWarehouseCode) AndAlso m_curString = curWarehouseCode Then
          CodeText = curWarehouseCode
          'No changes
          Return
        End If
        If String.IsNullOrEmpty(curWarehouseCode) Then
          Return
        End If
        'validate supplier code
        Dim beMasterEms As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(curWarehouseCode)
        If Not beMasterEms.SokoCode.Length > 0 Then
          'If there's error
          e.Cancel = True
          CodeText = m_curString 'MasterDialog.ModifiedEntity.Header.TorihikisakiCode
          Return
        Else
          CodeText = curWarehouseCode
        End If

      Catch ex As Exception
        DisplayBox.ShowCritical(ex)
      End Try
    End If
  End Sub

  '***********************************************************																														
  'Name          : DisplayReferScreen
  'Content       : Display refer screen when the user selects from CodeSet field
  'Return Value  : 
  'Argument      : 
  'Created date  : 2016/03/21  DuyTN
  'Modified date :        by       Content: 																											
  '***********************************************************
  Private Sub DisplayReferScreen(ByVal e As System.ComponentModel.CancelEventArgs, ByRef Item As Integer)
    If m_curItemIdx = CodeType.ProductCode_F Or m_curItemIdx = CodeType.ProductCode_T Then
      Dim newCode As String = String.Empty
      Dim location As Point = PCA.TSC.Kon.Tools.ControlTool.GetDialogLocation(PcaProductCode.FromReferButton)
      newCode = MasterDialog.ShowReferSmsDialog(If(Item = CodeType.ProductCode_F, PcaProductCode.FromCodeText, PcaProductCode.ToCodeText), location)
      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
        If m_curString = newCode Then
          'No changes
          Return
        End If
        Dim beMasterSms As PCA.TSC.Kon.BusinessEntity.BEMasterSms = MasterDialog.FindBEMasterSms(newCode)
        If Not beMasterSms.SyohinCode.Length > 0 Then
          'Validate failed
          If Not e Is Nothing Then e.Cancel = True
          If Item = CodeType.ProductCode_F Then
            PcaProductCode.FromCodeText = m_curString
          Else
            PcaProductCode.ToCodeText = m_curString
          End If
          Return
        End If
        If Item = CodeType.ProductCode_F Then
          PcaProductCode.FromCodeText = newCode
        Else
          PcaProductCode.ToCodeText = newCode
        End If
      End If
    ElseIf m_curItemIdx = CodeType.WHCode_F Or m_curItemIdx = CodeType.WHCode_T Then
      Dim newCode As String = String.Empty
      Dim location As Point = PCA.TSC.Kon.Tools.ControlTool.GetDialogLocation(rcsWareHouse.FromReferButton)
      newCode = MasterDialog.ShowReferSokoDialog(If(Item = CodeType.WHCode_F, rcsWareHouse.FromCodeText, rcsWareHouse.ToCodeText), location)
      If String.IsNullOrEmpty(newCode) Then
        If Not e Is Nothing Then e.Cancel = True
      Else
        If m_curString = newCode Then
          'No changes
          Return
        End If
        Dim beMasterEms As PCA.TSC.Kon.BusinessEntity.BEMasterSoko = MasterDialog.FindBEMasterSoko(newCode)
        If Not beMasterEms.SokoCode.Length > 0 Then
          'Validate failed
          If Not e Is Nothing Then e.Cancel = True
          If Item = CodeType.WHCode_F Then
            rcsWareHouse.FromCodeText = m_curString
          Else
            rcsWareHouse.ToCodeText = m_curString
          End If
          Return
        End If
        If Item = CodeType.WHCode_F Then
          rcsWareHouse.FromCodeText = newCode
        Else
          rcsWareHouse.ToCodeText = newCode
        End If
      End If
    End If
  End Sub
#End Region
#Region "Paint ColumnHeaders"
  ''' <summary>
  ''' join two  header 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub dgv_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles dgv.Paint
    Dim monthes1 As String() = {ColumnName.Productcode}
    Dim monthes2 As String() = {ColumnName.WarehousingMember, ColumnName.ShippingMumber, ColumnName.StockNumber}
    Dim j As Integer = COLUMNIDX.ProductCode 'Merge 1 ,2
    Dim i As Integer = COLUMNIDX.WHNum 'Merge 6 -> 12
    While j <= COLUMNIDX.ProductName
      Dim r1 As Rectangle = dgv.GetCellDisplayRectangle(j, -1, True)
      Dim w2 As Integer = dgv.GetCellDisplayRectangle(j + 1, -1, True).Width
      r1.X += 1
      r1.Y += 1
      r1.Width = r1.Width + w2 - 2
      r1.Height = CInt(r1.Height - 1)
      e.Graphics.FillRectangle(New SolidBrush(dgv.ColumnHeadersDefaultCellStyle.BackColor), r1)
      Dim format As New StringFormat()
      format.Alignment = StringAlignment.Near
      format.LineAlignment = StringAlignment.Center
      e.Graphics.DrawString(monthes1(j), dgv.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Color.White), r1, format)
      j += 2
    End While
    While i < COLUMNIDX.UnitName
      Dim r1 As Rectangle = dgv.GetCellDisplayRectangle(i, -1, True)
      Dim w2 As Integer = dgv.GetCellDisplayRectangle(i + 1, -1, True).Width
      r1.X += 1
      r1.Y += 1
      r1.Width = r1.Width + w2 - 2
      r1.Height = CInt(r1.Height - 1)
      e.Graphics.FillRectangle(New SolidBrush(dgv.ColumnHeadersDefaultCellStyle.BackColor), r1)
      Dim format As New StringFormat()
      format.Alignment = StringAlignment.Near
      format.LineAlignment = StringAlignment.Center
      e.Graphics.DrawString(monthes2(CInt(i / 2 - 3.5)), dgv.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Color.White), r1, format)
      i += 2
    End While

  End Sub

  Private Sub dgv_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgv.Scroll
    Dim rtHeader As Rectangle = dgv.DisplayRectangle
    rtHeader.Height = CInt(dgv.ColumnHeadersHeight)
    dgv.Invalidate(rtHeader)
  End Sub
#End Region

  ''' <summary>
  ''' check user press Tap or Enter
  ''' </summary >
  ''' <param name="keyData"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
    '
    Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
    Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子

    If (keyCode = Keys.Tab OrElse keyCode = Keys.Enter) Then
      'Validate if user press Enter/Tab +  Anykey except Shift, →, ↓
      m_isSearching = True
    Else
      m_isSearching = False
    End If

    Return MyBase.ProcessDialogKey(keyData)

  End Function
  ''' <summary>
  ''' when user press close
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub frmLotInput_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then connector.LogOffSystem()
  End Sub
  Private Sub frmTransLotList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
    If e.KeyCode = Keys.F8 Then m_isKeyDowned = True
  End Sub
#Region "Check condition"
  ''' <summary>
  ''' get data put into combobox csLotInfo1 after choose pcbLotDetail1
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub pcbLotDetail1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLotDetail1.SelectedIndexChanged
    Try
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim LotDetails As Integer = cmbLotDetail1.SelectedIndex
      Dim intCount As Integer = -1
      cmbLotInfo1.Items.Clear()
      PcalableName1.Text = " "

      If cmbLotDetail1.SelectedItem.ToString <> " " Then
        selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_LMD" & (LotDetails).ToString)
        reader = selectCommand.ExecuteReader

        While reader.Read() = True
          intCount = intCount + 1
          If nameArray Is Nothing Then
            nameArray = {String.Empty}
          Else
            ReDim Preserve nameArray(nameArray.Length)
          End If
          cmbLotInfo1.Items.Add(reader.GetValue("sl_lmd_ldcd").ToString)
          nameArray(nameArray.Length - 1) = reader.GetValue("sl_lmd_mei").ToString
        End While
        reader.Close()
        reader.Dispose()
      Else
        cmbLotInfo1.Items.Add(String.Empty)
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub
  ''' <summary>
  ''' get data put into combobox csLotInfo2 after choose pcbLotDetail2
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub pcbLotDetail2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLotDetail2.SelectedIndexChanged
    Try
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim LotDetails As Integer = cmbLotDetail2.SelectedIndex
      Dim intCount As Integer = -1
      cmbLotInfo2.Items.Clear()
      PcalableName2.Text = " "

      If cmbLotDetail2.SelectedItem.ToString <> " " Then
        selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_LMD" & (LotDetails).ToString)
        reader = selectCommand.ExecuteReader

        While reader.Read() = True
          intCount = intCount + 1
          If nameArray2 Is Nothing Then
            nameArray2 = {String.Empty}
          Else
            ReDim Preserve nameArray2(nameArray2.Length)
          End If
          cmbLotInfo2.Items.Add(reader.GetValue("sl_lmd_ldcd").ToString)
          nameArray2(nameArray2.Length - 1) = reader.GetValue("sl_lmd_mei").ToString
        End While
        reader.Close()
        reader.Dispose()
      Else
        cmbLotInfo2.Items.Add(String.Empty)
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub
  ''' <summary>
  ''' get data put into combobox csLotInfo3 after choose pcbLotDetail3
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub pcbLotDetail3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLotDetail3.SelectedIndexChanged
    Try
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim LotDetails As Integer = cmbLotDetail3.SelectedIndex
      Dim intCount As Integer = -1
      cmbLotInfo3.Items.Clear()
      PcalableName3.Text = " "

      If cmbLotDetail3.SelectedItem.ToString <> " " Then
        selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_LMD" & (LotDetails).ToString)
        reader = selectCommand.ExecuteReader

        While reader.Read() = True
          intCount = intCount + 1
          If nameArray3 Is Nothing Then
            nameArray3 = {String.Empty}
          Else
            ReDim Preserve nameArray3(nameArray3.Length)
          End If
          cmbLotInfo3.Items.Add(reader.GetValue("sl_lmd_ldcd").ToString)
          nameArray3(nameArray3.Length - 1) = reader.GetValue("sl_lmd_mei").ToString
        End While
        reader.Close()
        reader.Dispose()
      Else
        cmbLotInfo3.Items.Add(String.Empty)
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub
  ''' <summary>
  ''' event set name for PcalableName1 after choose combobox csLotInfo1 
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub csLotInfo1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLotInfo1.SelectedIndexChanged
    Dim index As Integer = cmbLotInfo1.SelectedIndex
    PcalableName1.Text = nameArray(index)
  End Sub
  ''' <summary>
  ''' event set name for PcalableName2 after choose combobox csLotInfo2
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub csLotInfo2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLotInfo2.SelectedIndexChanged
    Dim index As Integer = cmbLotInfo2.SelectedIndex
    PcalableName2.Text = nameArray2(index)
  End Sub
  ''' <summary>
  ''' event set name for PcalableName3 after choose combobox csLotInfo3
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub csLotInfo3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLotInfo3.SelectedIndexChanged
    Dim index As Integer = cmbLotInfo3.SelectedIndex
    PcalableName3.Text = nameArray3(index)
  End Sub
#End Region
  ''' <summary>
  ''' join productCode has two or more of the same data and lotNo same data
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub dgv_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgv.CellPainting
    If m_isDataSet AndAlso Not duplicatedCell Is Nothing Then

      If e.ColumnIndex <= COLUMNIDX.LotNo AndAlso e.RowIndex > -1 Then
        Using gridBrush As Brush = New SolidBrush(Me.dgv.GridColor), backColorBrush As Brush = New SolidBrush(e.CellStyle.BackColor)

          Using gridLinePen As Pen = New Pen(gridBrush)

            ' Clear cell  
            e.Graphics.FillRectangle(backColorBrush, e.CellBounds)

            ' Draw line (bottom border and right border of current cell)  
            'If next row cell has different content, only draw bottom border line of current cell  
            If e.RowIndex < ColumnResults.Length - 1 Then
              If duplicatedCell Is Nothing OrElse Not duplicatedCell(e.RowIndex + 1, e.ColumnIndex) Then
                e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1, e.CellBounds.Right - 1, e.CellBounds.Bottom - 1)
              End If
            Else
              e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1, e.CellBounds.Right - 1, e.CellBounds.Bottom - 1)
            End If

            ' Draw right border line of current cell  
            e.Graphics.DrawLine(gridLinePen, e.CellBounds.Right - 1, e.CellBounds.Top, e.CellBounds.Right - 1, e.CellBounds.Bottom)

            '  draw/fill content in current cell, and fill only one cell of multiple same cells  
            If Not e.Value Is Nothing Then
              If duplicatedCell(e.RowIndex, e.ColumnIndex) Then
              Else
                e.PaintContent(e.CellBounds) ' Fill in checkbox column
                If e.ColumnIndex = COLUMNIDX.LotNo Then
                  Dim a As Integer = 0
                End If
              End If
            End If
            e.Handled = True
          End Using
        End Using
      End If
    End If
  End Sub

  Private Sub csLotInfo3_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLotInfo3.Validated
    If m_isSearching Then
      m_isSearching = False
      Search()
    End If
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub
End Class

Public Class ColumnName
  Public Const Productcode As String = "商品"
  Public Const ProductName As String = "商品名"
  Public Const Warehouse As String = "倉庫名"
  Public Const LotNo As String = "ロットNo"
  Public Const ShippingDate As String = "入出庫日"
  Public Const Type As String = "区分"
  Public Const ShippingDestination As String = "入出荷先"
  Public Const WarehousingMember As String = "入庫"
  Public Const WarehousingQuantity As String = "入庫数量"
  Public Const ShippingMumber As String = "出庫"
  Public Const ShippingQuantity As String = "出庫数量"
  Public Const StockNumber As String = "在庫"
  Public Const StockQuantity As String = "在庫数量"
  Public Const Unit As String = "単位"
  Public Const SlipNo As String = "伝票番号"
  Public Const Note As String = "note"
  Public Const Id As String = "ID"
  Public Const RecType As String = "RECTYPE"
  Public Const UnitPrice As String = "UNITPRICE"

End Class

Public Class ColumnResult
  Public Property Productcode As String = String.empty
  Public Property ProductName As String = String.empty
  Public Property Warehouse As String = String.Empty
  Public Property LotNo As String = String.Empty
  Public Property ShippingDate As String = String.empty
  Public Property Type As String = String.empty
  Public Property ShippingDest As String = String.empty
  Public Property WHNum As Integer
  Public Property WHQty As Decimal
  Public Property ShippingNum As Integer
  Public Property ShippingQty As Decimal
  Public Property StockNum As Integer
  Public Property StockQty As Decimal
  Public Property UnitName As String = String.empty
  Public Property SlipNo As String = String.empty
  Public Property Note As String = String.empty
  Public Property Lotdetails As String = String.empty
  Public Property ID As String = String.empty
  Public Property UnitPrice As Decimal
  Public Property RecType As String = String.empty
  Public Property StockQtyDecDigit As Integer
  Public Property WHQtyDecDigit As Integer
  Public Property ShippingQtyDecDigit As Integer
  Public Property UnitPriceNoOfDecDigit As Integer
End Class
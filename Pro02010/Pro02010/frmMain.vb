﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports PCA.UITools
Imports PCA.Controls
Imports PCA.TSC.Kon.Tools
Imports PCA.TSC.Kon.Tools.PEKonInput
Imports System.Xml
Imports System.IO
Imports System.ComponentModel
Imports Sunloft.PCAIF
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports PCA.TSC.Kon
Imports PCA
Imports System.Globalization
Imports System

Public Class frmMain

#Region "Declare - 変数宣言"
  Private connector As IIntegratedApplication = Nothing
  'Table Column's width Constants
  Private Const NOTE_LENGTH As Integer = 40
  Private Const QUANTITY_LENGTH As Integer = 10
  Private Const NUMBER_LENGTH As Integer = 6

  Private Const BRANCH_WIDTH As Double = 1.0F
  Private Const TYPE_WIDTH As Double = 3.0F
  Private Const PRODUCT_CODE_WIDTH As Double = 3.0F
  Private Const COLOR_WIDTH As Double = 2.0F
  Private Const SIZE_WIDTH As Double = 2.0F
  Private Const WAREHOUSE_WIDTH As Double = 3.0F
  Private Const PRODUCT_NAME_WIDTH As Double = PRODUCT_CODE_WIDTH + COLOR_WIDTH + SIZE_WIDTH + WAREHOUSE_WIDTH
  Private Const TAX_WIDTH As Double = 3.0F
  Private Const NUMBER_WIDTH As Double = 2.0F
  Private Const QTT_PER_CASE_WIDTH As Double = 2.0F
  Private Const NO_OF_CASE_WIDTH As Double = 2.0F
  Private Const QUANTITY_WIDTH As Double = 2.5F
  Private Const UNIT_WITDH As Double = 1.5F
  Private Const UNIT_PRICE_WIDTH As Double = 3.0F
  Private Const AMOUNT_WIDTH As Double = 3.0F
  Private Const LOT_NO_WIDTH As Double = 3.0F
  Private Const SUP_LOT_NO_WIDTH As Double = 3.0F
  Private Const NOTE_WIDTH As Double = LOT_NO_WIDTH + SUP_LOT_NO_WIDTH

  'Pop up message location
  Private Const SUM_TABLE_WIDTH As Double = BRANCH_WIDTH + TYPE_WIDTH + PRODUCT_NAME_WIDTH + TAX_WIDTH + QUANTITY_WIDTH + UNIT_WITDH + NUMBER_WIDTH + UNIT_PRICE_WIDTH + AMOUNT_WIDTH + LOT_NO_WIDTH + SUP_LOT_NO_WIDTH
  Private Const TYPE_ERRMESSAGE_LOCATION As Double = (BRANCH_WIDTH + TYPE_WIDTH) / SUM_TABLE_WIDTH
  Private Const WAREHOUSE_ERRMESSAGE_LOCATION As Double = (BRANCH_WIDTH + TYPE_WIDTH + PRODUCT_NAME_WIDTH) / SUM_TABLE_WIDTH
  Private Const QUANTITY_ERRMESSAGE_LOCATION As Double = (BRANCH_WIDTH + TYPE_WIDTH + PRODUCT_NAME_WIDTH + TAX_WIDTH + QUANTITY_WIDTH) / SUM_TABLE_WIDTH
  Private Const NUMBER_ERRMESSAGE_LOCATION As Double = (BRANCH_WIDTH + TYPE_WIDTH + PRODUCT_NAME_WIDTH + TAX_WIDTH + QUANTITY_WIDTH + NUMBER_WIDTH) / SUM_TABLE_WIDTH
  Private Const LOT_ERRMESSAGE_LOCATION As Double = (BRANCH_WIDTH + TYPE_WIDTH + PRODUCT_NAME_WIDTH + TAX_WIDTH + QUANTITY_WIDTH + UNIT_WITDH + NUMBER_WIDTH + UNIT_PRICE_WIDTH + AMOUNT_WIDTH + LOT_NO_WIDTH) / SUM_TABLE_WIDTH

  Private Const LOT_NO_COLUMN = 8
  Private Const LOT_NO_ULOT_CELL = 0
  Private Const LOT_NO_ALOT_CELL = 1
  Private Const PRODUCT_COLUMN = 2
  Private Const PRODUCT_CODE_CELL = 0
  Private Const WAREHOUSE_CODE_CELL = 3

  'Save to common lib?
  Private Const CONVERT_FLAG_NONE As Integer = 0
  Private Const CONVERT_FLAG_DONE As Integer = 1

  'HeaderColumnName
  Private Const HEADER_NO As String = "No"
  Private Const HEADER_TYPE As String = "Type"
  Private Const HEADER_PRODUCT As String = "Product"
  Private Const HEADER_TAX As String = "Tax"
  Private Const HEADER_QTIES As String = "Qties"
  Private Const HEADER_NUM As String = "Number"
  Private Const HEADER_UNITPRICE As String = "Unitprice"
  Private Const HEADER_AMOUNT As String = "Amount"
  Private Const HEADER_OTHER As String = "Other"

  Private m_conf As Sunloft.PcaConfig
  Private m_appClass As ConnectToPCA = New ConnectToPCA

  Private m_strColName As String = String.Empty
  Private m_IntSlipNo As Integer
  Private m_strCurrentUser As String = ""
  Private m_strSearchItem As String = ""
  Private m_IsF12CloseFlag As Boolean = True
  Private m_IntRowIndex As Integer = -1
  Private m_IntRowCount As Integer = 0
  Private m_CustomerTaxIncluded As Integer
  Private m_IntTax_rate As Double() = {0}
  Private m_intOrderSlipID As Integer
  Private m_intPurSlipID As Integer

  Private AMS1Class As AMS1
  Private SL_SYKHClass As SL_SYKH
  Private SL_SYKDClass As SL_SYKD
  Private SYKD_List As SL_SYKD()
  Private SL_ZDNClass As SL_ZDN
  Private SL_LMBClass As SL_LMB
  Private MasterDialog As SLMasterSearchDialog

  Private m_strCurCode As String = String.Empty
  Private m_isShowDialog As Boolean = False
  Private m_SlipType As SlipType
  Private m_isDataChanged As Boolean = False
  Private m_isFormClosing As Boolean = False
  Private m_isUseLot As Boolean = True

  Private htItemAmountNum As Hashtable = New Hashtable
  Private htItemAmountQty As Hashtable = New Hashtable
  Private htItemErrRow As Hashtable = New Hashtable

  Private Enum SlipType
    Self
    Order
    Sup
  End Enum

  '<Browsable(False)> _
  Public Property IsValidate() As Boolean = False
#End Region

#Region "Initialize"

  Public Sub New()
    Try

      InitializeComponent()

      Dim strParam As String = Command()
      Dim intParam As Integer = 0
      Dim args As String = m_appClass.argumentContent

      Dim parts As String() = strParam.Split(New Char() {" "c})


      If parts.Length > 1 Then
        m_appClass.ConnectToPCA(strParam)
        connector = m_appClass.connector
      ElseIf Integer.TryParse(strParam, intParam) Then
        connector = Sunloft.PCAIF.ConnectToPCA.ConnectToPCACalledPG()
      Else
        m_appClass.ConnectToPCA() 'start normally (from code or menu)
        connector = m_appClass.connector
      End If

      m_conf = New Sunloft.PcaConfig

      InitControl()
      InitTable()

      setType.Focus()

      tblTable.Enabled = False

      PcaFunctionCommandRefer.Enabled = False
      PcaFunctionCommandCopy.Enabled = False
      PcaFunctionCommandNext.Enabled = False
      PcaFunctionCommandInv.Enabled = False

      ToolStripButtonNext.Enabled = False
      ToolStripButtonCopy.Enabled = False
      ToolStripButtonDelete.Enabled = False

      ToolStripButtonDelete.Enabled = False
      ToolStripButtonNext.Enabled = False
      ToolStripButtonCopy.Enabled = False

      If intParam <> 0 OrElse (args <> String.Empty AndAlso Integer.TryParse(args, intParam)) Then
        DisplaySlipByNo(intParam)
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitControl()
    Try

      AMS1Class = New AMS1(connector)
      AMS1Class.ReadAll() 'Get number of digits for fields
      SL_SYKHClass = New SL_SYKH(connector)
      SL_SYKDClass = New SL_SYKD(connector)
      SL_ZDNClass = New SL_ZDN(connector)
      SL_LMBClass = New SL_LMB(connector)
      MasterDialog = New SLMasterSearchDialog(connector)

      setSupCode.LimitLength = AMS1Class.ams1_SupplierLength
      setPerson.LimitLength = AMS1Class.ams1_PersonLength
      setDepartment.LimitLength = AMS1Class.ams1_DepartmentLength
      setMemoCode.LimitLength = AMS1Class.ams1_MemoLength

      m_SlipType = SlipType.Self

      ldtShipmentDate.Date = Now
      ldtDlvDate.Date = Now.AddDays(1)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitTable()
    Try
      With tblTable
        .DataRowCount = 99

        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader()
        InitTableBody()

        .UpdateTable()

        tblTable.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
        tblTable.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
        tblTable.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
        tblTable.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    Try

      MyBase.OnLoad(e)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>Initialize TableHeader</summary>
  ''' <remarks>Init Table Header when create Table</remarks>
  Private Sub InitTableHeader()
    Try
      tblTable.HeadTextHeight = 3
      tblTable.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
      tblTable.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell

      SetTableHeaderName()

      'Big column 1
      column = New PcaColumn(HEADER_NO, BRANCH_WIDTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.No)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 2
      column = New PcaColumn(HEADER_TYPE, TYPE_WIDTH)
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Type)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 3
      column = New PcaColumn(HEADER_PRODUCT, CSng(PRODUCT_NAME_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.ProductCodeDisp, CSng(PRODUCT_CODE_WIDTH), 1, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.Color, CSng(COLOR_WIDTH), 1, PRODUCT_CODE_WIDTH, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.Size, CSng(SIZE_WIDTH), 1, PRODUCT_CODE_WIDTH + COLOR_WIDTH, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.Warehouse2, CSng(WAREHOUSE_WIDTH), 1, PRODUCT_CODE_WIDTH + COLOR_WIDTH + SIZE_WIDTH, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.ProductName, CSng(PRODUCT_NAME_WIDTH), 1, 0.0F, 1.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.Spec, CSng(PRODUCT_NAME_WIDTH), 1, 0.0F, 2.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 4
      column = New PcaColumn(HEADER_TAX, CSng(TAX_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.TaxIncluded, CSng(TAX_WIDTH), 2, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.Rate, CSng(TAX_WIDTH), 1, 0.0F, 2.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 5
      column = New PcaColumn(HEADER_QTIES, CSng(NO_OF_CASE_WIDTH + QTT_PER_CASE_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.QtyPerCase, CSng(QTT_PER_CASE_WIDTH), 1, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.NoOfCase, CSng(NO_OF_CASE_WIDTH), 1, QTT_PER_CASE_WIDTH, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.Qty, CSng(QUANTITY_WIDTH), 2, 0.0F, 1.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.UnitName, CSng(UNIT_WITDH), 2, QUANTITY_WIDTH, 1.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 6
      column = New PcaColumn(HEADER_NUM, CSng(NUMBER_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Number, CSng(NUMBER_WIDTH), 3, 0.0F, 0.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 7
      column = New PcaColumn(HEADER_UNITPRICE, CSng(UNIT_PRICE_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.UnitPriceG, CSng(UNIT_PRICE_WIDTH), 2, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.UnitPrice, CSng(UNIT_PRICE_WIDTH), 1, 0.0F, 2.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 8
      column = New PcaColumn(HEADER_AMOUNT, CSng(AMOUNT_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.AmountG, CSng(AMOUNT_WIDTH), 2, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.Amount, CSng(AMOUNT_WIDTH), 1, 0.0F, 2.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Big column 9
      column = New PcaColumn(HEADER_OTHER, CSng(NOTE_WIDTH) + CSng(LOT_NO_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareFixTextCell(ColumnName.Note, CSng(NOTE_WIDTH) + CSng(LOT_NO_WIDTH), 2, 0.0F, 0.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.Arari, CSng(NOTE_WIDTH), 1, 0.0F, 2.0F)
      column.Cells.Add(cell)
      cell = tblTable.PrepareFixTextCell(ColumnName.UserLotNo, CSng(LOT_NO_WIDTH), 1, CSng(NOTE_WIDTH), 2.0F)
      column.Cells.Add(cell)
      tblTable.HeadColumns.Add(column)

      'Invisible column (From Lot Details screen)
      InitHeaderColumn(column, cell, 0, ColumnName.QtyNoOfDec, ColumnName.QtyNoOfDec)
      InitHeaderColumn(column, cell, 0, ColumnName.UnitPriceNoOfDec, ColumnName.UnitPriceNoOfDec)
      InitHeaderColumn(column, cell, 0, ColumnName.InvID, ColumnName.InvID)
      InitHeaderColumn(column, cell, 0, ColumnName.PrevInvID, ColumnName.PrevInvID)
      InitHeaderColumn(column, cell, 0, ColumnName.PrevNumber, ColumnName.PrevNumber)
      InitHeaderColumn(column, cell, 0, ColumnName.PrevDecQty, ColumnName.PrevDecQty)
      InitHeaderColumn(column, cell, 0, ColumnName.IsUseLot, ColumnName.IsUseLot)
      InitHeaderColumn(column, cell, 0, ColumnName.PrevID, ColumnName.PrevID)
      InitHeaderColumn(column, cell, 0, ColumnName.ConvertFlag, ColumnName.ConvertFlag)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>Initialize TableHeader(Default)</summary>
  ''' <param name="column"></param>
  ''' <param name="cell"></param>
  ''' <param name="columnWidth"></param>
  ''' <param name="cellName"></param>
  ''' <param name="columnName"></param>
  ''' <remarks>Init a Header column</remarks>
  Private Sub InitHeaderColumn(ByVal column As PcaColumn, ByVal cell As PcaCell, ByVal columnWidth As Integer, ByVal cellName As String, ByVal columnName As String)
    column = New PcaColumn(columnName, columnWidth)
    column.CanResize = True
    cell = tblTable.PrepareFixTextCell(cellName, columnWidth, 3, 0.0F, 0.0F)
    column.Cells.Add(cell)
    tblTable.HeadColumns.Add(column)
  End Sub

  ''' <summary>Initialize TableBody</summary>
  ''' <remarks>InitTableBody when create Table</remarks>
  Private Sub InitTableBody()
    Try
      tblTable.BodyTextHeight = 3

      tblTable.BodyColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableRowForeColor
      tblTable.BodyColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableRowBackColor
      tblTable.BodyColumns.DefaultAltStyle.ForeColor = DefaultLabelStyle.TableRowAltForeColor
      tblTable.BodyColumns.DefaultAltStyle.BackColor = DefaultLabelStyle.TableRowAltBackColor

      Dim column As PcaColumn
      Dim cell As PcaCell

      'Big column 1
      column = New PcaColumn(HEADER_NO, BRANCH_WIDTH)
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.No, 1.0F, 3.0F, 0.0F, 0.0F, ColumnName.No)
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 2
      column = New PcaColumn(HEADER_TYPE, TYPE_WIDTH)
      column.CanResize = True
      cell = New PcaCell(ColumnName.Type, CellType.ItemSelect, 0.0F, 0.0F)
      SLCmnFunction.SetPcaCellCombo(cell, SLConstants.ComboType.Shipping)
      cell.EditMode = True
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)
      tblTable.BodyColumns(HEADER_TYPE).AltStyle = New PcaColumnStyle(Color.Black, Color.White)
      tblTable.BodyColumns(HEADER_TYPE).Style = New PcaColumnStyle(Color.Black, Color.White)

      'Big column 3
      column = New PcaColumn(HEADER_PRODUCT, CSng(PRODUCT_NAME_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareSyohinCode(CSng(PRODUCT_CODE_WIDTH), 1, 0.0F, 0.0F, ColumnName.ProductCodeDisp)
      cell.LimitLength = CShort(AMS1Class.ams1_ProductLength)
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.Color, CSng(COLOR_WIDTH), 1, PRODUCT_CODE_WIDTH, 0.0F, ColumnName.Color)
      cell.LimitLength = 7
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.Size, CSng(SIZE_WIDTH), 1, PRODUCT_CODE_WIDTH + COLOR_WIDTH, 0.0F, ColumnName.Size)
      cell.LimitLength = 5
      column.Cells.Add(cell)
      cell = tblTable.PrepareSokoCode(CSng(WAREHOUSE_WIDTH), 1, PRODUCT_CODE_WIDTH + COLOR_WIDTH + SIZE_WIDTH, 0.0F, ColumnName.Warehouse2)
      cell.LimitLength = CShort(AMS1Class.ams1_WarehouseLength)
      cell.CellStyle.CorrectBorderSide = CellBorderSide.All
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.ProductName, CSng(PRODUCT_NAME_WIDTH), 1, 0.0F, 1.0F, ColumnName.ProductName)
      cell.LimitLength = 35
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.Spec, CSng(PRODUCT_NAME_WIDTH), 1, 0.0F, 2.0F, ColumnName.Spec)
      cell.LimitLength = 35
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 4
      column = New PcaColumn(HEADER_TAX, CSng(TAX_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareTextCell(ColumnName.TaxIncluded, CSng(TAX_WIDTH), 2, 0.0F, 0.0F, ColumnName.TaxIncluded)
      cell.CellStyle.Alignment = StringAlignment.Center
      cell.EditMode = False
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.Rate, CSng(TAX_WIDTH), 1, 0.0F, 2.0F, ColumnName.Rate)
      cell.CellStyle.Alignment = StringAlignment.Far
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 5
      column = New PcaColumn(HEADER_QTIES, CSng(NO_OF_CASE_WIDTH + QTT_PER_CASE_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.QtyPerCase, CSng(QTT_PER_CASE_WIDTH), 1, 0.0F, 0.0F, ColumnName.QtyPerCase)
      cell.LimitLength = NUMBER_LENGTH
      column.Cells.Add(cell)

      cell = tblTable.PrepareNumberCell(ColumnName.NoOfCase, CSng(NO_OF_CASE_WIDTH), 1, QTT_PER_CASE_WIDTH, 0.0F, ColumnName.NoOfCase)
      cell.LimitLength = NUMBER_LENGTH
      column.Cells.Add(cell)

      cell = tblTable.PrepareTextCell("Nothing", CSng(QUANTITY_WIDTH), 1, 0.0F, 1.0F, ColumnName.Qty)
      cell.EditMode = False
      cell.CellStyle.BorderSide = CellBorderSide.Right
      column.Cells.Add(cell)

      cell = New PcaCell(ColumnName.Qty, CellType.Number, 0.0, 2.0)
      cell = tblTable.PrepareNumberCell(ColumnName.Qty, CSng(QUANTITY_WIDTH), 1, 0.0F, 2.0F, ColumnName.Qty)
      cell.CanResize = True
      cell.AllowMinus = True
      column.Cells.Add(cell)

      cell = tblTable.PrepareTextCell(ColumnName.UnitName, CSng(UNIT_WITDH), 1, QUANTITY_WIDTH, 2.0F, ColumnName.UnitName)
      cell.EditMode = False
      cell.CellStyle.BorderSide = CellBorderSide.None
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 6
      column = New PcaColumn(HEADER_NUM, CSng(NUMBER_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.Number, CSng(NUMBER_WIDTH), 1, 0.0F, 2.0F, ColumnName.Number)
      cell.LimitLength = NUMBER_LENGTH
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 7
      column = New PcaColumn(HEADER_UNITPRICE, CSng(UNIT_PRICE_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.UnitPriceG, CSng(UNIT_PRICE_WIDTH), 2, 0.0F, 0.0F, ColumnName.UnitPriceG)
      cell.EditMode = False
      cell.Enabled = False
      column.Cells.Add(cell)
      cell = tblTable.PrepareNumberCell(ColumnName.UnitPrice, CSng(UNIT_PRICE_WIDTH), 1, 0.0F, 2.0F, ColumnName.UnitPrice)
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 8
      column = New PcaColumn(HEADER_AMOUNT, CSng(AMOUNT_WIDTH))
      column.CanResize = True
      cell = tblTable.PrepareNumberCell(ColumnName.AmountG, CSng(AMOUNT_WIDTH), 2, 0.0F, 0.0, ColumnName.AmountG)
      cell.AllowMinus = True
      cell.EditMode = False
      cell.Enabled = False
      column.Cells.Add(cell)
      cell = tblTable.PrepareNumberCell(ColumnName.Amount, CSng(AMOUNT_WIDTH), 1, 0.0F, 2.0, ColumnName.Amount)
      cell.AllowMinus = True
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      'Big column 9
      column = New PcaColumn(HEADER_OTHER, CSng(NOTE_WIDTH) + CSng(LOT_NO_WIDTH))
      column.CanResize = True

      cell = tblTable.PrepareTextCell(ColumnName.Note, CSng(NOTE_WIDTH) + CSng(LOT_NO_WIDTH), 2, 0.0F, 0.0F, ColumnName.Note)
      cell.LimitLength = NOTE_LENGTH
      column.Cells.Add(cell)
      cell = tblTable.PrepareNumberCell(ColumnName.Arari, CSng(NOTE_WIDTH), 1, 0.0F, 2.0F, ColumnName.Arari)
      cell.CalculatorEnabled = False
      cell.AllowMinus = True
      column.Cells.Add(cell)
      cell = tblTable.PrepareTextCell(ColumnName.UserLotNo, CSng(LOT_NO_WIDTH), 1, CSng(NOTE_WIDTH), 2.0F, ColumnName.UserLotNo)
      cell.EditMode = False
      column.Cells.Add(cell)
      tblTable.BodyColumns.Add(column)

      InitBodyColumnNumber(column, cell, ColumnName.QtyNoOfDec, ColumnName.QtyNoOfDec, 0, 3, ColumnName.QtyNoOfDec, False)
      InitBodyColumnNumber(column, cell, ColumnName.UnitPriceNoOfDec, ColumnName.UnitPriceNoOfDec, 0, 3, ColumnName.UnitPriceNoOfDec, False)
      InitBodyColumnNumber(column, cell, ColumnName.InvID, ColumnName.InvID, 0, 3, ColumnName.InvID, False)

      InitBodyColumnNumber(column, cell, ColumnName.PrevInvID, ColumnName.PrevInvID, 0, 3, ColumnName.PrevInvID, False)
      InitBodyColumnNumber(column, cell, ColumnName.PrevNumber, ColumnName.PrevNumber, 0, 3, ColumnName.PrevNumber, False)
      InitBodyColumnNumber(column, cell, ColumnName.PrevDecQty, ColumnName.PrevDecQty, 0, 3, ColumnName.PrevDecQty, False)
      InitBodyColumnNumber(column, cell, ColumnName.IsUseLot, ColumnName.IsUseLot, 0, 3, ColumnName.IsUseLot, False)
      InitBodyColumnNumber(column, cell, ColumnName.PrevID, ColumnName.PrevID, 0, 3, ColumnName.PrevID, False)
      InitBodyColumnNumber(column, cell, ColumnName.ConvertFlag, ColumnName.ConvertFlag, 0, 3, ColumnName.ConvertFlag, False)


    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>Initialize TableBody(Default)</summary>
  ''' <param name="column"></param>
  ''' <param name="cell"></param>
  ''' <param name="columnName"></param>
  ''' <param name="cellname"></param>
  ''' <param name="cellwidth"></param>
  ''' <param name="cellHeight"></param>
  ''' <param name="headerName"></param>
  ''' <param name="editable"></param>
  ''' <remarks>Init Text column when create Table </remarks>
  Private Sub InitBodyColumnText(column As PcaColumn, cell As PcaCell, columnName As String, cellname As String, cellwidth As Short, cellHeight As Short, headerName As String, Optional ByVal editable As Boolean = True)
    'Create column text
    column = New PcaColumn(columnName, cellwidth)
    column.CanResize = True
    cell = tblTable.PrepareTextCell(cellname, cellwidth, cellHeight, 0, 0, headerName)
    cell.EditMode = editable
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)
  End Sub

  ''' <summary>Initialize TableBody(Number)</summary>
  ''' <param name="column"></param>
  ''' <param name="cell"></param>
  ''' <param name="columnName"></param>
  ''' <param name="cellname"></param>
  ''' <param name="cellwidth"></param>
  ''' <param name="cellHeight"></param>
  ''' <param name="headerName"></param>
  ''' <param name="editable"></param>
  ''' <remarks>Init Number column when create Table </remarks>
  Private Sub InitBodyColumnNumber(column As PcaColumn, cell As PcaCell, columnName As String, cellname As String, cellwidth As Short, cellHeight As Short, headerName As String, Optional ByVal editable As Boolean = True)
    'Create column number. Allow decimal (For Current Quantity)
    column = New PcaColumn(columnName, cellwidth)
    column.CanResize = True
    cell = tblTable.PrepareNumberCell(cellname, cellwidth, cellHeight, 0, 0, headerName)
    cell.FormatDecimal = True
    cell.EditMode = editable
    column.Cells.Add(cell)
    tblTable.BodyColumns.Add(column)
  End Sub

  ''' <summary>
  ''' Set to initial mode
  ''' </summary>
  ''' <param name="isClearData">Clear all Hearer fields. False when Copying slip</param>
  ''' <remarks></remarks>
  Public Sub InitCreateMode(Optional isClearData As Boolean = True)
    Try
      HeaderLabel.State = LabelStateType.New
      If isClearData Then
        ClearData()
        m_IntSlipNo = 0
        m_strCurrentUser = ""
        m_strSearchItem = ""
        m_IsF12CloseFlag = True
        m_IntRowIndex = 0
        m_IntRowCount = 0
        tblTable.Enabled = False
      End If

      SL_SYKHClass = New SL_SYKH(connector)
      SL_SYKDClass = New SL_SYKD(connector)
      SYKD_List = Nothing

      setSupCode.Enabled = True
      setOrderNo.Enabled = True
      setPurNo.Enabled = True

      PcaFunctionCommandNext.Enabled = False
      PcaFunctionCommandCopy.Enabled = False

      ToolStripMenuItemDelete.Enabled = False
      ToolStripMenuItemNext.Enabled = False
      ToolStripMenuItemCopy.Enabled = False

      ToolStripButtonDelete.Enabled = False
      ToolStripButtonNext.Enabled = False
      ToolStripButtonCopy.Enabled = False
      m_isDataChanged = False
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Set to edit mode.
  ''' </summary>
  ''' <remarks>2015/11/27  by Lai Minh Hien</remarks>
  Private Sub InitEditMode()
    HeaderLabel.State = LabelStateType.Modify
    m_SlipType = SlipType.Self
    setSupCode.Enabled = False
    setOrderNo.Enabled = False
    setPurNo.Enabled = False
    PcaFunctionCommandCopy.Enabled = True
    PcaFunctionCommandNext.Enabled = True

    ToolStripMenuItemDelete.Enabled = True
    ToolStripMenuItemNext.Enabled = True
    ToolStripMenuItemCopy.Enabled = True
    ToolStripMenuItemDeleteRow.Enabled = True
    ToolStripMenuItemInsertRow.Enabled = True

    ToolStripButtonDelete.Enabled = True
    ToolStripButtonNext.Enabled = True
    ToolStripButtonCopy.Enabled = True
    ToolStripButtonDeleteRow.Enabled = True
    ToolStripButtonInsertRow.Enabled = True

    m_isDataChanged = False
  End Sub
#End Region

#Region "Private method"

  Private Sub SetTableHeaderName()
    Try
      ColumnName.Color = " " & AMS1Class.ColorName
      ColumnName.Size = AMS1Class.SizeName
      ColumnName.Spec = AMS1Class.SpecName
      ColumnName.QtyPerCase = AMS1Class.QttPerCaseName
      ColumnName.NoOfCase = AMS1Class.NoOfCaseName
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  Private Sub SetRetZaikoDlg(ByVal ZaikoDialog As Sunloft.PCAForms.SLZaikoDialog)
    Try
      Dim decQty As Decimal
      Dim decQtyPerCase As Decimal
      Dim intKeta As Integer : Dim intAmount As Integer : Dim intAmountG As Integer

      intKeta = SetPropertiesByProduct(m_IntRowIndex, tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode).ToString)

      tblTable.SetCellValue(m_IntRowIndex, ColumnName.UserLotNo, ZaikoDialog.RetStrLotNo)
      'tblTable.SetCellValue(m_IntRowIndex, ColumnName.UnitPrice, Format(ZaikoDialog.RetDecUnitPrice, "#,##0." & New String("0"c, intKeta)))
      tblTable.SetCellValue(m_IntRowIndex, ColumnName.UnitPriceG, Format(ZaikoDialog.RetDecUnitPrice, "#,##0." & New String("0"c, intKeta)))
      tblTable.SetCellValue(m_IntRowIndex, ColumnName.Number, ZaikoDialog.RetDecShippingNumber)
      tblTable.SetCellValue(m_IntRowIndex, ColumnName.Qty, ZaikoDialog.RetDecShippingQty)
      tblTable.SetCellValue(m_IntRowIndex, ColumnName.InvID, ZaikoDialog.RetIntInvID)
      tblTable.SetCellValue(m_IntRowIndex, ColumnName.Warehouse, ZaikoDialog.RetStrWarehouseCode)

      decQty = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Qty))
      decQtyPerCase = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.QtyPerCase))
      If decQtyPerCase = 0 Then
        decQtyPerCase = 1
        tblTable.SetCellValue(m_IntRowIndex, ColumnName.QtyPerCase, 1)
      End If
      If decQty Mod decQtyPerCase = 0D Then
        tblTable.SetCellValue(m_IntRowIndex, ColumnName.NoOfCase, Format(CInt(decQty / decQtyPerCase), "#,##0"))
      Else
        tblTable.SetCellValue(m_IntRowIndex, ColumnName.QtyPerCase, 1)
        tblTable.SetCellValue(m_IntRowIndex, ColumnName.NoOfCase, Format(decQty, "#,##0"))
      End If

      tblTable.SetCellValue(m_IntRowIndex, ColumnName.Amount, Format(CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPrice)) * decQty, "#,##0"))
      tblTable.SetCellValue(m_IntRowIndex, ColumnName.AmountG, Format(CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPriceG)) * decQty, "#,##0"))
      intAmount = CInt(CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPrice)) * CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Qty)))
      intAmountG = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.AmountG))
      tblTable.SetCellValue(m_IntRowIndex, ColumnName.Arari, Format(intAmount - intAmountG, "#,##0"))

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

  End Sub

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ClearData() As Boolean
    'Clear data 
    tblTable.ClearCellValues()
    m_IntRowCount = 0
    setType.CodeText = SLConstants.ShipmentKbn.NORMAL_SHIPPING_CODE.ToString
    ltxtWhNo.Text = ""
    setSupCode.CodeText = ""
    setOrderNo.CodeText = ""
    setPurNo.CodeText = ""
    ltxtWhNo2.Text = ""
    setPerson.CodeText = ""
    setDepartment.CodeText = ""
    setProject.CodeText = ""
    setMemoCode.CodeText = ""
    setMemoContent.CodeText = ""
    lblAddr.Text = ""
    lblSupFax.Text = "FAX: "
    lblSupName.Text = ""
    lblSupTax.Text = "消費税: "
    lblSupTel.Text = "TEL: "
    setPerson.NameText = " "
    setDepartment.NameText = ""
    setProject.NameText = ""
    ltxtCreatedAt.Text = ""
    ltxtCreatedBy.Text = ""
    ltxtModifiedAt.Text = ""
    ltxtModifiedBy.Text = ""
    Return True
  End Function

  ''' <summary>
  ''' Set number of decimal digit depends on product code
  ''' </summary>
  ''' <param name="rowIndex"></param>
  ''' <param name="strProductcode"></param>
  ''' <remarks>(PCA Sample)</remarks>
  Private Function SetPropertiesByProduct(ByVal rowIndex As Integer, ByVal strProductcode As String) As Integer
    Try
      Dim strStandardCode As String = SLCmnFunction.standardlizeCode(strProductcode, CShort(AMS1Class.ams1_ProductLength))
      Dim SMSClass As SMS = New SMS(connector)
      Dim SL_SMSClass As SL_SMS = New SL_SMS(connector)
      If SMSClass.ReadByID(strStandardCode) Then
        'Product found
        'Set quantity value
        Dim cell As PcaCell = tblTable.BodyColumns.GetCell(ColumnName.Qty)
        cell.DecimalDigit = CShort(SMSClass.sms_sketa)
        cell.FormatDecimal = True
        cell.Enabled = True

        'Set unit price value
        cell = tblTable.BodyColumns.GetCell(ColumnName.UnitPrice)
        cell.DecimalDigit = CShort(SMSClass.sms_tketa)
        cell.FormatDecimal = True
        cell.Enabled = True

        cell = tblTable.BodyColumns.GetCell(ColumnName.UnitPriceG)
        cell.DecimalDigit = CShort(SMSClass.sms_tketa)
        cell.FormatDecimal = True
        cell.Enabled = True

        cell = tblTable.BodyColumns.GetCell(ColumnName.Type)

        If m_SlipType = SlipType.Order Or SL_SYKHClass.sl_sykh_datakbn = SLConstants.SL_SYKH_RelaySlipType.OrderReceiving Then
          SLCmnFunction.SetPcaCellCombo(cell, SLConstants.ComboType.Shipping)
          tblTable.BodyColumns(HEADER_TYPE).AltStyle = New PcaColumnStyle(Color.Black, Color.White)
          tblTable.BodyColumns(HEADER_TYPE).Style = New PcaColumnStyle(Color.Black, Color.White)

        Else
          cell.SelectItems = {""}
          tblTable.BodyColumns(HEADER_TYPE).AltStyle = New PcaColumnStyle(Color.Black, Color.LightGray)
          tblTable.BodyColumns(HEADER_TYPE).Style = New PcaColumnStyle(Color.Black, Color.LightGray)
          cell.Enabled = False
        End If

        If SL_SMSClass.ReadByProductCode(strProductcode.Trim) Then
          cell = tblTable.BodyColumns.GetCell(ColumnName.IsUseLot)
          tblTable.SetCellValue(rowIndex, ColumnName.IsUseLot, SL_SMSClass.sl_sms_lkbn)
          If SL_SMSClass.sl_sms_lkbn = 1 Then
            m_isUseLot = True
          Else
            m_isUseLot = False
          End If
        Else
          tblTable.SetCellValue(rowIndex, ColumnName.IsUseLot, 0)
        End If

        tblTable.SetCellValue(rowIndex, ColumnName.No, rowIndex + 1)

        If HeaderLabel.State = LabelStateType.New Then tblTable.SetCellValue(rowIndex, ColumnName.PrevID, 0)

      End If
      Return SMSClass.sms_tketa
    Catch ex As Exception
      Return 0
      DisplayBox.ShowCritical(ex)
    End Try

  End Function

#End Region

#Region "PCA command Manager"
  ''' <summary>
  ''' Event called when PCACommandManager is called (PCA Function Bar button is pressed/Select from Menu, press F～ key)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try
      Dim commandItem As PcaCommandItem = e.CommandItem
      Dim isSuccess As Boolean = True

      Select Case commandItem.CommandName

        Case PcaCommandItemPrevious.CommandName 'F2
          Dim intPrevSlipNo As Integer = SL_SYKHClass.sl_sykh_denno
          If (HeaderLabel.State = LabelStateType.New And Not m_IsF12CloseFlag) Or (HeaderLabel.State = LabelStateType.Modify And m_isDataChanged) Then
            Dim dlgResult = DisplayBox.ShowContact(String.Empty)
            If dlgResult = Windows.Forms.DialogResult.Yes Then

              If ValidateInput() Then
                If HeaderLabel.State = LabelStateType.New Then
                  isSuccess = InsertData()
                Else
                  isSuccess = UpdateData()
                End If
              Else
                isSuccess = False
              End If

              If isSuccess Then
                intPrevSlipNo = SL_SYKHClass.sl_sykh_denno
                InitControl()
                HeaderLabel.State = LabelStateType.Modify
                m_IsF12CloseFlag = True
              Else
                Return
              End If
            ElseIf dlgResult = Windows.Forms.DialogResult.Cancel Then
              Return
            End If
          End If

          If HeaderLabel.State = LabelStateType.New Then intPrevSlipNo = -1

          'Display slip with max headerID          
          intPrevSlipNo = SL_SYKHClass.GetPrevSlipNo(intPrevSlipNo)

          If HeaderLabel.State = LabelStateType.New Then
            If SL_SYKHClass.GetNewSlipID() > 0 Then
              'Display slip with max header
              If DisplaySlipByNo(intPrevSlipNo) Then
                InitEditMode()
              End If
            Else
              DisplayBox.ShowError(SLConstants.NotifyMessage.PREVIOUS_MESSAGE_NOT_FOUND)
            End If

          ElseIf HeaderLabel.State = LabelStateType.Modify Then
            If intPrevSlipNo > 0 Then
              DisplaySlipByNo(intPrevSlipNo)
            Else
              DisplayBox.ShowError(SLConstants.NotifyMessage.PREVIOUS_MESSAGE_NOT_FOUND)
            End If

          End If

        Case PcaCommandItemNext.CommandName 'F3
          Dim intNextSlipNo As Integer = SL_SYKHClass.sl_sykh_denno

          If (HeaderLabel.State = LabelStateType.New And Not m_IsF12CloseFlag) Or (HeaderLabel.State = LabelStateType.Modify And m_isDataChanged) Then
            Dim dlgResult = DisplayBox.ShowContact(String.Empty)
            If dlgResult = Windows.Forms.DialogResult.Yes Then

              If ValidateInput() Then
                If HeaderLabel.State = LabelStateType.New Then
                  isSuccess = InsertData()
                Else
                  isSuccess = UpdateData()
                End If
              Else
                isSuccess = False
              End If

              If isSuccess Then
                intNextSlipNo = SL_SYKHClass.sl_sykh_denno
                InitControl()
                HeaderLabel.State = LabelStateType.Modify
                m_IsF12CloseFlag = True
              Else
                Return
              End If
            ElseIf dlgResult = Windows.Forms.DialogResult.Cancel Then
              Return
            End If
          End If

          'Only enable in Edit mode
          If HeaderLabel.State = LabelStateType.Modify Then

            intNextSlipNo = SL_SYKHClass.GetNextSlipNo(intNextSlipNo)

            If intNextSlipNo > 0 Then
              DisplaySlipByNo(intNextSlipNo)
            Else
              DisplayBox.ShowError(SLConstants.NotifyMessage.NEXT_MESSAGE_NOT_FOUND)
            End If

          End If

        Case PcaCommandItemSlipSearch.CommandName 'F6

          Dim DenpyoDialog = New SLDenSearchDialog(connector)
          Dim strSlipNo As String = DenpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.ShippingSlip)
          If Not String.IsNullOrEmpty(strSlipNo) Then
            ltxtWhNo.Text = DenpyoDialog.returnValue()
            ltxtWhNo_Validating(sender, New CancelEventArgs(False))
          End If

        Case PcaCommandItemInv.CommandName 'F7
          If CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.IsUseLot)) = 0 Then Exit Sub

          Dim ZaikoDialog As Sunloft.PCAForms.SLZaikoDialog
          Dim ShippingType As SLZaikoDialog.enmSlipType = CType(setType.CodeText, SLZaikoDialog.enmSlipType)
          Dim intStockID As Integer = -1 : Dim intNum As Integer = 0 : Dim decQty As Decimal = 0D

          If Not tblTable.GetCellValue(m_IntRowIndex, ColumnName.InvID) Is Nothing Then Integer.TryParse(tblTable.GetCellValue(m_IntRowIndex, ColumnName.InvID).ToString, intStockID)
          If Not tblTable.GetCellValue(m_IntRowIndex, ColumnName.Number) Is Nothing Then Integer.TryParse(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Number).ToString, intNum)
          If Not tblTable.GetCellValue(m_IntRowIndex, ColumnName.Qty) Is Nothing Then Decimal.TryParse(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Qty).ToString, decQty)
          ZaikoDialog = New Sunloft.PCAForms.SLZaikoDialog(connector, tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode).ToString, ldtShipmentDate.Date, _
                                                           ShippingType, tblTable.GetCellValue(m_IntRowIndex, ColumnName.Warehouse).ToString, intStockID, intNum, decQty, HeaderLabel.State)

          If ZaikoDialog.ShowDialog = Windows.Forms.DialogResult.OK Then SetRetZaikoDlg(ZaikoDialog)

        Case PcaCommandItemRefer.CommandName 'F8
          'SearchItem(sender)

        Case PcaCommandItemCopy.CommandName 'F11         
          InitCreateMode(False)
          ltxtWhNo.Text = String.Empty

          For intLpc As Integer = 0 To m_IntRowCount - 1
            tblTable.SetCellValue(intLpc, ColumnName.ConvertFlag, 0)
          Next

        Case PcaCommandItemSave.CommandName 'F12

          lblSupName.Focus() 'Focus on a label to validate all other text boxes.

          If ValidateInput() Then
            If HeaderLabel.State = LabelStateType.New Then
              isSuccess = InsertData()
            Else
              isSuccess = UpdateData()
            End If

            If isSuccess Then
              InitCreateMode()
              m_IsF12CloseFlag = True

            End If
          End If

        Case PcaCommandItemClose.CommandName 'F12
          m_isFormClosing = True
          Me.Close()

        Case PcaCommandItemDelete.CommandName 'Delete
          Dim intConvertSlipNo As Integer = SL_SYKHClass.CheckConvertedSlip
          If intConvertSlipNo > 0 Then
            SLCmnFunction.ShowToolTip("売上伝票へ" & SLConstants.NotifyMessage.CANNOT_DELETE_BY_CONVERTED & vbNewLine & "売上伝票番号:" & intConvertSlipNo _
                                      , SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
            Exit Sub
          End If

          Dim result As DialogResult = DisplayBox.ShowWarning(SLConstants.ConfirmMessage.DELETE_CONFIRM_MESSAGE)
          If result = Windows.Forms.DialogResult.Yes Then
            '在庫の削除
            Dim SL_ZHKClass As SL_ZHK = New SL_ZHK(connector)
            Dim intCoefficient As Integer = 1
            Dim session As ICustomSession
            session = connector.CreateTransactionalSession
            If SL_SYKHClass.sl_sykh_return = SLConstants.ShipmentKbn.RETURN_GOODS_CODE Then
              intCoefficient = 1
            Else
              intCoefficient = -1
            End If
            For intLpc As Integer = 0 To m_IntRowCount - 1
              SL_ZHKClass.sl_zhk_zdn_id = CInt(tblTable.GetCellValue(intLpc, ColumnName.PrevInvID))
              SL_ZHKClass.sl_zhk_datakbn = SLConstants.SL_ZHKSlipType.Shipping
              SL_ZHKClass.sl_zhk_dataid = SYKD_List(intLpc).sl_sykd_id

              SL_ZDNClass.sl_zdn_relay_zdnid = CInt(tblTable.GetCellValue(intLpc, ColumnName.PrevInvID))
              SL_ZDNClass.sl_zdn_nyukohonsu = CInt(tblTable.GetCellValue(intLpc, ColumnName.PrevNumber)) * intCoefficient
              SL_ZDNClass.sl_zdn_nyukosuryo = CDec(tblTable.GetCellValue(intLpc, ColumnName.PrevDecQty)) * intCoefficient
              If SL_ZDNClass.UpdateNumberQuantity(True, session) Then
                isSuccess = SL_ZHKClass.DeleteInventory(session, True, True)
              Else
                isSuccess = False
              End If
            Next
            If isSuccess Then
              isSuccess = SL_SYKHClass.Delete()
            End If

            If isSuccess Then
              session.Commit()
              session.Dispose()
              InitCreateMode()
            Else
              session.Rollback()
              session.Dispose()
            End If

          End If

        Case PcaCommandItemNew.CommandName

          InitCreateMode()

        Case PcaCommandItemReset.CommandName

          If HeaderLabel.State = LabelStateType.New Then

            InitCreateMode()
          ElseIf HeaderLabel.State = LabelStateType.Modify Then

            'Clear table, display current slip
            tblTable.ClearCellValues()
            m_IntRowCount = 0
            DisplaySlipByNo(SL_SYKHClass.sl_sykh_denno, False)

          End If
        Case PcaCommandItemDelRow.CommandName
          Dim result As Integer = MsgBox(SLConstants.ConfirmMessage.DELETE_ROW_CONFIRM_MESSAGE, MsgBoxStyle.YesNo, "確認")
          If result = DialogResult.Yes Then SLCmnFunction.DeleteRow(tblTable, m_IntRowIndex, m_IntRowCount, ColumnName.No)

        Case PcaCommandItemInsRow.CommandName
          SLCmnFunction.InsertRow(tblTable, m_IntRowIndex, m_IntRowCount, ColumnName.No, ColumnName.ProductCode, PRODUCT_COLUMN, PRODUCT_CODE_CELL)
          PcaFunctionCommandRefer.Enabled = True
          m_strSearchItem = ColumnName.ProductCode
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  '***********************************************************																														
  'Name          : PcaCommandManager1_UpdateCommandUI
  'Content       :  F12 = Close / Register
  'Return Value  : 
  'Argument      : slipNo, isClearDataFirst = False in case of Reset (Remain data in labels)
  'Created date  : 2015/11/23  by Lai Minh Hien (PCA Sample)
  'Modified date :   by       Content: 
  '***********************************************************	
  Private Sub PcaCommandManager1_UpdateCommandUI(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.UpdateCommandUI
    Dim commandItem As PcaCommandItem = e.CommandItem
    'Change F12 caption+function between 
    If commandItem Is PcaCommandItemSave Then
      commandItem.Enabled = Not m_IsF12CloseFlag

    ElseIf commandItem Is PcaCommandItemClose Then
      commandItem.Enabled = m_IsF12CloseFlag
      If m_IsF12CloseFlag Then
        ToolStripMenuItemSave.Enabled = False
        ToolStripButtonSave.Enabled = False
      Else
        ToolStripButtonSave.Enabled = True
      End If
      ToolStripButtonClose.Enabled = True
      ToolStripMenuItemClose.Enabled = True
    End If
    m_isFormClosing = False

    PcaCommandItemInv.Enabled = m_isUseLot And tblTable.Enabled
    PcaFunctionCommandInv.Enabled = PcaCommandItemInv.Enabled
  End Sub
#End Region

#Region "Display"

  Private Sub ToTokuisakiItems(ByVal CMS As CMS, ByVal TMS As TMS)
    Try
      If String.IsNullOrEmpty(CMS.cms_mei2) Then
        '名2が未設定時は名1を下段に
        Me.lblSupName.Text = Environment.NewLine & CMS.cms_mei1
      Else
        Me.lblSupName.Text = CMS.cms_mei1 & Environment.NewLine & CMS.cms_mei2
      End If

      If String.IsNullOrEmpty(CMS.cms_ad2) Then
        '住所2が未設定時は住所名1を下段に
        Me.lblAddr.Text = Environment.NewLine & CMS.cms_mail.PadRight(9) & CMS.cms_ad1
      Else
        Me.lblAddr.Text = CMS.cms_mail.PadRight(9) & CMS.cms_ad1 & Environment.NewLine & "　　　 　".PadRight(9) & CMS.cms_ad2
      End If

      Me.lblSupTel.Text = "TEL： " & CMS.cms_tel
      Me.lblSupFax.Text = "FAX： " & CMS.cms_fax

      If TMS.tms_tax.ToString.Length = 0 Then
        Me.lblSupTax.Text = "消費税： "
      Else
        Me.lblSupTax.Text = "消費税： " & TMS.NoticeTax(TMS)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Read Slip Data by SlipNo
  ''' </summary>
  ''' <param name="intSlipNo">SlipNo</param>
  ''' <param name="isClearDataFirst">data reset flag</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DisplaySlipByNo(ByVal intSlipNo As Integer, Optional isClearDataFirst As Boolean = True) As Boolean
    Try

      If isClearDataFirst Then ClearData()
      If SL_SYKHClass.ReadByID(intSlipNo) Then

        HeaderLabel.State = LabelStateType.Modify
        PcaFunctionCommandNext.Enabled = True
        ToolStripButtonDelete.Enabled = True
        PcaFunctionCommandCopy.Enabled = True
        tblTable.Enabled = True

        DisplayHeaderToScreen()

        'Get details data to class
        SL_SYKDClass.sl_sykd_hid = SL_SYKHClass.sl_sykh_id
        SYKD_List = SL_SYKDClass.ReadByHeaderID()
        'Display to screen
        m_IntRowCount = DisplayDetailsToScreen(SYKD_List)
        InitEditMode()

        Return True
      End If
      Return False 'Slip number is not existed
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Public Function DisplayDetailsToScreen(myTableDetails As SL_SYKD()) As Integer
    'Set from class to table
    Dim intCount As Integer = 0

    While intCount < myTableDetails.Length AndAlso Not myTableDetails(intCount) Is Nothing AndAlso Not myTableDetails(intCount).sl_sykd_scd Is Nothing
      Dim SMSClass As New SMS(connector)
      SMSClass.ReadByID(myTableDetails(intCount).sl_sykd_scd)

      tblTable.SetCellValue(intCount, ColumnName.No, myTableDetails(intCount).sl_sykd_eda)
      tblTable.SetCellValue(intCount, ColumnName.ProductCode, myTableDetails(intCount).sl_sykd_scd)

      tblTable.SetCellValue(intCount, ColumnName.Color, myTableDetails(intCount).sl_sykd_color)
      tblTable.SetCellValue(intCount, ColumnName.Size, myTableDetails(intCount).sl_sykd_size)
      tblTable.SetCellValue(intCount, ColumnName.Warehouse, myTableDetails(intCount).sl_sykd_souko)
      tblTable.SetCellValue(intCount, ColumnName.ProductName, myTableDetails(intCount).sl_sykd_mei)
      tblTable.SetCellValue(intCount, ColumnName.Spec, myTableDetails(intCount).sl_sykd_kikaku)

      Select Case myTableDetails(intCount).sl_sykd_tax
        Case 0
          tblTable.SetCellValue(intCount, ColumnName.Rate, String.Empty)
          tblTable.SetCellValue(intCount, ColumnName.TaxIncluded, SLConstants.TAX.NO_TAX)
        Case Else
          tblTable.SetCellValue(intCount, ColumnName.Rate, myTableDetails(intCount).sl_sykd_rate & "%")
          Select Case myTableDetails(intCount).sl_sykd_komi
            Case SLConstants.TAX.TAX_INCLUDED_CODE
              tblTable.SetCellValue(intCount, ColumnName.TaxIncluded, SLConstants.TAX.TAX_INCLUDED)
            Case Else
              tblTable.SetCellValue(intCount, ColumnName.TaxIncluded, SLConstants.TAX.TAX_EXCLUDED)
          End Select
      End Select

      tblTable.SetCellValue(intCount, ColumnName.QtyPerCase, myTableDetails(intCount).sl_sykd_iri)
      tblTable.SetCellValue(intCount, ColumnName.NoOfCase, myTableDetails(intCount).sl_sykd_hako)
      tblTable.SetCellValue(intCount, ColumnName.Qty, Format(Math.Round(CDec(myTableDetails(intCount).sl_sykd_suryo), SMSClass.sms_sketa), SLCmnFunction.FormatComma(SMSClass.sms_sketa)))
      tblTable.SetCellValue(intCount, ColumnName.UnitName, myTableDetails(intCount).sl_sykd_tani)
      tblTable.SetCellValue(intCount, ColumnName.Number, myTableDetails(intCount).sl_sykd_honsu)
      tblTable.SetCellValue(intCount, ColumnName.UnitPrice, Format(Math.Round(CDec(myTableDetails(intCount).sl_sykd_tanka), SMSClass.sms_tketa), SLCmnFunction.FormatComma(SMSClass.sms_tketa)))
      tblTable.SetCellValue(intCount, ColumnName.Amount, Format(CDec(CInt(myTableDetails(intCount).sl_sykd_kingaku)), "#,##0"))

      tblTable.SetCellValue(intCount, ColumnName.UnitPriceG, Format(Math.Round(CDec(myTableDetails(intCount).sl_sykd_gentan), SMSClass.sms_tketa), SLCmnFunction.FormatComma(SMSClass.sms_tketa)))
      tblTable.SetCellValue(intCount, ColumnName.AmountG, Format(CDec(CInt(myTableDetails(intCount).sl_sykd_genka)), "#,##0"))

      tblTable.SetCellValue(intCount, ColumnName.Arari, Format(CInt(myTableDetails(intCount).sl_sykd_arari), "#,##0"))
      tblTable.SetCellValue(intCount, ColumnName.UserLotNo, myTableDetails(intCount).sl_zdn_ulotno)
      tblTable.SetCellValue(intCount, ColumnName.Note, myTableDetails(intCount).sl_sykd_biko)
      tblTable.SetCellValue(intCount, ColumnName.InvID, myTableDetails(intCount).sl_zdn_id)
      tblTable.SetCellValue(intCount, ColumnName.PrevInvID, myTableDetails(intCount).sl_zdn_id)
      tblTable.SetCellValue(intCount, ColumnName.PrevNumber, myTableDetails(intCount).sl_sykd_honsu)
      tblTable.SetCellValue(intCount, ColumnName.PrevDecQty, Format(Math.Round(CDec(myTableDetails(intCount).sl_sykd_suryo), SMSClass.sms_sketa), SLCmnFunction.FormatComma(SMSClass.sms_sketa)))
      tblTable.SetCellValue(intCount, ColumnName.PrevID, myTableDetails(intCount).sl_sykd_id)
      tblTable.SetCellValue(intCount, ColumnName.PrevInvID, myTableDetails(intCount).sl_zdn_id)
      tblTable.SetCellValue(intCount, ColumnName.ConvertFlag, myTableDetails(intCount).sl_sykd_convert)

      SetPropertiesByProduct(intCount, myTableDetails(intCount).sl_sykd_scd)
      If m_SlipType = SlipType.Order Or SL_SYKHClass.sl_sykh_datakbn = SLConstants.SL_SYKH_RelaySlipType.OrderReceiving Then
        tblTable.SetCellValue(intCount, ColumnName.Type, myTableDetails(intCount).sl_sykd_skbn)
      End If
      intCount = intCount + 1
    End While
    Return intCount
  End Function

  Public Function DisplayHeaderToScreen() As Boolean
    Dim provider As CultureInfo = CultureInfo.CurrentCulture
    Try
      'Set Header from Class
      With SL_SYKHClass
        Dim SYKHClass As SYKH = New SYKH(connector)
        Dim JUCHClass As JUCH = New JUCH(connector)

        setType.CodeText = .sl_sykh_return.ToString
        ldtShipmentDate.Date = Date.ParseExact(.sl_sykh_uribi.ToString, "yyyyMMdd", provider)
        ldtDlvDate.Date = Date.ParseExact(.sl_sykh_nouki.ToString, "yyyyMMdd", provider)

        Select Case .sl_sykh_datakbn
          Case SLConstants.SL_SYKH_RelaySlipType.OrderReceiving
            setOrderNo.CodeText = JUCHClass.GetSlipNo(.sl_sykh_dataid).ToString
          Case SLConstants.SL_SYKH_RelaySlipType.Sales
            setPurNo.CodeText = SYKHClass.GetSlipNo(.sl_sykh_dataid).ToString
        End Select

        setSupCode.CodeText = .sl_sykh_tcd
        CodeTextValidating(, False) 'Previous slip -> previous slip (Same supCode)

        ltxtWhNo2.Text = .sl_sykh_denno2
        setPerson.CodeText = .sl_sykh_jtan
        CodeTextPersonValidating(, False)

        setDepartment.CodeText = .sl_sykh_jbmn
        CodeTextDepartmentValidating(, False)

        setMemoCode.CodeText = .sl_sykh_tekcd
        CodeTextMemoCodeValidating(, False)

        setMemoContent.CodeText = .sl_sykh_tekmei

        setProject.CodeText = .sl_sykh_pjcode
        CodeTextProjectValidating(, False)

        ltxtCreatedBy.Text = .sl_sykh_insuser
        ltxtCreatedAt.Text = CStr(.sl_sykh_insdate)
        ltxtModifiedBy.Text = .sl_sykh_upduser
        If CStr(.sl_sykh_upddate) <> SLConstants.NOTHING_DATE Then ltxtModifiedAt.Text = CStr(.sl_sykh_upddate)

        ltxtWhNo.Text = .sl_sykh_denno.ToString()
      End With
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  ''' <summary>
  ''' Display warehousing slip to screen (By Order slip no)
  ''' </summary>
  ''' <param name="intOrderNo">slip no</param>
  ''' <param name="isClearDataFirst">False in case of Reset (Remain data in labels)</param>
  ''' <returns></returns>
  ''' <remarks>(PCA Sample)</remarks>
  Private Function DisplaySlipByOrderNo(ByVal intOrderNo As Integer, Optional ByVal isClearDataFirst As Boolean = True) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim provider As CultureInfo = CultureInfo.CurrentCulture
      Dim SMSClass As SMS = New SMS(connector)

      If isClearDataFirst Then ClearData()

      m_SlipType = SlipType.Order

      'Get order slip data
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_JUCH")
      selectCommand.Parameters("@juch_jno").SetValue(intOrderNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        'Get header data
        HeaderLabel.State = LabelStateType.New
        PcaFunctionCommandNext.Enabled = False
        ToolStripButtonDelete.Enabled = False
        PcaFunctionCommandCopy.Enabled = False

        tblTable.Enabled = True
        setType.CodeText = SLConstants.ShipmentKbn.NORMAL_SHIPPING_CODE.ToString
        If IsDate(Format(CInt(reader.GetValue("juch_noki")), "0000/00/00")) Then
          ldtDlvDate.IntDate = CInt(reader.GetValue("juch_noki"))
        Else
          ldtDlvDate.Date = Now
        End If

        setSupCode.CodeText = reader.GetValue("juch_tcd").ToString
        CodeTextValidating(, False)
        setOrderNo.CodeText = CStr(reader.GetValue("juch_jno"))
        setPurNo.CodeText = ""
        ltxtWhNo2.Text = ""
        If reader.GetValue("juch_jtan").ToString.Trim <> String.Empty AndAlso CInt(reader.GetValue("juch_jtan").ToString) > 0 Then
          setPerson.CodeText = reader.GetValue("juch_jtan").ToString
          CodeTextPersonValidating(, False)
        End If
        If reader.GetValue("juch_jbmn").ToString.Trim <> String.Empty AndAlso CInt(reader.GetValue("juch_jbmn").ToString) > 0 Then
          setDepartment.CodeText = reader.GetValue("juch_jbmn").ToString
          CodeTextDepartmentValidating(, False)
        End If
        If reader.GetValue("juch_tekcd").ToString.Trim <> String.Empty AndAlso CInt(reader.GetValue("juch_tekcd").ToString) > 0 Then
          setMemoCode.CodeText = reader.GetValue("juch_tekcd").ToString
          CodeTextMemoCodeValidating(, False)
        End If
        setMemoContent.CodeText = reader.GetValue("juch_tekmei").ToString

        If reader.GetValue("juch_pjcode").ToString.Trim <> String.Empty Then
          setProject.CodeText = reader.GetValue("juch_pjcode").ToString
          CodeTextProjectValidating(, False)
        End If
        m_intOrderSlipID = CInt(reader.GetValue("juch_id"))

        'Get details data
        selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_JUCD")
        selectCommand.Parameters("@jucd_hid").SetValue(reader.GetValue("juch_id"))
        reader = selectCommand.ExecuteReader
        Dim intBranchCount As Integer = 0
        While reader.Read()
          SMSClass.ReadByID(reader.GetValue("jucd_scd").ToString)
          tblTable.SetCellValue(intBranchCount, ColumnName.No, intBranchCount + 1)
          tblTable.SetCellValue(intBranchCount, ColumnName.Type, 0)
          tblTable.SetCellValue(intBranchCount, ColumnName.ProductCode, reader.GetValue("jucd_scd"))

          tblTable.SetCellValue(intBranchCount, ColumnName.Color, reader.GetValue("jucd_color"))
          tblTable.SetCellValue(intBranchCount, ColumnName.Size, reader.GetValue("jucd_size"))
          tblTable.SetCellValue(intBranchCount, ColumnName.Warehouse, reader.GetValue("jucd_souko"))
          tblTable.SetCellValue(intBranchCount, ColumnName.ProductName, reader.GetValue("jucd_mei"))
          tblTable.SetCellValue(intBranchCount, ColumnName.Spec, reader.GetValue("jucd_kikaku"))

          Select Case CInt(reader.GetValue("jucd_tax"))
            Case 0
              tblTable.SetCellValue(intBranchCount, ColumnName.Rate, String.Empty)
              tblTable.SetCellValue(intBranchCount, ColumnName.TaxIncluded, SLConstants.TAX.NO_TAX)
            Case Else
              tblTable.SetCellValue(intBranchCount, ColumnName.Rate, CInt(reader.GetValue("jucd_rate").ToString) & "%")
              Select Case reader.GetValue("jucd_komi").ToString
                Case SLConstants.TAX.TAX_INCLUDED_CODE.ToString
                  tblTable.SetCellValue(intBranchCount, ColumnName.TaxIncluded, SLConstants.TAX.TAX_INCLUDED)
                Case Else
                  tblTable.SetCellValue(intBranchCount, ColumnName.TaxIncluded, SLConstants.TAX.TAX_EXCLUDED)
              End Select
          End Select

          tblTable.SetCellValue(intBranchCount, ColumnName.QtyPerCase, CInt(reader.GetValue("jucd_iri")))
          tblTable.SetCellValue(intBranchCount, ColumnName.NoOfCase, CInt(reader.GetValue("jucd_hako")))
          tblTable.SetCellValue(intBranchCount, ColumnName.Number, 1)
          tblTable.SetCellValue(intBranchCount, ColumnName.Qty, Format(reader.GetValue("jucd_suryo"), "#,##0." & New String("0"c, SMSClass.sms_sketa)))
          tblTable.SetCellValue(intBranchCount, ColumnName.UnitName, reader.GetValue("jucd_tani"))
          tblTable.SetCellValue(intBranchCount, ColumnName.UnitPrice, Format(CInt(reader.GetValue("jucd_tanka")), "#,##0." & New String("0"c, SMSClass.sms_tketa)))
          tblTable.SetCellValue(intBranchCount, ColumnName.Amount, Format(CInt(reader.GetValue("jucd_kingaku")), "#,##0"))

          tblTable.SetCellValue(intBranchCount, ColumnName.UnitPriceG, Format(CInt(reader.GetValue("jucd_gentan")), "#,##0." & New String("0"c, SMSClass.sms_tketa)))
          tblTable.SetCellValue(intBranchCount, ColumnName.AmountG, Format(CInt(reader.GetValue("jucd_genka")), "#,##0"))

          tblTable.SetCellValue(intBranchCount, ColumnName.Arari, Format(CInt(reader.GetValue("jucd_arari")), "#,##0"))

          tblTable.SetCellValue(intBranchCount, ColumnName.Note, reader.GetValue("jucd_biko"))

          SetPropertiesByProduct(intBranchCount, reader.GetValue("jucd_scd").ToString)
          intBranchCount = intBranchCount + 1
        End While
        If intBranchCount > 0 Then m_IntRowCount = intBranchCount - 1
        Return True
      End If
      reader.Close()
      reader.Dispose()
      Return False 'Slip number is not existed
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  ''' Display Slip
  ''' </summary>
  ''' <param name="intPurNo">Sales Slip No/ID</param>
  ''' <param name="isClearDataFirst"></param>
  ''' <returns></returns>
  ''' <remarks>Display warehousing slip to screen (By Purchase slip no)</remarks>
  Private Function DisplaySlipBySalesNo(ByVal intPurNo As Integer, Optional ByVal isClearDataFirst As Boolean = True) As Boolean
    Try
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim provider As CultureInfo = CultureInfo.CurrentCulture
      Dim SMSClass As SMS = New SMS(connector)

      If isClearDataFirst Then ClearData()

      m_SlipType = SlipType.Sup

      'Get order slip data
      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SYKH")
      selectCommand.Parameters("@sykh_denno").SetValue(intPurNo)
      reader = selectCommand.ExecuteReader
      If reader.Read() Then
        'Get header data
        HeaderLabel.State = LabelStateType.New
        PcaFunctionCommandNext.Enabled = False
        ToolStripButtonDelete.Enabled = False
        PcaFunctionCommandCopy.Enabled = False

        tblTable.Enabled = True
        setType.CodeText = SLConstants.ShipmentKbn.NORMAL_SHIPPING_CODE.ToString

        If IsDate(Format(CInt(reader.GetValue("sykh_uribi")), "0000/00/00")) Then
          ldtShipmentDate.IntDate = CInt(reader.GetValue("sykh_uribi"))
        Else
          ldtShipmentDate.Date = Now
        End If
        setSupCode.CodeText = reader.GetValue("sykh_tcd").ToString
        CodeTextValidating(, False)
        setOrderNo.CodeText = ""
        setPurNo.CodeText = reader.GetValue("sykh_denno").ToString
        ltxtWhNo2.Text = ""

        If reader.GetValue("sykh_jtan").ToString.Trim <> String.Empty AndAlso CInt(reader.GetValue("sykh_jtan").ToString) > 0 Then
          setPerson.CodeText = reader.GetValue("sykh_jtan").ToString
          CodeTextPersonValidating(, False)
        End If

        If reader.GetValue("sykh_jbmn").ToString.Trim <> String.Empty AndAlso CInt(reader.GetValue("sykh_jbmn").ToString) > 0 Then
          setDepartment.CodeText = reader.GetValue("sykh_jbmn").ToString
          CodeTextDepartmentValidating(, False)
        End If

        If reader.GetValue("sykh_tekcd").ToString.Trim <> String.Empty AndAlso CInt(reader.GetValue("sykh_tekcd").ToString) > 0 Then
          setMemoCode.CodeText = reader.GetValue("sykh_tekcd").ToString
          CodeTextMemoCodeValidating(, False)
        End If
        setMemoContent.CodeText = reader.GetValue("sykh_tekmei").ToString

        If reader.GetValue("sykh_pjcode").ToString.Trim <> String.Empty Then
          setProject.CodeText = reader.GetValue("sykh_pjcode").ToString
          CodeTextProjectValidating(, False)
        End If

        m_intPurSlipID = CInt(reader.GetValue("sykh_id"))
        'Get details data
        selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SYKD")
        selectCommand.Parameters("@sykd_hid").SetValue(reader.GetValue("sykh_id"))
        reader = selectCommand.ExecuteReader
        Dim intBranchCount As Integer = 0
        While reader.Read()
          SMSClass.ReadByID(reader.GetValue("sykd_scd").ToString)
          tblTable.SetCellValue(intBranchCount, ColumnName.No, intBranchCount + 1)
          tblTable.SetCellValue(intBranchCount, ColumnName.ProductCode, reader.GetValue("sykd_scd"))

          tblTable.SetCellValue(intBranchCount, ColumnName.Color, reader.GetValue("sykd_color"))
          tblTable.SetCellValue(intBranchCount, ColumnName.Size, reader.GetValue("sykd_size"))
          tblTable.SetCellValue(intBranchCount, ColumnName.Warehouse, reader.GetValue("sykd_souko"))
          tblTable.SetCellValue(intBranchCount, ColumnName.ProductName, reader.GetValue("sykd_mei"))
          tblTable.SetCellValue(intBranchCount, ColumnName.Spec, reader.GetValue("sykd_kikaku"))

          Select Case CInt(reader.GetValue("sykd_tax"))
            Case 0
              tblTable.SetCellValue(intBranchCount, ColumnName.Rate, String.Empty)
              tblTable.SetCellValue(intBranchCount, ColumnName.TaxIncluded, SLConstants.TAX.NO_TAX)
            Case Else
              tblTable.SetCellValue(intBranchCount, ColumnName.Rate, CInt(reader.GetValue("sykd_rate").ToString) & "%")
              Select Case reader.GetValue("sykd_komi").ToString
                Case SLConstants.TAX.TAX_INCLUDED_CODE.ToString
                  tblTable.SetCellValue(intBranchCount, ColumnName.TaxIncluded, SLConstants.TAX.TAX_INCLUDED)
                Case Else
                  tblTable.SetCellValue(intBranchCount, ColumnName.TaxIncluded, SLConstants.TAX.TAX_EXCLUDED)
              End Select
          End Select

          tblTable.SetCellValue(intBranchCount, ColumnName.QtyPerCase, CInt(reader.GetValue("sykd_iri")))
          tblTable.SetCellValue(intBranchCount, ColumnName.NoOfCase, CInt(reader.GetValue("sykd_hako")))
          tblTable.SetCellValue(intBranchCount, ColumnName.Number, 1)
          tblTable.SetCellValue(intBranchCount, ColumnName.Qty, Format(reader.GetValue("sykd_suryo"), "#,##0." & New String("0"c, SMSClass.sms_sketa)))
          tblTable.SetCellValue(intBranchCount, ColumnName.UnitName, reader.GetValue("sykd_tani"))
          tblTable.SetCellValue(intBranchCount, ColumnName.UnitPrice, Format(CInt(reader.GetValue("sykd_tanka")), "#,##0." & New String("0"c, SMSClass.sms_tketa)))
          tblTable.SetCellValue(intBranchCount, ColumnName.Amount, Format(CInt(reader.GetValue("sykd_kingaku")) - CInt(reader.GetValue("sykd_uchi")), "#,##0"))
          tblTable.SetCellValue(intBranchCount, ColumnName.UnitPriceG, Format(CInt(reader.GetValue("sykd_gentan")), "#,##0." & New String("0"c, SMSClass.sms_tketa)))
          tblTable.SetCellValue(intBranchCount, ColumnName.AmountG, Format(CInt(reader.GetValue("sykd_genka")) - CInt(reader.GetValue("sykd_uchi")), "#,##0"))
          tblTable.SetCellValue(intBranchCount, ColumnName.Note, reader.GetValue("sykd_biko"))

          SetPropertiesByProduct(intBranchCount, reader.GetValue("sykd_scd").ToString)

          intBranchCount = intBranchCount + 1
        End While
        If intBranchCount > 0 Then m_IntRowCount = intBranchCount - 1
        Return True
      End If
      reader.Close()
      reader.Dispose()
      Return False 'Slip number is not existed
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function
#End Region

#Region "Validate"
  '★please standardize of validate events

  ''' <summary>
  ''' After user input slip number, if there's corresponding data, display it, move to edit mode. If not, delete slip number.
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub ltxtWhNo_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles ltxtWhNo.Validating
    Try
      Dim intValue As Integer = 0
      If ltxtWhNo.Text = "" Then Return

      If Integer.TryParse(ltxtWhNo.Text, intValue) AndAlso Not DisplaySlipByNo(CInt(ltxtWhNo.Text)) Then
        e.Cancel = True
        ltxtWhNo.Text = ""
      Else
        InitEditMode()
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Order No Validating
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub setOrderNo_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setOrderNo.Validating
    If setOrderNo.CodeText = "" Then
      Return
    End If
    If Not DisplaySlipByOrderNo(CInt(setOrderNo.CodeText)) Then
      e.Cancel = True
      setOrderNo.CodeText = ""
    Else
      setOrderNo.Enabled = True
      setSupCode.Enabled = False
      setPurNo.Enabled = False
      m_SlipType = SlipType.Order
      Dim cell As PcaCell = tblTable.BodyColumns.GetCell(ColumnName.Type)
      SLCmnFunction.SetPcaCellCombo(cell, SLConstants.ComboType.Shipping)
      tblTable.BodyColumns(HEADER_TYPE).AltStyle = New PcaColumnStyle(Color.Black, Color.White)
      tblTable.BodyColumns(HEADER_TYPE).Style = New PcaColumnStyle(Color.Black, Color.White)
    End If

  End Sub



  ''' <summary>
  ''' Purchase No Validating
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub setPurNo_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setPurNo.Validating
    If setPurNo.CodeText = "" Then
      Return
    End If
    If Not DisplaySlipBySalesNo(CInt(setPurNo.CodeText)) Then
      e.Cancel = True
      setPurNo.CodeText = ""
    Else
      setSupCode.Enabled = False
      setOrderNo.Enabled = False
      setPurNo.Enabled = True
      m_SlipType = SlipType.Sup
      Dim cell As PcaCell = tblTable.BodyColumns.GetCell(ColumnName.Type)
      cell.SelectItems = {""}
      tblTable.BodyColumns(HEADER_TYPE).AltStyle = New PcaColumnStyle(Color.Black, Color.LightGray)
      tblTable.BodyColumns(HEADER_TYPE).Style = New PcaColumnStyle(Color.Black, Color.LightGray)
      cell.Enabled = False
    End If
  End Sub

  Private Sub PcaCodeSet_CodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setMemoCode.CodeTextValidating2, setProject.CodeTextValidating2, _
                                                                                                                     setSupCode.CodeTextValidating2, setPerson.CodeTextValidating2, _
                                                                                                                     setDepartment.CodeTextValidating2
    Try
      Dim objPcaCodeSet As PcaCodeSet = DirectCast(sender, PcaCodeSet)
      Dim isRet As Boolean
      If m_isFormClosing Then Return

      Select Case objPcaCodeSet.Name
        Case setMemoCode.Name
          CodeTextMemoCodeValidating(e)
        Case setProject.Name
          isRet = CodeTextProjectValidating(e)
          If tblTable.Enabled And isRet Then tblTable.Focus() : tblTable.SetCellFocus(0, ColumnName.ProductCode)
        Case setSupCode.Name
          CodeTextValidating(e)
        Case setPerson.Name
          CodeTextPersonValidating(e)
        Case setDepartment.Name
          CodeTextDepartmentValidating(e)
      End Select

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try


  End Sub

  ''' <summary>
  ''' Validate Warehouse
  ''' </summary>
  ''' <param name="rowIndex"></param>
  ''' <param name="newValue"></param>
  ''' <param name="needMessageBox"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ValidateSoko(ByVal rowIndex As Integer, ByVal newValue As String, ByVal needMessageBox As Boolean) As Boolean
    Try
      Dim headerLabelText As String = String.Empty
      Dim EMS As New EMS(connector)
      Dim strWHName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.Warehouse, newValue)

      If strWHName.Trim.Length = 0 Then
        Return False
      End If
      Me.HeaderLabel.Text = headerLabelText

      If String.IsNullOrEmpty(newValue) Then
        Me.tblTable.SetCellValue(rowIndex, MeisaiTableDefines.SokoCode, String.Empty)
      Else
        Me.tblTable.SetCellValue(rowIndex, MeisaiTableDefines.SokoCode, newValue)
      End If

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function



  ''' <summary>
  ''' Validating cell
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks>Modified: 2015/11/30 by Hien Content: Check allocated quantity </remarks>
  Private Sub tblTable_ValidatingCell(sender As System.Object, args As PCA.Controls.CancelCellEventArgs) Handles tblTable.ValidatingCell

    Try
      Dim myTable As TSC.Kon.Tools.TscMeisaiTable = DirectCast(sender, TSC.Kon.Tools.TscMeisaiTable)
      Dim decQty As Decimal = 0
      Dim decUnitPrice As Decimal = 0
      Dim intAmount As Integer = 0
      Dim intAmountG As Integer = 0

      Select Case args.CellName
        Case ColumnName.ProductCode
          If (args.NewValue Is Nothing OrElse args.NewValue.ToString = "") Then
            If Not (args.CurValue Is Nothing OrElse args.CurValue.ToString = String.Empty) Then args.Cancel = True
            Return
          End If

          If args.CurValue Is Nothing Then 'If input a new row
            If Not ValidateSyohin(m_IntRowIndex, args.NewValue.ToString, False) Then
              'product not found: return old value, keep focusing
              tblTable.SetCellValue(m_IntRowIndex, ColumnName.ProductCode, args.CurValue)
              tblTable.SetCellFocus(m_IntRowIndex, ColumnName.ProductCode)
            Else
              SetPropertiesByProduct(m_IntRowIndex, args.NewValue.ToString)
            End If

          ElseIf Not args.NewValue.ToString = args.CurValue.ToString Then 'input a different value
            If m_strCurCode.Trim = args.NewValue.ToString Then Exit Sub
            If Not ValidateSyohin(m_IntRowIndex, args.NewValue.ToString, False) Then
              'product not found: return old value, keep focusing
              tblTable.SetCellValue(m_IntRowIndex, ColumnName.ProductCode, args.CurValue)
              tblTable.SetCellFocus(m_IntRowIndex, ColumnName.ProductCode)
            Else
              SetPropertiesByProduct(m_IntRowIndex, args.NewValue.ToString)
            End If
          End If

        Case ColumnName.QtyPerCase, ColumnName.NoOfCase 'If both are set, quantity = Number of cases * quantity per case

          tblTable.SetCellValue(m_IntRowIndex, args.CellName, args.NewValue)

          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.QtyPerCase)) Then Exit Sub
          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.NoOfCase)) Then Exit Sub

          If CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.QtyPerCase)) <> 0 AndAlso CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.NoOfCase)) <> 0 Then

            decUnitPrice = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.QtyPerCase)) * CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.NoOfCase))
            tblTable.SetCellValue(m_IntRowIndex, ColumnName.Qty, decUnitPrice)

          End If

        Case ColumnName.Qty
          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPrice)) Then Exit Sub
          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Qty)) Then Exit Sub

          decUnitPrice = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPrice))
          decQty = CDec(args.NewValue)
          tblTable.SetCellValue(m_IntRowIndex, ColumnName.Amount, Format(decUnitPrice * decQty, "#,##0"))
          decUnitPrice = CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPriceG))
          tblTable.SetCellValue(m_IntRowIndex, ColumnName.AmountG, Format(decUnitPrice * decQty, "#,##0"))

        Case ColumnName.UnitPrice

          tblTable.SetCellValue(m_IntRowIndex, args.CellName, args.NewValue)

          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPrice)) Then Exit Sub
          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Qty)) Then Exit Sub

          intAmount = CInt(CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPrice)) * CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Qty)))
          tblTable.SetCellValue(m_IntRowIndex, ColumnName.Amount, Format(intAmount, "#,##0"))

          intAmountG = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.AmountG))
          tblTable.SetCellValue(m_IntRowIndex, ColumnName.Arari, Format(intAmount - intAmountG, "#,##0"))

        Case ColumnName.UnitPriceG

          tblTable.SetCellValue(m_IntRowIndex, args.CellName, args.NewValue)

          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPriceG)) Then Exit Sub
          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Qty)) Then Exit Sub

          intAmountG = CInt(CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.UnitPriceG)) * CDec(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Qty)))
          tblTable.SetCellValue(m_IntRowIndex, ColumnName.AmountG, Format(intAmountG, "#,##0"))

          intAmount = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Amount))
          tblTable.SetCellValue(m_IntRowIndex, ColumnName.Arari, Format(intAmount - intAmountG, "#,##0"))


        Case ColumnName.Warehouse
          Dim EMSClass As New EMS(connector)
          Dim strStandardCode = SLCmnFunction.standardlizeCode(args.NewValue.ToString, AMS1Class.ams1_WarehouseLength)

          If strStandardCode.Trim.Length > 0 AndAlso EMSClass.ReadKbnMasterByIDAndKbn(SLConstants.EMS.Warehouse, strStandardCode).ToString.Trim = String.Empty Then
            SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.IMPROPRIETY_INPUTDATA, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, 8)
            tblTable.SetCellValue(m_IntRowIndex, args.CellName, args.CurValue)
          Else
            tblTable.SetCellValue(m_IntRowIndex, args.CellName, strStandardCode)
          End If

        Case ColumnName.Amount, ColumnName.AmountG
          intAmount = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.Amount))
          intAmountG = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.AmountG))
          tblTable.SetCellValue(m_IntRowIndex, ColumnName.Arari, Format(intAmount - intAmountG, "#,##0"))

      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub CodeTextPersonValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True)
    Try
      HeaderLabel.Text = String.Empty
      Dim EMST = New EMST(connector)
      Dim EMSB = New EMSB(connector)
      Dim strPersonCode As String = SLCmnFunction.standardlizeCode(setPerson.CodeText.TrimEnd(), AMS1Class.ams1_PersonLength)
      EMST.ReadByCode(strPersonCode)

      If NotCheckIfNoChange Then
        If m_strCurCode = strPersonCode Then
          'no changes
          If Not String.IsNullOrEmpty(strPersonCode) AndAlso String.IsNullOrEmpty(Me.setPerson.NameText) Then
            Me.setPerson.NameText = EMST.emst_str
          End If
          Return
        End If
      End If

      If EMST.ReadByCode(strPersonCode) Then
        If EMSB.ReadByCode(EMST.emst_bmn) Then
          ValidatedTantosya(EMST, EMSB)
        Else
          ValidatedTantosya(EMST)
        End If

      Else
        If strPersonCode.Trim.Length > 0 Then DisplayBox.ShowNotify("担当者" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        If Me.IsValidate Then
          e.Cancel = True
        Else
          Me.setPerson.CodeText = m_strCurCode
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  Private Sub CodeTextDepartmentValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True)
    Try
      HeaderLabel.Text = String.Empty
      Dim EMSB = New EMSB(connector)
      Dim strBumoncode As String = SLCmnFunction.standardlizeCode(setDepartment.CodeText.TrimEnd(), AMS1Class.ams1_DepartmentLength)
      EMSB.ReadByCode(strBumoncode)

      If NotCheckIfNoChange Then
        If m_strCurCode = strBumoncode Then
          'No changes
          If Not String.IsNullOrEmpty(strBumoncode) AndAlso String.IsNullOrEmpty(Me.setDepartment.NameText) Then
            Me.setDepartment.NameText = EMSB.emsb_str
          End If
          Return
        End If
      End If

      If EMSB.ReadByCode(strBumoncode) Then
        ValidatedBumon(EMSB)
      Else
        If strBumoncode.Trim.Length > 0 Then DisplayBox.ShowNotify("部門" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        If Me.IsValidate Then
          e.Cancel = True
        Else
          Me.setDepartment.CodeText = m_strCurCode
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  Private Sub CodeTextMemoCodeValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True)
    Try
      HeaderLabel.Text = String.Empty
      Dim EMS As New EMS(connector)
      Dim strMemoCode As String = SLCmnFunction.standardlizeCode(setMemoCode.CodeText.TrimEnd(), AMS1Class.ams1_MemoLength)
      Dim strName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, strMemoCode)
      If NotCheckIfNoChange Then
        If m_strCurCode = strMemoCode Then
          'No changes
          Return
        End If
      End If

      If String.IsNullOrEmpty(strMemoCode) Then
        Me.setMemoContent.CodeText = String.Empty
        Return
      End If
      If EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, strMemoCode).Length = 0 Then
        If strMemoCode.Trim.Length > 0 Then DisplayBox.ShowNotify("摘要" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        If Me.IsValidate Then
          e.Cancel = True
        End If
      Else
        ValidatedTekiyo(strMemoCode, strName)
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  Private Function CodeTextProjectValidating(Optional ByVal e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True) As Boolean
    Try
      HeaderLabel.Text = String.Empty
      Dim PJMS As New PJMS(connector)
      Dim strPJCode As String = Me.setProject.CodeText.TrimEnd()
      PJMS.ReadByCode(strPJCode)
      If NotCheckIfNoChange Then
        If m_strCurCode = strPJCode Then
          'No changes
          If Not String.IsNullOrEmpty(strPJCode) AndAlso String.IsNullOrEmpty(Me.setProject.NameText) Then
            Me.setProject.NameText = PJMS.pjms_name
          End If
          Return True
        End If
      End If
      If String.IsNullOrEmpty(strPJCode) Then
        Me.setProject.CodeText = String.Empty
        Return True
      End If
      If PJMS.ReadByCode(strPJCode) Then
        ValidatedProject(PJMS)
      Else
        If strPJCode.Trim.Length > 0 Then DisplayBox.ShowNotify("案件" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
        If Me.IsValidate Then
          e.Cancel = True
        Else
          setProject.CodeText = strPJCode
        End If
        Return False
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
    Return True
  End Function

  Private Sub CodeTextValidating(Optional e As PCA.Controls.ValidatingEventArgs = Nothing, Optional ByVal NotCheckIfNoChange As Boolean = True)
    Try
      HeaderLabel.Text = String.Empty

      Dim TMS As New TMS(connector)
      Dim CMS As New CMS(connector)
      Dim strSupCode As String = SLCmnFunction.standardlizeCode(setSupCode.CodeText.TrimEnd(), AMS1Class.ams1_CustomerLength)
      If NotCheckIfNoChange Then
        If m_strCurCode = strSupCode Then
          setSupCode.CodeText = strSupCode
          Return
        End If
      End If

      m_IntRowCount = 0
      If IsValidate Then
        IsValidate = False
        If String.IsNullOrEmpty(strSupCode) Then
          e.Cancel = True
          setSupCode.CodeText = m_strCurCode
          Return
        End If
        'validate supplier code
        If Not TMS.ReadByCode(strSupCode) Then
          'If there's error
          e.Cancel = True
          If strSupCode.Trim.Length > 0 Then DisplayBox.ShowNotify("出荷先" & SLConstants.NotifyMessage.MASTER_NOT_FOUND)
          setSupCode.CodeText = m_strCurCode
          Return
        Else
          CMS.ReadByID(TMS.tms_cmsid)
          ToTokuisakiItems(CMS, TMS)
        End If
      Else
        'validate supplier code
        If Not TMS.ReadByCode(strSupCode) Then

          setSupCode.CodeText = m_strCurCode
          setSupCode.Focus()
        Else
          CMS.ReadByID(TMS.tms_cmsid)
          ToTokuisakiItems(CMS, TMS)
        End If
      End If

      If setSupCode.CodeText <> "" Then
        setOrderNo.Enabled = False
        setPurNo.Enabled = False
        tblTable.Enabled = True
        m_IsF12CloseFlag = False
        m_SlipType = SlipType.Self
        If SL_SYKHClass.sl_sykh_datakbn <> SLConstants.SL_SYKH_RelaySlipType.OrderReceiving Then
          Dim cell As PcaCell = tblTable.BodyColumns.GetCell(ColumnName.Type)
          cell.SelectItems = {""}
          tblTable.BodyColumns(HEADER_TYPE).AltStyle = New PcaColumnStyle(Color.Black, Color.LightGray)
          tblTable.BodyColumns(HEADER_TYPE).Style = New PcaColumnStyle(Color.Black, Color.LightGray)
          cell.Enabled = False
        End If

        setPerson.Focus()
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Validate input before register
  ''' </summary>
  ''' <returns>True/False: Is input valid or not</returns>
  ''' <remarks></remarks>
  Private Function ValidateInput() As Boolean
    Try
      Dim strErrMsg As String = String.Empty
      Dim PosForm As System.Drawing.Point
      Dim SL_ZDNClass As New SL_ZDN(connector)
      Dim strProductCode As String = String.Empty
      Dim strWarehouse As String = String.Empty
      Dim strLotNo As String = String.Empty
      Dim intNum As Integer = 0
      Dim decQty As Decimal = 0D
      Dim intAmountNum As Integer = 0
      Dim decAmountQty As Decimal = 0D

      Dim intForParse As Integer = 0
      Dim intNQErrRow As Integer = 0
      Dim intInvID As Integer = 0

      'check header data
      If String.IsNullOrEmpty(setSupCode.CodeText) Then
        SLCmnFunction.ShowToolTip("出荷先コード" & SLConstants.NotifyMessage.INPUT_REQUIRED, "", ToolTipIcon.Warning, setSupCode, ToolTip1, Me)
        Return False
      End If
      If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(0, ColumnName.ProductCode)) Then 'Check if there's any product in details table
        SLCmnFunction.ShowToolTip("明細" & SLConstants.NotifyMessage.INPUT_REQUIRED, "", ToolTipIcon.Warning, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
        Return False
      End If
      If ldtShipmentDate.Date > ldtDlvDate.Date Then
        SLCmnFunction.ShowToolTip("納期は出荷日以降を設定してください。", SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Warning _
                                  , PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
        ldtShipmentDate.Focus()
        Return False
      End If
      'check detail data
      For intCount = 0 To m_IntRowCount
        If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, ColumnName.ProductCode)) Then

          If tblTable.GetCellValue(intCount, ColumnName.Qty) Is Nothing Then tblTable.SetCellValue(intCount, ColumnName.Qty, 0)
          If tblTable.GetCellValue(intCount, ColumnName.Number) Is Nothing Then tblTable.SetCellValue(intCount, ColumnName.Number, 0)

          If tblTable.GetCellValue(intCount, ColumnName.Qty).ToString = String.Empty Then tblTable.SetCellValue(intCount, ColumnName.Qty, 0)
          If tblTable.GetCellValue(intCount, ColumnName.Number).ToString = String.Empty Then tblTable.SetCellValue(intCount, ColumnName.Number, 0)

          PosForm.Y = tblTable.Location.Y + tblTable.HeadColumns.Height * (intCount + 2)

          If SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, ColumnName.Warehouse)) Then
            PosForm.X = CInt(WAREHOUSE_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
            strErrMsg = "行目の倉庫"
          ElseIf SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, ColumnName.Type)) And m_SlipType = SlipType.Order Then
            PosForm.X = CInt(TYPE_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
            strErrMsg = "行目の区分"
          ElseIf SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, ColumnName.Qty)) Then
            PosForm.X = CInt(QUANTITY_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
            strErrMsg = "行目の数量"
          ElseIf SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, ColumnName.UserLotNo)) And CInt(tblTable.GetCellValue(intCount, ColumnName.IsUseLot)) = 1 Then
            PosForm.X = CInt(LOT_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
            strErrMsg = "行目のロットNo"
          End If

          If strErrMsg.Trim.Length > 0 Then
            SLCmnFunction.ShowToolTip((intCount + 1).ToString & strErrMsg & SLConstants.NotifyMessage.INPUT_REQUIRED, "", ToolTipIcon.Warning, PosForm, ToolTip1, Me)
            Return False
          Else

            If CInt(tblTable.GetCellValue(intCount, ColumnName.IsUseLot)) = 1 Then
              If SL_ZDNClass.ReadByID(CInt(tblTable.GetCellValue(intCount, ColumnName.InvID))) Then
                Integer.TryParse(tblTable.GetCellValue(intCount, ColumnName.InvID).ToString, intInvID)

                If setType.CodeText = CStr(SLConstants.ShipmentKbn.NORMAL_SHIPPING_CODE) Then
                  GetItemAmount()

                  Decimal.TryParse(tblTable.GetCellValue(intCount, ColumnName.Qty).ToString, decQty)
                  Integer.TryParse(tblTable.GetCellValue(intCount, ColumnName.Number).ToString, intNum)

                  intAmountNum = 0
                  decAmountQty = 0D
                  Integer.TryParse(htItemAmountNum(intInvID.ToString).ToString, intAmountNum)
                  Decimal.TryParse(htItemAmountQty(intInvID.ToString).ToString, decAmountQty)

                  If HeaderLabel.State = LabelStateType.New Then

                    If SL_ZDNClass.sl_zdn_suryo < decAmountQty Then
                      PosForm.X = CInt(QUANTITY_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
                      strErrMsg = "行目の入力数量が在庫数量を上回っています"
                      Integer.TryParse(htItemErrRow(intInvID.ToString).ToString, intNQErrRow)
                    ElseIf SL_ZDNClass.sl_zdn_honsu < intAmountNum Then
                      PosForm.X = CInt(NUMBER_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
                      strErrMsg = "行目の入力本数が在庫本数を上回っています"
                      Integer.TryParse(htItemErrRow(intInvID.ToString).ToString, intNQErrRow)
                    End If
                  Else
                    If SL_ZDNClass.sl_zdn_suryo + decQty < decAmountQty Then
                      PosForm.X = CInt(QUANTITY_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
                      strErrMsg = "行目の入力数量が在庫数量を上回っています"
                      Integer.TryParse(htItemErrRow(intInvID.ToString).ToString, intNQErrRow)
                    ElseIf SL_ZDNClass.sl_zdn_honsu + intNum < intAmountNum Then
                      PosForm.X = CInt(NUMBER_ERRMESSAGE_LOCATION * tblTable.Width + tblTable.Location.X)
                      strErrMsg = "行目の入力本数が在庫本数を上回っています"
                      Integer.TryParse(htItemErrRow(intInvID.ToString).ToString, intNQErrRow)
                    End If
                  End If

                End If

              Else
                strErrMsg = "行目のロットが存在しません"
              End If

            End If

            If strErrMsg.Trim.Length > 0 Then
              If intNQErrRow = 0 Then intNQErrRow = intCount + 1

              SLCmnFunction.ShowToolTip(intNQErrRow & strErrMsg, "商品情報が不正です", ToolTipIcon.Warning, PosForm, ToolTip1, Me)
              Return False
            End If

            If CInt(tblTable.GetCellValue(intCount, ColumnName.IsUseLot)) = 1 Then
              strProductCode = tblTable.GetCellValue(intCount, ColumnName.ProductCode).ToString
              strWarehouse = tblTable.GetCellValue(intCount, ColumnName.Warehouse).ToString
              strLotNo = tblTable.GetCellValue(intCount, ColumnName.UserLotNo).ToString
              If Not SL_ZDNClass.CheckLotExists(strProductCode, strWarehouse, strLotNo) Then
                SLCmnFunction.ShowToolTip("該当するロットが存在しません", "商品情報が不正です", ToolTipIcon.Warning, PosForm, ToolTip1, Me)
                Return False
              End If

            End If

          End If

        End If
      Next
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Sub GetItemAmount()
    Try

      Dim strInvID As String = String.Empty
      Dim intNum As Integer = 0
      Dim decQty As Decimal = 0D
      htItemAmountNum = New Hashtable
      htItemAmountQty = New Hashtable
      htItemErrRow = New Hashtable
      'tbltable
      For intLpc As Integer = 0 To m_IntRowCount - 1
        If CInt(tblTable.GetCellValue(intLpc, ColumnName.IsUseLot)) = 1 Then
          strInvID = tblTable.GetCellValue(intLpc, ColumnName.InvID).ToString
          intNum = Integer.Parse(tblTable.GetCellValue(intLpc, ColumnName.Number).ToString)
          decQty = Decimal.Parse(tblTable.GetCellValue(intLpc, ColumnName.Qty).ToString)

          If htItemAmountNum(strInvID) Is Nothing Then
            htItemAmountNum(strInvID) = intNum
            htItemAmountQty(strInvID) = decQty
          Else
            htItemAmountNum(strInvID) = Integer.Parse(htItemAmountNum(strInvID).ToString) + intNum
            htItemAmountQty(strInvID) = Decimal.Parse(htItemAmountQty(strInvID).ToString) + decQty
          End If
          htItemErrRow(strInvID) = intLpc
        End If
      Next

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)

    End Try
  End Sub

  Private Sub ValidatedTantosya(ByVal EMST As EMST, Optional ByVal EMSB As EMSB = Nothing)
    Try
      Me.setPerson.CodeText = EMST.emst_kbn
      Me.setPerson.NameText = EMST.emst_str
      If Not EMSB Is Nothing Then
        Me.setDepartment.CodeText = EMSB.emsb_kbn
        Me.setDepartment.NameText = EMSB.emsb_str
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub ValidatedBumon(ByVal EMSB As EMSB)
    Try
      Me.setDepartment.CodeText = EMSB.emsb_kbn
      Me.setDepartment.NameText = EMSB.emsb_str
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub ValidatedProject(ByVal PJMS As PJMS)
    Try
      Me.setProject.CodeText = PJMS.pjms_code
      Me.setProject.NameText = PJMS.pjms_name
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub ValidatedTekiyo(ByVal strCode As String, ByVal strName As String)
    Try
      Me.setMemoCode.CodeText = strCode
      Me.setMemoContent.CodeText = strName.Trim
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>  
  ''' Validate Product -> Display corresponding data to table
  ''' </summary>
  ''' <param name="rowIndex"></param>
  ''' <param name="newValue"></param>
  ''' <param name="needMessageBox"></param>
  ''' <returns></returns>
  ''' <remarks>Content: Set cell editable/uneditable based on Product code</remarks>
  Private Function ValidateSyohin(ByVal rowIndex As Integer, ByVal newValue As String, ByVal needMessageBox As Boolean) As Boolean
    Try
      Dim strStandardCode As String = SLCmnFunction.standardlizeCode(newValue, AMS1Class.ams1_ProductLength) 'Standardlize no of product code digit
      'Load data Product data from Product code
      Dim SMSClass As SMS = New SMS(connector)
      Dim ZaikoDialog As Sunloft.PCAForms.SLZaikoDialog
      Dim ShippingType As SLZaikoDialog.enmSlipType = CType(setType.CodeText, SLZaikoDialog.enmSlipType)
      Dim SMSPClass As SMSP = New SMSP(connector)
      Dim SL_SMSClass As SL_SMS = New SL_SMS(connector)
      Dim TaxClass As TAX = New TAX(connector)
      Dim TMSClass As TMS = New TMS(connector)

      If m_strCurCode.Trim = newValue Then Return False

      If SMSClass.ReadByID(strStandardCode) Then
        SMSPClass.ReadByID(strStandardCode)
        TMSClass.ReadByCode(setSupCode.CodeText)
        'Product found
        tblTable.SetCellValue(rowIndex, ColumnName.ProductCode, strStandardCode)
        tblTable.SetCellValue(rowIndex, ColumnName.ProductName, SMSClass.sms_mei)
        tblTable.SetCellValue(rowIndex, ColumnName.Spec, SMSClass.sms_kikaku)
        tblTable.SetCellValue(rowIndex, ColumnName.Color, SMSClass.sms_color)
        tblTable.SetCellValue(rowIndex, ColumnName.Size, SMSClass.sms_size)
        tblTable.SetCellValue(rowIndex, ColumnName.Warehouse, SMSClass.sms_souko)
        tblTable.SetCellValue(rowIndex, ColumnName.UnitPrice, Format(SLCmnFunction.GetUnitPrice(SMSPClass, TMSClass), SLCmnFunction.FormatComma(SMSClass.sms_tketa)))
        tblTable.SetCellValue(rowIndex, ColumnName.QtyPerCase, SMSClass.sms_iri)
        tblTable.SetCellValue(rowIndex, ColumnName.UnitName, SMSClass.sms_tani)
        tblTable.SetCellValue(rowIndex, ColumnName.UnitPriceG, 0)
        tblTable.SetCellValue(rowIndex, ColumnName.Amount, 0)
        tblTable.SetCellValue(rowIndex, ColumnName.AmountG, 0)
        tblTable.SetCellValue(rowIndex, ColumnName.Qty, String.Empty)
        tblTable.SetCellValue(rowIndex, ColumnName.NoOfCase, String.Empty)
        tblTable.SetCellValue(rowIndex, ColumnName.UserLotNo, String.Empty)
        tblTable.SetCellValue(rowIndex, ColumnName.Arari, String.Empty)
        tblTable.SetCellValue(rowIndex, ColumnName.InvID, 0)

        Dim taxRate As Decimal = SLCmnFunction.GetTaxRate(strStandardCode, Integer.Parse(ldtShipmentDate.Date.ToString("yyyyMMdd")), SMSPClass, TaxClass)

        Select Case SMSClass.smsp_tax
          Case 0
            tblTable.SetCellValue(rowIndex, ColumnName.Rate, String.Empty)
            tblTable.SetCellValue(rowIndex, ColumnName.TaxIncluded, SLConstants.TAX.NO_TAX)
          Case Else
            tblTable.SetCellValue(rowIndex, ColumnName.Rate, taxRate & "%")
            Select Case SMSClass.smsp_komi
              Case SLConstants.TAX.TAX_INCLUDED
                tblTable.SetCellValue(rowIndex, ColumnName.TaxIncluded, SLConstants.TAX.TAX_INCLUDED)
              Case Else
                tblTable.SetCellValue(rowIndex, ColumnName.TaxIncluded, SLConstants.TAX.TAX_EXCLUDED)
            End Select
        End Select

        If m_IntRowIndex = m_IntRowCount Then m_IntRowCount = m_IntRowCount + 1
        tblTable.SetCellValue(rowIndex, ColumnName.QtyNoOfDec, SMSClass.sms_sketa)
        tblTable.SetCellValue(rowIndex, ColumnName.UnitPriceNoOfDec, SMSClass.sms_tketa)

        If SL_SMSClass.ReadByProductCode(strStandardCode) Then
          tblTable.SetCellValue(rowIndex, ColumnName.IsUseLot, SL_SMSClass.sl_sms_lkbn)
          m_isUseLot = SL_SMSClass.sl_sms_lkbn = 1
        Else
          tblTable.SetCellValue(rowIndex, ColumnName.IsUseLot, 0)
          m_isUseLot = False
        End If

        If SL_SMSClass.sl_sms_lkbn = 1 Then
          ZaikoDialog = New Sunloft.PCAForms.SLZaikoDialog(connector, strStandardCode, ldtShipmentDate.Date, ShippingType, SMSClass.sms_souko)
          If ZaikoDialog.ShowDialog = Windows.Forms.DialogResult.OK Then SetRetZaikoDlg(ZaikoDialog)
        Else
          tblTable.SetCellValue(rowIndex, ColumnName.UnitPriceG, Format(SLCmnFunction.GetOriginalUnitPrice(SMSPClass, TMSClass), SLCmnFunction.FormatComma(SMSClass.sms_tketa)))
        End If

        Return True
      Else
        Return False
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function
#End Region

#Region "Search"
  ''' <summary>
  ''' Display search screen (Search items like press F8)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub SearchItem(ByVal sender As System.Object, Optional e As System.ComponentModel.CancelEventArgs = Nothing)
    Dim _CodeSet As PcaCodeSet = Nothing
    Dim itemName As String = String.Empty
    Dim newCode As String = String.Empty
    Dim location As Point = Tools.ControlTool.GetDialogLocation(Me.tblTable.RefButton)
    Dim strCurrentCode As String = String.Empty
    Dim EMST As New EMST(connector)
    Dim TMS As New TMS(connector)
    Dim EMSB As New EMSB(connector)
    Dim PJMS As New PJMS(connector)
    Dim EMS As New EMS(connector)

    Try
      Dim denpyoDialog As SLDenSearchDialog = New SLDenSearchDialog(connector)
      Dim MasterDialog As SLMasterSearchDialog = New SLMasterSearchDialog(connector)

      Select Case sender.GetType.ToString
        Case GetType(PCA.Controls.PcaCodeSet).ToString, GetType(PCA.Controls.PcaCommandManager).ToString ' "PCA.Controls.PcaCommandManager"
          If sender.GetType.ToString = GetType(PCA.Controls.PcaCodeSet).ToString Then
            _CodeSet = DirectCast(sender, PcaCodeSet)
            location = Tools.ControlTool.GetDialogLocation(_CodeSet.ReferButton)
          Else
            'location
            Select Case m_strSearchItem
              Case setPerson.Name
                location = Tools.ControlTool.GetDialogLocation(setPerson.ReferButton)
              Case setProject.Name
                location = Tools.ControlTool.GetDialogLocation(setProject.ReferButton)
              Case setDepartment.Name
                location = Tools.ControlTool.GetDialogLocation(setDepartment.ReferButton)
              Case setMemoCode.Name
                location = Tools.ControlTool.GetDialogLocation(setMemoCode.ReferButton)
              Case setSupCode.Name
                location = Tools.ControlTool.GetDialogLocation(setSupCode.ReferButton)
            End Select

          End If

          Select Case If(sender.GetType.ToString = GetType(PCA.Controls.PcaCodeSet).ToString, _CodeSet.Name, m_strSearchItem)
            Case setPerson.Name
              strCurrentCode = setPerson.CodeText
              newCode = MasterDialog.ShowReferTantosyaDialog(strCurrentCode, location)
            Case setProject.Name
              strCurrentCode = setProject.CodeText
              newCode = MasterDialog.ShowReferProjectDialog(strCurrentCode, location)
            Case setDepartment.Name
              strCurrentCode = setDepartment.CodeText
              newCode = MasterDialog.ShowReferBumonDialog(strCurrentCode, location)
            Case setMemoCode.Name
              strCurrentCode = setMemoCode.CodeText
              newCode = MasterDialog.ShowReferTekiyoDialog(strCurrentCode, location)
            Case setSupCode.Name
              strCurrentCode = setSupCode.CodeText
              newCode = MasterDialog.ShowReferTmsDialog(strCurrentCode, location)
            Case setOrderNo.Name
              newCode = denpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.PCAOrderSlip)
            Case setPurNo.Name
              newCode = denpyoDialog.ShowReferDialogSlip(SLDenSearchDialog.ENUM_DEN_TYPE.PCASalesSlip)
            Case Else
              SearchItemTable(sender, itemName, strCurrentCode, newCode, MasterDialog, True)
              Return
          End Select

          If String.IsNullOrEmpty(newCode) Then
            If Not e Is Nothing Then e.Cancel = True
          Else
            If strCurrentCode = newCode Then
              'No changes
              Return
            End If

            Select Case If(sender.GetType.ToString = GetType(PCA.Controls.PcaCodeSet).ToString, _CodeSet.Name, m_strSearchItem)
              Case setPurNo.Name
                setPurNo.CodeText = newCode
                setPurNo_Validating(sender, e)
              Case setOrderNo.Name
                setOrderNo.CodeText = newCode
                setOrderNo_Validating(sender, e)
              Case setPerson.Name
                If EMST.ReadByCode(newCode) Then
                  If EMSB.ReadByCode(EMST.emst_bmn) Then
                    ValidatedTantosya(EMST, EMSB)
                  Else
                    ValidatedTantosya(EMST)
                  End If
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If

              Case setProject.Name
                If PJMS.ReadByCode(newCode) Then
                  ValidatedProject(PJMS)
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If

              Case setDepartment.Name
                If EMSB.ReadByCode(newCode) Then
                  ValidatedBumon(EMSB)
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If

              Case setMemoCode.Name
                Dim strName As String = EMS.ReadKbnMasterByIDAndKbn(SLConstants.EMS.MemoJ, newCode)
                If strName.Trim.Length > 0 Then
                  ValidatedTekiyo(newCode, strName)
                Else
                  If Not e Is Nothing Then e.Cancel = True
                End If

              Case setSupCode.Name
                If TMS.ReadByCode(newCode) Then setSupCode.CodeText = TMS.tms_tcd

                CodeTextValidating()
            End Select
          End If

        Case GetType(PCA.TSC.Kon.Tools.TscMeisaiTable).ToString
          SearchItemTable(sender, itemName, strCurrentCode, newCode, MasterDialog)
      End Select

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  Private Sub SearchItemTable(ByVal sender As System.Object, itemName As String, strCurrentCode As String, newCode As String, MasterDialog As SLMasterSearchDialog, Optional isCommandManager As Boolean = False)
    Dim isGetData As Boolean = False
    Dim strNextCellName As String = String.Empty

    If isCommandManager Then
      itemName = m_strSearchItem
    Else
      Dim _table = DirectCast(sender, PCA.TSC.Kon.Tools.TscMeisaiTable)
      itemName = _table.InputName
    End If

    strCurrentCode = Me.tblTable.GetCellValue(m_IntRowIndex, itemName).ToString.Trim
    Select Case itemName
      Case MeisaiTableDefines.SyohinCode
        'Display sub screen
        newCode = MasterDialog.ShowReferSmsDialog(strCurrentCode, Location)
      Case MeisaiTableDefines.SokoCode
        newCode = MasterDialog.ShowReferSokoDialog(strCurrentCode, Location)
    End Select

    If String.IsNullOrEmpty(newCode) = False Then
      If newCode <> m_strCurCode Then
        Select Case itemName
          Case MeisaiTableDefines.SyohinCode
            'Validate if there's any changes
            strNextCellName = ColumnName.ProductName
            If Me.ValidateSyohin(m_IntRowIndex, newCode, True) = True Then isGetData = True
          Case MeisaiTableDefines.SokoCode
            strNextCellName = ColumnName.Qty
            If Me.ValidateSoko(m_IntRowIndex, newCode, True) = True Then isGetData = True
        End Select

        If (isGetData = False) Then
          'Validate error
          Me.tblTable.SetCellValue(m_IntRowIndex, itemName, strCurrentCode)
          Me.tblTable.SetCellFocus(m_IntRowIndex, itemName)
          Return
        End If
      End If
      Me.tblTable.SetCellValue(m_IntRowIndex, itemName, newCode)
      If String.IsNullOrEmpty(strNextCellName) = False Then Me.tblTable.SetCellFocus(m_IntRowIndex, strNextCellName)
    Else
      Me.tblTable.SetCellFocus(m_IntRowIndex, itemName)
      Return
    End If
  End Sub

#End Region

#Region "Sub Form Method"
  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then connector.LogOffSystem()
  End Sub

  Private Sub tblTable_EnabledChanged(sender As System.Object, e As System.EventArgs) Handles tblTable.EnabledChanged
    Try
      ToolStripMenuItemDeleteRow.Enabled = tblTable.Enabled
      ToolStripMenuItemInsertRow.Enabled = tblTable.Enabled

      ToolStripButtonDeleteRow.Enabled = tblTable.Enabled
      ToolStripButtonInsertRow.Enabled = tblTable.Enabled
    Catch ex As Exception

    End Try
  End Sub
#End Region

#Region " PcaCodeSet Methods"

  Private Sub CodeSet_CodeTextEnter(sender As System.Object, e As System.EventArgs) Handles setSupCode.CodeTextEnter, setPerson.CodeTextEnter, setDepartment.CodeTextEnter, setMemoCode.CodeTextEnter, setProject.CodeTextEnter, setOrderNo.CodeTextEnter, setPurNo.CodeTextEnter
    Dim myCodeSet As PcaCodeSet
    Try
      Select Case sender.GetType.ToString
        Case GetType(PCA.Controls.PcaCodeSet).ToString
          myCodeSet = DirectCast(sender, PcaCodeSet)
          m_strSearchItem = myCodeSet.Name


      End Select
      PcaFunctionCommandRefer.Enabled = True
      IsValidate = False
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub CodeSet_CodeTextLeave(sender As System.Object, e As System.EventArgs) Handles setSupCode.CodeTextLeave, setPerson.CodeTextLeave, setDepartment.CodeTextLeave, setMemoCode.CodeTextLeave, setProject.CodeTextLeave, setOrderNo.CodeTextLeave, setPurNo.CodeTextLeave
    Dim myCodeSet As PcaCodeSet
    Try
      Select Case sender.GetType.ToString
        Case GetType(PCA.Controls.PcaCodeSet).ToString
          myCodeSet = DirectCast(sender, PcaCodeSet)
          m_strSearchItem = ""
          PcaFunctionCommandRefer.Enabled = False
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub CodeSet_Enter(sender As System.Object, e As System.EventArgs) Handles setSupCode.Enter, setPerson.Enter, setDepartment.Enter, setMemoCode.Enter, setProject.Enter
    Try
      Dim objCodeSet As PcaCodeSet = DirectCast(sender, PcaCodeSet)
      m_strCurCode = objCodeSet.CodeText
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Changes when code text changes between Normal shipping + Return goods
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub setType_CodeTextChanged(sender As System.Object, e As System.EventArgs) Handles setType.CodeTextChanged
    Try
      'Type changes: Normal shipping & Return goods
      Select Case setType.CodeText
        Case CStr(SLConstants.ShipmentKbn.NORMAL_SHIPPING_CODE)
          setType.NameText = SLConstants.ShipmentKbn.NORMAL_SHIPPING
        Case CStr(SLConstants.ShipmentKbn.RETURN_GOODS_CODE)
          setType.NameText = SLConstants.ShipmentKbn.RETURN_GOODS
      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' leaves when code text changes 
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub setType_CodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setType.CodeTextValidating2
    Try
      If setType.CodeText.Trim.Length = 0 Then e.Cancel = True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Search Master 
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub CodeSet_ClickReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles setSupCode.ClickReferButton2, setPerson.ClickReferButton2, _
                                                                                                                     setDepartment.ClickReferButton2, setProject.ClickReferButton2, _
                                                                                                                     setMemoCode.ClickReferButton2, setOrderNo.ClickReferButton2, _
                                                                                                                     setPurNo.ClickReferButton2
    m_isShowDialog = True
    'Display Refer screen
    SearchItem(sender, e)
    m_isShowDialog = False
  End Sub
#End Region


  Private Sub HeaderItems_Changed(sender As System.Object, e As System.EventArgs) Handles setType.CodeTextChanged, ldtShipmentDate.DateChanged, setSupCode.CodeTextChanged, _
                                                                                          setOrderNo.CodeTextChanged, setPurNo.CodeTextChanged, setPerson.CodeTextChanged, _
                                                                                          setProject.CodeTextChanged, setDepartment.CodeTextChanged, _
                                                                                          ltxtWhNo2.TextChanged, setMemoCode.CodeTextChanged
    Try


      '変更が開始された
      If MasterDialog IsNot Nothing Then MasterDialog.BeginChanged()
      m_isDataChanged = True

      Dim objCodeset As PcaCodeSet = DirectCast(sender, PcaCodeSet)
      Dim intValue As Integer = 0

      If objCodeset.Name = setPurNo.Name Or objCodeset.Name = setOrderNo.Name Then
        If Not IsNumeric(objCodeset.CodeText) And objCodeset.CodeText.Trim.Length > 0 Then objCodeset.CodeText = System.Text.RegularExpressions.Regex.Replace(objCodeset.CodeText, "[^0-9]", "")
        If Not Integer.TryParse(objCodeset.CodeText, intValue) Then objCodeset.CodeText = Strings.Left(objCodeset.CodeText, objCodeset.CodeText.Length - 1)
      End If
    Catch ex As Exception

    End Try
  End Sub


#Region " MeisaiTable Methods"

  ''' <summary>
  ''' CodeTextEnter/Leave  Get Controller name, enable/disable F8 (Search)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks></remarks>
  Private Sub tblTable_LostCellFocus(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.LostCellFocus
    If m_strSearchItem = ColumnName.ProductCode Or m_strSearchItem = ColumnName.Warehouse Then
      m_strSearchItem = ""
      PcaFunctionCommandRefer.Enabled = False
    End If
  End Sub

  ''' <summary>
  ''' Get row index, set Lot no cell editable/uneditable based on Product code
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks>Content: Check allocated quantity </remarks>
  Private Sub tblTable_EnterInputRow(sender As Object, args As PCA.Controls.InputRowEventArgs) Handles tblTable.EnterInputRow
    Try
      m_IntRowIndex = args.RowIndex
      If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode)) Then
        SetPropertiesByProduct(m_IntRowIndex, tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode).ToString.Trim)
        tblTable.SetCellValue(args.RowIndex, ColumnName.No, args.RowIndex + 1)
      End If
      m_isUseLot = False
      If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(m_IntRowIndex, ColumnName.IsUseLot)) Then
        m_isUseLot = CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.IsUseLot)) = 1
      End If
      m_isDataChanged = True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Get row index, set Lot no cell editable/uneditable based on Product code
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks>Content: Check allocated quantity </remarks>
  Private Sub tblTable_GotCellFocus(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.GotCellFocus
    Dim isLotNoEditable As Boolean = False

    Try
      tblTable.SelectHeaderCell(args)
      If m_IntRowIndex < m_IntRowCount Then ' Choose an exsisting row
        If Not tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode) Is Nothing AndAlso tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode).ToString <> "" Then
          isLotNoEditable = True
        End If
      ElseIf m_IntRowIndex = m_IntRowCount Then 'Choose a new row, focus on product code, or in edit mode, select last row?
        If Not tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode) Is Nothing Then
          If tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode).ToString = "" Then
            isLotNoEditable = False
          Else
            isLotNoEditable = True
          End If
        End If

      End If
      'Set search item, enable Refer F8
      Select Case args.CellName
        Case ColumnName.ProductCode, ColumnName.Warehouse
          PcaFunctionCommandRefer.Enabled = True
          m_strSearchItem = args.CellName
          If Not tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode) Is Nothing Then
            m_strCurCode = tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode).ToString
          End If

          If m_strCurCode.Length > 0 And CInt(tblTable.GetCellValue(m_IntRowIndex, ColumnName.IsUseLot)) = 1 Then
            PcaFunctionCommandInv.Enabled = True
          Else
            PcaFunctionCommandInv.Enabled = False
          End If
        Case Else
          If tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode) Is Nothing Then
            tblTable.SetCellFocus(If(m_IntRowIndex = 0, 0, m_IntRowIndex - 1), ColumnName.ProductCode)
            Exit Sub
          End If

          PcaFunctionCommandRefer.Enabled = False
          If tblTable.GetCellValue(m_IntRowIndex, ColumnName.ProductCode).ToString.Trim.Length > 0 Then
            PcaFunctionCommandInv.Enabled = True
          Else
            PcaFunctionCommandInv.Enabled = False
          End If
          m_strSearchItem = ""

      End Select

      If isLotNoEditable Then

      Else
        'Jump in the next empty row, focus on Product code
        If m_IntRowIndex > m_IntRowCount Then
          tblTable.SetCellFocus(m_IntRowCount, ColumnName.ProductCode)
        Else
          tblTable.SetCellFocus(m_IntRowIndex, ColumnName.ProductCode)
        End If
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Search Table Details Items Master(Item, Warehouse ...)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks></remarks>
  Private Sub tblTable_ClickRefButton(sender As System.Object, args As PCA.Controls.CellEventArgs) Handles tblTable.ClickRefButton
    Try
      SearchItem(sender, New CancelEventArgs(False))
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub
#End Region

#Region " Insert"

  ''' <summary>
  ''' Save Data when press F12
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Insert new data in Create mode</remarks>
  Private Function InsertData() As Boolean
    Dim isSuccess As Boolean = True
    Dim session As ICustomSession

    Try
      session = connector.CreateTransactionalSession
      isSuccess = InsertHeader(session)
      If isSuccess Then isSuccess = InsertDetails(session)
      If isSuccess = True Then
        session.Commit()
        session.Dispose()
        SLCmnFunction.ShowToolTip("伝票番号：" & SL_SYKHClass.sl_sykh_denno & " で登録されました。", _
                                  SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
      Else
        session.Rollback()
        session.Dispose()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function

  ''' <summary>
  ''' If cell is not empty, save cell text value to table
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertHeader(ByRef session As ICustomSession) As Boolean 'Is Register or Edit
    Try
      With SL_SYKHClass

        .sl_sykh_id = SL_SYKHClass.GetNewID
        .sl_sykh_denno = SL_SYKHClass.GetNewSlipID

        .sl_sykh_return = Integer.Parse(setType.CodeText)
        .sl_sykh_uribi = ldtShipmentDate.IntDate
        .sl_sykh_nouki = ldtDlvDate.IntDate
        .sl_sykh_tcd = setSupCode.CodeText
        .sl_sykh_carrier = String.Empty
        .sl_sykh_jtan = setPerson.CodeText
        .sl_sykh_tekcd = setMemoCode.CodeText
        .sl_sykh_tekmei = setMemoContent.CodeText

        If Not SLCmnFunction.checkObjectNothingEmpty(setOrderNo.CodeText) Then
          .sl_sykh_datakbn = SLConstants.SL_SYKH_RelaySlipType.OrderReceiving
          .sl_sykh_dataid = m_intOrderSlipID
        ElseIf Not SLCmnFunction.checkObjectNothingEmpty(setPurNo.CodeText) Then
          .sl_sykh_datakbn = SLConstants.SL_SYKH_RelaySlipType.Sales
          .sl_sykh_dataid = m_intPurSlipID
        Else
          .sl_sykh_datakbn = SLConstants.SL_SYKH_RelaySlipType.ManualShipping
          .sl_sykh_dataid = 0
        End If
        .sl_sykh_denno2 = ltxtWhNo2.Text
        .sl_sykh_jbmn = setDepartment.CodeText
        .sl_sykh_pjcode = setProject.CodeText
        .sl_sykh_insdate = Now
        .sl_sykh_insuser = connector.UserId
      End With
      Return SL_SYKHClass.Insert(session)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
    End Try
  End Function

  ''' <summary>
  ''' Insert Details Data (Table)
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertDetails(ByRef session As ICustomSession) As Boolean
    Dim isSuccess As Boolean = True
    Try

      Dim sykd_lists() As SL_SYKD
      ReDim sykd_lists(m_IntRowCount)

      For intCount = 0 To m_IntRowCount 'Create in DB if new row, Update if existing row. Count number of new rows (to set system lot number)
        If isSuccess Then
          If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, ColumnName.ProductCode)) Then

            isSuccess = SaveDataToSYKDClass(intCount, sykd_lists)

            'Register details table in SL_SYKD
            If isSuccess Then isSuccess = sykd_lists(intCount).Insert(session)

            If isSuccess Then

              Dim SL_ZHKClass As SL_ZHK = New SL_ZHK(connector)
              isSuccess = If(isSuccess, SetToZHK(SL_ZHKClass, SL_SYKHClass, sykd_lists(intCount), intCount), False)

              isSuccess = If(isSuccess, SetToZDN(sykd_lists, SL_SYKHClass, intCount), False)
              If SL_ZHKClass.sl_zhk_zdn_id > 0 Then
                If SL_ZDNClass.UpdateNumberQuantity(False, session) Then
                  SL_ZHKClass.intRowCount = intCount
                  If SL_ZHKClass.Insert(session) Then
                    isSuccess = True
                  End If
                Else
                  isSuccess = False
                End If
              End If

            Else
              isSuccess = False
            End If
          End If
        End If
      Next

      Return isSuccess
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region

#Region " Update"

  ''' <summary>
  ''' Save Data when press F12
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Update new data in Modyfy mode</remarks>
  Private Function UpdateData() As Boolean
    Dim isSuccess As Boolean = True
    Dim session As ICustomSession

    Try
      session = connector.CreateTransactionalSession
      isSuccess = UpdateHeader(session)
      If isSuccess Then isSuccess = UpdateDetails(session)
      If isSuccess = True Then
        session.Commit()
        session.Dispose()
        SLCmnFunction.ShowToolTip("登録処理" & SLConstants.CommonMessage.COMPLATE_MESSAGE,
                                  SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSave.CommandId)
      Else
        session.Rollback()
        session.Dispose()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess

  End Function

  ''' <summary>
  ''' If cell is not empty, save cell text value to table
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateHeader(ByRef session As ICustomSession) As Boolean 'Is Register or Edit
    Try
      With SL_SYKHClass
        .sl_sykh_return = Integer.Parse(setType.CodeText)
        .sl_sykh_uribi = ldtShipmentDate.IntDate
        .sl_sykh_nouki = ldtDlvDate.IntDate
        .sl_sykh_tcd = setSupCode.CodeText
        .sl_sykh_carrier = String.Empty
        .sl_sykh_jtan = setPerson.CodeText
        .sl_sykh_tekcd = setMemoCode.CodeText
        .sl_sykh_tekmei = setMemoContent.CodeText
        If Not SLCmnFunction.checkObjectNothingEmpty(setOrderNo.CodeText) Then
          .sl_sykh_datakbn = SLConstants.SL_SYKH_RelaySlipType.OrderReceiving
          .sl_sykh_dataid = m_intOrderSlipID
        ElseIf Not SLCmnFunction.checkObjectNothingEmpty(setPurNo.CodeText) Then
          .sl_sykh_datakbn = SLConstants.SL_SYKH_RelaySlipType.Sales
          .sl_sykh_dataid = m_intPurSlipID
        Else
          .sl_sykh_datakbn = SLConstants.SL_SYKH_RelaySlipType.ManualShipping
          .sl_sykh_dataid = 0
        End If
        .sl_sykh_denno2 = ltxtWhNo2.Text
        .sl_sykh_jbmn = setDepartment.CodeText
        .sl_sykh_pjcode = setProject.CodeText

        .sl_sykh_upddate = Now
        .sl_sykh_upduser = connector.UserId


      End With

      Return SL_SYKHClass.Update(session)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
    End Try
  End Function

  ''' <summary>
  ''' Update Details Data (Table)
  ''' </summary>
  ''' <param name="session"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateDetails(ByRef session As ICustomSession) As Boolean
    Dim isSuccess As Boolean = True
    Dim SL_ZHKClass As SL_ZHK = New SL_ZHK(connector)
    Try

      ReDim Preserve SYKD_List(m_IntRowCount)

      SYKD_List(0).sl_sykd_hid = SL_SYKHClass.sl_sykh_id
      'Reset Sykd
      isSuccess = SYKD_List(0).DeleteByID(session)

      For intCount = 0 To m_IntRowCount 'Create in DB if new row, Update if existing row. Count number of new rows (to set system lot number)
        If isSuccess Then
          If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, ColumnName.ProductCode)) Then

            'Register details table in SL_SYKD
            isSuccess = SaveDataToSYKDClass(intCount, SYKD_List)
            isSuccess = If(isSuccess, SYKD_List(intCount).Insert(session), False)

            'UpdateZHK
            isSuccess = If(isSuccess, SetToZHK(SL_ZHKClass, SL_SYKHClass, SYKD_List(intCount), intCount, True), False)
            isSuccess = If(isSuccess, SL_ZHKClass.DeleteInventoryWithSEQ(session), False)
            isSuccess = If(isSuccess, SetToZHK(SL_ZHKClass, SL_SYKHClass, SYKD_List(intCount), intCount), False)
            isSuccess = If(isSuccess, SL_ZHKClass.Insert(session), False)

            'UpdateZDN
            isSuccess = If(isSuccess, SetToZDN(SYKD_List, SL_SYKHClass, intCount), False)
            isSuccess = If(isSuccess, SL_ZDNClass.UpdateNumberQuantity(False, session), False)
            If isSuccess Then
              If SL_SYKHClass.sl_sykh_return = SLConstants.ShipmentKbn.RETURN_GOODS_CODE Then
                SL_ZDNClass.sl_zdn_nyukosuryo = CDec(tblTable.GetCellValue(intCount, ColumnName.PrevDecQty))
                SL_ZDNClass.sl_zdn_nyukohonsu = CInt(tblTable.GetCellValue(intCount, ColumnName.PrevNumber))
              Else
                SL_ZDNClass.sl_zdn_nyukosuryo = CDec(tblTable.GetCellValue(intCount, ColumnName.PrevDecQty)) * -1
                SL_ZDNClass.sl_zdn_nyukohonsu = CInt(tblTable.GetCellValue(intCount, ColumnName.PrevNumber)) * -1
              End If
              isSuccess = SL_ZDNClass.UpdateNumberQuantity(True, session)
            End If

          End If
        End If
      Next

      Return isSuccess

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

#End Region


#Region " Save/Set Data"

  Private Function SaveDataToSYKDClass(ByRef intCount As Integer, ByRef sykd_lists As SL_SYKD()) As Boolean
    Try
      Dim SMSPClass As SMSP = New SMSP(connector)
      If HeaderLabel.State = LabelStateType.New Then sykd_lists(intCount) = New SL_SYKD(connector)
      sykd_lists(intCount) = New SL_SYKD(connector)

      'Save all data to m_tableDetails class
      With sykd_lists(intCount)

        .sl_sykd_hid = SL_SYKHClass.sl_sykh_id
        .sl_sykd_eda = intCount
        If m_SlipType = SlipType.Order Then
          SaveToClass(ColumnName.Type, intCount, .sl_sykd_skbn)
        Else
          .sl_sykd_skbn = 0
        End If

        .sl_sykd_scd = tblTable.GetCellValue(intCount, ColumnName.ProductCode).ToString.Trim
        SaveToClass(ColumnName.ProductName, intCount, .sl_sykd_mei)
        SaveToClass(ColumnName.Spec, intCount, .sl_sykd_kikaku)
        SaveToClass(ColumnName.Color, intCount, .sl_sykd_color)
        SaveToClass(ColumnName.Size, intCount, .sl_sykd_size)
        SaveToClass(ColumnName.Warehouse, intCount, .sl_sykd_souko)

        SMSPClass.ReadByID(tblTable.GetCellValue(intCount, ColumnName.ProductCode).ToString)
        If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, ColumnName.TaxIncluded)) Then
          .sl_sykd_tax = SMSPClass.smsp_tax
        Else
          .sl_sykd_tax = 0
        End If

        Select Case tblTable.GetCellValue(intCount, ColumnName.TaxIncluded).ToString.Trim
          Case SLConstants.TAX.TAX_INCLUDED
            .sl_sykd_komi = SLConstants.TAX.TAX_INCLUDED_CODE
          Case Else
            .sl_sykd_komi = SLConstants.TAX.TAX_EXCLUDED_CODE
        End Select

        SaveToClass(ColumnName.QtyPerCase, intCount, .sl_sykd_iri)
        SaveToClass(ColumnName.NoOfCase, intCount, .sl_sykd_hako)
        SaveToClass(ColumnName.Qty, intCount, .sl_sykd_suryo)
        SaveToClass(ColumnName.Number, intCount, .sl_sykd_honsu)

        SaveToClass(ColumnName.UnitName, intCount, .sl_sykd_tani)

        SaveToClass(ColumnName.UnitPrice, intCount, .sl_sykd_tanka)
        SaveToClass(ColumnName.Amount, intCount, .sl_sykd_kingaku)
        SaveToClass(ColumnName.UnitPriceG, intCount, .sl_sykd_gentan)
        SaveToClass(ColumnName.AmountG, intCount, .sl_sykd_genka)
        SaveToClass(ColumnName.Note, intCount, .sl_sykd_biko)
        SaveToClass(ColumnName.Arari, intCount, .sl_sykd_arari)
        If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, ColumnName.Rate)) Then
          Dim strRateTemp As String = tblTable.GetCellValue(intCount, ColumnName.Rate).ToString.Trim
          .sl_sykd_rate = CInt(strRateTemp.Substring(0, strRateTemp.Length - 1)) 'Get Tax rate, remove "%" 
        End If
        SaveToClass(ColumnName.UserLotNo, intCount, .sl_zdn_ulotno)
        SaveToClass(ColumnName.InvID, intCount, .sl_zdn_id)
        SaveToClass(ColumnName.ConvertFlag, intCount, .sl_sykd_convert)

      End With
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  Private Function SetToZDN(ByVal details As SL_SYKD(), ByVal header As SL_SYKH, ByVal intCount As Integer) As Boolean
    Dim intCoefficient As Integer
    Try
      With SL_ZDNClass
        If header.sl_sykh_return = SLConstants.ShipmentKbn.RETURN_GOODS_CODE Then
          intCoefficient = -1
        Else
          intCoefficient = 1
        End If
        .sl_zdn_id = CInt(tblTable.GetCellValue(intCount, ColumnName.InvID))
        .sl_zdn_relay_zdnid = CInt(tblTable.GetCellValue(intCount, ColumnName.PrevInvID))
        .sl_zdn_honsu = details(intCount).sl_sykd_honsu * intCoefficient
        .sl_zdn_suryo = CDec(details(intCount).sl_sykd_suryo) * intCoefficient
        .sl_zdn_nyukohonsu = details(intCount).sl_sykd_honsu * intCoefficient
        .sl_zdn_nyukosuryo = CDec(details(intCount).sl_sykd_suryo) * intCoefficient

      End With
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try
  End Function

  ''' <summary>
  ''' temporary function -> when PCAIF code change, rewrite this function
  ''' </summary>
  ''' <param name="_sl_zhk"></param>
  ''' <param name="intRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SetToZHK(ByRef _sl_zhk As SL_ZHK, ByVal _header As SL_SYKH, ByVal _detail As SL_SYKD, ByVal intRow As Integer, Optional ByVal isDelete As Boolean = False) As Boolean
    Dim is_success As Boolean = True
    Dim intCoefficient As Integer

    Try
      If _header.sl_sykh_return = SLConstants.ShipmentKbn.RETURN_GOODS_CODE Then
        intCoefficient = 1
      Else
        intCoefficient = -1
      End If
      With _sl_zhk
        .sl_zhk_datakbn = SLConstants.SL_ZHKSlipType.Shipping
        If isDelete Then
          .sl_zhk_zdn_id = CInt(tblTable.GetCellValue(intRow, ColumnName.PrevInvID))
          .sl_zhk_dataid = CInt(tblTable.GetCellValue(intRow, ColumnName.PrevID))
        Else
          .sl_zhk_zdn_id = CInt(tblTable.GetCellValue(intRow, ColumnName.InvID))
          .sl_zhk_dataid = _detail.sl_sykd_id
        End If
        .sl_zhk_seq = 1  'only 1 lot of each row
        .sl_zhk_date = _header.sl_sykh_uribi

        .sl_zhk_honsu = _detail.sl_sykd_honsu
        .sl_zhk_suryo = CDec(_detail.sl_sykd_suryo)

        .sl_zhk_insdate = Now
        .sl_zhk_insuser = connector.UserId
        .sl_zhk_upddate = Now
        .sl_zhk_upduser = connector.UserId
        .intRowCount = intRow
      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      is_success = False
    End Try
    Return is_success
  End Function

  ''' <summary>
  ''' If cell is not empty, save cell text value to table
  ''' </summary>
  ''' <param name="strColumnName">cell column name</param>
  ''' <param name="intCount">row index</param>
  ''' <param name="strTableDetailsName">table name</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SaveToClass(strColumnName As String, intCount As Integer, ByRef strTableDetailsName As String) As Boolean
    If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, strColumnName)) Then
      strTableDetailsName = tblTable.GetCellValue(intCount, strColumnName).ToString.Trim
    End If
    Return True
  End Function

  ''' <summary>
  ''' If cell is not empty, save cell number value to table
  ''' </summary>
  ''' <param name="strColumnName">cell column name</param>
  ''' <param name="intCount">row index</param>
  ''' <param name="strTableDetailsName">table name</param>
  ''' <returns></returns>
  ''' <remarks>2015/11/21  by Lai Minh Hien</remarks>
  Private Function SaveToClass(strColumnName As String, intCount As Integer, ByRef strTableDetailsName As Integer) As Boolean
    If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, strColumnName)) Then
      strTableDetailsName = CInt(tblTable.GetCellValue(intCount, strColumnName).ToString)
    End If
    Return True
  End Function

  ''' <summary>
  ''' If cell is not empty, save cell number value to table
  ''' </summary>
  ''' <param name="strColumnName">cell column name</param>
  ''' <param name="intCount">row index</param>
  ''' <param name="strTableDetailsName">table name</param>
  ''' <returns></returns>
  ''' <remarks>2015/11/21  by Lai Minh Hien</remarks>
  Private Function SaveToClass(strColumnName As String, intCount As Integer, ByRef strTableDetailsName As Decimal) As Boolean
    If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, strColumnName)) Then
      strTableDetailsName = CDec(tblTable.GetCellValue(intCount, strColumnName).ToString)
    End If
    Return True
  End Function

  Private Function SaveToClass(strColumnName As String, intCount As Integer, ByRef strTableDetailsName As Double) As Boolean
    If Not SLCmnFunction.checkObjectNothingEmpty(tblTable.GetCellValue(intCount, strColumnName)) Then
      strTableDetailsName = CDec(tblTable.GetCellValue(intCount, strColumnName).ToString)
    End If
    Return True
  End Function

#End Region

  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
    '
    Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
    Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子

    If (keyModifiers <> Keys.Shift AndAlso (keyCode = Keys.Tab OrElse keyCode = Keys.Enter)) _
    OrElse keyCode = Keys.Right _
    OrElse keyCode = Keys.Down Then
      'Validate if user press Enter/Tab +  Anykey except Shift, →, ↓
      Me.IsValidate = True
    ElseIf (keyModifiers = Keys.Shift AndAlso (keyCode = Keys.Tab OrElse keyCode = Keys.Enter)) _
    OrElse keyCode = Keys.Left _
    OrElse keyCode = Keys.Up Then
      'Not validate if user press Shift + Tab, Shift + Enter, ←, ↑
      Me.IsValidate = False
    End If

    Return MyBase.ProcessDialogKey(keyData)

  End Function

End Class

Friend Class ColumnName
  Public Const No As String = ""
  Public Const Type As String = "区分"
  Public Const ProductCode As String = MeisaiTableDefines.SyohinCode 'Set the same as table column name in library
  Public Const ProductCodeDisp As String = "商品コード" 'For display header column
  Public Shared Property Color As String = "色"
  Public Shared Property Size As String = "サイズ"
  Public Const Warehouse As String = MeisaiTableDefines.SokoCode
  Public Const Warehouse2 As String = "倉庫"
  Public Const ProductName As String = "商品名"
  Public Shared Property Spec As String = "規格・型番"
  Public Const TaxIncluded As String = "税区分"
  Public Const Rate As String = "税率"
  Public Const Number As String = "本数"
  Public Shared Property QtyPerCase As String = "入数"
  Public Shared Property NoOfCase As String = "箱数"
  Public Const Qty As String = "数量"
  Public Const UnitName As String = "単位"

  Public Const UnitPriceG As String = "原単価"
  Public Const UnitPrice As String = "単価"
  Public Const AmountG As String = "原価金額"
  Public Const Amount As String = "金額"
  Public Const Note As String = "備考"
  Public Const Arari As String = "粗利"
  Public Const UserLotNo As String = "ロットNo"

  Public Const QtyNoOfDec As String = "数量小数桁数"
  Public Const UnitPriceNoOfDec As String = "単価小数桁数"
  Public Const InvID As String = "在庫ID"
  Public Const PrevInvID As String = "編集前在庫ID"
  Public Const PrevNumber As String = "編集前本数"
  Public Const PrevDecQty As String = "編集前数量"
  Public Const PrevID As String = "編集前明細ID"
  Public Const IsUseLot As String = "ロット管理フラグ"
  Public Const ConvertFlag As String = "振替フラグ"

End Class
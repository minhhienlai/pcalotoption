﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
    Me.ServiceController1 = New System.ServiceProcess.ServiceController()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.FToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSave = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemNew = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemReset = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPrevious = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemNext = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRefer = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemDelete = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemCopy = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRowInsert = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRowDelete = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPrevious = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonNext = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonCreate = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonReset = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonDelete = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonCopy = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSave = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonInsertRow = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonDeleteRow = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.HeaderLabel = New PCA.TSC.Kon.Tools.TscInputHeaderLabel()
    Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
    Me.setType = New PCA.Controls.PcaCodeSet()
    Me.ldtShipmentDate = New Sunloft.PCAControls.SLPcaLabeledDate()
    Me.setSupCode = New PCA.Controls.PcaCodeSet()
    Me.ltxtWhNo2 = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.lblSupName = New PCA.TSC.Kon.Tools.TscLabel(Me.components)
    Me.lblSupTel = New PCA.TSC.Kon.Tools.TscLabel(Me.components)
    Me.lblSupFax = New PCA.TSC.Kon.Tools.TscLabel(Me.components)
    Me.lblSupTax = New PCA.TSC.Kon.Tools.TscLabel(Me.components)
    Me.setPerson = New PCA.Controls.PcaCodeSet()
    Me.setDepartment = New PCA.Controls.PcaCodeSet()
    Me.setMemoCode = New PCA.Controls.PcaCodeSet()
    Me.setMemoContent = New PCA.Controls.PcaCodeSet()
    Me.setProject = New PCA.Controls.PcaCodeSet()
    Me.ltxtCreatedBy = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtCreatedAt = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtModifiedBy = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.ltxtModifiedAt = New Sunloft.PCAControls.SLPcaLabeledTextBox()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrevious = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandNext = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSlipSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandCopy = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSave = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandInv = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemSave = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemNew = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemReset = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPrevious = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemNext = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSlipSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemDelete = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemCopy = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemInsRow = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemDelRow = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemInv = New PCA.Controls.PcaCommandItem()
    Me.ToolStripMenuItemInsertRow = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemDeleteRow = New System.Windows.Forms.ToolStripMenuItem()
    Me.tblTable = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.ContextMenuStripTable = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.lblAddr = New PCA.TSC.Kon.Tools.TscLabel(Me.components)
    Me.setOrderNo = New PCA.Controls.PcaCodeSet()
    Me.setPurNo = New PCA.Controls.PcaCodeSet()
    Me.ltxtWhNo = New PCA.Controls.PcaLabeledNumberBox()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.ldtDlvDate = New Sunloft.PCAControls.SLPcaLabeledDate()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.ContextMenuStripTable.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'FToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.FToolStripMenuItemFile, Nothing)
    Me.FToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSave, Me.ToolStripMenuItemClose})
    Me.FToolStripMenuItemFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.FToolStripMenuItemFile.Name = "FToolStripMenuItemFile"
    Me.FToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.FToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSave, Me.PcaCommandItemSave)
    Me.ToolStripMenuItemSave.Name = "ToolStripMenuItemSave"
    Me.ToolStripMenuItemSave.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSave.Text = "登録(&S)"
    Me.ToolStripMenuItemSave.ToolTipText = "伝票を登録します。"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    Me.ToolStripMenuItemClose.ToolTipText = "プログラムを終了します。"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemNew, Me.ToolStripMenuItemReset, Me.ToolStripMenuItemPrevious, Me.ToolStripMenuItemNext, Me.ToolStripMenuItemSearch, Me.ToolStripMenuItemRefer, Me.ToolStripMenuItemDelete, Me.ToolStripMenuItemCopy, Me.ToolStripMenuItemRowInsert, Me.ToolStripMenuItemRowDelete})
    Me.ToolStripMenuItemEdit.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemNew
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemNew, Me.PcaCommandItemNew)
    Me.ToolStripMenuItemNew.Name = "ToolStripMenuItemNew"
    Me.ToolStripMenuItemNew.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemNew.Text = "新規入力(&N)"
    Me.ToolStripMenuItemNew.ToolTipText = "伝票の新規作成を行います。"
    '
    'ToolStripMenuItemReset
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemReset, Me.PcaCommandItemReset)
    Me.ToolStripMenuItemReset.Name = "ToolStripMenuItemReset"
    Me.ToolStripMenuItemReset.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemReset.Text = "入力前に戻る(&O)"
    Me.ToolStripMenuItemReset.ToolTipText = "伝票を入力前の状態に戻します。"
    '
    'ToolStripMenuItemPrevious
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPrevious, Me.PcaCommandItemPrevious)
    Me.ToolStripMenuItemPrevious.Name = "ToolStripMenuItemPrevious"
    Me.ToolStripMenuItemPrevious.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemPrevious.Text = "前伝票(&B)"
    Me.ToolStripMenuItemPrevious.ToolTipText = "前の伝票を表示します。"
    '
    'ToolStripMenuItemNext
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemNext, Me.PcaCommandItemNext)
    Me.ToolStripMenuItemNext.Name = "ToolStripMenuItemNext"
    Me.ToolStripMenuItemNext.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemNext.Text = "次伝票(&F)"
    Me.ToolStripMenuItemNext.ToolTipText = "次の伝票を表示します。"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSlipSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemSearch.Text = "検索(&S)"
    Me.ToolStripMenuItemSearch.ToolTipText = "伝票を検索します。"
    '
    'ToolStripMenuItemRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRefer, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemRefer.Name = "ToolStripMenuItemRefer"
    Me.ToolStripMenuItemRefer.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemRefer.Text = "参照(&U)"
    Me.ToolStripMenuItemRefer.ToolTipText = "マスタの検索を行います。"
    '
    'ToolStripMenuItemDelete
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemDelete, Me.PcaCommandItemDelete)
    Me.ToolStripMenuItemDelete.Name = "ToolStripMenuItemDelete"
    Me.ToolStripMenuItemDelete.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemDelete.Text = "伝票削除(&X)"
    Me.ToolStripMenuItemDelete.ToolTipText = "伝票の削除を行います。"
    '
    'ToolStripMenuItemCopy
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemCopy, Me.PcaCommandItemCopy)
    Me.ToolStripMenuItemCopy.Name = "ToolStripMenuItemCopy"
    Me.ToolStripMenuItemCopy.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemCopy.Text = "伝票複写(&Y)"
    Me.ToolStripMenuItemCopy.ToolTipText = "伝票の複写を行います。"
    '
    'ToolStripMenuItemRowInsert
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRowInsert, Me.PcaCommandItemInsRow)
    Me.ToolStripMenuItemRowInsert.Name = "ToolStripMenuItemRowInsert"
    Me.ToolStripMenuItemRowInsert.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemRowInsert.Text = "行追加(&I)"
    '
    'ToolStripMenuItemRowDelete
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRowDelete, Me.PcaCommandItemDelRow)
    Me.ToolStripMenuItemRowDelete.Name = "ToolStripMenuItemRowDelete"
    Me.ToolStripMenuItemRowDelete.Size = New System.Drawing.Size(179, 22)
    Me.ToolStripMenuItemRowDelete.Text = "行削除(&D)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContent.Text = "目次(&C)"
    Me.ToolStripMenuItemContent.ToolTipText = "ヘルプを表示します。"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonPrevious, Me.ToolStripButtonNext, Me.ToolStripButtonCreate, Me.ToolStripButtonReset, Me.ToolStripButtonDelete, Me.ToolStripButtonCopy, Me.ToolStripButtonSave, Me.ToolStripButtonInsertRow, Me.ToolStripButtonDeleteRow, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1008, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonClose.ToolTipText = "プログラムを終了します。"
    '
    'ToolStripButtonPrevious
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPrevious, Me.PcaCommandItemPrevious)
    Me.ToolStripButtonPrevious.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonPrevious.Image = CType(resources.GetObject("ToolStripButtonPrevious.Image"), System.Drawing.Image)
    Me.ToolStripButtonPrevious.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPrevious.Name = "ToolStripButtonPrevious"
    Me.ToolStripButtonPrevious.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonPrevious.Text = "前伝票"
    Me.ToolStripButtonPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonPrevious.ToolTipText = "前伝票を表示します。"
    '
    'ToolStripButtonNext
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonNext, Me.PcaCommandItemNext)
    Me.ToolStripButtonNext.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonNext.Image = CType(resources.GetObject("ToolStripButtonNext.Image"), System.Drawing.Image)
    Me.ToolStripButtonNext.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonNext.Name = "ToolStripButtonNext"
    Me.ToolStripButtonNext.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonNext.Text = "次伝票"
    Me.ToolStripButtonNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonNext.ToolTipText = "次伝票を表示します。"
    '
    'ToolStripButtonCreate
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonCreate, Me.PcaCommandItemNew)
    Me.ToolStripButtonCreate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonCreate.Image = CType(resources.GetObject("ToolStripButtonCreate.Image"), System.Drawing.Image)
    Me.ToolStripButtonCreate.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonCreate.Name = "ToolStripButtonCreate"
    Me.ToolStripButtonCreate.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonCreate.Text = "新規"
    Me.ToolStripButtonCreate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonCreate.ToolTipText = "伝票を新規で作成します。"
    '
    'ToolStripButtonReset
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonReset, Me.PcaCommandItemReset)
    Me.ToolStripButtonReset.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonReset.Image = CType(resources.GetObject("ToolStripButtonReset.Image"), System.Drawing.Image)
    Me.ToolStripButtonReset.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonReset.Name = "ToolStripButtonReset"
    Me.ToolStripButtonReset.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonReset.Text = "入力前"
    Me.ToolStripButtonReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonReset.ToolTipText = "入力前の状態に戻します。"
    '
    'ToolStripButtonDelete
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonDelete, Me.PcaCommandItemDelete)
    Me.ToolStripButtonDelete.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonDelete.Image = CType(resources.GetObject("ToolStripButtonDelete.Image"), System.Drawing.Image)
    Me.ToolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonDelete.Name = "ToolStripButtonDelete"
    Me.ToolStripButtonDelete.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonDelete.Text = "削除"
    Me.ToolStripButtonDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonDelete.ToolTipText = "伝票を削除します。"
    '
    'ToolStripButtonCopy
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonCopy, Me.PcaCommandItemCopy)
    Me.ToolStripButtonCopy.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonCopy.Image = CType(resources.GetObject("ToolStripButtonCopy.Image"), System.Drawing.Image)
    Me.ToolStripButtonCopy.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonCopy.Name = "ToolStripButtonCopy"
    Me.ToolStripButtonCopy.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonCopy.Text = "複写"
    Me.ToolStripButtonCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonCopy.ToolTipText = "伝票を複写します。"
    '
    'ToolStripButtonSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSave, Me.PcaCommandItemSave)
    Me.ToolStripButtonSave.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonSave.Image = CType(resources.GetObject("ToolStripButtonSave.Image"), System.Drawing.Image)
    Me.ToolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSave.Name = "ToolStripButtonSave"
    Me.ToolStripButtonSave.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonSave.Text = "登録"
    Me.ToolStripButtonSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonSave.ToolTipText = "伝票を登録します。"
    '
    'ToolStripButtonInsertRow
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonInsertRow, Me.PcaCommandItemInsRow)
    Me.ToolStripButtonInsertRow.Image = CType(resources.GetObject("ToolStripButtonInsertRow.Image"), System.Drawing.Image)
    Me.ToolStripButtonInsertRow.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonInsertRow.Name = "ToolStripButtonInsertRow"
    Me.ToolStripButtonInsertRow.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonInsertRow.Text = "行挿入"
    Me.ToolStripButtonInsertRow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonDeleteRow
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonDeleteRow, Me.PcaCommandItemDelRow)
    Me.ToolStripButtonDeleteRow.Image = CType(resources.GetObject("ToolStripButtonDeleteRow.Image"), System.Drawing.Image)
    Me.ToolStripButtonDeleteRow.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonDeleteRow.Name = "ToolStripButtonDeleteRow"
    Me.ToolStripButtonDeleteRow.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonDeleteRow.Text = "行削除"
    Me.ToolStripButtonDeleteRow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"), System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonHelp.ToolTipText = "ヘルプを表示します。"
    '
    'HeaderLabel
    '
    Me.HeaderLabel.BackColor = System.Drawing.SystemColors.Control
    Me.HeaderLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.HeaderLabel.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Bold)
    Me.HeaderLabel.FusenFlag = PCA.TSC.Kon.BusinessEntity.Defines.DenpyoFusenCommentType.Nashi
    Me.HeaderLabel.FusenText = ""
    Me.HeaderLabel.Location = New System.Drawing.Point(0, 61)
    Me.HeaderLabel.Name = "HeaderLabel"
    Me.HeaderLabel.ShowFusenIcon = True
    Me.HeaderLabel.ShowSyoninIcon = True
    Me.HeaderLabel.Size = New System.Drawing.Size(1008, 25)
    Me.HeaderLabel.State = PCA.Controls.LabelStateType.[New]
    Me.HeaderLabel.SyoninFlag = PCA.TSC.Kon.BusinessEntity.Defines.DenpyoSyoninFlagType.Misyonin
    Me.HeaderLabel.SyoninText = ""
    Me.HeaderLabel.TabIndex = 2
    Me.HeaderLabel.TabStop = False
    '
    'setType
    '
    Me.setType.AllowCodeChars = New Char() {Global.Microsoft.VisualBasic.ChrW(48), Global.Microsoft.VisualBasic.ChrW(49)}
    Me.setType.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Number
    Me.setType.AutoRestore = True
    Me.setType.AutoTextSize = False
    Me.setType.AutoTopMargin = False
    Me.setType.ClientProcess = Nothing
    Me.setType.CodeText = "0"
    Me.setType.CodeTextAlignment = System.Windows.Forms.HorizontalAlignment.Center
    Me.setType.DenyCodeChars = New Char() {Global.Microsoft.VisualBasic.ChrW(52), Global.Microsoft.VisualBasic.ChrW(55), Global.Microsoft.VisualBasic.ChrW(56), Global.Microsoft.VisualBasic.ChrW(57), Global.Microsoft.VisualBasic.ChrW(50), Global.Microsoft.VisualBasic.ChrW(51), Global.Microsoft.VisualBasic.ChrW(53), Global.Microsoft.VisualBasic.ChrW(54), Global.Microsoft.VisualBasic.ChrW(0)}
    Me.setType.F8EventSupport = False
    Me.setType.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setType.GroupBoxVisible = False
    Me.setType.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.setType.LabelTopAlign = True
    Me.setType.LimitLength = 1
    Me.setType.LimitUnicodeLength = 1
    Me.setType.Location = New System.Drawing.Point(12, 92)
    Me.setType.MaxCodeLength = 3
    Me.setType.MaxNameLength = 10
    Me.setType.Name = "setType"
    Me.setType.NameText = "通常出荷"
    Me.setType.NameTextAlignment = PCA.Controls.CodeSetTextAlignment.Center
    Me.setType.ReferButtonVisible = False
    Me.setType.Size = New System.Drawing.Size(93, 44)
    Me.setType.TabIndex = 0
    Me.setType.Text = "出荷区分"
    Me.setType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'ldtShipmentDate
    '
    Me.ldtShipmentDate.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.ldtShipmentDate.ClientProcess = Nothing
    Me.ldtShipmentDate.DisplaySlash = True
    Me.ldtShipmentDate.EmptyIfDisabled = True
    Me.ldtShipmentDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.ldtShipmentDate.Holidays = Nothing
    Me.ldtShipmentDate.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.ldtShipmentDate.LabelText = "出荷日"
    Me.ldtShipmentDate.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtShipmentDate.LabelTopAlign = True
    Me.ldtShipmentDate.Location = New System.Drawing.Point(128, 92)
    Me.ldtShipmentDate.MaxLabelLength = 15
    Me.ldtShipmentDate.MaxTextLength = 15
    Me.ldtShipmentDate.MustInput = False
    Me.ldtShipmentDate.Name = "ldtShipmentDate"
    Me.ldtShipmentDate.Size = New System.Drawing.Size(105, 44)
    Me.ldtShipmentDate.TabIndex = 1
    '
    'setSupCode
    '
    Me.setSupCode.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setSupCode.AutoTopMargin = False
    Me.setSupCode.ClientProcess = Nothing
    Me.setSupCode.FillZeroFlag = True
    Me.setSupCode.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setSupCode.GroupBoxVisible = False
    Me.setSupCode.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setSupCode.LabelTopAlign = True
    Me.setSupCode.Location = New System.Drawing.Point(493, 92)
    Me.setSupCode.Margin = New System.Windows.Forms.Padding(18, 0, 0, 0)
    Me.setSupCode.MaxCodeLength = 15
    Me.setSupCode.MaxNameLength = 0
    Me.setSupCode.Name = "setSupCode"
    Me.setSupCode.Size = New System.Drawing.Size(126, 44)
    Me.setSupCode.TabIndex = 4
    Me.setSupCode.Text = "出荷先コード"
    Me.setSupCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.setSupCode.TrimEndMode = True
    Me.setSupCode.TrimStartMode = True
    '
    'ltxtWhNo2
    '
    Me.ltxtWhNo2.AllowTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.ltxtWhNo2.AutoTextSize = False
    Me.ltxtWhNo2.ClientProcess = Nothing
    Me.ltxtWhNo2.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.ltxtWhNo2.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.ltxtWhNo2.LabelText = "出荷No2"
    Me.ltxtWhNo2.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtWhNo2.LabelTopAlign = True
    Me.ltxtWhNo2.Location = New System.Drawing.Point(876, 92)
    Me.ltxtWhNo2.Margin = New System.Windows.Forms.Padding(21, 0, 0, 0)
    Me.ltxtWhNo2.MaxLabelLength = 32
    Me.ltxtWhNo2.MaxLength = 32
    Me.ltxtWhNo2.MaxTextLength = 32
    Me.ltxtWhNo2.MustInput = False
    Me.ltxtWhNo2.Name = "ltxtWhNo2"
    Me.ltxtWhNo2.Size = New System.Drawing.Size(120, 44)
    Me.ltxtWhNo2.TabIndex = 7
    Me.ltxtWhNo2.TabStop = False
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 708)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1008, 22)
    Me.StatusStrip1.TabIndex = 25
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'lblSupName
    '
    Me.lblSupName.AutoTextSize = False
    Me.lblSupName.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.lblSupName.Location = New System.Drawing.Point(12, 158)
    Me.lblSupName.MaxLine = 2
    Me.lblSupName.MaxTextLength = 50
    Me.lblSupName.Name = "lblSupName"
    Me.lblSupName.Size = New System.Drawing.Size(448, 40)
    Me.lblSupName.TabIndex = 21
    Me.lblSupName.TextAlign = System.Drawing.ContentAlignment.BottomLeft
    Me.lblSupName.TextBoxBorder = True
    '
    'lblSupTel
    '
    Me.lblSupTel.AutoTextSize = False
    Me.lblSupTel.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.lblSupTel.InvisibleBorderBottom = True
    Me.lblSupTel.InvisibleBorderRight = True
    Me.lblSupTel.Location = New System.Drawing.Point(12, 242)
    Me.lblSupTel.MaxTextLength = 25
    Me.lblSupTel.Name = "lblSupTel"
    Me.lblSupTel.Size = New System.Drawing.Size(200, 40)
    Me.lblSupTel.TabIndex = 23
    Me.lblSupTel.Text = "TEL："
    Me.lblSupTel.TextBoxBorder = True
    '
    'lblSupFax
    '
    Me.lblSupFax.AutoTextSize = False
    Me.lblSupFax.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.lblSupFax.InvisibleBorderBottom = True
    Me.lblSupFax.InvisibleBorderLeft = True
    Me.lblSupFax.Location = New System.Drawing.Point(213, 242)
    Me.lblSupFax.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.lblSupFax.MaxTextLength = 25
    Me.lblSupFax.Name = "lblSupFax"
    Me.lblSupFax.Size = New System.Drawing.Size(247, 40)
    Me.lblSupFax.TabIndex = 23
    Me.lblSupFax.Text = "FAX："
    Me.lblSupFax.TextBoxBorder = True
    '
    'lblSupTax
    '
    Me.lblSupTax.AutoTextSize = False
    Me.lblSupTax.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.lblSupTax.Location = New System.Drawing.Point(12, 282)
    Me.lblSupTax.MaxTextLength = 25
    Me.lblSupTax.Name = "lblSupTax"
    Me.lblSupTax.Size = New System.Drawing.Size(448, 42)
    Me.lblSupTax.TabIndex = 24
    Me.lblSupTax.Text = "消費税："
    Me.lblSupTax.TextBoxBorder = True
    '
    'setPerson
    '
    Me.setPerson.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setPerson.AutoRestore = True
    Me.setPerson.AutoTextSize = False
    Me.setPerson.AutoTopMargin = False
    Me.setPerson.ClientProcess = Nothing
    Me.setPerson.FillZeroFlag = True
    Me.setPerson.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setPerson.GroupBoxVisible = False
    Me.setPerson.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setPerson.Location = New System.Drawing.Point(515, 158)
    Me.setPerson.MaxCodeLength = 18
    Me.setPerson.MaxLabelLength = 15
    Me.setPerson.MaxNameLength = 30
    Me.setPerson.Name = "setPerson"
    Me.setPerson.Size = New System.Drawing.Size(484, 21)
    Me.setPerson.TabIndex = 8
    Me.setPerson.Text = "担当者"
    '
    'setDepartment
    '
    Me.setDepartment.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setDepartment.AutoRestore = True
    Me.setDepartment.AutoTextSize = False
    Me.setDepartment.AutoTopMargin = False
    Me.setDepartment.ClientProcess = Nothing
    Me.setDepartment.FillZeroFlag = True
    Me.setDepartment.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setDepartment.GroupBoxVisible = False
    Me.setDepartment.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setDepartment.Location = New System.Drawing.Point(515, 180)
    Me.setDepartment.MaxCodeLength = 18
    Me.setDepartment.MaxLabelLength = 15
    Me.setDepartment.MaxNameLength = 30
    Me.setDepartment.Name = "setDepartment"
    Me.setDepartment.Size = New System.Drawing.Size(466, 21)
    Me.setDepartment.TabIndex = 9
    Me.setDepartment.Text = "部門"
    '
    'setMemoCode
    '
    Me.setMemoCode.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setMemoCode.AutoRestore = True
    Me.setMemoCode.AutoTextSize = False
    Me.setMemoCode.AutoTopMargin = False
    Me.setMemoCode.ClientProcess = Nothing
    Me.setMemoCode.FillZeroFlag = True
    Me.setMemoCode.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setMemoCode.GroupBoxVisible = False
    Me.setMemoCode.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setMemoCode.Location = New System.Drawing.Point(515, 201)
    Me.setMemoCode.MaxCodeLength = 18
    Me.setMemoCode.MaxLabelLength = 15
    Me.setMemoCode.MaxNameLength = 0
    Me.setMemoCode.Name = "setMemoCode"
    Me.setMemoCode.Size = New System.Drawing.Size(252, 21)
    Me.setMemoCode.TabIndex = 10
    Me.setMemoCode.Text = "摘要"
    '
    'setMemoContent
    '
    Me.setMemoContent.AutoTextSize = False
    Me.setMemoContent.AutoTopMargin = False
    Me.setMemoContent.BackColor = System.Drawing.SystemColors.Control
    Me.setMemoContent.ClientProcess = Nothing
    Me.setMemoContent.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setMemoContent.GroupBoxVisible = False
    Me.setMemoContent.ImeMode = System.Windows.Forms.ImeMode.Hiragana
    Me.setMemoContent.LimitLength = 30
    Me.setMemoContent.Location = New System.Drawing.Point(764, 201)
    Me.setMemoContent.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.setMemoContent.MaxCodeLength = 30
    Me.setMemoContent.MaxLabelLength = 0
    Me.setMemoContent.MaxNameLength = 0
    Me.setMemoContent.Name = "setMemoContent"
    Me.setMemoContent.ReferButtonVisible = False
    Me.setMemoContent.Size = New System.Drawing.Size(217, 21)
    Me.setMemoContent.TabIndex = 11
    '
    'setProject
    '
    Me.setProject.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setProject.AutoRestore = True
    Me.setProject.AutoTextSize = False
    Me.setProject.AutoTopMargin = False
    Me.setProject.ClientProcess = Nothing
    Me.setProject.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setProject.GroupBoxVisible = False
    Me.setProject.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setProject.LimitLength = 15
    Me.setProject.Location = New System.Drawing.Point(515, 223)
    Me.setProject.MaxCodeLength = 18
    Me.setProject.MaxLabelLength = 15
    Me.setProject.MaxNameLength = 30
    Me.setProject.Name = "setProject"
    Me.setProject.Size = New System.Drawing.Size(466, 22)
    Me.setProject.TabIndex = 12
    Me.setProject.Text = "プロジェクト"
    '
    'ltxtCreatedBy
    '
    Me.ltxtCreatedBy.AutoTextSize = False
    Me.ltxtCreatedBy.ClientProcess = Nothing
    Me.ltxtCreatedBy.Enabled = False
    Me.ltxtCreatedBy.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtCreatedBy.LabelText = "登録者"
    Me.ltxtCreatedBy.Location = New System.Drawing.Point(515, 263)
    Me.ltxtCreatedBy.MaxLabelLength = 15
    Me.ltxtCreatedBy.MaxTextLength = 25
    Me.ltxtCreatedBy.MustInput = False
    Me.ltxtCreatedBy.Name = "ltxtCreatedBy"
    Me.ltxtCreatedBy.ReadOnly = True
    Me.ltxtCreatedBy.Size = New System.Drawing.Size(280, 21)
    Me.ltxtCreatedBy.TabIndex = 42
    Me.ltxtCreatedBy.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtCreatedAt
    '
    Me.ltxtCreatedAt.AutoTextSize = False
    Me.ltxtCreatedAt.ClientProcess = Nothing
    Me.ltxtCreatedAt.Enabled = False
    Me.ltxtCreatedAt.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtCreatedAt.LabelText = "登録日時"
    Me.ltxtCreatedAt.Location = New System.Drawing.Point(515, 283)
    Me.ltxtCreatedAt.MaxLabelLength = 15
    Me.ltxtCreatedAt.MaxTextLength = 25
    Me.ltxtCreatedAt.MustInput = False
    Me.ltxtCreatedAt.Name = "ltxtCreatedAt"
    Me.ltxtCreatedAt.ReadOnly = True
    Me.ltxtCreatedAt.Size = New System.Drawing.Size(280, 21)
    Me.ltxtCreatedAt.TabIndex = 42
    Me.ltxtCreatedAt.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtModifiedBy
    '
    Me.ltxtModifiedBy.AutoTextSize = False
    Me.ltxtModifiedBy.ClientProcess = Nothing
    Me.ltxtModifiedBy.Enabled = False
    Me.ltxtModifiedBy.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtModifiedBy.LabelText = "更新者"
    Me.ltxtModifiedBy.Location = New System.Drawing.Point(515, 303)
    Me.ltxtModifiedBy.MaxLabelLength = 15
    Me.ltxtModifiedBy.MaxTextLength = 25
    Me.ltxtModifiedBy.MustInput = False
    Me.ltxtModifiedBy.Name = "ltxtModifiedBy"
    Me.ltxtModifiedBy.ReadOnly = True
    Me.ltxtModifiedBy.Size = New System.Drawing.Size(280, 21)
    Me.ltxtModifiedBy.TabIndex = 42
    Me.ltxtModifiedBy.TextBackColor = System.Drawing.SystemColors.Control
    '
    'ltxtModifiedAt
    '
    Me.ltxtModifiedAt.AutoTextSize = False
    Me.ltxtModifiedAt.ClientProcess = Nothing
    Me.ltxtModifiedAt.Enabled = False
    Me.ltxtModifiedAt.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ltxtModifiedAt.LabelText = "更新日時"
    Me.ltxtModifiedAt.Location = New System.Drawing.Point(515, 323)
    Me.ltxtModifiedAt.MaxLabelLength = 15
    Me.ltxtModifiedAt.MaxTextLength = 25
    Me.ltxtModifiedAt.MustInput = False
    Me.ltxtModifiedAt.Name = "ltxtModifiedAt"
    Me.ltxtModifiedAt.ReadOnly = True
    Me.ltxtModifiedAt.Size = New System.Drawing.Size(280, 25)
    Me.ltxtModifiedAt.TabIndex = 42
    Me.ltxtModifiedAt.TextBackColor = System.Drawing.SystemColors.Control
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandPrevious, Me.PcaFunctionCommandNext, Me.PcaFunctionCommandSlipSearch, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandCopy, Me.PcaFunctionCommandClose, Me.PcaFunctionCommandSave, Me.PcaFunctionCommandInv})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 680)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1008, 28)
    Me.PcaFunctionBar1.TabIndex = 44
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    Me.PcaFunctionCommandHelp.ToolTipText = "ヘルプを参照します。"
    '
    'PcaFunctionCommandPrevious
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrevious, Me.PcaCommandItemPrevious)
    Me.PcaFunctionCommandPrevious.FunctionKey = PCA.Controls.FunctionKey.F2
    Me.PcaFunctionCommandPrevious.Tag = Nothing
    Me.PcaFunctionCommandPrevious.Text = "前伝票"
    Me.PcaFunctionCommandPrevious.ToolTipText = "前伝票を表示します。"
    '
    'PcaFunctionCommandNext
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandNext, Me.PcaCommandItemNext)
    Me.PcaFunctionCommandNext.FunctionKey = PCA.Controls.FunctionKey.F3
    Me.PcaFunctionCommandNext.Tag = Nothing
    Me.PcaFunctionCommandNext.Text = "次伝票"
    Me.PcaFunctionCommandNext.ToolTipText = "次伝票を表示します。"
    '
    'PcaFunctionCommandSlipSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSlipSearch, Me.PcaCommandItemSlipSearch)
    Me.PcaFunctionCommandSlipSearch.FunctionKey = PCA.Controls.FunctionKey.F6
    Me.PcaFunctionCommandSlipSearch.Tag = Nothing
    Me.PcaFunctionCommandSlipSearch.Text = "検索"
    Me.PcaFunctionCommandSlipSearch.ToolTipText = "伝票を検索します。"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    Me.PcaFunctionCommandRefer.ToolTipText = "マスタの検索を行います。"
    '
    'PcaFunctionCommandCopy
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandCopy, Me.PcaCommandItemCopy)
    Me.PcaFunctionCommandCopy.FunctionKey = PCA.Controls.FunctionKey.F11
    Me.PcaFunctionCommandCopy.Tag = Nothing
    Me.PcaFunctionCommandCopy.Text = "伝票複写"
    Me.PcaFunctionCommandCopy.ToolTipText = "伝票を複写します。"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    Me.PcaFunctionCommandClose.ToolTipText = "プログラムを終了します。"
    '
    'PcaFunctionCommandSave
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSave, Me.PcaCommandItemSave)
    Me.PcaFunctionCommandSave.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandSave.Tag = Nothing
    Me.PcaFunctionCommandSave.Text = "登録"
    Me.PcaFunctionCommandSave.ToolTipText = "伝票を登録します。"
    '
    'PcaFunctionCommandInv
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandInv, Me.PcaCommandItemInv)
    Me.PcaFunctionCommandInv.FunctionKey = PCA.Controls.FunctionKey.F7
    Me.PcaFunctionCommandInv.Tag = Nothing
    Me.PcaFunctionCommandInv.Text = "在庫検索"
    Me.PcaFunctionCommandInv.ToolTipText = "在庫検索画面を開きます。"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemPrevious, Me.PcaCommandItemNext, Me.PcaCommandItemSlipSearch, Me.PcaCommandItemRefer, Me.PcaCommandItemCopy, Me.PcaCommandItemSave, Me.PcaCommandItemClose, Me.PcaCommandItemNew, Me.PcaCommandItemDelete, Me.PcaCommandItemReset, Me.PcaCommandItemInv, Me.PcaCommandItemDelRow, Me.PcaCommandItemInsRow})
    Me.PcaCommandManager1.Parent = Me
    '
    'PcaCommandItemSave
    '
    Me.PcaCommandItemSave.CommandId = 12
    Me.PcaCommandItemSave.CommandName = "Save"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    Me.PcaCommandItemClose.CommandName = "Close"
    '
    'PcaCommandItemNew
    '
    Me.PcaCommandItemNew.CommandName = "New"
    '
    'PcaCommandItemPrevious
    '
    Me.PcaCommandItemPrevious.CommandId = 2
    Me.PcaCommandItemPrevious.CommandName = "Previous"
    '
    'PcaCommandItemNext
    '
    Me.PcaCommandItemNext.CommandId = 3
    Me.PcaCommandItemNext.CommandName = "Next"
    '
    'PcaCommandItemSlipSearch
    '
    Me.PcaCommandItemSlipSearch.CommandId = 6
    Me.PcaCommandItemSlipSearch.CommandName = "Search"
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandId = 7
    Me.PcaCommandItemRefer.CommandName = "Refer"
    '
    'PcaCommandItemDelete
    '
    Me.PcaCommandItemDelete.CommandName = "Delete"
    '
    'PcaCommandItemCopy
    '
    Me.PcaCommandItemCopy.CommandId = 11
    Me.PcaCommandItemCopy.CommandName = "Copy"
    '
    'PcaCommandItemInsRow
    '
    Me.PcaCommandItemInsRow.CommandName = "InsertRow"
    '
    'PcaCommandItemDelRow
    '
    Me.PcaCommandItemDelRow.CommandName = "DeleteRow"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    Me.PcaCommandItemHelp.CommandName = "Help"
    '
    'PcaCommandItemInv
    '
    Me.PcaCommandItemInv.CommandId = 7
    '
    'ToolStripMenuItemInsertRow
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemInsertRow, Me.PcaCommandItemInsRow)
    Me.ToolStripMenuItemInsertRow.Name = "ToolStripMenuItemInsertRow"
    Me.ToolStripMenuItemInsertRow.Size = New System.Drawing.Size(116, 22)
    Me.ToolStripMenuItemInsertRow.Text = "行挿入"
    '
    'ToolStripMenuItemDeleteRow
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemDeleteRow, Me.PcaCommandItemDelRow)
    Me.ToolStripMenuItemDeleteRow.Name = "ToolStripMenuItemDeleteRow"
    Me.ToolStripMenuItemDeleteRow.Size = New System.Drawing.Size(116, 22)
    Me.ToolStripMenuItemDeleteRow.Text = "行削除"
    '
    'tblTable
    '
    Me.tblTable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tblTable.BEKihonJoho = CType(resources.GetObject("tblTable.BEKihonJoho"), PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblTable.ContextMenu = Nothing
    Me.tblTable.ContextMenuStrip = Me.ContextMenuStripTable
    Me.tblTable.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.tblTable.HeadContextMenu = Nothing
    Me.tblTable.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblTable.Location = New System.Drawing.Point(12, 356)
    Me.tblTable.Name = "tblTable"
    Me.tblTable.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblTable.Size = New System.Drawing.Size(984, 320)
    Me.tblTable.TabIndex = 13
    '
    'ContextMenuStripTable
    '
    Me.ContextMenuStripTable.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ContextMenuStripTable.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemInsertRow, Me.ToolStripMenuItemDeleteRow})
    Me.ContextMenuStripTable.Name = "ContextMenuStripTable"
    Me.ContextMenuStripTable.Size = New System.Drawing.Size(117, 48)
    '
    'lblAddr
    '
    Me.lblAddr.AutoTextSize = False
    Me.lblAddr.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.lblAddr.InvisibleBorderTop = True
    Me.lblAddr.Location = New System.Drawing.Point(12, 198)
    Me.lblAddr.MaxLine = 2
    Me.lblAddr.MaxTextLength = 50
    Me.lblAddr.Name = "lblAddr"
    Me.lblAddr.Size = New System.Drawing.Size(448, 45)
    Me.lblAddr.TabIndex = 46
    Me.lblAddr.TextAlign = System.Drawing.ContentAlignment.BottomLeft
    Me.lblAddr.TextBoxBorder = True
    '
    'setOrderNo
    '
    Me.setOrderNo.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setOrderNo.AutoTopMargin = False
    Me.setOrderNo.ClientProcess = Nothing
    Me.setOrderNo.FillZeroFlag = True
    Me.setOrderNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setOrderNo.GroupBoxVisible = False
    Me.setOrderNo.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setOrderNo.LabelTopAlign = True
    Me.setOrderNo.LimitLength = 10
    Me.setOrderNo.Location = New System.Drawing.Point(637, 92)
    Me.setOrderNo.MaxCodeLength = 10
    Me.setOrderNo.MaxNameLength = 0
    Me.setOrderNo.Name = "setOrderNo"
    Me.setOrderNo.Size = New System.Drawing.Size(91, 44)
    Me.setOrderNo.TabIndex = 5
    Me.setOrderNo.TabStop = False
    Me.setOrderNo.Text = "受注No"
    Me.setOrderNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.setOrderNo.TrimEndMode = True
    Me.setOrderNo.TrimStartMode = True
    '
    'setPurNo
    '
    Me.setPurNo.AllowCodeTextFlags = PCA.Controls.AllowTextFlags.Hankaku
    Me.setPurNo.AutoTopMargin = False
    Me.setPurNo.ClientProcess = Nothing
    Me.setPurNo.FillZeroFlag = True
    Me.setPurNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setPurNo.GroupBoxVisible = False
    Me.setPurNo.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.setPurNo.LabelTopAlign = True
    Me.setPurNo.Location = New System.Drawing.Point(755, 92)
    Me.setPurNo.Margin = New System.Windows.Forms.Padding(18, 0, 0, 0)
    Me.setPurNo.MaxCodeLength = 10
    Me.setPurNo.MaxNameLength = 0
    Me.setPurNo.Name = "setPurNo"
    Me.setPurNo.Size = New System.Drawing.Size(91, 44)
    Me.setPurNo.TabIndex = 6
    Me.setPurNo.TabStop = False
    Me.setPurNo.Text = "売上No"
    Me.setPurNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.setPurNo.TrimEndMode = True
    Me.setPurNo.TrimStartMode = True
    '
    'ltxtWhNo
    '
    Me.ltxtWhNo.ClientProcess = Nothing
    Me.ltxtWhNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.ltxtWhNo.LabelText = "出荷No"
    Me.ltxtWhNo.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ltxtWhNo.LabelTopAlign = True
    Me.ltxtWhNo.LimitLength = 16
    Me.ltxtWhNo.Location = New System.Drawing.Point(390, 92)
    Me.ltxtWhNo.MaxLabelLength = 10
    Me.ltxtWhNo.MaxLimit = New Decimal(New Integer() {2147483647, 0, 0, 0})
    Me.ltxtWhNo.MaxTextLength = 10
    Me.ltxtWhNo.MinLimit = New Decimal(New Integer() {-2147483648, 0, 0, -2147483648})
    Me.ltxtWhNo.Name = "ltxtWhNo"
    Me.ltxtWhNo.ShowCalculator = False
    Me.ltxtWhNo.Size = New System.Drawing.Size(70, 44)
    Me.ltxtWhNo.TabIndex = 3
    Me.ltxtWhNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'ldtDlvDate
    '
    Me.ldtDlvDate.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.ldtDlvDate.ClientProcess = Nothing
    Me.ldtDlvDate.DisplaySlash = True
    Me.ldtDlvDate.EmptyIfDisabled = True
    Me.ldtDlvDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.ldtDlvDate.Holidays = Nothing
    Me.ldtDlvDate.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.ldtDlvDate.LabelText = "納期"
    Me.ldtDlvDate.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.ldtDlvDate.LabelTopAlign = True
    Me.ldtDlvDate.Location = New System.Drawing.Point(257, 92)
    Me.ldtDlvDate.MaxLabelLength = 15
    Me.ldtDlvDate.MaxTextLength = 15
    Me.ldtDlvDate.MustInput = False
    Me.ldtDlvDate.Name = "ldtDlvDate"
    Me.ldtDlvDate.Size = New System.Drawing.Size(105, 44)
    Me.ldtDlvDate.TabIndex = 2
    '
    'frmMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackColor = System.Drawing.SystemColors.Control
    Me.ClientSize = New System.Drawing.Size(1008, 730)
    Me.Controls.Add(Me.ldtDlvDate)
    Me.Controls.Add(Me.ltxtWhNo)
    Me.Controls.Add(Me.lblAddr)
    Me.Controls.Add(Me.tblTable)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.ltxtModifiedAt)
    Me.Controls.Add(Me.ltxtModifiedBy)
    Me.Controls.Add(Me.ltxtCreatedAt)
    Me.Controls.Add(Me.ltxtCreatedBy)
    Me.Controls.Add(Me.setPerson)
    Me.Controls.Add(Me.setDepartment)
    Me.Controls.Add(Me.setMemoCode)
    Me.Controls.Add(Me.setMemoContent)
    Me.Controls.Add(Me.setProject)
    Me.Controls.Add(Me.lblSupName)
    Me.Controls.Add(Me.lblSupTel)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.lblSupFax)
    Me.Controls.Add(Me.setType)
    Me.Controls.Add(Me.lblSupTax)
    Me.Controls.Add(Me.ldtShipmentDate)
    Me.Controls.Add(Me.setPurNo)
    Me.Controls.Add(Me.setOrderNo)
    Me.Controls.Add(Me.setSupCode)
    Me.Controls.Add(Me.ltxtWhNo2)
    Me.Controls.Add(Me.HeaderLabel)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1024, 768)
    Me.Name = "frmMain"
    Me.Text = "出荷ロット入力"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    Me.ContextMenuStripTable.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents ServiceController1 As System.ServiceProcess.ServiceController
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents FToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSave As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemNew As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemReset As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPrevious As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemNext As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemRefer As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemDelete As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemCopy As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPrevious As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonNext As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonCreate As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonReset As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonDelete As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonCopy As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonSave As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents HeaderLabel As PCA.TSC.Kon.Tools.TscInputHeaderLabel
  Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
  Private WithEvents setType As PCA.Controls.PcaCodeSet
  Private WithEvents setSupCode As PCA.Controls.PcaCodeSet
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents lblSupName As PCA.TSC.Kon.Tools.TscLabel
  Friend WithEvents lblSupTel As PCA.TSC.Kon.Tools.TscLabel
  Friend WithEvents lblSupFax As PCA.TSC.Kon.Tools.TscLabel
  Friend WithEvents lblSupTax As PCA.TSC.Kon.Tools.TscLabel
  Friend WithEvents setPerson As PCA.Controls.PcaCodeSet
  Friend WithEvents setDepartment As PCA.Controls.PcaCodeSet
  Friend WithEvents setMemoCode As PCA.Controls.PcaCodeSet
  Friend WithEvents setMemoContent As PCA.Controls.PcaCodeSet
  Friend WithEvents setProject As PCA.Controls.PcaCodeSet
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents ltxtCreatedBy As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemPrevious As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemNext As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSlipSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemCopy As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSave As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandPrevious As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandNext As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSlipSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandCopy As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSave As PCA.Controls.PcaFunctionCommand
  Friend WithEvents tblTable As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents lblAddr As PCA.TSC.Kon.Tools.TscLabel
  Private WithEvents setOrderNo As PCA.Controls.PcaCodeSet
  Private WithEvents setPurNo As PCA.Controls.PcaCodeSet
  Friend WithEvents ldtShipmentDate As Sunloft.PCAControls.SLPcaLabeledDate
  Friend WithEvents ltxtWhNo2 As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtCreatedAt As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtModifiedBy As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtModifiedAt As Sunloft.PCAControls.SLPcaLabeledTextBox
  Friend WithEvents ltxtWhNo As PCA.Controls.PcaLabeledNumberBox
  Friend WithEvents PcaCommandItemDelete As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemNew As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemReset As PCA.Controls.PcaCommandItem
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents ldtDlvDate As Sunloft.PCAControls.SLPcaLabeledDate
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaCommandItemInv As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandInv As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaCommandItemDelRow As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemInsRow As PCA.Controls.PcaCommandItem
  Friend WithEvents ToolStripMenuItemRowInsert As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemRowDelete As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ContextMenuStripTable As System.Windows.Forms.ContextMenuStrip
  Friend WithEvents ToolStripMenuItemInsertRow As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemDeleteRow As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripButtonInsertRow As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonDeleteRow As System.Windows.Forms.ToolStripButton

End Class

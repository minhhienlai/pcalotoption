﻿Imports Sunloft.PCAIF
Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports PCA.ApplicationIntegration
Imports PCA.Controls
Imports PCA
Imports PCA.TSC.Kon.Tools
Imports System.Windows.Forms
Imports System.Globalization
Imports System.Drawing

Public Class SLZaikoDialog
  Inherits Sunloft.PCAForms.SPClientBase

#Region " Declare"
  Public Property connector As IIntegratedApplication

  Private Const HEADER_COLUMN_PREFIX As String = "H_"
  Private Const BODY_COLUMN_PREFIX As String = "B_"

  Private Const DBITEM_DATE As String = "sl_zdn_uribi"
  Private Const DBITEM_WAREHOUSE_NAME As String = "sl_zdn_souko_name"
  Private Const DBITEM_WAREHOUSE_CODE As String = "sl_zdn_souko_code"
  Private Const DBITEM_LOTNO As String = "sl_zdn_ulotno"
  Private Const DBITEM_INV_NUMBER As String = "sl_zdn_honsu"
  Private Const DBITEM_INV_QTY As String = "sl_zdn_suryo"
  Private Const DBITEM_NUMBER As String = "input_number"
  Private Const DBITEM_QTY As String = "input_suryo"
  Private Const DBITEM_UNIT_PRICE As String = "sl_zdn_tanka"
  Private Const DBITEM_SDLDATE As String = "sl_zdn_sdldate"
  Private Const DBITEM_UBDATE As String = "sl_zdn_ubdate"
  Private Const DBITEM_BBDATE As String = "sl_zdn_bbdate"
  Private Const DBITEM_EXPDATE As String = "sl_zdn_expdate"
  Private Const DBITEM_ID As String = "sl_zdn_id"
  Private Const DBITEM_LOTDETAIL1 As String = "sl_zdn_ldmei1"
  Private Const DBITEM_LOTDETAIL2 As String = "sl_zdn_ldmei2"
  Private Const DBITEM_LOTDETAIL3 As String = "sl_zdn_ldmei3"
  Private Const DBITEM_LOTDETAIL4 As String = "sl_zdn_ldmei4"
  Private Const DBITEM_LOTDETAIL5 As String = "sl_zdn_ldmei5"
  Private Const DBITEM_LOTDETAIL6 As String = "sl_zdn_ldmei6"
  Private Const DBITEM_LOTDETAIL7 As String = "sl_zdn_ldmei7"
  Private Const DBITEM_LOTDETAIL8 As String = "sl_zdn_ldmei8"
  Private Const DBITEM_LOTDETAIL9 As String = "sl_zdn_ldmei9"
  Private Const DBITEM_LOTDETAIL10 As String = "sl_zdn_ldmei10"

  Friend WithEvents tblTable As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip

  Private m_SL_LMB As SL_LMB
  Private m_SMS As SMS
  Private m_intRowIndex As Integer

  Property RetIntInvID As Integer
  Property RetDecShippingNumber As Decimal
  Property RetDecShippingQty As Decimal
  Property RetDecInvNum As Decimal
  Property RetDecInvQty As Decimal
  Property RetStrLotNo As String
  Property RetDecUnitPrice As Decimal
  Property RetStrWarehouseCode As String

  Private m_ShipipngType As enmSlipType
  Private m_isValidating As Boolean = False

  Private m_intCurInvID As Integer = 0
  Private m_intCurInvNum As Integer = 0
  Private m_decCurInvQty As Decimal = 0D

  Private m_decValidateValue As Decimal = 0D
  Private m_strValidateCol As String = String.Empty
  Private m_intValidateRow As Integer = 0
  Private SlipState As PCA.Controls.LabelStateType = LabelStateType.New

  Public Enum enmSlipType
    typNormal  'Shipping
    typReturn  'Return Item
  End Enum

#End Region

#Region " Initialize"
  Public Sub New(ByVal myApp As IIntegratedApplication, ByVal strProductCode As String, ByVal dteShippingDate As DateTime, ByVal intType As enmSlipType, _
                 Optional ByVal strWarehouse As String = "", Optional ByVal intCurInvID As Integer = 0, Optional ByVal intCurInvNum As Integer = 0, _
                 Optional ByVal decCurInvQty As Decimal = 0D, Optional argSlipState As PCA.Controls.LabelStateType = LabelStateType.New)
    Try

      InitializeComponent()
      Me.connector = myApp
      Dim AMS1Class As New AMS1(connector)
      AMS1Class.ReadAll()

      setProduct.CodeText = strProductCode
      m_SL_LMB = New SL_LMB(connector)
      m_SL_LMB.ReadOnlyRow()
      m_SMS = New SMS(connector)
      m_SMS.ReadByID(strProductCode)

      setProduct.NameText = m_SMS.sms_mei
      txtShipDate.Text = dteShippingDate.ToShortDateString
      setWarehouse.CodeText = strWarehouse
      m_ShipipngType = intType

      setWarehouse.LimitLength = AMS1Class.ams1_WarehouseLength
      If intType = enmSlipType.typReturn Then
        rdtWarehousingDate.FromDate = dteShippingDate.AddMonths(-1)
        rdtWarehousingDate.ToDate = dteShippingDate
      End If
      With tblTable
        .DataRowCount = 99
        '入力用青枠の表示
        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader()     'ヘッダー部
        InitTableBody()       'ボディ部
        .UpdateTable()
        '.UpDownKeyAsTab = True
        .BodyColumns.DefaultStyle.BackColor = Color.White
        .BodyColumns.DefaultAltStyle.BackColor = Color.White
      End With
      m_intCurInvID = intCurInvID
      m_intCurInvNum = intCurInvNum
      m_decCurInvQty = decCurInvQty
      SlipState = argSlipState

      Get_ZaikoData()

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub SLZaikoDialog_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
    'set focus stock row
    If m_intRowIndex > 0 Then
      If CInt(tblTable.GetCellValue(m_intRowIndex, DBITEM_INV_NUMBER)) > 0 Then
        tblTable.SetCellFocus(m_intRowIndex, DBITEM_NUMBER)
      Else
        tblTable.SetCellFocus(m_intRowIndex, DBITEM_QTY)
      End If
    End If
  End Sub


  ''' <summary>
  ''' Initialize PCA Table Header Column + Cell
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitTableHeader()
    Try
      Dim arLotDetails As String() = {m_SL_LMB.sl_lmb_label1, m_SL_LMB.sl_lmb_label2, m_SL_LMB.sl_lmb_label3, m_SL_LMB.sl_lmb_label4, m_SL_LMB.sl_lmb_label5 _
                                     , m_SL_LMB.sl_lmb_label6, m_SL_LMB.sl_lmb_label7, m_SL_LMB.sl_lmb_label8, m_SL_LMB.sl_lmb_label9, m_SL_LMB.sl_lmb_label10}
      Dim arLotDetailHeaders As String() = {DBITEM_LOTDETAIL1, DBITEM_LOTDETAIL2, DBITEM_LOTDETAIL3, DBITEM_LOTDETAIL4, DBITEM_LOTDETAIL5 _
                                           , DBITEM_LOTDETAIL6, DBITEM_LOTDETAIL7, DBITEM_LOTDETAIL8, DBITEM_LOTDETAIL9, DBITEM_LOTDETAIL10}

      Dim intLpc As Integer = 0

      tblTable.HeadTextHeight = 1
      tblTable.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
      tblTable.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_DATE, "入庫日", StringAlignment.Center, 0.8))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_WAREHOUSE_NAME, "倉庫", StringAlignment.Near, 1))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_LOTNO, "ロットNo", StringAlignment.Near, 0.8))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_INV_NUMBER, "在庫本数", StringAlignment.Far, 0.8))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_INV_QTY, "在庫数", StringAlignment.Far, 0.8))

      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_NUMBER, "出荷本数", StringAlignment.Far, 0.8))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_QTY, "出荷数量", StringAlignment.Far, 0.8))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_UNIT_PRICE, "単価", StringAlignment.Far, 0.7))
      If m_SL_LMB.sl_lmb_sdlflg = 1 Then tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_SDLDATE, "出荷期限", StringAlignment.Center, 0.8))
      If m_SL_LMB.sl_lmb_ubdflg = 1 Then tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_UBDATE, "使用期限", StringAlignment.Center, 0.8))
      If m_SL_LMB.sl_lmb_bbdflg = 1 Then tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_BBDATE, "賞味期限", StringAlignment.Center, 0.8))
      If m_SL_LMB.sl_lmb_expflg = 1 Then tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_EXPDATE, "消費期限", StringAlignment.Center, 0.8))

      For intLpc = 0 To m_SL_LMB.sl_lmb_maxlotop - 1
        tblTable.HeadColumns.Add(SetHeaderColumn(arLotDetailHeaders(intLpc), arLotDetails(intLpc), StringAlignment.Center, 0.8))
      Next

      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_ID, DBITEM_ID, StringAlignment.Near, 0.0))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_WAREHOUSE_CODE, DBITEM_WAREHOUSE_CODE, StringAlignment.Near, 0.0))


    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Initialize PCA Table Body Column + Cell
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitTableBody()
    Try
      Dim arLotDetails As String() = {m_SL_LMB.sl_lmb_label1, m_SL_LMB.sl_lmb_label2, m_SL_LMB.sl_lmb_label3, m_SL_LMB.sl_lmb_label4, m_SL_LMB.sl_lmb_label5 _
                                    , m_SL_LMB.sl_lmb_label6, m_SL_LMB.sl_lmb_label7, m_SL_LMB.sl_lmb_label8, m_SL_LMB.sl_lmb_label9, m_SL_LMB.sl_lmb_label10}
      Dim arLotDetailHeaders As String() = {DBITEM_LOTDETAIL1, DBITEM_LOTDETAIL2, DBITEM_LOTDETAIL3, DBITEM_LOTDETAIL4, DBITEM_LOTDETAIL5 _
                                           , DBITEM_LOTDETAIL6, DBITEM_LOTDETAIL7, DBITEM_LOTDETAIL8, DBITEM_LOTDETAIL9, DBITEM_LOTDETAIL10}
      Dim intLpc As Integer = 0

      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_DATE, DBITEM_DATE, CellType.Text, StringAlignment.Center, False, 0.8))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_WAREHOUSE_NAME, DBITEM_WAREHOUSE_NAME, CellType.Text, StringAlignment.Near, False, 1))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_LOTNO, DBITEM_LOTNO, CellType.Text, StringAlignment.Near, False, 0.8))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_INV_NUMBER, DBITEM_INV_NUMBER, CellType.Number, StringAlignment.Far, False, 0.8))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_INV_QTY, DBITEM_INV_QTY, CellType.Number, StringAlignment.Far, False, 0.8))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_NUMBER, DBITEM_NUMBER, CellType.Number, StringAlignment.Far, True, 0.8))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_QTY, DBITEM_QTY, CellType.Number, StringAlignment.Far, True, 0.8, Short.Parse(m_SMS.sms_sketa.ToString)))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_UNIT_PRICE, DBITEM_UNIT_PRICE, CellType.Number, StringAlignment.Far, False, 0.7))
      If m_SL_LMB.sl_lmb_sdlflg = 1 Then tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_SDLDATE, DBITEM_SDLDATE, CellType.Text, StringAlignment.Center, False, 0.8))
      If m_SL_LMB.sl_lmb_ubdflg = 1 Then tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_UBDATE, DBITEM_UBDATE, CellType.Text, StringAlignment.Center, False, 0.8))
      If m_SL_LMB.sl_lmb_bbdflg = 1 Then tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_BBDATE, DBITEM_BBDATE, CellType.Text, StringAlignment.Center, False, 0.8))
      If m_SL_LMB.sl_lmb_expflg = 1 Then tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_EXPDATE, DBITEM_EXPDATE, CellType.Text, StringAlignment.Center, False, 0.8))

      For intLpc = 0 To m_SL_LMB.sl_lmb_maxlotop - 1
        tblTable.BodyColumns.Add(SetBodyColumn(arLotDetailHeaders(intLpc), arLotDetailHeaders(intLpc), CellType.Text, StringAlignment.Center, False, 0.8))
      Next
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_ID, DBITEM_ID, CellType.Text, StringAlignment.Near, False, 0.0))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_WAREHOUSE_CODE, DBITEM_WAREHOUSE_CODE, CellType.Text, StringAlignment.Near, False, 0.0))
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Setting PCA Table Header Column + Cell
  ''' </summary>
  ''' <param name="strColumnName">column name(show text)</param>
  ''' <param name="strCellName">cell name</param>
  ''' <param name="align">cell alignment</param>
  ''' <param name="txtWidth">column width</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SetHeaderColumn(ByVal strColumnName As String, ByVal strCellName As String, Optional ByVal align As StringAlignment = StringAlignment.Near, Optional ByVal txtWidth As Single = 1.0) As PcaColumn
    Dim column As PcaColumn
    Dim cell As PcaCell
    column = New PcaColumn(HEADER_COLUMN_PREFIX & strColumnName, txtWidth)
    Try
      column.CanResize = True
      column.AllowUserResize = True
      cell = tblTable.PrepareFixTextCell(strCellName)
      cell.CellStyle.Alignment = align
      cell.TextWidth = txtWidth
      column.Cells.Add(cell)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return column
  End Function

  ''' <summary>
  ''' Setting PCA Table Body Column + Cell
  ''' </summary>
  ''' <param name="strColumnName">column name</param>
  ''' <param name="strCellName">cell name</param>
  ''' <param name="CellType">pca cell type</param>
  ''' <param name="align">cell alignment</param>
  ''' <param name="isEdit">cell edit on/off</param>
  ''' <param name="txtWidth">column width</param>
  ''' <param name="intDigit">cell digit</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SetBodyColumn(ByVal strColumnName As String, ByVal strCellName As String, ByVal CellType As PCA.Controls.CellType, Optional ByVal align As StringAlignment = StringAlignment.Near, Optional ByVal isEdit As Boolean = False, Optional ByVal txtWidth As Single = 1.0, Optional ByVal intDigit As Short = 0) As PcaColumn
    Dim column As PcaColumn
    Dim cell As PcaCell
    column = New PcaColumn(BODY_COLUMN_PREFIX & strColumnName, txtWidth)
    Try
      column.CanResize = True
      column.AllowUserResize = True
      cell = New PcaCell(strCellName, CellType)
      cell.CellStyle.Alignment = align

      If (CellType = PCA.Controls.CellType.Number) Then
        cell.FormatComma = True
        cell.FormatDecimal = True
        cell.DecimalDigit = intDigit
      End If
      cell.EditMode = isEdit
      column.Cells.Add(cell)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return column
  End Function
#End Region

#Region " Show Table"
  ''' <summary>
  ''' Get Inventory From SL_ZDN -> Show Table
  ''' </summary>
  ''' <returns>Get+Show Success -> True</returns>
  ''' <remarks></remarks>
  Private Function Get_ZaikoData() As Boolean
    Dim isSuccess As Boolean = True
    Dim intRow As Integer
    Dim intRowCount As Integer = 0
    Dim intLotDetailRow As Integer = 0
    Dim SMSClass As SMS = New SMS(connector)

    Try
      Dim arLotDetails As String() = {}
      Dim arLotDetailHeaders As String() = {DBITEM_LOTDETAIL1, DBITEM_LOTDETAIL2, DBITEM_LOTDETAIL3, DBITEM_LOTDETAIL4, DBITEM_LOTDETAIL5 _
                                           , DBITEM_LOTDETAIL6, DBITEM_LOTDETAIL7, DBITEM_LOTDETAIL8, DBITEM_LOTDETAIL9, DBITEM_LOTDETAIL10}
      Dim intFromDate As Integer = 0
      Dim intToDate As Integer = CInt(Now.Year & Format(Now.Month, "00") & Format(Now.Day, "00"))

      Dim result As SL_ZDN()
      Dim SL_ZDNClass As SL_ZDN = New SL_ZDN(connector)
      result = SL_ZDNClass.ReadByProductDate(setProduct.CodeText.Trim, intFromDate, intToDate, setWarehouse.CodeText.Trim, txtLotNo.Text.Trim)
      tblTable.DataRowCount = 99
      tblTable.BodyRowCount = 99
      tblTable.ClearCellValues()
      If result Is Nothing Then
        intRowCount = 0
        Common.SLCmnFunction.ShowToolTip("在庫" & Common.SLConstants.NotifyMessage.NOT_FOUND, Common.SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, _
                                         CommandSearch.CommandId)
      Else
        intRowCount = result.Count
        If SMSClass.ReadByID(setProduct.CodeText.Trim) Then
          'Set quantity value
          Dim cell As PcaCell = tblTable.BodyColumns.GetCell(DBITEM_INV_QTY)
          cell.DecimalDigit = CShort(SMSClass.sms_sketa)
          cell.FormatDecimal = True
          cell.Enabled = True
        End If
      End If
      Dim Zaiko As Sunloft.PCAIF.SL_ZDN

      For intRow = 0 To intRowCount - 1
        Zaiko = result(intRow)
        
        arLotDetails = {Zaiko.sl_zdn_ldmei1, Zaiko.sl_zdn_ldmei2, Zaiko.sl_zdn_ldmei3, Zaiko.sl_zdn_ldmei4, Zaiko.sl_zdn_ldmei5 _
                       , Zaiko.sl_zdn_ldmei6, Zaiko.sl_zdn_ldmei7, Zaiko.sl_zdn_ldmei8, Zaiko.sl_zdn_ldmei9, Zaiko.sl_zdn_ldmei10}

        tblTable.SetCellValue(intRow, DBITEM_DATE, Date.ParseExact(Zaiko.sl_zdn_uribi.ToString.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString)
        tblTable.SetCellValue(intRow, DBITEM_WAREHOUSE_CODE, Zaiko.sl_zdn_souko.ToString.Trim())
        tblTable.SetCellValue(intRow, DBITEM_WAREHOUSE_NAME, Zaiko.sl_zdn_soukoMei.ToString.Trim())

        tblTable.SetCellValue(intRow, DBITEM_LOTNO, Zaiko.sl_zdn_ulotno.ToString.Trim())
        tblTable.SetCellValue(intRow, DBITEM_INV_NUMBER, Zaiko.sl_zdn_honsu.ToString.Trim())
        tblTable.SetCellValue(intRow, DBITEM_INV_QTY, Format(Zaiko.sl_zdn_suryo, Sunloft.Common.SLCmnFunction.FormatComma(SMSClass.sms_sketa)))
        tblTable.SetCellValue(intRow, DBITEM_UNIT_PRICE, Format(Zaiko.sl_zdn_tanka, Sunloft.Common.SLCmnFunction.FormatComma(SMSClass.sms_tketa)))

        tblTable.SetCellValue(intRow, DBITEM_SDLDATE, Common.SLCmnFunction.ConvertIntToDatestring(Zaiko.sl_zdn_sdldate.ToString.Trim()))
        tblTable.SetCellValue(intRow, DBITEM_UBDATE, Common.SLCmnFunction.ConvertIntToDatestring(Zaiko.sl_zdn_ubdate.ToString.Trim()))
        tblTable.SetCellValue(intRow, DBITEM_BBDATE, Common.SLCmnFunction.ConvertIntToDatestring(Zaiko.sl_zdn_bbdate.ToString.Trim()))
        tblTable.SetCellValue(intRow, DBITEM_EXPDATE, Common.SLCmnFunction.ConvertIntToDatestring(Zaiko.sl_zdn_expdate.ToString.Trim()))

        If m_intCurInvID = Zaiko.sl_zdn_id Then
          m_intRowIndex = intRow
          tblTable.SetCellValue(intRow, DBITEM_NUMBER, Format(m_intCurInvNum, "#,0"))
          tblTable.SetCellValue(intRow, DBITEM_QTY, Format(m_decCurInvQty, Sunloft.Common.SLCmnFunction.FormatComma(SMSClass.sms_sketa)))
          If SlipState <> LabelStateType.New Then
            tblTable.SetCellValue(intRow, DBITEM_INV_NUMBER, Format(Zaiko.sl_zdn_honsu + m_intCurInvNum, "#,0"))
            tblTable.SetCellValue(intRow, DBITEM_INV_QTY, Format(Zaiko.sl_zdn_suryo + m_decCurInvQty, Sunloft.Common.SLCmnFunction.FormatComma(SMSClass.sms_sketa)))
          End If
        End If
        For intLotDetailRow = 0 To m_SL_LMB.sl_lmb_maxlotop - 1
          tblTable.SetCellValue(intRow, arLotDetailHeaders(intLotDetailRow), arLotDetails(intLotDetailRow).ToString.Trim())
        Next

        tblTable.SetCellValue(intRow, DBITEM_ID, Zaiko.sl_zdn_id.ToString.Trim())
      Next intRow
      tblTable.DataRowCount = intRowCount
      tblTable.BodyRowCount = intRowCount

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function
#End Region

#Region " Return Value"
  ''' <summary>
  ''' return Input Inventory ID + Qty + Number
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function returnValue() As Collection
    Dim value As Collection = New Collection
    Dim decQty As Decimal
    Dim decNumber As Decimal
    Dim intInvID As Integer
    Try
      For intRow = 0 To tblTable.DataRowCount - 1
        If IsNothing(tblTable.GetCellValue(intRow, DBITEM_NUMBER)) = False AndAlso Decimal.TryParse(tblTable.GetCellValue(intRow, DBITEM_NUMBER).ToString, decNumber) = False Then decNumber = 0
        If IsNothing(tblTable.GetCellValue(intRow, DBITEM_QTY)) = False AndAlso Decimal.TryParse(tblTable.GetCellValue(intRow, DBITEM_QTY).ToString, decQty) = False Then decQty = 0

        If (decNumber <> 0 OrElse decQty <> 0) Then
          'check input row -> get data
          If Integer.TryParse(tblTable.GetCellValue(intRow, DBITEM_ID).ToString, intInvID) = False Then intInvID = 0
          Exit For
        End If
      Next intRow
      value.Add(intInvID)
      value.Add(decNumber)
      value.Add(decQty)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return value
  End Function
#End Region

#Region " Table Event"
  ''' <summary>
  ''' Check Qty/Number
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks></remarks>
  Private Sub tblTable_ValidatingCell(sender As System.Object, args As PCA.Controls.CancelCellEventArgs) Handles tblTable.ValidatingCell
    Dim myTable As TscMeisaiTable = DirectCast(sender, TscMeisaiTable)
    Try
      If m_isValidating Then Exit Sub
      Select Case args.CellName
        Case DBITEM_NUMBER
          'Check stock number 
          Dim decNewQty As Decimal
          Dim decInvQty As Decimal
          If Decimal.TryParse(tblTable.GetCellValue(m_intRowIndex, DBITEM_INV_NUMBER).ToString, decInvQty) = False Then decInvQty = 0

          If Decimal.TryParse(args.NewValue.ToString, decNewQty) Then
            If decInvQty - decNewQty < 0 And m_ShipipngType = enmSlipType.typNormal Then
              Common.SLCmnFunction.ShowToolTip("入力した本数が在庫本数を超えています。", "在庫本数チェック", ToolTipIcon.Error, tblTable.GetCellRectangle(Me, m_intRowIndex, DBITEM_NUMBER), ToolTip1, Me)
              args.Cancel = True
              Return
            Else
              tblTable.SetCellValue(m_intRowIndex, args.CellName, args.NewValue)
            End If
          End If

        Case DBITEM_QTY
          'Check stock quantity
          Dim decNewQty As Decimal
          Dim decInvQty As Decimal
          If Decimal.TryParse(tblTable.GetCellValue(m_intRowIndex, DBITEM_INV_QTY).ToString, decInvQty) = False Then decInvQty = 0

          If Decimal.TryParse(args.NewValue.ToString, decNewQty) Then
            If decInvQty - decNewQty < 0 And m_ShipipngType = enmSlipType.typNormal Then
              Common.SLCmnFunction.ShowToolTip("入力した数量が在庫数を超えています。", "在庫数チェック", ToolTipIcon.Error, tblTable.GetCellRectangle(Me, m_intRowIndex, DBITEM_NUMBER), ToolTip1, Me)
              args.Cancel = True
              Return
            End If
          End If
      End Select

      m_decValidateValue = 0
      m_intValidateRow = m_intRowIndex
      m_strValidateCol = args.CellName
      Decimal.TryParse(args.NewValue.ToString, m_decValidateValue)
      Dim keyCode As Keys = (args.KeyData And Keys.KeyCode)        'キーコード
      Dim keyModifiers As Keys = (args.KeyData And Keys.Modifiers) '修飾子
      If keyModifiers = Keys.Shift And keyCode = Keys.Return Then keyCode = Keys.Up
      Select Case keyCode
        Case Keys.Up, Keys.Left
          If m_intRowIndex = 0 And args.CellName = DBITEM_NUMBER Then Exit Select
          If args.CellName = DBITEM_NUMBER Then
            tblTable.SetCellFocus(m_intRowIndex - 1, DBITEM_QTY)
          Else
            If CInt(tblTable.GetCellValue(m_intRowIndex, DBITEM_INV_NUMBER)) > 0 Then
              tblTable.SetCellFocus(m_intRowIndex, DBITEM_NUMBER)
            Else
              tblTable.SetCellFocus(m_intRowIndex - 1, DBITEM_QTY)
            End If
          End If
        Case Keys.Down, Keys.Right, Keys.Return
          If m_intRowIndex = tblTable.BodyRowCount - 1 And args.CellName = DBITEM_QTY Then Exit Select
          If args.CellName = DBITEM_NUMBER Then
            tblTable.SetCellFocus(m_intRowIndex, DBITEM_QTY)
          Else
            If CInt(tblTable.GetCellValue(m_intRowIndex + 1, DBITEM_INV_NUMBER)) > 0 Then
              tblTable.SetCellFocus(m_intRowIndex + 1, DBITEM_NUMBER)
            Else
              tblTable.SetCellFocus(m_intRowIndex + 1, DBITEM_QTY)
            End If
          End If
      End Select

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub tblTable_ValidatedCell(sender As Object, args As PCA.Controls.CellEventArgs) Handles tblTable.ValidatedCell
    Try
      If m_strValidateCol.Length > 0 And m_intValidateRow > -1 And m_intValidateRow < tblTable.DataRowCount And m_decValidateValue > 0D Then
        tblTable.SetCellValue(m_intValidateRow, m_strValidateCol, m_decValidateValue)
        m_strValidateCol = String.Empty : m_intValidateRow = -1 : m_decValidateValue = 0D
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  ''' <summary>
  ''' Check Input Data(Qty)
  ''' </summary>
  ''' <returns>Is check success -> true , else -> false</returns>
  ''' <remarks></remarks>
  Private Function Check_RowData() As Boolean
    Dim decQty As Decimal
    Dim decNumber As Decimal
    Dim decInvQty As Decimal
    Dim decInvNum As Decimal
    Dim intExistCntQty As Integer = 0
    Dim intExistCnt As Integer = 0
    Dim strErrMsg As String = String.Empty
    Dim isSuccess As Boolean = True
    Dim intRow As Integer
    Try
      setWarehouse.Focus()
      For intRow = 0 To tblTable.DataRowCount - 1
        decNumber = 0
        decQty = 0
        If IsNothing(tblTable.GetCellValue(intRow, DBITEM_NUMBER)) = False AndAlso Decimal.TryParse(tblTable.GetCellValue(intRow, DBITEM_NUMBER).ToString, decNumber) = False Then decNumber = 0
        If IsNothing(tblTable.GetCellValue(intRow, DBITEM_QTY)) = False AndAlso Decimal.TryParse(tblTable.GetCellValue(intRow, DBITEM_QTY).ToString, decQty) = False Then decQty = 0
        If IsNothing(tblTable.GetCellValue(intRow, DBITEM_INV_NUMBER)) = False AndAlso Decimal.TryParse(tblTable.GetCellValue(intRow, DBITEM_INV_NUMBER).ToString, decInvNum) = False Then decInvNum = 0
        If IsNothing(tblTable.GetCellValue(intRow, DBITEM_INV_QTY)) = False AndAlso Decimal.TryParse(tblTable.GetCellValue(intRow, DBITEM_INV_QTY).ToString, decInvQty) = False Then decInvQty = 0

        If (decNumber <> 0 Or decQty <> 0) Then
          RetDecShippingNumber = decNumber
          RetDecShippingQty = decQty
          RetStrLotNo = tblTable.GetCellValue(intRow, DBITEM_LOTNO).ToString
          RetDecUnitPrice = CDec(tblTable.GetCellValue(intRow, DBITEM_UNIT_PRICE))
          RetDecInvNum = decInvNum
          RetDecInvQty = decInvQty
          RetIntInvID = CInt(tblTable.GetCellValue(intRow, DBITEM_ID))
          RetStrWarehouseCode = tblTable.GetCellValue(intRow, DBITEM_WAREHOUSE_CODE).ToString
          If (intExistCnt > 0) Then
            strErrMsg = "出荷本数ないし数量が複数行入力されています。"
            Exit For
          End If
          If (decQty <> 0) Then intExistCntQty += 1
          intExistCnt += 1

          If (decInvNum < decNumber) And m_ShipipngType = enmSlipType.typNormal Then
            strErrMsg = "出荷本数が在庫本数を超えています。"
            Exit For
          End If
          If (decInvQty < decQty) And m_ShipipngType = enmSlipType.typNormal Then
            strErrMsg = "出荷数量が在庫数量を超えています。"
            Exit For
          End If
        End If
      Next intRow

      If (intExistCntQty = 0) Then If strErrMsg.Trim.Length = 0 Then strErrMsg = "出荷数量が入力されていません。"

      If (String.IsNullOrEmpty(strErrMsg) = False) Then
        isSuccess = False
        Common.SLCmnFunction.ShowToolTip(strErrMsg, Common.SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Error, PcaFunctionBar1, ToolTip1, Me, CommandSelect.CommandId)
        If intRow = tblTable.DataRowCount Then intRow -= 1
        tblTable.SetCellFocus(intRow, DBITEM_QTY)

      End If


    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      isSuccess = False
    End Try
    Return isSuccess
  End Function

  ''' <summary>
  ''' Set RowIndex
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="args"></param>
  ''' <remarks></remarks>
  Private Sub tblTable_EnterInputRow(sender As Object, args As PCA.Controls.InputRowEventArgs) Handles tblTable.EnterInputRow
    Try
      m_intRowIndex = args.RowIndex
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  ''' <summary>
  ''' Search Warehouse Master
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub setWarehouse_ClickReferButton(sender As System.Object, e As System.EventArgs) Handles setWarehouse.ClickReferButton
    Dim newCode As String = String.Empty
    Dim myCodeSet As PcaCodeSet = CType(sender, PcaCodeSet)
    Try
      Dim masterDialog = New SLMasterSearchDialog(connector)
      Dim location As Point = ControlTool.GetDialogLocation(myCodeSet.ReferButton)
      newCode = masterDialog.ShowReferSokoDialog(myCodeSet.CodeText, location)
      If String.IsNullOrEmpty(newCode) = False Then myCodeSet.CodeText = newCode
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub


  Private Sub setWarehouse_CodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles setWarehouse.CodeTextValidating2
    Try
      Dim EMSClass As New EMS(connector)
      Dim AMS1Class As New AMS1(connector)
      AMS1Class.ReadAll()

      Dim strWHCode = Common.SLCmnFunction.standardlizeCode(setWarehouse.CodeText.Trim, AMS1Class.ams1_WarehouseLength)
      If setWarehouse.CodeText = strWHCode Or m_isValidating Then Return

      setWarehouse.CodeText = strWHCode
      If strWHCode.Trim.Length > 0 AndAlso EMSClass.ReadKbnMasterByIDAndKbn(Common.SLConstants.EMS.Warehouse, strWHCode).ToString.Trim = String.Empty Then
        Common.SLCmnFunction.ShowToolTip(Common.SLConstants.NotifyMessage.DATA_NOT_FOUND, Common.SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, setWarehouse, ToolTip1, Me)
        e.Cancel = True
      Else
        Get_ZaikoData()
      End If
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' check user press Tap or Enter
  ''' </summary >
  ''' <param name="keyData"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean

    Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
    Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子

    Select Case keyCode
      Case Keys.Down
        keyData = Keys.Return
      Case Keys.Up
        keyData = Keys.Return
      Case Else

    End Select

    Return MyBase.ProcessDialogKey(keyData)

  End Function

#End Region

#Region "CommandManager"
  Private Sub PcaCommandManager_Command(sender As System.Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command        '

    Select Case e.CommandItem.CommandName
      Case CommandClose.CommandName
        DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
      Case CommandSelect.CommandName
        m_isValidating = True
        If Check_RowData() Then DialogResult = System.Windows.Forms.DialogResult.OK
        m_isValidating = False
      Case CommandSearch.CommandName
        If Not tblTable.Focused Then tblTable.Focus() 'Leave all searching conditions fields
        Get_ZaikoData()
    End Select

  End Sub
#End Region

#Region " InitializeComponent/ About Control"
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SLZaikoDialog))
    Me.setProduct = New PCA.Controls.PcaCodeSet()
    Me.PcaRangeDate1 = New PCA.Controls.PcaRangeDate()
    Me.PcaRangeDate2 = New PCA.Controls.PcaRangeDate()
    Me.rdtWarehousingDate = New PCA.Controls.PcaRangeDate()
    Me.setWarehouse = New PCA.Controls.PcaCodeSet()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFCommandSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFCommandSelect = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.CommandSearch = New PCA.Controls.PcaCommandItem()
    Me.CommandSelect = New PCA.Controls.PcaCommandItem()
    Me.CommandClose = New PCA.Controls.PcaCommandItem()
    Me.txtLotNo = New PCA.Controls.PcaLabeledTextBox()
    Me.txtShipDate = New PCA.Controls.PcaLabeledTextBox()
    Me.tblTable = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.SuspendLayout()
    '
    'setProduct
    '
    Me.setProduct.AutoTopMargin = False
    Me.setProduct.ClientProcess = Nothing
    Me.setProduct.CodeEnabled = False
    Me.setProduct.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setProduct.GroupBoxVisible = False
    Me.setProduct.Location = New System.Drawing.Point(10, 10)
    Me.setProduct.MaxCodeLength = 15
    Me.setProduct.MaxLabelLength = 10
    Me.setProduct.MaxNameLength = 50
    Me.setProduct.Name = "setProduct"
    Me.setProduct.Size = New System.Drawing.Size(546, 22)
    Me.setProduct.TabIndex = 0
    Me.setProduct.Text = "商品"
    Me.setProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'PcaRangeDate1
    '
    Me.PcaRangeDate1.AutoTopMargin = False
    Me.PcaRangeDate1.ClientProcess = Nothing
    Me.PcaRangeDate1.Holidays = Nothing
    Me.PcaRangeDate1.Location = New System.Drawing.Point(403, -105)
    Me.PcaRangeDate1.Name = "PcaRangeDate1"
    Me.PcaRangeDate1.RangeGroupBoxText = "Date"
    Me.PcaRangeDate1.Size = New System.Drawing.Size(343, 55)
    Me.PcaRangeDate1.TabIndex = 3
    '
    'PcaRangeDate2
    '
    Me.PcaRangeDate2.AutoTopMargin = False
    Me.PcaRangeDate2.ClientProcess = Nothing
    Me.PcaRangeDate2.Holidays = Nothing
    Me.PcaRangeDate2.Location = New System.Drawing.Point(553, -105)
    Me.PcaRangeDate2.Name = "PcaRangeDate2"
    Me.PcaRangeDate2.RangeGroupBoxText = "Date"
    Me.PcaRangeDate2.Size = New System.Drawing.Size(343, 55)
    Me.PcaRangeDate2.TabIndex = 4
    '
    'rdtWarehousingDate
    '
    Me.rdtWarehousingDate.AllowEmpty = True
    Me.rdtWarehousingDate.AutoTopMargin = False
    Me.rdtWarehousingDate.ClientProcess = Nothing
    Me.rdtWarehousingDate.DisplaySlash = True
    Me.rdtWarehousingDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.rdtWarehousingDate.Holidays = Nothing
    Me.rdtWarehousingDate.Location = New System.Drawing.Point(804, 32)
    Me.rdtWarehousingDate.MaxLabelLength = 10
    Me.rdtWarehousingDate.Name = "rdtWarehousingDate"
    Me.rdtWarehousingDate.RangeGroupBoxText = "入荷日"
    Me.rdtWarehousingDate.RangeGroupBoxVisible = False
    Me.rdtWarehousingDate.Size = New System.Drawing.Size(343, 22)
    Me.rdtWarehousingDate.TabIndex = 1
    Me.rdtWarehousingDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdtWarehousingDate.Visible = False
    '
    'setWarehouse
    '
    Me.setWarehouse.AutoTopMargin = False
    Me.setWarehouse.ClientProcess = Nothing
    Me.setWarehouse.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.setWarehouse.GroupBoxVisible = False
    Me.setWarehouse.Location = New System.Drawing.Point(564, 32)
    Me.setWarehouse.MaxCodeLength = 17
    Me.setWarehouse.MaxLabelLength = 10
    Me.setWarehouse.MaxNameLength = 0
    Me.setWarehouse.Name = "setWarehouse"
    Me.setWarehouse.Size = New System.Drawing.Size(210, 22)
    Me.setWarehouse.TabIndex = 3
    Me.setWarehouse.Text = "倉庫"
    Me.setWarehouse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFCommandSearch, Me.PcaFCommandSelect, Me.PcaFCommandClose})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 475)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1202, 28)
    Me.PcaFunctionBar1.TabIndex = 10
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFCommandSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFCommandSearch, Me.CommandSearch)
    Me.PcaFCommandSearch.FunctionKey = PCA.Controls.FunctionKey.F4
    Me.PcaFCommandSearch.Tag = Nothing
    Me.PcaFCommandSearch.Text = "再表示"
    '
    'PcaFCommandSelect
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFCommandSelect, Me.CommandSelect)
    Me.PcaFCommandSelect.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFCommandSelect.Tag = Nothing
    Me.PcaFCommandSelect.Text = "設定"
    '
    'PcaFCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFCommandClose, Me.CommandClose)
    Me.PcaFCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFCommandClose.Tag = Nothing
    Me.PcaFCommandClose.Text = "閉じる"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.CommandSearch, Me.CommandSelect, Me.CommandClose})
    '
    'CommandSearch
    '
    Me.CommandSearch.CommandId = 4
    Me.CommandSearch.CommandName = "Search"
    '
    'CommandSelect
    '
    Me.CommandSelect.CommandId = 5
    Me.CommandSelect.CommandName = "Select"
    '
    'CommandClose
    '
    Me.CommandClose.CommandId = 12
    Me.CommandClose.CommandName = "Close"
    '
    'txtLotNo
    '
    Me.txtLotNo.ClientProcess = Nothing
    Me.txtLotNo.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.txtLotNo.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.txtLotNo.LabelText = "ロットNo"
    Me.txtLotNo.LimitLength = 20
    Me.txtLotNo.Location = New System.Drawing.Point(564, 9)
    Me.txtLotNo.MaxLabelLength = 10
    Me.txtLotNo.MaxLength = 20
    Me.txtLotNo.Name = "txtLotNo"
    Me.txtLotNo.Size = New System.Drawing.Size(210, 22)
    Me.txtLotNo.TabIndex = 2
    '
    'txtShipDate
    '
    Me.txtShipDate.ClientProcess = Nothing
    Me.txtShipDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.txtShipDate.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.txtShipDate.LabelText = "出荷日"
    Me.txtShipDate.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.txtShipDate.LimitLength = 20
    Me.txtShipDate.Location = New System.Drawing.Point(10, 32)
    Me.txtShipDate.MaxLabelLength = 10
    Me.txtShipDate.MaxLength = 20
    Me.txtShipDate.MaxTextLength = 18
    Me.txtShipDate.Name = "txtShipDate"
    Me.txtShipDate.ReadOnly = True
    Me.txtShipDate.Size = New System.Drawing.Size(196, 22)
    Me.txtShipDate.TabIndex = 43
    Me.txtShipDate.TabStop = False
    Me.txtShipDate.TextBackColor = System.Drawing.SystemColors.Control
    '
    'tblTable
    '
    Me.tblTable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tblTable.BackColor = System.Drawing.Color.White
    Me.tblTable.BEKihonJoho = CType(resources.GetObject("tblTable.BEKihonJoho"), PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblTable.ContextMenu = Nothing
    Me.tblTable.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.tblTable.HeadContextMenu = Nothing
    Me.tblTable.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblTable.Location = New System.Drawing.Point(10, 64)
    Me.tblTable.Name = "tblTable"
    Me.tblTable.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblTable.Size = New System.Drawing.Size(1182, 400)
    Me.tblTable.TabIndex = 126
    '
    'SLZaikoDialog
    '
    Me.ClientSize = New System.Drawing.Size(1202, 503)
    Me.Controls.Add(Me.tblTable)
    Me.Controls.Add(Me.txtShipDate)
    Me.Controls.Add(Me.txtLotNo)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.setWarehouse)
    Me.Controls.Add(Me.PcaRangeDate2)
    Me.Controls.Add(Me.PcaRangeDate1)
    Me.Controls.Add(Me.setProduct)
    Me.Controls.Add(Me.rdtWarehousingDate)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.Name = "SLZaikoDialog"
    Me.Text = "在庫検索"
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents PcaRangeDate2 As PCA.Controls.PcaRangeDate
  Friend WithEvents PcaRangeDate1 As PCA.Controls.PcaRangeDate
  Friend WithEvents rdtWarehousingDate As PCA.Controls.PcaRangeDate
  Friend WithEvents setWarehouse As PCA.Controls.PcaCodeSet
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaFCommandSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Private components As System.ComponentModel.IContainer
  Friend WithEvents CommandSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents CommandSelect As PCA.Controls.PcaCommandItem
  Friend WithEvents CommandClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFCommandSelect As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents txtLotNo As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents txtShipDate As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents setProduct As PCA.Controls.PcaCodeSet

#End Region

End Class

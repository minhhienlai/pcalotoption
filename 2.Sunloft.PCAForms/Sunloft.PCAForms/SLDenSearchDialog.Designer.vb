﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SLDenSearchDialog
  Inherits Sunloft.PCAForms.SPClientBase

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SLDenSearchDialog))
    Me.tblTable = New PCA.TSC.Kon.Tools.TscMeisaiTable()
    Me.btnCancel = New PCA.Controls.PcaButton()
    Me.btnSelect = New PCA.Controls.PcaButton()
    Me.btnSearch = New PCA.Controls.PcaButton()
    Me.rdtDate = New PCA.Controls.PcaRangeDate()
    Me.cmbCode2 = New Sunloft.PCAControls.SLPcaComboBox()
    Me.cmbCode1 = New Sunloft.PCAControls.SLPcaComboBox()
    Me.cmbDateRange = New Sunloft.PCAControls.SLPcaComboBox()
    Me.rtxtCode1 = New PCA.Controls.PcaRangeTextBox()
    Me.rtxtCode2 = New PCA.Controls.PcaRangeTextBox()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'tblTable
    '
    Me.tblTable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tblTable.BackColor = System.Drawing.Color.White
    Me.tblTable.BEKihonJoho = CType(resources.GetObject("tblTable.BEKihonJoho"), PCA.TSC.Kon.BusinessEntity.BEKihonJoho)
    Me.tblTable.ContextMenu = Nothing
    Me.tblTable.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.tblTable.HeadContextMenu = Nothing
    Me.tblTable.ImeMode = System.Windows.Forms.ImeMode.Disable
    Me.tblTable.Location = New System.Drawing.Point(10, 86)
    Me.tblTable.Name = "tblTable"
    Me.tblTable.SelectedMultiArea = New System.Drawing.Rectangle(-1) {}
    Me.tblTable.Size = New System.Drawing.Size(991, 404)
    Me.tblTable.TabIndex = 125
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.btnCancel.Location = New System.Drawing.Point(202, 3)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(84, 24)
    Me.btnCancel.TabIndex = 122
    Me.btnCancel.Text = "キャンセル"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'btnSelect
    '
    Me.btnSelect.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnSelect.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.btnSelect.Location = New System.Drawing.Point(105, 3)
    Me.btnSelect.Name = "btnSelect"
    Me.btnSelect.Size = New System.Drawing.Size(84, 24)
    Me.btnSelect.TabIndex = 123
    Me.btnSelect.Text = "設定(F5)"
    Me.btnSelect.UseVisualStyleBackColor = True
    '
    'btnSearch
    '
    Me.btnSearch.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnSearch.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.btnSearch.Location = New System.Drawing.Point(9, 3)
    Me.btnSearch.Name = "btnSearch"
    Me.btnSearch.Size = New System.Drawing.Size(84, 24)
    Me.btnSearch.TabIndex = 124
    Me.btnSearch.Text = "再表示(F4)"
    Me.btnSearch.UseVisualStyleBackColor = True
    '
    'rdtDate
    '
    Me.rdtDate.AutoTopMargin = False
    Me.rdtDate.ClientProcess = Nothing
    Me.rdtDate.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.rdtDate.Holidays = Nothing
    Me.rdtDate.Location = New System.Drawing.Point(152, 10)
    Me.rdtDate.MaxLabelLength = 0
    Me.rdtDate.MaxTextLength = 20
    Me.rdtDate.Name = "rdtDate"
    Me.rdtDate.RangeGroupBoxText = "Date"
    Me.rdtDate.RangeGroupBoxVisible = False
    Me.rdtDate.Size = New System.Drawing.Size(315, 22)
    Me.rdtDate.TabIndex = 2
    '
    'cmbCode2
    '
    Me.cmbCode2.DropDownWidth = 140
    Me.cmbCode2.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.cmbCode2.FormattingEnabled = True
    Me.cmbCode2.Location = New System.Drawing.Point(10, 54)
    Me.cmbCode2.MustInput = False
    Me.cmbCode2.Name = "cmbCode2"
    Me.cmbCode2.Size = New System.Drawing.Size(140, 22)
    Me.cmbCode2.TabIndex = 5
    '
    'cmbCode1
    '
    Me.cmbCode1.DropDownWidth = 140
    Me.cmbCode1.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.cmbCode1.FormattingEnabled = True
    Me.cmbCode1.Location = New System.Drawing.Point(10, 31)
    Me.cmbCode1.MustInput = False
    Me.cmbCode1.Name = "cmbCode1"
    Me.cmbCode1.Size = New System.Drawing.Size(140, 22)
    Me.cmbCode1.TabIndex = 3
    '
    'cmbDateRange
    '
    Me.cmbDateRange.DropDownWidth = 140
    Me.cmbDateRange.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.cmbDateRange.FormattingEnabled = True
    Me.cmbDateRange.Location = New System.Drawing.Point(10, 9)
    Me.cmbDateRange.MustInput = False
    Me.cmbDateRange.Name = "cmbDateRange"
    Me.cmbDateRange.Size = New System.Drawing.Size(140, 22)
    Me.cmbDateRange.TabIndex = 1
    '
    'rtxtCode1
    '
    Me.rtxtCode1.AutoTopMargin = False
    Me.rtxtCode1.ClientProcess = Nothing
    Me.rtxtCode1.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.rtxtCode1.Location = New System.Drawing.Point(152, 33)
    Me.rtxtCode1.MaxLabelLength = 0
    Me.rtxtCode1.Name = "rtxtCode1"
    Me.rtxtCode1.RangeGroupBoxText = "TextBox"
    Me.rtxtCode1.RangeGroupBoxVisible = False
    Me.rtxtCode1.Size = New System.Drawing.Size(315, 22)
    Me.rtxtCode1.TabIndex = 4
    '
    'rtxtCode2
    '
    Me.rtxtCode2.AutoTopMargin = False
    Me.rtxtCode2.ClientProcess = Nothing
    Me.rtxtCode2.Font = New System.Drawing.Font("MS Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.rtxtCode2.Location = New System.Drawing.Point(152, 53)
    Me.rtxtCode2.MaxLabelLength = 0
    Me.rtxtCode2.Name = "rtxtCode2"
    Me.rtxtCode2.RangeGroupBoxText = "TextBox"
    Me.rtxtCode2.RangeGroupBoxVisible = False
    Me.rtxtCode2.Size = New System.Drawing.Size(315, 22)
    Me.rtxtCode2.TabIndex = 6
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel1.Controls.Add(Me.btnCancel)
    Me.Panel1.Controls.Add(Me.btnSelect)
    Me.Panel1.Controls.Add(Me.btnSearch)
    Me.Panel1.Location = New System.Drawing.Point(713, 497)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(289, 34)
    Me.Panel1.TabIndex = 127
    '
    'SLDenSearchDialog
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1016, 534)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.rtxtCode2)
    Me.Controls.Add(Me.rtxtCode1)
    Me.Controls.Add(Me.tblTable)
    Me.Controls.Add(Me.rdtDate)
    Me.Controls.Add(Me.cmbCode2)
    Me.Controls.Add(Me.cmbCode1)
    Me.Controls.Add(Me.cmbDateRange)
    Me.Name = "SLDenSearchDialog"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
    Me.Text = "伝票検索画面"
    Me.Panel1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tblTable As PCA.TSC.Kon.Tools.TscMeisaiTable
  Friend WithEvents btnCancel As PCA.Controls.PcaButton
  Friend WithEvents btnSelect As PCA.Controls.PcaButton
  Friend WithEvents btnSearch As PCA.Controls.PcaButton
  Friend WithEvents rdtDate As PCA.Controls.PcaRangeDate
  Friend WithEvents cmbCode2 As Sunloft.PCAControls.SLPcaComboBox
  Friend WithEvents cmbCode1 As Sunloft.PCAControls.SLPcaComboBox
  Friend WithEvents cmbDateRange As Sunloft.PCAControls.SLPcaComboBox
  Friend WithEvents rtxtCode1 As PCA.Controls.PcaRangeTextBox
  Friend WithEvents rtxtCode2 As PCA.Controls.PcaRangeTextBox
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class

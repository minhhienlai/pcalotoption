﻿Imports PCA.BaseFramework
Imports Sunloft.PCAControls

Public Class SLPcaDialog
  Inherits PCA.UITools.PcaDialog
  Public Function MustInputCheck() As Boolean
    Try
      For Each ctrl As System.Windows.Forms.Control In DirectCast(Me.Controls, System.Windows.Forms.Control.ControlCollection)
        If TypeOf ctrl Is Sunloft.PCAControls.SLPcaLabel Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaLabel).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaComboBox Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaComboBox).MustInput = True AndAlso DirectCast(ctrl, Sunloft.PCAControls.SLPcaComboBox).SelectedIndex = -1 Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaTextBox Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaTextBox).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaNumberBox Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaNumberBox).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaMoneyBox Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaMoneyBox).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaDate Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaDate).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaLabeledTextBox Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaLabeledTextBox).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaLabeledNumberBox Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaLabeledNumberBox).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaLabeledMoneyBox Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaLabeledMoneyBox).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaLabeledComboBox Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaLabeledComboBox).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        ElseIf TypeOf ctrl Is Sunloft.PCAControls.SLPcaLabeledDate Then
          If DirectCast(ctrl, Sunloft.PCAControls.SLPcaLabeledComboBox).MustInput = True AndAlso String.IsNullOrEmpty(ctrl.Text.ToString.Trim) Then
            Return False
          End If
        End If
      Next
      Return True
    Catch ex As Exception
      Return False
    End Try
  End Function

End Class

﻿'Imports Sunloft.PcaConfig
Imports PCA.BaseFramework
Imports PCA.Controls
Imports PCA.TSC.Kon.BusinessEntity
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports Sunloft.PCAIF
Imports System.Windows.Forms
Imports System.Drawing
Imports PCA.ApplicationIntegration
Imports System.Globalization

Public Class SLDenSearchDialog
  Inherits Sunloft.PCAForms.SPClientBase

#Region "Declare - 変数宣言"
  '伝票検索の種別
  Public Enum ENUM_DEN_TYPE
    ShippingSlip          '出荷伝票検索
    WarehousingSlip       '入荷伝票検索
    InventorySlip         '在庫調整伝票検索
    TransferSlip          '振替伝票検索
    PCAOrderSlip          'PCA受注伝票検索
    PCASalesSlip          'PCA売上伝票検索
    'PCAOrderSlip         'PCA発注伝票検索
    PCAPurchaseOrderSlip  'PCA発注伝票検索
    PCAPurchaseSlip       'PCA仕入伝票検索
  End Enum

  '日付検索種類ID
  Private Enum ENUM_DATE_TYPE_ID
    Date1
    Date2
    ShippingDate    '出荷日
    DeliveryDate    '納期
    WarehousingDate '入荷日
    InventoryDate   '在庫調整日
    MovementDate    '移動日
    ArrivalDate     '到着日
    ReceiptDate     '受注日
    SalesDate       '売上日
    ChargeDate      '請求日
    OrderDate       '発注日
    PurchaseDate    '仕入日
    PaymentDate     '精算日
  End Enum

  '検索対象文字列定義(日付)
  Private Const STR_SHIPPINGDATE = "出荷日"      '出荷日
  Private Const STR_DELIVERYDATE = "納期"        '納期
  Private Const STR_WAREHOUSINGDATE = "入荷日"      '入荷日
  Private Const STR_INVENTORYDATE = "調整日付"   '在庫調整日
  Private Const STR_MOVEMENTDATE = "移動日"      '移動日
  Private Const STR_ARRIVALDATE = "着日"         '到着日
  Private Const STR_RECEIPTDATE = "受注日"       '受注日
  Private Const STR_SALESDATE = "売上日"         '売上日
  Private Const STR_CHARGEDATE = "請求日"        '請求日
  Private Const STR_ORDERDATE = "発注日"         '発注日
  Private Const STR_PURCHASEDATE = "仕入日"         '仕入日
  Private Const STR_PAYMENTDATE = "精算日"       '精算日

  'コード検索種類ID
  Private Enum ENUM_CODE_TYPE_ID
    SupplierCd        '仕入先
    CustomerCd        '得意先
    DeliverCd         '出荷先
    DirectDeliverCd   '直送先
    PersonCd          '担当者
    DepartmentCd      '部門
    ProjectCd         'プロジェクト
    SummaryCd         '摘要、調整理由
    ProductCd         '商品
    WarehouseCd       '倉庫
    WarehouseCd2      '倉庫(振替伝票用)
    SlipNo1           '伝票検索1
    SlipNo2           '伝票検索2
  End Enum

  'ヘッダセルインデックス
  Private Enum enmHeaderCell
    KeyDate
    DataType
    DenNo
    DenNo2
    Code1
    Code1Name
    Code2
    Code2Name
    Amount
    Memo
    ID
  End Enum

  '検索対象文字列定義(コード)
  Private Const STR_SHIPPINGCD = "出荷先"        '出荷先
  Private Const STR_PERSONCD = "担当者"          '担当者
  Private Const STR_DEPARTMENTCD = "部門"        '部門
  Private Const STR_PROJECTCD = "プロジェクト"   'プロジェクト
  Private Const STR_SUMMARYCD = "摘要"           '摘要
  Private Const STR_SLIPNO1 = "伝票No"           '伝票番号1
  Private Const STR_SLIPNO2 = "伝票No2"          '伝票番号2
  Private Const STR_ORDERCD = "発注先"           '発注先
  Private Const STR_SUPPLIERCD = "仕入先"        '仕入先
  Private Const STR_PAYMENTCD = "支払先"         '支払先
  Private Const STR_PRODUCTCD = "商品"           '商品
  Private Const STR_WAREHOUSECD = "倉庫"         '倉庫
  Private Const STR_REASONCD = "調整理由"        '調整理由
  Private Const STR_WHFROMCD = "移動元倉庫"      '移動元倉庫
  Private Const STR_WHTOCD = "移動先倉庫"        '移動先倉庫
  Private Const STR_CUSTOMERCD = "得意先"        '得意先
  Private Const STR_DELIVERYCD = "直送先"
  Private Const STR_BILLCD = "請求先"

  'Search Result: DB Field Name 2015/12/8 TNOGUCHI
  Private Const HEADER_COLUMN_PREFIX As String = "H_"
  Private Const BODY_COLUMN_PREFIX As String = "B_"

  Private Const DBITEM_DATE As String = "keydate"
  Private Const DBITEM_DENNO As String = "denno"
  Private Const DBITEM_DENNO2 As String = "denno2"
  Private Const DBITEM_CODE As String = "keycode"
  Private Const DBITEM_NAME As String = "keyname"
  Private Const DBITEM_CODE2 As String = "keycode2"
  Private Const DBITEM_NAME2 As String = "keyname2"
  Private Const DBITEM_AMOUNT As String = "amount"
  Private Const DBITEM_MEMO As String = "memo"
  Private Const DBITEM_ID As String = "keyid"
  Private Const DBITEM_DATATYPE As String = "datatype"

  Friend WithEvents TscMeisaiTable1 As PCA.TSC.Kon.Tools.TscMeisaiTable

  Public Property connector As IIntegratedApplication
  Private Property type As ENUM_DEN_TYPE
  Private m_AMS1Class As AMS1
  'Private selectCommand As ICustomCommand
  Private _conf As Sunloft.PcaConfig
  Private m_DateConditions As New List(Of SearchCondition)
  Private m_ItemConditions As New List(Of SearchCondition)
  Private m_SearchConfig As New SearchConfig
  Private m_strReturnVal As String
  Private m_intRowIndex As Integer = -1
  'Dim strCustomDateColumn As String = "日付", strCustomSlipColumn As String = "伝票No", strCustomSlipColumn2 As String = "伝票No2", strCustomcodeColumn As String = "コード1", _
  '                           strCustomCodeNameColumn As String = "コード名1", strCustomCodeColumn2 As String = "コード2", _
  '                            strCustomCodeNameColumn2 As String = "コード名2", strCustomMemoColumn As String = "摘要", strCustomIDColumn As String = "ID"
#End Region

#Region "Initialize - 初期化処理"
  'Sub New(ByVal myType As ENUM_DEN_TYPE, myConnector As IIntegratedApplication)
  Sub New(myConnector As IIntegratedApplication)
    Try
      InitializeComponent()
      _conf = New Sunloft.PcaConfig
      connector = myConnector
      'type = myType
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitForm(ByVal myType As ENUM_DEN_TYPE)
    Try
      rdtDate.FromDate = Date.Today.AddDays(-7)
      rdtDate.ToDate = Date.Today

      m_AMS1Class = New AMS1(connector)
      m_AMS1Class.ReadAll() 'Get number of digits for fields

      m_SearchConfig.DenNoTitle = "伝票No"
      m_SearchConfig.DenNo2Title = "伝票No2"
      m_SearchConfig.isShowCode2 = False
      m_SearchConfig.MemoTitle = "摘要"
      m_SearchConfig.DateTitle = "日付"
      m_SearchConfig.DataTypeTitle = ""
      m_SearchConfig.DefaultCode2ID = 1

      Select Case myType
        Case ENUM_DEN_TYPE.ShippingSlip
          Me.Text = "出荷伝票検索"
          m_SearchConfig.CodeTitle = "得意先コード"
          m_SearchConfig.NameTitle = "得意先名"
          m_SearchConfig.DataTypeTitle = "伝票区分"
          m_SearchConfig.TemplateName = "GET_SL_SYKH_SEARCH"

          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date1, .Title = STR_SHIPPINGDATE, .DBName = "sl_sykh_uribi"})
          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date2, .Title = STR_DELIVERYDATE, .DBName = "sl_sykh_nouki"})

          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.CustomerCd, .Title = STR_SHIPPINGCD, .DBName = "sl_sykh_tcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.PersonCd, .Title = STR_PERSONCD, .DBName = "sl_sykh_jtan"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.DepartmentCd, .Title = STR_DEPARTMENTCD, .DBName = "sl_sykh_jbmn"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.ProjectCd, .Title = STR_PROJECTCD, .DBName = "sl_sykh_pjcode"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SummaryCd, .Title = STR_SUMMARYCD, .DBName = "sl_sykh_tekcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo1, .Title = STR_SLIPNO1, .DBName = "sl_sykh_denno"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo2, .Title = STR_SLIPNO2, .DBName = "sl_sykh_denno2"})

        Case (ENUM_DEN_TYPE.WarehousingSlip)
          Me.Text = "入荷伝票検索"
          m_SearchConfig.CodeTitle = "仕入先コード"
          m_SearchConfig.NameTitle = "仕入先名"
          m_SearchConfig.DataTypeTitle = "伝票区分"
          m_SearchConfig.TemplateName = "GET_SL_NYKH_SEARCH"
          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date1, .Title = STR_WAREHOUSINGDATE, .DBName = "sl_nykh_uribi"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.CustomerCd, .Title = STR_SUPPLIERCD, .DBName = "sl_nykh_tcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.PersonCd, .Title = STR_PERSONCD, .DBName = "sl_nykh_jtan"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.DepartmentCd, .Title = STR_DEPARTMENTCD, .DBName = "sl_nykh_jbmn"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.ProjectCd, .Title = STR_PROJECTCD, .DBName = "sl_nykh_pjcode"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SummaryCd, .Title = STR_SUMMARYCD, .DBName = "sl_nykh_tekcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo1, .Title = STR_SLIPNO1, .DBName = "sl_nykh_denno"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo2, .Title = STR_SLIPNO2, .DBName = "sl_nykh_denno2"})

        Case ENUM_DEN_TYPE.InventorySlip
          Me.Text = "在庫調整伝票検索"
          m_SearchConfig.CodeTitle = "商品コード"
          m_SearchConfig.NameTitle = "商品名"
          m_SearchConfig.DataTypeTitle = "調整区分"
          m_SearchConfig.TemplateName = "GET_SL_ZADJ_SEARCH"

          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date1, .Title = STR_INVENTORYDATE, .DBName = "sl_zadj_date"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.ProductCd, .Title = STR_PRODUCTCD, .DBName = "sl_zadj_scd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.WarehouseCd, .Title = STR_WAREHOUSECD, .DBName = "sl_zadj_souko"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SummaryCd, .Title = STR_SUMMARYCD, .DBName = "sl_zadj_tekcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo1, .Title = STR_SLIPNO1, .DBName = "sl_zadj_denno"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo2, .Title = STR_SLIPNO2, .DBName = "sl_zadj_denno2"})

        Case ENUM_DEN_TYPE.TransferSlip
          Me.Text = "振替伝票検索"
          m_SearchConfig.CodeTitle = "商品コード"
          m_SearchConfig.NameTitle = "商品名"
          m_SearchConfig.TemplateName = "GET_SL_WHT_SEARCH"

          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date1, .Title = STR_MOVEMENTDATE, .DBName = "sl_wht_date"})
          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date2, .Title = STR_ARRIVALDATE, .DBName = "sl_wht_dlvdate"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.ProductCd, .Title = STR_SUPPLIERCD, .DBName = "sl_wht_scd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.WarehouseCd, .Title = STR_WHFROMCD, .DBName = "sl_wht_soukof"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.WarehouseCd2, .Title = STR_WHTOCD, .DBName = "sl_wht_soukot"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SummaryCd, .Title = STR_SUMMARYCD, .DBName = "sl_wht_tekcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo1, .Title = STR_SLIPNO1, .DBName = "sl_wht_denno"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo2, .Title = STR_SLIPNO2, .DBName = "sl_wht_denno2"})

        Case ENUM_DEN_TYPE.PCAOrderSlip
          Me.Text = "PCA受注伝票検索"
          m_SearchConfig.CodeTitle = "得意先コード"
          m_SearchConfig.NameTitle = "得意先名"
          m_SearchConfig.TemplateName = "GET_JUCH_SEARCH"

          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date1, .Title = STR_RECEIPTDATE, .DBName = "juch_jucbi"})
          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date2, .Title = STR_DELIVERYDATE, .DBName = "juch_noki"})

          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.CustomerCd, .Title = STR_CUSTOMERCD, .DBName = "juch_tcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.DirectDeliverCd, .Title = STR_DELIVERYCD, .DBName = "juch_ncd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.PersonCd, .Title = STR_PERSONCD, .DBName = "juch_jtan"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.DepartmentCd, .Title = STR_DEPARTMENTCD, .DBName = "juch_jbmn"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.ProjectCd, .Title = STR_PROJECTCD, .DBName = "juch_pjcode"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SummaryCd, .Title = STR_SUMMARYCD, .DBName = "juch_tekcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo1, .Title = STR_SLIPNO1, .DBName = "juch_jno"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo2, .Title = STR_SLIPNO2, .DBName = "juch_jno2"})

        Case ENUM_DEN_TYPE.PCASalesSlip
          Me.Text = "PCA売上伝票検索"
          m_SearchConfig.CodeTitle = "得意先コード"
          m_SearchConfig.NameTitle = "得意先名"
          m_SearchConfig.DataTypeTitle = "伝票区分"
          m_SearchConfig.TemplateName = "GET_SYKH_SEARCH"

          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date1, .Title = STR_SALESDATE, .DBName = "sykh_uribi"})
          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date2, .Title = STR_CHARGEDATE, .DBName = "sykh_seibi"})

          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.CustomerCd, .Title = STR_CUSTOMERCD, .DBName = "sykh_tcd"})
          'm_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.CustomerCd, .Title = STR_BILLCD, .DBName = "sykh_ocd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.DirectDeliverCd, .Title = STR_DELIVERYCD, .DBName = "sykh_ncd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.PersonCd, .Title = STR_PERSONCD, .DBName = "sykh_jtan"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.DepartmentCd, .Title = STR_DEPARTMENTCD, .DBName = "sykh_jbmn"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.ProjectCd, .Title = STR_PROJECTCD, .DBName = "sykh_pjcode"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SummaryCd, .Title = STR_SUMMARYCD, .DBName = "sykh_tekcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo1, .Title = STR_SLIPNO1, .DBName = "sykh_denno"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo2, .Title = STR_SLIPNO2, .DBName = "sykh_denno2"})

        Case ENUM_DEN_TYPE.PCAPurchaseOrderSlip
          Me.Text = "PCA発注伝票検索"
          m_SearchConfig.CodeTitle = "仕入先コード"
          m_SearchConfig.NameTitle = "仕入先名"
          m_SearchConfig.TemplateName = "GET_HACH_SEARCH"

          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date1, .Title = STR_ORDERDATE, .DBName = "hach_jucbi"})
          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date2, .Title = STR_DELIVERYDATE, .DBName = "hach_noki"})

          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.CustomerCd, .Title = STR_ORDERCD, .DBName = "hach_tcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.PersonCd, .Title = STR_PERSONCD, .DBName = "hach_jtan"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.DepartmentCd, .Title = STR_DEPARTMENTCD, .DBName = "hach_jbmn"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.ProjectCd, .Title = STR_PROJECTCD, .DBName = "hach_pjcode"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SummaryCd, .Title = STR_SUMMARYCD, .DBName = "hach_tekcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo1, .Title = STR_SLIPNO1, .DBName = "hach_jno"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo2, .Title = STR_SLIPNO2, .DBName = "hach_jno2"})

        Case ENUM_DEN_TYPE.PCAPurchaseSlip
          Me.Text = "PCA仕入伝票検索"
          m_SearchConfig.CodeTitle = "仕入先コード"
          m_SearchConfig.NameTitle = "仕入先名"
          m_SearchConfig.DataTypeTitle = "伝票区分"
          m_SearchConfig.TemplateName = "GET_NYKH_SEARCH"

          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date1, .Title = STR_PURCHASEDATE, .DBName = "nykh_uribi"})
          m_DateConditions.Add(New SearchCondition With {.ID = ENUM_DATE_TYPE_ID.Date2, .Title = STR_PAYMENTDATE, .DBName = "nykh_seibi"})

          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.CustomerCd, .Title = STR_SUPPLIERCD, .DBName = "nykh_tcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.DirectDeliverCd, .Title = STR_PAYMENTCD, .DBName = "nykh_ocd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.PersonCd, .Title = STR_PERSONCD, .DBName = "nykh_jtan"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.DepartmentCd, .Title = STR_DEPARTMENTCD, .DBName = "nykh_jbmn"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.ProjectCd, .Title = STR_PROJECTCD, .DBName = "nykh_pjcode"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SummaryCd, .Title = STR_SUMMARYCD, .DBName = "nykh_tekcd"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo1, .Title = STR_SLIPNO1, .DBName = "nykh_denno"})
          m_ItemConditions.Add(New SearchCondition With {.ID = ENUM_CODE_TYPE_ID.SlipNo2, .Title = STR_SLIPNO2, .DBName = "nykh_denno2"})
      End Select
      AddItemToCmb({cmbDateRange}, m_DateConditions)
      AddItemToCmb({cmbCode1, cmbCode2}, m_ItemConditions)
      cmbCode2.SelectedIndex = m_SearchConfig.DefaultCode2ID

      With tblTable
        .DataRowCount = 99

        '入力用青枠の表示
        .InputFrameMode = True
        .SelectRowMode = True
        InitTableHeader()     'ヘッダー部
        InitTableBody()       'ボディ部

        .UpdateTable()
        '.UpDownKeyAsTab = True
        .BodyColumns.DefaultStyle.BackColor = Color.White
        .BodyColumns.DefaultAltStyle.BackColor = Color.White
      End With
    Catch ex As Exception

    End Try
  End Sub
  '***********************************************************																														
  'Name          : InitTableHeader
  'Content       : Initialize PCA Table Header Column & Cell
  'Return Value  : none
  'Argument      : none
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub InitTableHeader()
    Try
      Dim isHidden As Boolean
      tblTable.HeadTextHeight = 1
      tblTable.HeadColumns.DefaultStyle.ForeColor = DefaultLabelStyle.TableTitleForeColor
      tblTable.HeadColumns.DefaultStyle.BackColor = DefaultLabelStyle.TableTitleBackColor

      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_DATE, "", False, 0.8))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_DATATYPE, "", False, 0.5))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_DENNO, "", False, 0.8))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_DENNO2, "", False, 0.8))

      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_CODE, ""))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_NAME, "", False, 2.0))
      If (m_SearchConfig.isShowCode2 = True) Then
        isHidden = False
      Else
        isHidden = True
      End If
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_CODE2, "", isHidden))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_NAME2, "", isHidden))
      If (m_SearchConfig.isShowAmount = True) Then
        isHidden = False
      Else
        isHidden = True
      End If
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_AMOUNT, "", isHidden))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_MEMO, ""))
      tblTable.HeadColumns.Add(SetHeaderColumn(DBITEM_ID, "", True))
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  '***********************************************************																														
  'Name          : SetHeaderColumn
  'Content       : Setting PCA Table Header Column & Cell
  'Return Value  : PCAColumn
  'Argument      : strColumnName -> Column Name, strCellName -> Cell Name
  '                isHidden -> Is Hidden Column Set True, txtWidth -> Column Width
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Function SetHeaderColumn(ByVal strColumnName As String, ByVal strCellName As String, Optional ByVal isHidden As Boolean = False, Optional ByVal txtWidth As Single = 1.0) As PcaColumn
    Dim column As PcaColumn
    Dim cell As PcaCell
    column = New PcaColumn(HEADER_COLUMN_PREFIX & strColumnName, txtWidth)
    Try
      column.CanResize = True
      column.AllowUserResize = True
      cell = tblTable.PrepareFixTextCell(strCellName)
      cell.HideSelection = isHidden
      column.Cells.Add(cell)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return column
  End Function

  '***********************************************************																														
  'Name          : SetBodyColumn
  'Content       : Setting PCA Table Body Column & Cell
  'Return Value  : PCAColumn
  'Argument      : strColumnName -> Column Name, strCellName -> Cell Name
  '                CellType -> Cell Type, isHidden -> Is Hidden Column Set True, txtWidth -> Column Width
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Function SetBodyColumn(ByVal strColumnName As String, ByVal strCellName As String, ByVal CellType As PCA.Controls.CellType, Optional ByVal isHidden As Boolean = False, Optional ByVal txtWidth As Single = 1.0) As PcaColumn
    Dim column As PcaColumn
    Dim cell As PcaCell
    column = New PcaColumn(BODY_COLUMN_PREFIX & strColumnName, txtWidth)
    Try
      column.CanResize = True
      column.AllowUserResize = True
      cell = New PcaCell(strCellName, CellType)
      cell.HideSelection = isHidden
      cell.EditMode = False
      column.Cells.Add(cell)
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return column
  End Function

  '***********************************************************																														
  'Name          : InitTableBody
  'Content       : Initialize PCA Table Body Column & Cell
  'Return Value  : none
  'Argument      : none
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub InitTableBody()
    Try
      Dim isHidden As Boolean

      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_DATE, DBITEM_DATE, CellType.Text, False, 0.8))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_DATATYPE, DBITEM_DATATYPE, CellType.Text, False, 0.5))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_DENNO, DBITEM_DENNO, CellType.Text, False, 0.8))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_DENNO2, DBITEM_DENNO2, CellType.Text, False, 0.8))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_CODE, DBITEM_CODE, CellType.Text))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_NAME, DBITEM_NAME, CellType.Text, False, 2.0))
      If (m_SearchConfig.isShowCode2 = True) Then
        isHidden = False
      Else
        isHidden = True
      End If
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_CODE2, DBITEM_CODE2, CellType.Text, isHidden))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_NAME2, DBITEM_NAME2, CellType.Text, isHidden))
      If (m_SearchConfig.isShowAmount = True) Then
        isHidden = False
      Else
        isHidden = True
      End If
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_AMOUNT, DBITEM_AMOUNT, CellType.Text, isHidden))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_MEMO, DBITEM_MEMO, CellType.Text))
      tblTable.BodyColumns.Add(SetBodyColumn(DBITEM_ID, DBITEM_ID, CellType.Text))
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  '***********************************************************																														
  'Name          : AddItemToCmb
  'Content       : Add to ComboBox Select Item 
  'Return Value  : none
  'Argument      : cmb -> setting ComboBox(multi), searchItemList -> ComboBox Item List
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub AddItemToCmb(ByVal cmb As PCA.Controls.PcaComboBox(), ByVal searchItemList As List(Of SearchCondition))
    Try
      Dim dicItemList As New Dictionary(Of Integer, String)
      For intIdx As Integer = 0 To searchItemList.Count - 1
        dicItemList.Add(searchItemList(intIdx).ID, searchItemList(intIdx).Title)
      Next

      '指定したComboBox分繰り返し
      For Each cmbItem As PCA.Controls.PcaComboBox In cmb
        cmbItem.DataSource = New BindingSource(dicItemList, Nothing)
        cmbItem.DisplayMember = "Value"
        cmbItem.ValueMember = "Key"
      Next

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub
#End Region

#Region "Button Event"
  Public Function returnValue() As String
    Return m_strReturnVal
  End Function

  Private Sub SLDenSearchDialog_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
    If (m_SearchConfig.TemplateName <> "") Then Search_SlipData()
  End Sub

  Private Sub SLDenSearchDialog_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
    Select Case e.KeyCode
      Case Keys.F4
        btnSearch_Click(sender, e)
      Case Keys.F5
        btnSelect_Click(sender, e)
      Case Keys.Escape
        btnCancel_Click(sender, e)
    End Select
  End Sub

  '***********************************************************																														
  'Name          : btnSelect_Click
  'Content       : Select Item -> return
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
    If (m_intRowIndex = -1) Then
      Sunloft.Message.DisplayBox.ShowNotify("データが選択されていません")
      Exit Sub
    End If
    m_strReturnVal = tblTable.GetCellValue(m_intRowIndex, DBITEM_DENNO).ToString
    DialogResult = System.Windows.Forms.DialogResult.OK
    Me.Close()
  End Sub

  '***********************************************************																														
  'Name          : btnCancel_Click
  'Content       : Cancel & Close Form
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
    m_strReturnVal = ""
    DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.Close()
  End Sub

  '***********************************************************																														
  'Name          : btnSearch_Click
  'Content       : Search Slip Data From SQL Template And Show PCATable
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
    Try
      Search_SlipData()
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  ''' <summary>
  ''' Search Denpyo Slip Data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub Search_SlipData()
    Dim intCode1 As Integer
    Dim intCode2 As Integer
    Dim intRow As Integer = 0
    Dim intDateSelectedIndex As Integer
    Dim selectCommand As ICustomCommand
    Try
      Dim reader As ICustomDataReader
      btnSearch.Focus()
      m_intRowIndex = -1

      If Integer.TryParse(cmbDateRange.SelectedIndex.ToString, intDateSelectedIndex) = False Then intDateSelectedIndex = 0
      Dim intItem1SelectedIndex As Integer
      If Integer.TryParse(cmbCode1.SelectedIndex.ToString, intItem1SelectedIndex) = False Then intItem1SelectedIndex = 0
      Dim intItem2SelectedIndex As Integer
      If Integer.TryParse(cmbCode2.SelectedIndex.ToString, intItem2SelectedIndex) = False Then intItem2SelectedIndex = 0

      selectCommand = connector.CreateIsolatedCommand(_conf.PcaAddinID.Trim & "%" & m_SearchConfig.TemplateName)
      selectCommand.Parameters("@DateFrom").SetValue(Integer.Parse(Format(rdtDate.FromDate, "yyyyMMdd")))
      selectCommand.Parameters("@DateTo").SetValue(Integer.Parse(Format(rdtDate.ToDate, "yyyyMMdd")))
      selectCommand.Parameters("@DateType").SetValue(m_DateConditions(intDateSelectedIndex).DBName)
      selectCommand.Parameters("@Code1StringFrom").SetValue(rtxtCode1.TextFrom)
      selectCommand.Parameters("@Code1StringTo").SetValue(rtxtCode1.TextTo)
      If Integer.TryParse(rtxtCode1.TextFrom.ToString, intCode1) = False Then intCode1 = 0
      If Integer.TryParse(rtxtCode1.TextTo.ToString, intCode2) = False Then intCode2 = 0
      selectCommand.Parameters("@Code1IntFrom").SetValue(intCode1)
      selectCommand.Parameters("@Code1IntTo").SetValue(intCode2)
      selectCommand.Parameters("@Code1Type").SetValue(m_ItemConditions(intItem1SelectedIndex).DBName)
      selectCommand.Parameters("@Code2StringFrom").SetValue(rtxtCode2.TextFrom)
      selectCommand.Parameters("@Code2StringTo").SetValue(rtxtCode2.TextTo)
      If Integer.TryParse(rtxtCode2.TextFrom.ToString, intCode1) = False Then intCode1 = 0
      If Integer.TryParse(rtxtCode2.TextFrom.ToString, intCode2) = False Then intCode2 = 0
      selectCommand.Parameters("@Code2IntFrom").SetValue(intCode1)
      selectCommand.Parameters("@Code2IntTo").SetValue(intCode2)
      selectCommand.Parameters("@Code2Type").SetValue(m_ItemConditions(intItem2SelectedIndex).DBName)
      reader = selectCommand.ExecuteReader

      'table reset - テーブル情報の再設定
      tblTable.DataRowCount = 99
      tblTable.UpdateTable()
      ResetTableConfig(intDateSelectedIndex)
      'Display result
      While (reader.Read() = True)
        tblTable.SetCellValue(intRow, DBITEM_DATE, Date.ParseExact(reader.GetValue(DBITEM_DATE).ToString.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString)
        If String.IsNullOrEmpty(m_SearchConfig.DataTypeTitle) = False Then tblTable.SetCellValue(intRow, DBITEM_DATATYPE, reader.GetValue(DBITEM_DATATYPE).ToString.Trim())
        tblTable.SetCellValue(intRow, DBITEM_DENNO, reader.GetValue(DBITEM_DENNO).ToString.Trim())
        tblTable.SetCellValue(intRow, DBITEM_DENNO2, reader.GetValue(DBITEM_DENNO2).ToString.Trim())
        tblTable.SetCellValue(intRow, DBITEM_CODE, reader.GetValue(DBITEM_CODE).ToString.Trim())
        tblTable.SetCellValue(intRow, DBITEM_NAME, reader.GetValue(DBITEM_NAME).ToString.Trim())
        If m_SearchConfig.isShowCode2 = True Then
          tblTable.SetCellValue(intRow, DBITEM_CODE2, reader.GetValue(DBITEM_CODE2).ToString.Trim())
          tblTable.SetCellValue(intRow, DBITEM_NAME2, reader.GetValue(DBITEM_NAME2).ToString.Trim())
        End If
        If m_SearchConfig.isShowAmount = True Then tblTable.SetCellValue(intRow, DBITEM_AMOUNT, reader.GetValue(DBITEM_AMOUNT).ToString.Trim())
        tblTable.SetCellValue(intRow, DBITEM_MEMO, reader.GetValue(DBITEM_MEMO).ToString.Trim())
        tblTable.SetCellValue(intRow, DBITEM_ID, reader.GetValue(DBITEM_ID).ToString.Trim())
        tblTable.BodyColumns(BODY_COLUMN_PREFIX & DBITEM_ID).TextWidth = 0
        intRow = intRow + 1
      End While
      tblTable.DataRowCount = intRow
      tblTable.BodyRowCount = intRow
      'tblTable.UpdateTable()
      reader.Close()
      reader.Dispose()
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub
  '***********************************************************																														
  'Name          : ResetTableConfig
  'Content       : Reset to PCATable Header & Body Column(Set Column Name, Hidden Column) 
  'Argument      : intDateSelectedIndex -> Date Combo Selected Index
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub ResetTableConfig(intDateSelectedIndex As Integer)
    Try
      tblTable.ClearCellValues()
      For Each item As PcaColumn In tblTable.HeadColumns
        item.Cells.Clear()
      Next
      tblTable.HeadColumns.Item(enmHeaderCell.KeyDate).Cells.Add(tblTable.PrepareFixTextCell(m_DateConditions(intDateSelectedIndex).Title))
      tblTable.HeadColumns.Item(enmHeaderCell.DataType).Cells.Add(tblTable.PrepareFixTextCell(m_SearchConfig.DataTypeTitle))
      tblTable.HeadColumns.Item(enmHeaderCell.DenNo).Cells.Add(tblTable.PrepareFixTextCell(m_SearchConfig.DenNoTitle))
      tblTable.HeadColumns.Item(enmHeaderCell.DenNo2).Cells.Add(tblTable.PrepareFixTextCell(m_SearchConfig.DenNo2Title))
      tblTable.HeadColumns.Item(enmHeaderCell.Code1).Cells.Add(tblTable.PrepareFixTextCell(m_SearchConfig.CodeTitle))
      tblTable.HeadColumns.Item(enmHeaderCell.Code1Name).Cells.Add(tblTable.PrepareFixTextCell(m_SearchConfig.NameTitle))
      tblTable.HeadColumns.Item(enmHeaderCell.Code2).Cells.Add(tblTable.PrepareFixTextCell(m_SearchConfig.Code2Title))
      tblTable.HeadColumns.Item(enmHeaderCell.Code2Name).Cells.Add(tblTable.PrepareFixTextCell(m_SearchConfig.Name2Title))
      tblTable.HeadColumns.Item(enmHeaderCell.Amount).Cells.Add(tblTable.PrepareFixTextCell(m_SearchConfig.AmountTitle))
      tblTable.HeadColumns.Item(enmHeaderCell.Memo).Cells.Add(tblTable.PrepareFixTextCell(m_SearchConfig.MemoTitle))

      If (String.IsNullOrEmpty(m_SearchConfig.DataTypeTitle) = True) Then
        tblTable.HeadColumns.Item(enmHeaderCell.DataType).TextWidth = 0
        tblTable.BodyColumns(BODY_COLUMN_PREFIX & DBITEM_DATATYPE).TextWidth = 0
      End If
      If (m_SearchConfig.isShowCode2 = False) Then
        tblTable.HeadColumns.Item(enmHeaderCell.Code2).TextWidth = 0
        tblTable.HeadColumns.Item(enmHeaderCell.Code2Name).TextWidth = 0
        tblTable.BodyColumns(BODY_COLUMN_PREFIX & DBITEM_CODE2).TextWidth = 0
        tblTable.BodyColumns(BODY_COLUMN_PREFIX & DBITEM_NAME2).TextWidth = 0
      End If
      If (m_SearchConfig.isShowAmount = False) Then
        tblTable.HeadColumns.Item(enmHeaderCell.Amount).TextWidth = 0
        tblTable.BodyColumns(BODY_COLUMN_PREFIX & DBITEM_AMOUNT).TextWidth = 0
      End If
      tblTable.HeadColumns.Item(enmHeaderCell.ID).TextWidth = 0
      tblTable.BodyColumns(BODY_COLUMN_PREFIX & DBITEM_ID).TextWidth = 0
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub
#End Region

#Region "Input Event"
  '***********************************************************																														
  'Name          : StandardlizeNoOfDigit
  'Content       : Get Code Length -> Zero Press of Code 
  'Argument      : type -> Slip Type, codeName -> Select ComboBox, rtxtCodename -> Ranged Text Box 
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub StandardlizeNoOfDigit(codeName As Sunloft.PCAControls.SLPcaComboBox, rtxtCodename As PCA.Controls.PcaRangeTextBox)
    Try
      Dim intCodeLen As Integer = 0
      'Get Code 1,2 limit length
      Select Case CInt(codeName.SelectedValue)
      Case ENUM_CODE_TYPE_ID.ProductCd
          intCodeLen = m_AMS1Class.ams1_ProductLength
      Case ENUM_CODE_TYPE_ID.CustomerCd
          intCodeLen = m_AMS1Class.ams1_CustomerLength
      Case ENUM_CODE_TYPE_ID.SupplierCd
          intCodeLen = m_AMS1Class.ams1_SupplierLength
      Case ENUM_CODE_TYPE_ID.DepartmentCd
          intCodeLen = m_AMS1Class.ams1_DepartmentLength
      Case ENUM_CODE_TYPE_ID.PersonCd
          intCodeLen = m_AMS1Class.ams1_PersonLength
      Case ENUM_CODE_TYPE_ID.SummaryCd
          intCodeLen = m_AMS1Class.ams1_MemoLength
      Case ENUM_CODE_TYPE_ID.WarehouseCd
          intCodeLen = m_AMS1Class.ams1_WarehouseLength
      End Select

      If (intCodeLen <> 0) Then
        rtxtCodename.TextFrom = standardlizeCode(rtxtCodename.TextFrom, intCodeLen)
        rtxtCodename.TextTo = standardlizeCode(rtxtCodename.TextTo, intCodeLen)
      End If

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

  '***********************************************************																														
  'Name          : standardlizeCode
  'Content       : Add 0 in front of code text that fits code's number of digit
  'Argument      : strCodeText -> code, intNoOfDigit -> Code Length 
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Function standardlizeCode(ByVal strCodeText As String, ByVal intNoOfDigit As Integer) As String
    Try
      If IsNumeric(strCodeText) Then
        If strCodeText.Length < intNoOfDigit Then
          For intCount As Integer = 1 To intNoOfDigit - strCodeText.Length
            strCodeText = "0" & strCodeText
          Next
        End If
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return strCodeText
  End Function

  '***********************************************************																														
  'Name          : rtxtCode_Leave
  'Content       : Add 0 in front of code text that fits code's number of digit
  'Argument      : 
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub rtxtCode_Leave(sender As System.Object, e As System.EventArgs) Handles rtxtCode1.LeaveFrom, rtxtCode2.LeaveFrom, rtxtCode1.Validated, rtxtCode2.Validated
    Try
      Select Case sender.GetType.ToString
        Case "PCA.Controls.PcaRangeTextBox"
          Dim rtxt As PcaRangeTextBox = CType(sender, PcaRangeTextBox)
          If (rtxt.Name = "rtxtCode1") Then
            StandardlizeNoOfDigit(cmbCode1, rtxtCode1)
          Else
            StandardlizeNoOfDigit(cmbCode2, rtxtCode2)
          End If
      End Select
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub tblTable_DoubleClick(sender As Object, e As System.EventArgs) Handles tblTable.DoubleClick
    btnSelect_Click(sender, e)
  End Sub

  '***********************************************************																														
  'Name          : tblTable_EnterInputRow
  'Content       : Set RowIndex
  'Argument      : 
  'Created date  : 2015/12/09  by tnoguchi
  'Modified date :        by       Content: 			
  '***********************************************************	
  Private Sub tblTable_EnterInputRow(sender As Object, args As PCA.Controls.InputRowEventArgs) Handles tblTable.EnterInputRow
    Try
      m_intRowIndex = args.RowIndex
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
  End Sub
#End Region

#Region "Class"
  Private Class SearchConfig
    Public Property TemplateName As String
    Public Property CodeTitle As String
    Public Property NameTitle As String
    Public Property Code2Title As String
    Public Property Name2Title As String
    Public Property DateTitle As String
    Public Property DenNoTitle As String
    Public Property DenNo2Title As String
    Public Property MemoTitle As String
    Public Property AmountTitle As String
    Public Property DataTypeTitle As String
    Public Property isShowCode2 As Boolean = False
    Public Property isShowAmount As Boolean = False
    Public Property DefaultCode2ID As Integer
  End Class

  Private Class SearchCondition
    Public Property ID As Integer
    Public Property DBName As String
    Public Property Title As String
  End Class
#End Region

#Region "SearchDenpyo"
  ''' <summary>
  ''' Search Slip And Get Result
  ''' </summary>
  ''' <returns>Slip ID</returns>
  ''' <remarks>2015/12/12</remarks>
  Public Function ShowReferDialogSlip(ByVal myType As ENUM_DEN_TYPE) As String
    'Dim denSearchDialog As SLDenSearchDialog = New SLDenSearchDialog(connector)
    Dim strReturnID As String = String.Empty
    Try
      InitForm(myType)
      Select Case Me.ShowDialog
        Case System.Windows.Forms.DialogResult.OK
          strReturnID = Me.returnValue()
      End Select
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return strReturnID
  End Function

  ' ''' <summary>
  ' ''' Search PCA Purchase Order Slip / 発注伝票検索
  ' ''' </summary>
  ' ''' <returns>Slip ID</returns>
  ' ''' <remarks>2015/12/12</remarks>
  'Private Function ReferDialogPCAPurchaseOrder() As String
  '  Dim newDenSearchDialog As SLDenSearchDialog = New SLDenSearchDialog(connector)
  '  Dim strReturnID As String = String.Empty
  '  Try
  '    Select Case newDenSearchDialog.ShowDialog()
  '      Case Windows.Forms.DialogResult.OK
  '        strReturnID = newDenSearchDialog.returnValue()
  '    End Select
  '  Catch ex As Exception
  '    Sunloft.Message.DisplayBox.ShowCritical(ex)
  '  End Try
  '  Return strReturnID
  'End Function
  ' ''' <summary>
  ' ''' Search PCA Purchase Slip / 仕入伝票検索
  ' ''' </summary>
  ' ''' <param name="connector">DBConnection</param>
  ' ''' <returns>Slip ID</returns>
  ' ''' <remarks>2015/12/12</remarks>
  'Private Function ReferDialogPCAPurchase() As String
  '  Dim newDenSearchDialog As SLDenSearchDialog = New SLDenSearchDialog(connector)
  '  Dim strReturnID As String = String.Empty
  '  Try
  '    Select Case Me.ShowDialog()
  '      Case Windows.Forms.DialogResult.OK
  '        strReturnID = newDenSearchDialog.returnValue()
  '    End Select
  '  Catch ex As Exception
  '    Sunloft.Message.DisplayBox.ShowCritical(ex)
  '  End Try
  '  Return strReturnID
  'End Function

  ' ''' <summary>
  ' ''' Search PCA Order Slip / 受注伝票検索
  ' ''' </summary>
  ' ''' <param name="connector">DBConnection</param>
  ' ''' <returns>Slip ID</returns>
  ' ''' <remarks>2015/12/12</remarks>
  'Public Function ReferDialogPCAOrder(ByVal connector As IIntegratedApplication) As String
  '  Dim newDenSearchDialog As SLDenSearchDialog = New SLDenSearchDialog(ENUM_DEN_TYPE.PCAOrderSlip, connector)
  '  Dim strReturnID As String = String.Empty
  '  Try
  '    Select Case newDenSearchDialog.ShowDialog()
  '      Case Windows.Forms.DialogResult.OK
  '        strReturnID = newDenSearchDialog.returnValue()
  '    End Select
  '  Catch ex As Exception
  '    Sunloft.Message.DisplayBox.ShowCritical(ex)
  '  End Try
  '  Return strReturnID
  'End Function
  ' ''' <summary>
  ' ''' Search PCA Sales Slip / 受注伝票検索
  ' ''' </summary>
  ' ''' <param name="connector">DBConnection</param>
  ' ''' <returns>Slip ID</returns>
  ' ''' <remarks>2015/12/12</remarks>
  'Public Function ReferDialogPCASales(ByVal connector As IIntegratedApplication) As String
  '  Dim newDenSearchDialog As SLDenSearchDialog = New SLDenSearchDialog(ENUM_DEN_TYPE.PCASalesSlip, connector)
  '  Dim strReturnID As String = String.Empty
  '  Try
  '    Select Case newDenSearchDialog.ShowDialog()
  '      Case Windows.Forms.DialogResult.OK
  '        strReturnID = newDenSearchDialog.returnValue()
  '    End Select
  '  Catch ex As Exception
  '    Sunloft.Message.DisplayBox.ShowCritical(ex)
  '  End Try
  '  Return strReturnID
  'End Function
  ' ''' <summary>
  ' ''' Search Inventory Transfer Slip / 振替伝票検索
  ' ''' </summary>
  ' ''' <param name="connector">DBConnection</param>
  ' ''' <returns>Slip ID</returns>
  ' ''' <remarks>2015/12/12</remarks>
  'Public Function ReferDialogTransfer(ByVal connector As IIntegratedApplication) As String
  '  Dim newDenSearchDialog As SLDenSearchDialog = New SLDenSearchDialog(ENUM_DEN_TYPE.TransferSlip, connector)
  '  Dim strReturnID As String = String.Empty
  '  Try
  '    Select Case newDenSearchDialog.ShowDialog()
  '      Case Windows.Forms.DialogResult.OK
  '        strReturnID = newDenSearchDialog.returnValue()
  '    End Select
  '  Catch ex As Exception
  '    Sunloft.Message.DisplayBox.ShowCritical(ex)
  '  End Try
  '  Return strReturnID
  'End Function
  ' ''' <summary>
  ' ''' Search Inventory Adjustment Slip / 在庫伝票検索
  ' ''' </summary>
  ' ''' <param name="connector">DBConnection</param>
  ' ''' <returns>Slip ID</returns>
  ' ''' <remarks>2015/12/12</remarks>
  'Public Function ReferDialogInventory(ByVal connector As IIntegratedApplication) As String
  '  Dim newDenSearchDialog As SLDenSearchDialog = New SLDenSearchDialog(ENUM_DEN_TYPE.InventorySlip, connector)
  '  Dim strReturnID As String = String.Empty
  '  Try
  '    Select Case newDenSearchDialog.ShowDialog()
  '      Case Windows.Forms.DialogResult.OK
  '        strReturnID = newDenSearchDialog.returnValue()
  '    End Select
  '  Catch ex As Exception
  '    Sunloft.Message.DisplayBox.ShowCritical(ex)
  '  End Try
  '  Return strReturnID
  'End Function
  ' ''' <summary>
  ' ''' Search Shipping Slip / 出荷伝票検索
  ' ''' </summary>
  ' ''' <returns>Slip ID</returns>
  ' ''' <remarks>2015/12/12</remarks>
  'Private Function ReferDialogShipping() As String
  '  Dim newDenSearchDialog As SLDenSearchDialog = New SLDenSearchDialog(connector)
  '  Dim strReturnID As String = String.Empty
  '  Try
  '    Select Case newDenSearchDialog.ShowDialog()
  '      Case Windows.Forms.DialogResult.OK
  '        strReturnID = newDenSearchDialog.returnValue()
  '    End Select
  '  Catch ex As Exception
  '    Sunloft.Message.DisplayBox.ShowCritical(ex)
  '  End Try
  '  Return strReturnID
  'End Function
#End Region
End Class

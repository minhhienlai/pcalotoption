﻿Imports PCA.BaseFramework
Imports PCA.Controls
Imports PCA.TSC.Kon.BusinessEntity
Imports PCA.TSC.Kon.BusinessEntity.Defines
Imports Sunloft.PCAIF
Imports System.Windows.Forms
Imports System.Drawing
Imports PCA.ApplicationIntegration
Imports System.Globalization

Public Class testForm
  Public Property connector As IIntegratedApplication
  Private _conf As Sunloft.PcaConfig

  Private Sub PcaButton1_Click(sender As System.Object, e As System.EventArgs) Handles PcaButton1.Click
    Dim newZaikoDialog As SLZaikoDialog = New SLZaikoDialog(connector, "0001", DateTime.Parse("2015/12/10"), SLZaikoDialog.enmSlipType.typNormal, "0001")
    Select Case newZaikoDialog.ShowDialog()
      Case System.Windows.Forms.DialogResult.OK
        For Each value In newZaikoDialog.returnValue()
          PcaLabel1.Text = PcaLabel1.Text & value.ToString & " "
        Next
      Case System.Windows.Forms.DialogResult.Cancel
        PcaLabel1.Text = "Cancelled"
    End Select
  End Sub

  Private Sub PcaButton2_Click(sender As System.Object, e As System.EventArgs) Handles PcaButton2.Click
    Dim newDenSearchDialog As SLDenSearchDialog = New SLDenSearchDialog(connector)
    Select Case newDenSearchDialog.ShowDialog()
      Case System.Windows.Forms.DialogResult.OK
        For Each value In newDenSearchDialog.returnValue()
          PcaLabel1.Text = PcaLabel1.Text & value.ToString & " "
        Next
      Case System.Windows.Forms.DialogResult.Cancel
        PcaLabel1.Text = "Cancelled"
    End Select
  End Sub

Private Sub PcaButton3_Click(sender As System.Object, e As System.EventArgs) Handles PcaButton3.Click
    Dim newDenSearchDialog As SLMasterSearchDialog = New SLMasterSearchDialog(connector)
    Dim strCurrentCd As String = String.Empty
    Dim newCode As String = newDenSearchDialog.ShowReferTekiyoDialog(strCurrentCd, MousePosition)
    'Dim location As System.Drawing.Point = ControlTool.GetDialogLocation(myCodeSet.ReferButton)

    PcaLabel1.Text = PcaLabel1.Text & newCode.ToString & " "

    'Select Case newDenSearchDialog.ShowDialog()
    '  Case Windows.Forms.DialogResult.OK
    '    For Each value In newDenSearchDialog.returnValue()
    '      PcaLabel1.Text = PcaLabel1.Text & value.ToString & " "
    '    Next
    '  Case Windows.Forms.DialogResult.Cancel
    '    PcaLabel1.Text = "Cancelled"
    'End Select
End Sub

  Public Sub New()

    ' この呼び出しはデザイナーで必要です。
    InitializeComponent()
    connector = New ConnectToPCA().ConnectToPCA

    ' InitializeComponent() 呼び出しの後で初期化を追加します。

  End Sub

  Private Sub PcaButton4_Click(sender As System.Object, e As System.EventArgs) Handles PcaButton4.Click
    Dim newDenSearchDialog As SLMasterSearchDialog = New SLMasterSearchDialog(connector)
    Dim strCurrentCd As String = String.Empty
    Dim newCode As String = newDenSearchDialog.ShowReferProjectDialog(strCurrentCd, MousePosition)
    'Dim location As System.Drawing.Point = ControlTool.GetDialogLocation(myCodeSet.ReferButton)

    PcaLabel1.Text = PcaLabel1.Text & newCode.ToString & " "
  End Sub
End Class
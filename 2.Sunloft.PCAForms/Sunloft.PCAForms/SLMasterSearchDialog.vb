﻿
Imports System.ComponentModel

Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity
Imports PCA.TSC.Kon.Tools
Imports PCA.TSC.Kon
Imports System.Xml
Imports PCA
Imports System.Windows.Forms
Imports System.Drawing

Public Class SLMasterSearchDialog
  Private m_strMsgTitle As String = "ロット管理オプション"
  ''' <summary>文字列の照合順序。日本語辞書順の時はTrue、バイナリ順の時はFalseです。
  ''' </summary>
  Protected m_JapaneseCollation As Boolean = True

  ''' <summary>フォーム
  ''' </summary>
  Protected m_Owner As IWin32Window
  Private Property connector As IIntegratedApplication
  ''' <summary>伝票入力用コンフィグエレメント
  ''' </summary>
  Private m_ExtendConfigXPaths As String() = {"ZeiTableLine", "PromptRegister", "PromptDeleteRow"}

  ''' <summary>取得状況
  ''' </summary>
  Public Class LoadStatus
    ''' <summary>常駐要否
    ''' </summary>
    Public Property NeedLoad As Boolean = False
    ''' <summary>ロード済
    ''' </summary>
    Public Property Loaded As Boolean = False
  End Class

  ''' <summary>会社基本情報
  ''' </summary>
  Public ReadOnly Property BEKihonJoho() As PCA.TSC.Kon.BusinessEntity.BEKihonJoho
    Get
      Return _BEKihonJoho
    End Get
  End Property
  Protected _BEKihonJoho As New BEKihonJoho


#Region "プロパティ"

  ''' <summary>デフォルトの伝票日付を取得または設定します。
  ''' </summary>
  Public Property DefaultHizuke As Integer = 0

  ''' <summary>
  ''' 伝票参照画面（一覧表示）の表示項目リスト
  ''' </summary>
  Public Property SearchResultList As List(Of ReferItems)

  ''' <summary>
  ''' 検索状態かどうかを取得または設定します。
  ''' </summary>
  Public Property IsSearchModeOn As Boolean

  ''' <summary>
  ''' 一覧検索状態かどうかを取得または設定します。
  ''' </summary>
  Public Property IsIchiranModeOn As Boolean
  'Public Property IsListing As Boolean

  ''' <summary>
  ''' 登録コマンドが有効かどうかを取得または設定します。
  ''' </summary>
  Public Property SaveState As Boolean

  ''' <summary>
  ''' データを入力中かどうかを取得または設定します。
  ''' </summary>
  Public Property IsEditing As Boolean

  ''' <summary>
  ''' 現在メインフォームの初期化中かどうかを取得または設定します。
  ''' </summary>
  Public Property IsInitializing As Boolean

  ''' <summary>
  ''' 選択状態のエンティティが編集可能なエンティティかどうかを取得または設定します。
  ''' </summary>
  Public Property ModifiableEntity As Boolean

  ''' <summary>
  ''' 検索方向（false:前伝票、true:次伝票）
  ''' </summary>
  Public Property SearchDirection As Boolean

  ''' <summary>
  ''' 税率リストを取得します。
  ''' </summary>
  Public Property ZeiRitsuList As List(Of Decimal)

  ''' <summary>
  ''' 税率テーブルを取得します。
  ''' </summary>
  Public Property TaxList As List(Of BETAX)

  ''' <summary>
  ''' 選択された業務エンティティに対して変更された内容を取得します。
  ''' </summary>
  Public Property ModifiedEntity As PEKonInput

  ''' <summary>
  ''' 選択された業務エンティティを取得または設定します。
  ''' </summary>
  Public Property SelectedEntity As PEKonInput = Nothing
  ''' <summary>
  ''' メッセージの重複を防ぐためのフラグを取得または設定します。
  ''' </summary>
  Public Property AlreadyShowMessage As Boolean

  ''' <summary>税率別合計テーブルの表示行数を取得。
  ''' </summary>
  Public ReadOnly Property ZeiTableLine() As Integer
    Get
      Return _ZeiTableLine
    End Get
  End Property
  Private _ZeiTableLine As Integer = 1

  ''' <summary>登録確認の要否を取得。
  ''' </summary>
  Public ReadOnly Property PromptRegister() As Boolean
    Get
      Return _PromptRegister
    End Get
  End Property
  Private _PromptRegister As Boolean = False

  ''' <summary>行削除確認の要否を取得。
  ''' </summary>
  Public ReadOnly Property PromptDeleteRow() As Boolean
    Get
      Return _PromptDeleteRow
    End Get
  End Property
  Private _PromptDeleteRow As Boolean = False

  ''' <summary>伝票種別を取得。
  ''' </summary>
  Public ReadOnly Property DenpyoType() As Tools.PEKonInput.DenpyoType
    Get
      Return _DenpyoType
    End Get
  End Property
  Private _DenpyoType As Tools.PEKonInput.DenpyoType = PEKonInput.DenpyoType.Uriage

  ' ''' <summary>システム区分を取得。
  ' ''' </summary>
  'Public ReadOnly Property SystemKubun() As BusinessEntity.Defines.SystemKubunType
  '  Get
  '    Return _SystemKubun
  '  End Get
  'End Property
  Private _SystemKubun As BusinessEntity.Defines.SystemKubunType = BusinessEntity.Defines.SystemKubunType.Kan

  'Public Property MainForm As PCA.BaseFramework.ClientBase

#End Region

  ''' <summary>コンストラクタ
  ''' </summary>
  Public Sub New(ByVal myApp As IIntegratedApplication)
    '伝票種別を設定
    _DenpyoType = DenpyoType
    Me.connector = myApp

    If connector.Edition = "SaaS" Then
      '文字列比較はバイナリ順
      m_JapaneseCollation = False
    Else
      '文字列比較は日本語辞書順
      m_JapaneseCollation = True
    End If

    ''システム区分を設定
    'If _DenpyoType = PEKonInput.DenpyoType.Mitsumori _
    'OrElse _DenpyoType = PEKonInput.DenpyoType.Juchu _
    'OrElse _DenpyoType = PEKonInput.DenpyoType.Uriage Then
    '  '見積、受注、売上時
    '  _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon
    'End If

    '会社基本情報
    _BEKihonJoho = Tools.KonAPI.LoadBEKihonJoho(CType(connector, PCA.ApplicationIntegration.IntegratedApplication))
    _ReferDialogCommonVariable.BEKihonJoho = _BEKihonJoho

  End Sub

  ''' <summary>コントローラークラスの初期化
  ''' </summary>
  ''' <returns>初期化に成功したときTrue</returns>
  ''' <remarks></remarks>
  Public Overridable Function Initialize(ByRef configXMLDoc As XmlDocument) As Boolean

    ''ログイン
    'If (Not PrepareApplication(m_ExtendConfigXPaths, configXMLDoc)) Then
    '  Return False
    'End If

    ''入力可能期間をチェック
    'If _BEKihonJoho.NyuryokuKikanFrom = 0 OrElse _BEKihonJoho.NyuryokuKikanTo = 0 Then
    '  ShowMessage.OnInformation(m_Owner, MessageDefines.Information.NoNyuryokuKikan, m_strMsgTitle)
    '  Return False
    'End If

    '' 税率リストを作成
    'Me.ZeiRitsuList = New List(Of Decimal)
    'Me.ZeiRitsuList = Tools.MakeText.MakeZeiRitsuList(BEKihonJoho)

    'TaxList = BEKihonJoho.TaxRate.BETAX

    Return True

  End Function

#Region "ログイン・ログオフ"

  ' ''' <summary>コンフィグファイルをロードしPCAクラウドAPIを使用可能にする。
  ' ''' （補足）成功時はTrue、エラー発生時はFalse。
  ' ''' </summary>
  'Public Function PrepareApplication(ByVal extendConfigXPaths As String(), ByRef configXMLDoc As XmlDocument) As Boolean

  '  Dim extendConfigStrings As String()

  '  If PrepareApplicationBasic(configXMLDoc) = False Then
  '    Return False
  '  End If

  '  extendConfigStrings = Tools.Config.ConfigXMLDocToStrings(configXMLDoc, extendConfigXPaths)

  '  '税率別合計欄の行数
  '  If IsNumeric(extendConfigStrings(0).Trim()) Then
  '    _ZeiTableLine = CInt(extendConfigStrings(0).Trim)
  '  End If
  '  '登録時確認の有無
  '  If extendConfigStrings(1).Length <> 0 _
  '  AndAlso extendConfigStrings(1).ToUpper = "TRUE" Then
  '    _PromptRegister = True
  '  End If
  '  '行削除確認の有無
  '  If extendConfigStrings(2).Length <> 0 _
  '  AndAlso extendConfigStrings(2).ToUpper = "TRUE" Then
  '    _PromptDeleteRow = True
  '  End If

  '  Return True

  'End Function

#End Region

#Region "ツール"


#End Region

#Region "入力補助"

  ''' <summary>
  ''' 入力が開始されたときに呼ばれるメソッドです。
  ''' </summary>
  ''' <returns>正常に入力開始できたかどうか</returns>
  ''' <remarks></remarks>
  Public Function BeginChanged() As Boolean
    '
    'フォームの初期化中は編集状態へ移行しない
    If IsInitializing Then
      Return False
    End If

    SaveState = True
    IsEditing = True

    Return True
  End Function

  ''' <summary>
  ''' 入力が終了されたときに呼ばれるメソッドです。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub EndChanged()
    SaveState = False
    IsEditing = False
  End Sub

#End Region

#Region "入力制御"

  ''' <summary>
  ''' 在アクティブ状態（参照中、ロック中）になっているエンティティを更新します。
  ''' </summary>
  ''' <param name="activeEntity">アクティブ状態のエンティティ</param>
  ''' <remarks></remarks>
  Public Sub UpdateActiveEntity(ByVal activeEntity As PEKonInput)
    '
    Me.SelectedEntity = activeEntity
    Me.ModifiedEntity = Me.MakeCloneEntity(activeEntity)

  End Sub

  ''' <summary>エンティティのクローンを作成。
  ''' </summary>
  ''' <param name="entity">作成対象のエンティティ</param>
  ''' <returns>クローン作成されたエンティティ</returns>
  ''' <remarks></remarks>
  Public Function MakeCloneEntity(ByVal entity As PEKonInput) As PEKonInput
    '
    Return DirectCast(entity.Clone(), PEKonInput)

  End Function

#End Region

#Region "チェック関数"

  ''' <summary>小数桁を含む各単価の値範囲をチェック。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティクラス</param>
  ''' <param name="denpyoType">伝票入力種別</param>
  Public Function CheckTankaKeta(peKonInput As PEKonInput, denpyoType As Tools.PEKonInput.DenpyoType) As Boolean
    '
    If peKonInput Is Nothing Or peKonInput.DetailList Is Nothing OrElse peKonInput.DetailList.Count = 0 Then
      Return True
    End If

    Dim errorMessage As String = String.Empty
    Dim kon As Boolean = False

    Select Case denpyoType
      Case PCA.TSC.Kon.Tools.PEKonInput.DenpyoType.Mitsumori, _
       PCA.TSC.Kon.Tools.PEKonInput.DenpyoType.Juchu, _
       PCA.TSC.Kon.Tools.PEKonInput.DenpyoType.Uriage
        '見積、受注、売上
        kon = True
    End Select

    For i As Integer = 0 To peKonInput.DetailList.Count - 1

      Dim detail As PEKonInputDetail = peKonInput.DetailList(i)

      If String.IsNullOrEmpty(detail.SyohinCode) OrElse detail.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Kiji Then
        Continue For
      End If

      Dim curMax As Decimal = 0
      Dim curMin As Decimal = 0
      Dim tankaKeta As Short = 0

      If detail.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan Then
        '一般商品は小数桁の設定値、以外は小数桁はゼロ固定
        tankaKeta = detail.TankaKeta
      End If

      Select Case tankaKeta
        Case 0
          curMax = InputDefines.MaximumTanka0
          curMin = InputDefines.MinimumTanka0
          Exit Select
        Case 1
          curMax = InputDefines.MaximumTanka1
          curMin = InputDefines.MinimumTanka1
          Exit Select
        Case 2
          curMax = InputDefines.MaximumTanka2
          curMin = InputDefines.MinimumTanka2
          Exit Select
        Case 3
          curMax = InputDefines.MaximumTanka3
          curMin = InputDefines.MinimumTanka3
          Exit Select
        Case 4
          curMax = InputDefines.MaximumTanka4
          curMin = InputDefines.MinimumTanka4
          Exit Select
      End Select

      If detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan Then
        '一般商品
        Select Case denpyoType
          Case Tools.PEKonInput.DenpyoType.Uriage, _
               Tools.PEKonInput.DenpyoType.Shire
            If (detail.Ku = BusinessEntity.Defines.KuType.Tankateisei AndAlso curMin > detail.Tanka) _
            OrElse (detail.Ku <> BusinessEntity.Defines.KuType.Tankateisei AndAlso 0 > detail.Tanka) _
            OrElse curMax < detail.Tanka Then
              '売上、仕入の単価訂正時のみ単価に負値を許す
              errorMessage = "単価が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If
          Case Else
            If (0 > detail.Tanka) OrElse (curMax < detail.Tanka) Then
              errorMessage = "単価が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If
        End Select

        If KonCalc.CountSyosuKeta(detail.Tanka) > CInt(tankaKeta) Then
          errorMessage = "単価が設定可能小数桁を超えている明細が存在するため登録できません。"
          Exit For
        End If

        If kon Then
          '販売管理のみ
          If (0 > detail.GenTanka) OrElse (curMax < detail.GenTanka) Then
            errorMessage = "原単価が設定可能範囲を超えている明細が存在するため登録できません。"
            Exit For
          End If

          If (curMin > detail.BaiTanka) OrElse (curMax < detail.BaiTanka) Then
            errorMessage = "売単価が設定可能範囲を超えている明細が存在するため登録できません。"
            Exit For
          End If

          If KonCalc.CountSyosuKeta(detail.GenTanka) > CInt(tankaKeta) Then
            errorMessage = "原単価が設定可能小数桁を超えている明細が存在するため登録できません。"
            Exit For
          End If

          If KonCalc.CountSyosuKeta(detail.BaiTanka) > CInt(tankaKeta) Then
            errorMessage = "売単価が設定可能小数桁を超えている明細が存在するため登録できません。"
            Exit For
          End If
        End If
      End If

      Select Case detail.MasterKubun
        Case BusinessEntity.Defines.MasterKubunType.Ippan, _
             BusinessEntity.Defines.MasterKubunType.Zatsuhin, _
             BusinessEntity.Defines.MasterKubunType.Zappi
          '一般商品、雑商品、諸雑費
          If kon Then
            '販売管理
            If 0 > detail.HyojunKakaku2 OrElse curMax < detail.HyojunKakaku2 Then
              errorMessage = "標準価格が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If curMin > detail.HyojunKakaku OrElse curMax < detail.HyojunKakaku Then
              errorMessage = "標準価格が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If KonCalc.CountSyosuKeta(detail.HyojunKakaku2) > CInt(tankaKeta) Then
              errorMessage = "標準価格が設定可能小数桁を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If KonCalc.CountSyosuKeta(detail.HyojunKakaku) > CInt(tankaKeta) Then
              errorMessage = "標準価格が設定可能小数桁を超えている明細が存在するため登録できません。"
              Exit For
            End If
          Else
            '仕入管理
            If (curMin > detail.HyojunShireTanka) OrElse (curMax < detail.HyojunShireTanka) Then
              errorMessage = "標準仕入単価が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If KonCalc.CountSyosuKeta(detail.HyojunShireTanka) > CInt(tankaKeta) Then
              errorMessage = "標準仕入単価が設定可能小数桁を超えている明細が存在するため登録できません。"
              Exit For
            End If

          End If
      End Select

    Next

    If Not String.IsNullOrEmpty(errorMessage) Then
      '何れかのエラーが存在する時
      ShowMessage.OnExclamation(m_Owner, errorMessage, m_strMsgTitle)
      Return False
    End If

    Return True

  End Function

  ' ''' <summary>商品の有効期間内をチェック。
  ' ''' </summary>
  ' ''' <param name="beMasterSms">商品マスター</param>
  ' ''' <param name="denpyoHizuke">伝票日付</param>
  ' ''' <returns>true：有効期間内、false：有効期間外</returns>
  ' ''' <remarks></remarks>
  'Public Function CheckSyohinYukoKikan(ByVal beMasterSms As BEMasterSms, ByVal denpyoHizuke As Integer) As Boolean
  '    '
  '    Return KonCalc.CheckSyohinYukoKikan(beMasterSms, denpyoHizuke)

  'End Function

  ' ''' <summary>商品の有効期間内をチェック。
  ' ''' </summary>
  ' ''' <param name="beMasterSms">商品マスター</param>
  ' ''' <param name="denpyoHizuke1">伝票日付１</param>
  ' ''' <param name="denpyoHizuke2">伝票日付２</param>
  'Public Function CheckSyohinYukoKikan(ByVal beMasterSms As BEMasterSms, ByVal denpyoHizuke1 As Integer, ByVal denpyoHizuke2 As Integer) As Boolean
  '    '
  '    Return KonCalc.CheckSyohinYukoKikan(beMasterSms, denpyoHizuke1, denpyoHizuke2)

  'End Function

  ' ''' <summary>商品の有効期間内をチェックし、続行確認画面を表示。
  ' ''' （補足）有効期間外有り、処理中断時はFalseを返す。
  ' ''' </summary>
  ' ''' <param name="detailList">伝票入力明細エンティティ</param>
  ' ''' <param name="denpyoHizuke">伝票日付</param>
  'Public Function CheckSyohinYukoKikanAndPrompt(ByVal detailList As List(Of PEKonInputDetail), ByVal denpyoHizuke As Integer) As Boolean

  '  If IsNothing(detailList) OrElse detailList.Count = 0 OrElse denpyoHizuke = 0 Then
  '    Return True
  '  End If

  '  Dim syohinCodeList As New List(Of String)()
  '  syohinCodeList = PrepareSyohinCodeList(detailList)

  '  Return CheckSyohinYukoKikanAndPrompt(syohinCodeList, denpyoHizuke)

  'End Function

  ' ''' <summary>商品の有効期間内をチェックし、続行確認画面を表示。
  ' ''' （補足）有効期間外有り、処理中断時はFalseを返す。
  ' ''' </summary>
  ' ''' <param name="syohinCodeList">商品コードリスト</param>
  ' ''' <param name="denpyoHizuke">伝票日付</param>
  ' ''' <returns>true：有効期間内または続行、false：有効期間外</returns>
  ' ''' <remarks></remarks>
  'Private Function CheckSyohinYukokikanAndPrompt(ByVal syohinCodeList As List(Of String), ByVal denpyoHizuke As Integer) As Boolean

  '  Dim result As Boolean = True

  '  If Not (IsNothing(syohinCodeList)) And syohinCodeList.Count > 0 Then

  '    '商品コードリストから該当商品の商品マスターリストを作成
  '    Dim beMasterSmsList As New List(Of BEMasterSms)
  '    beMasterSmsList = FindBySyohinCodeList(syohinCodeList)

  '    Dim resultSyohinCodeList As New List(Of String)()

  '    For Each beMasterSms As BEMasterSms In beMasterSmsList
  '      'If (beMasterSms.YukoKikanKaishi <> 0 And denpyoHizuke < beMasterSms.YukoKikanKaishi) _
  '      'OrElse (beMasterSms.YukoKikanSyuryo <> 0 And denpyoHizuke > beMasterSms.YukoKikanSyuryo) Then
  '      If KonCalc.CheckSyohinYukoKikan(beMasterSms, denpyoHizuke) = False Then
  '        '有効期間内かをチェック
  '        resultSyohinCodeList.Add("(" & beMasterSms.SyohinCode.PadRight(_BEKihonJoho.SyohinKeta) & ")" & beMasterSms.SyohinMei)
  '      End If
  '    Next

  '    If resultSyohinCodeList.Count > 0 Then
  '      '期間外の商品が存在する時は処理続行の確認を実行
  '      Dim listBoxDialog As Tools.TscListBoxDialog = _
  '          New Tools.TscListBoxDialog(m_strMsgTitle, _
  '                                     Controls.PcaGuideLabel.Icons.Question,
  '                                     "以下の商品は有効期間外となります。" & vbCrLf & "処理を続行しますか？", _
  '                                     53, _
  '                                      2, _
  '                                     resultSyohinCodeList)
  '      '
  '      listBoxDialog.CancelVisible = CStr(True)
  '      listBoxDialog.OkText = "続行"
  '      listBoxDialog.CancelText = "キャンセル"

  '      If listBoxDialog.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
  '        'キャンセルボタン押下時
  '        Return False
  '      End If

  '    End If
  '  End If

  '  Return result

  'End Function

  ' ''' <summary>重複排除し伝票入力明細エンティティに設定されている商品コードのリストを作成。
  ' ''' </summary>
  ' ''' <param name="detailList">売上明細のリスト</param>
  'Public Function PrepareSyohinCodeList(ByVal detailList As List(Of PEKonInputDetail)) As List(Of String)
  '  '
  '  Dim syohinCodeList As List(Of String) = New List(Of String)

  '  For Each detail In detailList
  '    Dim syohinCode As String = detail.SyohinCode
  '    If Not (String.IsNullOrEmpty(detail.SyohinCode)) _
  '    And Not (syohinCodeList.Exists(Function(match) match = syohinCode)) Then
  '      syohinCodeList.Add(detail.SyohinCode)
  '    End If
  '  Next

  '  Return syohinCodeList

  'End Function

  ' ''' <summary>商品コードリストから該当商品の商品マスターリストを作成。
  ' ''' </summary>
  ' ''' <param name="syohinCodeList">商品コードのリスト</param>
  'Public Function FindBySyohinCodeList(ByVal syohinCodeList As List(Of String)) As List(Of BEMasterSms)
  '  '
  '  Dim beMasterSmsList As New List(Of BEMasterSms)

  '  For Each syohinCode In syohinCodeList
  '    Dim beMasterSms As BEMasterSms = FindBEMasterSms(syohinCode)
  '    If beMasterSms.SyohinCode.Length <> 0 Then
  '      beMasterSmsList.Add(beMasterSms)
  '    End If
  '  Next

  '  ' 商品コード順にソート
  '  'beMasterSmsList.Sort(Function(a, b) b.SyohinCode - a.SyohinCode)

  '  Return beMasterSmsList

  'End Function



#End Region

#Region "◆編集メニュー"


  ''' <summary>行削除前に確認。
  ''' （補足）キャンセル時はFalseを返す。
  ''' </summary>
  Public Function ConfirmRowDelete() As Boolean
    '
    Dim yes As Boolean = True

    If _PromptDeleteRow Then
      '行削除前に確認を要する時
      Dim dlgResult As DialogResult = ShowMessage.OnQuestion(m_Owner, MessageDefines.Question.DeleteRow, m_strMsgTitle)
      yes = (dlgResult = System.Windows.Forms.DialogResult.Yes)
    End If

    Return yes

  End Function

  ''' <summary>変更後に未登録の時に登録確認を実行。
  ''' （補足）キャンセル時は保留としてFalseを返す。
  ''' </summary>
  ''' <param name="saveYes">YESボタン押下</param>
  Public Function ConfirmSaveChanged(ByRef saveYes As Boolean) As Boolean
    '
    Dim confirm As Boolean = True

    Dim dlgResult As DialogResult = ShowMessage.OnConfirm(m_Owner, MessageDefines.Question.SaveChangingData, m_strMsgTitle)
    If dlgResult = System.Windows.Forms.DialogResult.Yes Then
      confirm = True
      saveYes = True
    ElseIf dlgResult = System.Windows.Forms.DialogResult.No Then
      confirm = True
      saveYes = False
    ElseIf dlgResult = System.Windows.Forms.DialogResult.Cancel Then
      confirm = False
      saveYes = False
    End If

    Return confirm

  End Function

  ''' <summary>全ての明細が空（商品コードが未入力）か判定。
  ''' </summary>
  ''' <param name="peKonInput"></param>
  Public Function IsAllRowEmpty(ByVal peKonInput As PEKonInput) As Boolean
    '
    Dim detailCount As Integer = 0

    '共通エンティティの商品コードの設定有無をチェック
    For Each detail As PEKonInputDetail In peKonInput.DetailList
      If Not String.IsNullOrEmpty(detail.SyohinCode) Then
        detailCount += 1
        Exit For
      End If
    Next

    If detailCount = 0 Then
      Return True
    Else
      Return False
    End If

  End Function

#End Region

#Region "共通計算"



#End Region

#Region "在庫数の照会"

  ''' <summary>
  ''' 在庫数の照会を取得します
  ''' </summary>
  ''' <param name="SyohinCode">商品コード</param>
  ''' <param name="SokoCode">倉庫コード</param>
  ''' <returns>取得した「在庫数の照会」データ</returns>
  ''' <remarks></remarks>
  Public Function CalculateZaikosuNoSyokai(ByVal syohinCode As String, ByVal sokoCode As String) As BEZaikosuNoSyokai
    '
    Dim zaikosuNoSyokaiCondition As New ZaikosuNoSyokaiCondition
    Dim result As New ResultCalculateOutputZaikosuNoSyokai
    Dim beZaikosuNoSyokai As New BEZaikosuNoSyokai

    '商品コードと倉庫コードを指定
    '在庫管理しない場合は倉庫コードは無視される
    Try
      '
      zaikosuNoSyokaiCondition.SelectedSyohinKubun = 0
      zaikosuNoSyokaiCondition.SyohinCode = syohinCode.Trim()
      zaikosuNoSyokaiCondition.SokoCode = sokoCode.Trim()

      result = Tools.KonAPI.CalculateOutputZaikosuNoSyokai(connector, zaikosuNoSyokaiCondition)

      If result.Status = IntegratedStatus.Success _
      AndAlso result.ArrayOfBEZaikosuNoSyokai.BEZaikosuNoSyokai.Count > 0 Then
        beZaikosuNoSyokai = result.ArrayOfBEZaikosuNoSyokai.BEZaikosuNoSyokai(0)
      End If

      If beZaikosuNoSyokai.SyohinCode.Length = 0 Then
        '該当データが取得不可の時は、現在個数ゼロのまま商品コード、倉庫コード・名を補填
        Dim beMasterSoko As BEMasterSoko = FindBEMasterSoko(sokoCode.Trim())
        beZaikosuNoSyokai.SyohinCode = syohinCode
        beZaikosuNoSyokai.SokoCode = sokoCode
        beZaikosuNoSyokai.SokoMei = beMasterSoko.SokoMei
      End If

      Return beZaikosuNoSyokai

    Catch ex As Exception
      Throw

    End Try

  End Function

#End Region

#Region "マスター参照画面、検証"

  ''' <summary>参照画面の共通変数
  ''' </summary>
  Public Property ReferDialogCommonVariable As New ReferDialogCommonVariable

  ''' <summary>参照画面為の表示項目リストプロパティを一括で格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferItemsList() As Boolean
    '
    _ReferDialogCommonVariable.BEKihonJoho = _BEKihonJoho
    'システム区分
    _ReferDialogCommonVariable.SystemKubunUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
    _ReferDialogCommonVariable.SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kan
    '_ReferDialogCommonVariable.SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon
    '使用区分
    _ReferDialogCommonVariable.ShiyoKubunUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem

    If ReferSms.NeedLoad = True Then    '商品
      If LoadReferSms() = False Then
        Return False
      End If
    End If
    If ReferSoko.NeedLoad = True Then    '倉庫
      If LoadReferSoko() = False Then
        Return False
      End If
    End If
    If ReferBumon.NeedLoad = True Then    '部門
      If LoadReferBumon() = False Then
        Return False
      End If
    End If
    If ReferTantosya.NeedLoad = True Then    '担当者
      If LoadReferTantosya() = False Then
        Return False
      End If
    End If
    If ReferProject.NeedLoad = True Then    'プロジェクト
      If LoadReferProject() = False Then
        Return False
      End If
    End If
    If ReferTekiyo.NeedLoad = True Then    '売上仕入等の摘要
      If LoadReferTekiyo() = False Then
        Return False
      End If
    End If
    'If ReferTms.NeedLoad = True Then    '得意先
    '  If LoadReferTms() = False Then
    '    Return False
    '  End If
    'End If
    If ReferRms.NeedLoad = True Then    '得意先
      If LoadReferRms() = False Then
        Return False
      End If
    End If
    If ReferYms.NeedLoad = True Then    '直送先
      If LoadReferYms() = False Then
        Return False
      End If
    End If

    Return True

  End Function

  ''' <summary>参照画面の表示開始位置を設定
  ''' </summary>
  ''' <param name="location">表示開始位置</param>
  ''' <param name="referDialog">参照画面</param>
  Public Sub LocateReferDialog(ByVal location As Point, ByRef referDialog As TscReferDialog)
    '
    If location.IsEmpty = False Then
      '表示開始位置は任意設定
      referDialog.StartPosition = FormStartPosition.Manual
      '表示開始位置
      referDialog.Location = location
      '画面起動時に開始位置調整を行うかどうか
      referDialog.AutoStartPosition = False
      '画面起動時に画面内に収まるように表示位置調整を行うかどうか
      referDialog.AutoLocationAdjust = True
    End If

  End Sub

  ' ''' <summary>参照画面の表示開始位置を設定(ロット詳細設定)
  ' ''' </summary>
  ' ''' <param name="location">表示開始位置</param>
  ' ''' <param name="referDialog">参照画面</param>
  Public Sub LocateReferLmdDialog(ByVal location As Point, ByRef referDialog As Sunloft.SearchEx.LmdReferDialog)
    '
    If location.IsEmpty = False Then
      '表示開始位置は任意設定
      referDialog.StartPosition = FormStartPosition.Manual
      '表示開始位置
      referDialog.Location = location
      '画面起動時に開始位置調整を行うかどうか
      referDialog.AutoStartPosition = False
      '画面起動時に画面内に収まるように表示位置調整を行うかどうか
      referDialog.AutoLocationAdjust = True
    End If

  End Sub

#Region "   商品"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferSms As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferSmsTool As ReferSmsDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferSmsList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferSms() As Boolean
    '
    Dim masterSmsTool As New MasterSmsTool
    m_ReferSmsTool = New ReferSmsDialogTool(_ReferDialogCommonVariable)

    If MasterSms.Loaded = True Then
      If _ReferSms.Loaded = True Then
        'ロード済の時はクリア
        ReferSmsList.Clear()
        _ReferSms.Loaded = False
      End If
      '取得済のマスターリストから
      ReferSmsList = m_ReferSmsTool.SettingReferItemsList(BEMasterSmsList)
    Else
      ReferSmsList = m_ReferSmsTool.LoadReferItemsList(connector, masterSmsTool)
    End If

    _ReferSms.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferSmsDialog(ByVal code As String, ByVal location As Point) As String
    Dim masterSmsTool As New MasterSmsTool
    Dim dialogResult As DialogResult
    Dim newCode As String = String.Empty
    Try
      If _ReferSms.Loaded = False Then
        m_ReferSmsTool = New ReferSmsDialogTool(_ReferDialogCommonVariable)
        ReferSmsList = m_ReferSmsTool.LoadReferItemsList(connector, masterSmsTool)
      End If

      Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferSmsTool.Setting, ReferSmsList, connector)
      LocateReferDialog(location, referDialog) '表示位置等を設定

      newCode = String.Empty

      referDialog.IApplication = connector
      referDialog.SelectedItem = code

      '参照画面を表示
      dialogResult = referDialog.ShowDialog()
      newCode = referDialog.SelectedItem
      If (dialogResult = System.Windows.Forms.DialogResult.Cancel) Then newCode = String.Empty
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return newCode
  End Function

   ''' <summary>コードを検証し伝票入力エンティティに設定
   ''' </summary>
   ''' <param name="rowIndex">行インデックス</param>
   ''' <param name="newcode">商品コード</param>
   ''' <param name="peKonInput">伝票入力エンティティ</param>
   ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
   ''' <param name="headerLabelText">ヘッダーラベル表示メッセージ</param>
  Public Function ValidatingSyohin(ByVal rowIndex As Integer, _
                                   ByVal newCode As String, _
                                   ByRef peKonInput As PEKonInput, _
                                   ByVal needMessageBox As Boolean, _
                                   ByRef headerLabelText As String) As Boolean
    '
    Dim torihikisakiText As String = String.Empty
    Dim rendoText As String = String.Empty
    Dim denpyoText As String = String.Empty
    Dim senyoText As String = String.Empty
    If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
      torihikisakiText = BusinessEntity.ItemText.Tokuisaki
      rendoText = BusinessEntity.ItemText.Juchu
      denpyoText = BusinessEntity.ItemText.Uriage
      senyoText = BusinessEntity.ItemText.Shiire
    Else
      torihikisakiText = BusinessEntity.ItemText.Shiresaki
      rendoText = BusinessEntity.ItemText.Hachu
      denpyoText = BusinessEntity.ItemText.Shiire
      senyoText = BusinessEntity.ItemText.Uriage
    End If

    Dim rendoSiji As String = String.Empty
    Dim rendoHeaderId As Integer = 0
    Dim rendoSequence As Integer = 0

    If String.IsNullOrEmpty(newCode) = True Then
      If String.IsNullOrEmpty(peKonInput.DetailList(rowIndex).SyohinCode) = True Then
        '未入力→未入力時
        Return True
      Else
        '入力済→未入力時、商品コードは必須入力
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    Tools.MessageDefines.Exclamation.IncorrectValue, _
                                    m_strMsgTitle)
        End If
        Return False
      End If
    End If

    If String.IsNullOrEmpty(peKonInput.Header.TorihikisakiCode) = True Then
      '得意（仕入）先が未設定
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.NotInput, torihikisakiText), _
                                  m_strMsgTitle)
      End If
      Return False
    End If

    '商品マスターを取得
    Dim beMasterSms As BEMasterSms = FindBEMasterSms(newCode)
    If beMasterSms.SyohinCode.Length = 0 Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Syohin), _
                                  m_strMsgTitle)
      End If
      Return False
    End If

    '専用商品をチェック
    If (_SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon And beMasterSms.SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.Kan) _
    OrElse (_SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kan And beMasterSms.SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.Kon) Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.DifferentMasterKubun, senyoText), _
                                  m_strMsgTitle)
      End If
      Return False
    End If

    '使用禁止または有効期間外をチェック
    If beMasterSms.ShiyoKubun = PCA.TSC.Kon.BusinessEntity.Defines.ShiyoKubunType.Kinshi _
    OrElse KonCalc.CheckSyohinYukoKikan(beMasterSms, peKonInput.Header.DenpyoHizuke) = False Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.UnusableOrExpired, BusinessEntity.ItemText.Syohin), _
                                  m_strMsgTitle)
      End If
      Return False
    End If

    '受／発注からの連動明細の時、変更前後をチェック
    If peKonInput.DetailList(rowIndex).RendoHeaderId <> 0 Then
      If peKonInput.DetailList(rowIndex).MasterKubun <> beMasterSms.MasterKubun Then
        'マスター区分が不一致
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(Tools.MessageDefines.Exclamation.CantChangeMasterKubun, rendoText, denpyoText), _
                                    m_strMsgTitle)
        End If
        Return False
      End If
      If peKonInput.DetailList(rowIndex).SuryoKeta <> beMasterSms.SuryoKeta Then
        '数量小数桁が不一致
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(Tools.MessageDefines.Exclamation.CantChangeSuryoKeta, rendoText, denpyoText), _
                                    m_strMsgTitle)
        End If
        Return False
      End If
      '連動項目を退避
      rendoSiji = peKonInput.DetailList(rowIndex).RendoShiji
      rendoHeaderId = peKonInput.DetailList(rowIndex).RendoHeaderId
      rendoSequence = peKonInput.DetailList(rowIndex).RendoSequence
    End If

    '明細SEQを退避
    Dim sequence As Integer = peKonInput.DetailList(rowIndex).Sequence
    '入金／支払済フラグを退避
    Dim kinzumiFlag As PCA.TSC.Kon.BusinessEntity.Defines.KinzumiFlagType = peKonInput.DetailList(rowIndex).KinzumiFlag
    '伝票入力エンティティ明細を初期化
    peKonInput.DetailList(rowIndex) = New PEKonInputDetail()

    If rendoHeaderId <> 0 Then
      '連動項目を戻す
      peKonInput.DetailList(rowIndex).RendoShiji = rendoSiji
      peKonInput.DetailList(rowIndex).RendoHeaderId = rendoHeaderId
      peKonInput.DetailList(rowIndex).RendoSequence = rendoSequence
    End If

    '商品項目を伝票入力エンティティに設定
    peKonInput.DetailList(rowIndex).Sequence = sequence                                  '明細SEQ
    peKonInput.DetailList(rowIndex).SyohinCode = beMasterSms.SyohinCode                  '商品コード
    peKonInput.DetailList(rowIndex).MasterKubun = beMasterSms.MasterKubun                'マスター区分
    peKonInput.DetailList(rowIndex).ZeiKubun = beMasterSms.ZeiKubun                      '税区分
    peKonInput.DetailList(rowIndex).ZeikomiKubun = beMasterSms.ZeikomiKubun              '税込区分
    peKonInput.DetailList(rowIndex).TankaKeta = beMasterSms.TankaKeta                    '単価小数桁
    peKonInput.DetailList(rowIndex).SyohinMei = beMasterSms.SyohinMei                    '品名
    peKonInput.DetailList(rowIndex).KikakuKataban = beMasterSms.KikakuKataban            '規格・型番
    peKonInput.DetailList(rowIndex).Color = beMasterSms.Color                            '色
    peKonInput.DetailList(rowIndex).Size = beMasterSms.Size                              'サイズ
    peKonInput.DetailList(rowIndex).Irisu = beMasterSms.Irisu                            '入数     
    peKonInput.DetailList(rowIndex).Tani = beMasterSms.Tani                              '単位
    '税率
    peKonInput.DetailList(rowIndex).ZeiRitsu = KonCalc.SearchZeiRitsu(Me.TaxList, peKonInput.Header.DenpyoHizuke, beMasterSms.ZeiKubun)
    peKonInput.DetailList(rowIndex).IrisuKeta = beMasterSms.IrisuKeta                    ' 入数小数桁
    peKonInput.DetailList(rowIndex).HakosuKeta = beMasterSms.HakosuKeta                  ' 箱数小数桁
    peKonInput.DetailList(rowIndex).SuryoHasu = beMasterSms.SuryoHasu                    ' 数量端数
    peKonInput.DetailList(rowIndex).GenkaHasu = peKonInput.Header.GenkaHasu             ' 原価端数
    peKonInput.DetailList(rowIndex).SuryoKeta = beMasterSms.SuryoKeta                    ' 数量小数桁

    '倉庫
    Dim isChangeSoko As Boolean = True
    If beMasterSms.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan _
    AndAlso Me.BEKihonJoho.ZaikoKanri = PCA.TSC.Kon.BusinessEntity.Defines.KanriType.Suru Then
      '一般商品で、会社基本情報上倉庫別在庫管理を行う時
      If Not String.IsNullOrEmpty(beMasterSms.SokoCode) _
      AndAlso Not beMasterSms.SokoCode.Equals(Tools.MakeText.MakeCommonCode(Me.BEKihonJoho.SokoKeta)) Then
        peKonInput.DetailList(rowIndex).SokoCode = beMasterSms.SokoCode
        isChangeSoko = False
      Else
        If Not String.IsNullOrEmpty(peKonInput.Header.BumonSoko) Then
          '商品マスターの倉庫コード未設定の時 部門倉庫コードを設定
          peKonInput.DetailList(rowIndex).SokoCode = peKonInput.Header.BumonSoko
        Else
          '部門倉庫コードも未設定の時は共通倉庫コードを設定
          peKonInput.DetailList(rowIndex).SokoCode = Tools.MakeText.MakeCommonCode(Me.BEKihonJoho.SokoKeta)
        End If
      End If
    Else
      '倉庫別在庫管理外の時は、共通倉庫コードを設定
      peKonInput.DetailList(rowIndex).SokoCode = Tools.MakeText.MakeCommonCode(Me.BEKihonJoho.SokoKeta)
    End If

    '商品項目・計算式コード
    If beMasterSms.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan Then
      '一般商品のみ
      peKonInput.DetailList(rowIndex).SyohinKomoku1 = beMasterSms.SyohinKomoku1
      peKonInput.DetailList(rowIndex).SyohinKomoku2 = beMasterSms.SyohinKomoku2
      peKonInput.DetailList(rowIndex).SyohinKomoku3 = beMasterSms.SyohinKomoku3
      peKonInput.DetailList(rowIndex).SyokonKeisan = 0    'このサンプルでは計算式を扱わないため
    Else
      peKonInput.DetailList(rowIndex).SyohinKomoku1 = 0D
      peKonInput.DetailList(rowIndex).SyohinKomoku2 = 0D
      peKonInput.DetailList(rowIndex).SyohinKomoku3 = 0D
      peKonInput.DetailList(rowIndex).SyokonKeisan = 0
    End If

    peKonInput.DetailList(rowIndex).Hakosu = 0D              ' 箱数
    peKonInput.DetailList(rowIndex).Suryo = 0D               ' 数量

    '単価金額
    Select Case beMasterSms.MasterKubun
      Case PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan
        '一般商品
        If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
          '商魂
          peKonInput.DetailList(rowIndex).Tanka = Me.ChoiceTanka(peKonInput.Header.TekiyoBaikaNo, peKonInput.Header.Kakeritsu, beMasterSms)
          peKonInput.DetailList(rowIndex).GenTanka = beMasterSms.Genka
          peKonInput.DetailList(rowIndex).BaiTanka = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).Kingaku = 0D
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunShireTanka = 0D
        Else
          '商管
          peKonInput.DetailList(rowIndex).Tanka = beMasterSms.ShireTanka
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = 0D
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = 0D
          peKonInput.DetailList(rowIndex).HyojunShireTanka = beMasterSms.ShireTanka
        End If
        Exit Select

      Case PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Zatsuhin, PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Zappi
        '雑商品、諸雑費
        If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
          '商魂
          peKonInput.DetailList(rowIndex).Tanka = 0D
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = Me.ChoiceTanka(peKonInput.Header.TekiyoBaikaNo, peKonInput.Header.Kakeritsu, beMasterSms)
          peKonInput.DetailList(rowIndex).Genka = beMasterSms.Genka
          peKonInput.DetailList(rowIndex).BaikaKingaku = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunKakaku = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunShireTanka = 0D
        Else
          '商管
          peKonInput.DetailList(rowIndex).Tanka = 0D
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = beMasterSms.ShireTanka
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = 0D
          peKonInput.DetailList(rowIndex).HyojunShireTanka = beMasterSms.ShireTanka
        End If
        Exit Select
      Case Else
        '値引、記事
        If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
          '商魂
          peKonInput.DetailList(rowIndex).Tanka = 0D
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = 0D
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = 0D
          peKonInput.DetailList(rowIndex).HyojunShireTanka = 0D
        Else
          '商管
          peKonInput.DetailList(rowIndex).Tanka = 0D
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = 0D
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = 0D
          peKonInput.DetailList(rowIndex).HyojunShireTanka = 0D
        End If
        Exit Select
    End Select

    If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
      '入荷／発注マーク
      peKonInput.DetailList(rowIndex).KanMark = _
          KonCalc.JudgeKanMarkText(peKonInput.Header.Denku, _
                                   peKonInput.DetailList(rowIndex).MasterKubun, _
                                   beMasterSms.SystemKubun, _
                                   peKonInput.DetailList(rowIndex).Ku, _
                                   peKonInput.DetailList(rowIndex).KanMark)
    End If

    Dim checkZaikoware As Boolean = True
    If peKonInput.Header.Denku = PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Keiyaku Then
      '契約伝票は在庫割れのチェック対象外
      checkZaikoware = False
    End If

    headerLabelText = String.Empty
    ''消費税、粗利益、利益率を計算（在庫割れも数量がゼロの為、チェック対象外）
    'Me.CalcNotKingakuItems(peKonInput, rowIndex, False, checkZaikoware, False, needMessageBox, headerLabelText)

    ''合計を再計算
    'Me.CalcTotal(peKonInput, needMessageBox, Me._DenpyoType)

    Return True

  End Function

  ''' <summary>適用売価を選択。
  ''' </summary>
  ''' <param name="tekiyoBaikaNo">適用売価№</param>
  ''' <param name="kakeritsu">売価掛率</param>
  ''' <param name="beMasterSms">商品マスター</param>
  Public Function ChoiceTanka(ByVal tekiyoBaikaNo As Short, ByVal kakeritsu As Short, ByVal beMasterSms As BEMasterSms) As Decimal
    '
    '適用売価＝適用売価Noにて指定した単価×売価掛率÷100 (切り捨て)
    '掛率は小数桁を整数化した数値(100.0→1000)
    Dim baika As Decimal = 0D

    Select Case tekiyoBaikaNo
      Case 0
        '標準価格
        baika = beMasterSms.HyojunKakaku
      Case 1
        '売価1
        baika = beMasterSms.Baika1
      Case 2
        '売価2
        baika = beMasterSms.Baika2
      Case 3
        '売価3
        baika = beMasterSms.Baika3
      Case 4
        '売価4
        baika = beMasterSms.Baika4
      Case 5
        '売価5
        baika = beMasterSms.Baika5
      Case 6
        '原価
        baika = beMasterSms.Genka
    End Select

    Return KonCalc.RoundOff(baika * kakeritsu / 1000D, 0)

  End Function
#End Region

#Region "   倉庫"
  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferSoko As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferSokoTool As ReferSokoDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferSokoList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferSoko() As Boolean
    '
    Dim masterSokoTool As New MasterSokoTool
    m_ReferSokoTool = New ReferSokoDialogTool(_ReferDialogCommonVariable)

    If MasterSoko.Loaded = True Then
      If _ReferSoko.Loaded = True Then
        'ロード済の時はクリア
        ReferSokoList.Clear()
        _ReferSoko.Loaded = False
      End If
      '取得済のマスターリストから
      ReferSokoList = m_ReferSokoTool.SettingReferItemsList(BEMasterSokoList)
    Else
      ReferSokoList = m_ReferSokoTool.LoadReferItemsList(connector, masterSokoTool)
    End If

    _ReferSoko.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferSokoDialog(ByVal code As String, ByVal location As Point) As String
    '
    Dim masterSokoTool As New MasterSokoTool
    Dim dialogResult As DialogResult
    Dim newCode As String = String.Empty
    Try
      If _ReferSoko.Loaded = False Then
        m_ReferSokoTool = New ReferSokoDialogTool(_ReferDialogCommonVariable)
        ReferSokoList = m_ReferSokoTool.LoadReferItemsList(connector, masterSokoTool)
      End If

      Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferSokoTool.Setting, ReferSokoList, connector)
      LocateReferDialog(location, referDialog) '表示位置等を設定

      newCode = String.Empty

      referDialog.IApplication = connector
      referDialog.SelectedItem = code

      '参照画面を表示
      dialogResult = referDialog.ShowDialog()
      newCode = referDialog.SelectedItem
      If (dialogResult = System.Windows.Forms.DialogResult.Cancel) Then newCode = String.Empty

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try

    Return newCode

  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="rowIndex">行インデックス</param>
  ''' <param name="newCode">倉庫コード</param>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="headerLabelText">ヘッダーラベル表示メッセージ</param>
  Public Function VaiidatingSoko(ByVal rowIndex As Integer, _
                                 ByVal newCode As String, _
                                 ByRef peKonInput As PEKonInput, _
                                 ByVal needMessageBox As Boolean, _
                                 ByRef headerLabelText As String) As Boolean
    '
    If String.IsNullOrEmpty(newCode) = True Then
      '未入力は共通倉庫にする
      newCode = MakeText.MakeCommonCode(Me.BEKihonJoho.SokoKeta)
    End If

    '倉庫マスターを取得
    Dim beMasterSoko As BEMasterSoko = FindBEMasterSoko(newCode)

    If beMasterSoko.SokoCode.Length <> 0 Then
      '倉庫コードを設定
      peKonInput.DetailList(rowIndex).SokoCode = beMasterSoko.SokoCode

      '数量が入力済時は、在庫割れをチェック
      If peKonInput.DetailList(rowIndex).Suryo <> 0 Then
        'If peKonInput.Header.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Keiyaku Then
        '  '契約伝票は在庫割れチェックの対象外
        '  headerLabelText = Me.CheckZaikoware(peKonInput.DetailList(rowIndex))
        'End If
      End If
    Else
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Soko), _
                                  m_strMsgTitle)
      End If
      Return False
    End If

    Return True

  End Function

#End Region

#Region "   部門"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferBumon As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferBumonTool As ReferBumonDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferBumonList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferBumon() As Boolean
    '
    Dim masterBumonTool As New MasterBumonTool
    m_ReferBumonTool = New ReferBumonDialogTool(_ReferDialogCommonVariable)

    If MasterBumon.Loaded = True Then
      If _ReferBumon.Loaded = True Then
        'ロード済の時はクリア
        ReferBumonList.Clear()
        _ReferBumon.Loaded = False
      End If
      '取得済のマスターリストから
      ReferBumonList = m_ReferBumonTool.SettingReferItemsList(BEMasterBumonList)
    Else
      ReferBumonList = m_ReferBumonTool.LoadReferItemsList(connector, masterBumonTool)
    End If

    _ReferBumon.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferBumonDialog(ByVal code As String, ByVal location As Point) As String
    Dim masterBumonTool As New MasterBumonTool
    Dim dialogResult As DialogResult
    Dim newCode As String = String.Empty
    Try
      If _ReferBumon.Loaded = False Then
        m_ReferBumonTool = New ReferBumonDialogTool(_ReferDialogCommonVariable)
        ReferBumonList = m_ReferBumonTool.LoadReferItemsList(connector, masterBumonTool)
      End If

      Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferBumonTool.Setting, ReferBumonList, connector)
      LocateReferDialog(location, referDialog) '表示位置等を設定

      referDialog.IApplication = connector
      referDialog.SelectedItem = code

      '参照画面を表示
      dialogResult = referDialog.ShowDialog()
      newCode = referDialog.SelectedItem
      If (dialogResult = System.Windows.Forms.DialogResult.Cancel) Then newCode = String.Empty
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return newCode
  End Function


  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newCode">部門コード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingBumon(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    '
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、共通コードを設定し処理続行
      newCode = MakeText.MakeCommonCode(_BEKihonJoho.BumonKeta)
    End If

    'マスターを取得
    Dim beMasterBumon As BEMasterBumon = FindBEMasterBumon(newCode)

    If beMasterBumon.BumonCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Bumon), _
                                  "部門検索")
      End If
      Return False
    End If

    _ModifiedEntity.Header.BumonCode = beMasterBumon.BumonCode
    _ModifiedEntity.Header.BumonMei = beMasterBumon.BumonMei
    _ModifiedEntity.Header.BumonSoko = beMasterBumon.SokoCode

    Return True

  End Function
#End Region

#Region "   担当者"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferTantosya As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferTantosyaTool As ReferTantosyaDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferTantosyaList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferTantosya() As Boolean
    '
    Dim masterTantosyaTool As New MasterTantosyaTool
    m_ReferTantosyaTool = New ReferTantosyaDialogTool(_ReferDialogCommonVariable)

    If MasterTantosya.Loaded = True Then
      If _ReferTantosya.Loaded = True Then
        'ロード済の時はクリア
        ReferTantosyaList.Clear()
        _ReferTantosya.Loaded = False
      End If
      '取得済のマスターリストから
      ReferTantosyaList = m_ReferTantosyaTool.SettingReferItemsList(BEMasterTantosyaList)
    Else
      ReferTantosyaList = m_ReferTantosyaTool.LoadReferItemsList(connector, masterTantosyaTool)
    End If

    _ReferTantosya.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferTantosyaDialog(ByVal code As String, ByVal location As Point) As String
    '
    Dim masterTantosyaTool As New MasterTantosyaTool
    Dim dialogResult As DialogResult
    Dim newCode As String = String.Empty
    Try
      If _ReferTantosya.Loaded = False Then
        m_ReferTantosyaTool = New ReferTantosyaDialogTool(_ReferDialogCommonVariable)
        ReferTantosyaList = m_ReferTantosyaTool.LoadReferItemsList(connector, masterTantosyaTool)
      End If

      Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferTantosyaTool.Setting, ReferTantosyaList, connector)
      LocateReferDialog(location, referDialog) '表示位置等を設定

      referDialog.IApplication = connector
      referDialog.SelectedItem = code

      '参照画面を表示
      dialogResult = referDialog.ShowDialog()
      newCode = referDialog.SelectedItem
      If (dialogResult = System.Windows.Forms.DialogResult.Cancel) Then newCode = String.Empty
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return newCode
  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newCode">担当者コード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingTantosya(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    '
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、項目を初期化
      _ModifiedEntity.Header.TantosyaCode = String.Empty
      _ModifiedEntity.Header.TantosyaMei = String.Empty
      '部門は残したまま
      Return True
    End If

    'マスターを取得
    Dim beMasterTantosya As BEMasterTantosya = FindBEMasterTantosya(newCode)

    If beMasterTantosya.TantosyaCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Tantosya), _
                                  "担当者検索")
      End If
      Return False
    End If

    _ModifiedEntity.Header.TantosyaCode = beMasterTantosya.TantosyaCode
    _ModifiedEntity.Header.TantosyaMei = beMasterTantosya.TantosyaMei

    '部門マスターを取得
    Dim beMasterBumon As BEMasterBumon = FindBEMasterBumon(beMasterTantosya.BumonCode)
    If beMasterBumon.BumonCode.Length <> 0 Then
      _ModifiedEntity.Header.BumonCode = beMasterBumon.BumonCode
      _ModifiedEntity.Header.BumonMei = beMasterBumon.BumonMei
      _ModifiedEntity.Header.BumonSoko = beMasterBumon.SokoCode
    End If

    Return True

  End Function
#End Region

#Region "   プロジェクト"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferProject As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferProjectTool As ReferProjectDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferProjectList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferProject() As Boolean
    '
    Dim masterProjectTool As New MasterProjectTool
    m_ReferProjectTool = New ReferProjectDialogTool(_ReferDialogCommonVariable)

    If MasterProject.Loaded = True Then
      If _ReferProject.Loaded = True Then
        'ロード済の時はクリア
        ReferProjectList.Clear()
        _ReferProject.Loaded = False
      End If
      '取得済のマスターリストから
      ReferProjectList = m_ReferProjectTool.SettingReferItemsList(BEMasterProjectList)
    Else
      ReferProjectList = m_ReferProjectTool.LoadReferItemsList(connector, masterProjectTool)
    End If

    _ReferProject.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferProjectDialog(ByVal code As String, ByVal location As Point) As String
    '
    Dim masterProjectTool As New MasterProjectTool
    Dim dialogResult As DialogResult
    Dim newCode As String = String.Empty
    Try
      If _ReferProject.Loaded = False Then
        LoadBEMasterProject()

        m_ReferProjectTool = New ReferProjectDialogTool(_ReferDialogCommonVariable)

        'Hien edited 03/04 start
        'ReferProjectList = m_ReferProjectTool.LoadReferItemsList(connector, masterProjectTool)
        Dim newBEMasterProjectList As List(Of BEMasterProject) = New List(Of BEMasterProject)
        For i = 0 To BEMasterProjectList.Count - 1
          If BEMasterProjectList(i).ShiyoKubun = BusinessEntity.Defines.ShiyoKubunType.Kyoka Then
            newBEMasterProjectList.Add(BEMasterProjectList(i))
          End If
        Next

        ReferProjectList = m_ReferProjectTool.SettingReferItemsList(newBEMasterProjectList)
        'Hien edited 03/04 end 

      End If
      m_ReferProjectTool.Setting()

      Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferProjectTool.Setting, ReferProjectList, connector)
      LocateReferDialog(location, referDialog) '表示位置等を設定

      newCode = String.Empty

      referDialog.IApplication = connector
      referDialog.SelectedItem = code

      '参照画面を表示
      dialogResult = referDialog.ShowDialog()
      newCode = referDialog.SelectedItem
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return newCode
  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newcode">プロジェクトコード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingProject(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    '
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、項目を初期化
      _ModifiedEntity.Header.ProCode = String.Empty
      _ModifiedEntity.Header.ProMei = String.Empty
      Return True
    End If

    'プロジェクトマスターを取得
    Dim beMasterProject As BEMasterProject = FindBEMasterProject(newCode)

    If beMasterProject.ProCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, _BEKihonJoho.ProjectMei), _
                                  "プロジェクトマスタ検索")
      End If
      Return False
    End If

    _ModifiedEntity.Header.ProCode = beMasterProject.ProCode
    _ModifiedEntity.Header.ProMei = beMasterProject.ProMei

    Return True

  End Function

#End Region

#Region "   売上・仕入等の摘要"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferTekiyo As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferTekiyoTool As ReferCommonKubunDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferTekiyoList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferTekiyo() As Boolean
    '
    Dim masterTekiyoTool As New MasterTekiyoTool
    _ReferDialogCommonVariable.KubunId = PCA.TSC.Kon.BusinessEntity.Defines.KubunIdType.TekiyoKubun7
    m_ReferTekiyoTool = New ReferCommonKubunDialogTool(_ReferDialogCommonVariable)

    If MasterTekiyo.Loaded = True Then
      If _ReferTekiyo.Loaded = True Then
        'ロード済の時はクリア
        ReferTekiyoList.Clear()
        _ReferTekiyo.Loaded = False
      End If
      '取得済のマスターリストから
      ReferTekiyoList = m_ReferTekiyoTool.SettingReferItemsList(BEMasterTekiyoList)
    Else
      ReferTekiyoList = m_ReferTekiyoTool.LoadReferItemsList(connector, masterTekiyoTool)
    End If

    _ReferTekiyo.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferTekiyoDialog(ByVal code As String, ByVal location As Point) As String
    Dim masterTekiyoTool As New MasterTekiyoTool
    Dim dialogResult As DialogResult
    Dim newCode As String = String.Empty
    Try
      _ReferDialogCommonVariable.KubunId = BusinessEntity.Defines.KubunIdType.TekiyoKubun7  '受注売上摘要

      If _ReferTekiyo.Loaded = False Then
        m_ReferTekiyoTool = New ReferCommonKubunDialogTool(_ReferDialogCommonVariable)
        ReferTekiyoList = m_ReferTekiyoTool.LoadReferItemsList(connector, masterTekiyoTool)
      End If

      Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferTekiyoTool.Setting, ReferTekiyoList, connector)
      LocateReferDialog(location, referDialog) '表示位置等を設定

      referDialog.IApplication = connector
      referDialog.SelectedItem = code

      '参照画面を表示
      dialogResult = referDialog.ShowDialog()
      newCode = referDialog.SelectedItem
      If (dialogResult = System.Windows.Forms.DialogResult.Cancel) Then newCode = String.Empty

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return newCode
  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newcode">摘要コード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingTekiyo(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、処理続行
      Return True
    End If

    '摘要マスターを取得
    Dim beMasterTekiyo As BEMasterTekiyo = FindBEMasterTekiyo(BusinessEntity.Defines.KubunIdType.TekiyoKubun7, newCode)

    If beMasterTekiyo.TekiyoCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Tekiyo), _
                                  "摘要検索")
      End If
      Return False
    End If

    _ModifiedEntity.Header.TekiyoCode = beMasterTekiyo.TekiyoCode
    _ModifiedEntity.Header.Tekiyo = beMasterTekiyo.TekiyoMei

    Return True

  End Function


#End Region

#Region "   直送先"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferYms As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferYmsTool As ReferYmsDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferYmsList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferYms() As Boolean
    '
    Dim masterYmsTool As New MasterYmsTool
    m_ReferYmsTool = New ReferYmsDialogTool(_ReferDialogCommonVariable)

    If MasterYms.Loaded = True Then
      If _ReferYms.Loaded = True Then
        'ロード済の時はクリア
        ReferYmsList.Clear()
        _ReferYms.Loaded = False
      End If
      '取得済のマスターリストから
      ReferYmsList = m_ReferYmsTool.SettingReferItemsList(BEMasterYmsList)
    Else
      ReferYmsList = m_ReferYmsTool.LoadReferItemsList(connector, masterYmsTool)
    End If

    _ReferYms.Loaded = True

    Return True

  End Function

  ''' <summary>直送マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferYmsDialog(ByVal code As String, ByVal location As Point) As String
    Dim masterYmsTool As New MasterYmsTool
    Dim dialogResult As DialogResult
    Dim newCode As String = String.Empty
    Try

      If _ReferYms.Loaded = False Then
        _ReferDialogCommonVariable.SystemKubun = BusinessEntity.Defines.SystemKubunType.Kon
        m_ReferYmsTool = New ReferYmsDialogTool(_ReferDialogCommonVariable)
        ReferYmsList = m_ReferYmsTool.LoadReferItemsList(connector, masterYmsTool)
      End If

      Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferYmsTool.Setting, ReferYmsList, connector)
      LocateReferDialog(location, referDialog) '表示位置等を設定

      referDialog.IApplication = connector
      referDialog.SelectedItem = code

      '参照画面を表示
      dialogResult = referDialog.ShowDialog()
      newCode = referDialog.SelectedItem
      If (dialogResult = System.Windows.Forms.DialogResult.Cancel) Then newCode = String.Empty
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return newCode
  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newcode">直送先コード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingChokusosaki(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    '
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、項目を初期化
      _ModifiedEntity.Header.ChokusosakiCode = String.Empty
      _ModifiedEntity.Header.ChokusosakiMei1 = String.Empty
      _ModifiedEntity.Header.ChokusosakiMei2 = String.Empty
      _ModifiedEntity.Header.ChokusosakiComment = String.Empty
      Return True
    End If

    '直送先マスターを取得
    Dim beMasterYms As BEMasterYms = FindBEMasterYms(newCode)

    If beMasterYms.ChokusosakiCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Chokusosaki), _
                                  m_strMsgTitle)
      End If
      Return False
    End If

    _ModifiedEntity.Header.ChokusosakiCode = beMasterYms.ChokusosakiCode
    _ModifiedEntity.Header.ChokusosakiMei1 = beMasterYms.ChokusosakiMei1
    _ModifiedEntity.Header.ChokusosakiMei2 = beMasterYms.ChokusosakiMei2
    _ModifiedEntity.Header.ChokusosakiComment = beMasterYms.Comment

    Return True

  End Function

#End Region

#Region "   得意先、仕入先"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferTms As New LoadStatus

  Public Property ReferRms As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferTmsTool As ReferTmsRmsDialogTool

  Private m_ReferRmsTool As ReferTmsRmsDialogTool
  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferTmsList As List(Of ReferItems) = New List(Of ReferItems)

  Public Property ReferRmsList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferTms() As Boolean
    '
    Dim masterTmsTool As New MasterTmsTool
    m_ReferTmsTool = New ReferTmsRmsDialogTool(_ReferDialogCommonVariable)

    If MasterTms.Loaded = True Then
      If _ReferTms.Loaded = True Then
        'ロード済の時はクリア
        ReferTmsList.Clear()
        _ReferTms.Loaded = False
      End If
      '取得済のマスターリストから
      ReferTmsList = m_ReferTmsTool.SettingReferItemsList(BEMasterTmsList)
    Else
      ReferTmsList = m_ReferTmsTool.LoadReferItemsList(connector, masterTmsTool)
    End If

    _ReferTms.Loaded = True

    Return True

  End Function

  Public Function LoadReferRms() As Boolean
    '
    Dim masterRmsTool As New MasterRmsTool
    m_ReferRmsTool = New ReferTmsRmsDialogTool(_ReferDialogCommonVariable)

    If MasterRms.Loaded = True Then
      If _ReferRms.Loaded = True Then
        'ロード済の時はクリア
        ReferRmsList.Clear()
        _ReferRms.Loaded = False
      End If
      '取得済のマスターリストから
      ReferRmsList = m_ReferRmsTool.SettingReferItemsList(BEMasterTmsList)
    Else
      ReferRmsList = m_ReferRmsTool.LoadReferItemsList(connector, masterRmsTool)
    End If

    _ReferRms.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferTmsDialog(ByVal code As String, ByVal location As Point) As String
    Dim masterTmsTool As New MasterTmsTool
    Dim dialogResult As DialogResult
    Dim newCode As String = String.Empty
    Try
      If _ReferTms.Loaded = False Then
        _ReferDialogCommonVariable.SystemKubun = BusinessEntity.Defines.SystemKubunType.Kon
        m_ReferTmsTool = New ReferTmsRmsDialogTool(_ReferDialogCommonVariable)
        ReferTmsList = m_ReferTmsTool.LoadReferItemsList(connector, masterTmsTool)
      End If

      Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferTmsTool.Setting, ReferTmsList, connector)

      LocateReferDialog(location, referDialog) '表示位置等を設定

      newCode = String.Empty
      referDialog.IApplication = connector
      referDialog.SelectedItem = code

      '参照画面を表示
      dialogResult = referDialog.ShowDialog(m_Owner)
      newCode = referDialog.SelectedItem
      If (dialogResult = System.Windows.Forms.DialogResult.Cancel) Then newCode = String.Empty

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return newCode

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferRmsDialog(ByVal code As String, ByVal location As Point) As String
    '
    Dim masterRmsTool As New MasterRmsTool
    Dim dialogResult As DialogResult
    Dim newCode As String = String.Empty
    Try
      If _ReferRms.Loaded = False Then
        _ReferDialogCommonVariable.SystemKubun = BusinessEntity.Defines.SystemKubunType.Kan
        m_ReferRmsTool = New ReferTmsRmsDialogTool(_ReferDialogCommonVariable)
        ReferRmsList = m_ReferRmsTool.LoadReferItemsList(connector, masterRmsTool)
      End If

      Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferRmsTool.Setting, ReferRmsList, connector)

      LocateReferDialog(location, referDialog) '表示位置等を設定

      newCode = String.Empty
      referDialog.IApplication = connector
      referDialog.SelectedItem = code

      '参照画面を表示
      dialogResult = referDialog.ShowDialog(m_Owner)
      newCode = referDialog.SelectedItem
      If (dialogResult = System.Windows.Forms.DialogResult.Cancel) Then newCode = String.Empty

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    End Try
    Return newCode

  End Function


#End Region

#Region "   ロット詳細"

  ' ''' <summary>マスター参照画面を表示
  ' ''' </summary>
  ' ''' <param name="code">旧コード</param>
  ' ''' <param name="newCode">新コード</param>
  ' ''' <param name="location">参照画面表示位置</param>
  ' ''' <param name="connector">コネクタ</param>
  ' ''' 
  Public Function ShowReferLotDetailDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point, ByVal connector As IIntegratedApplication, _
                                           ByVal intLotDetailNo As Integer) As DialogResult
    '
    Dim masterLotDetailTool As New Sunloft.SearchEx.MasterLotDetailTool
    Dim masterLotDetailSettings As New Sunloft.SearchEx.LmdReferDialogSettings
    Dim dialogResult As DialogResult


    Dim referDialog As Sunloft.SearchEx.LmdReferDialog = New Sunloft.SearchEx.LmdReferDialog(masterLotDetailSettings, intLotDetailNo, connector)


    LocateReferLmdDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty

    referDialog.IApplication = connector
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog()
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

#End Region

#End Region

#Region "マスター取得"

#Region "   商品マスター取得"

  '商品マスター・取得状況
  Public Property MasterSms As New LoadStatus

  ''' <summary>商品マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterSmsList As New List(Of BusinessEntity.BEMasterSms)

  ''' <summary>商品マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterSms() As Boolean
    '
    If _MasterSms.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterSmsList.Clear()
      _MasterSms.Loaded = False
    End If

    'マスターを全件取得
    _BEMasterSmsList = FindBEMasterSmsList(String.Empty)
    _MasterSms.Loaded = True

    Return True

  End Function

  ''' <summary>商品マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterSmsList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterSms)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterSmsTool As New PCA.TSC.Kon.BusinessEntity.MasterSmsTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterSms As New PCA.TSC.Kon.BusinessEntity.BEMasterSms
    Dim beMasterSmsList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterSms)

    If code.Length <> 0 Then
      If _MasterSms.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterSmsList.Find(Function(s) Tools.TscStringTool.Equals(s.SyohinCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterSmsTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterSms = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterSms)
        beMasterSmsList.Add(beMasterSms)
      End If
    Else
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterSmsTool, findParameter)
      'API経由でテーブルから全件取得
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterSms = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterSms)
          beMasterSmsList.Add(beMasterSms)
        Next
      End If
    End If

    Return beMasterSmsList

  End Function

  ''' <summary>商品マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件</param>
  Public Function FindBEMasterSms(ByVal code As String) As BusinessEntity.BEMasterSms
    '
    Dim beMasterSms As New BusinessEntity.BEMasterSms
    Dim beMasterSmsList As New List(Of BusinessEntity.BEMasterSms)

    If code.Length <> 0 Then
      beMasterSmsList = FindBEMasterSmsList(code)
      If Not (beMasterSmsList Is Nothing) AndAlso beMasterSmsList.Count > 0 Then
        beMasterSms = beMasterSmsList.Item(0)
      End If
    End If

    Return beMasterSms

  End Function

#End Region

#Region "   倉庫マスター取得"

  '倉庫マスター・取得状況
  Public Property MasterSoko As New LoadStatus

  ''' <summary>倉庫マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterSokoList As New List(Of BusinessEntity.BEMasterSoko)

  ''' <summary>倉庫マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterSoko() As Boolean
    '
    If _MasterSoko.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterSokoList.Clear()
      _MasterSoko.Loaded = False
    End If

    _BEMasterSokoList = FindBEMasterSokoList(String.Empty)
    _MasterSoko.Loaded = True

    Return True

  End Function

  ''' <summary>倉庫マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterSokoList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterSoko)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterSokoTool As New PCA.TSC.Kon.BusinessEntity.MasterSokoTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterSoko As New PCA.TSC.Kon.BusinessEntity.BEMasterSoko
    Dim beMasterSokoList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterSoko)

    If code.Length <> 0 Then
      If _MasterSoko.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterSokoList.Find(Function(s) Tools.TscStringTool.Equals(s.SokoCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterSokoTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterSoko = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterSoko)
        beMasterSokoList.Add(beMasterSoko)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterSokoTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterSoko = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterSoko)
          beMasterSokoList.Add(beMasterSoko)
        Next
      End If
    End If

    Return beMasterSokoList

  End Function

  ''' <summary>倉庫マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterSoko(ByVal code As String) As BusinessEntity.BEMasterSoko
    '
    Dim beMasterSoko As New BusinessEntity.BEMasterSoko
    Dim beMasterSokoList As New List(Of BusinessEntity.BEMasterSoko)

    If code.Length <> 0 Then
      beMasterSokoList = FindBEMasterSokoList(code)
      If Not (beMasterSokoList Is Nothing) AndAlso beMasterSokoList.Count > 0 Then
        beMasterSoko = beMasterSokoList.Item(0)
      End If
    End If

    Return beMasterSoko

  End Function

#End Region

#Region "   部門マスター取得"

  '部門マスター・取得状況
  Public Property MasterBumon As New LoadStatus

  ''' <summary>部門マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterBumonList As New List(Of BusinessEntity.BEMasterBumon)

  ''' <summary>部門マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterBumon() As Boolean
    '
    If _MasterBumon.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterBumonList.Clear()
      _MasterBumon.Loaded = False
    End If

    _BEMasterBumonList = FindBEMasterBumonList(String.Empty)
    _MasterBumon.Loaded = True

    Return True

  End Function

  ''' <summary>部門マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterBumonList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterBumon)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterBumonTool As New PCA.TSC.Kon.BusinessEntity.MasterBumonTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterBumon As New PCA.TSC.Kon.BusinessEntity.BEMasterBumon
    Dim beMasterBumonList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterBumon)

    If code.Length <> 0 Then
      If _MasterBumon.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterBumonList.Find(Function(s) Tools.TscStringTool.Equals(s.BumonCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterBumonTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterBumon = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterBumon)
        beMasterBumonList.Add(beMasterBumon)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterBumonTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterBumon = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterBumon)
          beMasterBumonList.Add(beMasterBumon)
        Next
      End If
    End If

    Return beMasterBumonList

  End Function

  ''' <summary>部門マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterBumon(ByVal code As String) As BusinessEntity.BEMasterBumon
    '
    Dim beMasterBumon As New BusinessEntity.BEMasterBumon
    Dim beMasterBumonList As New List(Of BusinessEntity.BEMasterBumon)

    If code.Length <> 0 Then
      beMasterBumonList = FindBEMasterBumonList(code)
      If Not (beMasterBumonList Is Nothing) AndAlso beMasterBumonList.Count > 0 Then
        beMasterBumon = beMasterBumonList.Item(0)
      End If
    End If

    Return beMasterBumon

  End Function

#End Region

#Region "   担当者マスター取得"

  '担当者マスター・取得状況
  Public Property MasterTantosya As New LoadStatus

  ''' <summary>担当者マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterTantosyaList As New List(Of BusinessEntity.BEMasterTantosya)

  ''' <summary>担当者マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterTantosya() As Boolean
    '
    If _MasterTantosya.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterTantosyaList.Clear()
      _MasterTantosya.Loaded = False
    End If

    _BEMasterTantosyaList = FindBEMasterTantosyaList(String.Empty)
    _MasterTantosya.Loaded = True

    Return True

  End Function

  ''' <summary>担当者マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterTantosyaList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTantosya)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterTantosyaTool As New PCA.TSC.Kon.BusinessEntity.MasterTantosyaTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterTantosya As New PCA.TSC.Kon.BusinessEntity.BEMasterTantosya
    Dim beMasterTantosyaList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTantosya)

    If code.Length <> 0 Then
      If _MasterTantosya.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterTantosyaList.Find(Function(s) Tools.TscStringTool.Equals(s.TantosyaCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterTantosyaTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterTantosya = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterTantosya)
        beMasterTantosyaList.Add(beMasterTantosya)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterTantosyaTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterTantosya = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterTantosya)
          beMasterTantosyaList.Add(beMasterTantosya)
        Next
      End If
    End If

    Return beMasterTantosyaList

  End Function

  ''' <summary>担当者マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterTantosya(ByVal code As String) As BusinessEntity.BEMasterTantosya
    '
    Dim beMasterTantosya As New BusinessEntity.BEMasterTantosya
    Dim beMasterTantosyaList As New List(Of BusinessEntity.BEMasterTantosya)

    If code.Length <> 0 Then
      beMasterTantosyaList = FindBEMasterTantosyaList(code)
      If Not (beMasterTantosyaList Is Nothing) AndAlso beMasterTantosyaList.Count > 0 Then
        beMasterTantosya = beMasterTantosyaList.Item(0)
      End If
    End If

    Return beMasterTantosya

  End Function

#End Region

#Region "   プロジェクトマスター取得"

  'プロジェクトマスター・取得状況
  Public Property MasterProject As New LoadStatus

  ''' <summary>プロジェクトマスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterProjectList As New List(Of BusinessEntity.BEMasterProject)

  ''' <summary>プロジェクトマスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterProject() As Boolean
    '
    If _MasterProject.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterProjectList.Clear()
      _MasterProject.Loaded = False
    End If

    _BEMasterProjectList = FindBEMasterProjectList(String.Empty)
    _MasterProject.Loaded = True

    Return True

  End Function

  ''' <summary>プロジェクトマスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterProjectList(ByVal code As String) As List(Of BusinessEntity.BEMasterProject)
    '
    Dim findParameter As New Tools.FindParameter
    Dim masterProjectTool As New BusinessEntity.MasterProjectTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterProject As New BusinessEntity.BEMasterProject
    Dim beMasterProjectList As New List(Of BusinessEntity.BEMasterProject)

    If code.Length <> 0 Then
      If _MasterProject.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterProjectList.Find(Function(s) Tools.TscStringTool.Equals(s.ProCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterProjectTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterProject = DirectCast(beObject, BusinessEntity.BEMasterProject)
        beMasterProjectList.Add(beMasterProject)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterProjectTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterProject = DirectCast(beObjectList(i), BusinessEntity.BEMasterProject)
          beMasterProjectList.Add(beMasterProject)
        Next
      End If
    End If

    Return beMasterProjectList

  End Function

  ''' <summary>プロジェクトマスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterProject(ByVal code As String) As BusinessEntity.BEMasterProject
    '
    Dim beMasterProject As New BusinessEntity.BEMasterProject
    Dim beMasterProjectList As New List(Of BusinessEntity.BEMasterProject)

    If code.Length <> 0 Then
      beMasterProjectList = FindBEMasterProjectList(code)
      If Not (beMasterProjectList Is Nothing) AndAlso beMasterProjectList.Count > 0 Then
        beMasterProject = beMasterProjectList.Item(0)
      End If
    End If

    Return beMasterProject

  End Function

#End Region

#Region "   得意先マスター取得"

  '得意先マスター・取得状況
  Public Property MasterTms As New LoadStatus

  ''' <summary>得意先マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterTmsList As New List(Of BusinessEntity.BEMasterTms)

  ''' <summary>得意先マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterTms() As Boolean
    '
    If _MasterTms.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterTmsList.Clear()
      _MasterTms.Loaded = False
    End If

    _BEMasterTmsList = FindBEMasterTmsList(String.Empty)
    _MasterTms.Loaded = True

    Return True

  End Function

  ''' <summary>得意先マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterTmsList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTms)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterTmsTool As New PCA.TSC.Kon.BusinessEntity.MasterTmsTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterTms As New PCA.TSC.Kon.BusinessEntity.BEMasterTms
    Dim beMasterTmsList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTms)

    If code.Length <> 0 Then
      If _MasterTms.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterTmsList.Find(Function(s) Tools.TscStringTool.Equals(s.TokuisakiCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterTmsTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterTms = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterTms)
        beMasterTmsList.Add(beMasterTms)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterTmsTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterTms = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterTms)
          beMasterTmsList.Add(beMasterTms)
        Next
      End If
    End If

    Return beMasterTmsList

  End Function

  ''' <summary>得意先マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterTms(ByVal code As String) As BusinessEntity.BEMasterTms
    '
    Dim beMasterTms As New BusinessEntity.BEMasterTms
    Dim beMasterTmsList As New List(Of BusinessEntity.BEMasterTms)

    If code.Length <> 0 Then
      beMasterTmsList = FindBEMasterTmsList(code)
      If Not (beMasterTmsList Is Nothing) AndAlso beMasterTmsList.Count > 0 Then
        beMasterTms = beMasterTmsList.Item(0)
      End If
    End If

    Return beMasterTms

  End Function

#End Region

#Region "   直送先マスター取得"

  '直送先マスター・取得状況
  Public Property MasterYms As New LoadStatus

  ''' <summary>直送先マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterYmsList As New List(Of BusinessEntity.BEMasterYms)

  ''' <summary>直送先マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterYms() As Boolean
    '
    If _MasterYms.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterYmsList.Clear()
      _MasterYms.Loaded = False
    End If

    _BEMasterYmsList = FindBEMasterYmsList(String.Empty)
    _MasterYms.Loaded = True

    Return True

  End Function

  ''' <summary>直送先マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterYmsList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterYms)
    '
    Dim findParameter As New Tools.FindParameter
    Dim masterYmsTool As New PCA.TSC.Kon.BusinessEntity.MasterYmsTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterYms As New PCA.TSC.Kon.BusinessEntity.BEMasterYms
    Dim beMasterYmsList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterYms)

    If code.Length <> 0 Then
      If _MasterYms.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterYmsList.Find(Function(s) Tools.TscStringTool.Equals(s.ChokusosakiCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterYmsTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterYms = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterYms)
        beMasterYmsList.Add(beMasterYms)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterYmsTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterYms = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterYms)
          beMasterYmsList.Add(beMasterYms)
        Next
      End If
    End If

    Return beMasterYmsList

  End Function

  ''' <summary>直送先マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterYms(ByVal code As String) As BusinessEntity.BEMasterYms
    '
    Dim beMasterYms As New BusinessEntity.BEMasterYms
    Dim beMasterYmsList As New List(Of BusinessEntity.BEMasterYms)

    If code.Length <> 0 Then
      beMasterYmsList = FindBEMasterYmsList(code)
      If Not (beMasterYmsList Is Nothing) AndAlso beMasterYmsList.Count > 0 Then
        beMasterYms = beMasterYmsList.Item(0)
      End If
    End If

    Return beMasterYms

  End Function

#End Region

#Region "   仕入先マスター取得"

  '仕入先マスター・取得状況
  Public Property MasterRms As New LoadStatus

  ''' <summary>仕入先マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterRmsList As New List(Of BusinessEntity.BEMasterRms)

  ''' <summary>仕入先マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterRms() As Boolean
    '
    If _MasterRms.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterRmsList.Clear()
      _MasterRms.Loaded = False
    End If

    _BEMasterRmsList = FindBEMasterRmsList(String.Empty)
    _MasterRms.Loaded = True

    Return True

  End Function

  ''' <summary>仕入先マスターを取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterRmsList(ByVal code As String) As List(Of PCA.TSC.Kon.BusinessEntity.BEMasterRms)
    '
    Dim findParameter As New PCA.TSC.Kon.Tools.FindParameter
    Dim masterRmsTool As New PCA.TSC.Kon.BusinessEntity.MasterRmsTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterRms As New PCA.TSC.Kon.BusinessEntity.BEMasterRms
    Dim beMasterRmsList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterRms)

    If code.Length <> 0 Then
      If _MasterRms.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterRmsList.Find(Function(s) Tools.TscStringTool.Equals(s.ShiresakiCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterRmsTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterRms = DirectCast(beObject, PCA.TSC.Kon.BusinessEntity.BEMasterRms)
        beMasterRmsList.Add(beMasterRms)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterRmsTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterRms = DirectCast(beObjectList(i), PCA.TSC.Kon.BusinessEntity.BEMasterRms)
          beMasterRmsList.Add(beMasterRms)
        Next
      End If
    End If

    Return beMasterRmsList

  End Function

  ''' <summary>仕入先マスターを1件取得
  ''' </summary>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterRms(ByVal code As String) As BusinessEntity.BEMasterRms
    '
    Dim beMasterRms As New BusinessEntity.BEMasterRms
    Dim beMasterRmsList As New List(Of BusinessEntity.BEMasterRms)

    If code.Length <> 0 Then
      beMasterRmsList = FindBEMasterRmsList(code)
      If Not (beMasterRmsList Is Nothing) AndAlso beMasterRmsList.Count > 0 Then
        beMasterRms = beMasterRmsList.Item(0)
      End If
    End If

    Return beMasterRms

  End Function


#End Region

#Region "   摘要マスター取得"

  '摘要マスター・取得状況
  Public Property MasterTekiyo As New LoadStatus

  ''' <summary>摘要先マスター全件リストプロパティ
  ''' </summary>
  Public Property BEMasterTekiyoList As New List(Of BusinessEntity.BEMasterTekiyo)

  ''' <summary>摘要マスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEMasterTekiyo() As Boolean
    '
    If _MasterTekiyo.Loaded = True Then
      'ロード済の時はクリア
      _BEMasterTekiyoList.Clear()
      _MasterTekiyo.Loaded = False
    End If

    _BEMasterTekiyoList = FindBEMasterTekiyoList(0, String.Empty)
    _MasterTekiyo.Loaded = True

    Return True

  End Function

  ''' <summary>摘要マスターを取得
  ''' </summary>
  ''' <param name="kubunId">区分ID。ゼロの場合は全件取得</param>
  ''' <param name="code">マスターコード。空文字の場合は全件取得</param>
  Public Function FindBEMasterTekiyoList(ByVal kubunId As PCA.TSC.Kon.BusinessEntity.Defines.KubunIdType, ByVal code As String) As List(Of BusinessEntity.BEMasterTekiyo)
    '
    Dim findParameter As New Tools.FindParameter
    Dim masterTekiyoTool As New PCA.TSC.Kon.BusinessEntity.MasterTekiyoTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beMasterTekiyo As New PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo
    Dim beMasterTekiyoList As New List(Of PCA.TSC.Kon.BusinessEntity.BEMasterTekiyo)

    If kubunId <> 0 AndAlso code.Length <> 0 Then
      If _MasterTekiyo.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEMasterTekiyoList.Find(Function(s) s.KubunId = kubunId And Tools.TscStringTool.Equals(s.TekiyoCode, code, m_JapaneseCollation))
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.KubunIdUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.KubunId = kubunId
        findParameter.CodeUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Code = code.Trim()
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterTekiyoTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beMasterTekiyo = DirectCast(beObject, BusinessEntity.BEMasterTekiyo)
        beMasterTekiyoList.Add(beMasterTekiyo)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), masterTekiyoTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beMasterTekiyo = DirectCast(beObjectList(i), BusinessEntity.BEMasterTekiyo)
          beMasterTekiyoList.Add(beMasterTekiyo)
        Next
      End If
    End If

    Return beMasterTekiyoList

  End Function

  ''' <summary>摘要マスターを1件取得
  ''' </summary>
  ''' <param name="kubunId">区分ID。ゼロの場合はゼロ件取得</param>
  ''' <param name="code">マスターコード。空文字の場合はゼロ件取得</param>
  Public Function FindBEMasterTekiyo(ByVal kubunId As BusinessEntity.Defines.KubunIdType, ByVal code As String) As BusinessEntity.BEMasterTekiyo
    '
    Dim beMasterTekiyo As New BusinessEntity.BEMasterTekiyo
    Dim beMasterTekiyoList As New List(Of BusinessEntity.BEMasterTekiyo)

    If code.Length <> 0 Then
      beMasterTekiyoList = FindBEMasterTekiyoList(kubunId, code)
      If Not (beMasterTekiyoList Is Nothing) AndAlso beMasterTekiyoList.Count > 0 Then
        beMasterTekiyo = beMasterTekiyoList.Item(0)
      End If
    End If

    Return beMasterTekiyo

  End Function


#End Region

#Region "   領域ユーザーマスター取得"

  '領域ユーザーマスター・取得状況
  Public Property AreaUser As New LoadStatus

  ''' <summary>領域ユーザーマスター全件リストプロパティ
  ''' </summary>
  Public Property BEAreaUserList As New List(Of BusinessEntity.BEAreaUser)

  ''' <summary>領域ユーザーマスター全件をマスターリストプロパティに格納    
  ''' </summary>
  Public Function LoadBEAreaUser() As Boolean
    '
    If _AreaUser.Loaded = True Then
      'ロード済の時はクリア
      _BEAreaUserList.Clear()
      _AreaUser.Loaded = False
    End If

    _BEAreaUserList = FindBEAreaUserList(0)
    _AreaUser.Loaded = True

    Return True

  End Function

  ''' <summary>領域ユーザーマスターを取得
  ''' </summary>
  ''' <param name="id">ID。ゼロの場合は全件取得</param>
  Public Function FindBEAreaUserList(ByVal id As Integer) As List(Of BusinessEntity.BEAreaUser)
    '
    Dim findParameter As New Tools.FindParameter
    Dim areaUserTool As New BusinessEntity.AreaUserTool
    Dim notUniqueFlag As Boolean
    Dim beObject As New Object
    Dim beObjectList As New List(Of Object)
    Dim beAreaUser As New BusinessEntity.BEAreaUser
    Dim beAreaUserList As New List(Of BusinessEntity.BEAreaUser)

    If id <> 0 Then
      If _AreaUser.Loaded = True Then
        '全件マスターリストプロパティより取得
        beObject = _BEAreaUserList.Find(Function(s) s.Id = id)
      Else
        'API経由でテーブルから一件だけ取得
        findParameter.IdUsing = BusinessEntity.Defines.UsingType.UsingOneItem
        findParameter.Id = id
        beObject = Tools.KonAPI.FindUniqueBE(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), areaUserTool, findParameter, notUniqueFlag)
      End If
      If Not beObject Is Nothing Then
        beAreaUser = DirectCast(beObject, BusinessEntity.BEAreaUser)
        beAreaUserList.Add(beAreaUser)
      End If
    Else
      'API経由でテーブルから全件取得
      beObjectList = Tools.KonAPI.FindBEList(CType(connector, PCA.ApplicationIntegration.IntegratedApplication), areaUserTool, findParameter)
      If Not beObjectList Is Nothing Then
        For i As Integer = 0 To beObjectList.Count - 1
          beAreaUser = DirectCast(beObjectList(i), BusinessEntity.BEAreaUser)
          beAreaUserList.Add(beAreaUser)
        Next
      End If
    End If

    Return beAreaUserList

  End Function

  ''' <summary>領域ユーザーマスターを1件取得
  ''' </summary>
  ''' <param name="id">ID。ゼロの場合はゼロ件取得</param>
  Public Function FindBEAreaUser(ByVal id As Integer) As BusinessEntity.BEAreaUser
    '
    Dim beAreaUser As New BusinessEntity.BEAreaUser
    Dim beAreaUserList As New List(Of BusinessEntity.BEAreaUser)

    If id <> 0 Then
      beAreaUserList = FindBEAreaUserList(id)
      If Not (beAreaUserList Is Nothing) AndAlso beAreaUserList.Count > 0 Then
        beAreaUser = beAreaUserList.Item(0)
      End If
    End If

    Return beAreaUser

  End Function


#End Region

#End Region

End Class

﻿
Imports System.ComponentModel

Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon.BusinessEntity
Imports PCA.TSC.Kon.Tools
Imports PCA.TSC.Kon
Imports System.Xml
Imports PCA

Public Class TscInputControllerBase

  Inherits TscKonControllerBase


#Region "在庫割れチェック用エンティティ"

  ''' <summary>
  ''' 在庫割れチェック用エンティティクラス
  ''' </summary>
  Protected Class ZaikoCheckEntity

    ''' <summary>
    ''' 商品コード
    ''' </summary>
    Public Property SyohinCode() As String = String.Empty

    ''' <summary>
    ''' 倉庫コード
    ''' </summary>
    Public Property SokoCode() As String = String.Empty

    ''' <summary>
    ''' チェック数量
    ''' </summary>
    Public Property Suryo() As Decimal = 0D

    ''' <summary>
    ''' 現在庫数
    ''' </summary>
    Public Property Zaikosu() As Decimal = 0D

    ''' <summary>
    ''' 注文点
    ''' </summary>
    Public Property Chumonten() As Decimal = 0D

  End Class

#End Region

  ''' <summary>伝票入力用コンフィグエレメント
  ''' </summary>
  Private m_ExtendConfigXPaths As String() = {"ZeiTableLine", "PromptRegister", "PromptDeleteRow"}

#Region "プロパティ"

  ''' <summary>デフォルトの伝票日付を取得または設定します。
  ''' </summary>
  Public Property DefaultHizuke As Integer = 0

  ''' <summary>
  ''' 伝票参照画面（一覧表示）の表示項目リスト
  ''' </summary>
  Public Property SearchResultList As List(Of ReferItems)

  ''' <summary>
  ''' 検索状態かどうかを取得または設定します。
  ''' </summary>
  Public Property IsSearchModeOn As Boolean

  ''' <summary>
  ''' 一覧検索状態かどうかを取得または設定します。
  ''' </summary>
  Public Property IsIchiranModeOn As Boolean
  'Public Property IsListing As Boolean

  ''' <summary>
  ''' 登録コマンドが有効かどうかを取得または設定します。
  ''' </summary>
  Public Property SaveState As Boolean

  ''' <summary>
  ''' データを入力中かどうかを取得または設定します。
  ''' </summary>
  Public Property IsEditing As Boolean

  ''' <summary>
  ''' 現在メインフォームの初期化中かどうかを取得または設定します。
  ''' </summary>
  Public Property IsInitializing As Boolean

  ''' <summary>
  ''' 選択状態のエンティティが編集可能なエンティティかどうかを取得または設定します。
  ''' </summary>
  Public Property ModifiableEntity As Boolean

  ''' <summary>
  ''' 検索方向（false:前伝票、true:次伝票）
  ''' </summary>
  Public Property SearchDirection As Boolean

  ''' <summary>
  ''' 税率リストを取得します。
  ''' </summary>
  Public Property ZeiRitsuList As List(Of Decimal)

  ''' <summary>
  ''' 税率テーブルを取得します。
  ''' </summary>
  Public Property TaxList As List(Of BETAX)

  ''' <summary>
  ''' 選択された業務エンティティに対して変更された内容を取得します。
  ''' </summary>
  Public Property ModifiedEntity As PEKonInput

  ''' <summary>
  ''' 選択された業務エンティティを取得または設定します。
  ''' </summary>
  Public Property SelectedEntity As PEKonInput = Nothing
  ''' <summary>
  ''' メッセージの重複を防ぐためのフラグを取得または設定します。
  ''' </summary>
  Public Property AlreadyShowMessage As Boolean

  ''' <summary>税率別合計テーブルの表示行数を取得。
  ''' </summary>
  Public ReadOnly Property ZeiTableLine() As Integer
    Get
      Return _ZeiTableLine
    End Get
  End Property
  Private _ZeiTableLine As Integer = 1

  ''' <summary>登録確認の要否を取得。
  ''' </summary>
  Public ReadOnly Property PromptRegister() As Boolean
    Get
      Return _PromptRegister
    End Get
  End Property
  Private _PromptRegister As Boolean = False

  ''' <summary>行削除確認の要否を取得。
  ''' </summary>
  Public ReadOnly Property PromptDeleteRow() As Boolean
    Get
      Return _PromptDeleteRow
    End Get
  End Property
  Private _PromptDeleteRow As Boolean = False

  ''' <summary>伝票種別を取得。
  ''' </summary>
  Public ReadOnly Property DenpyoType() As Tools.PEKonInput.DenpyoType
    Get
      Return _DenpyoType
    End Get
  End Property
  Private _DenpyoType As Tools.PEKonInput.DenpyoType = PEKonInput.DenpyoType.Uriage

  ''' <summary>システム区分を取得。
  ''' </summary>
  Public ReadOnly Property SystemKubun() As BusinessEntity.Defines.SystemKubunType
    Get
      Return _SystemKubun
    End Get
  End Property
  Private _SystemKubun As BusinessEntity.Defines.SystemKubunType = BusinessEntity.Defines.SystemKubunType.Kan

  'Public Property MainForm As PCA.BaseFramework.ClientBase

#End Region

  ''' <summary>コンストラクタ
  ''' </summary>
  Public Sub New(ByVal owner As IWin32Window, ByVal denpyoType As Tools.PEKonInput.DenpyoType)
    '
    MyBase.New(owner)

    '伝票種別を設定
    _DenpyoType = denpyoType

    'システム区分を設定
    If _DenpyoType = PEKonInput.DenpyoType.Mitsumori _
    OrElse _DenpyoType = PEKonInput.DenpyoType.Juchu _
    OrElse _DenpyoType = PEKonInput.DenpyoType.Uriage Then
      '見積、受注、売上時
      _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon
    End If

  End Sub

  ''' <summary>コントローラークラスの初期化
  ''' </summary>
  ''' <returns>初期化に成功したときTrue</returns>
  ''' <remarks></remarks>
  Public Overridable Function Initialize(ByRef configXMLDoc As XmlDocument) As Boolean

    'ログイン
    If (Not PrepareApplication(m_ExtendConfigXPaths, configXMLDoc)) Then
      Return False
    End If

    '入力可能期間をチェック
    If _BEKihonJoho.NyuryokuKikanFrom = 0 OrElse _BEKihonJoho.NyuryokuKikanTo = 0 Then
      ShowMessage.OnInformation(m_Owner, MessageDefines.Information.NoNyuryokuKikan, CommonVariable.BasicConfig.ProgramName)
      Return False
    End If

    ' 税率リストを作成
    Me.ZeiRitsuList = New List(Of Decimal)
    Me.ZeiRitsuList = Tools.MakeText.MakeZeiRitsuList(BEKihonJoho)

    TaxList = BEKihonJoho.TaxRate.BETAX

    Return True

  End Function

#Region "ログイン・ログオフ"

  ''' <summary>コンフィグファイルをロードしPCAクラウドAPIを使用可能にする。
  ''' （補足）成功時はTrue、エラー発生時はFalse。
  ''' </summary>
  Public Function PrepareApplication(ByVal extendConfigXPaths As String(), ByRef configXMLDoc As XmlDocument) As Boolean

    Dim extendConfigStrings As String()

    If MyBase.PrepareApplicationBasic(configXMLDoc) = False Then
      Return False
    End If

    extendConfigStrings = Tools.Config.ConfigXMLDocToStrings(configXMLDoc, extendConfigXPaths)

    '税率別合計欄の行数
    If IsNumeric(extendConfigStrings(0).Trim()) Then
      _ZeiTableLine = CInt(extendConfigStrings(0).Trim)
    End If
    '登録時確認の有無
    If extendConfigStrings(1).Length <> 0 _
    AndAlso extendConfigStrings(1).ToUpper = "TRUE" Then
      _PromptRegister = True
    End If
    '行削除確認の有無
    If extendConfigStrings(2).Length <> 0 _
    AndAlso extendConfigStrings(2).ToUpper = "TRUE" Then
      _PromptDeleteRow = True
    End If

    Return True

  End Function

#End Region

#Region "ツール"

  ''' <summary>デフォルトの伝票日付を設定。
  ''' </summary>
  Public Sub InitializeDefaultHizuke()

    DefaultHizuke = Tools.Convert.DateToHizuke(Date.Today())

    If (DefaultHizuke < _BEKihonJoho.NyuryokuKikanFrom) Then
      DefaultHizuke = _BEKihonJoho.NyuryokuKikanFrom
    End If
    If (DefaultHizuke > _BEKihonJoho.NyuryokuKikanTo) Then
      DefaultHizuke = _BEKihonJoho.NyuryokuKikanTo
    End If

  End Sub

  ''' <summary>伝区から請求（精算）日の入力可否を判定
  ''' </summary>
  ''' <param name="denku">伝区</param>
  Public Function EnabledKakeHizuke(ByVal denku As PCA.TSC.Kon.BusinessEntity.Defines.DenkuType) As Boolean
    '
    If denku = PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Kake Then
      Return True
    Else
      Return False
    End If

  End Function

  ''' <summary>売上、仕入の伝票日付変更後のチェックを実施。
  ''' （補足）エラー時にはヘッダーラベル表示文言を返す。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="oldHizuke">変更前の日付</param>
  Public Function ValidatingDenpyoHizukeChaged(ByRef peKonInput As PEKonInput, ByVal oldHizuke As Integer) As String
    '
    Dim headerLabelText As String = String.Empty

    If peKonInput.Header.Denku = PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Kake Then
      '掛の時、請求（精算）日が同日ならコピー
      If oldHizuke = peKonInput.Header.DenpyoHizuke Then
        peKonInput.Header.Hizuke = peKonInput.Header.DenpyoHizuke
      End If
    End If

    '請求（精算）締日と在庫締切日をチェック
    headerLabelText = CheckSeiAndZaikoZumi(peKonInput)

    Return headerLabelText

  End Function

  ''' <summary>請求（精算）締日と在庫締切日をチェック
  ''' （補足）エラー時にはヘッダーラベル表示文言を返す。
  ''' </summary>
  ''' <param name="peKonInput"></param>
  Public Function CheckSeiAndZaikoZumi(ByVal peKonInput As PEKonInput) As String
    '
    Dim seiText As String = String.Empty
    '請求、精算の何れかを設定
    If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kan Then
      seiText = PCA.TSC.Kon.BusinessEntity.ItemText.Seikyu
    Else
      seiText = PCA.TSC.Kon.BusinessEntity.ItemText.Seisan
    End If

    Dim headerLabelText As String = String.Empty

    Dim seiZumi As Boolean = False      '請求（精算）済の場合はTrue
    Dim zaikoZumi As Boolean = False    '在庫締切済の場合はTrue

    '請求（精算）済を判定
    seiZumi = IsSeiShimeZumi(peKonInput)
    '在庫締切済を判定
    zaikoZumi = IsZaikoShimeZumi(peKonInput)

    If seiZumi And Not (zaikoZumi) Then
      '請求（精算）済のみ
      headerLabelText = String.Format(Tools.MessageDefines.HeaderLabel.CheckHizukeBySei, seiText)
    ElseIf Not (seiZumi) And zaikoZumi Then
      '在庫締切済のみ
      headerLabelText = Tools.MessageDefines.HeaderLabel.CheckHizukeByZaiko
    ElseIf seiZumi And zaikoZumi Then
      '請求（精算）済かつ在庫締切済
      headerLabelText = String.Format(Tools.MessageDefines.HeaderLabel.CheckHizukeBySeiZaiko, seiText)
    End If

    Return headerLabelText

  End Function

  ''' <summary>請求（精算）締切済をチェック。請求（支払）先が未設定の時は無条件にFalse。
  ''' </summary>
  ''' <param name="peKonInput"></param>
  ''' <returns>請求（精算）済の場合はTrue</returns>
  ''' <remarks></remarks>
  Public Function IsSeiShimeZumi(ByVal peKonInput As PEKonInput) As Boolean
    '
    '請求（精算）締切済みで、掛伝票の請求（精算）日が締日以前
    Return (peKonInput.Header.SeiKikanTo > 0 _
            And peKonInput.Header.Denku = PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Kake _
            And peKonInput.Header.Hizuke <= peKonInput.Header.SeiKikanTo)

  End Function

  ''' <summary>在庫締切済をチェック
  ''' </summary>
  ''' <param name="peKonInput"></param>
  ''' <returns>在庫締切済の場合はTrue</returns>
  ''' <remarks></remarks>
  Public Function IsZaikoShimeZumi(ByVal peKonInput As PEKonInput) As Boolean
    '
    '契約以外の伝票で、伝票日付が締日以前
    Return (peKonInput.Header.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Keiyaku _
            And peKonInput.Header.DenpyoHizuke <= MyBase.BEKihonJoho.ZaikoKikanTo)

  End Function

  ''' <summary>ユーザーIDのユーザー名を取得します。ユーザーが存在しない場合は渡されたIDの文字列を返す。
  ''' </summary>
  ''' <param name="id">ユーザーID</param>
  Public Function FindAreaUserName(ByVal id As Integer) As String
    '
    Dim beAreaUser As BEAreaUser = MyBase.FindBEAreaUser(id)
    If beAreaUser.Id = 0 Then
      '「特定できません」ではなく処理IDと同様にユーザーIDそのものとする
      Return id.ToString()
    Else
      Return beAreaUser.NameCache.Trim()
    End If

  End Function

  ''' <summary>未収残高を取得。
  ''' </summary>
  ''' <param name="beMasterTms">得意先マスター</param>
  Public Function FindMisyuZandaka(ByVal beMasterTms As BEMasterTms) As Decimal
    '
    Dim seikyusaki As New BEMasterTms

    seikyusaki = FindSeikyusaki(beMasterTms)

    Return KonCalc.CalcMisyuZandaka(seikyusaki)

  End Function

  ''' <summary>請求先の得意先マスターを取得。
  ''' </summary>
  ''' <param name="tokuisaki">得意先</param>
  Public Function FindSeikyusaki(ByVal tokuisaki As BEMasterTms) As BEMasterTms
    '
    Dim seikyusaki As New BEMasterTms

    If tokuisaki.TokuisakiCode = tokuisaki.SeikyusakiCode Then
      '得意先＝請求先、親
      seikyusaki = tokuisaki
    Else
      seikyusaki = MyBase.FindBEMasterTms(tokuisaki.SeikyusakiCode)
    End If

    Return seikyusaki

  End Function

  ''' <summary>スポット得意先の登録画面を表示。
  ''' （補足）入力されたスポット得意先のマスター項目、キャンセルボタン押下時はNothingを返す。
  ''' </summary>
  ''' <param name="spotTokuisaki">>変更前の得意先マスター</param>
  ''' <param name="isNew">スポット新規登録かどうか</param>
  ''' <param name="tokuisakiKeisyo">得意先の敬称</param>
  Public Function ShowSpotTokuisakiDialog(ByRef spotTokuisaki As BEMasterTms, ByVal isNew As Boolean, ByVal tokuisakiKeisyo As String) As BEMasterTms

    If isNew Then
      Dim dlgResult As DialogResult = ShowMessage.OnQuestion(m_Owner, String.Format(MessageDefines.Question.InputSpotTorihikisaki, PCA.TSC.Kon.BusinessEntity.ItemText.Tokuisaki), CommonVariable.BasicConfig.ProgramName)
      If dlgResult <> Windows.Forms.DialogResult.Yes Then
        Return Nothing
      End If
    End If

    Dim resultTokuisaki As New BEMasterTms

    Dim dlg As New InputSpotTmsDialog(_BEKihonJoho)

    If spotTokuisaki IsNot Nothing AndAlso spotTokuisaki.TokuisakiCode.Length <> 0 Then
      dlg.TokuisakiMei1 = spotTokuisaki.TokuisakiMei1
      dlg.TokuisakiMei2 = spotTokuisaki.TokuisakiMei2
      dlg.SenpoTantosyaMei = spotTokuisaki.SenpoTantosyaMei
      dlg.Keisyo = tokuisakiKeisyo
      dlg.MailAddress = spotTokuisaki.AitesakiMailAddress
      dlg.YubinBango = spotTokuisaki.YubinBango
      dlg.Jyusyo1 = spotTokuisaki.Jyusyo1
      dlg.Jyusyo2 = spotTokuisaki.Jyusyo2
      dlg.TelNo = spotTokuisaki.AitesakiTelNo
      dlg.FAXNo = spotTokuisaki.AitesakiFaxNo
    Else
      '得意先の敬称を渡しておく
      dlg.Keisyo = tokuisakiKeisyo
    End If

    If dlg.ShowDialog() = DialogResult.OK Then
      resultTokuisaki.TokuisakiMei1 = dlg.TokuisakiMei1
      resultTokuisaki.TokuisakiMei2 = dlg.TokuisakiMei2
      resultTokuisaki.SenpoTantosyaMei = dlg.SenpoTantosyaMei
      resultTokuisaki.AitesakiKeisyo = dlg.Keisyo
      resultTokuisaki.AitesakiMailAddress = dlg.MailAddress
      resultTokuisaki.YubinBango = dlg.YubinBango
      resultTokuisaki.Jyusyo1 = dlg.Jyusyo1
      resultTokuisaki.Jyusyo2 = dlg.Jyusyo2
      resultTokuisaki.AitesakiTelNo = dlg.TelNo
      resultTokuisaki.AitesakiFaxNo = dlg.FAXNo
    Else
      resultTokuisaki = Nothing
    End If

    Return resultTokuisaki

  End Function

#End Region

#Region "入力補助"

  ''' <summary>
  ''' 入力が開始されたときに呼ばれるメソッドです。
  ''' </summary>
  ''' <returns>正常に入力開始できたかどうか</returns>
  ''' <remarks></remarks>
  Public Function BeginChanged() As Boolean
    '
    'フォームの初期化中は編集状態へ移行しない
    If IsInitializing Then
      Return False
    End If

    SaveState = True
    IsEditing = True

    Return True
  End Function

  ''' <summary>
  ''' 入力が終了されたときに呼ばれるメソッドです。
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub EndChanged()
    SaveState = False
    IsEditing = False
  End Sub

#End Region

#Region "入力制御"

  ''' <summary>
  ''' 在アクティブ状態（参照中、ロック中）になっているエンティティを更新します。
  ''' </summary>
  ''' <param name="activeEntity">アクティブ状態のエンティティ</param>
  ''' <remarks></remarks>
  Public Sub UpdateActiveEntity(ByVal activeEntity As PEKonInput)
    '
    Me.SelectedEntity = activeEntity
    Me.ModifiedEntity = Me.MakeCloneEntity(activeEntity)

  End Sub

  ''' <summary>エンティティのクローンを作成。
  ''' </summary>
  ''' <param name="entity">作成対象のエンティティ</param>
  ''' <returns>クローン作成されたエンティティ</returns>
  ''' <remarks></remarks>
  Public Function MakeCloneEntity(ByVal entity As PEKonInput) As PEKonInput
    '
    Return DirectCast(entity.Clone(), PEKonInput)

  End Function

#End Region

#Region "チェック関数"

  ''' <summary>日付が入力可能期間範囲内かをチェック
  ''' </summary>
  ''' <param name="denpyoHizuke">伝票日付</param>
  ''' <param name="needMessageBox">メッセージボックス表示要否</param>
  Public Function CheckNyuryokuKikan(ByVal denpyoHizuke As Integer, needMessageBox As Boolean) As Boolean
    '
    If denpyoHizuke < _BEKihonJoho.NyuryokuKikanFrom OrElse denpyoHizuke > _BEKihonJoho.NyuryokuKikanTo Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, Tools.MessageDefines.Exclamation.DenpyoHizukeIsOutside, CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    Else
      Return True
    End If

  End Function

  ''' <summary>小数桁を含む各単価の値範囲をチェック。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティクラス</param>
  ''' <param name="denpyoType">伝票入力種別</param>
  Public Function CheckTankaKeta(peKonInput As PEKonInput, denpyoType As Tools.PEKonInput.DenpyoType) As Boolean
    '
    If peKonInput Is Nothing Or peKonInput.DetailList Is Nothing OrElse peKonInput.DetailList.Count = 0 Then
      Return True
    End If

    Dim errorMessage As String = String.Empty
    Dim kon As Boolean = False

    Select Case denpyoType
      Case PCA.TSC.Kon.Tools.PEKonInput.DenpyoType.Mitsumori, _
       PCA.TSC.Kon.Tools.PEKonInput.DenpyoType.Juchu, _
       PCA.TSC.Kon.Tools.PEKonInput.DenpyoType.Uriage
        '見積、受注、売上
        kon = True
    End Select

    For i As Integer = 0 To peKonInput.DetailList.Count - 1

      Dim detail As PEKonInputDetail = peKonInput.DetailList(i)

      If String.IsNullOrEmpty(detail.SyohinCode) OrElse detail.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Kiji Then
        Continue For
      End If

      Dim curMax As Decimal = 0
      Dim curMin As Decimal = 0
      Dim tankaKeta As Short = 0

      If detail.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan Then
        '一般商品は小数桁の設定値、以外は小数桁はゼロ固定
        tankaKeta = detail.TankaKeta
      End If

      Select Case tankaKeta
        Case 0
          curMax = InputDefines.MaximumTanka0
          curMin = InputDefines.MinimumTanka0
          Exit Select
        Case 1
          curMax = InputDefines.MaximumTanka1
          curMin = InputDefines.MinimumTanka1
          Exit Select
        Case 2
          curMax = InputDefines.MaximumTanka2
          curMin = InputDefines.MinimumTanka2
          Exit Select
        Case 3
          curMax = InputDefines.MaximumTanka3
          curMin = InputDefines.MinimumTanka3
          Exit Select
        Case 4
          curMax = InputDefines.MaximumTanka4
          curMin = InputDefines.MinimumTanka4
          Exit Select
      End Select

      If detail.MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan Then
        '一般商品
        Select Case denpyoType
          Case Tools.PEKonInput.DenpyoType.Uriage, _
               Tools.PEKonInput.DenpyoType.Shire
            If (detail.Ku = BusinessEntity.Defines.KuType.Tankateisei AndAlso curMin > detail.Tanka) _
            OrElse (detail.Ku <> BusinessEntity.Defines.KuType.Tankateisei AndAlso 0 > detail.Tanka) _
            OrElse curMax < detail.Tanka Then
              '売上、仕入の単価訂正時のみ単価に負値を許す
              errorMessage = "単価が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If
          Case Else
            If (0 > detail.Tanka) OrElse (curMax < detail.Tanka) Then
              errorMessage = "単価が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If
        End Select

        If KonCalc.CountSyosuKeta(detail.Tanka) > CInt(tankaKeta) Then
          errorMessage = "単価が設定可能小数桁を超えている明細が存在するため登録できません。"
          Exit For
        End If

        If kon Then
          '販売管理のみ
          If (0 > detail.GenTanka) OrElse (curMax < detail.GenTanka) Then
            errorMessage = "原単価が設定可能範囲を超えている明細が存在するため登録できません。"
            Exit For
          End If

          If (curMin > detail.BaiTanka) OrElse (curMax < detail.BaiTanka) Then
            errorMessage = "売単価が設定可能範囲を超えている明細が存在するため登録できません。"
            Exit For
          End If

          If KonCalc.CountSyosuKeta(detail.GenTanka) > CInt(tankaKeta) Then
            errorMessage = "原単価が設定可能小数桁を超えている明細が存在するため登録できません。"
            Exit For
          End If

          If KonCalc.CountSyosuKeta(detail.BaiTanka) > CInt(tankaKeta) Then
            errorMessage = "売単価が設定可能小数桁を超えている明細が存在するため登録できません。"
            Exit For
          End If
        End If
      End If

      Select Case detail.MasterKubun
        Case BusinessEntity.Defines.MasterKubunType.Ippan, _
             BusinessEntity.Defines.MasterKubunType.Zatsuhin, _
             BusinessEntity.Defines.MasterKubunType.Zappi
          '一般商品、雑商品、諸雑費
          If kon Then
            '販売管理
            If 0 > detail.HyojunKakaku2 OrElse curMax < detail.HyojunKakaku2 Then
              errorMessage = "標準価格が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If curMin > detail.HyojunKakaku OrElse curMax < detail.HyojunKakaku Then
              errorMessage = "標準価格が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If KonCalc.CountSyosuKeta(detail.HyojunKakaku2) > CInt(tankaKeta) Then
              errorMessage = "標準価格が設定可能小数桁を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If KonCalc.CountSyosuKeta(detail.HyojunKakaku) > CInt(tankaKeta) Then
              errorMessage = "標準価格が設定可能小数桁を超えている明細が存在するため登録できません。"
              Exit For
            End If
          Else
            '仕入管理
            If (curMin > detail.HyojunShireTanka) OrElse (curMax < detail.HyojunShireTanka) Then
              errorMessage = "標準仕入単価が設定可能範囲を超えている明細が存在するため登録できません。"
              Exit For
            End If

            If KonCalc.CountSyosuKeta(detail.HyojunShireTanka) > CInt(tankaKeta) Then
              errorMessage = "標準仕入単価が設定可能小数桁を超えている明細が存在するため登録できません。"
              Exit For
            End If

          End If
      End Select

    Next

    If Not String.IsNullOrEmpty(errorMessage) Then
      '何れかのエラーが存在する時
      ShowMessage.OnExclamation(m_Owner, errorMessage, CommonVariable.BasicConfig.ProgramName)
      Return False
    End If

    Return True

  End Function

  ' ''' <summary>商品の有効期間内をチェック。
  ' ''' </summary>
  ' ''' <param name="beMasterSms">商品マスター</param>
  ' ''' <param name="denpyoHizuke">伝票日付</param>
  ' ''' <returns>true：有効期間内、false：有効期間外</returns>
  ' ''' <remarks></remarks>
  'Public Function CheckSyohinYukoKikan(ByVal beMasterSms As BEMasterSms, ByVal denpyoHizuke As Integer) As Boolean
  '    '
  '    Return KonCalc.CheckSyohinYukoKikan(beMasterSms, denpyoHizuke)

  'End Function

  ' ''' <summary>商品の有効期間内をチェック。
  ' ''' </summary>
  ' ''' <param name="beMasterSms">商品マスター</param>
  ' ''' <param name="denpyoHizuke1">伝票日付１</param>
  ' ''' <param name="denpyoHizuke2">伝票日付２</param>
  'Public Function CheckSyohinYukoKikan(ByVal beMasterSms As BEMasterSms, ByVal denpyoHizuke1 As Integer, ByVal denpyoHizuke2 As Integer) As Boolean
  '    '
  '    Return KonCalc.CheckSyohinYukoKikan(beMasterSms, denpyoHizuke1, denpyoHizuke2)

  'End Function

  ''' <summary>商品の有効期間内をチェックし、続行確認画面を表示。
  ''' （補足）有効期間外有り、処理中断時はFalseを返す。
  ''' </summary>
  ''' <param name="detailList">伝票入力明細エンティティ</param>
  ''' <param name="denpyoHizuke">伝票日付</param>
  Public Function CheckSyohinYukoKikanAndPrompt(ByVal detailList As List(Of PEKonInputDetail), ByVal denpyoHizuke As Integer) As Boolean

    If IsNothing(detailList) OrElse detailList.Count = 0 OrElse denpyoHizuke = 0 Then
      Return True
    End If

    Dim syohinCodeList As New List(Of String)()
    syohinCodeList = PrepareSyohinCodeList(detailList)

    Return CheckSyohinYukoKikanAndPrompt(syohinCodeList, denpyoHizuke)

  End Function

  ''' <summary>商品の有効期間内をチェックし、続行確認画面を表示。
  ''' （補足）有効期間外有り、処理中断時はFalseを返す。
  ''' </summary>
  ''' <param name="syohinCodeList">商品コードリスト</param>
  ''' <param name="denpyoHizuke">伝票日付</param>
  ''' <returns>true：有効期間内または続行、false：有効期間外</returns>
  ''' <remarks></remarks>
  Private Function CheckSyohinYukokikanAndPrompt(ByVal syohinCodeList As List(Of String), ByVal denpyoHizuke As Integer) As Boolean

    Dim result As Boolean = True

    If Not (IsNothing(syohinCodeList)) And syohinCodeList.Count > 0 Then

      '商品コードリストから該当商品の商品マスターリストを作成
      Dim beMasterSmsList As New List(Of BEMasterSms)
      beMasterSmsList = FindBySyohinCodeList(syohinCodeList)

      Dim resultSyohinCodeList As New List(Of String)()

      For Each beMasterSms As BEMasterSms In beMasterSmsList
        'If (beMasterSms.YukoKikanKaishi <> 0 And denpyoHizuke < beMasterSms.YukoKikanKaishi) _
        'OrElse (beMasterSms.YukoKikanSyuryo <> 0 And denpyoHizuke > beMasterSms.YukoKikanSyuryo) Then
        If KonCalc.CheckSyohinYukoKikan(beMasterSms, denpyoHizuke) = False Then
          '有効期間内かをチェック
          resultSyohinCodeList.Add("(" & beMasterSms.SyohinCode.PadRight(_BEKihonJoho.SyohinKeta) & ")" & beMasterSms.SyohinMei)
        End If
      Next

      If resultSyohinCodeList.Count > 0 Then
        '期間外の商品が存在する時は処理続行の確認を実行
        Dim listBoxDialog As Tools.TscListBoxDialog = _
            New Tools.TscListBoxDialog(CommonVariable.BasicConfig.ProgramName, _
                                       Controls.PcaGuideLabel.Icons.Question,
                                       "以下の商品は有効期間外となります。" & vbCrLf & "処理を続行しますか？", _
                                       53, _
                                        2, _
                                       resultSyohinCodeList)
        '
        listBoxDialog.CancelVisible = CStr(True)
        listBoxDialog.OkText = "続行"
        listBoxDialog.CancelText = "キャンセル"

        If listBoxDialog.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
          'キャンセルボタン押下時
          Return False
        End If

      End If
    End If

    Return result

  End Function

  ''' <summary>重複排除し伝票入力明細エンティティに設定されている商品コードのリストを作成。
  ''' </summary>
  ''' <param name="detailList">売上明細のリスト</param>
  Public Function PrepareSyohinCodeList(ByVal detailList As List(Of PEKonInputDetail)) As List(Of String)
    '
    Dim syohinCodeList As List(Of String) = New List(Of String)

    For Each detail In detailList
      Dim syohinCode As String = detail.SyohinCode
      If Not (String.IsNullOrEmpty(detail.SyohinCode)) _
      And Not (syohinCodeList.Exists(Function(match) match = syohinCode)) Then
        syohinCodeList.Add(detail.SyohinCode)
      End If
    Next

    Return syohinCodeList

  End Function

  ''' <summary>商品コードリストから該当商品の商品マスターリストを作成。
  ''' </summary>
  ''' <param name="syohinCodeList">商品コードのリスト</param>
  Public Function FindBySyohinCodeList(ByVal syohinCodeList As List(Of String)) As List(Of BEMasterSms)
    '
    Dim beMasterSmsList As New List(Of BEMasterSms)

    For Each syohinCode In syohinCodeList
      Dim beMasterSms As BEMasterSms = MyBase.FindBEMasterSms(syohinCode)
      If beMasterSms.SyohinCode.Length <> 0 Then
        beMasterSmsList.Add(beMasterSms)
      End If
    Next

    ' 商品コード順にソート
    'beMasterSmsList.Sort(Function(a, b) b.SyohinCode - a.SyohinCode)

    Return beMasterSmsList

  End Function

  ''' <summary>合計金額、合計原価の桁数をチェック。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  Public Function CheckGokeiKeta(ByVal peKonInput As PEKonInput) As Boolean
    '
    Dim checkEntity As PEKonInput = peKonInput.Clone()
    Dim genkaGokei As Decimal = 0D

    KonCalc.CalcTotal(_DenpyoType, checkEntity, ZeiRitsuList, Me._BEKihonJoho.ZeiHasuKurai)

    For Each detail As PEKonInputDetail In checkEntity.DetailList
      If KonCalc.JudgeDetailState(_DenpyoType, detail) = peKonInput.MeisaiStateType.Valid Then
        genkaGokei = genkaGokei + detail.Genka
      End If
    Next

    '伝票全体合計
    If genkaGokei > InputDefines.MaximumKingaku _
    OrElse genkaGokei < InputDefines.MinimumKingaku Then
      '原価合計が範囲外の時
      Return False
    End If

    If (checkEntity.Total.KingakuGokei + checkEntity.Total.SotoZeiGokei > InputDefines.MaximumKingaku) _
     OrElse (checkEntity.Total.KingakuGokei + checkEntity.Total.SotoZeiGokei < InputDefines.MinimumKingaku) Then
      '売上合計＋外税合計が範囲外の時
      Return False
    End If

    '税率別合計
    For i As Integer = 0 To checkEntity.SubtotalList.Count - 1
      If (checkEntity.SubtotalList(i).KingakuGokei + checkEntity.SubtotalList(i).SotoZeiGokei > InputDefines.MaximumKingaku) _
      OrElse (checkEntity.SubtotalList(i).KingakuGokei + checkEntity.SubtotalList(i).SotoZeiGokei < InputDefines.MinimumKingaku) Then
        '売上合計＋外税合計が範囲外の時
        Return False
      End If
    Next

    Return True

  End Function


#End Region

#Region "◆編集メニュー"

  ''' <summary>税区分の変更画面を表示、税項目に変更があれば計算し直し伝票入力エンティティに格納。
  ''' （補足）OKボタン押下時にTrueを返す。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="rowIndex">行インデックス</param>
  Public Function ShowInputZeikubunDialogAndValidating(ByVal peKonInput As PEKonInput, ByVal rowIndex As Integer, ByRef headerLabelText As String) As Boolean

    '税区分の変更画面を初期化
    Dim dlg As New InputZeiKubunDialog(Me._BEKihonJoho)

    '変更前の値を退避
    Dim oldZeiRitsu As Decimal = peKonInput.DetailList(rowIndex).ZeiRitsu
    Dim oldZeikomiKubun As BusinessEntity.Defines.ZeikomiKubnType = peKonInput.DetailList(rowIndex).ZeikomiKubun
    dlg.ZeiRitsu = oldZeiRitsu
    dlg.ZeikomiKubun = oldZeikomiKubun

    '税区分の変更画面を表示
    If dlg.ShowDialog(m_Owner) <> DialogResult.OK Then
      'キャンセルボタン押下時は終了
      Return False
    End If

    '税率、税区分が変更された時は再計算
    If oldZeiRitsu <> dlg.ZeiRitsu OrElse oldZeikomiKubun <> dlg.ZeikomiKubun Then
      '変更を開始
      Me.BeginChanged()

      peKonInput.DetailList(rowIndex).ZeiRitsu = dlg.ZeiRitsu
      peKonInput.DetailList(rowIndex).ZeikomiKubun = dlg.ZeikomiKubun

      '金額以外を再計算
      Me.CalcNotKingakuItems(peKonInput, rowIndex, False, False, False, True, headerLabelText)
      '合計を再計算
      Me.CalcTotal(peKonInput, True, _DenpyoType)
    End If

    Return True

  End Function

  ''' <summary>スポット得意先の登録を表示、入力した値を伝票入力エンティティに格納。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <remarks></remarks>
  Public Sub ValidatingSpotTokuisaki(ByRef peKonInput As PEKonInput)
    '
    Dim spotTokuisaki As New BEMasterTms

    '新規登録の場合は空白で表示する
    If peKonInput.Header.SpotAitesakiId = -1 OrElse peKonInput.Header.SpotAitesakiId > 0 Then
      spotTokuisaki.TokuisakiMei1 = peKonInput.Header.TorihikisakiMei1
      spotTokuisaki.TokuisakiMei2 = peKonInput.Header.TorihikisakiMei2
      spotTokuisaki.SenpoTantosyaMei = peKonInput.Header.SenpoTantosyaMei
      spotTokuisaki.AitesakiKeisyo = peKonInput.Header.Keisyo
      spotTokuisaki.AitesakiMailAddress = peKonInput.Header.MailAddress
      spotTokuisaki.YubinBango = peKonInput.Header.YubinBango
      spotTokuisaki.Jyusyo1 = peKonInput.Header.Jyusyo1
      spotTokuisaki.Jyusyo2 = peKonInput.Header.Jyusyo2
      spotTokuisaki.AitesakiTelNo = peKonInput.Header.TelNo
      spotTokuisaki.AitesakiFaxNo = peKonInput.Header.FAXNo

    Else
      spotTokuisaki.AitesakiKeisyo = peKonInput.Header.Keisyo
      '訂正時だと敬称が空の場合があるので得意先マスターから敬称を取得する
      If String.IsNullOrEmpty(peKonInput.Header.Keisyo) AndAlso peKonInput.Header.AitesakiId <> 0 Then
        Dim beMasterTms As BEMasterTms = MyBase.FindBEMasterTms(MakeText.MakeCommonCode(_BEKihonJoho.TokuisakiKeta))
        If beMasterTms.TokuisakiCode.Length <> 0 Then
          spotTokuisaki.AitesakiKeisyo = beMasterTms.AitesakiKeisyo
        End If
      End If
    End If

    Dim resultTokuisaki As BEMasterTms = Me.ShowSpotTokuisakiDialog(spotTokuisaki, False, peKonInput.Header.Keisyo)

    If resultTokuisaki Is Nothing Then
      'スポット得意先入力画面でキャンセルボタン押下
      Return
    End If

    '変更が開始された
    Me.BeginChanged()

    peKonInput.Header.TorihikisakiMei1 = resultTokuisaki.TokuisakiMei1
    peKonInput.Header.TorihikisakiMei2 = resultTokuisaki.TokuisakiMei2
    peKonInput.Header.SenpoTantosyaMei = resultTokuisaki.SenpoTantosyaMei
    peKonInput.Header.Keisyo = resultTokuisaki.AitesakiKeisyo
    peKonInput.Header.MailAddress = resultTokuisaki.AitesakiMailAddress
    peKonInput.Header.YubinBango = resultTokuisaki.YubinBango
    peKonInput.Header.Jyusyo1 = resultTokuisaki.Jyusyo1
    peKonInput.Header.Jyusyo2 = resultTokuisaki.Jyusyo2
    peKonInput.Header.TelNo = resultTokuisaki.AitesakiTelNo
    peKonInput.Header.FAXNo = resultTokuisaki.AitesakiFaxNo

    '新規登録の時は登録フラグとして-1を設定
    If peKonInput.Header.SpotAitesakiId = 0 Then
      peKonInput.Header.SpotAitesakiId = -1
    End If

  End Sub

  ''' <summary>伝票複写時に各マスターの使用区分（商品は有効期間も）を一括チェック。
  ''' （補足）エラーがあり続行確認画面でキャンセルボタンを押下でFalseを返す。
  ''' </summary>
  ''' <param name="denpyoType">伝票種類</param>
  Public Function CheckShiyoKubunOfAllMasterAndPrompt(ByVal denpyoType As Tools.PEKonInput.DenpyoType) As Boolean
    '
    If _BEKihonJoho Is Nothing Then
      Return False
    End If

    If IsNothing(Me.SelectedEntity) OrElse IsNothing(Me.SelectedEntity.Header) Then
      Return False
    End If

    Dim result As Boolean = True

    Dim errorMessage As New List(Of String)()

    '得意先
    Select Case denpyoType
      Case PEKonInput.DenpyoType.Mitsumori, _
           PEKonInput.DenpyoType.Juchu, _
           PEKonInput.DenpyoType.Uriage
        '見積、受注、売上
        Dim beMasterTms As BEMasterTms = MyBase.FindBEMasterTms(Me.SelectedEntity.Header.TorihikisakiCode)
        If beMasterTms.TokuisakiCode.Length <> 0 Then
          If beMasterTms.ShiyoKubun = PCA.TSC.Kon.BusinessEntity.Defines.ShiyoKubunType.Kinshi Then
            '使用禁止
            errorMessage.Add("■得意先")
            errorMessage.Add("(" & beMasterTms.TokuisakiCode.PadRight(_BEKihonJoho.TokuisakiKeta) & ")" & Me.SelectedEntity.Header.TorihikisakiMei1)
          End If
        End If
      Case PEKonInput.DenpyoType.Hachu
        '発注で直送先に得意先の指定有り
        If Me.SelectedEntity.Header.ChokusosakiFlag = PCA.TSC.Kon.BusinessEntity.Defines.ChokusosakiFlagType.Tokuisaki Then
          Dim beMasterTms As BEMasterTms = MyBase.FindBEMasterTms(Me.SelectedEntity.Header.ChokusosakiCode)
          If beMasterTms.TokuisakiCode.Length <> 0 Then
            '使用禁止
            If beMasterTms.ShiyoKubun = PCA.TSC.Kon.BusinessEntity.Defines.ShiyoKubunType.Kinshi Then
              errorMessage.Add("■得意先")
              errorMessage.Add("(" & beMasterTms.TokuisakiCode.PadRight(_BEKihonJoho.TokuisakiKeta) & ")" & Me.SelectedEntity.Header.ChokusosakiMei1)
            End If
          End If
        End If
    End Select

    '仕入先
    Select Case denpyoType
      Case PEKonInput.DenpyoType.Hachu, _
           PEKonInput.DenpyoType.Shire
        '発注、仕入
        Dim beMasterRms As BEMasterRms = MyBase.FindBEMasterRms(Me.SelectedEntity.Header.TorihikisakiCode)
        If beMasterRms.ShiresakiCode.Length <> 0 Then
          If beMasterRms.ShiyoKubun = PCA.TSC.Kon.BusinessEntity.Defines.ShiyoKubunType.Kinshi Then
            '使用禁止
            If errorMessage.Count > 0 Then
              errorMessage.Add(" ")
            End If
            errorMessage.Add("■仕入先")
            errorMessage.Add("(" & beMasterRms.ShiresakiCode.PadRight(_BEKihonJoho.ShiresakiKeta) + ")" & Me.SelectedEntity.Header.TorihikisakiMei1)
          End If
        End If
    End Select

    '直送先
    If Me.SelectedEntity.Header.ChokusosakiCode.Length <> 0 Then
      If denpyoType = PEKonInput.DenpyoType.Juchu _
      OrElse denpyoType = PEKonInput.DenpyoType.Uriage _
      OrElse (denpyoType = PEKonInput.DenpyoType.Hachu And SelectedEntity.Header.ChokusosakiFlag = PCA.TSC.Kon.BusinessEntity.Defines.ChokusosakiFlagType.Chokusosaki) Then
        '受注、売上、発注で直送先の指定有り
        Dim beMasterYms As BEMasterYms = FindBEMasterYms(Me.SelectedEntity.Header.ChokusosakiCode)
        If beMasterYms.ChokusosakiCode.Length <> 0 Then
          If beMasterYms.ShiyoKubun = PCA.TSC.Kon.BusinessEntity.Defines.ShiyoKubunType.Kinshi Then
            '使用禁止
            If errorMessage.Count > 0 Then
              errorMessage.Add(" ")
            End If
            errorMessage.Add("■直送先")
            errorMessage.Add("(" & beMasterYms.ChokusosakiCode.PadRight(_BEKihonJoho.ChokusosakiKeta) & ")" & Me.SelectedEntity.Header.ChokusosakiMei1)
          End If
        End If
      End If
    End If
    'プロジェクト
    If Me.SelectedEntity.Header.ProCode.Length <> 0 Then
      Dim beMasterProject As BEMasterProject = MyBase.FindBEMasterProject(Me.SelectedEntity.Header.ProCode)
      If beMasterProject.ProCode.Length <> 0 Then
        If beMasterProject.ShiyoKubun = PCA.TSC.Kon.BusinessEntity.Defines.ShiyoKubunType.Kinshi Then
          '使用禁止
          If errorMessage.Count > 0 Then
            errorMessage.Add(" ")
          End If
          errorMessage.Add("■" & _BEKihonJoho.ProjectMei)
          errorMessage.Add("(" & beMasterProject.ProCode.PadRight(16) & ")" & Me.SelectedEntity.Header.ProMei)
        End If
      End If
    End If
    '商品
    If Me.SelectedEntity.DetailList IsNot Nothing AndAlso Me.SelectedEntity.DetailList.Count > 0 Then

      ' 商品コードリストを作成
      Dim smsCodeList As New List(Of String)
      smsCodeList = PrepareSyohinCodeList(SelectedEntity.DetailList)

      ' 商品コードリストから該当商品の商品マスターリストを作成
      Dim beMasterSmsList As New List(Of BEMasterSms)
      beMasterSmsList = FindBySyohinCodeList(smsCodeList)

      smsCodeList.Clear()

      For Each beMasterSms As BEMasterSms In beMasterSmsList
        If beMasterSms.ShiyoKubun = PCA.TSC.Kon.BusinessEntity.Defines.ShiyoKubunType.Kinshi _
        OrElse KonCalc.CheckSyohinYukoKikan(beMasterSms, Me.DefaultHizuke) = False Then
          '使用禁止、または有効期間外
          smsCodeList.Add("(" & beMasterSms.SyohinCode.PadRight(_BEKihonJoho.SyohinKeta) & ")" & beMasterSms.SyohinMei)
        End If
      Next

      If smsCodeList.Count > 0 Then
        If errorMessage.Count > 0 Then
          errorMessage.Add(" ")
        End If
        errorMessage.Add("■商品")
        errorMessage.AddRange(smsCodeList)
      End If

    End If

    If errorMessage.Count > 0 Then
      'エラーが存在する時は処理続行の確認を実行。
      Using listBoxDialog As New TscListBoxDialog(CommonVariable.BasicConfig.ProgramName, Controls.PcaGuideLabel.Icons.Question, _
        "以下のマスターは使用禁止または有効期間外です。" & vbCr & vbLf & "処理を続行しますか？", 58, 2, errorMessage)
        listBoxDialog.CancelVisible = CStr(True)
        listBoxDialog.OkText = "続行"
        listBoxDialog.CancelText = "キャンセル"
        If listBoxDialog.ShowDialog() = DialogResult.Cancel Then
          'キャンセルボタン押下時
          result = False
        End If
      End Using

    End If

    Return result

  End Function

  ''' <summary>行削除前に確認。
  ''' （補足）キャンセル時はFalseを返す。
  ''' </summary>
  Public Function ConfirmRowDelete() As Boolean
    '
    Dim yes As Boolean = True

    If _PromptDeleteRow Then
      '行削除前に確認を要する時
      Dim dlgResult As DialogResult = ShowMessage.OnQuestion(m_Owner, MessageDefines.Question.DeleteRow, CommonVariable.BasicConfig.ProgramName)
      yes = (dlgResult = Windows.Forms.DialogResult.Yes)
    End If

    Return yes

  End Function

  ''' <summary>変更後に未登録の時に登録確認を実行。
  ''' （補足）キャンセル時は保留としてFalseを返す。
  ''' </summary>
  ''' <param name="saveYes">YESボタン押下</param>
  Public Function ConfirmSaveChanged(ByRef saveYes As Boolean) As Boolean
    '
    Dim confirm As Boolean = True

    Dim dlgResult As DialogResult = ShowMessage.OnConfirm(m_Owner, MessageDefines.Question.SaveChangingData, CommonVariable.BasicConfig.ProgramName)
    If dlgResult = Windows.Forms.DialogResult.Yes Then
      confirm = True
      saveYes = True
    ElseIf dlgResult = Windows.Forms.DialogResult.No Then
      confirm = True
      saveYes = False
    ElseIf dlgResult = Windows.Forms.DialogResult.Cancel Then
      confirm = False
      saveYes = False
    End If

    Return confirm

  End Function

  ''' <summary>全ての明細が空（商品コードが未入力）か判定。
  ''' </summary>
  ''' <param name="peKonInput"></param>
  Public Function IsAllRowEmpty(ByVal peKonInput As PEKonInput) As Boolean
    '
    Dim detailCount As Integer = 0

    '共通エンティティの商品コードの設定有無をチェック
    For Each detail As PEKonInputDetail In peKonInput.DetailList
      If Not String.IsNullOrEmpty(detail.SyohinCode) Then
        detailCount += 1
        Exit For
      End If
    Next

    If detailCount = 0 Then
      Return True
    Else
      Return False
    End If

  End Function

#End Region

#Region "共通計算"

  ''' <summary>明細を計算。数量または計算後数量に変更がある時は在庫割れをチェック。
  ''' </summary>
  ''' <param name="changedItem">変更された項目</param>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="rowIndex">行インデックス</param>
  ''' <param name="showMessage">エラーが発生したときにメッセージを表示するか</param>
  ''' <param name="headerLabelText">ヘッダーラベルに表示するメッセージ</param>
  ''' <returns>処理結果</returns>
  Public Function CalcMeisai(ByVal changedItem As InputDefines.ChangedItem, _
                             ByRef peKonInput As PEKonInput, _
                             ByVal rowIndex As Integer, _
                             ByVal showMessage As Boolean, _
                             ByRef headerLabelText As String) As Boolean
    '
    Return CalcMeisai(changedItem, peKonInput, rowIndex, showMessage, True, headerLabelText)

  End Function

  ''' <summary>明細を計算。
  ''' （補足）戻り値がTrue時でもヘッダーラベルメッセージを返す。
  ''' </summary>
  ''' <param name="changedItem">変更された項目</param>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="rowIndex">行インデックス</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="checkZaikoware">数量または計算後数量に変更がある時の在庫割れチェック要否</param>
  ''' <param name="headerLabelText">ヘッダーラベルに表示するメッセージ</param>
  Public Function CalcMeisai(ByVal changedItem As InputDefines.ChangedItem, _
                             ByRef peKonInput As PEKonInput, _
                             ByVal rowIndex As Integer, _
                             ByVal needMessageBox As Boolean, _
                             ByVal checkZaikoware As Boolean, _
                             ByRef headerLabelText As String) As Boolean
    '
    Dim denpypName As String = String.Empty
    Dim name As String = String.Empty

    If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
      denpypName = BusinessEntity.ItemText.Juchu
      name = BusinessEntity.ItemText.Uriage
    Else
      denpypName = BusinessEntity.ItemText.Hachu
      name = BusinessEntity.ItemText.Shiire
    End If

    '計算前の項目値を退避
    Dim oldIrisu As Decimal = peKonInput.DetailList(rowIndex).Irisu
    Dim oldHakosu As Decimal = peKonInput.DetailList(rowIndex).Hakosu
    Dim oldSuryo As Decimal = peKonInput.DetailList(rowIndex).Suryo
    Dim oldTanka As Decimal = peKonInput.DetailList(rowIndex).Tanka
    Dim oldKingaku As Decimal = peKonInput.DetailList(rowIndex).Kingaku

    '発注、仕入用に退避
    Dim reservedGenka As Decimal = peKonInput.DetailList(rowIndex).Genka
    Dim reservedBaikaKingaku As Decimal = peKonInput.DetailList(rowIndex).BaikaKingaku
    Dim reservedArarieki As Decimal = peKonInput.DetailList(rowIndex).Ararieki

    Dim curSuryo As Decimal = 0D
    Select Case changedItem
      Case InputDefines.ChangedItem.Irisu
        '入数が変更された時
        If peKonInput.DetailList(rowIndex).Irisu <> 0D _
        AndAlso peKonInput.DetailList(rowIndex).Hakosu <> 0D Then
          '入数、箱数が共にゼロ以外の時、数量を計算
          curSuryo = peKonInput.DetailList(rowIndex).Irisu * peKonInput.DetailList(rowIndex).Hakosu
          peKonInput.DetailList(rowIndex).Suryo = KonCalc.Round(curSuryo, peKonInput.DetailList(rowIndex).SuryoKeta, peKonInput.DetailList(rowIndex).SuryoHasu)
        Else
          '入数がゼロの時は箱数をクリア
          peKonInput.DetailList(rowIndex).Hakosu = 0D
        End If
      Case InputDefines.ChangedItem.Hakosu
        '箱数が変更された時
        If peKonInput.DetailList(rowIndex).Hakosu <> 0D Then
          '箱数がゼロ以外の時は数量を計算
          curSuryo = peKonInput.DetailList(rowIndex).Irisu * peKonInput.DetailList(rowIndex).Hakosu
          peKonInput.DetailList(rowIndex).Suryo = KonCalc.Round(curSuryo, peKonInput.DetailList(rowIndex).SuryoKeta, peKonInput.DetailList(rowIndex).SuryoHasu)
        End If
    End Select

    If _DenpyoType = peKonInput.DenpyoType.Uriage OrElse _DenpyoType = peKonInput.DenpyoType.Shire Then
      '売上、仕入の各連動時は、数量確定後に変更前後の符号をチェック
      If peKonInput.DetailList(rowIndex).RendoHeaderId <> 0D _
      AndAlso ((oldSuryo > 0D AndAlso peKonInput.DetailList(rowIndex).Suryo < 0D) _
      OrElse (oldSuryo < 0D AndAlso peKonInput.DetailList(rowIndex).Suryo > 0D)) Then
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, String.Format(MessageDefines.Exclamation.CantChangeSuryoSign, denpypName, name), CommonVariable.BasicConfig.ProgramName)
        End If
        peKonInput.DetailList(rowIndex).Irisu = oldIrisu
        peKonInput.DetailList(rowIndex).Hakosu = oldHakosu
        peKonInput.DetailList(rowIndex).Suryo = oldSuryo
        peKonInput.DetailList(rowIndex).Tanka = oldTanka
        peKonInput.DetailList(rowIndex).Kingaku = oldKingaku

        Return False
      End If
    End If

    '金額と原価を計算
    If Me.CalcKingakuAndGenka(peKonInput.DetailList(rowIndex), peKonInput.Header.KingakuHasu, changedItem, needMessageBox) = False Then
      peKonInput.DetailList(rowIndex).Irisu = oldIrisu
      peKonInput.DetailList(rowIndex).Hakosu = oldHakosu
      peKonInput.DetailList(rowIndex).Suryo = oldSuryo
      peKonInput.DetailList(rowIndex).Tanka = oldTanka
      peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
      Return False
    End If

    '売価を計算
    If Me.CalcBaikaKingaku(peKonInput.DetailList(rowIndex), peKonInput.Header.KingakuHasu, changedItem, needMessageBox) = False Then
      peKonInput.DetailList(rowIndex).Irisu = oldIrisu
      peKonInput.DetailList(rowIndex).Hakosu = oldHakosu
      peKonInput.DetailList(rowIndex).Suryo = oldSuryo
      peKonInput.DetailList(rowIndex).Tanka = oldTanka
      peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
      Return False
    End If

    If changedItem = InputDefines.ChangedItem.Suryo OrElse peKonInput.DetailList(rowIndex).Suryo <> oldSuryo Then
      '数量、または計算後数量に変更がある時、在庫割れをチェック
      Me.CalcNotKingakuItems(peKonInput, rowIndex, False, checkZaikoware, True, needMessageBox, headerLabelText)
    Else
      If changedItem = InputDefines.ChangedItem.Tanka _
      OrElse changedItem = InputDefines.ChangedItem.Gentanka _
      OrElse changedItem = InputDefines.ChangedItem.Kingaku _
      OrElse changedItem = InputDefines.ChangedItem.Genka Then
        '単価、原単価、金額、原価の変更時は原価割れをチェック
        Me.CalcNotKingakuItems(peKonInput, rowIndex, True, False, True, needMessageBox, headerLabelText)
      Else
        Me.CalcNotKingakuItems(peKonInput, rowIndex, False, False, True, needMessageBox, headerLabelText)
      End If
    End If

    If _DenpyoType = peKonInput.DenpyoType.Hachu _
    OrElse _DenpyoType = peKonInput.DenpyoType.Shire Then
      '発注と仕入でも原価・売価・粗利益が計算されてしまうので戻す
      peKonInput.DetailList(rowIndex).Genka = reservedGenka
      peKonInput.DetailList(rowIndex).BaikaKingaku = reservedBaikaKingaku
      peKonInput.DetailList(rowIndex).Ararieki = reservedArarieki
    End If

    Return True

  End Function

  ''' <summary>値引商品の値引率（数量）を変更した時の値引額を計算。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="rowIndex">行インデックス</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function CalcNebikiByRitsu(ByRef peKonInput As PEKonInput, ByVal rowIndex As Integer, ByVal needMessageBox As Boolean) As Boolean
    '
    'ゼロの時は、単位をクリア
    If peKonInput.DetailList(rowIndex).Suryo = 0D Then
      peKonInput.DetailList(rowIndex).Tani = String.Empty
      Return True
    End If

    Dim curGokeiKingaku As Decimal = 0D
    Dim curNebikikingaku As Decimal = 0D

    If rowIndex = 0 Then
      '先頭行は値引ゼロ
      peKonInput.DetailList(rowIndex).Kingaku = 0D
    Else
      For i As Integer = 0 To rowIndex - 1
        '対象明細より上にある有効明細の金額を合計する
        If KonCalc.JudgeDetailState(_DenpyoType, peKonInput.DetailList(i)) = peKonInput.MeisaiStateType.Valid Then
          curGokeiKingaku = curGokeiKingaku + peKonInput.DetailList(i).Kingaku
        End If

        '値引額を算出
        curNebikikingaku = curGokeiKingaku * peKonInput.DetailList(rowIndex).Suryo * -1D
        curNebikikingaku = curNebikikingaku / 100D
        curNebikikingaku = KonCalc.Round(curNebikikingaku, 0, PCA.TSC.Kon.BusinessEntity.Defines.HasuType.Kirisute)
        '金額範囲をチェック
        If curNebikikingaku > InputDefines.MaximumKingaku OrElse curNebikikingaku < InputDefines.MinimumKingaku Then
          If needMessageBox Then
            ShowMessage.OnExclamation(m_Owner, _
                                      String.Format(Tools.MessageDefines.Exclamation.Overflow, BusinessEntity.ItemText.Kingaku), _
                                      CommonVariable.BasicConfig.ProgramName)
          End If
          Return False
        End If
        peKonInput.DetailList(rowIndex).Kingaku = curNebikikingaku
      Next
    End If

    Dim headerLabelText As String = String.Empty
    '原価割れ・在庫割れチェックを行わず、消費税、粗利益、利益率、合計を計算
    Me.CalcNotKingakuItems(peKonInput, rowIndex, False, False, True, needMessageBox, headerLabelText)

    Return True

  End Function

  ''' <summary>金額計算後、消費税、粗利益、利益率、指定により合計を計算。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="rowIndex">行インデックス</param>
  ''' <param name="checkGenkaware">原価割れチェック要否</param>
  ''' <param name="checkZaikoware">在庫割れチェック要否</param>
  ''' <param name="calculateTotal">合計計算の要否</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="headerLabelText">ヘッダーラベルに表示するメッセージ</param>
  Public Sub CalcNotKingakuItems(ByRef peKonInput As PEKonInput, _
                                 ByVal rowIndex As Integer, _
                                 ByVal checkGenkaware As Boolean, _
                                 ByVal checkZaikoware As Boolean, _
                                 ByVal calculateTotal As Boolean, _
                                 ByVal needMessageBox As Boolean, _
                                 ByRef headerLabelText As String)
    '
    Dim syohizeiTsuchi As PCA.TSC.Kon.BusinessEntity.Defines.SyohizeiTsuchiType = PCA.TSC.Kon.BusinessEntity.Defines.SyohizeiTsuchiType.Menzei
    If _DenpyoType = peKonInput.DenpyoType.Hachu OrElse _DenpyoType = peKonInput.DenpyoType.Shire Then
      syohizeiTsuchi = peKonInput.Header.SyohizeiTsuchiKan
    Else
      syohizeiTsuchi = peKonInput.Header.SyohizeiTsuchiKon
    End If

    Dim sotoZeigaku As Decimal = peKonInput.DetailList(rowIndex).SotoZeigaku
    Dim uchiZeigaku As Decimal = peKonInput.DetailList(rowIndex).UchiZeigaku
    '消費税を計算
    KonCalc.CalcMeisaiZei(peKonInput.Header.SyohizeiHasu, _
                          syohizeiTsuchi,
                          peKonInput.DetailList(rowIndex).ZeiRitsu, _
                          peKonInput.DetailList(rowIndex).ZeikomiKubun, _
                          peKonInput.DetailList(rowIndex).Kingaku, _
                          sotoZeigaku, _
                          uchiZeigaku, _
                          _BEKihonJoho.ZeiHasuKurai)

    peKonInput.DetailList(rowIndex).SotoZeigaku = sotoZeigaku
    peKonInput.DetailList(rowIndex).UchiZeigaku = uchiZeigaku

    If _DenpyoType = peKonInput.DenpyoType.Mitsumori _
    OrElse _DenpyoType = peKonInput.DenpyoType.Juchu _
    OrElse _DenpyoType = peKonInput.DenpyoType.Uriage Then
      '粗利益を計算
      peKonInput.DetailList(rowIndex).Ararieki = _
          KonCalc.CalcAraRieki(peKonInput.DetailList(rowIndex).Genka, _
                               peKonInput.DetailList(rowIndex).Kingaku, _
                               peKonInput.DetailList(rowIndex).UchiZeigaku, _
                               peKonInput.DetailList(rowIndex).MasterKubun, _
                               peKonInput.DetailList(rowIndex).ZeiRitsu, _
                               peKonInput.Header.SyohizeiTsuchiKon, _
                               peKonInput.DetailList(rowIndex).ZeikomiKubun, _
                               _BEKihonJoho.ZeiHasuKurai, _
                               peKonInput.Header.SyohizeiHasu)

      If peKonInput.DetailList(rowIndex).MasterKubun = BusinessEntity.Defines.MasterKubunType.Ippan _
       Or peKonInput.DetailList(rowIndex).MasterKubun = BusinessEntity.Defines.MasterKubunType.Zatsuhin Then
        '一般商品、雑商品の時は利益率を計算
        peKonInput.DetailList(rowIndex).RiekiRitsu = _
            KonCalc.CalcRiekiRitsu(peKonInput.DetailList(rowIndex).Ararieki, _
                                   peKonInput.DetailList(rowIndex).Kingaku, _
                                   peKonInput.DetailList(rowIndex).UchiZeigaku)

      End If
    End If

    If checkGenkaware Then
      '原価割れをチェック
      headerLabelText = Me.CheckGenkaware(peKonInput.DetailList(rowIndex))
    End If

    'If checkZaikoware Then
    '  '在庫割れをチェック
    '  headerLabelText = Me.CheckZaikoware(peKonInput.DetailList(rowIndex))
    'End If

    If calculateTotal Then
      '合計を計算
      Me.CalcTotal(peKonInput, needMessageBox, _DenpyoType)
    End If

  End Sub

  ''' <summary>粗利益変更により金額（単価）、消費税、利益率を計算。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="rowIndex">行インデックス</param>
  ''' <param name="calculateTotal">合計を計算するかどうか</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="headerLabelText">ヘッダーラベルに表示するメッセージ</param>
  Public Function CalcMeisaiByArarieki(ByRef peKonInput As PEKonInput, ByVal rowIndex As Integer, ByVal calculateTotal As Boolean, ByVal needMessageBox As Boolean, ByRef headerLabelText As String) As Boolean
    '
    '変更前の項目値を退避
    Dim oldArarieki As Decimal = peKonInput.DetailList(rowIndex).Ararieki
    Dim oldTanka As Decimal = peKonInput.DetailList(rowIndex).Tanka
    Dim oldKingaku As Decimal = peKonInput.DetailList(rowIndex).Kingaku
    Dim oldGenka As Decimal = peKonInput.DetailList(rowIndex).Genka

    '粗利益を仮金額に設定
    Dim curKingaku As Decimal = peKonInput.DetailList(rowIndex).Ararieki

    If peKonInput.DetailList(rowIndex).ZeiRitsu <> 0 _
    AndAlso peKonInput.DetailList(rowIndex).ZeikomiKubun = PCA.TSC.Kon.BusinessEntity.Defines.ZeikomiKubnType.Zeikomi Then
      '税込時は粗利益を税抜に変換
      curKingaku = _
          KonCalc.CalcNukiKingakuToKomi(curKingaku, peKonInput.DetailList(rowIndex).ZeiRitsu, Me.BEKihonJoho.ZeiHasuKurai, peKonInput.Header.SyohizeiHasu)
    End If

    '税抜粗利益に原価を加算し仮金額を計算
    curKingaku = curKingaku + peKonInput.DetailList(rowIndex).Genka

    '金額と数量の符号をチェック
    If Me.CheckFugoKingakuAndSuryo(curKingaku, peKonInput.DetailList(rowIndex).Suryo, needMessageBox) = False Then
      Return False
    End If

    If peKonInput.DetailList(rowIndex).Tanka = 0D Then
      '金額入力時
      If curKingaku > InputDefines.MaximumKingaku OrElse curKingaku < InputDefines.MinimumKingaku Then
        '金額が範囲外の時
        peKonInput.DetailList(rowIndex).Ararieki = oldArarieki
        peKonInput.DetailList(rowIndex).Tanka = oldTanka
        peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
        peKonInput.DetailList(rowIndex).Genka = oldGenka
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(MessageDefines.Exclamation.Overflow, BusinessEntity.ItemText.Kingaku), _
                                    CommonVariable.BasicConfig.ProgramName)
        End If
        Return False
      Else
        '金額が範囲内の時　仮金額を伝票入力エンティティの金額に格納
        peKonInput.DetailList(rowIndex).Kingaku = curKingaku
      End If
    Else
      '単価入力時　単価→金額、原価の順で再計算
      peKonInput.DetailList(rowIndex).Tanka = KonCalc.CalcTanka(curKingaku, peKonInput.DetailList(rowIndex).Suryo, PCA.TSC.Kon.BusinessEntity.Defines.HasuType.ShisyaGonyu, peKonInput.DetailList(rowIndex).TankaKeta)
      '単価桁数、単価小数桁をチェック
      If KonCalc.CheckTankaKeta(peKonInput.DetailList(rowIndex).Tanka, peKonInput.DetailList(rowIndex).TankaKeta) = False Then
        'エラー時
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(MessageDefines.Exclamation.Overflow, BusinessEntity.ItemText.Tanka), _
                                    CommonVariable.BasicConfig.ProgramName)
        End If
        peKonInput.DetailList(rowIndex).Ararieki = oldArarieki
        peKonInput.DetailList(rowIndex).Tanka = oldTanka
        peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
        peKonInput.DetailList(rowIndex).Genka = oldGenka
        Return False
      End If

      '金額、原価を計算
      If Me.CalcKingakuAndGenka(peKonInput.DetailList(rowIndex), peKonInput.Header.KingakuHasu, InputDefines.ChangedItem.None, needMessageBox) = False Then
        'エラー時
        peKonInput.DetailList(rowIndex).Ararieki = oldArarieki
        peKonInput.DetailList(rowIndex).Tanka = oldTanka
        peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
        peKonInput.DetailList(rowIndex).Genka = oldGenka
        Return False
      End If
    End If

    '消費税、粗利益、利益率、合計を原価割れチェック付きで計算
    Me.CalcNotKingakuItems(peKonInput, rowIndex, True, False, True, needMessageBox, headerLabelText)

    Return True

  End Function

  ''' <summary>利益率変更により金額（単価）、消費税、粗利益、利益率を計算。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="rowIndex">行インデックス</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="headerLabelText">ヘッダーラベルに表示するメッセージ</param>
  ''' <returns>整合性等のチェックにひっかからずに計算が完了したときTrue</returns>
  Public Function CalcMeisaiByRiekiRitsu(ByRef peKonInput As PEKonInput, ByVal rowIndex As Integer, ByVal needMessageBox As Boolean, ByRef headerLabelText As String) As Boolean
    '
    '変更前の項目値を退避
    Dim oldArarieki As Decimal = peKonInput.DetailList(rowIndex).Ararieki
    Dim oldTanka As Decimal = peKonInput.DetailList(rowIndex).Tanka
    Dim oldKingaku As Decimal = peKonInput.DetailList(rowIndex).Kingaku
    Dim oldGenka As Decimal = peKonInput.DetailList(rowIndex).Genka

    '原価を仮金額に設定
    Dim curKingaku As Decimal = peKonInput.DetailList(rowIndex).Genka

    If peKonInput.DetailList(rowIndex).ZeiRitsu <> 0D _
    And peKonInput.DetailList(rowIndex).ZeikomiKubun = PCA.TSC.Kon.BusinessEntity.Defines.ZeikomiKubnType.Zeikomi Then
      '税込時は原価を税抜に変換
      curKingaku =
          KonCalc.CalcKomiKingakuToNuki(curKingaku, peKonInput.DetailList(rowIndex).ZeiRitsu, _BEKihonJoho.ZeiHasuKurai, peKonInput.Header.SyohizeiHasu)
    End If

    '原価÷（100%－利益率）×100%で仮金額を計算
    curKingaku = curKingaku / (100D - peKonInput.DetailList(rowIndex).RiekiRitsu) * 100D

    If peKonInput.DetailList(rowIndex).ZeiRitsu <> 0D _
    AndAlso peKonInput.DetailList(rowIndex).ZeikomiKubun <> PCA.TSC.Kon.BusinessEntity.Defines.ZeikomiKubnType.Zeikomi Then
      '税込の場合は税込に戻す
      curKingaku = KonCalc.CalcNukiKingakuToKomi(curKingaku, peKonInput.DetailList(rowIndex).ZeiRitsu, _BEKihonJoho.ZeiHasuKurai, peKonInput.Header.SyohizeiHasu)
    End If

    If peKonInput.DetailList(rowIndex).Tanka = 0D Then
      '金額入力時、仮金額を端数処理
      curKingaku = KonCalc.Round(curKingaku, 0, PCA.TSC.Kon.BusinessEntity.Defines.HasuType.ShisyaGonyu)
      If curKingaku > InputDefines.MaximumKingaku Or curKingaku < InputDefines.MinimumKingaku Then
        '金額が範囲外の時
        peKonInput.DetailList(rowIndex).Ararieki = oldArarieki
        peKonInput.DetailList(rowIndex).Tanka = oldTanka
        peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
        peKonInput.DetailList(rowIndex).Genka = oldGenka
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, String.Format(Tools.MessageDefines.Exclamation.Overflow, ItemText.Kingaku), CommonVariable.BasicConfig.ProgramName)
        End If
        Return False
      Else
        '金額が範囲内の時　仮金額を伝票入力エンティティの金額に格納
        peKonInput.DetailList(rowIndex).Kingaku = curKingaku
      End If
    Else
      '単価入力時　単価→金額、原価の順で再計算
      peKonInput.DetailList(rowIndex).Tanka = _
          KonCalc.CalcTanka(curKingaku, peKonInput.DetailList(rowIndex).Suryo, PCA.TSC.Kon.BusinessEntity.Defines.HasuType.ShisyaGonyu, peKonInput.DetailList(rowIndex).TankaKeta)

      '単価桁数、単価小数桁をチェック
      If KonCalc.CheckTankaKeta(peKonInput.DetailList(rowIndex).Tanka, peKonInput.DetailList(rowIndex).TankaKeta) = False Then
        'エラー時
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, String.Format(Tools.MessageDefines.Exclamation.Overflow, ItemText.Tanka), CommonVariable.BasicConfig.ProgramName)
        End If
        peKonInput.DetailList(rowIndex).Ararieki = oldArarieki
        peKonInput.DetailList(rowIndex).Tanka = oldTanka
        peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
        peKonInput.DetailList(rowIndex).Genka = oldGenka
        Return False
      End If

      '金額計算
      If Me.CalcKingakuAndGenka(peKonInput.DetailList(rowIndex), peKonInput.Header.KingakuHasu, InputDefines.ChangedItem.None, needMessageBox) = False Then
        'エラー時
        peKonInput.DetailList(rowIndex).Ararieki = oldArarieki
        peKonInput.DetailList(rowIndex).Tanka = oldTanka
        peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
        peKonInput.DetailList(rowIndex).Genka = oldGenka
        Return False
      End If
    End If

    '金額と数量の符号をチェック
    If Me.CheckFugoKingakuAndSuryo(peKonInput.DetailList(rowIndex).Kingaku, peKonInput.DetailList(rowIndex).Suryo, needMessageBox) = False Then
      'エラー時
      peKonInput.DetailList(rowIndex).Ararieki = oldArarieki
      peKonInput.DetailList(rowIndex).Tanka = oldTanka
      peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
      peKonInput.DetailList(rowIndex).Genka = oldGenka
      Return False
    End If

    '金額と原価の符号をチェック
    If Me.CheckFugoKingakuAndGenka(peKonInput.DetailList(rowIndex).Kingaku, peKonInput.DetailList(rowIndex).Genka, needMessageBox) = False Then
      'エラー時
      peKonInput.DetailList(rowIndex).Ararieki = oldArarieki
      peKonInput.DetailList(rowIndex).Tanka = oldTanka
      peKonInput.DetailList(rowIndex).Kingaku = oldKingaku
      peKonInput.DetailList(rowIndex).Genka = oldGenka
      Return False
    End If

    '消費税、粗利益、利益率、合計を原価割れチェック付きで計算
    Me.CalcNotKingakuItems(peKonInput, rowIndex, True, False, True, needMessageBox, headerLabelText)

    Return True

  End Function

  ''' <summary>全明細と合計を再計算。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Sub ReCalcDenpyo(ByRef peKonInput As PEKonInput, ByVal needMessageBox As Boolean)
    '
    If String.IsNullOrEmpty(peKonInput.Header.TorihikisakiCode) Then
      '得意（仕入）先が未入力時は対象外
      Return
    End If

    '全明細を再計算
    For index As Integer = 0 To peKonInput.DetailList.Count - 1
      If KonCalc.JudgeDetailState(_DenpyoType, peKonInput.DetailList(index)) = Tools.PEKonInput.MeisaiStateType.Valid Then
        If peKonInput.DetailList(index).Tanka <> 0 Then
          '単価がゼロ以外の時は再計算（単価ゼロ時は金額入力）
          If Me.CalcKingakuAndGenka(peKonInput.DetailList(index), peKonInput.Header.KingakuHasu, InputDefines.ChangedItem.Tanka, needMessageBox) = False Then
            peKonInput.DetailList(index).Kingaku = 0
            peKonInput.DetailList(index).Genka = 0
          End If
        End If
        If peKonInput.DetailList(index).BaiTanka <> 0 Then
          If Me.CalcBaikaKingaku(peKonInput.DetailList(index), peKonInput.Header.KingakuHasu, InputDefines.ChangedItem.BaiTanka, needMessageBox) = False Then
            peKonInput.DetailList(index).BaikaKingaku = 0
          End If
        End If
        Dim headerLabelText As String = String.Empty
        '原価割れ・在庫割れチェック、合計計算を行わず消費税、粗利益、利益率を再計算
        Me.CalcNotKingakuItems(peKonInput, index, False, False, False, needMessageBox, headerLabelText)

      End If
    Next

    '合計を再計算
    Me.CalcTotal(peKonInput, needMessageBox, _DenpyoType)

  End Sub


  ''' <summary>
  ''' 合計金額を計算します。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="fixedRate">固定で残す税率</param>
  ''' <remarks></remarks>
  Public Sub CalcTotal(peKonInput As PEKonInput, needMessageBox As Boolean, fixedRate As Decimal, denpyoType As Tools.PEKonInput.DenpyoType)
    '
    KonCalc.CalcTotal(denpyoType, peKonInput, ZeiRitsuList, _BEKihonJoho.ZeiHasuKurai, fixedRate)

    If needMessageBox Then
      If peKonInput.Total.KingakuGokei + peKonInput.Total.SotoZeiGokei > InputDefines.MaximumKingaku _
      OrElse peKonInput.Total.KingakuGokei + peKonInput.Total.SotoZeiGokei < InputDefines.MinimumKingaku Then
        ShowMessage.OnExclamation(m_Owner, String.Format(Tools.MessageDefines.Exclamation.Overflow, "合計金額"), CommonVariable.BasicConfig.ProgramName)
        Return
      End If

      For i As Integer = 0 To peKonInput.SubtotalList.Count - 1
        If peKonInput.SubtotalList(i).KingakuGokei + peKonInput.SubtotalList(i).SotoZeiGokei > InputDefines.MaximumKingaku _
        Or peKonInput.SubtotalList(i).KingakuGokei + peKonInput.SubtotalList(i).SotoZeiGokei < InputDefines.MinimumKingaku Then
          ShowMessage.OnExclamation(m_Owner, String.Format(Tools.MessageDefines.Exclamation.Overflow, "合計金額"), CommonVariable.BasicConfig.ProgramName)
          Exit For
        End If
      Next
    End If
  End Sub

  ''' <summary>
  ''' 合計金額を計算します。
  ''' </summary>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="denpyoType">伝票種類</param>
  ''' <remarks></remarks>
  Public Sub CalcTotal(peKonInput As PEKonInput, needMessageBox As Boolean, denpyoType As Tools.PEKonInput.DenpyoType)
    CalcTotal(peKonInput, needMessageBox, -1, denpyoType)
  End Sub

  ''' <summary>売価金額を計算。
  ''' </summary>
  ''' <param name="detail">明細データ</param>
  ''' <param name="kingakuHasu">金額端数</param>
  ''' <param name="changedItem">売価計算を行う原因となった変更された項目</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function CalcBaikaKingaku(ByRef detail As PEKonInputDetail, kingakuHasu As BusinessEntity.Defines.HasuType, changedItem As InputDefines.ChangedItem, needMessageBox As Boolean) As Boolean
    '
    Dim curBaikaKingaku As Decimal = 0D

    Select Case changedItem
      Case InputDefines.ChangedItem.Gentanka, _
           InputDefines.ChangedItem.Genka, _
           InputDefines.ChangedItem.Kingaku, _
           InputDefines.ChangedItem.BaikaKingaku
        '原単価、原価、金額、売価金額の変更時は計算対象外
        Return True
    End Select

    If detail.SyokonKeisan <> 0 OrElse detail.BaiTanka <> 0 Then
      '計算式未使用で単価ゼロを除き、金額を計算
      If KonCalc.CalcKingaku(detail.Suryo, detail.BaiTanka, kingakuHasu, curBaikaKingaku) = False Then
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(Tools.MessageDefines.Exclamation.Overflow, BusinessEntity.ItemText.BaikaKingaku), _
                                    CommonVariable.BasicConfig.ProgramName)
        End If
        Return False
      End If
    Else
      curBaikaKingaku = detail.BaikaKingaku
    End If

    detail.BaikaKingaku = curBaikaKingaku

    Return True

  End Function

  ''' <summary>金額、原価を計算。
  ''' </summary>
  ''' <param name="detail">伝票入力エンティティ明細</param>
  ''' <param name="kingakuHasu">金額端数</param>
  ''' <param name="changedItem">変更元項目</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function CalcKingakuAndGenka(detail As PEKonInputDetail, kingakuHasu As PCA.TSC.Kon.BusinessEntity.Defines.HasuType, changedItem As InputDefines.ChangedItem, needMessageBox As Boolean) As Boolean
    '
    Dim curKingaku As Decimal = 0D
    Dim curGenka As Decimal = 0D

    If changedItem = InputDefines.ChangedItem.BaiTanka Then
      '売単価の変更時は計算対象外
      Return True
    End If
    If changedItem = InputDefines.ChangedItem.Genka Then
      '原価の変更時は金額との符号チェック
      Return CheckFugoKingakuAndGenka(detail.Kingaku, detail.Genka, needMessageBox)
    End If

    If changedItem = InputDefines.ChangedItem.Kingaku Then
      '金額の変更時
      If detail.Ku <> PCA.TSC.Kon.BusinessEntity.Defines.KuType.Tankateisei Then
        '単価訂正以外は数量との符号チェック
        If CheckFugoKingakuAndSuryo(detail.Kingaku, detail.Suryo, needMessageBox) = False Then
          Return False
        End If
      End If

      '原価の符号を一致させる
      If detail.Kingaku > 0D AndAlso detail.Genka < 0D Then
        detail.Genka = detail.Genka * -1D
      ElseIf detail.Kingaku < 0D AndAlso detail.Genka > 0D Then
        detail.Genka = detail.Genka * -1D
      End If
      Return True

    End If

    '以下、入数、箱数、数量、単価、原単価、売価の変更時

    If detail.SyokonKeisan <> 0 OrElse detail.Tanka <> 0 Then
      '計算式未使用で単価ゼロを除き、金額を計算
      If KonCalc.CalcKingaku(detail.Suryo, detail.Tanka, kingakuHasu, curKingaku) = False Then
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(Tools.MessageDefines.Exclamation.Overflow, BusinessEntity.ItemText.Kingaku), _
                                    CommonVariable.BasicConfig.ProgramName)
        End If
        Return False
      End If
    Else
      curKingaku = detail.Kingaku
    End If

    If detail.Ku <> PCA.TSC.Kon.BusinessEntity.Defines.KuType.Tankateisei Then
      '単価訂正以外は数量との符号チェック
      If CheckFugoKingakuAndSuryo(curKingaku, detail.Suryo, needMessageBox) = False Then
        Return False
      End If
    End If

    If detail.SyokonKeisan <> 0 OrElse detail.GenTanka <> 0 Then
      '計算式未使用で原単価ゼロを除き、原価を計算
      If KonCalc.CalcKingaku(detail.Suryo, detail.GenTanka, detail.GenkaHasu, curGenka) = False Then
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(Tools.MessageDefines.Exclamation.Overflow, BusinessEntity.ItemText.Genka), _
                                    CommonVariable.BasicConfig.ProgramName)
        End If
        Return False
      End If
    Else
      curGenka = detail.Genka
    End If

    '金額と原価の符号チェック
    If CheckFugoKingakuAndGenka(curKingaku, curGenka, needMessageBox) = False Then
      Return False
    End If

    '金額、原価共に桁数超過がない時に伝票入力エンティティ明細に格納
    detail.Kingaku = curKingaku
    detail.Genka = curGenka

    Return True

  End Function

  ''' <summary>金額と原価の符号の整合性をチェック。
  ''' </summary>
  ''' <param name="kingaku">金額</param>
  ''' <param name="genka">原価</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function CheckFugoKingakuAndGenka(ByVal kingaku As Decimal, ByVal genka As Decimal, ByVal needMessageBox As Boolean) As Boolean
    '
    If (kingaku < 0D AndAlso genka > 0D) OrElse (kingaku > 0D AndAlso genka < 0D) Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.NotEqualSign, BusinessEntity.ItemText.Kingaku, BusinessEntity.ItemText.Genka), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    Return True

  End Function

  ''' <summary>金額と数量の符号の整合性をチェック。
  ''' </summary>
  ''' <param name="kingaku">金額</param>
  ''' <param name="suryo">数量</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function CheckFugoKingakuAndSuryo(ByVal kingaku As Decimal, ByVal suryo As Decimal, ByVal needMessageBox As Boolean) As Boolean
    '
    If (kingaku < 0D AndAlso suryo > 0D) OrElse (kingaku > 0D AndAlso suryo < 0D) Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.NotEqualSign, BusinessEntity.ItemText.Suryo, BusinessEntity.ItemText.Kingaku), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    Return True
  End Function

  ''' <summary>原価割れをチェックし、不足の時は警告文言を返す。
  ''' （補足）原価割れチェックをするかどうかの判定は今後対応予定。
  ''' </summary>
  ''' <param name="detail">伝票入力エンティティ明細</param>
  ''' <returns></returns>
  Public Function CheckGenkaware(ByVal detail As PEKonInputDetail) As String
    '
    If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kan Then
      '見積、受注、売上伝票はチェックしない
      Return String.Empty
    End If

    If (detail.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan AndAlso detail.Ku <> PCA.TSC.Kon.BusinessEntity.Defines.KuType.Tankateisei) _
    OrElse detail.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Zatsuhin Then
      '一般商品の売上と返品、雑商品
      If Math.Abs(detail.Kingaku) < Math.Abs(detail.Genka) Then
        Return Tools.MessageDefines.HeaderLabel.Genkaware
      End If
    End If

    Return String.Empty

  End Function


  ''' <summary>在庫ソート項目を比較（第１キー＝商品コード、第２キー＝倉庫コード）
  ''' （補足）戻り値は、ゼロ未満時AがBより小さい／ゼロの時AとBが等しい／ゼロより大きい時AがBより大きい
  ''' </summary>
  ''' <param name="zaikoA">在庫の照会A</param>
  ''' <param name="zaikoB">在庫の照会B</param>
  Protected Function CompareZaikoItems(ByVal zaikoA As BEZaikosuNoSyokai, ByVal zaikoB As BEZaikosuNoSyokai) As Integer
    '
    '第１キー(商品コード)で比較
    Dim compareResult As Integer = TscStringTool.Compare(zaikoA.SyohinCode, zaikoB.SyohinCode, m_JapaneseCollation)

    If compareResult = 0 Then
      '同一キーだった場合、第２キー(倉庫コード)で比較
      Return TscStringTool.Compare(zaikoA.SokoCode, zaikoB.SokoCode, m_JapaneseCollation)
    Else
      Return compareResult
    End If

  End Function

#End Region

  ''' <summary>
  ''' 受注伝票を検索します。
  ''' </summary>
  ''' <param name="juchuNo">受注伝票No</param>
  ''' <returns>受注伝票</returns>
  ''' <remarks></remarks>
  Public Function FindInputJUC(ByVal juchuNo As Integer, ByVal needMessageBox As Boolean) As BEInputJUC
    '
    Dim inputJUCCondition As New InputJUCCondition
    Dim beInputJUC As New BEInputJUC
    Dim beInputJUCList As New List(Of BEInputJUC)

    Try
      inputJUCCondition.JuchuNo = juchuNo
      beInputJUCList = MyBase.FindByConditionBEInputJUCList(inputJUCCondition)
      If beInputJUCList.Count = 0 Then
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(MessageDefines.Exclamation.DenpyoNotExist, BusinessEntity.ItemText.Juchu), _
                                    CommonVariable.BasicConfig.ProgramName)
          Return beInputJUC
        End If
      End If

      Return beInputJUCList(0)

    Catch ex As Exception
      Throw

    End Try

  End Function

#Region "在庫数の照会"

  ''' <summary>
  ''' 在庫数の照会を取得します
  ''' </summary>
  ''' <param name="SyohinCode">商品コード</param>
  ''' <param name="SokoCode">倉庫コード</param>
  ''' <returns>取得した「在庫数の照会」データ</returns>
  ''' <remarks></remarks>
  Public Function CalculateZaikosuNoSyokai(ByVal syohinCode As String, ByVal sokoCode As String) As BEZaikosuNoSyokai
    '
    Dim zaikosuNoSyokaiCondition As New ZaikosuNoSyokaiCondition
    Dim result As New ResultCalculateOutputZaikosuNoSyokai
    Dim beZaikosuNoSyokai As New BEZaikosuNoSyokai

    '商品コードと倉庫コードを指定
    '在庫管理しない場合は倉庫コードは無視される
    Try
      '
      zaikosuNoSyokaiCondition.SelectedSyohinKubun = 0
      zaikosuNoSyokaiCondition.SyohinCode = syohinCode.Trim()
      zaikosuNoSyokaiCondition.SokoCode = sokoCode.Trim()

      result = Tools.KonAPI.CalculateOutputZaikosuNoSyokai(CommonVariable.IApplication, zaikosuNoSyokaiCondition)

      If result.Status = IntegratedStatus.Success _
      AndAlso result.ArrayOfBEZaikosuNoSyokai.BEZaikosuNoSyokai.Count > 0 Then
        beZaikosuNoSyokai = result.ArrayOfBEZaikosuNoSyokai.BEZaikosuNoSyokai(0)
      End If

      If beZaikosuNoSyokai.SyohinCode.Length = 0 Then
        '該当データが取得不可の時は、現在個数ゼロのまま商品コード、倉庫コード・名を補填
        Dim beMasterSoko As BEMasterSoko = MyBase.FindBEMasterSoko(sokoCode.Trim())
        beZaikosuNoSyokai.SyohinCode = syohinCode
        beZaikosuNoSyokai.SokoCode = sokoCode
        beZaikosuNoSyokai.SokoMei = beMasterSoko.SokoMei
      End If

      Return beZaikosuNoSyokai

    Catch ex As Exception
      Throw

    End Try

  End Function

#End Region

#Region "マスター参照画面、検証"

  ''' <summary>参照画面の共通変数
  ''' </summary>
  Public Property ReferDialogCommonVariable As New ReferDialogCommonVariable

  ''' <summary>参照画面為の表示項目リストプロパティを一括で格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferItemsList() As Boolean
    '
    _ReferDialogCommonVariable.BEKihonJoho = _BEKihonJoho
    'システム区分
    _ReferDialogCommonVariable.SystemKubunUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem
    _ReferDialogCommonVariable.SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kan
    '_ReferDialogCommonVariable.SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon
    '使用区分
    _ReferDialogCommonVariable.ShiyoKubunUsing = PCA.TSC.Kon.BusinessEntity.Defines.UsingType.UsingOneItem

    If ReferSms.NeedLoad = True Then    '商品
      If LoadReferSms() = False Then
        Return False
      End If
    End If
    If ReferSoko.NeedLoad = True Then    '倉庫
      If LoadReferSoko() = False Then
        Return False
      End If
    End If
    If ReferBumon.NeedLoad = True Then    '部門
      If LoadReferBumon() = False Then
        Return False
      End If
    End If
    If ReferTantosya.NeedLoad = True Then    '担当者
      If LoadReferTantosya() = False Then
        Return False
      End If
    End If
    If ReferProject.NeedLoad = True Then    'プロジェクト
      If LoadReferProject() = False Then
        Return False
      End If
    End If
    If ReferTekiyo.NeedLoad = True Then    '売上仕入等の摘要
      If LoadReferTekiyo() = False Then
        Return False
      End If
    End If
    'If ReferTms.NeedLoad = True Then    '得意先
    '  If LoadReferTms() = False Then
    '    Return False
    '  End If
    'End If
    If ReferRms.NeedLoad = True Then    '得意先
      If LoadReferRms() = False Then
        Return False
      End If
    End If
    If ReferYms.NeedLoad = True Then    '直送先
      If LoadReferYms() = False Then
        Return False
      End If
    End If

    Return True

  End Function

  ''' <summary>参照画面の表示開始位置を設定
  ''' </summary>
  ''' <param name="location">表示開始位置</param>
  ''' <param name="referDialog">参照画面</param>
  Public Sub LocateReferDialog(ByVal location As Point, ByRef referDialog As TscReferDialog)
    '
    If location.IsEmpty = False Then
      '表示開始位置は任意設定
      referDialog.StartPosition = FormStartPosition.Manual
      '表示開始位置
      referDialog.Location = location
      '画面起動時に開始位置調整を行うかどうか
      referDialog.AutoStartPosition = False
      '画面起動時に画面内に収まるように表示位置調整を行うかどうか
      referDialog.AutoLocationAdjust = True
    End If

  End Sub

  ''' <summary>参照画面の表示開始位置を設定(ロット詳細設定)
  ''' </summary>
  ''' <param name="location">表示開始位置</param>
  ''' <param name="referDialog">参照画面</param>
  Public Sub LocateReferLmdDialog(ByVal location As Point, ByRef referDialog As Sunloft.SearchEx.LmdReferDialog)
    '
    If location.IsEmpty = False Then
      '表示開始位置は任意設定
      referDialog.StartPosition = FormStartPosition.Manual
      '表示開始位置
      referDialog.Location = location
      '画面起動時に開始位置調整を行うかどうか
      referDialog.AutoStartPosition = False
      '画面起動時に画面内に収まるように表示位置調整を行うかどうか
      referDialog.AutoLocationAdjust = True
    End If

  End Sub

#Region "   商品"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferSms As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferSmsTool As ReferSmsDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferSmsList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferSms() As Boolean
    '
    Dim masterSmsTool As New MasterSmsTool
    m_ReferSmsTool = New ReferSmsDialogTool(_ReferDialogCommonVariable)

    If MasterSms.Loaded = True Then
      If _ReferSms.Loaded = True Then
        'ロード済の時はクリア
        ReferSmsList.Clear()
        _ReferSms.Loaded = False
      End If
      '取得済のマスターリストから
      ReferSmsList = m_ReferSmsTool.SettingReferItemsList(BEMasterSmsList)
    Else
      ReferSmsList = m_ReferSmsTool.LoadReferItemsList(CommonVariable.IApplication, masterSmsTool)
    End If

    _ReferSms.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferSmsDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point, ByVal connector As IIntegratedApplication) As DialogResult
    '
    Dim masterSmsTool As New MasterSmsTool
    Dim dialogResult As DialogResult

    CommonVariable.IApplication = connector
    If _ReferSms.Loaded = False Then
      m_ReferSmsTool = New ReferSmsDialogTool(_ReferDialogCommonVariable)
      ReferSmsList = m_ReferSmsTool.LoadReferItemsList(CommonVariable.IApplication, masterSmsTool)
    End If

    Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferSmsTool.Setting, ReferSmsList, CommonVariable.IApplication)
    LocateReferDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty

    referDialog.IApplication = CommonVariable.IApplication
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog()
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="rowIndex">行インデックス</param>
  ''' <param name="newcode">商品コード</param>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="headerLabelText">ヘッダーラベル表示メッセージ</param>
  Public Function ValidatingSyohin(ByVal rowIndex As Integer, _
                                   ByVal newCode As String, _
                                   ByRef peKonInput As PEKonInput, _
                                   ByVal needMessageBox As Boolean, _
                                   ByRef headerLabelText As String) As Boolean
    '
    Dim torihikisakiText As String = String.Empty
    Dim rendoText As String = String.Empty
    Dim denpyoText As String = String.Empty
    Dim senyoText As String = String.Empty
    If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
      torihikisakiText = BusinessEntity.ItemText.Tokuisaki
      rendoText = BusinessEntity.ItemText.Juchu
      denpyoText = BusinessEntity.ItemText.Uriage
      senyoText = BusinessEntity.ItemText.Shiire
    Else
      torihikisakiText = BusinessEntity.ItemText.Shiresaki
      rendoText = BusinessEntity.ItemText.Hachu
      denpyoText = BusinessEntity.ItemText.Shiire
      senyoText = BusinessEntity.ItemText.Uriage
    End If

    Dim rendoSiji As String = String.Empty
    Dim rendoHeaderId As Integer = 0
    Dim rendoSequence As Integer = 0

    If String.IsNullOrEmpty(newCode) = True Then
      If String.IsNullOrEmpty(peKonInput.DetailList(rowIndex).SyohinCode) = True Then
        '未入力→未入力時
        Return True
      Else
        '入力済→未入力時、商品コードは必須入力
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    Tools.MessageDefines.Exclamation.IncorrectValue, _
                                    CommonVariable.BasicConfig.ProgramName)
        End If
        Return False
      End If
    End If

    If String.IsNullOrEmpty(peKonInput.Header.TorihikisakiCode) = True Then
      '得意（仕入）先が未設定
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.NotInput, torihikisakiText), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    '商品マスターを取得
    Dim beMasterSms As BEMasterSms = MyBase.FindBEMasterSms(newCode)
    If beMasterSms.SyohinCode.Length = 0 Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Syohin), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    '専用商品をチェック
    If (_SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon And beMasterSms.SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.Kan) _
    OrElse (_SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kan And beMasterSms.SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterSmsSystemKubunType.Kon) Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.DifferentMasterKubun, senyoText), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    '使用禁止または有効期間外をチェック
    If beMasterSms.ShiyoKubun = PCA.TSC.Kon.BusinessEntity.Defines.ShiyoKubunType.Kinshi _
    OrElse KonCalc.CheckSyohinYukoKikan(beMasterSms, peKonInput.Header.DenpyoHizuke) = False Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.UnusableOrExpired, BusinessEntity.ItemText.Syohin), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    '受／発注からの連動明細の時、変更前後をチェック
    If peKonInput.DetailList(rowIndex).RendoHeaderId <> 0 Then
      If peKonInput.DetailList(rowIndex).MasterKubun <> beMasterSms.MasterKubun Then
        'マスター区分が不一致
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(Tools.MessageDefines.Exclamation.CantChangeMasterKubun, rendoText, denpyoText), _
                                    CommonVariable.BasicConfig.ProgramName)
        End If
        Return False
      End If
      If peKonInput.DetailList(rowIndex).SuryoKeta <> beMasterSms.SuryoKeta Then
        '数量小数桁が不一致
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, _
                                    String.Format(Tools.MessageDefines.Exclamation.CantChangeSuryoKeta, rendoText, denpyoText), _
                                    CommonVariable.BasicConfig.ProgramName)
        End If
        Return False
      End If
      '連動項目を退避
      rendoSiji = peKonInput.DetailList(rowIndex).RendoShiji
      rendoHeaderId = peKonInput.DetailList(rowIndex).RendoHeaderId
      rendoSequence = peKonInput.DetailList(rowIndex).RendoSequence
    End If

    '明細SEQを退避
    Dim sequence As Integer = peKonInput.DetailList(rowIndex).Sequence
    '入金／支払済フラグを退避
    Dim kinzumiFlag As PCA.TSC.Kon.BusinessEntity.Defines.KinzumiFlagType = peKonInput.DetailList(rowIndex).KinzumiFlag
    '伝票入力エンティティ明細を初期化
    peKonInput.DetailList(rowIndex) = New PEKonInputDetail()

    If rendoHeaderId <> 0 Then
      '連動項目を戻す
      peKonInput.DetailList(rowIndex).RendoShiji = rendoSiji
      peKonInput.DetailList(rowIndex).RendoHeaderId = rendoHeaderId
      peKonInput.DetailList(rowIndex).RendoSequence = rendoSequence
    End If

    '商品項目を伝票入力エンティティに設定
    peKonInput.DetailList(rowIndex).Sequence = sequence                                  '明細SEQ
    peKonInput.DetailList(rowIndex).SyohinCode = beMasterSms.SyohinCode                  '商品コード
    peKonInput.DetailList(rowIndex).MasterKubun = beMasterSms.MasterKubun                'マスター区分
    peKonInput.DetailList(rowIndex).ZeiKubun = beMasterSms.ZeiKubun                      '税区分
    peKonInput.DetailList(rowIndex).ZeikomiKubun = beMasterSms.ZeikomiKubun              '税込区分
    peKonInput.DetailList(rowIndex).TankaKeta = beMasterSms.TankaKeta                    '単価小数桁
    peKonInput.DetailList(rowIndex).SyohinMei = beMasterSms.SyohinMei                    '品名
    peKonInput.DetailList(rowIndex).KikakuKataban = beMasterSms.KikakuKataban            '規格・型番
    peKonInput.DetailList(rowIndex).Color = beMasterSms.Color                            '色
    peKonInput.DetailList(rowIndex).Size = beMasterSms.Size                              'サイズ
    peKonInput.DetailList(rowIndex).Irisu = beMasterSms.Irisu                            '入数     
    peKonInput.DetailList(rowIndex).Tani = beMasterSms.Tani                              '単位
    '税率
    peKonInput.DetailList(rowIndex).ZeiRitsu = KonCalc.SearchZeiRitsu(Me.TaxList, peKonInput.Header.DenpyoHizuke, beMasterSms.ZeiKubun)
    peKonInput.DetailList(rowIndex).IrisuKeta = beMasterSms.IrisuKeta                    ' 入数小数桁
    peKonInput.DetailList(rowIndex).HakosuKeta = beMasterSms.HakosuKeta                  ' 箱数小数桁
    peKonInput.DetailList(rowIndex).SuryoHasu = beMasterSms.SuryoHasu                    ' 数量端数
    peKonInput.DetailList(rowIndex).GenkaHasu = peKonInput.Header.GenkaHasu             ' 原価端数
    peKonInput.DetailList(rowIndex).SuryoKeta = beMasterSms.SuryoKeta                    ' 数量小数桁

    '倉庫
    Dim isChangeSoko As Boolean = True
    If beMasterSms.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan _
    AndAlso Me.BEKihonJoho.ZaikoKanri = PCA.TSC.Kon.BusinessEntity.Defines.KanriType.Suru Then
      '一般商品で、会社基本情報上倉庫別在庫管理を行う時
      If Not String.IsNullOrEmpty(beMasterSms.SokoCode) _
      AndAlso Not beMasterSms.SokoCode.Equals(Tools.MakeText.MakeCommonCode(Me.BEKihonJoho.SokoKeta)) Then
        peKonInput.DetailList(rowIndex).SokoCode = beMasterSms.SokoCode
        isChangeSoko = False
      Else
        If Not String.IsNullOrEmpty(peKonInput.Header.BumonSoko) Then
          '商品マスターの倉庫コード未設定の時 部門倉庫コードを設定
          peKonInput.DetailList(rowIndex).SokoCode = peKonInput.Header.BumonSoko
        Else
          '部門倉庫コードも未設定の時は共通倉庫コードを設定
          peKonInput.DetailList(rowIndex).SokoCode = Tools.MakeText.MakeCommonCode(Me.BEKihonJoho.SokoKeta)
        End If
      End If
    Else
      '倉庫別在庫管理外の時は、共通倉庫コードを設定
      peKonInput.DetailList(rowIndex).SokoCode = Tools.MakeText.MakeCommonCode(Me.BEKihonJoho.SokoKeta)
    End If

    '商品項目・計算式コード
    If beMasterSms.MasterKubun = PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan Then
      '一般商品のみ
      peKonInput.DetailList(rowIndex).SyohinKomoku1 = beMasterSms.SyohinKomoku1
      peKonInput.DetailList(rowIndex).SyohinKomoku2 = beMasterSms.SyohinKomoku2
      peKonInput.DetailList(rowIndex).SyohinKomoku3 = beMasterSms.SyohinKomoku3
      peKonInput.DetailList(rowIndex).SyokonKeisan = 0    'このサンプルでは計算式を扱わないため
    Else
      peKonInput.DetailList(rowIndex).SyohinKomoku1 = 0D
      peKonInput.DetailList(rowIndex).SyohinKomoku2 = 0D
      peKonInput.DetailList(rowIndex).SyohinKomoku3 = 0D
      peKonInput.DetailList(rowIndex).SyokonKeisan = 0
    End If

    peKonInput.DetailList(rowIndex).Hakosu = 0D              ' 箱数
    peKonInput.DetailList(rowIndex).Suryo = 0D               ' 数量

    '単価金額
    Select Case beMasterSms.MasterKubun
      Case PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Ippan
        '一般商品
        If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
          '商魂
          peKonInput.DetailList(rowIndex).Tanka = Me.ChoiceTanka(peKonInput.Header.TekiyoBaikaNo, peKonInput.Header.Kakeritsu, beMasterSms)
          peKonInput.DetailList(rowIndex).GenTanka = beMasterSms.Genka
          peKonInput.DetailList(rowIndex).BaiTanka = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).Kingaku = 0D
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunShireTanka = 0D
        Else
          '商管
          peKonInput.DetailList(rowIndex).Tanka = beMasterSms.ShireTanka
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = 0D
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = 0D
          peKonInput.DetailList(rowIndex).HyojunShireTanka = beMasterSms.ShireTanka
        End If
        Exit Select

      Case PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Zatsuhin, PCA.TSC.Kon.BusinessEntity.Defines.MasterKubunType.Zappi
        '雑商品、諸雑費
        If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
          '商魂
          peKonInput.DetailList(rowIndex).Tanka = 0D
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = Me.ChoiceTanka(peKonInput.Header.TekiyoBaikaNo, peKonInput.Header.Kakeritsu, beMasterSms)
          peKonInput.DetailList(rowIndex).Genka = beMasterSms.Genka
          peKonInput.DetailList(rowIndex).BaikaKingaku = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunKakaku = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = beMasterSms.HyojunKakaku
          peKonInput.DetailList(rowIndex).HyojunShireTanka = 0D
        Else
          '商管
          peKonInput.DetailList(rowIndex).Tanka = 0D
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = beMasterSms.ShireTanka
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = 0D
          peKonInput.DetailList(rowIndex).HyojunShireTanka = beMasterSms.ShireTanka
        End If
        Exit Select
      Case Else
        '値引、記事
        If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
          '商魂
          peKonInput.DetailList(rowIndex).Tanka = 0D
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = 0D
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = 0D
          peKonInput.DetailList(rowIndex).HyojunShireTanka = 0D
        Else
          '商管
          peKonInput.DetailList(rowIndex).Tanka = 0D
          peKonInput.DetailList(rowIndex).GenTanka = 0D
          peKonInput.DetailList(rowIndex).BaiTanka = 0D
          peKonInput.DetailList(rowIndex).Kingaku = 0D
          peKonInput.DetailList(rowIndex).Genka = 0D
          peKonInput.DetailList(rowIndex).BaikaKingaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku = 0D
          peKonInput.DetailList(rowIndex).HyojunKakaku2 = 0D
          peKonInput.DetailList(rowIndex).HyojunShireTanka = 0D
        End If
        Exit Select
    End Select

    If _SystemKubun = PCA.TSC.Kon.BusinessEntity.Defines.SystemKubunType.Kon Then
      '入荷／発注マーク
      peKonInput.DetailList(rowIndex).KanMark = _
          KonCalc.JudgeKanMarkText(peKonInput.Header.Denku, _
                                   peKonInput.DetailList(rowIndex).MasterKubun, _
                                   beMasterSms.SystemKubun, _
                                   peKonInput.DetailList(rowIndex).Ku, _
                                   peKonInput.DetailList(rowIndex).KanMark)
    End If

    Dim checkZaikoware As Boolean = True
    If peKonInput.Header.Denku = PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Keiyaku Then
      '契約伝票は在庫割れのチェック対象外
      checkZaikoware = False
    End If

    headerLabelText = String.Empty
    '消費税、粗利益、利益率を計算（在庫割れも数量がゼロの為、チェック対象外）
    Me.CalcNotKingakuItems(peKonInput, rowIndex, False, checkZaikoware, False, needMessageBox, headerLabelText)

    '合計を再計算
    Me.CalcTotal(peKonInput, needMessageBox, Me._DenpyoType)

    Return True

  End Function

  ''' <summary>適用売価を選択。
  ''' </summary>
  ''' <param name="tekiyoBaikaNo">適用売価№</param>
  ''' <param name="kakeritsu">売価掛率</param>
  ''' <param name="beMasterSms">商品マスター</param>
  Public Function ChoiceTanka(ByVal tekiyoBaikaNo As Short, ByVal kakeritsu As Short, ByVal beMasterSms As BEMasterSms) As Decimal
    '
    '適用売価＝適用売価Noにて指定した単価×売価掛率÷100 (切り捨て)
    '掛率は小数桁を整数化した数値(100.0→1000)
    Dim baika As Decimal = 0D

    Select Case tekiyoBaikaNo
      Case 0
        '標準価格
        baika = beMasterSms.HyojunKakaku
      Case 1
        '売価1
        baika = beMasterSms.Baika1
      Case 2
        '売価2
        baika = beMasterSms.Baika2
      Case 3
        '売価3
        baika = beMasterSms.Baika3
      Case 4
        '売価4
        baika = beMasterSms.Baika4
      Case 5
        '売価5
        baika = beMasterSms.Baika5
      Case 6
        '原価
        baika = beMasterSms.Genka
    End Select

    Return KonCalc.RoundOff(baika * kakeritsu / 1000D, 0)

  End Function

#End Region

#Region "   倉庫"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferSoko As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferSokoTool As ReferSokoDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferSokoList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferSoko() As Boolean
    '
    Dim masterSokoTool As New MasterSokoTool
    m_ReferSokoTool = New ReferSokoDialogTool(_ReferDialogCommonVariable)

    If MyBase.MasterSoko.Loaded = True Then
      If _ReferSoko.Loaded = True Then
        'ロード済の時はクリア
        ReferSokoList.Clear()
        _ReferSoko.Loaded = False
      End If
      '取得済のマスターリストから
      ReferSokoList = m_ReferSokoTool.SettingReferItemsList(MyBase.BEMasterSokoList)
    Else
      ReferSokoList = m_ReferSokoTool.LoadReferItemsList(CommonVariable.IApplication, masterSokoTool)
    End If

    _ReferSoko.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferSokoDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point) As DialogResult
    '
    Dim masterSokoTool As New MasterSokoTool
    Dim dialogResult As DialogResult

    If _ReferSoko.Loaded = False Then
      m_ReferSokoTool = New ReferSokoDialogTool(_ReferDialogCommonVariable)
      ReferSokoList = m_ReferSokoTool.LoadReferItemsList(CommonVariable.IApplication, masterSokoTool)
    End If

    Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferSokoTool.Setting, ReferSokoList, CommonVariable.IApplication)
    LocateReferDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty

    referDialog.IApplication = CommonVariable.IApplication
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog()
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="rowIndex">行インデックス</param>
  ''' <param name="newCode">倉庫コード</param>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  ''' <param name="headerLabelText">ヘッダーラベル表示メッセージ</param>
  Public Function VaiidatingSoko(ByVal rowIndex As Integer, _
                                 ByVal newCode As String, _
                                 ByRef peKonInput As PEKonInput, _
                                 ByVal needMessageBox As Boolean, _
                                 ByRef headerLabelText As String) As Boolean
    '
    If String.IsNullOrEmpty(newCode) = True Then
      '未入力は共通倉庫にする
      newCode = MakeText.MakeCommonCode(Me.BEKihonJoho.SokoKeta)
    End If

    '倉庫マスターを取得
    Dim beMasterSoko As BEMasterSoko = MyBase.FindBEMasterSoko(newCode)

    If beMasterSoko.SokoCode.Length <> 0 Then
      '倉庫コードを設定
      peKonInput.DetailList(rowIndex).SokoCode = beMasterSoko.SokoCode

      '数量が入力済時は、在庫割れをチェック
      If peKonInput.DetailList(rowIndex).Suryo <> 0 Then
        'If peKonInput.Header.Denku <> PCA.TSC.Kon.BusinessEntity.Defines.DenkuType.Keiyaku Then
        '  '契約伝票は在庫割れチェックの対象外
        '  headerLabelText = Me.CheckZaikoware(peKonInput.DetailList(rowIndex))
        'End If
      End If
    Else
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Soko), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    Return True

  End Function

#End Region

#Region "   部門"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferBumon As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferBumonTool As ReferBumonDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferBumonList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferBumon() As Boolean
    '
    Dim masterBumonTool As New MasterBumonTool
    m_ReferBumonTool = New ReferBumonDialogTool(_ReferDialogCommonVariable)

    If MyBase.MasterBumon.Loaded = True Then
      If _ReferBumon.Loaded = True Then
        'ロード済の時はクリア
        ReferBumonList.Clear()
        _ReferBumon.Loaded = False
      End If
      '取得済のマスターリストから
      ReferBumonList = m_ReferBumonTool.SettingReferItemsList(MyBase.BEMasterBumonList)
    Else
      ReferBumonList = m_ReferBumonTool.LoadReferItemsList(CommonVariable.IApplication, masterBumonTool)
    End If

    _ReferBumon.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferBumonDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point) As DialogResult
    '
    Dim masterBumonTool As New MasterBumonTool
    Dim dialogResult As DialogResult

    If _ReferBumon.Loaded = False Then
      m_ReferBumonTool = New ReferBumonDialogTool(_ReferDialogCommonVariable)
      ReferBumonList = m_ReferBumonTool.LoadReferItemsList(CommonVariable.IApplication, masterBumonTool)
    End If

    Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferBumonTool.Setting, ReferBumonList, CommonVariable.IApplication)
    LocateReferDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty

    referDialog.IApplication = CommonVariable.IApplication
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog()
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newCode">部門コード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingBumon(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    '
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、共通コードを設定し処理続行
      newCode = MakeText.MakeCommonCode(_BEKihonJoho.BumonKeta)
    End If

    'マスターを取得
    Dim beMasterBumon As BEMasterBumon = FindBEMasterBumon(newCode)

    If beMasterBumon.BumonCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Bumon), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    _ModifiedEntity.Header.BumonCode = beMasterBumon.BumonCode
    _ModifiedEntity.Header.BumonMei = beMasterBumon.BumonMei
    _ModifiedEntity.Header.BumonSoko = beMasterBumon.SokoCode

    Return True

  End Function

#End Region

#Region "   担当者"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferTantosya As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferTantosyaTool As ReferTantosyaDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferTantosyaList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferTantosya() As Boolean
    '
    Dim masterTantosyaTool As New MasterTantosyaTool
    m_ReferTantosyaTool = New ReferTantosyaDialogTool(_ReferDialogCommonVariable)

    If MasterTantosya.Loaded = True Then
      If _ReferTantosya.Loaded = True Then
        'ロード済の時はクリア
        ReferTantosyaList.Clear()
        _ReferTantosya.Loaded = False
      End If
      '取得済のマスターリストから
      ReferTantosyaList = m_ReferTantosyaTool.SettingReferItemsList(BEMasterTantosyaList)
    Else
      ReferTantosyaList = m_ReferTantosyaTool.LoadReferItemsList(CommonVariable.IApplication, masterTantosyaTool)
    End If

    _ReferTantosya.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferTantosyaDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point) As DialogResult
    '
    Dim masterTantosyaTool As New MasterTantosyaTool
    Dim dialogResult As DialogResult

    If _ReferTantosya.Loaded = False Then
      m_ReferTantosyaTool = New ReferTantosyaDialogTool(_ReferDialogCommonVariable)
      ReferTantosyaList = m_ReferTantosyaTool.LoadReferItemsList(CommonVariable.IApplication, masterTantosyaTool)
    End If

    Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferTantosyaTool.Setting, ReferTantosyaList, CommonVariable.IApplication)
    LocateReferDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty

    referDialog.IApplication = CommonVariable.IApplication
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog()
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newCode">担当者コード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingTantosya(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    '
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、項目を初期化
      _ModifiedEntity.Header.TantosyaCode = String.Empty
      _ModifiedEntity.Header.TantosyaMei = String.Empty
      '部門は残したまま
      Return True
    End If

    'マスターを取得
    Dim beMasterTantosya As BEMasterTantosya = FindBEMasterTantosya(newCode)

    If beMasterTantosya.TantosyaCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Tantosya), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    _ModifiedEntity.Header.TantosyaCode = beMasterTantosya.TantosyaCode
    _ModifiedEntity.Header.TantosyaMei = beMasterTantosya.TantosyaMei

    '部門マスターを取得
    Dim beMasterBumon As BEMasterBumon = FindBEMasterBumon(beMasterTantosya.BumonCode)
    If beMasterBumon.BumonCode.Length <> 0 Then
      _ModifiedEntity.Header.BumonCode = beMasterBumon.BumonCode
      _ModifiedEntity.Header.BumonMei = beMasterBumon.BumonMei
      _ModifiedEntity.Header.BumonSoko = beMasterBumon.SokoCode
    End If

    Return True

  End Function

#End Region

#Region "   プロジェクト"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferProject As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferProjectTool As ReferProjectDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferProjectList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferProject() As Boolean
    '
    Dim masterProjectTool As New MasterProjectTool
    m_ReferProjectTool = New ReferProjectDialogTool(_ReferDialogCommonVariable)

    If MasterProject.Loaded = True Then
      If _ReferProject.Loaded = True Then
        'ロード済の時はクリア
        ReferProjectList.Clear()
        _ReferProject.Loaded = False
      End If
      '取得済のマスターリストから
      ReferProjectList = m_ReferProjectTool.SettingReferItemsList(BEMasterProjectList)
    Else
      ReferProjectList = m_ReferProjectTool.LoadReferItemsList(CommonVariable.IApplication, masterProjectTool)
    End If

    _ReferProject.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferProjectDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point) As DialogResult
    '
    Dim masterProjectTool As New MasterProjectTool
    Dim dialogResult As DialogResult

    If _ReferProject.Loaded = False Then
      m_ReferProjectTool = New ReferProjectDialogTool(_ReferDialogCommonVariable)
      ReferProjectList = m_ReferProjectTool.LoadReferItemsList(CommonVariable.IApplication, masterProjectTool)
    End If

    Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferProjectTool.Setting, ReferProjectList, CommonVariable.IApplication)
    LocateReferDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty

    referDialog.IApplication = CommonVariable.IApplication
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog()
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newcode">プロジェクトコード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingProject(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    '
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、項目を初期化
      _ModifiedEntity.Header.ProCode = String.Empty
      _ModifiedEntity.Header.ProMei = String.Empty
      Return True
    End If

    'プロジェクトマスターを取得
    Dim beMasterProject As BEMasterProject = FindBEMasterProject(newCode)

    If beMasterProject.ProCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, _BEKihonJoho.ProjectMei), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    _ModifiedEntity.Header.ProCode = beMasterProject.ProCode
    _ModifiedEntity.Header.ProMei = beMasterProject.ProMei

    Return True

  End Function

#End Region

#Region "   売上・仕入等の摘要"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferTekiyo As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferTekiyoTool As ReferCommonKubunDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferTekiyoList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferTekiyo() As Boolean
    '
    Dim masterTekiyoTool As New MasterTekiyoTool
    _ReferDialogCommonVariable.KubunId = PCA.TSC.Kon.BusinessEntity.Defines.KubunIdType.TekiyoKubun7
    m_ReferTekiyoTool = New ReferCommonKubunDialogTool(_ReferDialogCommonVariable)

    If MasterTekiyo.Loaded = True Then
      If _ReferTekiyo.Loaded = True Then
        'ロード済の時はクリア
        ReferTekiyoList.Clear()
        _ReferTekiyo.Loaded = False
      End If
      '取得済のマスターリストから
      ReferTekiyoList = m_ReferTekiyoTool.SettingReferItemsList(BEMasterTekiyoList)
    Else
      ReferTekiyoList = m_ReferTekiyoTool.LoadReferItemsList(CommonVariable.IApplication, masterTekiyoTool)
    End If

    _ReferTekiyo.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferTekiyoDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point) As DialogResult
    '
    Dim masterTekiyoTool As New MasterTekiyoTool
    Dim dialogResult As DialogResult

    If _ReferTekiyo.Loaded = False Then
      m_ReferTekiyoTool = New ReferCommonKubunDialogTool(_ReferDialogCommonVariable)
      ReferTekiyoList = m_ReferTekiyoTool.LoadReferItemsList(CommonVariable.IApplication, masterTekiyoTool)
    End If

    Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferTekiyoTool.Setting, ReferTekiyoList, CommonVariable.IApplication)
    LocateReferDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty

    referDialog.IApplication = CommonVariable.IApplication
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog()
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newcode">摘要コード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingTekiyo(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    '
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、処理続行
      Return True
    End If

    '摘要マスターを取得
    Dim beMasterTekiyo As BEMasterTekiyo = FindBEMasterTekiyo(BusinessEntity.Defines.KubunIdType.TekiyoKubun7, newCode)

    If beMasterTekiyo.TekiyoCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Tekiyo), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    _ModifiedEntity.Header.TekiyoCode = beMasterTekiyo.TekiyoCode
    _ModifiedEntity.Header.Tekiyo = beMasterTekiyo.TekiyoMei

    Return True

  End Function

#End Region

#Region "   直送先"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferYms As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferYmsTool As ReferYmsDialogTool

  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferYmsList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferYms() As Boolean
    '
    Dim masterYmsTool As New MasterYmsTool
    m_ReferYmsTool = New ReferYmsDialogTool(_ReferDialogCommonVariable)

    If MasterYms.Loaded = True Then
      If _ReferYms.Loaded = True Then
        'ロード済の時はクリア
        ReferYmsList.Clear()
        _ReferYms.Loaded = False
      End If
      '取得済のマスターリストから
      ReferYmsList = m_ReferYmsTool.SettingReferItemsList(BEMasterYmsList)
    Else
      ReferYmsList = m_ReferYmsTool.LoadReferItemsList(CommonVariable.IApplication, masterYmsTool)
    End If

    _ReferYms.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferYmsDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point) As DialogResult
    '
    Dim masterYmsTool As New MasterYmsTool
    Dim dialogResult As DialogResult

    If _ReferYms.Loaded = False Then
      m_ReferYmsTool = New ReferYmsDialogTool(_ReferDialogCommonVariable)
      ReferYmsList = m_ReferYmsTool.LoadReferItemsList(CommonVariable.IApplication, masterYmsTool)
    End If

    Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferYmsTool.Setting, ReferYmsList, CommonVariable.IApplication)
    LocateReferDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty

    referDialog.IApplication = CommonVariable.IApplication
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog()
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' </summary>
  ''' <param name="newcode">直送先コード</param>
  ''' <param name="needMessageBox">メッセージボックスを表示するかどうか</param>
  Public Function ValidatingChokusosaki(ByVal newCode As String, ByVal needMessageBox As Boolean) As Boolean
    '
    If String.IsNullOrEmpty(newCode) Then
      '未入力の時、項目を初期化
      _ModifiedEntity.Header.ChokusosakiCode = String.Empty
      _ModifiedEntity.Header.ChokusosakiMei1 = String.Empty
      _ModifiedEntity.Header.ChokusosakiMei2 = String.Empty
      _ModifiedEntity.Header.ChokusosakiComment = String.Empty
      Return True
    End If

    '直送先マスターを取得
    Dim beMasterYms As BEMasterYms = FindBEMasterYms(newCode)

    If beMasterYms.ChokusosakiCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Chokusosaki), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    _ModifiedEntity.Header.ChokusosakiCode = beMasterYms.ChokusosakiCode
    _ModifiedEntity.Header.ChokusosakiMei1 = beMasterYms.ChokusosakiMei1
    _ModifiedEntity.Header.ChokusosakiMei2 = beMasterYms.ChokusosakiMei2
    _ModifiedEntity.Header.ChokusosakiComment = beMasterYms.Comment

    Return True

  End Function

#End Region

#Region "   得意先、仕入先"

  ''' <summary>参照画面表示項目・取得状況
  ''' </summary>
  Public Property ReferTms As New LoadStatus

  Public Property ReferRms As New LoadStatus

  ''' <summary>参照画面ツールクラス
  ''' </summary>
  Private m_ReferTmsTool As ReferTmsRmsDialogTool

  Private m_ReferRmsTool As ReferTmsRmsDialogTool
  ''' <summary>参照画面表示項目リスト
  ''' </summary>
  Public Property ReferTmsList As List(Of ReferItems) = New List(Of ReferItems)

  Public Property ReferRmsList As List(Of ReferItems) = New List(Of ReferItems)

  ''' <summary>マスター参照画面用に表示項目リストプロパティに格納。
  ''' （補足）エラー発生時はFalseを返す。
  ''' </summary>
  Public Function LoadReferTms() As Boolean
    '
    Dim masterTmsTool As New MasterTmsTool
    m_ReferTmsTool = New ReferTmsRmsDialogTool(_ReferDialogCommonVariable)

    If MasterTms.Loaded = True Then
      If _ReferTms.Loaded = True Then
        'ロード済の時はクリア
        ReferTmsList.Clear()
        _ReferTms.Loaded = False
      End If
      '取得済のマスターリストから
      ReferTmsList = m_ReferTmsTool.SettingReferItemsList(BEMasterTmsList)
    Else
      ReferTmsList = m_ReferTmsTool.LoadReferItemsList(CommonVariable.IApplication, masterTmsTool)
    End If

    _ReferTms.Loaded = True

    Return True

  End Function

  Public Function LoadReferRms() As Boolean
    '
    Dim masterRmsTool As New MasterRmsTool
    m_ReferRmsTool = New ReferTmsRmsDialogTool(_ReferDialogCommonVariable)

    If MasterRms.Loaded = True Then
      If _ReferRms.Loaded = True Then
        'ロード済の時はクリア
        ReferRmsList.Clear()
        _ReferRms.Loaded = False
      End If
      '取得済のマスターリストから
      ReferRmsList = m_ReferRmsTool.SettingReferItemsList(BEMasterTmsList)
    Else
      ReferRmsList = m_ReferRmsTool.LoadReferItemsList(CommonVariable.IApplication, masterRmsTool)
    End If

    _ReferRms.Loaded = True

    Return True

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferTmsDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point, ByVal connector As IIntegratedApplication) As DialogResult
    '
    Dim masterTmsTool As New MasterTmsTool
    Dim dialogResult As DialogResult
    CommonVariable.IApplication = connector
    If _ReferTms.Loaded = False Then
      m_ReferTmsTool = New ReferTmsRmsDialogTool(_ReferDialogCommonVariable)
      ReferTmsList = m_ReferTmsTool.LoadReferItemsList(CommonVariable.IApplication, masterTmsTool)
    End If

    Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferTmsTool.Setting, ReferTmsList, CommonVariable.IApplication)

    LocateReferDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty
    referDialog.IApplication = CommonVariable.IApplication
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog(m_Owner)
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  Public Function ShowReferRmsDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point, ByVal connector As IIntegratedApplication) As DialogResult
    '
    Dim masterRmsTool As New MasterRmsTool
    Dim dialogResult As DialogResult
    CommonVariable.IApplication = connector
    If _ReferRms.Loaded = False Then
      m_ReferRmsTool = New ReferTmsRmsDialogTool(_ReferDialogCommonVariable)
      ReferRmsList = m_ReferRmsTool.LoadReferItemsList(CommonVariable.IApplication, masterRmsTool)
    End If

    Dim referDialog As Tools.TscReferDialog = New Tools.TscReferDialog(m_ReferRmsTool.Setting, ReferRmsList, CommonVariable.IApplication)

    LocateReferDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty
    referDialog.IApplication = CommonVariable.IApplication
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog(m_Owner)
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' （補足）スポット得意先の登録も表示可能。
  ''' </summary>
  ''' <param name="newCode">得意先コード</param>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="needMessageBox">エラーメッセージを表示するかどうか</param>
  Public Function ValidatingTokuisaki(ByVal newCode As String, ByRef peKonInput As PEKonInput, ByVal needMessageBox As Boolean) As Boolean
    Return Me.ValidatingTokuisaki(newCode, peKonInput, needMessageBox, True)
  End Function

  ''' <summary>コードを検証し伝票入力エンティティに設定
  ''' （補足）スポット得意先の登録も引数指定により表示可能。
  ''' </summary>
  ''' <param name="newCode">得意先コード</param>
  ''' <param name="peKonInput">伝票入力エンティティ</param>
  ''' <param name="needMessageBox">エラーメッセージを表示するかどうか</param>
  ''' <param name="createSpot">スポット得意先を作成するかどうか</param>
  Public Function ValidatingTokuisaki(ByVal newCode As String, ByRef peKonInput As PEKonInput, ByVal needMessageBox As Boolean, ByVal createSpot As Boolean) As Boolean

    If String.IsNullOrEmpty(newCode) = True Then
      '未入力→未入力時
      If String.IsNullOrEmpty(peKonInput.Header.TorihikisakiCode) = True Then
        Return True
      Else
        '入力済→未入力時、得意先コードは必須入力
        If needMessageBox Then
          ShowMessage.OnExclamation(m_Owner, MessageDefines.Exclamation.IncorrectValue, CommonVariable.BasicConfig.ProgramName)
        End If
        Return False
      End If
    End If

    '得意先を取得
    Dim shiiresaki As BEMasterRms = MyBase.FindBEMasterRms(newCode)

    If shiiresaki.ShiresakiCode.Length = 0 Then
      '取得不可の時、
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Tokuisaki), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    ''請求先を取得
    'Dim seikyusaki As BEMasterTms = FindSeikyusaki(tokuisaki)
    'If seikyusaki.TokuisakiCode.Length = 0 Then
    '  '請求先が存在しない
    '  If needMessageBox Then
    '    ShowMessage.OnExclamation(m_Owner, _
    '                              String.Format(Tools.MessageDefines.Exclamation.Unregistered, BusinessEntity.ItemText.Seikyusaki), _
    '                              CommonVariable.BasicConfig.ProgramName)
    '  End If
    '  Return False
    'ElseIf seikyusaki.TokuisakiCode <> seikyusaki.SeikyusakiCode Then
    '  '請求先の請求先が設定されている
    '  If needMessageBox Then
    '    ShowMessage.OnExclamation(m_Owner, Tools.MessageDefines.Exclamation.SeikyusakiOnSeikyusaki, CommonVariable.BasicConfig.ProgramName)
    '  End If
    '  Return False
    'End If

    If shiiresaki.ShiyoKubun = PCA.TSC.Kon.BusinessEntity.Defines.ShiyoKubunType.Kinshi Then
      '使用禁止
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(Tools.MessageDefines.Exclamation.Unusable, BusinessEntity.ItemText.Tokuisaki), _
                                  CommonVariable.BasicConfig.ProgramName)
      End If
      Return False
    End If

    '得意先項目を伝票入力エンティティに設定
    peKonInput.Header.TorihikisakiCode = shiiresaki.ShiresakiCode
    peKonInput.Header.AitesakiId = shiiresaki.AitesakiId
    '名1～メールアドレス、先方担当者は後述
    'peKonInput.Header.TekiyoBaikaNo = tokuisaki.tek
    'peKonInput.Header.Kakeritsu = tokuisaki.
    peKonInput.Header.ZeiKansan = shiiresaki.ZeiKansan
    'peKonInput.Header.KingakuHasu = seikyusaki.KingakuHasu                      '請求先から設定
    'peKonInput.Header.SyohizeiHasu = seikyusaki.SyohizeiHasu                    '請求先から設定
    'peKonInput.Header.SyohizeiTsuchiKon = seikyusaki.SyohizeiTsuchi             '請求先から設定
    'peKonInput.Header.BaiTankaKansan = tokuisaki.UritankaKansan                 '売単価換算？
    'peKonInput.Header.SyatenCode = tokuisaki.SyatenCode                         '売上のみ使用
    'peKonInput.Header.StoreTorihikisakiCode = tokuisaki.StoreTorihikisakiCode   '売上のみ使用
    'peKonInput.Header.SeisakiCode = tokuisaki.SeikyusakiCode                    '請求先コード
    peKonInput.Header.SenpoTantosyaId = shiiresaki.SenpoTantosyaId
    '相手先ID（スポット用）は後述
    '部門項目
    Dim beMasterBumon As BEMasterBumon = MyBase.FindBEMasterBumon(shiiresaki.SyuBumon)
    If beMasterBumon.BumonCode.Length <> 0 Then
      peKonInput.Header.BumonCode = beMasterBumon.BumonCode
      peKonInput.Header.BumonMei = beMasterBumon.BumonMei
      peKonInput.Header.BumonSoko = beMasterBumon.SokoCode
    End If
    '担当者項目
    Dim beMasterTantosya As BEMasterTantosya = MyBase.FindBEMasterTantosya(shiiresaki.Syutantosya)
    If beMasterTantosya.TantosyaCode.Length <> 0 Then
      peKonInput.Header.TantosyaCode = beMasterTantosya.TantosyaCode
      peKonInput.Header.TantosyaMei = beMasterTantosya.TantosyaMei
    End If
    'peKonInput.Header.BunruiCode = tokuisaki.BunruiCode                         '売上のみ使用
    'peKonInput.Header.DenpyoKubun = tokuisaki.DenpyoKubun                       '売上のみ使用

    'ゼロ得意先の時、スポット得意先の登録画面を表示
    Dim spotTokuisaki As BEMasterTms = Nothing
    If createSpot AndAlso newCode = MakeText.MakeCommonCode(Me.BEKihonJoho.TokuisakiKeta) Then
      spotTokuisaki = Me.ShowSpotTokuisakiDialog(spotTokuisaki, True, shiiresaki.AitesakiKeisyo)
    End If

    If spotTokuisaki IsNot Nothing Then
      '登録フラグとしてスポット相手先IDに-1を設定
      peKonInput.Header.SpotAitesakiId = -1

      peKonInput.Header.TorihikisakiMei1 = spotTokuisaki.TokuisakiMei1
      peKonInput.Header.TorihikisakiMei2 = spotTokuisaki.TokuisakiMei2
      peKonInput.Header.Jyusyo1 = spotTokuisaki.Jyusyo1
      peKonInput.Header.Jyusyo2 = spotTokuisaki.Jyusyo2
      peKonInput.Header.YubinBango = spotTokuisaki.YubinBango
      peKonInput.Header.TelNo = spotTokuisaki.AitesakiTelNo
      peKonInput.Header.FAXNo = spotTokuisaki.AitesakiFaxNo
      peKonInput.Header.Keisyo = spotTokuisaki.AitesakiKeisyo
      peKonInput.Header.MailAddress = spotTokuisaki.AitesakiMailAddress
      '
      peKonInput.Header.SenpoTantosyaMei = spotTokuisaki.SenpoTantosyaMei
    Else
      'スポット登録なし
      peKonInput.Header.SpotAitesakiId = 0

      If newCode = MakeText.MakeCommonCode(Me.BEKihonJoho.TokuisakiKeta) Then
        'ゼロ得意先でスポット登録なしの時は、得意先項目に初期値を設定
        peKonInput.Header.TorihikisakiMei1 = String.Empty
        peKonInput.Header.TorihikisakiMei2 = String.Empty
        peKonInput.Header.Jyusyo1 = String.Empty
        peKonInput.Header.Jyusyo2 = String.Empty
        peKonInput.Header.YubinBango = String.Empty
        peKonInput.Header.TelNo = String.Empty
        peKonInput.Header.FAXNo = String.Empty
        peKonInput.Header.Keisyo = String.Empty
        peKonInput.Header.MailAddress = String.Empty
        '先方担当者のみゼロ得意先のものを設定
        peKonInput.Header.SenpoTantosyaMei = shiiresaki.SenpoTantosyaMei
      Else
        peKonInput.Header.TorihikisakiMei1 = shiiresaki.ShiresakiMei1
        peKonInput.Header.TorihikisakiMei2 = shiiresaki.ShiresakiMei2
        peKonInput.Header.Jyusyo1 = shiiresaki.Jyusyo1
        peKonInput.Header.Jyusyo2 = shiiresaki.Jyusyo2
        peKonInput.Header.YubinBango = shiiresaki.YubinBango
        peKonInput.Header.TelNo = shiiresaki.AitesakiTelNo
        peKonInput.Header.FAXNo = shiiresaki.AitesakiFaxNo
        peKonInput.Header.Keisyo = shiiresaki.AitesakiKeisyo
        peKonInput.Header.MailAddress = shiiresaki.AitesakiMailAddress
        peKonInput.Header.SyohizeiTsuchiKan = shiiresaki.SyohizeiKeisan
        peKonInput.Header.ZeiKansan = shiiresaki.ZeiKansan
        '通常の得意先はマスタの設定値のまま
        peKonInput.Header.SenpoTantosyaMei = shiiresaki.SenpoTantosyaMei
      End If
    End If
    '◇伝票エンティティ外
    'peKonInput.Header.TorihikisakiComment = tokuisaki.Comment
    'peKonInput.Header.SeiKikanTo = seikyusaki.SeikyuKikanTo                 '請求先から設定
    'peKonInput.Header.SeiShimebi = seikyusaki.SeikyuShimebi                 '請求先から設定
    'peKonInput.Header.MisyuZandaka = KonCalc.CalcMisyuZandaka(seikyusaki)   '請求先から設定
    '原価端数の設定
    'peKonInput.Header.GenkaHasu = seikyusaki.KingakuHasu                    '請求先から設定

    ' 明細が入力されていたら伝票の再計算を行う
    If peKonInput.DetailList.Exists(Function(match) String.IsNullOrEmpty(match.SyohinCode) = False) Then
      Me.ReCalcDenpyo(peKonInput, needMessageBox)
    End If

    Return True

  End Function


#End Region

#Region "   ロット詳細"

  ''' <summary>マスター参照画面を表示
  ''' </summary>
  ''' <param name="code">旧コード</param>
  ''' <param name="newCode">新コード</param>
  ''' <param name="location">参照画面表示位置</param>
  ''' <param name="connector">コネクタ</param>
  ''' 
  Public Function ShowReferLotDetailDialog(ByVal code As String, ByRef newCode As String, ByVal location As Point, ByVal connector As IIntegratedApplication, _
                                           ByVal intLotDetailNo As Integer) As DialogResult
    '
    Dim masterLotDetailTool As New Sunloft.SearchEx.MasterLotDetailTool
    Dim masterLotDetailSettings As New Sunloft.SearchEx.LmdReferDialogSettings
    Dim dialogResult As DialogResult


    Dim referDialog As Sunloft.SearchEx.LmdReferDialog = New Sunloft.SearchEx.LmdReferDialog(masterLotDetailSettings, intLotDetailNo, connector)


    LocateReferLmdDialog(location, referDialog) '表示位置等を設定

    newCode = String.Empty

    referDialog.IApplication = connector
    referDialog.SelectedItem = code

    '参照画面を表示
    dialogResult = referDialog.ShowDialog()
    newCode = referDialog.SelectedItem

    Return dialogResult

  End Function

#End Region

#End Region

  ''' <summary>見積Noで見積伝票を検索。
  ''' </summary>
  ''' <param name="mitsumoriNo">見積No</param>
  ''' <returns>見積伝票</returns>
  ''' <remarks></remarks>
  Public Function FindByMitsumoriNo(ByVal mitsumoriNo As Integer, ByVal needMessageBox As Boolean) As BEInputMIT
    '
    Dim inputMITCondition As New InputMITCondition
    Dim beInputMIT As New BEInputMIT
    Dim beInputMITList As New List(Of BEInputMIT)

    inputMITCondition.MitsumoriNo = mitsumoriNo
    beInputMITList = MyBase.FindByConditionBEInputMITList(inputMITCondition)
    If beInputMITList.Count = 0 Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(MessageDefines.Exclamation.DenpyoNotExist, BusinessEntity.ItemText.Mitsumori), _
                                  CommonVariable.BasicConfig.ProgramName)
        Return beInputMIT
      End If
    End If

    Return beInputMITList(0)

  End Function

  ''' <summary>受注Noで受注伝票を検索。
  ''' </summary>
  ''' <param name="juchuNo">受注No</param>
  ''' <returns>受注伝票</returns>
  ''' <remarks></remarks>
  Public Function FindByJuchuNo(ByVal juchuNo As Integer, ByVal needMessageBox As Boolean) As BEInputJUC
    '
    Dim inputJUCCondition As New InputJUCCondition
    Dim beInputJUC As New BEInputJUC
    Dim beInputJUCList As New List(Of BEInputJUC)

    inputJUCCondition.JuchuNo = juchuNo
    beInputJUCList = MyBase.FindByConditionBEInputJUCList(inputJUCCondition)
    If beInputJUCList.Count = 0 Then
      If needMessageBox Then
        ShowMessage.OnExclamation(m_Owner, _
                                  String.Format(MessageDefines.Exclamation.DenpyoNotExist, BusinessEntity.ItemText.Juchu), _
                                  CommonVariable.BasicConfig.ProgramName)
        Return beInputJUC
      End If
    End If

    Return beInputJUCList(0)

  End Function

End Class

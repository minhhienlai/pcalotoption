﻿Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon
Imports PCA.Controls
Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.PCAIF
Imports Sunloft.Common
Imports System.Globalization
Imports System.Text

Public Class frmList
  Private Const WIDTH_TYPE As Integer = 100
  Private Const WIDTH_WAREHOUSE_DATE As Integer = 150
  Private Const WIDTH_SLIP_NO As Integer = 100
  Private Const WIDTH_SUP_CODE As Integer = 100
  Private Const WIDTH_SUP_NAME As Integer = 300
  Private Const WIDTH_PRODUCT_CODE As Integer = 150
  Private Const WIDTH_PRODUCT_NAME As Integer = 300
  Private Const WIDTH_LOT_NO As Integer = 200
  Private Const WIDTH_QUANTITY As Integer = 100
  Private Const WIDTH_UNIT As Integer = 100
  Private Const WIDTH_UNIT_PRICE As Integer = 100
  Private Const WIDTH_SHOUMI As Integer = 150
  Private Const WIDTH_NOTE As Integer = 100
  Private Const WIDTH_LOT_DETAILS As Integer = 150
  Private Const HEIGHT_HEADER As Integer = 25
  Private Const NUMBER_TYPE As String = "Number"
  Private Const TEXT_TYPE As String = "String"
  Private Const DATE_TYPE As String = "Date"

  Private Const SUPPLIER_CODE_COLUMN As Integer = 3
  Private Const PRODUCT_NAME_COLUMN As Integer = 6

  Private Const PROGRAM_TO_CALL As String = "\Pro03010.exe" 'Click table cell -> Display slip
  Private Const RPT_FILENAME As String = "WarehousingList.rpt" ' Form to preview/Print
  Private Const RPT_TITLE As String = "入荷ロット一覧表"
  Private Const MAX_ROW_COUNT = 99

  Private Const FROM_CODE_TEXT As Integer = 0
  Private Const TO_CODE_TEXT As Integer = 1
  Private Const NO_SELECTED_CODETEXT As Integer = -1

  Private connector As IIntegratedApplication = Nothing
  Private m_appClass As ConnectToPCA = New ConnectToPCA()
  Private m_SL_LMBClass As SL_LMB
  Private m_AMS1Class As AMS1
  Private arLotDetails(10) As String
  Private m_conf As Sunloft.PcaConfig = New Sunloft.PcaConfig
  Private m_IntRowIndex As Integer = -1
  Private MasterDialog As SLMasterSearchDialog
  Private DispColums As ColumnResult() = {New ColumnResult}
  Private m_isSearching As Boolean = False
  Private m_strCurCode As String = String.Empty
  Private m_intCurItem As Integer
  Private m_isKeyDown As Boolean = False

#Region "Initialize"

  Public Sub New()
    Try
      InitializeComponent()

      m_appClass.ConnectToPCA()
      connector = m_appClass.connector

      m_SL_LMBClass = New SL_LMB(connector)
      m_SL_LMBClass.ReadOnlyRow()
      m_AMS1Class = New AMS1(connector)
      m_AMS1Class.ReadAll()
      MasterDialog = New SLMasterSearchDialog(connector)
      arLotDetails = {m_SL_LMBClass.sl_lmb_label1, m_SL_LMBClass.sl_lmb_label2, m_SL_LMBClass.sl_lmb_label3, m_SL_LMBClass.sl_lmb_label4, m_SL_LMBClass.sl_lmb_label5, _
                       m_SL_LMBClass.sl_lmb_label6, m_SL_LMBClass.sl_lmb_label7, m_SL_LMBClass.sl_lmb_label8, m_SL_LMBClass.sl_lmb_label9, m_SL_LMBClass.sl_lmb_label10}

      InitTable()

      PcaFunctionCommandPrint.Enabled = False
      PcaFunctionCommandExport.Enabled = False
      PcaFunctionCommandRefer.Enabled = False

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    Try
      Dim configXMLDoc As New Xml.XmlDocument

      MyBase.OnLoad(e)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitTable()
    Try

      With dgv
        InitTableColumn(ColumnName.Type, WIDTH_TYPE, DataGridViewContentAlignment.MiddleCenter)
        InitTableColumn(ColumnName.WhDate, WIDTH_WAREHOUSE_DATE, DataGridViewContentAlignment.MiddleCenter)
        InitTableColumn(ColumnName.SlipNo, WIDTH_SLIP_NO, DataGridViewContentAlignment.MiddleRight)
        InitTableColumn(ColumnName.SupCode, WIDTH_SUP_CODE)
        InitTableColumn(ColumnName.SupName, WIDTH_PRODUCT_NAME)
        InitTableColumn(ColumnName.ProductCode, WIDTH_PRODUCT_CODE)
        InitTableColumn(ColumnName.ProductName, WIDTH_PRODUCT_NAME)
        InitTableColumn(ColumnName.LotNo, WIDTH_LOT_NO)
        InitTableColumn(ColumnName.Qty, WIDTH_QUANTITY, DataGridViewContentAlignment.MiddleRight)
        InitTableColumn(ColumnName.UnitName, WIDTH_UNIT)
        InitTableColumn(ColumnName.UnitPrice, WIDTH_UNIT_PRICE, DataGridViewContentAlignment.MiddleRight)
        InitTableColumn(ColumnName.Note, WIDTH_NOTE)
        If m_SL_LMBClass.sl_lmb_bbdflg = 1 Then
          InitTableColumn(ColumnName.Shoumi, WIDTH_SHOUMI, DataGridViewContentAlignment.MiddleCenter)
        Else
          InitTableColumn(ColumnName.Shoumi, WIDTH_SHOUMI, DataGridViewContentAlignment.MiddleCenter, , , False)
        End If

        For intCount As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
          InitTableColumn(arLotDetails(intCount), WIDTH_LOT_DETAILS)
        Next

        'Set header size (Default size is a bit small)
        dgv.ColumnHeadersHeight = HEIGHT_HEADER
        dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        For intCount As Integer = 0 To dgv.Columns.Count - 1
          dgv.Columns(intCount).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        Next
        'Set table not sortable
        For Each column As DataGridViewColumn In .Columns
          column.SortMode = DataGridViewColumnSortMode.NotSortable
          column.HeaderCell.Style.ForeColor = Color.White
        Next
      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitTableColumn(ByVal strHeaderText As String, ByVal intColumnWidth As Integer _
                              , Optional ByVal alignment As DataGridViewContentAlignment = DataGridViewContentAlignment.MiddleLeft _
                              , Optional ByVal strColumnname As String = "", Optional ByVal isReadonly As Boolean = True _
                              , Optional ByVal isVisible As Boolean = True)
    Dim col As New DataGridViewTextBoxColumn
    col.ReadOnly = isReadonly
    col.Visible = isVisible
    If strColumnname = String.Empty Then
      col.Name = strHeaderText
    Else
      col.Name = strColumnname
    End If
    col.Width = intColumnWidth

    col.DefaultCellStyle.Alignment = alignment
    dgv.Columns.Add(col)
  End Sub

#End Region


#Region "PCA Command Manager"
  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Dim commandItem As PcaCommandItem = e.CommandItem
    Select Case commandItem.CommandName
      Case PcaCommandItemClose.CommandName
        Me.Close()
      Case PcaCommandItemSearch.CommandName
        Search()
      Case PcaCommandItemPreview.CommandName
        PrintData(True)
      Case PcaCommandItemPrint.CommandName
        If Not SLCmnFunction.checkObjectNothingEmpty(dgv.Rows.Item(0).Cells.Item(ColumnName.ProductCode)) Then
          If MsgBox(RPT_TITLE & "を" & SLConstants.ConfirmMessage.PRINT_CONFIRM_MESSAGE, MsgBoxStyle.OkCancel, SLConstants.NotifyMessage.TITLE_MESSAGE) = MsgBoxResult.Ok Then
            PrintData()
          End If
        End If
      Case PcaCommandItemExport.CommandName
        If OutPutCSV() Then
          SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, SLConstants.CommonMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me _
                                    , PcaCommandItemExport.CommandId)
        End If
      Case PcaCommandItemRefer.CommandName
        If m_isKeyDown Then m_isKeyDown = False : Exit Sub
        DisplayReferScreen(Nothing, m_intCurItem)
    End Select

  End Sub
#End Region


#Region "Private Method"

  Private Function OutPutCSV() As Boolean

    Try
      Dim strSaveFileName As String = String.Empty
      Dim strColumns() As String
      Dim res As System.Windows.Forms.DialogResult

      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        .OverwritePrompt = True

        res = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False
        strSaveFileName = .FileName
      End With

      strColumns = {ColumnName.Type, ColumnName.WhDate, ColumnName.SlipNo, ColumnName.SupCode, ColumnName.SupName, ColumnName.ProductCode, _
                    ColumnName.ProductName, ColumnName.LotNo, ColumnName.Qty, ColumnName.UnitPrice, ColumnName.UnitName, ColumnName.Shoumi, ColumnName.Note}

      SLCmnFunction.OutPutCSVFromDataGridView(strColumns, dgv, strSaveFileName, DispColums.Count)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function

  Private Function PrintData(Optional ByVal isPreview As Boolean = False) As Boolean

    Try
      Dim ReportName As String = RPT_FILENAME
      Dim ReportTitle As String = RPT_TITLE
      Dim rpt As New Sunloft.Windows.Forms.SLCrystalReport
      Dim RpParam As New Dictionary(Of String, Object)
      Dim editedDataTable As DataTable = Nothing

      rpt.ReportFile = IO.Path.GetDirectoryName(Application.ExecutablePath) & "\" & ReportName
      EditPrintData(editedDataTable)
      rpt.DataSource = editedDataTable
      ' 出力開始
      If isPreview Then
        rpt.Preview(ReportTitle, True)
      Else
        rpt.PrintToPrinter()
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try

    Return True
  End Function

  Private Function EditPrintData(ByRef argEditedData As DataTable) As Boolean

    Dim editData As New DataSet1.DataTable1DataTable
    Dim editRow As DataRow
    Dim intLpc As Integer = 0

    Try
      While intLpc < dgv.Rows.Count AndAlso Not SLCmnFunction.checkObjectNothingEmpty(dgv.Rows.Item(intLpc).Cells.Item(ColumnName.ProductCode))
        editRow = editData.NewRow
        editRow.Item("Condition") = "入荷日: " & rdtWhDate.FromDate.ToShortDateString & " ～ " & rdtWhDate.ToDate.ToShortDateString
        If rsetSupCode.FromCodeText.Trim <> String.Empty AndAlso rsetSupCode.ToCodeText.Trim <> String.Empty Then
          editRow.Item("Condition") = editRow.Item("Condition").ToString & "      仕入先：" & rsetSupCode.FromCodeText & " ～ " & rsetSupCode.ToCodeText
        End If

        If dgv.Rows.Item(intLpc).Cells.Item(ColumnName.Type).Value.ToString = "通常入荷" Then
          editRow.Item("Type") = "入荷"
        Else
          editRow.Item("Type") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.Type).Value.ToString
        End If

        editRow.Item("WhDate") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.WhDate).Value
        editRow.Item("SlipNo") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.SlipNo).Value
        editRow.Item("SupCode") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.SupCode).Value
        editRow.Item("SupName") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.SupName).Value
        editRow.Item("ProductCode") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.ProductCode).Value
        editRow.Item("ProductName") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.ProductName).Value
        editRow.Item("LotNo") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.LotNo).Value
        editRow.Item("Quantity") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.Qty).Value
        editRow.Item("Unit") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.UnitName).Value.ToString
        editRow.Item("UnitPrice") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.UnitPrice).Value
        editRow.Item("Shoumi") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.Shoumi).Value
        editRow.Item("Note") = dgv.Rows.Item(intLpc).Cells.Item(ColumnName.Note).Value

        editData.Rows.Add(editRow)
        intLpc = intLpc + 1
      End While

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
    argEditedData = editData
    Return True
  End Function

  Private Sub Search()
    dgv.Focus()
    dgv.Rows.Clear()
    GetSearchResult()
    DispSearchResult()
  End Sub

  Private Sub ValidateSupplierCode(e As PCA.Controls.ValidatingEventArgs, ByRef CodeText As String)
    Try
      Dim curTokuisakiCode As String = SLCmnFunction.standardlizeCode(CodeText.TrimEnd(), m_AMS1Class.ams1_SupplierLength)
      If Not String.IsNullOrEmpty(curTokuisakiCode) AndAlso m_strCurCode = curTokuisakiCode Then
        CodeText = curTokuisakiCode
        'No changes
        Return
      End If

      If String.IsNullOrEmpty(curTokuisakiCode) Then Return

      'validate supplier code
      Dim beMasterRms As PCA.TSC.Kon.BusinessEntity.BEMasterRms = MasterDialog.FindBEMasterRms(curTokuisakiCode)

      If Not beMasterRms.ShiresakiCode.Length > 0 Then
        'If there's error
        e.Cancel = True
        CodeText = m_strCurCode
        Return
      Else
        CodeText = curTokuisakiCode
      End If

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub GetSearchResult()
    Try
      Dim selectCommand As ICustomCommand
      Dim reader As ICustomDataReader
      Dim intFromDate As Integer = rdtWhDate.FromIntDate
      Dim intToDate As Integer = rdtWhDate.ToIntDate
      Dim strFromSupCode As String = rsetSupCode.FromCodeText
      Dim strToSupCode As String = rsetSupCode.ToCodeText
      Dim intCount As Integer = -1
      Dim dtShomi As Date
      Dim intRetType As Integer = 0

      Array.Clear(DispColums, 0, DispColums.Length)

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & "GET_SL_NYKD_Pro03020")
      selectCommand.Parameters("@fromDate").SetValue(intFromDate)
      selectCommand.Parameters("@toDate").SetValue(intToDate)
      selectCommand.Parameters("@fromSupCode").SetValue(strFromSupCode)
      selectCommand.Parameters("@toSupCode").SetValue(strToSupCode)
      reader = selectCommand.ExecuteReader

      While reader.Read()
        intCount += 1
        ReDim Preserve DispColums(intCount)
        DispColums(intCount) = New ColumnResult

        Integer.TryParse(reader.GetValue("sl_nykh_return").ToString, intRetType)
        Select Case intRetType
          Case SLConstants.WarehouseKbn.NORMAL_WAREHOUSING_CODE
            DispColums(intCount).Type = SLConstants.WarehouseKbn.NORMAL_WAREHOUSING
          Case SLConstants.WarehouseKbn.RETURN_GOODS_CODE
            DispColums(intCount).Type = SLConstants.WarehouseKbn.RETURN_GOODS
        End Select

        DispColums(intCount).WhDate = Date.ParseExact(reader.GetValue("sl_nykh_uribi").ToString(), "yyyyMMdd", CultureInfo.CurrentCulture).ToString("yyyy/MM/dd")
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_nykh_denno")) Then DispColums(intCount).SlipNo = CInt(reader.GetValue("sl_nykh_denno"))
        DispColums(intCount).SupCode = reader.GetValue("sl_nykh_tcd").ToString()
        DispColums(intCount).SupName = reader.GetValue("cms_mei1").ToString().TrimEnd & " " & reader.GetValue("cms_mei2").ToString().TrimEnd
        DispColums(intCount).ProductCode = reader.GetValue("sl_nykd_scd").ToString()
        DispColums(intCount).ProductName = reader.GetValue("sl_nykd_mei").ToString().TrimEnd
        DispColums(intCount).LotNo = reader.GetValue("sl_nykd_ulotno").ToString()
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_nykd_suryo")) Then DispColums(intCount).Qty = CDec(reader.GetValue("sl_nykd_suryo"))
        DispColums(intCount).UnitName = reader.GetValue("sl_nykd_tani").ToString()
        If Not SLCmnFunction.checkObjectNothingEmpty(reader.GetValue("sl_nykd_tanka")) Then DispColums(intCount).UnitPrice = CDec(reader.GetValue("sl_nykd_tanka"))
        DispColums(intCount).Note = reader.GetValue("sl_nykd_biko").ToString()

        If Date.TryParseExact(reader.GetValue("sl_nykd_bbdate").ToString(), "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, dtShomi) Then
          DispColums(intCount).Shoumi = dtShomi.ToString("yyyy/MM/dd")
        End If

        For intCountLotDetails As Integer = 1 To m_SL_LMBClass.sl_lmb_maxlotop
          DispColums(intCount).Lotdetails(intCountLotDetails - 1) = reader.GetValue("sl_nykd_ldmei" & intCountLotDetails.ToString).ToString()
        Next

        DispColums(intCount).QtyNoOfDecDigit = CInt(reader.GetValue("sms_sketa"))
        DispColums(intCount).UnitPriceNoOfDecDigit = CInt(reader.GetValue("sms_tketa"))
      End While
      reader.Close()
      reader.Dispose()

      If Not DispColums(0) Is Nothing Then
        PcaFunctionCommandPrint.Enabled = True
        PcaFunctionCommandExport.Enabled = True
      Else
        PcaFunctionCommandPrint.Enabled = False
        PcaFunctionCommandExport.Enabled = False
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemSearch.CommandId)
        rdtWhDate.FocusFrom()
      End If
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

  Private Sub DispSearchResult()
    Try
      For intCount As Integer = 0 To DispColums.Length - 1
        If Not SLCmnFunction.checkObjectNothingEmpty(DispColums(intCount)) Then
          dgv.Rows.Add(New DataGridViewRow)
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.Type).Value = DispColums(intCount).Type
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.WhDate).Value = DispColums(intCount).WhDate
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.SlipNo).Value = DispColums(intCount).SlipNo
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.SupCode).Value = DispColums(intCount).SupCode
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.SupName).Value = DispColums(intCount).SupName
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.ProductCode).Value = DispColums(intCount).ProductCode
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.ProductName).Value = DispColums(intCount).ProductName
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.LotNo).Value = DispColums(intCount).LotNo
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.Qty).Value = DispColums(intCount).Qty.ToString(SLCmnFunction.FormatComma(DispColums(intCount).QtyNoOfDecDigit))
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.UnitName).Value = DispColums(intCount).UnitName
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.UnitPrice).Value = DispColums(intCount).UnitPrice.ToString(SLCmnFunction.FormatComma(DispColums(intCount).UnitPriceNoOfDecDigit))
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.Note).Value = DispColums(intCount).Note
          dgv.Rows.Item(intCount).Cells.Item(ColumnName.Shoumi).Value = DispColums(intCount).Shoumi

          For intCountLotDetails As Integer = 0 To m_SL_LMBClass.sl_lmb_maxlotop - 1
            dgv.Rows.Item(intCount).Cells.Item(arLotDetails(intCountLotDetails)).Value = DispColums(intCount).Lotdetails(intCountLotDetails)
          Next
        End If
      Next

    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
    Finally
    End Try
  End Sub

  Private Sub DisplayReferScreen(e As System.ComponentModel.CancelEventArgs, ByRef Item As Integer)

    Dim newCode As String = String.Empty
    Dim location As Point = Tools.ControlTool.GetDialogLocation(rsetSupCode.FromReferButton)
    newCode = MasterDialog.ShowReferRmsDialog(If(Item = FROM_CODE_TEXT, rsetSupCode.FromCodeText, rsetSupCode.ToCodeText), location)
    If String.IsNullOrEmpty(newCode) Then
      If Not e Is Nothing Then e.Cancel = True
    Else
      If m_strCurCode = newCode Then Return

      Dim beMasterRms As PCA.TSC.Kon.BusinessEntity.BEMasterRms = MasterDialog.FindBEMasterRms(newCode)
      If Not beMasterRms.ShiresakiCode.Length > 0 Then
        'Validate failed
        If Not e Is Nothing Then e.Cancel = True
        If Item = FROM_CODE_TEXT Then
          rsetSupCode.FromCodeText = m_strCurCode
        Else
          rsetSupCode.ToCodeText = m_strCurCode
        End If
        Return
      End If

      If Item = FROM_CODE_TEXT Then
        rsetSupCode.FromCodeText = newCode
      Else
        rsetSupCode.ToCodeText = newCode
      End If
    End If
  End Sub

#End Region

#Region "rsetSupCode Events"


  Private Sub rsetSupCode_FromCodeTextEnter(sender As System.Object, e As System.EventArgs) Handles rsetSupCode.FromCodeTextEnter
    PcaFunctionCommandRefer.Enabled = True
    m_strCurCode = rsetSupCode.FromCodeText
    m_intCurItem = FROM_CODE_TEXT
  End Sub

  Private Sub rsetSupCode_ToCodeTextEnter(sender As System.Object, e As System.EventArgs) Handles rsetSupCode.ToCodeTextEnter
    m_isSearching = False
    PcaFunctionCommandRefer.Enabled = True
    m_strCurCode = rsetSupCode.ToCodeText
    m_intCurItem = TO_CODE_TEXT
  End Sub

  Private Sub rsetSupCode_CodeTextLeave(sender As System.Object, e As System.EventArgs) Handles rsetSupCode.FromCodeTextLeave, rsetSupCode.ToCodeTextLeave
    PcaFunctionCommandRefer.Enabled = False
    m_intCurItem = NO_SELECTED_CODETEXT
  End Sub

  Private Sub rsetSupCode_ToCodeTextValidated(sender As System.Object, e As System.EventArgs) Handles rsetSupCode.ToCodeTextValidated
    PcaFunctionCommandRefer.Enabled = False
    If m_isSearching Then
      m_isSearching = False
      Search()
    End If
  End Sub

  Private Sub rsetSupCode_FromCodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles rsetSupCode.FromCodeTextValidating2
    ValidateSupplierCode(e, rsetSupCode.FromCodeText)
  End Sub

  Private Sub rsetSupCode_ToCodeTextValidating2(sender As System.Object, e As PCA.Controls.ValidatingEventArgs) Handles rsetSupCode.ToCodeTextValidating2
    ValidateSupplierCode(e, rsetSupCode.ToCodeText)
  End Sub

  Private Sub rsetSupCode_ClickFromReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles rsetSupCode.ClickFromReferButton2
    DisplayReferScreen(e, FROM_CODE_TEXT)
  End Sub

  Private Sub rsetSupCode_ClickToReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles rsetSupCode.ClickToReferButton2
    DisplayReferScreen(e, TO_CODE_TEXT)
  End Sub

#End Region

  Private Sub frmLotInput_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    If Not m_appClass.isAttach Then
      connector.LogOffSystem()
    End If
  End Sub

  Protected Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean

    Dim keyCode As Keys = (keyData And Keys.KeyCode)        'キーコード
    Dim keyModifiers As Keys = (keyData And Keys.Modifiers) '修飾子

    If (keyCode = Keys.Tab OrElse keyCode = Keys.Enter) Then
      'Validate if user press Enter/Tab +  Anykey except Shift, →, ↓
      m_isSearching = True
    Else
      m_isSearching = False
    End If

    Return MyBase.ProcessDialogKey(keyData)

  End Function


#Region "Form Events"
  Private Sub frmTransLotList_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
    If e.KeyCode = Keys.F8 Then m_isKeyDown = True
  End Sub
#End Region

  'Merge header cell
  Private Sub DataGridView1_Paint(sender As Object, e As System.Windows.Forms.PaintEventArgs) Handles dgv.Paint
    Dim monthes As String() = {ColumnName.SupCode, ColumnName.ProductCode}
    Dim intSupCol As Integer = SUPPLIER_CODE_COLUMN 'Merge 2-3 and 4-5
    Dim format As New StringFormat()
    Dim Rect As Rectangle
    Dim intWidth As Integer
    While intSupCol <= PRODUCT_NAME_COLUMN
      Rect = dgv.GetCellDisplayRectangle(intSupCol, -1, True)
      intWidth = dgv.GetCellDisplayRectangle(intSupCol + 1, -1, True).Width
      Rect.X += 1 : Rect.Y += 1
      Rect.Width = Rect.Width + intWidth - 2 : Rect.Height = CInt(Rect.Height - 1)
      e.Graphics.FillRectangle(New SolidBrush(dgv.ColumnHeadersDefaultCellStyle.BackColor), Rect)
      format.Alignment = StringAlignment.Near
      format.LineAlignment = StringAlignment.Center
      e.Graphics.DrawString(monthes(intSupCol \ 2 - 1), dgv.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Color.White), Rect, format)
      intSupCol += 2
    End While
  End Sub
  Private Sub DataGridView1_Scroll(sender As Object, e As System.Windows.Forms.ScrollEventArgs) Handles dgv.Scroll
    Dim rtHeader As Rectangle = dgv.DisplayRectangle
    rtHeader.Height = CInt(dgv.ColumnHeadersHeight / 2)
    dgv.Invalidate(rtHeader)
  End Sub

  Private Sub dgv_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellDoubleClick
    If dgv.Rows.Item(e.RowIndex).Cells.Item(ColumnName.SlipNo).Value Is Nothing Then
      Return
    End If
    SLCmnFunction.StartUpInputExe(IO.Path.GetDirectoryName(Application.ExecutablePath) & PROGRAM_TO_CALL, m_appClass.EncryptedLoginString _
                                  , dgv.Rows.Item(e.RowIndex).Cells.Item(ColumnName.SlipNo).Value.ToString)
  End Sub
End Class

Public Class ColumnResult
  Public Property Type As String = String.Empty
  Public Property WhDate As String = String.Empty
  Public Property SlipNo As Integer
  Public Property SupCode As String = String.Empty
  Public Property SupName As String = String.Empty
  Public Property ProductCode As String = String.Empty
  Public Property ProductName As String = String.Empty
  Public Property LotNo As String = String.Empty
  Public Property Qty As Decimal
  Public Property UnitName As String = String.Empty
  Public Property UnitPrice As Decimal
  Public Property Note As String = String.Empty
  Public Property Shoumi As String = String.Empty

  Public Property Lotdetails() As String() = New String(10) {}

  Public Property QtyNoOfDecDigit As Integer
  Public Property UnitPriceNoOfDecDigit As Integer
End Class

Public Class ColumnName
  Public Const Type As String = "区分"
  Public Const WhDate As String = "入荷日"
  Public Const SlipNo As String = "伝票番号"
  Public Const SupCode As String = "仕入先コード"
  Public Const SupName As String = "仕入先名"
  Public Const ProductCode As String = "商品コード"
  Public Const ProductName As String = "商品名"
  Public Const LotNo As String = "ロットNo"
  Public Const Qty As String = "数量"
  Public Const UnitName As String = "単位"
  Public Const UnitPrice As String = "単価"
  Public Const Shoumi As String = "賞味期限"
  Public Const Note As String = "備考"

  Public Const Lotdetail As String = "ロット詳細"
  Public Const Lotdetail1 As String = "ロット詳細1"
  Public Const Lotdetail2 As String = "ロット詳細2"
  Public Const Lotdetail3 As String = "ロット詳細3"
  Public Const Lotdetail4 As String = "ロット詳細4"
  Public Const Lotdetail5 As String = "ロット詳細5"
  Public Const Lotdetail6 As String = "ロット詳細6"
  Public Const Lotdetail7 As String = "ロット詳細7"
  Public Const Lotdetail8 As String = "ロット詳細8"
  Public Const Lotdetail9 As String = "ロット詳細9"
  Public Const Lotdetail10 As String = "ロット詳細10"
End Class
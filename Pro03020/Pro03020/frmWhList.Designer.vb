﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmList
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmList))
    Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.ToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemPreview = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemExport = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemSearch = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRefer = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPrint = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonPreview = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonExport = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonSearch = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.rdtWhDate = New PCA.Controls.PcaRangeDate()
    Me.rsetSupCode = New PCA.Controls.PcaRangeCodeSet()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandSearch = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPrint = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandPreview = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandExport = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemPrint = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemPreview = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemExport = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemSearch = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
    Me.dgv = New System.Windows.Forms.DataGridView()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
    Me.MenuStrip1.Size = New System.Drawing.Size(1176, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'ToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemFile, Nothing)
    Me.ToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemPrint, Me.ToolStripMenuItemPreview, Me.ToolStripMenuItemExport, Me.ToolStripMenuItemClose})
    Me.ToolStripMenuItemFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemFile.Name = "ToolStripMenuItemFile"
    Me.ToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.ToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPrint, Me.PcaCommandItemPrint)
    Me.ToolStripMenuItemPrint.Name = "ToolStripMenuItemPrint"
    Me.ToolStripMenuItemPrint.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPrint.Text = "印刷(&P)"
    '
    'ToolStripMenuItemPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemPreview, Me.PcaCommandItemPreview)
    Me.ToolStripMenuItemPreview.Name = "ToolStripMenuItemPreview"
    Me.ToolStripMenuItemPreview.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemPreview.Text = "印刷プレビュー(&V)"
    '
    'ToolStripMenuItemExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemExport, Me.PcaCommandItemExport)
    Me.ToolStripMenuItemExport.Name = "ToolStripMenuItemExport"
    Me.ToolStripMenuItemExport.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemExport.Text = "出力(&O)"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(193, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemSearch, Me.ToolStripMenuItemRefer})
    Me.ToolStripMenuItemEdit.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemSearch, Me.PcaCommandItemSearch)
    Me.ToolStripMenuItemSearch.Name = "ToolStripMenuItemSearch"
    Me.ToolStripMenuItemSearch.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemSearch.Text = "再表示(&R)"
    '
    'ToolStripMenuItemRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRefer, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemRefer.Name = "ToolStripMenuItemRefer"
    Me.ToolStripMenuItemRefer.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemRefer.Text = "参照(&U)"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(130, 22)
    Me.ToolStripMenuItemContent.Text = "目次（&C)"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonPrint, Me.ToolStripButtonPreview, Me.ToolStripButtonExport, Me.ToolStripButtonSearch, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1176, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPrint, Me.PcaCommandItemPrint)
    Me.ToolStripButtonPrint.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonPrint.Image = CType(resources.GetObject("ToolStripButtonPrint.Image"), System.Drawing.Image)
    Me.ToolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPrint.Name = "ToolStripButtonPrint"
    Me.ToolStripButtonPrint.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonPrint.Text = "印刷"
    Me.ToolStripButtonPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonPreview, Me.PcaCommandItemPreview)
    Me.ToolStripButtonPreview.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonPreview.Image = CType(resources.GetObject("ToolStripButtonPreview.Image"), System.Drawing.Image)
    Me.ToolStripButtonPreview.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonPreview.Name = "ToolStripButtonPreview"
    Me.ToolStripButtonPreview.Size = New System.Drawing.Size(81, 34)
    Me.ToolStripButtonPreview.Text = "プレビュー"
    Me.ToolStripButtonPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonExport, Me.PcaCommandItemExport)
    Me.ToolStripButtonExport.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonExport.Image = CType(resources.GetObject("ToolStripButtonExport.Image"), System.Drawing.Image)
    Me.ToolStripButtonExport.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonExport.Name = "ToolStripButtonExport"
    Me.ToolStripButtonExport.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonExport.Text = "出力"
    Me.ToolStripButtonExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonSearch, Me.PcaCommandItemSearch)
    Me.ToolStripButtonSearch.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonSearch.Image = CType(resources.GetObject("ToolStripButtonSearch.Image"), System.Drawing.Image)
    Me.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonSearch.Name = "ToolStripButtonSearch"
    Me.ToolStripButtonSearch.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonSearch.Text = "再表示"
    Me.ToolStripButtonSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"), System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    '
    'rdtWhDate
    '
    Me.rdtWhDate.AutoTopMargin = False
    Me.rdtWhDate.ClientProcess = Nothing
    Me.rdtWhDate.DayLabel = ""
    Me.rdtWhDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.rdtWhDate.Holidays = Nothing
    Me.rdtWhDate.Location = New System.Drawing.Point(18, 87)
    Me.rdtWhDate.MaxLabelLength = 10
    Me.rdtWhDate.MonthLabel = "/"
    Me.rdtWhDate.Name = "rdtWhDate"
    Me.rdtWhDate.RangeBorder = True
    Me.rdtWhDate.RangeGroupBoxText = "入荷日"
    Me.rdtWhDate.RangeGroupBoxVisible = False
    Me.rdtWhDate.Size = New System.Drawing.Size(331, 22)
    Me.rdtWhDate.TabIndex = 2
    Me.rdtWhDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rdtWhDate.YearLabel = "/"
    '
    'rsetSupCode
    '
    Me.rsetSupCode.AutoTopMargin = False
    Me.rsetSupCode.ClientProcess = Nothing
    Me.rsetSupCode.CodeTextAlignment = System.Windows.Forms.HorizontalAlignment.Right
    Me.rsetSupCode.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.rsetSupCode.Location = New System.Drawing.Point(381, 87)
    Me.rsetSupCode.MaxCodeLength = 10
    Me.rsetSupCode.MaxLabelLength = 10
    Me.rsetSupCode.MaxNameLength = 0
    Me.rsetSupCode.Name = "rsetSupCode"
    Me.rsetSupCode.RangeGroupBoxText = "仕入先"
    Me.rsetSupCode.RangeGroupBoxVisible = False
    Me.rsetSupCode.Size = New System.Drawing.Size(287, 22)
    Me.rsetSupCode.TabIndex = 3
    Me.rsetSupCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.rsetSupCode.TextAlignCodeSet = System.Drawing.ContentAlignment.MiddleRight
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 769)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1176, 22)
    Me.StatusStrip1.TabIndex = 4
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandSearch, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandPrint, Me.PcaFunctionCommandPreview, Me.PcaFunctionCommandExport, Me.PcaFunctionCommandClose})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 741)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1176, 28)
    Me.PcaFunctionBar1.TabIndex = 5
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    '
    'PcaFunctionCommandSearch
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandSearch, Me.PcaCommandItemSearch)
    Me.PcaFunctionCommandSearch.FunctionKey = PCA.Controls.FunctionKey.F5
    Me.PcaFunctionCommandSearch.Tag = Nothing
    Me.PcaFunctionCommandSearch.Text = "再表示"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    '
    'PcaFunctionCommandPrint
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPrint, Me.PcaCommandItemPrint)
    Me.PcaFunctionCommandPrint.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPrint.Tag = Nothing
    Me.PcaFunctionCommandPrint.Text = "印刷"
    '
    'PcaFunctionCommandPreview
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandPreview, Me.PcaCommandItemPreview)
    Me.PcaFunctionCommandPreview.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandPreview.ModifierKeys = PCA.Controls.FunctionModifierKeys.Shift
    Me.PcaFunctionCommandPreview.Tag = Nothing
    Me.PcaFunctionCommandPreview.Text = "プレビュー"
    '
    'PcaFunctionCommandExport
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandExport, Me.PcaCommandItemExport)
    Me.PcaFunctionCommandExport.FunctionKey = PCA.Controls.FunctionKey.F10
    Me.PcaFunctionCommandExport.Tag = Nothing
    Me.PcaFunctionCommandExport.Text = "出力"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemSearch, Me.PcaCommandItemRefer, Me.PcaCommandItemPrint, Me.PcaCommandItemExport, Me.PcaCommandItemClose, Me.PcaCommandItemPreview})
    '
    'PcaCommandItemPrint
    '
    Me.PcaCommandItemPrint.CommandName = "CommandPrint"
    '
    'PcaCommandItemPreview
    '
    Me.PcaCommandItemPreview.CommandName = "CommandPreview"
    '
    'PcaCommandItemExport
    '
    Me.PcaCommandItemExport.CommandId = 10
    Me.PcaCommandItemExport.CommandName = "Export"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandName = "CommandClose"
    '
    'PcaCommandItemSearch
    '
    Me.PcaCommandItemSearch.CommandId = 5
    Me.PcaCommandItemSearch.CommandName = "CommandSearch"
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandName = "CommandRefer"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandName = "CommandHelp"
    '
    'dgv
    '
    Me.dgv.AllowUserToAddRows = False
    Me.dgv.AllowUserToDeleteRows = False
    Me.dgv.AllowUserToResizeRows = False
    Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dgv.BackgroundColor = System.Drawing.SystemColors.Control
    Me.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
    DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
    DataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue
    DataGridViewCellStyle1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
    DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control
    DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
    DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
    Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
    Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
    Me.dgv.EnableHeadersVisualStyles = False
    Me.dgv.Location = New System.Drawing.Point(18, 134)
    Me.dgv.Name = "dgv"
    Me.dgv.RowHeadersVisible = False
    Me.dgv.RowTemplate.Height = 21
    Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
    Me.dgv.Size = New System.Drawing.Size(1146, 601)
    Me.dgv.TabIndex = 7
    '
    'frmList
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1176, 791)
    Me.Controls.Add(Me.dgv)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.rsetSupCode)
    Me.Controls.Add(Me.rdtWhDate)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1192, 829)
    Me.Name = "frmList"
    Me.Text = " 入荷ロット一覧表"
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout

End Sub
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents ToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPrint As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemPreview As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemExport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemRefer As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPrint As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonPreview As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonExport As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonSearch As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemSearch As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPrint As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemExport As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemPreview As PCA.Controls.PcaCommandItem
  Friend WithEvents rdtWhDate As PCA.Controls.PcaRangeDate
  Friend WithEvents rsetSupCode As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandSearch As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandPrint As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandPreview As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandExport As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
  Friend WithEvents dgv As System.Windows.Forms.DataGridView

End Class

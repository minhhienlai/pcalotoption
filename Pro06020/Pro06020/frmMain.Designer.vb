﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
  Inherits System.Windows.Forms.Form

  'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Windows フォーム デザイナーで必要です。
  Private components As System.ComponentModel.IContainer

  'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
  'Windows フォーム デザイナーを使用して変更できます。  
  'コード エディターを使って変更しないでください。
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
    Me.ServiceController1 = New System.ServiceProcess.ServiceController()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.FToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemOutput = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemClose = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemRefer = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItemContent = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.ToolStripButtonClose = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonOutput = New System.Windows.Forms.ToolStripButton()
    Me.ToolStripButtonHelp = New System.Windows.Forms.ToolStripButton()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.PcaFunctionBar1 = New PCA.Controls.PcaFunctionBar()
    Me.PcaFunctionCommandHelp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandMoveUp = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandMoveDown = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandRefer = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandOutput = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandClose = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandAllSelect = New PCA.Controls.PcaFunctionCommand()
    Me.PcaFunctionCommandAllUnSelect = New PCA.Controls.PcaFunctionCommand()
    Me.PcaCommandManager1 = New PCA.Controls.PcaCommandManager(Me.components)
    Me.PcaCommandItemOutput = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemClose = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemRefer = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemHelp = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemMoveUp = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemMoveDown = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemAllSelect = New PCA.Controls.PcaCommandItem()
    Me.PcaCommandItemAllUnSelect = New PCA.Controls.PcaCommandItem()
    Me.rbtnFileDialog = New PCA.Controls.PcaRefButton()
    Me.Button1 = New System.Windows.Forms.Button()
    Me.Button2 = New System.Windows.Forms.Button()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.lcmbSeparator = New PCA.Controls.PcaLabeledComboBox()
    Me.chkHeader = New PCA.Controls.PcaCheckBox()
    Me.lstTable = New PCA.Controls.PcaListBox()
    Me.lblSelectData = New PCA.Controls.PcaLabel()
    Me.lblCondition = New PCA.Controls.PcaLabel()
    Me.rsetCode1 = New PCA.Controls.PcaRangeCodeSet()
    Me.rsetCode2 = New PCA.Controls.PcaRangeCodeSet()
    Me.txtName1 = New PCA.Controls.PcaLabeledTextBox()
    Me.txtName2 = New PCA.Controls.PcaLabeledTextBox()
    Me.lblContents = New PCA.Controls.PcaLabel()
    Me.txtOutputPath = New PCA.Controls.PcaLabeledTextBox()
    Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
    Me.lstContentsHidden = New PCA.Controls.PcaListBox()
    Me.lstContents = New PCA.Controls.PcaCheckedListBox()
    Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
    Me.rdtDate = New PCA.Controls.PcaRangeDate()
    Me.Button3 = New System.Windows.Forms.Button()
    Me.MenuStrip1.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FToolStripMenuItemFile, Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemHelp})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
    Me.MenuStrip1.TabIndex = 0
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'FToolStripMenuItemFile
    '
    Me.PcaCommandManager1.SetCommandItem(Me.FToolStripMenuItemFile, Nothing)
    Me.FToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemOutput, Me.ToolStripMenuItemClose})
    Me.FToolStripMenuItemFile.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.FToolStripMenuItemFile.Name = "FToolStripMenuItemFile"
    Me.FToolStripMenuItemFile.Size = New System.Drawing.Size(96, 20)
    Me.FToolStripMenuItemFile.Text = "ファイル(&F)"
    '
    'ToolStripMenuItemOutput
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemOutput, Me.PcaCommandItemOutput)
    Me.ToolStripMenuItemOutput.Name = "ToolStripMenuItemOutput"
    Me.ToolStripMenuItemOutput.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemOutput.Text = "出力(&O)"
    Me.ToolStripMenuItemOutput.ToolTipText = "CSV出力を行います。"
    '
    'ToolStripMenuItemClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemClose, Me.PcaCommandItemClose)
    Me.ToolStripMenuItemClose.Name = "ToolStripMenuItemClose"
    Me.ToolStripMenuItemClose.Size = New System.Drawing.Size(137, 22)
    Me.ToolStripMenuItemClose.Text = "閉じる(&X)"
    Me.ToolStripMenuItemClose.ToolTipText = "プログラムを終了します。"
    '
    'ToolStripMenuItemEdit
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemEdit, Nothing)
    Me.ToolStripMenuItemEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemRefer})
    Me.ToolStripMenuItemEdit.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
    Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(68, 20)
    Me.ToolStripMenuItemEdit.Text = "編集(&E)"
    '
    'ToolStripMenuItemRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemRefer, Me.PcaCommandItemRefer)
    Me.ToolStripMenuItemRefer.Name = "ToolStripMenuItemRefer"
    Me.ToolStripMenuItemRefer.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemRefer.Text = "参照(&U)"
    Me.ToolStripMenuItemRefer.ToolTipText = "マスタの検索を行います。"
    '
    'ToolStripMenuItemHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemHelp, Nothing)
    Me.ToolStripMenuItemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemContent})
    Me.ToolStripMenuItemHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripMenuItemHelp.Name = "ToolStripMenuItemHelp"
    Me.ToolStripMenuItemHelp.Size = New System.Drawing.Size(82, 20)
    Me.ToolStripMenuItemHelp.Text = "ヘルプ(&H)"
    '
    'ToolStripMenuItemContent
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripMenuItemContent, Me.PcaCommandItemHelp)
    Me.ToolStripMenuItemContent.Name = "ToolStripMenuItemContent"
    Me.ToolStripMenuItemContent.Size = New System.Drawing.Size(123, 22)
    Me.ToolStripMenuItemContent.Text = "目次(&C)"
    Me.ToolStripMenuItemContent.ToolTipText = "ヘルプを表示します。"
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonClose, Me.ToolStripButtonOutput, Me.ToolStripButtonHelp})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(1008, 37)
    Me.ToolStrip1.TabIndex = 1
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'ToolStripButtonClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonClose, Me.PcaCommandItemClose)
    Me.ToolStripButtonClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonClose.Image = CType(resources.GetObject("ToolStripButtonClose.Image"), System.Drawing.Image)
    Me.ToolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonClose.Name = "ToolStripButtonClose"
    Me.ToolStripButtonClose.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonClose.Text = "閉じる"
    Me.ToolStripButtonClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonClose.ToolTipText = "プログラムを終了します。"
    '
    'ToolStripButtonOutput
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonOutput, Me.PcaCommandItemOutput)
    Me.ToolStripButtonOutput.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonOutput.Image = Global.Pro06020.My.Resources.Resources.export
    Me.ToolStripButtonOutput.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonOutput.Name = "ToolStripButtonOutput"
    Me.ToolStripButtonOutput.Size = New System.Drawing.Size(39, 34)
    Me.ToolStripButtonOutput.Text = "出力"
    Me.ToolStripButtonOutput.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonOutput.ToolTipText = "CSV出力を行います。"
    '
    'ToolStripButtonHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.ToolStripButtonHelp, Me.PcaCommandItemHelp)
    Me.ToolStripButtonHelp.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.ToolStripButtonHelp.Image = CType(resources.GetObject("ToolStripButtonHelp.Image"), System.Drawing.Image)
    Me.ToolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.ToolStripButtonHelp.Name = "ToolStripButtonHelp"
    Me.ToolStripButtonHelp.Size = New System.Drawing.Size(53, 34)
    Me.ToolStripButtonHelp.Text = "ヘルプ"
    Me.ToolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
    Me.ToolStripButtonHelp.ToolTipText = "ヘルプを表示します。"
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 708)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1008, 22)
    Me.StatusStrip1.TabIndex = 25
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'PcaFunctionBar1
    '
    Me.PcaFunctionBar1.Commands.AddRange(New PCA.Controls.PcaFunctionCommand() {Me.PcaFunctionCommandHelp, Me.PcaFunctionCommandMoveUp, Me.PcaFunctionCommandMoveDown, Me.PcaFunctionCommandRefer, Me.PcaFunctionCommandOutput, Me.PcaFunctionCommandClose, Me.PcaFunctionCommandAllSelect, Me.PcaFunctionCommandAllUnSelect})
    Me.PcaFunctionBar1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.PcaFunctionBar1.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
    Me.PcaFunctionBar1.Location = New System.Drawing.Point(0, 680)
    Me.PcaFunctionBar1.Name = "PcaFunctionBar1"
    Me.PcaFunctionBar1.Size = New System.Drawing.Size(1008, 28)
    Me.PcaFunctionBar1.TabIndex = 44
    Me.PcaFunctionBar1.TabStop = False
    '
    'PcaFunctionCommandHelp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandHelp, Me.PcaCommandItemHelp)
    Me.PcaFunctionCommandHelp.Tag = Nothing
    Me.PcaFunctionCommandHelp.Text = "ヘルプ"
    Me.PcaFunctionCommandHelp.ToolTipText = "ヘルプを参照します。"
    '
    'PcaFunctionCommandMoveUp
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandMoveUp, Me.PcaCommandItemMoveUp)
    Me.PcaFunctionCommandMoveUp.FunctionKey = PCA.Controls.FunctionKey.F6
    Me.PcaFunctionCommandMoveUp.Tag = Nothing
    Me.PcaFunctionCommandMoveUp.Text = "上へ移動"
    Me.PcaFunctionCommandMoveUp.ToolTipText = "選択項目を上に移動します。"
    '
    'PcaFunctionCommandMoveDown
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandMoveDown, Me.PcaCommandItemMoveDown)
    Me.PcaFunctionCommandMoveDown.FunctionKey = PCA.Controls.FunctionKey.F7
    Me.PcaFunctionCommandMoveDown.Tag = Nothing
    Me.PcaFunctionCommandMoveDown.Text = "下へ移動"
    Me.PcaFunctionCommandMoveDown.ToolTipText = "選択項目を下に移動します。"
    '
    'PcaFunctionCommandRefer
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandRefer, Me.PcaCommandItemRefer)
    Me.PcaFunctionCommandRefer.FunctionKey = PCA.Controls.FunctionKey.F8
    Me.PcaFunctionCommandRefer.Tag = Nothing
    Me.PcaFunctionCommandRefer.Text = "参照"
    Me.PcaFunctionCommandRefer.ToolTipText = "マスタの検索を行います。"
    '
    'PcaFunctionCommandOutput
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandOutput, Me.PcaCommandItemOutput)
    Me.PcaFunctionCommandOutput.FunctionKey = PCA.Controls.FunctionKey.F9
    Me.PcaFunctionCommandOutput.Tag = Nothing
    Me.PcaFunctionCommandOutput.Text = "出力"
    Me.PcaFunctionCommandOutput.ToolTipText = "CSV出力を行います。"
    '
    'PcaFunctionCommandClose
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandClose, Me.PcaCommandItemClose)
    Me.PcaFunctionCommandClose.FunctionKey = PCA.Controls.FunctionKey.F12
    Me.PcaFunctionCommandClose.Tag = Nothing
    Me.PcaFunctionCommandClose.Text = "閉じる"
    Me.PcaFunctionCommandClose.ToolTipText = "プログラムを終了します。"
    '
    'PcaFunctionCommandAllSelect
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandAllSelect, Me.PcaCommandItemAllSelect)
    Me.PcaFunctionCommandAllSelect.FunctionKey = PCA.Controls.FunctionKey.F3
    Me.PcaFunctionCommandAllSelect.Tag = Nothing
    Me.PcaFunctionCommandAllSelect.Text = "全選択"
    Me.PcaFunctionCommandAllSelect.ToolTipText = "出力項目を全選択します。"
    '
    'PcaFunctionCommandAllUnSelect
    '
    Me.PcaCommandManager1.SetCommandItem(Me.PcaFunctionCommandAllUnSelect, Me.PcaCommandItemAllUnSelect)
    Me.PcaFunctionCommandAllUnSelect.FunctionKey = PCA.Controls.FunctionKey.F4
    Me.PcaFunctionCommandAllUnSelect.Tag = Nothing
    Me.PcaFunctionCommandAllUnSelect.Text = "全解除"
    Me.PcaFunctionCommandAllUnSelect.ToolTipText = "出力項目を全解除します。"
    '
    'PcaCommandManager1
    '
    Me.PcaCommandManager1.CommandItems.AddRange(New PCA.Controls.PcaCommandItem() {Me.PcaCommandItemHelp, Me.PcaCommandItemMoveUp, Me.PcaCommandItemMoveDown, Me.PcaCommandItemRefer, Me.PcaCommandItemOutput, Me.PcaCommandItemClose, Me.PcaCommandItemAllSelect, Me.PcaCommandItemAllUnSelect})
    Me.PcaCommandManager1.Parent = Me
    '
    'PcaCommandItemOutput
    '
    Me.PcaCommandItemOutput.CommandId = 9
    Me.PcaCommandItemOutput.CommandName = "Output"
    '
    'PcaCommandItemClose
    '
    Me.PcaCommandItemClose.CommandId = 12
    Me.PcaCommandItemClose.CommandName = "Close"
    '
    'PcaCommandItemRefer
    '
    Me.PcaCommandItemRefer.CommandId = 8
    Me.PcaCommandItemRefer.CommandName = "Refer"
    '
    'PcaCommandItemHelp
    '
    Me.PcaCommandItemHelp.CommandId = 1
    Me.PcaCommandItemHelp.CommandName = "Help"
    '
    'PcaCommandItemMoveUp
    '
    Me.PcaCommandItemMoveUp.CommandId = 6
    Me.PcaCommandItemMoveUp.CommandName = "MoveUp"
    '
    'PcaCommandItemMoveDown
    '
    Me.PcaCommandItemMoveDown.CommandId = 7
    Me.PcaCommandItemMoveDown.CommandName = "MoveDown"
    '
    'PcaCommandItemAllSelect
    '
    Me.PcaCommandItemAllSelect.CommandId = 3
    Me.PcaCommandItemAllSelect.CommandName = "AllSelect"
    '
    'PcaCommandItemAllUnSelect
    '
    Me.PcaCommandItemAllUnSelect.CommandId = 4
    Me.PcaCommandItemAllUnSelect.CommandName = "AllUnSelect"
    '
    'rbtnFileDialog
    '
    Me.PcaCommandManager1.SetCommandItem(Me.rbtnFileDialog, Nothing)
    Me.rbtnFileDialog.Location = New System.Drawing.Point(816, 92)
    Me.rbtnFileDialog.Name = "rbtnFileDialog"
    Me.rbtnFileDialog.Size = New System.Drawing.Size(21, 22)
    Me.rbtnFileDialog.TabIndex = 75
    Me.rbtnFileDialog.UseVisualStyleBackColor = True
    '
    'Button1
    '
    Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.PcaCommandManager1.SetCommandItem(Me.Button1, Nothing)
    Me.Button1.Location = New System.Drawing.Point(876, 419)
    Me.Button1.Name = "Button1"
    Me.Button1.Size = New System.Drawing.Size(75, 58)
    Me.Button1.TabIndex = 10
    Me.Button1.Text = "上へ"
    Me.Button1.UseVisualStyleBackColor = True
    '
    'Button2
    '
    Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.PcaCommandManager1.SetCommandItem(Me.Button2, Nothing)
    Me.Button2.Location = New System.Drawing.Point(876, 500)
    Me.Button2.Name = "Button2"
    Me.Button2.Size = New System.Drawing.Size(75, 58)
    Me.Button2.TabIndex = 11
    Me.Button2.Text = "下へ"
    Me.Button2.UseVisualStyleBackColor = True
    '
    'lcmbSeparator
    '
    Me.lcmbSeparator.ClientProcess = Nothing
    Me.lcmbSeparator.DropDownWidth = 210
    Me.lcmbSeparator.Items.AddRange(New Object() {"1:カンマ区切り(CSV方式)", "2:タブ区切り(TXT方式)"})
    Me.lcmbSeparator.LabelText = "区切り文字"
    Me.lcmbSeparator.Location = New System.Drawing.Point(12, 72)
    Me.lcmbSeparator.MaxLabelLength = 15
    Me.lcmbSeparator.MaxTextLength = 30
    Me.lcmbSeparator.Name = "lcmbSeparator"
    Me.lcmbSeparator.SelectedIndex = 0
    Me.lcmbSeparator.SelectedItem = "1:カンマ区切り(CSV方式)"
    Me.lcmbSeparator.SelectedText = ""
    Me.lcmbSeparator.SelectedValue = Nothing
    Me.lcmbSeparator.Size = New System.Drawing.Size(315, 22)
    Me.lcmbSeparator.TabIndex = 1
    Me.lcmbSeparator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'chkHeader
    '
    Me.chkHeader.Location = New System.Drawing.Point(680, 71)
    Me.chkHeader.MaxTextLength = 40
    Me.chkHeader.Name = "chkHeader"
    Me.chkHeader.Size = New System.Drawing.Size(280, 22)
    Me.chkHeader.TabIndex = 0
    Me.chkHeader.Text = "項目タイトル名を先頭行に出力する"
    Me.chkHeader.UseVisualStyleBackColor = True
    '
    'lstTable
    '
    Me.lstTable.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.lstTable.FormattingEnabled = True
    Me.lstTable.Location = New System.Drawing.Point(12, 147)
    Me.lstTable.Name = "lstTable"
    Me.lstTable.Size = New System.Drawing.Size(315, 524)
    Me.lstTable.TabIndex = 3
    '
    'lblSelectData
    '
    Me.lblSelectData.Location = New System.Drawing.Point(12, 125)
    Me.lblSelectData.Name = "lblSelectData"
    Me.lblSelectData.Size = New System.Drawing.Size(140, 22)
    Me.lblSelectData.TabIndex = 58
    Me.lblSelectData.Text = "データ選択"
    '
    'lblCondition
    '
    Me.lblCondition.Location = New System.Drawing.Point(370, 125)
    Me.lblCondition.Name = "lblCondition"
    Me.lblCondition.Size = New System.Drawing.Size(140, 22)
    Me.lblCondition.TabIndex = 59
    Me.lblCondition.Text = "出力条件"
    '
    'rsetCode1
    '
    Me.rsetCode1.AutoTopMargin = False
    Me.rsetCode1.ClientProcess = Nothing
    Me.rsetCode1.Location = New System.Drawing.Point(370, 191)
    Me.rsetCode1.MaxNameLength = 0
    Me.rsetCode1.Name = "rsetCode1"
    Me.rsetCode1.RangeGroupBoxText = "コード1"
    Me.rsetCode1.RangeGroupBoxVisible = False
    Me.rsetCode1.Size = New System.Drawing.Size(273, 22)
    Me.rsetCode1.TabIndex = 5
    Me.rsetCode1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'rsetCode2
    '
    Me.rsetCode2.AutoTopMargin = False
    Me.rsetCode2.ClientProcess = Nothing
    Me.rsetCode2.Location = New System.Drawing.Point(370, 212)
    Me.rsetCode2.MaxNameLength = 0
    Me.rsetCode2.Name = "rsetCode2"
    Me.rsetCode2.RangeGroupBoxText = "コード2"
    Me.rsetCode2.RangeGroupBoxVisible = False
    Me.rsetCode2.Size = New System.Drawing.Size(273, 22)
    Me.rsetCode2.TabIndex = 6
    Me.rsetCode2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'txtName1
    '
    Me.txtName1.ClientProcess = Nothing
    Me.txtName1.LabelText = "名称1"
    Me.txtName1.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.txtName1.Location = New System.Drawing.Point(370, 257)
    Me.txtName1.MaxTextLength = 50
    Me.txtName1.Name = "txtName1"
    Me.txtName1.Size = New System.Drawing.Size(490, 22)
    Me.txtName1.TabIndex = 7
    '
    'txtName2
    '
    Me.txtName2.ClientProcess = Nothing
    Me.txtName2.LabelText = "名称2"
    Me.txtName2.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.txtName2.Location = New System.Drawing.Point(370, 278)
    Me.txtName2.MaxTextLength = 50
    Me.txtName2.Name = "txtName2"
    Me.txtName2.Size = New System.Drawing.Size(490, 22)
    Me.txtName2.TabIndex = 8
    '
    'lblContents
    '
    Me.lblContents.Location = New System.Drawing.Point(370, 322)
    Me.lblContents.Name = "lblContents"
    Me.lblContents.Size = New System.Drawing.Size(140, 22)
    Me.lblContents.TabIndex = 71
    Me.lblContents.Text = "出力項目"
    '
    'txtOutputPath
    '
    Me.txtOutputPath.ClientProcess = Nothing
    Me.txtOutputPath.LabelText = "出力パス"
    Me.txtOutputPath.LabelTextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.txtOutputPath.Location = New System.Drawing.Point(12, 92)
    Me.txtOutputPath.MaxLabelLength = 15
    Me.txtOutputPath.MaxTextLength = 100
    Me.txtOutputPath.Name = "txtOutputPath"
    Me.txtOutputPath.Size = New System.Drawing.Size(805, 22)
    Me.txtOutputPath.TabIndex = 2
    '
    'lstContentsHidden
    '
    Me.lstContentsHidden.FormattingEnabled = True
    Me.lstContentsHidden.Location = New System.Drawing.Point(581, 344)
    Me.lstContentsHidden.Name = "lstContentsHidden"
    Me.lstContentsHidden.Size = New System.Drawing.Size(202, 316)
    Me.lstContentsHidden.TabIndex = 77
    Me.lstContentsHidden.Visible = False
    '
    'lstContents
    '
    Me.lstContents.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lstContents.FormattingEnabled = True
    Me.lstContents.Location = New System.Drawing.Point(370, 344)
    Me.lstContents.Name = "lstContents"
    Me.lstContents.Size = New System.Drawing.Size(490, 324)
    Me.lstContents.TabIndex = 9
    '
    'rdtDate
    '
    Me.rdtDate.AllowEmpty = True
    Me.rdtDate.AutoTopMargin = False
    Me.rdtDate.ClientProcess = Nothing
    Me.rdtDate.DisplaySlash = True
    Me.rdtDate.Holidays = Nothing
    Me.rdtDate.Location = New System.Drawing.Point(370, 147)
    Me.rdtDate.Name = "rdtDate"
    Me.rdtDate.RangeGroupBoxText = "日付"
    Me.rdtDate.RangeGroupBoxVisible = False
    Me.rdtDate.Size = New System.Drawing.Size(413, 22)
    Me.rdtDate.TabIndex = 4
    Me.rdtDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Button3
    '
    Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.PcaCommandManager1.SetCommandItem(Me.Button3, Nothing)
    Me.Button3.Location = New System.Drawing.Point(876, 344)
    Me.Button3.Name = "Button3"
    Me.Button3.Size = New System.Drawing.Size(75, 58)
    Me.Button3.TabIndex = 78
    Me.Button3.Text = "reload"
    Me.Button3.UseVisualStyleBackColor = True
    Me.Button3.Visible = False
    '
    'frmMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackColor = System.Drawing.SystemColors.Control
    Me.ClientSize = New System.Drawing.Size(1008, 730)
    Me.Controls.Add(Me.Button3)
    Me.Controls.Add(Me.rdtDate)
    Me.Controls.Add(Me.Button2)
    Me.Controls.Add(Me.Button1)
    Me.Controls.Add(Me.lstContentsHidden)
    Me.Controls.Add(Me.txtOutputPath)
    Me.Controls.Add(Me.rbtnFileDialog)
    Me.Controls.Add(Me.lblContents)
    Me.Controls.Add(Me.txtName2)
    Me.Controls.Add(Me.txtName1)
    Me.Controls.Add(Me.rsetCode2)
    Me.Controls.Add(Me.rsetCode1)
    Me.Controls.Add(Me.lblCondition)
    Me.Controls.Add(Me.lblSelectData)
    Me.Controls.Add(Me.lstTable)
    Me.Controls.Add(Me.chkHeader)
    Me.Controls.Add(Me.lcmbSeparator)
    Me.Controls.Add(Me.PcaFunctionBar1)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.MenuStrip1)
    Me.Controls.Add(Me.lstContents)
    Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 10.0!)
    Me.KeyPreview = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MinimumSize = New System.Drawing.Size(1024, 768)
    Me.Name = "frmMain"
    Me.Text = "汎用データ出力"
    Me.MenuStrip1.ResumeLayout(false)
    Me.MenuStrip1.PerformLayout
    Me.ToolStrip1.ResumeLayout(false)
    Me.ToolStrip1.PerformLayout
    Me.ResumeLayout(false)
    Me.PerformLayout

End Sub
  Friend WithEvents ServiceController1 As System.ServiceProcess.ServiceController
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents FToolStripMenuItemFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemOutput As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemClose As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemEdit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemRefer As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItemContent As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents ToolStripButtonClose As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonOutput As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripButtonHelp As System.Windows.Forms.ToolStripButton
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents PcaFunctionBar1 As PCA.Controls.PcaFunctionBar
  Friend WithEvents PcaCommandManager1 As PCA.Controls.PcaCommandManager
  Friend WithEvents PcaCommandItemMoveUp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemMoveDown As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemRefer As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemOutput As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemClose As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandMoveUp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandMoveDown As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandRefer As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandOutput As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandClose As PCA.Controls.PcaFunctionCommand
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents PcaCommandItemHelp As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaFunctionCommandHelp As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaCommandItemAllSelect As PCA.Controls.PcaCommandItem
  Friend WithEvents PcaCommandItemAllUnSelect As PCA.Controls.PcaCommandItem
  Friend WithEvents lcmbSeparator As PCA.Controls.PcaLabeledComboBox
  Friend WithEvents chkHeader As PCA.Controls.PcaCheckBox
  Friend WithEvents lblSelectData As PCA.Controls.PcaLabel
  Friend WithEvents lstTable As PCA.Controls.PcaListBox
  Friend WithEvents lblCondition As PCA.Controls.PcaLabel
  Friend WithEvents lblContents As PCA.Controls.PcaLabel
  Friend WithEvents txtName2 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents txtName1 As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents rsetCode2 As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents rsetCode1 As PCA.Controls.PcaRangeCodeSet
  Friend WithEvents txtOutputPath As PCA.Controls.PcaLabeledTextBox
  Friend WithEvents rbtnFileDialog As PCA.Controls.PcaRefButton
  Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
  Friend WithEvents lstContentsHidden As PCA.Controls.PcaListBox
  Friend WithEvents lstContents As PCA.Controls.PcaCheckedListBox
  Friend WithEvents PcaFunctionCommandAllSelect As PCA.Controls.PcaFunctionCommand
  Friend WithEvents PcaFunctionCommandAllUnSelect As PCA.Controls.PcaFunctionCommand
  Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
  Friend WithEvents Button2 As System.Windows.Forms.Button
  Friend WithEvents Button1 As System.Windows.Forms.Button
  Friend WithEvents rdtDate As PCA.Controls.PcaRangeDate
  Friend WithEvents Button3 As System.Windows.Forms.Button

End Class

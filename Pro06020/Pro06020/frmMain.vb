﻿Imports Sunloft.PCAControls
Imports Sunloft.Message
Imports Sunloft.PCAForms
Imports Sunloft.Common
Imports PCA.Controls
Imports System.ComponentModel
Imports Sunloft.PCAIF
Imports PCA.ApplicationIntegration
Imports PCA.TSC.Kon
Imports System.Text


Public Class frmMain

#Region "Declare - 変数宣言"
  Property connector As IIntegratedApplication = Nothing

  Private m_conf As Sunloft.PcaConfig
  Private m_appClass As ConnectToPCA = New ConnectToPCA

  Private AMS1Class As AMS1
  Private m_isNotValidate As Boolean = True

  Private m_tableData() As SL_GDO
  Private m_isEnableMoveUP As Boolean = True
  Private m_isEnableMoveDown As Boolean = True
  Private m_isUseReferCommand1 As Boolean = False
  Private m_isUseReferCommand2 As Boolean = False
  Private m_isEnableOutput As Boolean = False

  Private Const SEPARATOR_CSV As Integer = 0
  Private Const SEPARATOR_TXT As Integer = 1

  Private Const CTR_HEIGHT As Integer = 22
  Private Const CTR_GAP As Integer = 22
  Private Const CTR_GAP_END As Integer = 28
  Private Const CTR_TOP_DATE As Integer = 147
  Private Const CTR_TOP_CODE1 As Integer = 191
  Private Const CTR_TOP_CODE2 As Integer = 212
  Private Const CTR_TOP_TEXT1 As Integer = 257
  Private Const CTR_TOP_TEXT2 As Integer = 278
  Private Const CTR_TOP_CONTENTS As Integer = 344
  Private Const CTR_HEIGHT_CONTENTS As Integer = 324

  Private m_isFromTextEnter As Boolean = True

#End Region

#Region "Initialize"

  Public Sub New()
    Try

      InitializeComponent()

      m_appClass.ConnectToPCA()
      connector = m_appClass.connector
      m_conf = New Sunloft.PcaConfig

      InitControl()

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub InitControl()
    Try

      AMS1Class = New AMS1(connector)
      AMS1Class.ReadAll() 'Get number of digits for fields
      ResetCtlLocation()

      SetTableList()
      txtOutputPath.Text = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory).ToString & "\" & lstTable.SelectedItem.ToString
      SetFileExt()
      chkHeader.Checked = True

      '### Function Enabled
      PcaFunctionCommandRefer.Enabled = False
      PcaFunctionCommandOutput.Enabled = False
      PcaFunctionCommandMoveDown.Enabled = False

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    Try

      MyBase.OnLoad(e)
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub


#End Region

#Region " Form Method"
  Private Sub frmMain_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
    If Not m_appClass.isAttach Then connector.LogOffSystem()
  End Sub
#End Region

#Region " PcaCodeSet Methods"

#End Region

#Region " Other Control Methods"
  Private Sub rbtnFileDialog_Click(sender As System.Object, e As System.EventArgs) Handles rbtnFileDialog.Click
    Try
      With FolderBrowserDialog1
        .SelectedPath = txtOutputPath.Text.Replace("\" & IO.Path.GetFileNameWithoutExtension(txtOutputPath.Text.Trim) & IO.Path.GetExtension(txtOutputPath.Text.Trim), String.Empty)

        If .ShowDialog() = DialogResult.OK Then
          '選択されたフォルダを表示する
          SetFileExt(.SelectedPath)
        End If
      End With
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return
    End Try
  End Sub

  Private Sub ListBox_TextBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lcmbSeparator.SelectedIndexChanged, lstTable.SelectedIndexChanged
    Try

      SetFileExt()
      Select Case sender.GetType.Name
        Case lcmbSeparator.GetType.Name
          'Dim obj As PcaLabeledTextBox
          'obj = DirectCast(sender, PcaLabeledTextBox)
          'SetTableCols(obj.SelectedIndex)
        Case lstTable.GetType.Name
          SetTableCols(m_tableData(lstTable.SelectedIndex))
      End Select

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return
    End Try
  End Sub

  Private Sub lstContents_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstContents.SelectedIndexChanged
    Try
      If lstContents.SelectedIndex = 0 Then
        m_isEnableMoveUP = False
      Else
        m_isEnableMoveUP = True
      End If

      If lstContents.SelectedIndex = lstContents.Items.Count - 1 Then
        m_isEnableMoveDown = False
      Else
        m_isEnableMoveDown = True
      End If
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    End Try
  End Sub

  Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
    MoveItem(True)
  End Sub

  Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
    MoveItem(False)
  End Sub

  Private Sub RangeCodeSet_FromCodeTextEnter(sender As System.Object, e As System.EventArgs) Handles rsetCode1.FromCodeTextEnter, rsetCode2.FromCodeTextEnter
    m_isFromTextEnter = True
  End Sub
  Private Sub RangeCodeSet_ToCodeTextEnter(sender As System.Object, e As System.EventArgs) Handles rsetCode1.ToCodeTextEnter, rsetCode2.ToCodeTextEnter
    m_isFromTextEnter = False
  End Sub

  Private Sub RangeCodeSet_ClickFromReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles rsetCode1.ClickFromReferButton2, rsetCode2.ClickFromReferButton2
    m_isFromTextEnter = True
    If Not SearchItem(Me.ActiveControl) Then e.Cancel = True

  End Sub
  Private Sub RangeCodeSet_ClickToReferButton2(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles rsetCode1.ClickToReferButton2, rsetCode2.ClickToReferButton2
    m_isFromTextEnter = False
    If Not SearchItem(Me.ActiveControl) Then e.Cancel = True
  End Sub

  Private Sub RangeCodeSet_ToCodeTextLeave(sender As System.Object, e As System.EventArgs) Handles rsetCode1.ToCodeTextLeave, rsetCode2.ToCodeTextLeave, _
                                                                                                   rsetCode1.FromCodeTextLeave, rsetCode2.FromCodeTextLeave
    Dim objCtrl As PcaRangeCodeSet = DirectCast(sender, PcaRangeCodeSet)
    If m_isFromTextEnter Then
      objCtrl.FromCodeText = If(objCtrl.FromCodeText.Trim.Length = 0, String.Empty, objCtrl.FromCodeText.PadLeft(objCtrl.LimitLength, "0"c))
    Else
      objCtrl.ToCodeText = If(objCtrl.ToCodeText.Trim.Length = 0, String.Empty, objCtrl.ToCodeText.PadLeft(objCtrl.LimitLength, "0"c))
    End If

  End Sub

#End Region


#Region " Private Methods"
  Private Function SetTableList() As Boolean
    Try
      Dim SL_GDO As New SL_GDO(connector)
      lstTable.Text = String.Empty
      ReDim m_tableData(1)

      m_tableData = SL_GDO.ReadAll()
      lstTable.Items.Clear()
      For intLpc As Integer = 0 To m_tableData.Count - 1
        lstTable.Items.Add(m_tableData(intLpc).sl_gdo_title)
      Next

      If lstTable.Items.Count > 0 Then
        lstTable.SelectedIndex = 0
        m_isEnableOutput = True
      Else
        m_isEnableOutput = False
      End If
      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function SetFileExt(Optional ByVal strDialogPath As String = "") As Boolean
    Try
      If lstTable.SelectedIndex < 0 Then Return False

      Dim strFileBase As String = lstTable.SelectedItem.ToString
      Dim strFolderPath As String = If(txtOutputPath.Text.Length > 0, txtOutputPath.Text.Replace(IO.Path.GetFileNameWithoutExtension(txtOutputPath.Text.Trim) & IO.Path.GetExtension(txtOutputPath.Text.Trim), String.Empty), System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory).ToString & "\")
      If strDialogPath.Trim.Length > 0 And Strings.Right(strDialogPath, 1) <> "\" Then strDialogPath &= "\"

      Select Case lcmbSeparator.SelectedIndex
        Case SEPARATOR_CSV
          If strDialogPath.Trim.Length > 0 Then
            txtOutputPath.Text = strDialogPath & strFileBase & ".csv"
          Else
            txtOutputPath.Text = strFolderPath & strFileBase & ".csv"
          End If

        Case SEPARATOR_TXT
          If strDialogPath.Trim.Length > 0 Then
            txtOutputPath.Text = strDialogPath & strFileBase & ".txt"
          Else
            txtOutputPath.Text = strFolderPath & strFileBase & ".txt"
          End If

      End Select

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function SetTableCols(ByVal selectedTable As SL_GDO) As Boolean

    Try
      Dim SL_GDOD As SL_GDOD = New SL_GDOD(connector)
      Dim ColsData As SL_GDOD() = {New SL_GDOD(connector)}
      Dim aryColsHidden() As String = {""}
      Dim aryCols() As String = {""}
      Dim isGdodExists As Boolean
      Dim isItemSelected As Boolean = False
      Dim arySortedString() As String = {""}

      If selectedTable.sl_gdo_template_name.Trim.Length = 0 Then Return False

      lstContents.Items.Clear()
      lstContentsHidden.Items.Clear()

      ColsData = SL_GDOD.ReadColsData(connector.UserId, selectedTable.sl_gdo_id)
      If ColsData Is Nothing Then isGdodExists = False Else isGdodExists = ColsData(ColsData.Length - 1).isExisted

      lstContents.BeginUpdate()
      lstContentsHidden.BeginUpdate()

      If isGdodExists Then
        arySortedString = SLCmnFunction.GetColsName(selectedTable.sl_gdo_template_name, False, ColsData).Split(","c)
        For intLpc As Integer = 0 To ColsData.Length - 1
          If ColsData(intLpc).sl_gdod_select_flag = 1 Then isItemSelected = True Else isItemSelected = False
          lstContents.Items.Add(arySortedString(intLpc), isItemSelected)
          lstContentsHidden.Items.Add(ColsData(intLpc).sl_gdod_item_name)
        Next
      Else
        aryColsHidden = SLCmnFunction.GetColsName(selectedTable.sl_gdo_template_name, False).Split(","c)
        aryCols = SLCmnFunction.GetColsName(selectedTable.sl_gdo_template_name, True).Split(","c)
        For intLpc As Integer = 0 To aryCols.Length - 1
          lstContents.Items.Add(aryCols(intLpc).ToString, True)
          lstContentsHidden.Items.Add(aryColsHidden(intLpc).ToString)
        Next

      End If
      lstContentsHidden.EndUpdate()
      lstContents.EndUpdate()
      lstContents.CheckOnClick = True

      If lstContents.Items.Count > 0 Then
        m_isEnableOutput = True
        SetControlProp()
        lstContents.Focus() : lstContents.SelectedIndex = 0
      Else
        m_isEnableOutput = False
      End If

      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    Finally
    End Try

  End Function

  Private Function SetControlProp() As Boolean

    Try
      Dim SL_AMS1 As New AMS1(connector)
      Const DEF_LENGTH As Integer = 4

      SL_AMS1.ReadAll()

      With m_tableData(lstTable.SelectedIndex)
        Dim isUseDate As Boolean = If(.sl_gdo_use_date = 1 And .sl_gdo_date_title.Length > 0 And .sl_gdo_date_name.Length > 0, True, False)
        Dim isUseCode1 As Boolean = If(.sl_gdo_use_code1 = 1 And .sl_gdo_code_title1.Length > 0 And .sl_gdo_code_name1.Length > 0, True, False)
        Dim isUseCode2 As Boolean = If(.sl_gdo_use_code2 = 1 And .sl_gdo_code_title2.Length > 0 And .sl_gdo_code_name2.Length > 0, True, False)
        Dim isUseText1 As Boolean = If(.sl_gdo_use_text1 = 1 And .sl_gdo_text_title1.Length > 0 And .sl_gdo_text_name1.Length > 0, True, False)
        Dim isUseText2 As Boolean = If(.sl_gdo_use_text2 = 1 And .sl_gdo_text_title2.Length > 0 And .sl_gdo_text_name2.Length > 0, True, False)
        Dim intRemoveHeight As Integer = 0
        Dim isExistsCond As Boolean = False

        ResetCtlLocation()
        chkHeader.Checked = True

        'date
        rdtDate.Visible = isUseDate
        rdtDate.FromDate = Today
        rdtDate.toDate = Today
        If isUseDate Then
          rdtDate.RangeGroupBoxText = .sl_gdo_date_title : isExistsCond = True
        Else
          intRemoveHeight += CTR_HEIGHT + CTR_GAP
        End If

        'code1
        rsetCode1.FromCodeText = String.Empty
        rsetCode1.ToCodeText = String.Empty
        rsetCode1.Visible = isUseCode1
        rsetCode1.Top -= intRemoveHeight
        If isUseCode1 Then
          rsetCode1.RangeGroupBoxText = .sl_gdo_code_title1 : isExistsCond = True
        Else
          intRemoveHeight += CTR_HEIGHT
        End If

        'code2
        rsetCode2.FromCodeText = String.Empty
        rsetCode2.ToCodeText = String.Empty
        rsetCode2.Visible = isUseCode2
        rsetCode2.Top -= intRemoveHeight
        If isUseCode2 Then
          rsetCode2.RangeGroupBoxText = .sl_gdo_code_title2 : isExistsCond = True
        Else
          intRemoveHeight += CTR_HEIGHT
        End If

        If Not isUseCode1 And Not isUseCode2 Then intRemoveHeight += CTR_GAP

        'text1 
        txtName1.Text = String.Empty
        txtName1.Visible = isUseText1
        txtName1.Top -= intRemoveHeight
        If isUseText1 Then
          txtName1.LabelText = .sl_gdo_text_title1 : isExistsCond = True
        Else
          intRemoveHeight += CTR_HEIGHT
        End If

        'text2
        txtName2.Text = String.Empty
        txtName2.Visible = isUseText2
        txtName2.Top -= intRemoveHeight
        If isUseText2 Then
          txtName2.LabelText = .sl_gdo_text_title2 : isExistsCond = True
        Else
          intRemoveHeight += CTR_HEIGHT
        End If

        'label 
        lblCondition.Visible = isExistsCond

        'refer
        Select Case .sl_gdo_code_search_idx1
          Case SLConstants.GDOSearchIndex.Customer To SLConstants.GDOSearchIndex.Remarks
            m_isUseReferCommand1 = True
          Case Else
            m_isUseReferCommand1 = False
        End Select

        Select Case .sl_gdo_code_search_idx2
          Case SLConstants.GDOSearchIndex.Customer To SLConstants.GDOSearchIndex.Remarks
            m_isUseReferCommand2 = True
          Case Else
            m_isUseReferCommand2 = False
        End Select

        rsetCode1.ReferButtonVisible = m_isUseReferCommand1
        rsetCode2.ReferButtonVisible = m_isUseReferCommand2


          'code length
          'SL_AMS1
        Select Case .sl_gdo_code_search_idx1
          Case SLConstants.GDOSearchIndex.Customer
            rsetCode1.LimitLength = SL_AMS1.ams1_CustomerLength
            rsetCode1.MaxCodeLength = SL_AMS1.ams1_CustomerLength + 1
          Case SLConstants.GDOSearchIndex.Project
            rsetCode1.LimitLength = DEF_LENGTH
            rsetCode1.MaxCodeLength = DEF_LENGTH + 1
          Case SLConstants.GDOSearchIndex.Product
            rsetCode1.LimitLength = SL_AMS1.ams1_ProductLength
            rsetCode1.MaxCodeLength = SL_AMS1.ams1_ProductLength + 1
          Case SLConstants.GDOSearchIndex.Remarks
            rsetCode1.LimitLength = SL_AMS1.ams1_MemoLength
            rsetCode1.MaxCodeLength = SL_AMS1.ams1_MemoLength + 1
          Case SLConstants.GDOSearchIndex.Supplier
            rsetCode1.LimitLength = SL_AMS1.ams1_SupplierLength
            rsetCode1.MaxCodeLength = SL_AMS1.ams1_SupplierLength + 1
          Case SLConstants.GDOSearchIndex.Warehouse
            rsetCode1.LimitLength = SL_AMS1.ams1_WarehouseLength
            rsetCode1.MaxCodeLength = SL_AMS1.ams1_WarehouseLength + 1
          Case Else
            rsetCode1.LimitLength = DEF_LENGTH
            rsetCode1.MaxCodeLength = DEF_LENGTH + 1
        End Select

        Select Case .sl_gdo_code_search_idx2
          Case SLConstants.GDOSearchIndex.Customer
            rsetCode2.LimitLength = SL_AMS1.ams1_CustomerLength
            rsetCode2.MaxCodeLength = SL_AMS1.ams1_CustomerLength + 1
          Case SLConstants.GDOSearchIndex.Project
            rsetCode2.LimitLength = DEF_LENGTH
            rsetCode2.MaxCodeLength = DEF_LENGTH + 1
          Case SLConstants.GDOSearchIndex.Product
            rsetCode2.LimitLength = SL_AMS1.ams1_ProductLength
            rsetCode2.MaxCodeLength = SL_AMS1.ams1_ProductLength + 1
          Case SLConstants.GDOSearchIndex.Remarks
            rsetCode2.LimitLength = SL_AMS1.ams1_MemoLength
            rsetCode2.MaxCodeLength = SL_AMS1.ams1_MemoLength + 1
          Case SLConstants.GDOSearchIndex.Supplier
            rsetCode2.LimitLength = SL_AMS1.ams1_SupplierLength
            rsetCode2.MaxCodeLength = SL_AMS1.ams1_SupplierLength + 1
          Case SLConstants.GDOSearchIndex.Warehouse
            rsetCode2.LimitLength = SL_AMS1.ams1_WarehouseLength
            rsetCode2.MaxCodeLength = SL_AMS1.ams1_WarehouseLength + 1
          Case Else
            rsetCode2.LimitLength = DEF_LENGTH
            rsetCode2.MaxCodeLength = DEF_LENGTH + 1
        End Select

        'adjustment height
        If isExistsCond Then
          lstContents.Top = CTR_TOP_CONTENTS - intRemoveHeight
          lstContents.Height = CTR_HEIGHT_CONTENTS + intRemoveHeight + If(isUseDate, 0, (CTR_GAP_END - CTR_GAP))

        Else
          lstContents.Top = lstTable.Top
          lstContents.Height = lstTable.Height
        End If
        lblContents.Top = lstContents.Top - CTR_GAP

      End With
      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try

  End Function

  Private Function MoveItem(ByVal isMoveUp As Boolean) As Boolean
    Try
      Dim intIndex As Integer
      Dim strText As String
      Dim strTextHidden As String
      Dim isChecked As Boolean
      Dim intInsertIndex As Integer

      If Me.lstContents.SelectedIndex > -1 Then

        intIndex = Me.lstContents.SelectedIndex
        If isMoveUp Then
          If intIndex = 0 Then Return False
        Else
          If intIndex = Me.lstContents.Items.Count - 1 Then Return False
        End If

        'get data moveing source
        strText = Me.lstContents.SelectedItem.ToString
        strTextHidden = Me.lstContentsHidden.Items(intIndex).ToString
        isChecked = Me.lstContents.GetItemChecked(intIndex)

        'remove data
        Me.lstContents.Items.RemoveAt(intIndex)
        Me.lstContentsHidden.Items.RemoveAt(intIndex)

        'insert data
        If isMoveUp Then
          intInsertIndex = intIndex - 1
        Else
          intInsertIndex = intIndex + 1
        End If

        Me.lstContents.Items.Insert(intInsertIndex, strText)
        Me.lstContents.SetItemChecked(intInsertIndex, isChecked)
        Me.lstContentsHidden.Items.Insert(intInsertIndex, strTextHidden)

        'select after moving data
        Me.lstContents.SelectedIndex = intInsertIndex
        Me.lstContents.Focus()

      End If

      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function ResetCtlLocation() As Boolean
    Try

      'code1
      rsetCode1.Visible = True
      rsetCode1.Top = CTR_TOP_CODE1

      'code2
      rsetCode2.Visible = True
      rsetCode2.Top = CTR_TOP_CODE2

      'text1 
      txtName1.Visible = True
      txtName1.Top = CTR_TOP_TEXT1

      'text2
      txtName2.Visible = True
      txtName2.Top = CTR_TOP_TEXT2

      'label 
      lblCondition.Visible = True

      'adjustment height
      lstContents.Top = CTR_TOP_CONTENTS
      lstContents.Height = CTR_HEIGHT_CONTENTS

      Return True
    Catch ex As Exception
      Sunloft.Message.DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function SearchItem(ByVal sender As System.Object) As Boolean
    Dim rsetCtrl As PcaRangeCodeSet
    Dim itemName As String = String.Empty
    Dim newCode As String = String.Empty
    Dim location As Point '= Tools.ControlTool.GetDialogLocation(Me.tblResult.RefButton)
    Dim strCurrentCode As String = String.Empty
    Dim seachIndex As SLConstants.GDOSearchIndex = SLConstants.GDOSearchIndex.None

    Dim EMST As New EMST(connector)
    Dim RMS As New RMS(connector)
    Dim EMSB As New EMSB(connector)
    Dim PJMS As New PJMS(connector)
    Dim EMS As New EMS(connector)

    Try
      Dim masterDialog As SLMasterSearchDialog = New SLMasterSearchDialog(connector)

      rsetCtrl = DirectCast(sender, PcaRangeCodeSet)
      If m_isFromTextEnter Then
        location = Tools.ControlTool.GetDialogLocation(rsetCtrl.FromReferButton)
        strCurrentCode = rsetCtrl.FromCodeText.Trim
      Else
        location = Tools.ControlTool.GetDialogLocation(rsetCtrl.ToReferButton)
        strCurrentCode = rsetCtrl.ToCodeText.Trim
      End If

      If rsetCtrl.Name = rsetCode1.Name Then
        seachIndex = m_tableData(lstTable.SelectedIndex).sl_gdo_code_search_idx1
      Else
        seachIndex = m_tableData(lstTable.SelectedIndex).sl_gdo_code_search_idx2
      End If


      Select Case seachIndex
        Case SLConstants.GDOSearchIndex.Customer
          newCode = masterDialog.ShowReferTmsDialog(strCurrentCode, location)
        Case SLConstants.GDOSearchIndex.Supplier
          newCode = masterDialog.ShowReferRmsDialog(strCurrentCode, location)
        Case SLConstants.GDOSearchIndex.Product
          newCode = masterDialog.ShowReferSmsDialog(strCurrentCode, location)
        Case SLConstants.GDOSearchIndex.Warehouse
          newCode = masterDialog.ShowReferSokoDialog(strCurrentCode, location)
        Case SLConstants.GDOSearchIndex.Project
          newCode = masterDialog.ShowReferProjectDialog(strCurrentCode, location)
        Case SLConstants.GDOSearchIndex.Remarks
          newCode = masterDialog.ShowReferTekiyoDialog(strCurrentCode, location)

        Case Else

          Return True
      End Select

      If m_isFromTextEnter Then
        rsetCtrl.FromCodeText = newCode
        If newCode.Trim.Length = 0 Then rsetCtrl.FocusFrom()
      Else
        rsetCtrl.ToCodeText = newCode
        If newCode.Trim.Length = 0 Then rsetCtrl.FocusTo()
      End If

      Return newCode.Trim.Length > 0

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  ''' <summary>
  ''' F3,F4 function. check all item or uncheck all item
  ''' </summary>
  ''' <param name="isAllchecked">F3 -> true ,F4 -> False</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SelectedContents(ByVal isAllchecked As Boolean) As Boolean
    Try

      For intLpc As Integer = 0 To lstContents.Items.Count - 1
        lstContents.SetItemChecked(intLpc, isAllchecked)
      Next

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  ''' <summary>The output of the display contents</summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function OutPutCSV() As Boolean

    Dim strSaveFileName As String = String.Empty
    Dim strHeader As String = String.Empty
    Dim sbOutPut As New StringBuilder
    Dim intLpc As Integer = 0
    Dim intRow As Integer = 0
    Dim strSeparator As String = String.Empty

    Dim TableData As SL_GDO

    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

    Try
      If lcmbSeparator.SelectedIndex = 0 Then strSeparator = "," Else strSeparator = vbTab
      If lstContents.CheckedItems.Count = 0 Then
        SLCmnFunction.ShowToolTip("出力する項目" & SLConstants.NotifyMessage.SELECT_REQUIRED, _
                                  SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemOutput.CommandId)
        Return False
      End If

      If lstTable.SelectedIndex < 0 Then Return False

      TableData = m_tableData(lstTable.SelectedIndex)
      sbOutPut = GetOutputData(TableData, strSeparator)

      If sbOutPut.Length = 0 Then
        SLCmnFunction.ShowToolTip(SLConstants.NotifyMessage.DATA_NOT_FOUND, SLConstants.NotifyMessage.TITLE_MESSAGE, ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemOutput.CommandId)
        Return False
      End If

      With SaveFileDialog1
        .RestoreDirectory = True
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
        .Filter = SLConstants.CSV.CSV_FILE_FILTER
        If lcmbSeparator.SelectedIndex = 1 Then .Filter = .Filter.Replace("csv", "txt") : .Filter = .Filter.Replace("CSV", "TXT")
        .OverwritePrompt = True
        .FileName = TableData.sl_gdo_title

        Dim res As System.Windows.Forms.DialogResult = .ShowDialog(Me)

        If res = Windows.Forms.DialogResult.Cancel Then Return False

        strSaveFileName = .FileName

      End With

      'Header 
      If chkHeader.Checked Then
        For intLpc = 0 To lstContents.Items.Count - 1
          If lstContents.GetItemChecked(intLpc) Then strHeader &= lstContents.Items(intLpc).ToString & strSeparator
        Next
        strHeader = If(strHeader.Trim.Length > 0, Strings.Left(strHeader.Trim.ToString, strHeader.Trim.ToString.Length - 1), String.Empty)
      End If

      If Not sbOutPut Is Nothing AndAlso sbOutPut.Length > 0 Then
        SLCmnFunction.OutPutCSV(strHeader, sbOutPut, strSaveFileName)
      Else

      End If


    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try

    Return True

  End Function

  Private Function GetOutputData(ByVal TableData As SL_GDO, ByVal strSeparator As String) As StringBuilder
    Dim selectCommand As ICustomCommand
    Dim reader As ICustomDataReader = Nothing
    Dim sbOutPut As New StringBuilder
    Dim dtDate As Date
    Dim strInsertData As String
    Dim strOtherSeparator As String = If(strSeparator = ",", " ", ",")

    Try

      Dim intColCnt As Integer = 0
      Dim intLpc As Integer = 0

      Dim sbRow As New StringBuilder

      selectCommand = connector.CreateIsolatedCommand(m_conf.PcaAddinID.Trim & "%" & TableData.sl_gdo_template_name)

      With TableData

        Dim isUseDate As Boolean = If(.sl_gdo_use_date = 1 And .sl_gdo_date_title.Length > 0 And .sl_gdo_date_name.Length > 0, True, False)
        Dim isUseCode1 As Boolean = If(.sl_gdo_use_code1 = 1 And .sl_gdo_code_title1.Length > 0 And .sl_gdo_code_name1.Length > 0, True, False)
        Dim isUseCode2 As Boolean = If(.sl_gdo_use_code2 = 1 And .sl_gdo_code_title2.Length > 0 And .sl_gdo_code_name2.Length > 0, True, False)
        Dim isUseText1 As Boolean = If(.sl_gdo_use_text1 = 1 And .sl_gdo_text_title1.Length > 0 And .sl_gdo_text_name1.Length > 0, True, False)
        Dim isUseText2 As Boolean = If(.sl_gdo_use_text2 = 1 And .sl_gdo_text_title2.Length > 0 And .sl_gdo_text_name2.Length > 0, True, False)

        If isUseDate Then
          If Date.TryParse(rdtDate.FromIntDate.ToString, dtDate) Then selectCommand.Parameters("@" & .sl_gdo_date_name & "From").SetValue(rdtDate.FromIntDate.ToString.Replace("/", ""))
          If Date.TryParse(rdtDate.ToIntDate.ToString, dtDate) Then selectCommand.Parameters("@" & .sl_gdo_date_name & "To").SetValue(rdtDate.ToIntDate.ToString.Replace("/", ""))
        End If
        If isUseCode1 Then
          If rsetCode1.FromCodeText.Length > 0 Then selectCommand.Parameters("@" & .sl_gdo_code_name1 & "From").SetValue(rsetCode1.FromCodeText)
          If rsetCode1.ToCodeText.Length > 0 Then selectCommand.Parameters("@" & .sl_gdo_code_name1 & "To").SetValue(rsetCode1.ToCodeText)
        End If
        If isUseCode2 Then
          If rsetCode2.FromCodeText.Length > 0 Then selectCommand.Parameters("@" & .sl_gdo_code_name2 & "From").SetValue(rsetCode2.FromCodeText)
          If rsetCode2.ToCodeText.Length > 0 Then selectCommand.Parameters("@" & .sl_gdo_code_name2 & "To").SetValue(rsetCode2.ToCodeText)
        End If
        If isUseText1 Then If txtName1.Text.Trim.Length > 0 Then selectCommand.Parameters("@" & .sl_gdo_text_name1).SetValue(txtName1.Text.Trim)

        If isUseText2 Then If txtName2.Text.Trim.Length > 0 Then selectCommand.Parameters("@" & .sl_gdo_text_name2).SetValue(txtName2.Text.Trim)

      End With

      reader = selectCommand.ExecuteReader

      intColCnt = reader.ColumnCount
      While reader.Read()
        sbRow.Length = 0
        For intLpc = 0 To lstContents.Items.Count - 1

          strInsertData = reader.GetValue(lstContentsHidden.Items(intLpc).ToString).ToString.Replace(strSeparator, strOtherSeparator)
          If lstContents.GetItemChecked(intLpc) Then sbRow.Append(strInsertData & strSeparator)
        Next
        sbOutPut.AppendLine(Strings.Left(sbRow.ToString, sbRow.Length - 1))

      End While
      Return sbOutPut
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return Nothing
    Finally
      If Not reader Is Nothing Then
        reader.Close()
        reader.Dispose()
      End If
      Cursor.Current = Cursors.Default
    End Try

  End Function

  Private Function DeleteHistryData(ByRef session As ICustomSession) As Boolean
    Try
      Dim SL_GDOD = New SL_GDOD(connector) With {.sl_gdod_userid = connector.UserId, _
                                                 .sl_gdod_id = -1, .sl_gdod_hid = m_tableData(lstTable.SelectedIndex).sl_gdo_id}
      Return SL_GDOD.Delete(session)

    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

  Private Function InsertHistryData(ByRef session As ICustomSession) As Boolean

    Try
      Dim SL_GDOD = New SL_GDOD(connector)

      For intLpc As Integer = 0 To lstContents.Items.Count - 1
        With SL_GDOD
          .sl_gdod_id = -1
          .sl_gdod_hid = m_tableData(lstTable.SelectedIndex).sl_gdo_id
          .sl_gdod_userid = connector.UserId
          .sl_gdod_item_name = lstContentsHidden.Items(intLpc).ToString
          .sl_gdod_order = intLpc + 1
          .sl_gdod_select_flag = If(lstContents.GetItemChecked(intLpc), 1S, 0S)
          If Not .Insert(session) Then Return False
        End With
      Next

      Return True
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
      Return False
    End Try
  End Function

#End Region

#Region "PCA command Manager"
  ''' <summary>
  ''' Event called when PCACommandManager is called (PCA Function Bar button is pressed/Select from Menu, press F～ key)
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub PcaCommandManager1_Command(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.Command
    Try
      Dim commandItem As PcaCommandItem = e.CommandItem
      Dim isSuccess As Boolean = True

      Select Case commandItem.CommandName

        Case PcaCommandItemHelp.CommandName 'F1

        Case PcaCommandItemAllSelect.CommandName 'F3
          SelectedContents(True)

        Case PcaCommandItemAllUnSelect.CommandName 'F4
          'control keycode
          If Me.ActiveControl.Name = lcmbSeparator.Name Then lstContents.Focus()

          SelectedContents(False)

        Case PcaCommandItemMoveUp.CommandName 'F6
          MoveItem(True)

        Case PcaCommandItemMoveDown.CommandName  'F7
          MoveItem(False)

        Case PcaCommandItemRefer.CommandName  'F8

        Case PcaCommandItemOutput.CommandName  'F9
          Dim session As ICustomSession = Nothing
          Dim isRet As Boolean = False
          Cursor.Current = Cursors.WaitCursor
          If OutPutCSV() Then
            session = connector.CreateTransactionalSession
            isRet = DeleteHistryData(session)
            If isRet Then isRet = InsertHistryData(session)
            If isRet Then
              session.Commit()
              SLCmnFunction.ShowToolTip(SLConstants.CSV.CSV_OUTPUT_COMPLETE_MESSAGE, "完了", ToolTipIcon.Info, PcaFunctionBar1, ToolTip1, Me, PcaCommandItemOutput.CommandId)
            Else
              session.Rollback()

            End If
            session.Dispose()
          End If
        Case PcaCommandItemClose.CommandName  'F12

          Me.Close()

      End Select
    Catch ex As Exception
      DisplayBox.ShowCritical(ex)
    Finally
      Cursor.Current = Cursors.Default
    End Try
  End Sub

  '***********************************************************																														
  'Name          : PcaCommandManager1_UpdateCommandUI
  'Content       :  F12 = Close / Register
  'Return Value  : 
  'Argument      : slipNo, isClearDataFirst = False in case of Reset (Remain data in labels)
  'Created date  : 2015/11/23  by Lai Minh Hien (PCA Sample)
  'Modified date :   by       Content: 
  '***********************************************************	
  Private Sub PcaCommandManager1_UpdateCommandUI(sender As Object, e As PCA.Controls.CommandItemEventArgs) Handles PcaCommandManager1.UpdateCommandUI
    Dim commandItem As PcaCommandItem = e.CommandItem

    ToolStripMenuItemOutput.Enabled = False
    ToolStripMenuItemClose.Enabled = True

    ToolStripButtonOutput.Enabled = False
    ToolStripButtonClose.Enabled = True

    'movebutton enable
    If lstContents.SelectedIndex < 0 Then m_isEnableMoveUP = False : m_isEnableMoveDown = False

    Button1.Enabled = m_isEnableMoveUP
    PcaFunctionCommandMoveUp.Enabled = m_isEnableMoveUP
    Button2.Enabled = m_isEnableMoveDown
    PcaFunctionCommandMoveDown.Enabled = m_isEnableMoveDown

    'refer command enabled
    If Me.ActiveControl.Name = rsetCode1.Name Then
      PcaFunctionCommandRefer.Enabled = m_isUseReferCommand1
      ToolStripMenuItemRefer.Enabled = m_isUseReferCommand1
    ElseIf Me.ActiveControl.Name = rsetCode2.Name Then
      PcaFunctionCommandRefer.Enabled = m_isUseReferCommand2
      ToolStripMenuItemRefer.Enabled = m_isUseReferCommand2
    Else
      PcaFunctionCommandRefer.Enabled = False
      ToolStripMenuItemRefer.Enabled = False
    End If

    'output command enbaled
    PcaFunctionCommandOutput.Enabled = m_isEnableOutput
    ToolStripMenuItemOutput.Enabled = m_isEnableOutput
    ToolStripButtonOutput.Enabled = m_isEnableOutput

  End Sub
#End Region

  
  Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
    InitControl()
  End Sub
End Class
